package com.deerwalk.z5


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(TrackUserInterface)
class TrackUserInterfaceTests {

    void testSomething() {
        fail "Implement me"
    }
}
