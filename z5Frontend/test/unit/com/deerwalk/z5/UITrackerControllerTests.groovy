package com.deerwalk.z5



import org.junit.*
import grails.test.mixin.*

@TestFor(UITrackerController)
@Mock(UITracker)
class UITrackerControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()


        assert "/UITracker/list" == response.redirectedUrl
    }

    void "index test"(){
        when:
        controller.index();

        then:
        response.redirectedUrl=="/UITracker/list"
    }

    void testList() {

        def model = controller.list()

        assert model.UITrackerInstanceList.size() == 0
        assert model.UITrackerInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.UITrackerInstance != null
    }

    void testSave() {
        controller.save()

        assert model.UITrackerInstance != null
        assert view == '/UITracker/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/UITracker/show/1'
        assert controller.flash.message != null
        assert UITracker.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/UITracker/list'

        populateValidParams(params)
        def UITracker = new UITracker(params)

        assert UITracker.save() != null

        params.id = UITracker.id

        def model = controller.show()

        assert model.UITrackerInstance == UITracker
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/UITracker/list'

        populateValidParams(params)
        def UITracker = new UITracker(params)

        assert UITracker.save() != null

        params.id = UITracker.id

        def model = controller.edit()

        assert model.UITrackerInstance == UITracker
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/UITracker/list'

        response.reset()

        populateValidParams(params)
        def UITracker = new UITracker(params)

        assert UITracker.save() != null

        // test invalid parameters in update
        params.id = UITracker.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/UITracker/edit"
        assert model.UITrackerInstance != null

        UITracker.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/UITracker/show/$UITracker.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        UITracker.clearErrors()

        populateValidParams(params)
        params.id = UITracker.id
        params.version = -1
        controller.update()

        assert view == "/UITracker/edit"
        assert model.UITrackerInstance != null
        assert model.UITrackerInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/UITracker/list'

        response.reset()

        populateValidParams(params)
        def UITracker = new UITracker(params)

        assert UITracker.save() != null
        assert UITracker.count() == 1

        params.id = UITracker.id

        controller.delete()

        assert UITracker.count() == 0
        assert UITracker.get(UITracker.id) == null
        assert response.redirectedUrl == '/UITracker/list'
    }
}
