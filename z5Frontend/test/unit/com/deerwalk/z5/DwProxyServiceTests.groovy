package com.deerwalk.z5


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(DwProxyService)
class DwProxyServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
