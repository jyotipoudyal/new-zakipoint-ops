// Set the dimensions of the canvas / graph
var margin = {top: 8, right: 8, bottom: 28, left: 48},
    width = 280 - margin.left - margin.right,
    height = 80 - margin.top - margin.bottom;
var xColumn, yColumn;
var transitionDuration = 800;
var nationalBenchmargin=4;
var textDuration = 30;
var dFormat=d3.time.format("%B %Y")

// Parse the date / time
var parseDate = d3.time.format("%d-%b-%y").parse;
var path1, path2;

// Set the ranges
var x = d3.time.scale().range([0, width]);
var y = d3.scale.linear().range([height, 0]);

// Define the axes
var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5).tickSubdivide(true);

var yAxis = d3.svg.axis().scale(y)
    .orient("left")

// Define the line
var valueline = d3.svg.line()
    .x(function (d) {
        return x(d.date);
    })
    .y(function (d) {
        return y(d.close);
    });


var valuelinePaid = d3.svg.line()
    .x(function (d) {
        return x(d.date);
    })
    .y(function (d) {
        return y(d.medicalClaims);
    });

var valuelinePhPaid = d3.svg.line()
    .x(function (d) {
        return x(d.date);
    })
    .y(function (d) {
        return y(d.pharmacy);
    });


var valueline1a = d3.svg.line()
    .x(function (d) {
        return x(d.date);
    })
    .y(function (d) {
        return y(d.benchmark);
    });

var line = d3.svg.line()
    .x(function (d) {
        return x(d[xColumn]);
    })
    .y(function (d) {
        return y(d[yColumn]);
    });

// Adds the svg canvas
var svg1, svg2, svg3;
var threshold = (height / 2);


/*function render(data) {
 //console.log(data);


 svg1 = d3.select("#graph1")
 .append("svg")
 .attr("width", width + margin.left + margin.right)
 .attr("height", height + margin.top + margin.bottom)
 .append("g")
 .attr("transform",
 "translate(" + margin.left + "," + margin.top + ")");

 x.domain(d3.extent(data, function (d) {
 return d.date;
 }).reverse());
 y.domain(d3.extent(data, function (d) {
 return d.close;
 }));


 var median = svg1.append("line")
 .attr("x1", 0)
 .attr("y1", threshold)
 .attr("x2", width)
 .attr("y2", threshold)
 .attr("stroke-width", 1)
 .attr("stroke", "gray");


 svg1.append("linearGradient")
 .attr("id", "line-gradient")
 .attr("gradientUnits", "userSpaceOnUse")
 .attr("x1", 0).attr("y1", height)
 .attr("x2", 0).attr("y2", 0)
 .selectAll("stop")
 .data([
 {offset: "0%", color: "#939393"},
 {offset: "50%", color: "#939393"},
 {offset: "50%", color: "red"},
 {offset: "100%", color: "red"}
 ])
 .enter().append("stop")
 .attr("offset", function (d) {
 return d.offset;
 })
 .attr("stop-color", function (d) {
 return d.color;
 });


 path1 = svg1.append("path")
 .attr("class", "line")
 .attr("d", valueline(data));
 var totalLength = path1.node().getTotalLength();


 path1
 .attr("stroke-dasharray", totalLength + " " + totalLength)
 .attr("stroke-dashoffset", totalLength)
 .transition()
 .duration(transitionDuration)
 .ease("linear")
 .attr("stroke-dashoffset", 0)
 .each('end', function () {
 showText("#graph1", "$" + Math.round(+d3.mean(data, function (d) {
 return d.close;
 })) + "/PMPM", 0, textDuration);
 });

 }*/

var div ;

function render1(d,type) {
    $("#tooltips").remove();
    $("#graph1").html("");
    $("#graph1Legend1").find("span").html("");
    $("#graph1Legend2").find("span").html("");

    div = d3.select(".contentWrapper").append("div")
        .attr("class", "tooltip")
        .attr("id", "tooltips")
        .style("opacity", 0);



    var parseDate4 = d3.time.format("%Y-%m").parse;
    var parseYear = d3.time.format("%Y").parse;
    var pharmacyClaims = d.pharmacyClaims;
    var memberMonths = d.memberMonths;
    var benchMark = d.benchmark;
    var newVal;
    var data = [];
    $.each(pharmacyClaims,function(key,val) {
        newVal = new Object();
        newVal.date=parseDate4(key);
        //newVal.close=(pharmacyClaims[key]/memberMonths[key])|| 0;
        newVal.close=$.isNumeric( (pharmacyClaims[key]/memberMonths[key]))?(pharmacyClaims[key]/memberMonths[key]):0;
        newVal.pharmacy=pharmacyClaims[key]|| 0;
        newVal.memberMonths=memberMonths[key]|| 0;
        newVal.benchmark=Math.round(benchMark)|| 0;
        data.push(newVal);
    });

    data.sort(custom_sort);

    var lastData=data.slice(-12);
    var initialValue=new Object();
    initialValue.memberMonths=0;
    initialValue.pharmacy=0;

    var pmpmValue=lastData.reduce(function(acc,obj){
            //acc.push(item*2);   //mapping
            acc.memberMonths+=obj.memberMonths;
            acc.pharmacy+=obj.pharmacy;

        return acc;
    },initialValue);

    //var pmpmYearly=(initialValue.pharmacy/initialValue.memberMonths)||0
    var pmpmYearly=$.isNumeric( (initialValue.pharmacy/initialValue.memberMonths) )?(initialValue.pharmacy/initialValue.memberMonths):0;
    if(type=="Paid") {
        pmpmYearly=initialValue.pharmacy;
    }

    var formatDate =d3.time.format("%d-%b-%y"),
        parseDate = formatDate.parse,
        bisectDate = d3.bisector(function(d) { return d.date; }).left,
        //format0 = function(d) { return formatDate(d.date) + " - $ " + d.close; },
        //format1 = function(d) { return formatDate(d.date) + " - $ " + d.benchmark; },
        formatOutput2 = function(d) { return "Time"+parseDate(d.date) +"/PMPM actual"; },
        formatOutput0 = function(d) { return "$"+Math.round(d.close) +" "+type+" actual"; },
        formatOutput3 = function(d) { return d3.format("$,")(Math.round(d.pharmacy)) +" Paid actual"; },
        //formatOutput0 ="$"+Math.round(pmpmYearly) +" PMPM actual",
        formatOutput1 = function(d) { return "$"+Math.round(d.benchmark) +" "+type+" benchmark"; };


    svg2 = d3.select("#graph1").append("svg")
        .attr("width", width + margin.left + margin.right+120)
        .attr("height", height + margin.top + margin.bottom);
    var g2 = svg2.append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    x.domain(d3.extent(data, function (d) {
        return d.date;
    }));
   /* y.domain(d3.extent(data, function (d) {
        return d.close;
    }));*/

    var array_y0;

    if(type=="Paid") {
        array_y0 = d3.extent(data, function (d) {
            return d.pharmacy;
        });
    }else{
        array_y0=d3.extent(data, function(d) { return d.close; });
        var array_y1=d3.extent(data, function(d) { return d.benchmark; });
        array_y0=array_y0.concat(array_y1);
    }

    y.domain([d3.min(array_y0),d3.max(array_y0)]);

    /*var median = g2.append("line")
     .attr("x1", 0)
     .attr("y1", threshold)
     .attr("x2", width)
     .attr("y2", threshold)
     .attr("stroke-width", 1)
     .attr("stroke", "gray");

     g2.append("linearGradient")
     .attr("id", "line-gradient2")
     .attr("gradientUnits", "userSpaceOnUse")
     .attr("x1", 0).attr("y1", height)
     .attr("x2", 0).attr("y2", 0)
     .selectAll("stop")
     .data([
     {offset: "0%", color: "#939393"},
     {offset: "60%", color: "#939393"},
     {offset: "60%", color: "red"},
     {offset: "100%", color: "red"}
     ])
     .enter().append("stop")
     .attr("offset", function (d) {
     return d.offset;
     })
     .attr("stop-color", function (d) {
     return d.color;
     });*/
    //var value1=Math.round(data[data.length-1].close)

    var value1=d3.format("$,")(Math.round(pmpmYearly))
    var value2=Math.round(data[data.length-1].date)
    var value3=Math.round(data[data.length-1].benchmark)


    var totalLength;

    if(type=="PMPM") {
        var path2b = g2.append("path")
            .attr("d", valueline1a(data))
            .attr("class", "lineb")
            .attr('shape-rendering', ' crispEdges');

        totalLength = path2b.node().getTotalLength();

        path2b
            .attr("stroke-dasharray", totalLength + " " + totalLength)
            .attr("stroke-dashoffset", totalLength)
            .transition()
            .duration(transitionDuration)
            .ease("linear")
            .attr("stroke-dashoffset", 0)
    }

    path2 = g2.append("path");

    if(type=="PMPM") {
        path2.attr("d", valueline(data))
            .attr("class", "line")
            .attr('shape-rendering', 'crispEdges');
    }else{
        path2.attr("d", valuelinePhPaid(data))
            .attr("class", "line1")
            .attr('shape-rendering', 'crispEdges');
    }


     totalLength = path2.node().getTotalLength();
    path2
        .attr("stroke-dasharray", totalLength + " " + totalLength)
        .attr("stroke-dashoffset", totalLength)
        .transition()
        .duration(transitionDuration)
        .ease("linear")
        .attr("stroke-dashoffset", 0)
        .each('end', function () {
            //showText("#graph1", "$" + value1 + "/PMPM", 0, textDuration)
            //showTextBench("#graph1Legend1", "$" +value1 + " PMPM actual", 0, textDuration);
            //showTextBench("#graph1Legend2", "$" +value3 + " PMPM expected", 0, textDuration);
            /*$("#graph1Legend1").find("span").html(value1 +" "+type+" actual");
            if(type=="PMPM") {
                $("#graph1Legend2").find("span").html("$" + value3 + " " + type + " benchmark");
            }*/

            var ab=$("#graph1Legend1").find("span").html(value1 +" "+type+""+" actual");
            if(type=="PMPM") {
                $("#graph1Legend2").find("span").html("$" + value3 + " " + type + "" + " benchmark");
                ab.css("color","red");
            }else{
                ab.css("color","green");
            }



            /* showText("#graph2", "$" + Math.round(+d3.mean(data, function (d) {
             return d.close;
             })) + "/PMPM", 0, textDuration);*/
        });


    if(type=="PMPM") {
        var textLabel = g2.append('g')
            .classed('labels-group', true);

        textLabel
            .append('text')
            .classed('actualLabel', true)
            .attr({
                'x': function () {
                    return x(value2) + nationalBenchmargin;
                },
                'y': function () {
                    return y(value3) + 3;
                }
            })
            .text(function (d, i) {
                return "National"
            })

        textLabel
            .append('text')
            .classed('actualLabel', true)
            .attr({
                'x': function () {
                    return x(value2) + nationalBenchmargin;
                },
                'y': function () {
                    return y(value3) + 15;
                }
            })
            .text(function (d, i) {
                return "Benchmark"
            })
    }






    var focus = g2.append("g")
        .attr("class", "focus")
        .style("display", "block");

    /*focus.append("line")
        .attr("class", "x")
        .attr("y1", 0);*/

    /*focus.append("circle")
        .attr("class", "y0")
        .attr("r", 3);

    focus.append("text")
        .attr("class", "y0")
        .attr("dy", "-1em");

    focus.append("circle")
        .attr("class", "y1")
        .attr("r", 3);

    focus.append("text")
        .attr("class", "y1")
        .attr("dy", "-1em");*/


    g2.append("rect")
        .attr("class", "overlay")
        .attr("fill","none")
        .attr("pointer-events","all")
        .attr("width", width)
        .attr("height", height)
        .on("mouseover", function() { })
        .on("mouseout", function() {
            div.transition()
                .duration(500)
                .style("opacity", 0);

        })
        .on("mousemove", mousemove1);





    var formatDate =d3.time.format("%y");
    var dupVal=[];
    var abc= data.reduce(function(newVal,item){
            if(dupVal.indexOf(formatDate(item.date))<0){
                dupVal.push(formatDate(item.date))
                newVal.push(item.date)

            }

            return newVal;

    },[]);

    var main_xAxis2 = d3.svg.axis()
        .scale(x)
        //.ticks(d3.time.months, 12)//should display 1 year intervals
        .tickFormat(d3.time.format('%Y'))//%Y-for year boundaries, such as 2011
        .tickValues(abc)
        .outerTickSize(0)
        .orient("bottom");



    g2.append("g")
        .attr("class", "x axis xaxisLeft")
        .attr("transform", "translate(0," + (height) + ")")
        .call(main_xAxis2)
        .selectAll("path")
        .attr("stroke", "#ccc");

    var main_yAxisLeft = d3.svg.axis()
        .scale(y)
        .ticks(3)
        .tickSize(2)
        .outerTickSize(0)
        .orient("left");

    g2.append("g")
        .attr("class", "y axis yaxisLeft")
        .call(main_yAxisLeft)
        .selectAll("text")
        .attr("x", -5)


    function mousemove1() {

        var x0 = x.invert(d3.mouse(this)[0]),
            i = bisectDate(data, x0, 1),
            d0 = data[i - 1],
            d1 = data[i],
            d = x0 - d0.date > d1.date - x0 ? d1 : d0;

        //focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");
        //focus.select("text.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");//.text(formatOutput0(d));
        //focus.select("circle.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");
        //focus.select("text.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");//.text(formatOutput1(d));
        //focus.select(".x").attr("transform", "translate(" + x(d.date) + ","+y(d.benchmark>d.close?d.benchmark:d.close)+")").attr("y2",height-y(d.benchmark>d.close?d.benchmark:d.close));
        //focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));

        div.transition()
            .duration(100)
            .style("opacity",.9)

        var addPaid="<span class='formatOutput2'>"+formatOutput3(d)+"</span>"
        var addBenchMark="<br>"+addPaid+"<br><span class='formatOutput1'>"+formatOutput1(d)+"</span>"

        div .html("<span class='formatOutput0'>"+formatOutput0(d)+"</span>"+addBenchMark+"<br><br><span class='formatTime'>"+dFormat(d.date)+"</span>")
            .style("left", (d3.event.pageX +30) + "px")
            .style("top", (d3.event.pageY -200) + "px");

        //focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.actual) + ")").attr("x2", main_width + main_x(d.time1));
        //focus.select(".y1").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.expected) + ")").attr("x2", main_width + main_x(d.time1));
        //focus.select(".y1").attr("transform", "translate(0, " + main_y(d.expected) + ")").attr("x1", main_x(d.time1));
    }
}

function formatDates(d) {
    var oldDate=new Date(d);
    var newDate2=oldDate;
    newDate2.setFullYear(newDate2.getFullYear() - 1);
    var leapCondition=false;

    if((newDate2.getFullYear() % 400 == 0) || ((newDate2.getFullYear() % 4 == 0) && (newDate2.getFullYear() % 100 != 0))){
        leapCondition=true;
    }


    if((newDate2.getMonth()+1)==2 && leapCondition){
        leapCondition=true;
    }else{
        leapCondition=false;
    }

    if(leapCondition){
        newDate2.setDate(newDate2.getDate() +2);

    }else{
        newDate2.setDate(newDate2.getDate() +1);
    }

    var dateString= d3.time.format("%Y")(d).toString()
    return dateString;
}

function renderFunnelBar(data,content){
    $("#"+content).html("")
    $("#"+content+"Content").html("")
    var arr=["RxUtilization","ClaimsUnder10K","ClaimsOver10K","MedAdherence","Admission","ERUtil","Biometric","GapInCareReport","PMPM","Risk"]


    var margin = {top: 24, right: 8, bottom: 8, left: 160},
        width = 1000 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;


    var y0 = Math.max(Math.abs(d3.min(data.map(function (d) {
        return d.MemberCount;
    }))), Math.abs(d3.max(data.map(function (d) {
        return d.MemberCount;
    }))));

    //var colors =["#FDE895", "#B3C5E9", "#B3C5E9", "#A8D18D", "#A8D18D", "#0099c6"];
    var colors =["#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5"];


    var y = d3.scale.linear()
        .domain([-y0/2, y0/2])
        .range([height,0])
        .nice();

    var x = d3.scale.ordinal()
        .domain(data.map(function (d) {
            return d.Status;
        }))
        //.domain(d3.range(data.length))
        .rangeRoundBands([0, width], .45);


    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var svg = d3.select("#"+content).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + (margin.left) + "," + margin.top + ")");

   /* svg.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        //.attr("class", function(d) { return -d.MemberCount < 0 ? "bar negative" : "bar positive"; })
        .attr("class","bar positive")
        .attr("fill",function(d,i){return colors[i];})
        .attr("y", function(d) {
            //return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
            return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
        })
        .attr("height", function (d) {
            return Math.abs(y(-d.MemberCount) - y(0));
        })
        .attr("x", function(d) { return x(d.Status); })
        .attr("width", x.rangeBand())
        .transition()
        .duration(1000)*/

    /*svg.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        //.attr("class", function(d) { return -d.MemberCount < 0 ? "bar negative" : "bar positive"; })
        .attr("class","bar positive")
        .attr("fill",function(d,i){return colors[i];})
        .attr("y", function(d) {
            return y(Math.max(0, -d.MemberCount));
        })
        .attr("height", function (d) {
            //return Math.abs(y(-d.MemberCount) - y(0))/2;
            return 0;
        })
        .attr("x", function(d) { return x(d.Status); })
        .attr("width", x.rangeBand())
        .transition()
        .duration(1000)
        .attr("height", function (d) {
            return Math.abs(y(-d.MemberCount) - y(0))/2;
        })

    svg.selectAll(".bars")
        .data(data)
        .enter().append("rect")
        //.attr("class", function(d) { return -d.MemberCount < 0 ? "bar negative" : "bar positive"; })
        .attr("class","bars positive")
        .attr("fill",function(d,i){return colors[i];})
        .attr("y", function(d) {
            //return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
            return y(Math.max(0, -d.MemberCount))
        })
        .attr("height", function (d) {
            return 0;
        })
        .attr("x", function(d) { return x(d.Status); })
        .attr("width", x.rangeBand())
        .transition()
        .duration(1000)
        .attr("y", function (d) {
            return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
        }).attr("height", function (d) {
            return Math.abs(y(-d.MemberCount) - y(0))/2;
        })*/

    var mainBars=svg.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        //.attr("class", function(d) { return -d.MemberCount < 0 ? "bar negative" : "bar positive"; })
    mainBars.attr("class","bar positive")
        .attr("fill",function(d,i){return colors[i];})
        .attr("y", function(d) {
            //return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
            return y(Math.max(0, -d.MemberCount));
        })
        .attr("height", function (d) {
            return 0;
        })
        .attr("x", function(d) { return x(d.Status); })
        .attr("width", x.rangeBand())
        .transition()
        .duration(1000)
        .attr("y", function (d) {
            return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
        }).attr("height", function (d) {
            return Math.abs(y(-d.MemberCount) - y(0));
        })

    mainBars.append("svg:title").text(function(d,i){
        return d.Status+" : "+Math.abs(d.MemberCount);
    })







    var height2 = 300 - margin.top - margin.bottom;

    var svg2 = d3.select("#"+content+"Content").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height2 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + (margin.left) + "," + margin.top + ")");


    arr=arr.reverse()
    var space=0;

    for(i=0;i<arr.length;i++){
       var key=arr[i];
        var container=svg2.append("g");
        var bars=container.selectAll(".rect1")
            .data(data)
            .enter()
            .append("svg:a")
            .attr("xlink:href", function(d,i){return "/programTracking/pt?metrics="+key.toLowerCase();})

        bars.append("rect").attr("y", function(d) {
                //return (height2-(i*17-10));
                return space;
            })
            .attr("class",function(d){
                if(d[key]>10)
                return "redfill"
                else if(d[key]>0)
                return "bluefill"
                else
                return "greenfill"

            })
            .attr("x", function(d) { return x(d.Status); })
            //.attr("width", x.rangeBand())
            .attr("height",0)
            .attr("width", x.rangeBand() )
            .transition()
            .delay((i+1)*70)
            .duration(300)
            .attr("height",16)

        bars.append("text")
            .attr("y", function(d) {
                return space+12;
            }).attr("x", function(d) { return x(d.Status)+40; })
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .text(function(d){
                if(d[key]<0)
                return  "("+d[key]+" %)"
                else
                return  "(+"+d[key]+" %)"
            })

        var keyElement=key;
        if(key=="Risk") key+=" (Prospective)"
        bars.append("svg:title")
            .text(function(d, i) { return key+" : " + d[keyElement]; })






        if(key=="Risk") key+=" (Prospective)"



        container.append("text")
            .attr("class", "text_y")
            .attr("text-anchor", "end")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(" + (20) + "," + ((space+12)) + ")")  // text is drawn off the screen top left, move down and out and rotate
            .text("")
            .transition()
            .duration(300)
            .delay((i+1)*90)
            .text(""+key+"");

        space=space+28;

        /*container.append("text")
            .attr("class", "text_y")
            .attr("text-anchor", "end")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate(" + (15) + "," + ((height2-(i*17)-2)) + ")")  // text is drawn off the screen top left, move down and out and rotate
            .transition()
            .duration(100)
            .delay(i*100)
            .text(""+key+"");*/

    }



   /* svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);*/

    var xAxis = d3.svg.axis()
        .scale(x)
        .tickSize(0)
        .orient("top");

    var  ax2=svg.append("g")
        .attr("class", "x axis funnel")
        .attr("transform", "translate(0,-10)")
        .call(xAxis)

    var mTicks = data.map(function (d) {
        return d.Status
    });

    var main_xAxis2 = d3.svg.axis()
        .scale(x)
        .tickSize(0)
        .tickValues(mTicks)
        .tickFormat(formatTicks)
        .orient("bottom");

    var ax1=svg.append("g")
        .attr("class", "x axis funnel")
        .attr("transform", "translate(0," + (y(0)) + ")")
        .call(main_xAxis2)
        .selectAll(".tick text")
        .style("pointer-events", "none")
        .call(wrap, x.rangeBand());

    function formatTicks(d) {
        var arr=$.grep(data, function(e){ return e.Status === d; })
        return arr[0].MemberCount
    }

   /* svg.append("g")
        .attr("class", "x axis")
        .append("line")
        .attr("y1", y(0))
        .attr("y2", y(0))
        .attr("x1", 0)
        .attr("x2", width);*/

    $(("#"+content)).prepend("<div style='text-align: center;font-size: 28px;' class='mb25'><a href='javascript:void(0)'>"+data[0].ProgramName+"</a></div>")

}

function wrap(text, width) {
    text.each(function() {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.1, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y-6).attr("dy", dy + "em");
        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", 0).attr("y", y-6).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
            }
        }
    });
}

function render4(da) {
    $("#graph4").html("");
    /*var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
     main_width = 500 - main_margin.left - main_margin.right,
     main_height =300 - main_margin.top - main_margin.bottom;*/

    var main_margin = {top: 20, right: 150, bottom: 50, left: 95},
        main_width = 850 - main_margin.left - main_margin.right,
        main_height = 350 - main_margin.top - main_margin.bottom;
    var pharmacyClaims = da;
    var memberMonths = da.value;
    var newVal;
    var data = [];
    var parseDate4 = d3.time.format("%Y-%m-%d").parse;
    //var colors = d3.scale.category10().range();
    //var colors =["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395", "#994499", "#22aa99", "#aaaa11", "#6633cc", "#e67300", "#8b0707", "#651067", "#329262", "#5574a6", "#3b3eac"];
    //var colors =["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395", "#994499", "#22aa99", "#aaaa11", "#6633cc", "#e67300", "#8b0707", "#651067", "#329262", "#5574a6", "#3b3eac",'#1f77b4','#ff7f0e','#ff7f0e','#2ca02c'];

    $.each(pharmacyClaims, function (key, val) {
        newVal = new Object();
        newVal.time1 =+val.projectedCostConcurrent;
        newVal.actual =+val.percentOfPopulation
        newVal.population =+val.population
        newVal.radius =+val.projectedCostProspective
        newVal.cost =+val.cost
        newVal.projectedCostActual =+val.projectedCostActual
        newVal.projectedCostConcurrentActual =+val.projectedCostConcurrentActual
        newVal.name =val.metricDisplay
        newVal.metricCode =val.metricCode
        if(key!="metaInfo")
        data.push(newVal);
    });



    data.sort(function(a, b) { return d3.descending(a.radius, b.radius); });

    data=data.slice(0,10)
    data.sort(function(a, b) { return d3.descending(a.radius, b.radius); });

//            var percent=Math.round(((data[1].actual-data[0].actual)/data[0].actual)*100)



    var main_x = d3.scale.linear()
        .range([0, main_width]);

    var actualDatas=data;
    actualDatas.sort(function(a, b) { return d3.ascending(a.time1, b.time1); });

    main_x.domain([0, actualDatas[actualDatas.length - 1].time1*1.2]);

    var main_radius=d3.scale.linear().range([6,24])

    var main_y = d3.scale.linear()
        .range([main_height, 0]);

    var main_xAxis = d3.svg.axis()
        .scale(main_x)
        .outerTickSize(0)
        .tickFormat(function(d,i){ return "k"; })
        .orient("bottom");

    /*var colorScale = d3.scale.quantize()
        .domain([0,data.length])
        .range(colors);*/



    var main_yAxisLeft = d3.svg.axis()
        .scale(main_y)
        .ticks(5)
        .tickSize(-main_width)
        //.tickFormat(function(d){return d.actual/1000+"k";})
        .outerTickSize(0)
        .orient("left");

    var main_line0 = d3.svg.line()
        .x(function (d) {
            return main_x(d.time1);
        })
        .y(function (d) {
            return main_y(d.population);
        });

    var svg = d3.select("#graph4").append("svg")
        .attr("width", main_width + main_margin.left + main_margin.right)
        .attr("height", main_height + main_margin.top + main_margin.bottom);

    svg.append("defs").append("clipPath")
        .attr("id", "clip")
        .append("rect")
        .attr("width", main_width)
        .attr("height", main_height);

    var main = svg.append("g")
        .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");

        var value1 = Math.round(data[data.length - 1].population);


        main_radius.domain(d3.extent(data, function (d) {
            return d.radius;
        }));

        var array_y0 = d3.extent(data, function (d) {
            return d.population;
        });
        main_y.domain([0,d3.max(array_y0)*1.2]);

        var main_xAxis2 = d3.svg.axis()
            .scale(main_x)
            .outerTickSize(0)
            .ticks(5)
            .tickFormat(function(d,i){ return "$"+(d/1000)+"K"; })
            .orient("bottom");

        main.append("g")
            .attr("class", "x axis xaxisLeft")
            .attr("transform", "translate(0,"+ (main_height) + ")")
            .call(main_xAxis2)
            .selectAll("path")
            .attr("stroke", "#ccc");


    main.append("g")
        .attr("class", "y axis yaxisLeft")
        //.attr("transform", "translate("+ (-20) + ",0)")
        .call(main_yAxisLeft)
        //.selectAll("line")
        //.attr("stroke", "#ccc")
        .selectAll("text")
        //.attr("y", 6)
        .attr("x", -35)
        //.style("text-anchor", "start");

    main.selectAll(".tick")
        .attr("stroke-dasharray", "3 3")
        .attr("opacity", ".5");

    main.append("text")
        .attr("class", "text_y")
        .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate(" + (-80) + "," + 157 + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
        .text("Population Volume");

    main.append("text")
        .attr("class", "text_y")
        .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate(" + 276 + "," + (main_height+38 ) + ")")  // text is drawn off the screen top left, move down and out and rotate
        .text("Actual Cost");



        main.selectAll("line")
            .attr("stroke", "#ccc");


    var div2 = d3.select(".contentWrapper").append("div")
        .attr("class", "tooltip3")
        .style("opacity", 0);

    var outerCircle=main.append("g").attr("class", "outerCircle");
    //var innerCircle=main.append("g").attr("class", "innerCircle");



   /* var bubble = d3.layout.pack()
        .sort(null)
        .size([main_width, main_height])
        .padding(1.5);*/


        main.append('g')
            .selectAll('circle')
            .data(data)
            .enter()
            .append("svg:a")
            .attr("xlink:href", function(d,i){return "index/"+ d.metricCode;})
            //.attr("onclick", "postRequestForGet(this);return false;")
            .append('circle')
            .attr("class", "y2")
            .attr("id",function(d,i){
                return "y2_"+d.metricCode;
            })
            .attr("fill",function(d,i){
                return colorScale[d.metricCode]
            })
            .attr("stroke",function (d) {
                var colorOpa=splitColors(colorScale[d.metricCode],true);
                return "rgba("+colorOpa[1]+", "+colorOpa[2]+", "+colorOpa[3]+", 1)"})
            .attr("stroke-width","1px")
            .attr("cx", function (d) {
                return main_x(d.time1);
            })
            .attr("cy", function (d) {
                return main_y(d.population);
            })
            .attr("r", function (d) {
                return 0;
            }).on("mouseenter", mouseIn).on("mouseout",mouseOut).transition()
            .duration(1000)
            .attr("r", function (d) {
                return main_radius(d.radius);
            })

    function mouseOut(d) {
        d3.select("#y2_"+d.metricCode).attr("class", "y2").attr("fill",colorScale[d.metricCode]);
        div2.transition()
            .duration(100)
            .style("opacity", 0);
        d3.select("g.outerCircle").remove();
        outerCircle=main.append("g").attr("class", "outerCircle");
        //d3.select("g.innerCircle").remove();
        //innerCircle=main.append("g").attr("class", "innerCircle");
    }

    function mouseIn(d,i) {
        //d3.select(this).attr("class", "y0").attr("r",main_radius(d.radius)+1)
        //var prefix = d3.formatPrefix(Math.round(d.radius));
        var color=splitColors(colorScale[d.metricCode]);
        var colorOpa=splitColors(colorScale[d.metricCode],true);
        d3.select("#y2_"+d.metricCode).attr("class", "y2-hover").attr("fill","rgba("+colorOpa[1]+", "+colorOpa[2]+", "+colorOpa[3]+", 1)");
        var p = $("#y2_"+d.metricCode);
        var position = p.offset();
        var left = position.left;
        var top = position.top;
        div2.transition()
            .duration(100)
            .style("opacity",1)
            //.style("color","rgba("+colorOpa[1]+", "+colorOpa[2]+", "+colorOpa[3]+", 1)")
            .style("color","black")
            .style("border","1px solid rgba("+colorOpa[1]+", "+colorOpa[2]+", "+colorOpa[3]+", 1)");

        var topPosition=245;

        var value=0

        var table="<table class='tooltip-table'>" +
            "<tr><td class='text-center formatOutput3' colspan='2'><strong>"+ d.name+"</strong></td></tr>" +
            "<tr><td class='text-rights'>Population Size (Members count)</td><td> "+d.population+"</td></tr>" +
            "<tr><td class='text-rights'>Population Size (% of Total)</td><td> "+d.actual.toFixed(1)+"</td></tr>";
            //if(Math.round(d.time1)>0){
            value=(d.time1>1)?"$"+(Math.round(d.time1)/1000).toFixed(2)+" K":"N/A"
                table+="<tr><td class='text-rights'>Disease Related Expected Paid</td><td>"+value+"</td></tr>"
                //table+="<tr><td class='text-rights'>Disease Related Actual Costs</td><td> $ 0 K</td></tr>";
            topPosition+=15;
            //}
            value=(d.radius>1)?"$"+(Math.round(d.radius)/1000).toFixed(2)+" K":"N/A"
            table+="<tr><td class='text-rights'>Disease Related Predicted Paid</td><td> "+value+"</td></tr>";

        value=(d.cost>1)?"$"+(Math.round(d.cost)/1000).toFixed(2)+" K":"N/A"
            table+="<tr><td class='text-rights'>Total Paid - Actual Current year</td><td> "+value+"</td></tr>";

        value=(d.projectedCostConcurrentActual>1)?" $"+(Math.round(d.projectedCostConcurrentActual)/1000).toFixed(2)+" K":"N/A"
        table+="<tr><td class='text-rights'>Total Paid - Expected Current year</td><td>"+value+"</td></tr>";

        value=(d.projectedCostActual>1)?" $"+(Math.round(d.projectedCostActual)/1000).toFixed(2)+" K":"N/A"
        table+="<tr><td class='text-rights'>Total Paid - Predicted for Next Year</td><td>"+value+"</td></tr>";
        table+="</table>";

        /*var table="<table>" +
            "<tr><td class='text-center formatOutput3' colspan='2'><strong>"+ d.name+"</strong></td></tr>" +
            "<tr><td class='text-rights'>Actual Cost</td><td> $"+(Math.round(d.time1)/1000).toFixed(2)+" K</td></tr>" +
            "<tr><td class='text-rights'>Population Size (Members count)</td><td> "+d.population+"</td></tr>" +
            "<tr><td class='text-rights'>Population Size (% of Total)</td><td> "+d.actual.toFixed(1)+"</td></tr>" +
            "<tr><td class='text-rights'>Predicted Cost</td><td> $"+(Math.round(d.radius)/1000).toFixed(2)+" K</td></tr>"+
            "<tr><td class='text-rights'>Member Cost</td><td> $"+(Math.round(d.cost)/1000).toFixed(2)+" K</td></tr>"+
            "<tr><td class='text-rights'>Member Projected Cost</td><td> $"+(Math.round(d.projectedCostActual)/1000).toFixed(2)+" K</td></tr>"+
            "</table>"*/

        div2.html(table)
            .style("left", (left-100) + "px")
            .style("top", (top-topPosition) + "px");

        /* div2.html("<span class='formatOutput3' ><strong>"+ d.name+"</strong></span><br>Actual Cost: $" + (Math.round(d.time1)/1000).toFixed(2)+" K<br>" +
         "Population Size (# Members): "+d.population+"<br>" +
         "Population Size (% Total): "+d.actual.toFixed(1)+"<br>" +
         "Predicted Cost: $"+(Math.round(d.radius)/1000).toFixed(2)+" K"  )
             .style("left", (left-60) + "px")
             .style("top", (top-85) + "px"); */

            //-main_radius(d.radius)*2-8

   /* .style("left", (d3.event.pageX + 8) + "px")
            .style("top", (d3.event.pageY - 30) + "px");*/

        /*innerCircle.append("svg:a")
            .attr("xlink:href", "populationRisk/index/"+ d.metricCode)
            .append("circle").attr({
            cx: main_x(d.time1),
            cy: main_y(d.population),
            r: main_radius(d.radius),
            fill: "rgba("+colorOpa[1]+", "+colorOpa[2]+", "+colorOpa[3]+", 1)",
            shapeRendering:"optimizeSpeed"
        });*/



        outerCircle.append("circle").attr({
            cx: main_x(d.time1),
            cy: main_y(d.population),
            r: main_radius(d.radius)+4,
            fill: "none",
            shapeRendering:"optimizeSpeed",
            stroke: "rgba("+colorOpa[1]+", "+colorOpa[2]+", "+colorOpa[3]+", 1)",
            strokeWidth:"2"
        });

    }


    var datass=data.sort(function(a, b) { return d3.ascending(a.name, b.name); });
    var legend = main.selectAll(".legend")
        .data(datass)
        .enter()
        .append("svg:a")
        .attr("xlink:href", function(d,i){return "index/"+ d.metricCode;})
        .append("g")
        .attr("class", "legend")
        .attr("width", 100)
        .attr("transform", function (d, i) {
            return "translate(20," + (i * 30) + ")";
        }).on("mouseenter", mouseIn).on("mouseout",mouseOut);

    legend.append("rect").attr("class", "legend")
        .attr("x", main_width)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", function (d) {
            var colorOpa=splitColors(colorScale[d.metricCode])
            return colorOpa
        })

    legend.append("text")
        .attr("x", main_width+25)
        .attr("y", 14)
        //.attr("dy", ".35em")
        .style("text-anchor", "start")
        .style("fill", function (d) {
            return "grey"
        })
        .attr("font-size", "11px")
        .text(function (d) {
            return d.name;
        });
    }

function render3(datas){

    $("#graph3").html("");
    $("#graph3-0").html("");
    $("#graph3-1").html("");
    $("#graph3-2").html("");
    $("#graph3-3").html("");

    div = d3.select(".contentWrapper").append("div")
        .attr("class", "tooltip")
        .attr("id", "tooltips")
        .style("opacity", 0);

    var total=datas.default.highRisk+datas.default.normalRisk+datas.default.averageRisk+datas.default.lowRisk;
    if(total==0) {
        $("#graph3").html('<div style="margin-top:44px;"><i class="fa fa-exclamation-triangle fa-4 "></i> Data Unavailable! </div>');
        return false;
    }

    var memberCount=datas.default.memberCount
    /*var high=datas.default.highRisk/memberCount,
        norm=datas.default.normalRisk/memberCount,
        low=datas.default.lowRisk/memberCount;

    var avg=1-high-norm-low;*/

    var high=datas.default.highRisk/memberCount,
        norm=datas.default.normalRisk/memberCount,
        low=datas.default.lowRisk/memberCount;

    var avg=1-high-norm-low;


    var highNumber=datas.default.highRisk,
        normNumber=datas.default.normalRisk,
        lowNumber=datas.default.lowRisk;

    var avgNumber=(memberCount-highNumber-normNumber-lowNumber);



    var parseDate4 = d3.time.format("%Y-%m-%d").parse;

    var toDate=parseDate4(datas.toDate),
        fromDate=parseDate4(datas.fromDate);

    var members_high_drill= datas.default.members_high_drill;
    var members_avg_drill=datas.default.members_avg_drill
    var members_norm_drill=datas.default.members_norm_drill
    var members_low_drill=datas.default.members_low_drill





    if(isNaN(high)) high=0;

    if(isNaN(avg)) avg=0;

    if(isNaN(norm)) norm=0;

    if(isNaN(low)) low=0;

    //if(high==low==avg==norm) return false;

    var oldData= [
        { type:"High Risk", value: high,count:highNumber,color:"rgba(236, 62, 64, .6)",colorHigh:"rgba(236, 62, 64, 1)",drill:members_high_drill },
        {  type:"Average Risk", value: avg,count:avgNumber,color:"rgba(255, 155, 43, .6)",colorHigh:"rgba(255, 155, 43, 1)",drill:members_avg_drill  },
        { type:"Normal Risk", value: norm,count:normNumber,color:"rgba(102, 102, 102, .6)",colorHigh:"rgba(102, 102, 102, 1)",drill:members_norm_drill  },
        {  type:"Low Risk", value: low,count:lowNumber,color:"rgba(1, 164, 109, .6)",colorHigh:"rgba(1, 164, 109, 1)",drill:members_low_drill  }
    ];

    /*var oldData= [
        { type:"High Risk", value: .30,color:"rgba(236, 62, 64, .6)",colorHigh:"rgba(236, 62, 64, 1)"},
        { type:"Average Risk", value:.45,color:"rgba(255, 155, 43, .6)",colorHigh:"rgba(255, 155, 43, 1)"},
        { type:"Normal Risk", value:.15,color:"rgba(102, 102, 102, .6)",colorHigh:"rgba(102, 102, 102, 1)"},
        { type:"Low Risk", value:.10,color:"rgba(1, 164, 109, .6)",colorHigh:"rgba(1, 164, 109, 1)"}
    ];*/

    oldData.sort(function(a, b) {
        return  b.value - a.value;
    });

    var daata=[];

    $.each(oldData,function(key,val) {
        var a=new Object();
        var count=key+1;
        a.index=count/10;
        a.type=val.type;
        a.value=val.value;
        a.count=val.count;
        a.colorCode=val.color;
        a.drill=val.drill;
        a.colorHigh=val.colorHigh;
        daata.push(a);
    });






  /*  var daata = [
        { index:.1,  type:"High Risk", value: (datas.default.highRisk/total) },

        { index:.2,  type:"Normal Risk", value:(datas.default.normalRisk/total) },

        { index:.3,  type:"Average Risk", value: (datas.default.averageRisk/total)  },
        { index:.4, type:"Low Risk", value:(datas.default.lowRisk/total)  }
    ];*/

    daata.sort(function(a, b) {
        return parseFloat(a.value) - parseFloat(b.value);
    });






   /* var daata=[
        {index: .1,  value: .4},
        {index: .2,  value: .3},
        {index: .3,  value: .2},
        {index: .4,  value: .1}
    ];*/


    var width = 360,
        height = 180,
        //radius = Math.min(width, height) ,
        radius = 200,
        spacing = .5;

   /* var color = d3.scale.linear()
        .range(["hsl(180,70%,70%)", "hsl(-180,70%,70%)"])
        .interpolate(interpolateHsl);*/

    var color3 =['red','#FF9B2B', '#ccc','#01A46D'];

    var arc = d3.svg.arc()
        .startAngle(function(d) { return ((Math.PI/2)-(d.value * .5 * Math.PI)); })
        .endAngle(Math.PI /2)
        .innerRadius(function(d) { if(d.index==.1){ return 0; }else{return  (d.index-.1 + spacing) * radius; }})
        .outerRadius(function(d) { return (d.index + spacing) * radius; });

    var arc1 = d3.svg.arc()
        .startAngle(function(d) { return ((Math.PI/2)-(d.value * .5 * Math.PI)); })
        .endAngle(Math.PI /2)
        .innerRadius(function(d) { if(d.index==.1){ return 2; }else{return  (d.index-.1 + spacing) * radius+2  }})
        //.innerRadius(function(d) {  return 0;})
        .outerRadius(function(d) { return (d.index + spacing) * radius +2; })


    var formatter = d3.format(".0%");
    var svg = d3.select("#graph3").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        //.attr("transform", "translate(" + (width/4)*1 + "," + (height/4)*3  + ")");
        .attr("transform", "translate(" + 10 + "," + 120 + ")");


    var field = svg.selectAll("g")
                .data(daata)
                .enter().append("g").attr("id",function(d,i){return i+"_arc";});


    field.append("svg:a")
        .attr("data-href", function(d,i){
            return createZ5Link(d.drill);
        })
        .attr("onclick", "postRequestForGet(this);return false;")
        .append("path");
    field.append("text");

    d3.transition().duration(0).each(tick);
    d3.select(self.frameElement).style("height", height + "px");

    function tick() {
        field = field
            .each(function(d) { this._value = d.value; })
            .data(daata)
            .each(function(d) { d.previousValue = this._value; })


        field.select("path")
            .transition()
            .ease("linear")
            //.duration(1500)
            .attr("id", function(d,i) { return "id_"+d.index; })
            .attrTween("d", arcTween)
            .style("opacity", function(d) { return 1; })
            .style("fill", function(d,i) { return d.colorCode })





        field.select("text")
            .attr("x", function(d) { return  (d.index-.1 + spacing) * radius;  })
            .attr("y",function(d) { return 0; })
            //.text(function(d) { return formatter(d.value ); })
            .text(function(d) { return formatter(d.value ); })
            .style("font-size","9px")
            .transition()
            .ease("linear")
            .attr("transform", function(d) {
                return "rotate(0)"
                    + "translate(0,0)"
                    + "rotate(0)"
            });

        field.select("path")
            .on("mouseover",mouseOver)
            .on("mouseout", mouseOut);

    }

    function arcTween(d,k) {
        var i = d3.interpolateNumber(d.previousValue, d.value);
        return function(t) {
            switch (d.type) {
                case("High Risk"):
                    //$("#graph3-0").html(formatter(d.value ) +" ("+ d.count +") high");
                    var link= $('<a>',{
                        text:formatter(d.value ) +" ("+ d.count +") high",
                        'data-href':createZ5Link(d.drill),
                        click:function(){postRequestForGet(link);return false;},
                        class:"color-red graph3Legends"
                    });
                    //link.attr("click",function(){postRequestForGet(link);return false;})

                    $("#graph3-0").html(link);
                    //var ids=d3.select("path #id_"+d.index);
                    break;
                case("Average Risk"):


                    //$("#graph3-1").html(formatter(d.value )+" ("+ d.count +") med");
                    var link= $('<a>',{
                        text:formatter(d.value ) +" ("+ d.count +") med",
                        'data-href':createZ5Link(d.drill),
                        click:function(){postRequestForGet(link);return false;},
                        class:"color-orange graph3Legends"

                    });
                    $("#graph3-1").html(link);
                    break;
                case("Normal Risk"):
                    //$("#graph3-2").html(formatter(d.value ) +" ("+ d.count +") norm");
                    var link= $('<a>',{
                        text:formatter(d.value ) +" ("+ d.count +") norm",
                        'data-href':createZ5Link(d.drill),
                        click:function(){postRequestForGet(link);return false;},
                        class:"color-gray graph3Legends"});
                    $("#graph3-2").html(link);
                    break;
                case("Low Risk"):
                    //$("#graph3-3").html(formatter(d.value )+" ("+ d.count +") low");
                    var link= $('<a>',{
                        text:formatter(d.value ) +" ("+ d.count +") low",
                        'data-href':createZ5Link(d.drill),
                        click:function(){postRequestForGet(link);return false;},
                        class:"color-green graph3Legends"

                    });
                    $("#graph3-3").html(link);
                    break;
            }
            d.value = i(t);
            return arc(d); };
    }

    function mouseOver(d,k) {

        //====================================================
        //====================================================
            d3.select(this).transition()
                .ease("linear")
                .attrTween("d",function(d){
                    var i = d3.interpolateNumber(d.previousValue, d.value);
                    return function(t) {
                        d.value = i(t);
                        return arc1(d); };
                })
                .attr("stroke","none")
                .style("fill", function(d,i) { return d.colorHigh; })

        /*svg.append("g:use")
           .attr("xlink:href", "#1abc")
         <use id="use" xlink:href="#c1" />   // this is used as z-index in svg element
           */

        switch (d.type) {
            case("High Risk"):
                $("#graph3-0").css("font-size","14px")
                break;
            case("Average Risk"):
                $("#graph3-1").css("font-size","14px")
                break;
            case("Normal Risk"):
                $("#graph3-2").css("font-size","14px")
                break;
            case("Low Risk"):
                $("#graph3-3").css("font-size","14px")
                break;
        }

        div.transition()
            .duration(100)
            .style("opacity",.6);
        var table="<div id='prRisk'><b>"+formatter(d.value )+"&nbsp;"+ d.type+"</b><br><br></div><span class='formatTime'>"+dFormat(fromDate)+"</span>-<span class='formatTime'>"+dFormat(toDate)+"</span>"

        /*    div .html("<span class='formatTime'>"+dFormat(da.date)+"</span><br><span class='formatOutput0'>"+formatOutputs(da)+" %High Risk</span><br><span class='formatOutput1'>"+formatOutputs(db)+" %Medium Risk</span><br><span class='formatOutput2'>"+formatOutputs(dc)+" %Low Risk</span>")
         .style("left", (d3.event.pageX +30) + "px")
         .style("top", (d3.event.pageY - 60) + "px");*/
/*
        div .html(table)
            .style("left", (d3.event.pageX -50) + "px")
            .style("top", (d3.event.pageY - 100) + "px");*/

        div .html(table)
            .style("left", (d3.event.pageX -50) + "px")
            .style("top", (d3.event.pageY - 225) + "px");


        $("#prRisk").css("color", d.colorHigh);

    }

    function mouseOut(d,k) {

        d3.select(this).transition()
            .ease("linear")
            .attrTween("d",function(d){
                var i = d3.interpolateNumber(d.previousValue, d.value);
                return function(t) {
                    d.value = i(t);
                    return arc(d); };
            })
            .attr("stroke","none")
            .style("fill", function(d,i) { return d.colorCode; })

        div.transition()
            .duration(100)
            .style("opacity",0);
        switch (d.type) {
            case("High Risk"):
                $("#graph3-0").css("font-size","12px")
                break;
            case("Average Risk"):
                $("#graph3-1").css("font-size","12px")
                break;
            case("Normal Risk"):
                $("#graph3-2").css("font-size","12px")
                break;
            case("Low Risk"):
                $("#graph3-3").css("font-size","12px")
                break;
        }

    }


// Avoid shortest-path interpolation.
    function interpolateHsl(a, b) {
        var i = d3.interpolateString(a, b);
        return function(t) {
            return d3.hsl(i(t));
        };
    }


}

function render3Pie(datas){

    $("#graph3").html("");
    $("#graph3-0").html("");
    $("#graph3-1").html("");
    $("#graph3-2").html("");
    $("#graph3-3").html("");

    var data = [
        { k: "High Risk", v: datas.default.highRisk },
        { k: "Normal Risk", v:datas.default.normalRisk },
        { k: "Average Risk", v: datas.default.averageRisk  },
        { k: "Low Risk", v: datas.default.lowRisk  }
    ];

    var width = 220 - margin.left - margin.right,
        height = 220 - margin.top - margin.bottom,
        radius = Math.min(width, height) / 2;

    var color3 =['red','#FF9B2B', '#ccc','#01A46D'];


    /* var color = d3.scale.ordinal()
         .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);*/

    var arc = d3.svg.arc()
        .outerRadius(radius - 10)
        .innerRadius(0);

    var labelArc = d3.svg.arc()
        .outerRadius(radius - 40)
        .innerRadius(radius - 40);

    var pie = d3.layout.pie()
        .sort(null)
        .value(function(d) { return d.v; });

    var svg = d3.select("#graph3").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");



        var g = svg.selectAll(".arc")
            .data(pie(data))
            .enter().append("g")
            .attr("class", "arc");

        g.append("path")
            .attr("d", arc)
            .style("fill", function(d,i) { return color3[i]; });

        g.append("text")
            .attr("transform", function(d) { return "translate(" + labelArc.centroid(d) + ")"; })
            .attr("dy", ".55em")
            .text(function(d) { return ""+ d.data.k; });


    function type(d) {
        d.v = +d.v;
        return d;
    }


    /*var dataset = [
        { label: 'Abulia', count: 10 },
        { label: 'Betelgeuse', count: 20 },
        { label: 'Cantaloupe', count: 30 },
        { label: 'Dijkstra', count: 40 }
    ];

    var width = 360;
    var height = 360;
    var radius = Math.min(width, height) / 2;

    var color =['#A60F2B', '#648C85', '#B3F2C9', '#528C18', '#C3F25C'];

    var svg = d3.select('#graph3')
        .append('svg')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr('transform', 'translate(' + (width / 2) +  ',' + (height / 2) + ')');

    var arc = d3.arc()
        .innerRadius(0)
        .outerRadius(radius);

    var pie = d3.pie()
        .value(function(d) { return d.count; })
        .sort(null);

    var path = svg.selectAll('path')
        .data(pie(dataset))
        .enter()
        .append('path')
        .attr('d', arc)
        .attr('fill', function(d, i) {
            return color[i];
        });*/

}




function render3Old(data) {
    //var series3 = 3, // number of series
    //    values3 = 12; // number of values
    /*var data3 = d3.range(series3).map(function () {
     return d3.range(values3).map(function () {
     return Math.random() * 100 | 0;
     });
     });
     console.log(data3);


     var totalValue = +d3.sum(data3, function (d) {
     return d3.sum(d);
     });*/
    var margin = {top: 10, right: 8, bottom: 10, left: 8},
        width = 220 - margin.left - margin.right,
        height = 80 - margin.top - margin.bottom;

    $("#graph3").html("");
    $("#graph3-0").html("");
    $("#graph3-1").html("");
    $("#graph3-2").html("");
    $("#graph3-3").html("");


    var parseDate3 = d3.time.format("%Y-%m").parse;
    var reporting = data;


    var highRiskVal,lowRiskVal,avgRiskVal,normRiskValue;
    var data3 = [];
    var high = [];
    var avg = [];
    var norm = [];
    var low = [];
    var dates = [];
    $.each(reporting,function(key,val) {
        var newDate=parseDate3(key);

        highRiskVal = new Object();
        highRiskVal.date=newDate;
        highRiskVal.risk=val.highRisk.toFixed(2);

        avgRiskVal = new Object();
        avgRiskVal.date = newDate;
        avgRiskVal.risk=(100-val.highRisk-val.normalRisk-val.lowRisk).toFixed(2);

        lowRiskVal = new Object();
        lowRiskVal.date = newDate;
        lowRiskVal.risk=val.lowRisk.toFixed(2);

        normRiskValue = new Object();
        normRiskValue.date = newDate;
        normRiskValue.risk=val.normalRisk.toFixed(2);

        high.push(highRiskVal);
        avg.push(avgRiskVal);
        norm.push(normRiskValue);
        low.push(lowRiskVal);
        dates.push(newDate);
    });
    data3.push(high.sort(custom_sort));
    data3.push(avg.sort(custom_sort));
    data3.push(norm.sort(custom_sort));
    data3.push(low.sort(custom_sort));

    var max = d3.max(data3, function(d) {
        return d3.max(d, function(di) {
            return +di.risk;
        })
    });

    var streamMakerUp = (max * .2);
    var streamMakerDown = (max * .4);
    /*var totalValue = +d3.sum(data3, function (d) {
     return d3.sum(d);
     });*/

    var x3 = d3.time.scale()
        .domain(d3.extent(dates))
        .range([0, width]);

    var y3 = d3.scale.ordinal()
        .domain(d3.range(4))
        .rangePoints([0, height], 1);

    var color3 = {'0': 'red', '1': '#FF9B2B', '2': '#ccc', '3': '#01A46D'};

    /*var color = d3.scale.ordinal()
     .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56"]);*/

    var area3 = d3.svg.area()
        .interpolate("basis")
        .x(function (d, i) {
            return x3(d.date);
        })
        .y0(function (d) {
            if($.isNumeric((d.risk /streamMakerUp)))
            return -(d.risk /streamMakerUp);
            else
            return 0
        })
        .y1(function (d) {
            if($.isNumeric(d.risk / streamMakerDown))
                return d.risk / streamMakerDown;
            else
            return 0
        });

    svg3 = d3.select("#graph3").append("svg")
        .attr("width", width)
        .attr("height", height);

    svg3.selectAll("path")
        .data(data3)
        .enter().append("path")
        .attr("transform", function (d, i) {
            //console.log(y3(i));
            return "translate(0," + y3(i) + ")";
        })
        .style("fill", "white")
        .transition()
        .duration(transitionDuration)
        .style("fill", function (d, i) {
            return color3[i];
        })
        .attr('d', area3)
        .each('end', function (dataMean, i) {
            var sum = +d3.mean(dataMean, function(d) { return +d.risk;});
            $("#graph3-" + i).html("");
            switch (i) {
                case(0):
                    //showText3("#graph3-" + i, "" + Math.round((sum )) + "% high", 0, textDuration);
                    $("#graph3-0").html(Math.round((sum )) + "% high");
                    break;
                case(1):
                    //showText3("#graph3-" + i, "" + Math.round((sum) ) + "% med", 0, textDuration);
                    $("#graph3-1").html(Math.round((sum )) + "% med");
                    break;
                case(2):
                    //showText3("#graph3-" + i, "" + Math.round((sum )) + "% low", 0, textDuration);

                    $("#graph3-2").html(Math.round((sum )) + "% normal");
                    break;
                case(3):
                    //showText3("#graph3-" + i, "" + Math.round((sum )) + "% low", 0, textDuration);

                    $("#graph3-3").html(Math.round((sum )) + "% low");
                    break;
            }
        });

    var dData1=data3[0];
    var dData2=data3[1];
    var dData3=data3[2];
    var dData4=data3[3];

    var bisectDate = d3.bisector(function(d) { return d.date; }).left;
    var formatOutputs = function(d) { return  d.risk+"%"; };

    /*var focus = svg3.append("g")
        .attr("class", "focus")
        .style("display", "block");

    focus.append("circle")
        .attr("class", "y0")
        .attr("r", 3);
    focus.append("circle")
        .attr("class", "y1")
        .attr("r", 3);
    focus.append("circle")
        .attr("class", "y2")
        .attr("r", 3);*/

    svg3.append("rect")
        .attr("class", "overlay")
        .attr("fill","none")
        .attr("pointer-events","all")
        .attr("width", width)
        .attr("height", height)
        .on("mouseout", function() {
            div.transition()
                .duration(500)
                .style("opacity", 0);
        })
        .on("mousemove", function() {
            var x0 = x3.invert(d3.mouse(this)[0]),
                ia = bisectDate(dData1, x0, 1),
                d0a = dData1[ia - 1],
                d1a = dData1[ia],
                da = x0 - d0a.date > d1a.date - x0 ? d1a : d0a,
                ib = bisectDate(dData2, x0, 1),
                d0b = dData2[ib - 1],
                d1b = dData2[ib],
                db = x0 - d0b.date > d1b.date - x0 ? d1b : d0b,
                ic = bisectDate(dData3, x0, 1),
                d0c = dData3[ic - 1],
                d1c = dData3[ic],
                dc = x0 - d0c.date > d1c.date - x0 ? d1c : d0c,
                id = bisectDate(dData4, x0, 1),
                d0d = dData4[id - 1],
                d1d = dData4[id],
                dd = x0 - d0d.date > d1d.date - x0 ? d1d : d0d;



            dFormat(db.date);

            //focus.select("circle.y0").attr("transform", "translate(" + x3(da.date) + "," + y3(formatOutputs(da)) + ")");
            //focus.select("circle.y1").attr("transform", "translate(" + x3(da.date) + "," + y3(formatOutputs(db)) + ")");
            //focus.select("circle.y2").attr("transform", "translate(" + x3(da.date) + "," + y3(formatOutputs(dc)) + ")");

            div.transition()
                .duration(100)
                .style("opacity",.9);
            var table="<table>" +
                "<tr class='formatOutput0'><td>"+formatOutputs(da)+"</td><td>&nbsp;High</td></tr>" +
                "<tr class='formatOutput4'><td>"+formatOutputs(db)+"</td><td>&nbsp;Medium</td></tr>" +
                "<tr class='formatOutput1'><td>"+formatOutputs(dc)+"</td><td>&nbsp;Normal</td></tr>" +
                "<tr class='formatOutput2'><td>"+formatOutputs(dd)+"</td><td>&nbsp;Low</td></tr>" +
                "<tr><td colspan='2'>&nbsp;</td></tr>" +
                "<tr><td colspan='2' class='formatTime'>"+dFormat(da.date)+"</td></tr>" +
                "</table>"

        /*    div .html("<span class='formatTime'>"+dFormat(da.date)+"</span><br><span class='formatOutput0'>"+formatOutputs(da)+" %High Risk</span><br><span class='formatOutput1'>"+formatOutputs(db)+" %Medium Risk</span><br><span class='formatOutput2'>"+formatOutputs(dc)+" %Low Risk</span>")
                .style("left", (d3.event.pageX +30) + "px")
                .style("top", (d3.event.pageY - 60) + "px");*/

            div .html(table)
                .style("left", (d3.event.pageX +30) + "px")
                .style("top", (d3.event.pageY - 200) + "px");


        })

}


function type(d) {
    d.date = parseDate(d.date);
    d.close = +d.close;
    return d;
}

function custom_sort(a, b) {
    return a.date - b.date;
}

function sort_dates(a, b) {
    return a - b;
}

var showText = function (target, message, index, interval) {
    if (index < message.length) {
        $(target).next("div").find("span").append(message[index++]);
        setTimeout(function () {
            showText(target, message, index, interval);
        }, interval);
    }
}

var showTextBench = function (target, message, index, interval) {
    if (index < message.length) {
        $(target).find("span").append(message[index++]);
        setTimeout(function () {
            showTextBench(target, message, index, interval);
        }, interval);
    }
}

var showText3 = function (target, message, index, interval) {
    if (index < message.length) {
        $(target).append(message[index++]);
        setTimeout(function () {
            showText3(target, message, index, interval);
        }, interval);
    }
}

/*var executeGraph = function executeGraph(path1,path2,path3) {
 //d3.tsv(path1, type, render);
 //d3.tsv(path2, type, render2);

 d3.json(path1,render);
 d3.json(path2,render2);
 d3.json(path3,render3);
 }*/

function render2(d,type){

    $("#graph2").html("");
    $("#graph2Legend1").find("span").html("");
    $("#graph2Legend2").find("span").html("");
    $("#tooltips").remove();

    div = d3.select(".contentWrapper").append("div")
        .attr("class", "tooltip")
        .attr("id", "tooltips")
        .style("opacity", 0);


    var parseDate4 = d3.time.format("%Y-%m").parse;
    var parseYear = d3.time.format("%Y").parse;
    var medicalClaims = d.medicalClaims;
    var memberMonths = d.memberMonths;
    var bm = d.benchmark;
    var newVal;
    var data = [];
    $.each(medicalClaims,function(key,val) {
        newVal = new Object();
        newVal.date=parseDate4(key);
        //newVal.close=(medicalClaims[key]/memberMonths[key])|| 0;
        newVal.close=$.isNumeric( (medicalClaims[key]/memberMonths[key]))?(medicalClaims[key]/memberMonths[key]):0;
        newVal.medicalClaims=(medicalClaims[key])|| 0;
        newVal.memberMonths=(memberMonths[key])|| 0;
        newVal.benchmark=Math.round(bm)|| 0;
        data.push(newVal);
    });

    data.sort(custom_sort);

    var lastData=data.slice(-12);
    var initialValue=new Object();
    initialValue.memberMonths=0;
    initialValue.medicalClaims=0;

    var pmpmValue=lastData.reduce(function(acc,obj){
            //mapping
            acc.memberMonths+=obj.memberMonths;
            acc.medicalClaims+=obj.medicalClaims;

        return acc;
    },initialValue);

    //var pmpmYearly=(initialValue.medicalClaims/initialValue.memberMonths)||0

    var pmpmYearly=$.isNumeric( (initialValue.medicalClaims/initialValue.memberMonths))?(initialValue.medicalClaims/initialValue.memberMonths):0;
    if(type=="Paid") {
        pmpmYearly=initialValue.medicalClaims;
    }


    var formatDate =d3.time.format("%d-%b-%y"),
        parseDate = formatDate.parse,
        bisectDate = d3.bisector(function(d) { return d.date; }).left,
    //format0 = function(d) { return formatDate(d.date) + " - $ " + d.close; },
    //format1 = function(d) { return formatDate(d.date) + " - $ " + d.benchmark; },
        formatOutput2 = function(d) { return "Time"+d.date +" "+type+""+" actual"; },
        formatOutput3 = function(d) { return d3.format("$,")(Math.round(d.medicalClaims)) +" Paid actual"; },
        formatOutput0 = function(d) { return "$"+Math.round(d.close) +" PMPM actual"; },
        formatOutput1 = function(d) { return "$"+Math.round(d.benchmark) +" PMPM benchmark"; };


    svg1 = d3.select("#graph2")
        .append("svg")
        .attr("width", width + margin.left + margin.right+120)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

    x.domain(d3.extent(data, function (d) {
        return d.date;
    }));

    var array_y0;
    if(type=="Paid") {
        array_y0 = d3.extent(data, function (d) {
            return d.medicalClaims;
        });
    }else{
        array_y0 = d3.extent(data, function (d) {
            return d.close;
        });

        var array_y1=d3.extent(data, function(d) { return d.benchmark; });
        array_y0=array_y0.concat(array_y1);
    }

    y.domain([d3.min(array_y0),d3.max(array_y0)]);

    var main_yAxisLeft = d3.svg.axis()
        .scale(y)
        .ticks(3)
        .tickSize(2)
        .outerTickSize(0)
        .orient("left");






   /* y.domain(d3.extent(data, function (d) {
        return d.close;
    }));*/
    //var value1=Math.round(data[data.length-1].close)
    var value1=d3.format("$,")(Math.round(pmpmYearly))
    var value2=Math.round(data[data.length-1].date)
    var value3=Math.round(data[data.length-1].benchmark)


    /*var median = svg1.append("line")
     .attr("x1", 0)
     .attr("y1", threshold)
     .attr("x2", width)
     .attr("y2", threshold)
     .attr("stroke-width", 1)
     .attr("stroke", "gray");


     svg1.append("linearGradient")
     .attr("id", "line-gradient")
     .attr("gradientUnits", "userSpaceOnUse")
     .attr("x1", 0).attr("y1", height)
     .attr("x2", 0).attr("y2", 0)
     .selectAll("stop")
     .data([
     {offset: "0%", color: "#939393"},
     {offset: "50%", color: "#939393"},
     {offset: "50%", color: "red"},
     {offset: "100%", color: "red"}
     ])
     .enter().append("stop")
     .attr("offset", function (d) {
     return d.offset;
     })
     .attr("stop-color", function (d) {
     return d.color;
     });*/




    if(type=="PMPM") {
        var path2b = svg1.append("path")
            .attr("d", valueline1a(data))
            .attr("class", "lineb")
            .attr('shape-rendering', ' crispEdges');

        var totalLength = path2b.node().getTotalLength();

        path2b
            .attr("stroke-dasharray", totalLength + " " + totalLength)
            .attr("stroke-dashoffset", totalLength)
            .transition()
            .duration(transitionDuration)
            .ease("linear")
            .attr("stroke-dashoffset", 0)
    }


    if(type=="PMPM") {
        path1 = svg1.append("path")
            .attr("class", "line")
            .attr('shape-rendering', 'crispEdges')
            .attr("d", valueline(data));
    }else{
        path1 = svg1.append("path")
            .attr("class", "line1")
            .attr('shape-rendering', 'crispEdges')
            .attr("d", valuelinePaid(data));
    }


    var totalLength = path1.node().getTotalLength();


    path1
        .attr("stroke-dasharray", totalLength + " " + totalLength)
        .attr("stroke-dashoffset", totalLength)
        .transition()
        .duration(transitionDuration)
        .ease("linear")
        .attr("stroke-dashoffset", 0)
        .each('end', function () {
            //showText("#graph2", "$" +value1 + "/PMPM", 0, textDuration);
            //showTextBench("#graph2Legend1", "$" +value1 + " PMPM actual", 0, textDuration);
            //showTextBench("#graph2Legend2", "$" +value3 + " PMPM expected", 0, textDuration);
            var aa=$("#graph2Legend1").find("span").html(value1 +" "+type+""+" actual");
            if(type=="PMPM") {
                $("#graph2Legend2").find("span").html("$" + value3 + " " + type + "" + " benchmark");
                aa.css("color","red");
            }else{
                aa.css("color","green");
            }



        });



    var focus = svg1.append("g")
        .attr("class", "focus")
        .style("display", "block");



    if(type=="PMPM") {
        var textLabel = svg1.append('g')
            .classed('labels-group', true);
        textLabel
            .append('text')
            .classed('actualLabel', true)
            .attr({
                'x': function () {
                    return x(value2) + nationalBenchmargin;
                },
                'y': function () {
                    return y(value3) + 3;
                }
            })
            .text(function (d, i) {
                return "National"
            })

        textLabel
            .append('text')
            .classed('actualLabel', true)
            .attr({
                'x': function () {
                    return x(value2) + nationalBenchmargin;
                },
                'y': function () {
                    return y(value3) + 15;
                }
            })
            .text(function (d, i) {
                return "Benchmark"
            })
    }




    /*focus.append("circle")
        .attr("class", "y02")
        .attr("r", 2);



    focus.append("circle")
        .attr("class", "y12")
        .attr("r", 2);*/



    svg1.append("rect")
        .attr("class", "overlay")
        .attr("fill","none")
        .attr("pointer-events","all")
        .attr("width", width)
        .attr("height", height)
        .on("mouseover", function() { focus.style("display", null); })
        .on("mouseout", function() {
            div.transition()
                .duration(500)
                .style("opacity", 0);
            focus.style("display", "none");
        })
        .on("mousemove", mousemove);

    svg1.append("g")
        .attr("class", "y axis yaxisLeft")
        .call(main_yAxisLeft)
        .selectAll("text")
        .attr("x", -5)

    var formatDatee =d3.time.format("%y");
    var dupVal=[];
    var abc= data.reduce(function(newVal,item){
        if(dupVal.indexOf(formatDatee(item.date))<0){
            dupVal.push(formatDatee(item.date))
            newVal.push(item.date)

        }

        return newVal;

    },[]);

    var main_xAxis2 = d3.svg.axis()
        .scale(x)
        //.ticks(d3.time.months, 12)//should display 1 year intervals
        .tickFormat(d3.time.format('%Y'))//%Y-for year boundaries, such as 2011
        .tickValues(abc)
        .outerTickSize(0)
        .orient("bottom");



    svg1.append("g")
        .attr("class", "x axis xaxisLeft")
        .attr("transform", "translate(0," + (height) + ")")
        .call(main_xAxis2)
        .selectAll("path")
        .attr("stroke", "#ccc");

    function mousemove() {
        var x0 = x.invert(d3.mouse(this)[0]),
            i = bisectDate(data, x0, 1),
            d0 = data[i - 1],
            d1 = data[i],
            d = x0 - d0.date > d1.date - x0 ? d1 : d0;
        //focus.select("circle.y02").attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");
        //focus.select("circle.y12").attr("transform", "translate(" + x(d.date) + "," + y(d.benchmark) + ")");
        //focus.select("text.y0").attr("transform", "translate(" + x(d.date) + "," + y(d.close) + ")");//.text(formatOutput0(d));

        //focus.select("text.y1").attr("transform", "translate(" + x(d.date) + "," + y(d.benchmark) + ")");//.text(formatOutput1(d));
        //focus.select(".x").attr("transform", "translate(" + x(d.date) + ","+y(d.benchmark>d.close?d.benchmark:d.close)+")").attr("y2",height-y(d.benchmark>d.close?d.benchmark:d.close));

        var addPaid="<span class='formatOutput2'>"+formatOutput3(d)+"</span>"
        var addBenchMark="<br>"+addPaid+"<br><span class='formatOutput1'>"+formatOutput1(d)+"</span>"


        div.transition()
            .duration(100)
            .style("opacity",.9);
        div .html("<span class='formatOutput0'>"+formatOutput0(d)+"</span>"+addBenchMark+"<br><br><span class='formatTime'>"+dFormat(d.date)+"</span>")
            .style("left", (d3.event.pageX +30) + "px")
            .style("top", (d3.event.pageY - 200) + "px");

        //focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.actual) + ")").attr("x2", main_width + main_x(d.time1));
        //focus.select(".y1").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.expected) + ")").attr("x2", main_width + main_x(d.time1));
        //focus.select(".y1").attr("transform", "translate(0, " + main_y(d.expected) + ")").attr("x1", main_x(d.time1));
    }


}

function render4Data(d){
    /*console.log(d);
     d3.select("#graph1").append("ul").selectAll("li")
     .data(d) // top level data-join
     .enter().append("li")
     .each(function() {
     var li = d3.select(this);

     li.append("p")
     .text(function(d) { return d.reporting; });

     li.append("ul").selectAll("li")
     .data(function(d) { return d.medicalClaims; }) // second level data-join
     .enter().append("li")
     .text(function(d) { return d + ": " + d; });
     });*/



    /* var parseDate4 = d3.time.format("%Y-%m").parse;
     d3.json(path3, function (error, data) {
     var medicalClaims = data.reporting.medicalClaims;
     var memberMonths = data.reporting.memberMonths;
     console.log(medicalClaims);

     $.each(medicalClaims,function(key,val) {
     key=parseDate4(key);
     val=+val;
     });
     console.log(medicalClaims);*/
    /*memberMonths.forEach(function(d,i) {
     d.date = parseDate4(d.date);
     d.close = +d.close;
     });*/






    /* x.domain(d3.extent(data, function (d) {
     return d.date;
     }).reverse());
     y.domain(d3.extent(data, function (d) {
     return d.close;
     }));*/


    /*
     svg1 = d3.select("#graph1")
     .append("svg")
     .attr("width", width + margin.left + margin.right)
     .attr("height", height + margin.top + margin.bottom)
     .append("g")
     .attr("transform",
     "translate(" + margin.left + "," + margin.top + ")");

     var median = svg1.append("line")
     .attr("x1", 0)
     .attr("y1", threshold)
     .attr("x2", width)
     .attr("y2", threshold)
     .attr("stroke-width", 1)
     .attr("stroke", "gray");


     svg1.append("linearGradient")
     .attr("id", "line-gradient")
     .attr("gradientUnits", "userSpaceOnUse")
     .attr("x1", 0).attr("y1", height)
     .attr("x2", 0).attr("y2", 0)
     .selectAll("stop")
     .data([
     {offset: "0%", color: "#939393"},
     {offset: "50%", color: "#939393"},
     {offset: "50%", color: "red"},
     {offset: "100%", color: "red"}
     ])
     .enter().append("stop")
     .attr("offset", function (d) {
     return d.offset;
     })
     .attr("stop-color", function (d) {
     return d.color;
     });


     path1 = svg1.append("path")
     .attr("class", "line")
     .attr("d", valueline(data));
     var totalLength = path1.node().getTotalLength();


     path1
     .attr("stroke-dasharray", totalLength + " " + totalLength)
     .attr("stroke-dashoffset", totalLength)
     .transition()
     .duration(transitionDuration)
     .ease("linear")
     .attr("stroke-dashoffset", 0)
     .each('end', function () {
     showText("#graph1", "$" + Math.round(+d3.mean(data, function (d) {
     return d.close;
     })) + "/PMPM", 0, textDuration);
     });*/


//});


}
