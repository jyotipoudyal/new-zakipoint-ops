function renderData(data,htmlDom) {


    /*var newVal;
     var data = [];
     $.each(datas,function(key,val) {
     newVal = new Object();
     newVal.percentOfAvoidableEr=val.percentOfAvoidableEr
     newVal.totalPaidAmountForEr=val.totalPaidAmountForEr
     newVal.totalPaidAmountForAvoidableEr=val.totalPaidAmountForAvoidableEr
     newVal.year=val.year
     data.push(newVal);
     });*/

    var margin = {top: 20, right: 10, bottom: 40, left: 20},
        width = 500 - margin.left - margin.right,
        height = 350 - margin.top - margin.bottom;

    var x = d3.scale.ordinal()
        .rangeRoundBands([0, 300],.4);

    var y = d3.scale.linear()
        .rangeRound([height, 0]);

    var color = d3.scale.ordinal()
        .range([ "#034f96","#b53322"]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format(".2s"));

    var svg = d3.select("#bargraph1").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + -25 + "," + margin.top + ")");

    color.domain(d3.keys(data[0]).filter(function(key) {
        return (key !== "year" && key !== "percentOfAvoidableEr");
    }));

    data.forEach(function(d) {
        var y0 = 0;
        d.ages = color.domain().map(function(name) {
//                    console.log(name)
            return {name: name, y0: y0, y1: y0 += +d[name]};
        });
        d.total = d.ages[d.ages.length - 1].y1;
    });

    data.sort(function(a, b) { return b.order - a.order; });

    x.domain(data.map(function(d) { return d.year; }));
    var yearValues=data.map(function(d) { return d.year; });
    y.domain([0, d3.max(data, function(d) { return d.total*1.2; })]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    var main_xAxis1 =d3.svg.axis()
        .scale(x)
        .tickValues([yearValues[yearValues.length - 1]])
        .tickSize(0)
//                    .tickFormat(d3.time.format("%b"))
        .tickFormat(function(d,i) { return dataset[i]; })
        .orient("bottom");

    svg.append("g")
        .attr("class", "x axis xaxisLeft2")
        .attr("transform", "translate(0," + (height+19) + ")")
        .call(main_xAxis1);


    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Population");

    var state = svg.selectAll(".state")
        .data(data)
        .enter().append("g")
        .attr("class", "g")
        .attr("transform", function(d) { return "translate(" + x(d.year) + ",0)"; });

    state.selectAll("rect")
        .data(function(d) { return d.ages; })
        .enter().append("rect")
        .attr("width", x.rangeBand())
        .attr("y", function(d) { return y(d.y1); })
        .attr("height", function(d) { return y(d.y0) - y(d.y1); })
//                    .style("fill", function(d) { return color(d.name); });
        /*   .style("fill",function(d) {
         return color(d.name);

         })*/
        .attr("class", function(d) {
            if(color(d.name)=="#034f96")
                return "dbar"
            else
                return "hbar"

        });

    var legend = svg.selectAll(".legend")
        .data(color.domain().slice())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + (i * 22-15) + ")"; });

    legend.append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill",function(d) {
            if(d=="totalPaidAmountForAvoidableEr")
                d="url(#pattern-stripe)"
            else
                d="#034f96"
            return d;
        });
//                    .style("fill", color);

    legend.append("text")
        .attr("x", width -165)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "start")
//                    .style("fill",color)
        .style("fill",function(d) {
            if(d=="totalPaidAmountForAvoidableEr")
                d="#b53322"
            else
                d="#034f96"
            return d;
        })
        .attr("font-weight", "bold")
        .text(function(d) {
            if(d=="totalPaidAmountForAvoidableEr")
                d="of total was avoidable"
            else
                d="Total ER spend"
            return d;
        });

    var textAppend=svg.selectAll(".text").data(data).enter()
        .append("text")
        .attr("class","text")
        .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 24 + ")"; })
        .text(function(d, i) {
            return Math.round(d.percentOfAvoidableEr,2)+"%"
        }).attr("font-family", "sans-serif")
        .attr("font-size", "15px")
        .attr("font-weight", "bold")
        .attr("fill", "#b53322");

    var textAppend2=svg.selectAll(".text1").data(data).enter()
        .append("text")
        .attr("class","text1")
        .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 0 + ")"; })
        .text(function(d, i){
            return d3.format("$,")(Math.round(d.totalPaidAmountForEr));/*Math.round(d.totalPaidAmountForEr)*/
        }).attr("font-family", "sans-serif")
        .attr("font-size", "15px")
        .attr("font-weight", "bold")
        .attr("fill", "#034f96");


};