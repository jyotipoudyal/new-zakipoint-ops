function getBarGraph(id,val,total){
    /*var svgContainer = d3.select('#bar_'+i).append("svg")
     .attr("width", 150)
     .attr("height", 45);

     //Draw the Rectangle
     var rectangle = svgContainer.append("rect")
     .attr("x", 10)
     .attr("y", 10)
     .attr("width", 40)
     .attr("height", 10);*/


    var data = [val];
    var width = 80;
    height = 8;

    var x = d3.scale.linear()
        .domain([0, total])
        .range([0, width]);

    var svg = d3.select("#"+id).append('svg')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'chart');

    svg.selectAll('.chart')
        .data(data)
        .enter().append('rect')
        .attr('class', 'bar')
        .attr('y', function(d, i) { return i * 20 })
        .attr('width', function(d) { return x(d); })
        .attr('height', 8);

}