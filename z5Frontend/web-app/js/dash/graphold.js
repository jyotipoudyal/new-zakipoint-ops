var outerWidth = 220;
      var outerHeight = 60;
      var margin2 = { left: 8, top: 8, right: 8, bottom: 8 };
      var xColumn = "timestamp";
      var yColumn = "temperature";

      var svg2 = d3.select("#graph2").append("svg")
        .attr("width",  outerWidth)
        .attr("height", outerHeight);
      var g2 = svg2.append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
      var path2 ;
	  
	 
      var innerWidth  = outerWidth  - margin2.left - margin2.right;
      var innerHeight = outerHeight - margin2.top  - margin2.bottom;

      var xScale = d3.time.scale().range([0, innerWidth]);
      var yScale = d3.scale.linear().range([innerHeight, 0]);

      /*var xAxis = d3.svg.axis().scale(xScale)
    .orient("bottom").ticks(5);*/
    var xAxis2 = d3.svg.axis().scale(xScale)
    .orient("bottom").ticks(0).tickSize(0);

var yAxis2 = d3.svg.axis().scale(yScale)
    .orient("left").ticks(5);

      var line = d3.svg.line()
        .x(function(d) { return xScale(d[xColumn]); })
        .y(function(d) { return yScale(d[yColumn]); });




function render2(data){
  xScale.domain( d3.extent(data, function (d){ return d[xColumn]; }));
  yScale.domain( d3.extent(data, function (d){ return d[yColumn]; }));
 // d3.select("#graph2Mean").html(Math.round(+d3.mean(data, function (d){ return d[yColumn]; })));

	var threshold = (innerHeight/2);

var median = g2.append("line")
                     .attr("x1", 0)
                     .attr("y1",threshold)
                     .attr("x2", outerWidth)
                     .attr("y2", threshold)
                     .attr("stroke-width", 1)
                     .attr("stroke", "gray");
		
	g2.append("linearGradient")				
        .attr("id", "line-gradient2")			
        .attr("gradientUnits", "userSpaceOnUse")	
        .attr("x1",0).attr("y1",innerHeight)			
        .attr("x2", 0).attr("y2",0)		
    .selectAll("stop")						
        .data([								
           {offset: "0%", color: "#939393"},   
            {offset: "50%", color: "#939393"},  
            {offset: "50%", color: "red"},    
            {offset: "100%", color: "red"}
        ])					
    .enter().append("stop")			
        .attr("offset", function(d) { return d.offset; })	
        .attr("stop-color", function(d) { return d.color; });
		
		path2 = g2.append("path");
        path2.attr("d", line(data))
				.attr("class", "lines")
				.attr('shape-rendering',' crispEdges')
				
				 var totalLength = path2.node().getTotalLength();
    path2
      .attr("stroke-dasharray", totalLength + " " + totalLength)
      .attr("stroke-dashoffset", totalLength)
      .transition()
        .duration(800)
        .ease("linear")
        .attr("stroke-dashoffset", 0)
        .each('end',function(){

          showText("#graph2",""+Math.round(+d3.mean(data, function (d){ return d[yColumn];}))+"/PMPM", 0, 50);   
         /* $("#graph2").next("div").html(
            "<span>$" + Math.round(+d3.mean(data, function (d){ return d[yColumn];})) + "/PMPM</span>"
            )*/
        });

      /* g2.append("g")
        .attr("class", "x axis")
        .attr("stroke", "steelblue")
        .attr("transform", "translate(0," + (innerHeight/2) + ")")
        .call(xAxis2);*/

    // Add the Y Axis
   /* g.append("g")
        .attr("class", "y axis")
        .call(yAxis);

*/
      }

      function type2(d){
        d.timestamp = new Date(d.timestamp);
        d.temperature = +d.temperature;
        return d;
      }

     // d3.csv("data.csv", type, render);