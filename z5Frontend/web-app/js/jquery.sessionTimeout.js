﻿/*jshint browser:true*/

//
// jquery.sessionTimeout.js
//
// After a set amount of time, a dialog is shown to the user with the option
// to either log out now, or stay connected. If log out now is selected,
// the page is redirected to a logout URL. If stay connected is selected,
// a keep-alive URL is requested through AJAX. If no options is selected
// after another set amount of time, the page is automatically redirected
// to a timeout URL.
//
//
// USAGE
//
//   1. Include jQuery
//   2. Include jQuery UI (for dialog)
//   3. Include jquery.sessionTimeout.js
//   4. Call $.sessionTimeout(); after document ready
//
//
// OPTIONS
//
//   message
//     Text shown to user in dialog after warning period.
//     Default: 'Your session is about to expire.'
//
//   keepAliveUrl
//     URL to call through AJAX to keep session alive. This resource should do something innocuous that would keep the session alive, which will depend on your server-side platform.
//     Default: '/keep-alive'
//
//   redirUrl
//     URL to take browser to if no action is take after warning period
//     Default: '/timed-out'
//
//   logoutUrl
//     URL to take browser to if user clicks "Log Out Now"
//     Default: '/log-out'
//
//   warnAfter
//     Time in milliseconds after page is opened until warning dialog is opened
//     Default: 900000 (15 minutes)
//
//   redirAfter
//     Time in milliseconds after page is opened until browser is redirected to redirUrl
//     Default: 1200000 (20 minutes)
//
(function( $ ){
    jQuery.sessionTimeout = function( options ) {
       /* var defaults = {
            message      : 'Your session is about to expire in 10 minutes. Do you want to refresh your session?',
            keepAliveUrl : '/hcphub/logout/ajaxKeepAlive',
            firstWarnAfter    : 1200000, // 20 minutes
            secondWarnAfter   : 300000, // 5 minutes
            thirdWarnAfter3   : 240000 // 4 minute

        };*/

        var defaults = {
            message      : 'Your session is about to expire in 10 minutes. Do you want to refresh your session?',
            keepAliveUrl : '/logout/ajaxKeepAlive',//'/hcphub/logout/ajaxKeepAlive'
            firstWarnAfter    : 1800000, // 20 minutes   1800000
            secondWarnAfter   : 300000,   // 5 minutes
            thirdWarnAfter3   : 240000,  // 4 minute
            forthWarnAfter    : 70000   // 70sec

        };

        // Extend user-set options over defaults
        var o = defaults,
            dialogTimer;

        if ( options ) { o = $.extend( defaults, options ); }



        function controlDialogTimer(action){
            switch(action) {
                case 'start':
                    //alert("sdsd");
                    // After first warning period, show dialog and start redirect timer
                    dialogTimer = setTimeout(function(){
                        //$("#sessionText").text("Your session is about to expire in 10 minutes. Do you want to refresh your session?");
                        $('#sessionBox').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        //controlDialogTimer('start2');
                    }, o.firstWarnAfter);
                    break;
                case 'start2':
                    // After second warning period, show dialog and start redirect timer
                    dialogTimer = setTimeout(function(){
                        $("#sessionText").text("Your session is about to expire in 5 minutes. Do you want to refresh your session?");
                        $('#sessionBox').modal('show');
                        controlDialogTimer('start3');
                    }, o.secondWarnAfter);
                    break;
                case 'start3':
                    // After third warning period, show dialog and start redirect timer
                    dialogTimer = setTimeout(function(){
                        $("#sessionText").text("Your session is about to expire in 1 minute. Do you want to refresh your session?");
                        $('#sessionBox').modal('show');
                        controlDialogTimer('start4');
                    }, o.thirdWarnAfter3);
                    break;
                case 'start4':
                    // After forth warning period, show dialog and start redirect timer
                    dialogTimer = setTimeout(function(){
                        //window.location.reload();
                        /*var str=window.location.pathname.split("/").join("ef<<f!30/!gsk<<//!@!err/2cphu>><<//co")
                        window.location.href= '/login/sessionExpired?p='+str*/
//                        window.location.href= o.deny
                    }, o.forthWarnAfter);
                    break;
                case 'stop':
                    clearTimeout(dialogTimer);
                    break;
            }
        }


        // Begin warning period
        controlDialogTimer('start');
    };
})( jQuery );