dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dbCreate = "update" // one of 'create', 'create-drop','update'
    properties {
        maxActive = 50
        maxIdle = 25
        minIdle = 5
        initialSize = 5
        maxWait = 10000
        maxAge = 10 * 60000
        removeAbandoned=true
        removeAbandonedTimeout=400
        logAbandoned=true
        minEvictableIdleTimeMillis=1800000
        timeBetweenEvictionRunsMillis=1800000
        numTestsPerEvictionRun=3
        testOnBorrow=true
        testWhileIdle=true
        testOnReturn=true
        validationQuery="SELECT 1"
    }
}

dataSource_usermanagement {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    dbCreate = "update" // one of 'create', 'create-drop','update'
    properties {
        maxActive = 50
        maxIdle = 25
        minIdle = 5
        initialSize = 5
        maxWait = 10000
        maxAge = 10 * 60000
        removeAbandoned=true
        removeAbandonedTimeout=400
        logAbandoned=true
        minEvictableIdleTimeMillis=1800000
        timeBetweenEvictionRunsMillis=1800000
        numTestsPerEvictionRun=3
        testOnBorrow=true
        testWhileIdle=true
        testOnReturn=true
        validationQuery="SELECT 1"
    }
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        /*dataSource {
            dbCreate = "update" // one of 'create', 'create-drop','update'
            url = "jdbc:mysql://localhost:3306/zakipoint"
            username = "root"
            password = "root"
        }
        dataSource_usermanagement{
            dbCreate = "update" // one of 'create', 'create-drop','update'
            url = "jdbc:mysql://localhost:3306/usermanagement?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
            username = "root"
            password = "root"
        }*/

        dataSource {
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dbCreate = "update"
            url = ""
            username = ""
            password = ""
        }
        dataSource_usermanagement{
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dbcreate = "update"
            url = ""
            username = ""
            password = ""
        }
    }

    qc {
        dataSource {
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dbCreate = "update"
            url = ""
            username = ""
            password = ""
        }
        dataSource_usermanagement{
            pooled = true
            driverClassName = "com.mysql.jdbc.Driver"
            dbcreate = "update"
            url = ""
            username = ""
            password = ""
        }
        /*dataSource {
            dbCreate = "update"
            pooled = true
            url = "jdbc:mysql://localhost:3306/zakipoint?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
            username = "casqc"
            password = "Dearwalk_123"
        }

        dataSource_usermanagement {
            dbCreate = "update"
            pooled = true
            url = "jdbc:mysql://localhost:3306/usermanagement?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
            username = "casqc"
            password = "Dearwalk_123"
        }*/
    }

    production {
        dataSource {
            dbCreate = "update"
            pooled = true
            url = "jdbc:mysql://54.172.59.146:3306/usermanagement?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
            username = "casqc"
            password = "Dearwalk_123"
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                maxWait = 10000
                maxAge = 10 * 60000
                removeAbandoned=true
                removeAbandonedTimeout=400
                logAbandoned=true
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }

        dataSource_usermanagement {
            dbCreate = "update"
            pooled = true
            url = "jdbc:mysql://54.172.59.146:3306/usermanagement?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
            username = "casqc"
            password = "Dearwalk_123"
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                maxWait = 10000
                maxAge = 10 * 60000
                removeAbandoned=true
                removeAbandonedTimeout=400
                logAbandoned=true
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }
}
