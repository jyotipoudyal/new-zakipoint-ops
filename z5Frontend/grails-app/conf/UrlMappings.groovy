import com.deerwalk.security.DwClient

class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller: "dashboard",action: "index")
		"/explorer"(controller: "programTracking",action: "explorer")
		"/explorerDemo"(controller: "programTracking",action: "explorers")
		"/trackDemo"(controller: "programTracking",action: "pTracking")
		"/gapInCare"(controller: "dashboard",action: "gapInCare")
		"/imaging"(controller: "dashboard",action: "imagingServices")
		"/programTrackingComparison"(controller: "programTracking",action: "cohortComparison")
		"/logAnalytics"(controller: "logAnalytics",action: "logsAnalytics")
		"/setClient"(controller: "dashboard",action: "setClient")
		"/checkUrl"(controller: "dashboard",action: "checkUrl")
		"/clientPanel"(controller: "clientPanel",action: "list")

		//"/*/uITracker/create"(controller: "UITracker",action:"create")
		//"/*/uITracker/abcd"(controller: "UITracker",action:"abcd")
		//"/*/uITracker/list"(controller: "UITracker",action:"list")



		"500"(view:'/error')
	}
}
