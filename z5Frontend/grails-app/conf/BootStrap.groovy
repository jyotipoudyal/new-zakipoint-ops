import com.deerwalk.security.Requestmap
class BootStrap {

    def helperService

    def init = { servletContext ->
        servletContext.setAttribute('USER_URL_LOGGING', true)
        servletContext.setAttribute('requestMap',Requestmap.findAll())
//        servletContext.setAttribute('csDate',"10-01-2010")
//        servletContext.setAttribute('ceDate',"09-30-2015")
//        servletContext.setAttribute('clientIdentity',"0000")


        println "{new Date()} = ${new Date()}"

        println "!!! Build Success !!!"
    }
    def destroy = {
    }
}
