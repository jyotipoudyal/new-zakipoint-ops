package z5

import com.deerwalk.z5.LogAnalytics
import grails.converters.JSON
import grails.plugins.springsecurity.SpringSecurityService
import groovy.time.TimeCategory
import groovy.time.TimeDuration
import org.springframework.web.multipart.MultipartHttpServletRequest

import javax.servlet.http.HttpServletRequest

class SessionFilters {

    def grailsApplication
    def helperService
    def springSecurityService
    def grailsLinkGenerator
    def z5UserDetailsService

    def filters = {
        all(controller:'*', action:'*') {
            before = {

                if (controllerName.equals("secure") && actionName.equals("receptor")) return true

                if (controllerName.equals("faq")) return true
                if (controllerName.equals("dashboard") && actionName.equals("brandLogo")) return true

                if (controllerName.equals('cas') || controllerName.equals('logout')) return true
//                if (controllerName.equals('clientPanel')) return true

                if(!session.group) session.group=[:]

                if(!session.er) session.er=[:]

                if(!session.hr) session.hr=[:]

                def requestHost = getRequestHost(request)

                def client= springSecurityService.principal.attributes?.get("client")

                if(!client.contains(requestHost)){
                    response.sendRedirect("${grailsLinkGenerator.link(controller:'logout',action:'accessDenied')}")
                    return false;
                }

                if(requestHost!=grailsApplication.config.grails.navigationUrl){

                    /*if(requestHost.equals("empb")) {
                        requestHost="maclellan"
                    };*/


                    session.clientname=requestHost
                    session.group=helperService.setClientUrl(requestHost)
                }else{
                    session.clientname=""
                    session.group=[:]
                        if(actionName!="setClient" && actionName!="viewForClientGroup" ){
                            redirect(controller: "dashboard",action: "setClient")
                            return false
                        }
                }

                if(!request.xhr){
                    def map=[:];
                    map.controllerId=controllerName
                    map.actionId=actionName?actionName:"index"
                    map.uri=request.forwardURI
                    map.client=requestHost
                    map.userId = springSecurityService.currentUser.username
//                    z5UserDetailsService.userTracking(map,params)

                    def doNotTraceAction=["clientLogo","logsAnalytics","setClient","checkUrl","clientPanel"]
                    def doNotTraceController=["clientPanel"]

                    if(doNotTraceAction.contains(map.actionId)) return true;
                    if(doNotTraceController.contains(map.controllerId)) return true;



                    map.parameters=  (params as JSON).toString()
                    map.timeStamp=new Date()
                    try{

                        def logdataOld=LogAnalytics.findAllByUserIdAndClient(map.userId,map.client,[max: 1, sort: "timeStamp", order: "desc", offset: 0])[0]
                        def flag=false;
                        if(logdataOld){
                            if(logdataOld?.controllerId!="logout"){
                                if(logdataOld?.uri==map.uri){
                                    flag= true;
                                }else{
                                    TimeDuration duration = TimeCategory.minus(map.timeStamp, logdataOld.timeStamp)
                                    logdataOld.duration= (duration.minutes?(duration.minutes+" minutes "):"")+duration?.seconds+" seconds"
                                    if(duration.toMilliseconds()>1800000){
                                        logdataOld.duration="30 minutes"
                                    }
                                    logdataOld.save(flush: true,failOnError: true);
                                }

                            }else{
                                logdataOld?.duration="--"
                                logdataOld.save(flush: true,failOnError: true);
                            }
                        }


                        if(flag) return true;

                        def logData=new LogAnalytics(map);
                        logData.save(flush: true,failOnError: true);

                    }catch (Exception e){
                        e.printStackTrace()
                    }

                }







                /*if(!session.requestHost){
                    if(!(controllerName=="dashboard" && actionName =="clients") && requestHost=="login" && !controllerName.equals('logout') ){
                        redirect(controller: "dashboard",action: "clients")
                        return
                    }
                }*/
                /*if(!session.client){
                    redirect(controller: "dashboard",action: "client")
                    return
                }*/

            }
            after = { Map model ->

                /*def lastFetchedParams=session.currentUrl.tokenize("?")[1]
                def map=[:]
                lastFetchedParams.tokenize("&").each{

                    def aa=it.tokenize("=")
                    map[aa[0]]=aa[1]

                }
                def reportingDates=helperService.getReportingDates(servletContext.getAt("ceDate"))
                map.reportingFrom=reportingDates.priorFrom
                map.reportingTo=reportingDates.currentTo
                servletContext.setAttribute('parameters',map)*/



            }
            afterView = { Exception e ->



            }
        }






    }

    def private getRequestHost(HttpServletRequest request) {
        String requestHost = request.getHeader('Host')
        return requestHost.toString().split("\\.")[0]
    }

    def private logAnalytics(HttpServletRequest request,session) {

    }
}
