grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
/*grails.server.port.https=8445
grails.tomcat.keystorePath="C:/certs/z5.jks"
grails.tomcat.keystorePassword="changeit"*/


//grails.server.host="z5dev.deerwalk.local"
//grails.tomcat.truststorePath="C:/certs/z5.jks"
//grails.tomcat.truststorePassword="changeit"
//grails.tomcat.clientAuth = "want"
/*grails.tomcat.keystorePath="/usr/java/jdk1.7.0_79/bin/zakipoint.jks"
grails.tomcat.keystorePassword = "dearwalk12"*/
//grails.tomcat.keyAlias="login.deerwalk.local"
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]


grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()

        mavenRepo "http://repo.spring.io/milestone/"
        mavenRepo(name:"deerplugin", root: "http://plugin.deerwalk.com/artifactory/simple/Local-Internal-Repository")
        mavenRepo "http://repo.grails.org/grails/plugins"

//        uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://repo.grails.org/grails/core"

        mavenRepo "https://repository.jboss.org/nexus/content/groups/public/"
        mavenRepo "http://jaspersoft.artifactoryonline.com/jaspersoft/third-party-ce-artifacts/"

    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        runtime 'mysql:mysql-connector-java:5.1.34'
        compile ("deerwalk:dw-security:1.2.2")
        runtime 'net.sourceforge.jexcelapi:jxl:2.6.12'
        compile 'com.itextpdf:itextpdf:5.4.3'
        compile 'com.itextpdf.tool:xmlworker:5.4.3'
        compile 'commons-beanutils:commons-beanutils:1.8.3'
        runtime 'org.springframework:spring-test:3.1.0.RELEASE'
//        compile "net.sf.ehcache:ehcache-core:2.4.6"

    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.1.6"

        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.4"

        build ":tomcat:$grailsVersion"

        runtime ":database-migration:1.3.2"
        compile ':cache:1.0.1'

        compile ":spring-security-cas:1.0.5"
        compile ":spring-security-core:1.2.7.3"
        compile ":joda-time:1.4"

        compile ":jqgrid:3.8.0.1"
        compile ":jquery-ui:1.8.24"
        compile ":mail:1.0.7"
        compile ":quartz:1.0-RC13"
        compile ('org.grails.plugins:export:1.6') {
            excludes 'itext'
        }


        compile ":jdbc-pool:7.0.47"
        compile ":ckeditor:3.6.6.1.0"
//        runtime ':jasper:1.11.0'
        compile ":remote-pagination:0.4.8"
        compile ":multi-select:0.2"
//        compile ":rendering:1.0.0"
        compile ('org.grails.plugins:rendering:1.0.0')

    }
}
