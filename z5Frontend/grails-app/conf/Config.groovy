import grails.util.Environment
import org.apache.log4j.EnhancedPatternLayout
import org.apache.log4j.Level
import grails.plugins.springsecurity.SecurityConfigType


grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [html: ['text/html', 'application/xhtml+xml'],
                     xml: ['text/xml', 'application/xml'],
                     text: 'text/plain',
                     js: 'text/javascript',
                     rss: 'application/rss+xml',
                     atom: 'application/atom+xml',
                     css: 'text/css',
                     csv: 'text/csv',
                     all: '*/*',
                     json: ['application/json','text/json'],
                     form: 'application/x-www-form-urlencoded',
                     multipartForm: 'multipart/form-data'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

grails.plugins.springsecurity.successHandler.DefaultSavedRequest = '/logout/index'
// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart = false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']
// enable query caching by default
grails.hibernate.cache.queries = true

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console
    appenders {
        console name: 'stdout',
                layout: new EnhancedPatternLayout(conversionPattern: "%d{yyyy-MM-dd HH:mm:ss,SSS} %-5p %c: %m%n %throwable{short}")

        rollingFile name: "stacktrace", datePattern: "'.'yy-MM-dd", append: true,
                file: "/var/log/tomcat/grails/springSecTest-stacktrace.log"

        rollingFile name: "errorLogger", datePattern: "'.'yy-MM-dd", append: true, threshold: Level.ERROR,
                file: "/var/log/tomcat/grails/springSecTest-error.log",
                layout: new EnhancedPatternLayout('%d{yyyy-MM-dd HH:mm:ss,SSS} %-5p %c: %m%n %throwable{0}%n')

    }
    root {
        info 'stdout'
        error 'errorLogger'
        additivity = true
    }

//    debug 'org.springframework.security'
//    debug 'org.jasig.cas'

    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'
}


// Added by the Spring Security Core plugin:
grails.plugins.springsecurity.active = true
grails.plugins.springsecurity.cas.active=true

grails.plugins.springsecurity.userLookup.userDomainClassName = 'com.deerwalk.security.User'
grails.plugins.springsecurity.userLookup.authorityJoinClassName = 'com.deerwalk.security.UserRole'
grails.plugins.springsecurity.authority.className = 'com.deerwalk.security.Role'
grails.plugins.springsecurity.requestMap.className = 'com.deerwalk.security.Requestmap'

grails.plugins.springsecurity.securityConfigType = SecurityConfigType.Requestmap

grails.plugins.springsecurity.cas.loginUri = '/login'
grails.plugins.springsecurity.cas.serviceUrl = '/j_spring_cas_security_check'

grails.plugin.databasemigration.reports.updateOntart = true
grails.plugin.databasemigration.reports.changelogFileName = "changelog.groovy"


grails {
    mail {
        host = "smtp.gmail.com"
        port = 465
        username = "z5@zakipoint.com"
        password = "iam@Dearwalk12"
        props = ["mail.smtp.auth":"true",
                 "mail.smtp.socketFactory.port":"465",
                 "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
                 "mail.smtp.socketFactory.fallback":"false"]
    }
}
//To make the fields nullable by default
grails.gorm.default.constraints = {
    '*'(nullable: true, blank: true)
}


environments {
    development {
//      grails.serverURL='http://login.deerwalk.local:9090/z5'
//      grails.plugins.springsecurity.logout.afterLogoutUrl = 'http://login.deerwalk.local:9090/z5'
        grails.plugins.springsecurity.cas.serverUrlPrefix = 'http://login.deerwalk.local:8080/cas'
        grails.plugins.springsecurity.logout.afterLogoutUrl = 'http://login.deerwalk.local:8080/cas/logout'
        grails.plugins.springsecurity.service.name = 'z5'
        grails.plugins.springsecurity.cas.proxyCallbackUrl = 'http://login.deerwalk.local:9090/z5/secure/receptor'
        grails.plugins.springsecurity.cas.proxyReceptorUrl = '/secure/receptor'
        grails.backEndURL="http://das.deerwalk.local:8181/DasTest/"
        grails.protocol="http"
        grails.navigationUrl="login"
        grails.bobUrl="zakipoint"
        grails.hostPath=".deerwalk.local:9090/z5"

        grails.config.locations = [
//              "file:/home/skshah/dwconfig/${appName}/trunk/${Environment.current.name}-config.groovy",
//              "file:/home/skshah/dwconfig/${appName}/trunk/${Environment.current.name}-datasource.properties"
                "file:/config/datasource.properties"

        ]


    }

    qc {
        grails.config.locations = [
                "file:/dwconfig/front/qc.groovy",
                "file:/dwconfig/front/qc.properties"
        ]
    }



    production {
        grails.config.locations = [
                "file:/dwconfig/front/prod-config.groovy",
                "file:/dwconfig/front/prod-datasource.properties"
        ]
    }
}






