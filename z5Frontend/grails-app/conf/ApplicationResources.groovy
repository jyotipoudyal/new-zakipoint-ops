modules = {
    application {
//        dependsOn 'jquery'

        resource url:'js/application.js',disposition: "head"
        resource url:'js/jquery.min.js',disposition: "head"
        resource url:'js/angular.js',disposition: "head"
        resource url:'js/jquery.validate.js',disposition: "head"
        resource url:'js/d3.min.js',disposition: "head"
        resource url:'js/bootstrap.min.js',disposition: "head"
        resource url:'js/bootstrap-multiselect.js',disposition: "head"
        resource url:'js/autocomplete/jquery.tokeninput.js',disposition: "head"
//        resource url:'js/bootstrap-slider.js',disposition: "head"
    }

    //d3 graphs
    graph{
        resource url:'js/dash/linegraph.js',disposition: "head"
    }



    css{
        resource url: 'css/normalize.css',disposition: "head"
        resource url: 'font-awesome/css/font-awesome.min.css',disposition: "head"
        resource url: 'bootstrap/css/bootstrap.min.css',disposition: "head"
        resource url: 'css/layout.css',disposition: "head"
        resource url: 'css/graph.css',disposition: "head"
        resource url: 'css/bootsrap-multiselect.css',disposition: "head"
        resource url: 'js/autocomplete/token-input.css',disposition: "head"
    }

    pdf{
        resource url: 'css/layout.css',disposition: "head"
    }
}