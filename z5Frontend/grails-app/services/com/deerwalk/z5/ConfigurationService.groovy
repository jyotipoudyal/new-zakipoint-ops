package com.deerwalk.z5

import com.deerwalk.security.DwClient
import com.deerwalk.security.DwLevelOfAggregation
import com.deerwalk.security.DwClientGroup
import com.deerwalk.security.User
import com.deerwalk.security.UserDwClient
import com.deerwalk.security.UserPassword
import com.deerwalk.security.*
import grails.converters.JSON
import groovy.sql.Sql
import groovy.time.TimeCategory
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.springframework.web.context.request.RequestContextHolder

class ConfigurationService {
	static transactional = false

	def grailsApplication
	def helperService
	def fetchService
	def frontEndService
	def loaService
	def sessionFactory
	def servletContext = ServletContextHolder.servletContext
	def dataSource
	def fieldService

	def deleteAndFetchClientConfig(clientId, updateUM = false) {
		def client = Client.findByClientId(clientId)
		if(!client){
			client=new Client(clientId: clientId,name: "zakipoint",domain: "zakipoint.zakipointhealth.com")
			client.save(flush: true,failOnError: true);
		}


		def containsData = true
		String message = "Client Configured <br>"
		def page = 1
		while (containsData) {
			def parameters = "table=ClientConfig&clientId=${client.clientId}&page=${page}&pageSize=5000"
			def data = fetchService.unCachedFetchData(parameters, "BusinessLevel")
			def jsonData
			def clientConfigJson
			def resultSet

			if (!data) {
				return "No data received for specified parameters so, task couldn't be completed!";
			}
			try {
				jsonData = JSON.parse(data?.toString())
				message += "JSON Length: ${data?.toString()?.length()} \n"
				clientConfigJson = jsonData.getAt('clientConfig')
				resultSet = jsonData.getAt('businessLevels')
			} catch (e) {
				return "Invalid Json. Fail to parse JSON";
			}
			if (!resultSet) {
				containsData = false
				break;
			}
			if (updateUM) {
				message += configUMClient(client, resultSet, clientConfigJson, jsonData)
			} else {
				message += configClient(client, resultSet, clientConfigJson, jsonData, page)
			}
			page++
		}
		return message;
	}

	private def configUMClient(Client client,resultSet,clientConfigJson, jsonData){
		def message=''
		if (!resultSet || !clientConfigJson){
			return "Data not found."
		}
		def clientConfig=clientConfigJson.getAt('0');
		def clientCycleStartDate=clientConfig.getAt('cycleStartDate')
		def clientCycleEndDate=clientConfig.getAt('cycleEndDate')
		//clientConfig = {lvl5_display:"Member Enrollment",lvl2_display:"Carrier",lvl4_desc:"divisionName",lvl4_display:"Unit",cycleEndDate:"2013-05-31",lvl2:"carrierId",lvl3:"groupId",lvl1_desc:"planTypeDesc",lvl4:"divisionId",lvl5:"udf6_eligibility",lvl2_desc:"carrierName",lvl5_desc:"udf6_eligibility",lvl3_desc:"groupName",lvl1:"planTypeId",lvl3_display:"Group",lvl1_display:"Plan",cycleStartDate:"2008-06-01",clientType:"Commercial"}
		//key = groupId
		def cycleDateData=getGroupWiseCycleDate(clientConfig,jsonData);

		def dataForClientParam = frontEndService.getDataForLOA(clientConfigJson)

//		println "dataForClientParam = $dataForClientParam"
		def dataMap = frontEndService.getBusinessLevel(resultSet,clientConfig,cycleDateData)
//		def datForBusinessRelation = dataMap.getAt('businessRelation')
		def datForBusinessLevel = dataMap.getAt('businessLevel')

//		println "datForBusinessLevel = $datForBusinessLevel"
		//dropAllClientParams
		/*def clientParams = ClientParams.findAllByClient(client)
		clientParams.each {
		    if(it.name!='DYNAMIC-REPORT-LEVEL'){
			  it.delete(flush: true)
		    }
		}*/
		println "-----client---"

		DwClient dwClient=frontEndService.findDwClientByDomain(client?.domain);
		println "dwClient = $dwClient"
		message += "**********************<br>"
		//message+=loaConfiguration(client,dataForClientParam)
		message += "Deleting old DwLevelOfAggregation.. <br>"

		int count=DwLevelOfAggregation.executeUpdate("delete from DwLevelOfAggregation where dwClient=:dwClient",[dwClient:dwClient])
		message += "Deleted: ${count} <br>"
		message += "**********************<br>"
		message+=updateConfiguration(null,dataForClientParam,DwLevelOfAggregation.class.getSimpleName(),dwClient,DwLevelOfAggregation.getPackage().getName())

		println "package name = "+DwLevelOfAggregation.getPackage().getName()
		println "simplename = "+DwLevelOfAggregation.class.getSimpleName()

		message += "**********************<br>"
		//dropAllBusinessLevels
		//message+=businessLevelEntryConfiguration(client,datForBusinessLevel)

		message+=updateConfiguration(null,datForBusinessLevel,DwClientGroup.class.getSimpleName(),dwClient,DwLevelOfAggregation.getPackage().getName())
		message += "***************END****************"
		return message;

	}


	private def configClient(Client client,resultSet,clientConfigJson, jsonData,page){
		def message=''
		if (!resultSet || !clientConfigJson){
			return "Data not found."
		}
		def clientConfig=clientConfigJson.getAt('0');
		def clientCycleStartDate=clientConfig.getAt('cycleStartDate')
		def clientCycleEndDate=clientConfig.getAt('cycleEndDate')
		def cycleDateData=getGroupWiseCycleDate(clientConfig,jsonData);

		def dataMap = frontEndService.getBusinessLevel(resultSet,clientConfig,cycleDateData)
		def datForBusinessRelation = dataMap.getAt('businessRelation')
		def datForBusinessLevel = dataMap.getAt('businessLevel')

		if(page==1){
			//dropAllBusinessLevels
			message += "**********************<br>"

			def oldLoas = LevelOfAggregation.findAllByClient(client)
			def dataForClientParam = frontEndService.getDataForLOA(clientConfigJson,oldLoas)
			message += "**********************<br>"
			message += updateConfiguration(client,dataForClientParam,'LevelOfAggregation')

			boolean cycleDateForClient=false;
			if(clientCycleStartDate){
				Date date=helperService.getFormatedDateTime(clientCycleStartDate,"yyyy-MM-dd")
				if(date){
					cycleDateForClient=true
					client.cycleStartDate=date
				}
			}
			if(clientCycleEndDate){
				Date date=helperService.getFormatedDateTime(clientCycleEndDate,"yyyy-MM-dd")
				if(date){
					cycleDateForClient=true
					client.cycleEndDate=date
				}
			}
			try{
				if(cycleDateForClient){
					message += "**********************<br>"
					if(client.save(flush:true)){
						message += "Updated: client cycle date success.<br>"
					}
				}
			}catch(e){
				message+="Fail to update client cycle date.<br>"
			}
		}
		message += updateConfiguration(client,datForBusinessLevel,'BusinessLevelEntry',null,'com.deerwalk.z5',page)
		//dropAllBusinessRelation
		message += "**********************<br>"
		message += updateConfiguration(client,datForBusinessRelation,'BusinessRelation',null,'com.deerwalk.z5',page)
		message += "***************END****************"
		return message;

	}

	private def getGroupWiseCycleDate(clientConfig,jsonArray){
		def cycleDates=[:]
		for(i in 1..15){
			def key=clientConfig.getAt('lvl'+i)?:null;
			if(key){
				cycleDates['lvl'+i]=[data:jsonArray.getAt(key),key:key];
			}
		}
		def cycleDateData=[:]
		cycleDates.each{key,val ->
			if(val && val['data']){
				val['data'].each{keys,value ->
					def tempKey=value[val['key']]+"_"+key;
					cycleDateData[tempKey]=[cycleEndDate:value['cycleEndDate'],cycleStartDate:value['cycleStartDate']]
				}
			}
		}
		return cycleDateData
	}


	private def updateConfiguration(client,data,classParam,DwClient dwClient=null,defaultPackage='com.deerwalk.z5',page=1){
		try{
			String className = defaultPackage+'.'+classParam;
			Class clazz=new GroovyClassLoader().loadClass(className)
//			Class clazz = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz
//			Class clazz = grailsApplication.getDomainClass(className).clazz
			def message=''
			if(client && page==1){
				message += "Deleting old "+classParam+".. <br>"

				int count=clazz.executeUpdate("delete from "+classParam+" where client=:client",[client:client])
				message += "Deleted: ${count} <br>"
				message += "**********************<br>"
			}
			message += "Updating "+classParam+"... <br>"
			def status = frontEndService.updateDB(data,classParam,client,dwClient)
			message += "Updated: Success: ${status['updated']} || Failed: ${status['failed']} || Existed: ${status['existed']}<br>"
			return message;
		}catch(e){
			e.printStackTrace()
			log.error(e.getMessage());
			return "Fail to update $classParam";
		}
	}





}
