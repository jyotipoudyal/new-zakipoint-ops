package com.deerwalk.z5


import org.apache.commons.configuration.Configuration
import org.apache.commons.configuration.PropertiesConfiguration
import org.codehaus.groovy.grails.web.context.ServletContextHolder

class DisplayService {
    def servletContext
    def grailsAttributes

    def newService(){
        return "hello"
    }

    def showContent(key) {
        /*Properties props = new Properties("grails-app/i18n/display.properties")
        File propsFile = new File();
        props.load(propsFile.newDataInputStream())
        return props.getProperty(key)*/
//        println servletContext.getRealPath('/WEB-INF')
//        println servletContext.getResource('/WEB-INF/grails-app/i18n/display.properties').text
//        def servletContext = ServletContextHolder.servletContext.getRealPath("/") + "/WEB-INF/display.properties"
        def servletContext = "/config/display.properties"
//        "file:/home/skshah/dwconfig/${appName}/trunk/${Environment.current.name}-config.groovy"
//        Properties config = new Properties()
//        config.load(servletContext.getResourceAsStream("/WEB-INF/display.properties"))
        Configuration config = new PropertiesConfiguration(servletContext.toString());
        def value=config.getProperty(key);
        if(value.getClass().toString().contains("ArrayList")){
            value=value.join(", ")
        }
        return value
//        config.save()
    }

    def insertContent(key,value,url){
        /*Properties config = new Properties()
        config.load(servletContext.getResourceAsStream("/WEB-INF/display.properties"))*/
//        def servletContext = ServletContextHolder.servletContext.getRealPath("/") + "/WEB-INF/display.properties"
        def servletContext = "/config/display.properties"
        Configuration config = new PropertiesConfiguration(servletContext.toString());
        String prevContent=config.getProperty(key)
        config.setProperty(key, value);
        config.save();
        def trackChanges=new UITracker(name: key,content: value,url:url,prevContent:prevContent)
        trackChanges.save(flush: true,failOnError: true);
        println("Display Property Successfully Updated..");

        //alt

        /*Configuration config = new PropertiesConfiguration("grails-app/i18n/display.properties");
        List<String> list = new ArrayList<>();
        Iterator<String> keys = config.getKeys();
        while(keys.hasNext()){
            String key = (String) keys.next();
            list.add(key);
        }
        println "list = $list"*/


       /* Properties props = new Properties()
        File propsFile = new File("grails-app/i18n/display.properties");
        props.load(propsFile.newDataInputStream())
        props.setProperty(key,value)
        props.store(propsFile.newWriter(), null)
        props.load(propsFile.newDataInputStream())*/

    }


}
