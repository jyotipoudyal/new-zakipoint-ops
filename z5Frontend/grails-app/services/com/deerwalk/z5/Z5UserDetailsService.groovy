package com.deerwalk.z5

import grails.converters.JSON
import groovy.time.TimeCategory
import groovy.time.TimeDuration

class Z5UserDetailsService  {


     def serviceMethod(){
     }

     def userTracking(map,params){

          def doNotTraceAction=["clientLogo","logsAnalytics","setClient","checkUrl","clientPanel"]
          def doNotTraceController=["clientPanel"]

          if(doNotTraceAction.contains(map.actionId)) return true;
          if(doNotTraceController.contains(map.controllerId)) return true;

          map.parameters=  (params as JSON).toString()
          map.timeStamp=new Date()
          try{

               def logdataOld=LogAnalytics.findAllByUserIdAndClient(map.userId,map.client,[max: 1, sort: "timeStamp", order: "desc", offset: 0])[0]
               def flag=false;
               if(logdataOld){
                    if(logdataOld?.controllerId!="logout"){
                         if(logdataOld?.uri==map.uri){
                              flag= true;
                         }else{
                              TimeDuration duration = TimeCategory.minus(map.timeStamp, logdataOld.timeStamp)
                              logdataOld.duration= (duration.minutes?(duration.minutes+" minutes "):"")+duration?.seconds+" seconds"
                              if(duration.toMilliseconds()>1800000){
                                   logdataOld.duration="30 minutes"
                              }
                              logdataOld.save(flush: true,failOnError: true);
                         }

                    }else{
                         logdataOld?.duration="--"
                         logdataOld.save(flush: true,failOnError: true);
                    }
               }


               if(flag) return true;

               def logData=new LogAnalytics(map);
               logData.save(flush: true,failOnError: true);

          }catch (Exception e){
               e.printStackTrace()
          }

     }



}
