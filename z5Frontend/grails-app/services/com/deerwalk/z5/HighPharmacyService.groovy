package com.deerwalk.z5

import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class HighPharmacyService {

    def fetchService
    def exportService
    def helperService
    def searchService

    Map setDynamicReportParams(params,session,dates) {
        LinkedHashMap dynamicReportParams = [:]
        dynamicReportParams['reportingFrom'] =params.reportingFrom
        dynamicReportParams['reportingTo'] = params.reportingTo
        dynamicReportParams['basis'] =params.reportingBasis
        dynamicReportParams['phiCSDate'] = params.csDate
        dynamicReportParams['phiCEDate'] =params.ceDate
        dynamicReportParams['udf17Id'] =params.regional
        dynamicReportParams['page'] =params.page
        dynamicReportParams['pageSize'] =params.pageSize
//        dynamicReportParams['clientId'] = params.clientId
        dynamicReportParams['clientId'] = helperService.fetchClientId()
        dynamicReportParams['eligibilityType'] = ['medical','vision','dental']
        dynamicReportParams['reportingPaidThrough'] = "2014-09-30"
        dynamicReportParams['order'] = params.order
        dynamicReportParams['brandName'] = params.brandName
        dynamicReportParams['exclude'] = params.exclude
        dynamicReportParams['range'] = params?.range
        dynamicReportParams['isExclude'] = params?.isExclude
        dynamicReportParams['groups']= searchService.getGroups()
        return dynamicReportParams
    }

    def getHighPharmacyTable1(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        try{
            json = fetchTableData(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);
            rawJSON=parsedJSON.getAt('reporting');
        }catch(e){
            log.error(e)
            log.error('No Data received')
            return '';
        }
        return rawJSON
    }


    private def fetchTableData(report, params, module='highPharmacy') {
        module="";
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def parameters

        if (params && params['reportingTo'] && params['reportingFrom'] && params['basis'] && params['clientId']) {
            def loaParams = params["loaParams"]
            /*if(session.user.userAccess){
                loaParams = helperService.checkSelectedLoaInUserAccess(loaParams, session.user.userAccess);
            }*/
            if(report.equals('provider:erVisit') || report.equals('provider:snapshot') || report.equals('provider:thirtyDayReadmit')){
                module = 'provider'
                parameters = "clientId=${params['clientId']}${fetchService.getLoaRequestParameterSectionForProviderDashBoard(loaParams)}"
            }else{
                parameters = "clientId=${params['clientId']}"
            }
            if(params["switchedUserId"]){
                parameters += "&userId="+params["switchedUserId"]
            }
            parameters = parameters + "&reportingBasis=${params['basis']}&reportingTo=${params['reportingTo']}&reportingFrom=${params['reportingFrom']}&report=${report}"
            parameters = parameters + "&eligibilityType=${params.getAt('eligibilityType')}"
            if (params['basis'] == 'ServiceDate') {
                parameters = parameters + "&reportingPaidThrough=${params['reportingPaidThrough']}"
            }
            if (params['participation']) {
                parameters = parameters + "&isParticipation=${params['participation']}"
                if (params && params['program_name']) {
                    parameters = parameters + "&program_name=${params['program_name']}"
                }
                if (params && params['program_type']) {
                    parameters = parameters + "&program_type=${params['program_type']}"
                }
            }

            if(params['dataView']){
                parameters = parameters + "&dataView=${params['dataView']}"
            }

            if(params['phiCSDate']){
                parameters = parameters + "&phiCSDate=${params['phiCSDate']}"
            }

            if(params['phiCEDate']){
                parameters = parameters + "&phiCEDate=${params['phiCEDate']}"
            }

            if(params && params["trendingField"]){
                parameters = parameters + "&trendingField=${params['trendingField']}"
            }
            if(params['udf17Id']){
                parameters = parameters + "&udf17Id=${params['udf17Id']}"
            }
            if(params['brandName']){
                parameters = parameters + "&brandName=${params['brandName']}"
            }
            if(params.getAt('groups')){
                parameters = parameters + "&group=${params.getAt('groups')}"
            }
            if(params.getAt('isExclude')){
                parameters = parameters + "&isExclude=${params.getAt('isExclude')}"
            }
            if(params.getAt('range')){
                parameters = parameters + "&range=${params.getAt('range')}"
            }
            if(params.getAt('exclude')){
                parameters = parameters + "&exclude=${params.getAt('exclude')}"
            }

            parameters = parameters + "&order=${params.getAt('order')}"

            if(params.getAt('page')){
                parameters = parameters + "&page=${params.getAt('page')}"
                parameters = parameters + "&pageSize=${params.getAt('pageSize')}"
            }


            parameters = parameters+"&dataFetchFrom=true"



            /*def client = Client.findByClientId(params['clientId'])
            def clientRecordTypes = frontEndService.getClientRecordType(client)
            def mainString="&recordTypes="
            StringBuilder arrayElements=new StringBuilder()
            clientRecordTypes.each{
                arrayElements.append(it.toString()).append(",")
            }
            def temp=arrayElements.toString().lastIndexOf(",")
            def finalArrayElements=arrayElements.toString().substring(0,temp)
            mainString=mainString+"["+arrayElements+"]"

            parameters = parameters + mainString

            if (params['cohortId']) {
                parameters = parameters + '&cohortId=' + params['cohortId'];
            }
            if(params['reportConfig']){
                parameters = parameters + '&reportConfig=' + params['reportConfig'];
            }
            if(params['keyField']){
                parameters = parameters + '&keyField=' + params['keyField'];
            }*/

//            println "parameters = $parameters"
//            parameters="clientId=2000&reportingBasis=PaidDate&reportingTo=2015-06-30&reportingFrom=2014-07-01&eligibilityType=[medical]&report=medicalClaimsPmpm&phiCSDate=01-01-2010&phiCEDate=06-30-2015"
            return fetchService.fetchData(parameters, module)
        } else {
            return null
        }
    }


}
