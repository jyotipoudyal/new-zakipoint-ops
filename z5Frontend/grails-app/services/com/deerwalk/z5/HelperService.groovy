package com.deerwalk.z5

import com.z5.FieldContentType
import groovy.time.TimeCategory
import org.apache.commons.configuration.Configuration
import org.apache.commons.configuration.PropertiesConfiguration
import org.codehaus.groovy.grails.commons.ApplicationHolder
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONException
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.context.request.RequestContextHolder

import java.awt.Color
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat

class HelperService {
	def grailsApplication
	static transactional = false

	Double getRoundedPercentageChange(value,comparedTo) {
		if(!comparedTo || comparedTo==0){return 0}
		if(!value){value=0}
		Double temp=((value-comparedTo)/comparedTo)
		return (Double)temp.round(4)
	}

	

	def csvWriter(fields,data,out,def dateArr=[]){

		def g = grailsApplication.mainContext.getBean('org.codehaus.groovy.grails.plugins.web.taglib.FormatTagLib')

//        response.setContentType("application/ms-excel"); // or you can use text/csv
//        response.setHeader("Content-Disposition", "attachment; filename=output.csv");
		try {
			// Write the header line
			// OutputStream out = response.getOutputStream();
			String header = fields.join(", ");
			header=header+"\n"
			out.write(header.getBytes());
			// Write the content
			String line;
			if(fields[1]=="ER Spend"){
				for (Map da : data) {
					line=new String("${g.formatDate(date: da.time,format: "MMM-yyyy")}"+","+"${da.totalPaidAmount},-,${da.erVisitPer1000PerMonth}"+","+"${da.benchMarkValue}"+"\n");
					out.write(line.toString().getBytes());
				}
			}else if(fields[1]=="Annualized ER Cost Actual"){
				for (Map da : data) {
					def val=da.projected2;
					def act1=g.formatNumber(number:Double.parseDouble(da.actual1)*val,maxFractionDigits: 2);
					def act2=g.formatNumber(number:Double.parseDouble(da.actual2)*val,maxFractionDigits: 2);;

					line=new String("${g.formatDate(date: da.year,format: "MMM-yyyy")},"+"${da.actual1},${act1},${da.actual2},${act2}"+"\n");
					out.write(line.toString().getBytes());
				}
			}else if(fields[1]=="Total ER Spend"){
				for (Map da : data) {
//                    println "da.year--------- = "+da.year
					line=new String("${da.year}"+","+"${g.formatNumber(number: da.erRatio.totalPaidAmountForEr,maxFractionDigits: 2)}"+","+"${g.formatNumber(number: da.erRatio.totalPaidAmountForAvoidableEr,maxFractionDigits: 2)},");
					line+=new String("${g.formatNumber(number: da.erRatio.percentOfAvoidableEr,maxFractionDigits: 2)}"+","+"${g.formatNumber(number: da.erVisitRatio.totalErVisitsperThousand,maxFractionDigits: 2)},");
					line+=new String("${g.formatNumber(number: da.erVisitRatio.totalAvoidableErVisitsperThousand,maxFractionDigits: 2)}"+","+"${g.formatNumber(number: da.erVisitRatio.percentOfAvoidableErVisits,maxFractionDigits: 2)}\n");
					out.write(line.toString().getBytes());
				}
			}else if(fields[3]=="Member Diagnosis for ER Visit") {


				for (Map da : data) {
					line="${formatMonthYearDate(dateArr[0])},"
					line+="${formatMonthYearDate(dateArr[1])},"
					line += da.total.description+","
					line += " ,"
					line += da.total.member+","
					line += da.total.erVisitor+","
					line +="${g.formatNumber(number: da.total.erCost,maxFractionDigits: 0)},"
					line += "${g.formatNumber(number: da.total.nonErVisitor,maxFractionDigits: 2)},"
					line += "${g.formatNumber(number: da.total.oneTimeVisitor,maxFractionDigits: 2)},"
					line += "${g.formatNumber(number: da.total.twoOrMoreTimesVisitor,maxFractionDigits: 2)}"
					da.remove('total');
					if (da) {
						da.sort{it.value.description}.each {
							line += "\n"

							line += " , , ,"
							line += it.value.description+","
							line += "-,"
							line += it.value.erVisitor+","
							line +="${g.formatNumber(number: it.value.erCost,maxFractionDigits: 0)},"
							line += "${g.formatNumber(number: it.value.nonErVisitor,maxFractionDigits: 0)},"
							line += "${g.formatNumber(number: it.value.oneTimeVisitor,maxFractionDigits: 0)},"
							line += "${g.formatNumber(number: it.value.twoOrMoreTimesVisitor,maxFractionDigits: 0)}"
						}
					}
					line += "\n"
					out.write(line.toString().getBytes());
				}
			}else if(fields[3]=="Chronic Condition Member Count") {

				for (Map da : data) {
					line="${formatMonthYearDate(dateArr[0])},"
					line+="${formatMonthYearDate(dateArr[1])},"
					line += da.total.description+","
					line += da.total.member+","
					line += da.total.erVisitor+","
					line +="${g.formatNumber(number: da.total.erCost,maxFractionDigits: 0)},"
					line += "${g.formatNumber(number: da.total.nonErVisitor,maxFractionDigits: 0)},"
					line += "${g.formatNumber(number: da.total.oneTimeVisitor,maxFractionDigits: 0)},"
					line += "${g.formatNumber(number: da.total.twoOrMoreTimesVisitor,maxFractionDigits: 0)}"

					line += "\n"
					out.write(line.toString().getBytes());
				}
			}else if(fields[3]=="Total ER spend for Avoidable Diagnosis"){
				for(Map da:data){
                    line="${formatMonthYearDate(dateArr[0])},"
                    line+="${formatMonthYearDate(dateArr[1])},"
					line+="${da.condition},"
					line+="${g.formatNumber(number: da.totalAmount,maxFractionDigits: 0)},"
					line+="${g.formatNumber(number: da.percentOfTotalAmount,maxFractionDigits: 2)},"
					line+="${g.formatNumber(number: da.erVisit,maxFractionDigits: 0)},"
					line+="${g.formatNumber(number: da.percentOfErVisitPer1000,maxFractionDigits: 0)}\n"
					out.write(line.toString().getBytes());
				}
			}else if(fields[1].toString().contains("Total Avoidable ER Visits")){
				for(Map da:data){
					line="${da.year},"
					line+="${g.formatNumber(number: da.avoidable,maxFractionDigits: 2)},"
					line+="${g.formatNumber(number: da.urgent,maxFractionDigits: 2)}\n"

					out.write(line.toString().getBytes());
				}
			}

			String linebr=new String("\n");
			out.write(linebr.toString().getBytes());

		} catch (Exception e) {
			log.error(e);
		}
	}

	public static boolean isLeapYear(yr){
		def year=yr.toInteger();
		Calendar cal = Calendar.getInstance(); //gets Calendar based on local timezone and locale
		cal.set(Calendar.YEAR, year); //setting the calendar year
		int noOfDays = cal.getActualMaximum(Calendar.DAY_OF_YEAR);

		if(noOfDays > 365){
			return true;
		}

		return false;
	}



	def getReportingDates(ceDate){
		def reportingDates=[:]
		def newDate=Date.parse("MM-dd-yyyy",ceDate)
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");

		use(TimeCategory) {

			def date0=(newDate)

			/*def date2=(newDate-730.days)
			def date3=(newDate-1095.days)*/
			def lastMonth=(newDate-1.month)

			def days=1.day;
			//==========================================================================

			def date1=(date0-1.year)
			if(isLeapYear(year.format(date1)) && (month.format(date1)=="02")){
				days=2.days;
			}else{
				days=1.day;
			}

			reportingDates.currentFrom=df.format((date1+days));
			reportingDates.currentTo=df.format(date0);

			//==========================================================================

			def date2=(date1-1.year)

			if(isLeapYear(year.format(date2)) && (month.format(date2)=="02")){
				days=2.days;
			}else{
				days=1.day;
			}


			reportingDates.pastFrom=df.format((date2+days));
			reportingDates.pastTo=df.format(date1);


			//==========================================================================

			def date3=(date2-1.year)

			if(isLeapYear(year.format(date3)) && (month.format(date3)=="02")){
				days=2.days;
			}else{
				days=1.day;
			}

		reportingDates.priorFrom=df.format((date3+days));
		reportingDates.priorTo=df.format(date2);

			if(isLeapYear(year.format(date1)) && (month.format(date1)=="02")){
				days=2.days;
			}else{
				days=1.day;
			}

		reportingDates.pastMonth=df.format(lastMonth+days);
		}

		return reportingDates

		/*reportingDates.currentFrom=(datesTo[2].toInteger()-1)+"-"+(datesFrom[0])+"-"+datesFrom[1]
		reportingDates.currentTo=datesTo[2]+"-"+datesTo[0]+"-"+datesTo[1]

		reportingDates.pastFrom=(datesTo[2].toInteger()-2)+"-"+(datesFrom[0])+"-"+datesFrom[1]
		reportingDates.pastTo=(datesTo[2].toInteger()-1)+"-"+datesTo[0]+"-"+datesTo[1]

		reportingDates.priorFrom=(datesTo[2].toInteger()-2)+"-"+(datesFrom[0])+"-"+datesFrom[1]
		reportingDates.priorTo=(datesTo[2].toInteger()-2)+"-"+datesTo[0]+"-"+datesTo[1]*/

	}

	def getReportingEndDate(ceDate,toDate){
		def reportingDates=[:]
		def newDate=Date.parse("MM-dd-yyyy",ceDate)
		def newDate2=Date.parse("yyyy-MM-dd",toDate)
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		def outPut=newDate2>=newDate?newDate:newDate2;
		return df.format(outPut);

	}
	def getCycleEndDate(ceDate,toDate){
		def reportingDates=[:]
		def newDate=Date.parse("MM-dd-yyyy",ceDate)
		def newDate2=Date.parse("yyyy-MM-dd",toDate)
		DateFormat df = new SimpleDateFormat("MMMM");
		def outPut=newDate2>=newDate?newDate:newDate2;
		return df.format(outPut);

	}



	def addYear(toDate){
		def newDate=Date.parse("yyyy-MM-dd",toDate)
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		def date1;
		use(TimeCategory) {
			date1 = (newDate + 1.year)
		}
		return df.format(date1);

	}

	def addDay(toDate){
		def newDate=Date.parse("yyyy-MM-dd",toDate)
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		def date1;
		use(TimeCategory) {
			date1 = (newDate +1.day)
		}
		return df.format(date1);

	}

	def formatMonthYearDate(toDate){
		def newDate=Date.parse("yyyy-MM-dd",toDate)
		DateFormat df = new SimpleDateFormat("MMM yyyy");

		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");

		def dateFrom;
		use(TimeCategory) {
			dateFrom = (newDate - 1.year)
			def days;
			if(isLeapYear(year.format(dateFrom)) && (month.format(dateFrom)=="02")){
				days=2.days;
			}else{
				days=1.day;
			}

			dateFrom = dateFrom+days;
		}

		return df.format(dateFrom)+"- "+df.format(newDate);

	}

	def getPercentageString(value){
		Double tempValue
		if(!value || value=='null'){tempValue = 0}else{tempValue = value}
		return String.format('%.2f',tempValue)+'%'
	}

	def getFormattedPercentageString(value){
		Double tempValue
		if(!value || value=='null'){tempValue = 0}else{tempValue = (value*100)}
		return String.format('%.2f',tempValue)+'%'
	}

	def  getPercentageChange(value,comparedTo) {
		if(!comparedTo || comparedTo==0){return 'N/A'}
		if(!value){value=0}
		Double temp=((value-comparedTo)/comparedTo)*100
		return String.format('%.2f',temp)+'%'
	}

	String getRoundedStringValue(Double valueToRound){
		if(valueToRound){
			return String.format('%(,.2f',valueToRound)
		}else{
			return '0.00'
		}
	}
	Double getRoundedStringValueReturnDouble(Double valueToRound){
		if(valueToRound){
			return Double.valueOf(String.format('%(.2f',valueToRound))
		}else{
			return 0.00
		}
	}

	Double getRoundedValueReturnDouble(Double valueToRound){
		if(valueToRound){
			DecimalFormat df = new DecimalFormat("#.00");
			return Double.valueOf(df.format(valueToRound))
		}else{
			return 0.00
		}
	}

	def getFormattedInteger(Integer value){
		if(value == null){
			return ''
		}
		DecimalFormat integerFormatter = new DecimalFormat('###,###');
		String formattedInteger = integerFormatter.format(value);
		return formattedInteger
	}

	def getFormattedIntegerAndZero(Integer value){
		if(value==0){
			return '0'
		}
		else if(!value){
			return '--'
		}
		DecimalFormat integerFormatter = new DecimalFormat('###,###');
		String formattedInteger = integerFormatter.format(value);
		return formattedInteger
	}


	def getFormattedIntegerForAspose(Integer value){
		if(!value && value !=0){
			return ''
		}
		DecimalFormat integerFormatter = new DecimalFormat('###,###');
		String formattedInteger = integerFormatter.format(value);
		return formattedInteger
	}
	def getFormattedDoubleForAspose(Double value){
		if(!value && value !=0){
			return ''
		}
		DecimalFormat doubleFormatter = new DecimalFormat('###,###.##');
		String formattedDouble = doubleFormatter.format(value);
		return formattedDouble
	}

	def getFormattedDouble(Double value){
		DecimalFormat doubleFormatter = new DecimalFormat('###,###.##');
		String formattedDouble = doubleFormatter.format(value);
		return formattedDouble


	}

	Double getFormattedCurrencyForInteger(Double valueToRound){
//		return String.format('$%(,.0f', valueToRound)
		return valueToRound
	}

	def getFormattedDoubleValue(Double value){
		if(!value && value !=0){
			return ''
		}
		DecimalFormat formattedValue = new DecimalFormat('#.##');
		String result = formattedValue.format(value);
		return result
	}

	String checkStringIfNull(String value){
		if(!value) return ''
		else return value
	}

	String generateDashForNullValue(String value){
		if(!value) return '--'
		else return value
	}

	//TODO rename this method
	String getFormattedData(valueToRound){
		if (valueToRound instanceof String || !valueToRound){
			return ""
		}else{
			valueToRound = (Double)valueToRound
		}
		return String.format('%(,.0f', valueToRound)
	}
	String getFormattedValue(Double valueToRound){
		return String.format('%(,.0f', valueToRound)
	}
	Double getFormattedCurrency(Double valueToRound){
		if(valueToRound==null){
			return ''
		}
//		return String.format('$%(,.2f', valueToRound)
		return  valueToRound
	}
	String getFormattedCurrencySign(Double valueToRound){
		if(valueToRound==null){
			return ''
		}
		return (valueToRound>=0)?("\$"+String.format('%,.2f', valueToRound)):("-\$"+String.format('%,.2f', -valueToRound))
	}
	def getSum(jsonArray, columnKey){
		def total = 0
		jsonArray.each {keys,value->
			value.each{key ,val->
				if(key==columnKey){
					total =total+val
				}
			}
		}
		return   total
	}

	def getTotalSumLifeStyle(dataList,columnKey){
		def total = 0
		dataList.each {value->
			value.each{key ,val->
				if(key==columnKey){
					total =total+val
				}
			}
		}
		return   total
	}

	Double divideGracefully(dividend, divisor){
		Double temp
		if(dividend && divisor && divisor!=0){
			temp = (Double)dividend/divisor
			return (Double)temp.round(4)
		}else{
			return 0.00
		}
	}

	String getPercentage(dividend, divisor){
		Double temp
		if(dividend && divisor && divisor!=0){
			temp = (Double)(dividend/divisor)*100
			temp = (Double)temp.round(2)
			return temp.toString()+'%'
		}else{
			return '0.00%'
		}
	}

	String getPercentWithoutDecimal(dividend, divisor){
		Double temp
		if(dividend && divisor && divisor!=0){
			temp = (Double)(dividend/divisor)*100
			temp = (Double)temp.round()
			return temp.toInteger().toString()+'%'
		}else{
			return '0%'
		}
	}

	Double getValueFromPercentage(percent){
		String temp=percent
		int index=temp.indexOf("%")
		String temp1=temp.substring(0,index)
		return Float.parseFloat(temp1)
	}

	def getZeroForNull(value){
		if(value){
			return value
		}else{
			return 0
		}
	}
	def getOneForNull(value){
		if(value){
			if(value==0)
				return 1
			else
				return value
		}else{
			return 1
		}
	}

	def getSplittedCamelCaseString(field,value){
		def finalString=field?field.displayName:camelToStandardCase(value)
		return finalString
	}

	String camelToStandardCase(String camelCasedString){
		return (camelCasedString.substring(0, 1).toUpperCase()+camelCasedString.substring(1).replaceAll(/\B[A-Z]/) { ' ' + it })
	}

	def getTrimmedArrayString(value){
		StringBuilder newString = new StringBuilder("[")
		def tempArray = []
		if(value!='' && value.getAt(0)=='[' && value.getAt(value.length()-1)==']' && value.length()>2){
			value = value.getAt(1..value.length()-2)
			tempArray = value.split(', ')
			tempArray.eachWithIndex{ val, ind ->
				newString.append(val)
				if(ind<tempArray.size()-1){newString.append(',')}
			}
			newString.append(']')
			return newString.toString()
		}else{
			return value
		}
	}

	public String listToString(data,int maxCount=-1,String defaultValue=''){
		StringBuilder str = new StringBuilder("")
		data.eachWithIndex{ def entry, int i ->
			if (i<=maxCount || maxCount==-1){
				if (str.toString().equals("")){
					str.append(entry)
				}
				else{
					if (i == data.size()-1){
						str.append(" and ").append(entry)
					}else if (i==maxCount){
						str.append(" and ").append(data.size()-maxCount).append(" others ")
					}else{
						str.append(", ").append(entry)
					}
				}
			}
		}
		return (str.toString().equals("")) ? defaultValue : str.toString()

		//return str
	}

	String getGetListString(list){
		if(list instanceof Collection || list?.class?.isArray()){
			StringBuilder result = new StringBuilder("[")
			int size = list.size()
			list.eachWithIndex{val, i ->
				result.append(val).append("${(i<size-1) ? ',' : ''}")
			}
			result.append("]")
			return result.toString()
		}else{
			return getTrimmedArrayString(list)
		}
	}



	def getPer1000(value, memberMonth, month){
		def mmPerMonth = divideGracefully(memberMonth,month)
		def result = (Double)(divideGracefully(value*1000,mmPerMonth))*(12/month)
		return result.round(2)
	}

	def getAbsolutePer1000(value, memberMonth, month){
		def mmPerMonth = divideGracefully(memberMonth,month)
		def result = (Double)(divideGracefully(value*1000,mmPerMonth))
		return result.round(0)
	}
	/**
	 *
	 * @param client
	 * @return
	 * @Deprecated using instead getClientLogo of MakaluServie
	 */

	@Deprecated
	String  getClientLogo(client){
		def root = ApplicationHolder.getApplication().getMainContext().getResource("/").getFile().getAbsolutePath()
		def defLogo =  root+"/images/reportLogo.png"
		if(client){
			def logoPath = root +"/images/logo/" + client?.logoFileName
			boolean exists = (new File(logoPath)).exists();
			if (exists) {
				return logoPath
			} else {
				return defLogo
			}
		}else{
			return defLogo
		}

	}



	//backup before changing to clientinformation object
//	def getClientTheme(client){
//
//		def root = ApplicationHolder.getApplication().getMainContext().getResource("/").getFile().getAbsolutePath()
//		def themeName
//		def theme = client.theme
//		def clientTheme
//		clientTheme='js/jquery-ui/themes/cupertino'
//		if(theme){
//			boolean exist = new File(root+"/js/jquery-ui/themes/${theme.themeDirectory}/custom.css").exists()
//			if(exist){
//				clientTheme="js/jquery-ui/themes/${theme.themeDirectory}" //= root+"/js/jquery-ui/themes/${theme}"
//				return clientTheme
//			}
//		}
//
//		return clientTheme
//	}

	def assignGroup(value,category,qualityMetric,map,list){
		def group = 'ZZ'


		if(list.contains(category)){
			qualityMetric.each{
				if (value.equals(it.code)){
					group= map.getAt(category)?map.getAt(category):'ZZ' //it.qmGroup

				}
			}
		}

		return group
	}

	def findQualityMetricDisplayName(category,qualityMetric,list){
		if(list.contains(category)){
			return qualityMetric.find{it.code==category}.displayName
		}
	}

	def assignSubGroup(group,subgroup){
		/*def subCount = subgroup.getAt(group)
		def count = subCount+1
		if(count<10){
			count = '0'+(String)count
		}
		def assign =  group//+(String)count
		subgroup[group]= subCount+1
		return assign*/
		return subgroup.find{it.code==group}?.qmGroup
	}
	Double getZeroIfNull(value){
		if(value){
			return (Double)value
		}else{
			return 0.00
		}
	}


	def getFormatedDateTime(String dateString,String format) {
		try{
			if (dateString == null || dateString.length() == 0) return null;
			return new SimpleDateFormat(format).parse(dateString);
		}catch(e){
			return null
		}
	}

	def getFormatedDate(String dateString,String format) throws Exception{
		if (dateString == null || dateString.length() == 0) return null;
		return new SimpleDateFormat(format).parse(dateString);

	}

	def getDateString(Date date, String pattern) {
		if (date == null) return null;
		return new SimpleDateFormat(pattern).format(date);
	}
/**
 * Convert string into float
 * @param floatValue
 * @param defaultValue
 * @return
 */
	Float convertStringToFloat(String floatValue,Float defaultValue){
		if(floatValue){
			floatValue=floatValue.trim();
			if(floatValue.startsWith('(') && floatValue.endsWith(')')){
				if(defaultValue!=null){
					return defaultValue
				}
				floatValue='-'+floatValue.replace('(','').replace(')','');
			}
			if(floatValue.contains(',')){
				floatValue=floatValue.replaceAll(',','');
			}
			if(floatValue.isFloat()){
				return floatValue.toFloat()
			}
		}
		return defaultValue;
	}
	Double divideGracefullyOnlyPositive(dividend, divisor){
		Double temp=divideGracefully(dividend, divisor)
		if(temp<0.0){
			return 0.0;
		}
		return temp;
	}
	def getPer1000OnlyPositive(value, memberMonth, month){
		if(value<0.0){
			return 0.0
		}
		return getPer1000(value, memberMonth, month)
	}
	String getPositiveRoundedStringValue(Double valueToRound){
		if(!valueToRound || valueToRound<0){
			return '0.00'
		}
		return String.format('%(,.2f',valueToRound)
	}
	String getPositiveFormattedStringValue(Double valueToRound){
		if(!valueToRound || valueToRound<0){
			return '0'
		}
		return String.format('%(,.0f',valueToRound)
	}

	def int getMonthsDifference(Date date1, Date date2) {
		int m1 = date1.getYear() * 12 + date1.getMonth();
		int m2 = date2.getYear() * 12 + date2.getMonth();
		return m2 - m1 + 1;
	}

	def getNameListOfFilesInDirectory(String directory){
		def root=grailsApplication.getMainContext().getResource("/").getFile().getAbsolutePath()
		directory=root+directory
		def listOfFile=[]
		new File(directory).eachFile {file->
			listOfFile<<file.getName()
		}
		return listOfFile
	}
	def replaceInQueryString(url,key,value){
		if(!url){
			return null;
		}
		return url.replaceAll("([?&]" + key + ")=[^?&]+",'$1=' + value);
	}

	//returns index of month in given date
	def getMonthFromDate(String date,format){
		def parsedDate=Date.parse(format, date)
		Calendar cal=Calendar.getInstance();
		cal.setTime(parsedDate)
		def monthOfYear=cal.get(Calendar.MONTH)
		return cal
	}

	def sortQuaterlyDate(column){
		def quarterMap=['Q1':'Jan','Q2':'Feb','Q3':'Mar','Q4':'Apr'];
		column=column.sort { e1, e2 ->
			try{
				getFormatedDate(e1.key,'MMM-yyyy') <=>getFormatedDate(e2.key,'MMM-yyyy');
			}catch(Exception e){
				if (e1.key.contains('-')){
					def quarter1=e1.key.split ('-')[0]
					def quarter2=e2.key.split ('-')[0]
					getFormatedDateTime(e1.key.replace(quarter1,quarterMap.getAt(quarter1)),'MMM-yyyy') <=>getFormatedDateTime(e2.key.replace(quarter2,quarterMap.getAt(quarter2)),'MMM-yyyy');
				}else{
					e1.key <=> e2.key
				}

			}
		}
		return column
	}

	def sortDateList(list,dateFormat){
		if(!list){
			return list;
		}
		try{
			SimpleDateFormat sdf=new SimpleDateFormat(dateFormat);
			return list.sort{
				sdf.parse(it)
			}
		}catch(e){
			log.error(e.getMessage())
		}
		return list;
	}

	def getDateObject(key,format){
		def parsedDate
		try{
			parsedDate=Date.parse(format, key)
		}catch (e){
			return key
		}

		Calendar cal=Calendar.getInstance();
		cal.setTime(parsedDate)
		return cal
	}


	def getFormattedValue(FieldContentType contentType,value){
		switch(contentType){
			case FieldContentType.Amount:
				return getFormattedCurrency(value);
				break;
			case FieldContentType.LargeAmount:
				return getFormattedCurrencyForInteger(value);
				break;
			case FieldContentType.Number:
				return getFormattedValue(value);
			case FieldContentType.WholeNumber:
				def temp = getPositiveRoundedStringValue(value)
				return temp ;
			default:
				return getFormattedValue(value)
				break;
		}
	}


	public byte[] fileToByteArray(File file) throws IOException{

		ByteArrayOutputStream ous = null;
		InputStream ios = null;
		try {
			byte[] buffer = new byte[32768];
			ous = new ByteArrayOutputStream();
			ios = new FileInputStream(file);
			int read = 0;
			while ( (read = ios.read(buffer)) != -1 ) {
				ous.write(buffer, 0, read);
			}
		} finally {
			try {
				if ( ous != null )
					ous.close();
			} catch ( IOException e) {
			}

			try {
				if ( ios != null )
					ios.close();
			} catch ( IOException e) {
			}
			file.delete()
		}
		return ous.toByteArray();
	}



	def setColor(metric,metricType){
		Color color = Color.BLACK
		if(metric.toLowerCase().contains('not')){
			if(metricType == 'negative'){
				color = color.black
			}else{
				color = Color.RED
			}
		}else{
			if(metricType == 'negative'){
				color = Color.RED
			}else{
				color = color.black
			}
		}
		return color
	}

	def trimWhiteSpace(String s){
		s = s.replaceAll("\\s","")
		return s
	}

	public static boolean containsWhitespace(String str) {
		if (!hasLength(str)) {
			return false;
		}
		int strLen = str.length();
		for (int i = 0; i < strLen; i++) {
			if (Character.isWhitespace(str.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasLength(String str) {
		return (str != null && str.length() > 0);
	}

	public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
		Map<String, Object> retMap = new HashMap<String, Object>();

		if(json != JSONObject.NULL) {
			retMap = toMap(json);
		}
		return retMap;
	}

	public def getJSON(json){
		def loaP = json
		return loaP
	}

	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while(keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for(int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	def checkSelectedLoaInUserAccess(selectedLOA, userAccess){
		def userAccessLevelName = (userAccess.keySet() as String[])[0]
		def selectedLOAToCheck = selectedLOA[userAccessLevelName]
		def userAccessLevelToCheck = userAccess[userAccessLevelName]
		if(selectedLOAToCheck instanceof ArrayList) {
			Collections.sort(selectedLOAToCheck)
		}
		if(userAccessLevelToCheck instanceof ArrayList) {
			Collections.sort(userAccessLevelToCheck)
		}

		if(userAccessLevelToCheck.equals(selectedLOAToCheck)){
			selectedLOA.remove(userAccessLevelName)
		}
	}



	def fetchClientName(){

		def sessions = RequestContextHolder.currentRequestAttributes().getSession()
		return sessions.clientname
		/*def servletContext = "/config/display.properties"
		Configuration config = new PropertiesConfiguration(servletContext.toString());
		def value=config.getProperty("client.clientName")
		return value*/
	}

	def fetchClientId(){
		/*def servletContext = "/config/display.properties"
		Configuration config = new PropertiesConfiguration(servletContext.toString());
		def value=config.getProperty("client.clientId")
		return value*/
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def clientUrl=ClientPanel.findByClient(session.clientname)
		if(clientUrl)
		return clientUrl.parent?clientUrl.parent.clientId:clientUrl.clientId;
	}

	def setClientUrl(client){
		def map=[:]
//		def servletContext = "/config/display.properties"
//		Configuration config = new PropertiesConfiguration(servletContext.toString());
//		def value=config.getProperty("client."+client)
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def clientDates,csDate,ceDate;

		def clientUrl=ClientPanel.findByClient(client)
		def value=clientUrl?.uId



//		if(client!=grailsApplication.config.grails.bobUrl){
		if(!clientUrl?.isBob){

			clientDates=BusinessLevelEntry.findByCodeAndLevel(value.toString(),"groupId")
			csDate=clientDates?.cycleStartDate
			ceDate=clientDates?.cycleEndDate
			map[value]="1"

			/*def name=client.toString().split("");
			def before=name-name.last();
			before=before.join("").capitalize()
			before+=" "+name.last().capitalize()*/

//			csDates=BusinessLevelEntry
			/*map[client]="1"
			map.each{key,val->
				def name=key.toString().split("");
				def before=name-name.last();
				before=before.join("")
				before+=" "+name.last()

				if(key==client){
					map[key]="1"
				}else{
					map[key]="0"
				}
			}*/
		}else{
			def groupId=clientUrl.parent?clientUrl.parent.clientId:(clientUrl?.clientId);
//			clientDates=Client.findByClientId(config.getProperty("client.clientId"))
			clientDates=Client.findByClientId(groupId)
			csDate=clientDates?.cycleStartDate
			ceDate=clientDates?.cycleEndDate
			map=[:]
		}

		if(!csDate){
			csDate="2015-07-01"
			ceDate="2017-04-30"
		}else{
			csDate.clearTime()
			ceDate.clearTime()
		}
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
		session.csDate=df.format(Date.parse("yyyy-MM-dd",csDate.toString()))
		session.ceDate=df.format(Date.parse("yyyy-MM-dd",ceDate.toString()))
		session.isBiometrics=clientUrl.biometrics
		return map

	}
}
