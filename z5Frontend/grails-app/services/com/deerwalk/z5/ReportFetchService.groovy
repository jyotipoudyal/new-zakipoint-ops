package com.deerwalk.z5

import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class ReportFetchService {

    def fetchService;

    def getDataRequest(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        try{
            json = fetchChartData(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);
            rawJSON=parsedJSON.getAt('reporting');
        }catch(e){
            log.error(e)
            log.error('No Data received .................')
            return '';
        }
        return rawJSON
    }

    def getBiometricDataRequest(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        try{
            json = fetchChartData(report,dynamicReportParams)
            rawJSON=JSON.parse(json);
        }catch(e){
            log.error(e)
            log.error('No Data received .................')
            return '';
        }
        return rawJSON
    }




    def getCohortList(dynamicReportParams,report){
        def rawJSON=null;
        def json;
        try{
            json = fetchCohortList(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);
            rawJSON=parsedJSON
        }catch(e){
            log.error(e)
            log.error('No Data received for cohort......................')
            return '';
        }
        return rawJSON
    }

    def getCohortComparatorList(dynamicReportParams,report){
        def rawJSON=null;
        def json;
        try{
            json = fetchCohortList(report,dynamicReportParams,"cohortTrack")
            def parsedJSON=JSON.parse(json);
            rawJSON=parsedJSON
        }catch(e){
            log.error(e)
            log.error('No Data received for cohort......................')
            return '';
        }
        return rawJSON
    }

    private def fetchCohortList(report, params,module="cohort") {
        def parameters
        parameters = "clientId=${params['clientId']}"
        if (params['task']) {
            parameters = parameters + "&task=${params['task']}"
        }
        if (params['domain']) {
            parameters = parameters + "&domain=${params['domain']}"
        }
        if (params['showInIdentity']) {
            parameters = parameters + "&showInIdentity=${params['showInIdentity']}"
        }
        if (params['groups']) {
            parameters = parameters + "&groupId=${params['groups'][0]}"
        }
        if (params['comparatorId']) {
            parameters = parameters + "&comparatorId=${params['comparatorId']}"
        }

        return fetchService.fetchData(parameters, module)

    }


     private def fetchChartData(report, params) {
        def module=""
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def parameters


        if (params && params['reportingTo'] && params['reportingFrom'] && params['basis'] && params['clientId']) {
            def loaParams = params["loaParams"]
            /*if(session.user.userAccess){
                loaParams = helperService.checkSelectedLoaInUserAccess(loaParams, session.user.userAccess);
            }*/
            if(report.equals('provider:erVisit') || report.equals('provider:snapshot') || report.equals('provider:thirtyDayReadmit')){
//                module = 'provider'
                parameters = "clientId=${params['clientId']}${fetchService.getLoaRequestParameterSectionForProviderDashBoard(loaParams)}"
            }else{
                parameters = "clientId=${params['clientId']}"
            }
            if(params["switchedUserId"]){
                parameters += "&userId="+params["switchedUserId"]
            }
            parameters = parameters + "&reportingBasis=${params['basis']}&reportingTo=${params['reportingTo']}&reportingFrom=${params['reportingFrom']}&report=${report}"
            parameters = parameters + "&eligibilityType=${params.getAt('eligibilityType')}"
            if (params['basis'] == 'ServiceDate') {
//                parameters = parameters + "&reportingPaidThrough=${params['reportingThrough']}"
            }
            if (params['participation']) {
                parameters = parameters + "&isParticipation=${params['participation']}"
                if (params && params['program_name']) {
                    parameters = parameters + "&program_name=${params['program_name']}"
                }
                if (params && params['program_type']) {
                    parameters = parameters + "&program_type=${params['program_type']}"
                }
            }

            if(params['dataView']){
                parameters = parameters + "&dataView=${params['dataView']}"
            }

            if(params['phiCSDate']){
                parameters = parameters + "&phiCSDate=${params['phiCSDate']}"
            }

            if(params['phiCEDate']){
                parameters = parameters + "&phiCEDate=${params['phiCEDate']}"
            }

            if(params && params["trendingField"]){
                parameters = parameters + "&trendingField=${params['trendingField']}"
            }

            if(report=="populationRisk"){
                parameters = parameters + "&recordTypes=${params.getAt('recordTypes')}"
            }
            if(params['udf17Id']){
                parameters = parameters + "&udf17Id=${params['udf17Id']}"
            }

            if(params['includes']){
                parameters = parameters + "&includes=${params['includes']}"
            }

            if(params.getAt('groups')){
                parameters = parameters + "&group=${params.getAt('groups')}"
            }
            if(params.getAt('isExclude')){
                parameters = parameters + "&isExclude=${params.getAt('isExclude')}"
            }
            if(params.getAt('range')){
                parameters = parameters + "&range=${params.getAt('range')}"
            }
            if(params.getAt('exclude')){
                parameters = parameters + "&exclude=${params.getAt('exclude')}"
            }
            if(params.getAt('trend')){
                parameters = parameters + "&trend=${params.getAt('trend')}"
            }
            if(params.getAt('order')){
                parameters = parameters + "&order=${params.getAt('order')}"
            }
            if(params.getAt('page')){
                parameters = parameters + "&page=${params.getAt('page')}"
                parameters = parameters + "&pageSize=${params.getAt('pageSize')}"
            }
            /*if(params.getAt('metric')){
                parameters = parameters + "&metric=${params.getAt('metric')}"
            }*/
            if(params.getAt('benchmarkKey')){
                parameters = parameters + "&benchmarkKey=${params.getAt('benchmarkKey')}"
            }
            if(params.getAt('metricCode')){
                parameters = parameters + "&metricCode=${params.getAt('metricCode')}"
            }
            if(params.getAt('biometricId')){
                parameters = parameters + "&biometricId=${params.getAt('biometricId')}"
            }

            if(params.getAt('biometricName')){
                parameters = parameters + "&biometricName=${params.getAt('biometricName')}"
            }
            if(params.getAt('chronicCode')){
                parameters = parameters + "&chronicCode=${params.getAt('chronicCode')}"
            }

            if(params.getAt('category')){
                parameters = parameters + "&category=${params.getAt('category')}"
            }
            parameters=parameters+"&dataFetchFrom=true"

            if(report.equals("populationRiskSankeyBuilderListener")) module="reportFromDomain"
            else if(report.equals("biometricSnapshot")) module="reportFromDomain"
            else if(report.equals("biometricSankeyBuilderListener")) module="reportFromDomain"

            return fetchService.fetchData(parameters, module)
        } else {
            return null
        }
    }
}
