package com.deerwalk.z5

import grails.util.Environment
import net.sf.ehcache.Ehcache
import org.apache.log4j.Logger
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.context.request.RequestContextHolder

class FetchService {
    boolean cacheHitFlag = true
    def urlMappingService
    def grailsApplication
    def performanceLogger = Logger.getLogger("performanceLogger")
    def servletContext = ServletContextHolder.servletContext
    def dwProxyService
    def helperService
    def searchService


    def fetchData(String parameters, module) {
        cacheHitFlag = true
//        if(module=="qoc") module="report"
//        if(module=="dashboard") module="dashboard"
        if(!module)module="Zakipoint"

        long startTime = System.nanoTime();
        def session = RequestContextHolder.currentRequestAttributes().getSession()
//        parameters = parameters + session.user["userPhiString"] + session.user["userFiString"]
//        storeCurrentBackendCall(parameters,module)
        long dasStartTime = System.nanoTime();
        def returnMap = grailsApplication.mainContext.fetchService.cachedFetchData(parameters,module, null, null)
//        def returnMap = null;
        println("Das Query Time : ${((System.nanoTime() - dasStartTime) / 1000000000)} seconds.")

        def data = null
        def hasCollision = !parameters.equals(returnMap['parameters'])
        if(returnMap && !hasCollision && !returnMap['data'].equals("{}") && returnMap['data']!=null && returnMap['data'].length()>100 ){
            data = returnMap['data']
        }else{
            if(returnMap && hasCollision){
                log.warn 'CACHE COLLISION: INPUT PARAM:' + parameters + ' != OUTPUT PARAM: ' +returnMap['parameters']
                def collisionCount = 1
                if(servletContext.getAttribute('collisionCount')){
                    collisionCount = servletContext.getAttribute('collisionCount').toInteger()+collisionCount
                    servletContext.setAttribute("collisionCount",collisionCount)
                }else{
                    servletContext.setAttribute("collisionCount",collisionCount)
                }
            }
            if (cacheHitFlag){
                data = unCachedFetchData(parameters, module)
            }
            else
                data = returnMap?.data

//            evictAllForCurrentRequest(parameters,module, null, null)
        }

        logPerformance("${urlMappingService.getModuleUrl(module)}?${parameters}","${((System.nanoTime() - startTime) / 1000000000)}",cacheHitFlag,returnMap.getAt('responseCode'))
        return data
    }

    def fetchMemberData(String parameters, module) {
        cacheHitFlag = true
        long startTime = System.nanoTime();
        def session = RequestContextHolder.currentRequestAttributes().getSession()
//        parameters = parameters + session.user["userPhiString"] + session.user["userFiString"]
//        storeCurrentBackendCall(parameters,module)
        long dasStartTime = System.nanoTime();
        def returnMap = grailsApplication.mainContext.fetchService.cachedFetchData(parameters,module, null, null)
        println("Das Query Time : ${((System.nanoTime() - dasStartTime) / 1000000000)} seconds.")

        def data = null
        def hasCollision = !parameters.equals(returnMap['parameters'])
        if(returnMap && !hasCollision && !returnMap['data'].equals("{}") && returnMap['data']!=null && returnMap['data'].length()>100 ){

            data = returnMap['data']
        }else{
            if(returnMap && hasCollision){
                log.warn 'CACHE COLLISION: INPUT PARAM:' + parameters + ' != OUTPUT PARAM: ' +returnMap['parameters']
                def collisionCount = 1
                if(servletContext.getAttribute('collisionCount')){
                    collisionCount = servletContext.getAttribute('collisionCount').toInteger()+collisionCount
                    servletContext.setAttribute("collisionCount",collisionCount)
                }else{
                    servletContext.setAttribute("collisionCount",collisionCount)
                }
            }
            if (cacheHitFlag){
                data = unCachedFetchData(parameters, module)
            }
            else
                data = returnMap?.data

//            evictAllForCurrentRequest(parameters,module, null, null)
        }

        logPerformance("${urlMappingService.getModuleUrl(module)}?${parameters}","${((System.nanoTime() - startTime) / 1000000000)}",cacheHitFlag,returnMap.getAt('responseCode'))
        return data
    }

    //Checks the request scope for recentBackendCalls and evicts all the corresponding cache elements


    def unCachedFetchData(parameters, module){
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        if(!parameters.toString().contains('phiCSDate') && session.user){
            parameters = parameters + session.user["userPhiString"]
            parameters = parameters + session.user["userFiString"]
        }
        long startTime = System.nanoTime();
        def returnMap = cachedFetchData(parameters,module, null, null)
//        logPerformance("${urlMappingService.getModuleUrl(module)}?${parameters}","${((System.nanoTime() - startTime) / 1000000000)}",false,returnMap.getAt('responseCode'))
        return returnMap.getAt('data')
    }

    def logPerformance(String url, String time, boolean cached, responseCode){
        String cachedString = cached ? "CACHED" : (responseCode==200?"FRESH":'ERROR')
        if (performanceLogger && (Environment.PRODUCTION == Environment.getCurrent())){
            String md5Query = url.encodeAsMD5();
            def session = RequestContextHolder.currentRequestAttributes().getSession()
            String clientId = session?.client?.clientId
            String logLine = '"'+clientId+'";"'+cachedString+'";"'+time+'";"'+md5Query+'";"'+url+'";"'+responseCode+'"';
            performanceLogger.info(logLine);
        }else{
            log.info "${cachedString}: ${responseCode}: ${url}"
        }
//        if ( !(Environment.TEST== Environment.getCurrent())){
            logURLForEveryone("${cachedString}::${responseCode}::${url}")
//        }
    }



//    @Cacheabsle(cache = "backEndData", cacheResolver = "makaluCacheResolver")
    def cachedFetchData(String parameters,module, clientName, token){
        clientName=helperService.fetchClientName()
//        clientName=searchService.getGroups()
        cacheHitFlag = false;
        if (!parameters) return null
//        def mainUrl = grailsApplication.config.grails.backEndMAINURL;
        def moduleUrl = urlMappingService.getModuleUrl(module)
        def url       = new URL(moduleUrl)
        def sessions = RequestContextHolder.currentRequestAttributes().getSession()

        try {
            def connection = url.openConnection()
            connection.setRequestMethod("POST")
            connection.doOutput = true
            Writer wr           = new OutputStreamWriter(connection.outputStream)
            String ticket = null
            String parameterWithTicket = ""

            if(Environment.DEVELOPMENT != Environment.getCurrent()){
                println "********entering ticket generation***************"
                def mainUrl = grailsApplication.config.grails.backEndMAINURL;
                //grailsApplication.config.grails.backEndURL = "https://das-qc.deerwalk.com";
                //Das Router always need to  deploy in root without port. If there are 2 colons (:) then it will go directly to dasTest instead of DasRouter.
                if (!token){
                    ticket = dwProxyService.getProxyTicket(mainUrl, 1)
                }
                else         ticket = dwProxyService.getProxyTicket(token, mainUrl, 1)

                if(grailsApplication.config.grails.backEndURL.toString().split(':').size()>1){
                    println(grailsApplication.config.grails.backEndURL+"==Grails backendUrl WIth all parameter"+moduleUrl);
                }else{
                    println("Grails backendUrl main url"+mainUrl);
                }
            }


            if(!clientName && module != 'ClusterSwitch' && module != 'clusterHealth' && module!='indexBackupProcess' && module!='loadTables'){
//                clientName   = getClientName(getRequestHost(),parameters)
                clientName=helperService.fetchClientName()
            }
            parameterWithTicket = getProxyTicketAndClient(ticket, clientName, parameters)
            println "URL :: " + moduleUrl +"?" +parameterWithTicket
            sessions.currentUrl=moduleUrl +"?" +parameters;
            wr.write(parameterWithTicket)
            wr.flush()
            wr.close()
            connection.connect();
            def session = RequestContextHolder.currentRequestAttributes().getSession()
            session['casError'] = false
            if(connection.getResponseCode()==401){
                log.error "URL :: " + moduleUrl +"?" +parameterWithTicket+" response :: " +connection.getResponseCode()
                session['casError'] = true
            }
            /*def map=[:]
            def reportingDates=helperService.getReportingDates(servletContext.getAt("ceDate"))
            parameters.tokenize("&").each{

                def aa=it.tokenize("=")
                map[aa[0]]=aa[1]

            }
            map.reportingFrom=reportingDates.priorFrom
            map.reportingTo=reportingDates.currentTo

            servletContext.setAttribute('parameters',map)*/
            return [parameters:parameters,data:connection.content.text,responseCode:connection.getResponseCode()]
        }catch (e) {
            e.printStackTrace()
            return [parameters:parameters,data:null,responseCode:500]
        }
    }

    public String getProxyTicketAndClient(String ticket, String clientName, String parameter) {
        parameter = parameter + "&ticket=" + ticket
        if(clientName){
            parameter = parameter + "&" + "clientName=" + clientName
        }
        parameter
    }


    //Store the backend call parameters in the request scope
    def storeCurrentBackendCall(String parameters,module){
        def request   = RequestContextHolder.getRequestAttributes().getRequest()
        if(request.rescentBackendCalls && request.rescentBackendCalls instanceof List){
            request.recentBackendCalls.add(['parameter':parameters,'module':module])
        }else{
            request.recentBackendCalls = [['parameter':parameters,'module':module]]
        }
        return request.recentBackendCalls
    }

    def logURLForEveryone(String url){
        if(servletContext.getAttribute('USER_URL_LOGGING')){
            def session = RequestContextHolder.currentRequestAttributes().getSession()
            if(session.getAttribute('LOG-URL')){
                def count = session.getAttribute('LOG-COUNT')
                LinkedList urlList = session.getAttribute('LOGGED-URL')
                if(urlList && urlList instanceof List){
                    if(urlList.size()>9){
                        urlList.remove(0)
                    }
                    urlList.add(url)
                }else{
                    urlList = [url] as LinkedList
                }
                if(!count){
                    count=0
                }
                if(count<50){
                    if(url && url!='' && url.length()<10000){
                        session.setAttribute('LOGGED-URL',urlList)
                        session.setAttribute('LOG-COUNT',count+1)
                    }
                }else{
                    session.removeAttribute('LOGGED-URL')
                    session.removeAttribute('LOG-COUNT')
                    session.setAttribute('LOG-URL',false)
                }
            }else{
                session.removeAttribute('LOGGED-URL')
                session.removeAttribute('LOG-COUNT')
            }
        }
    }

    def getProviderParams(params, field) {
        def cleanParams = [:]
        params.each { key, val ->
            if (key.contains(field)) {
                def replaceKey = key.replaceAll(field, 'provider')
                if (val && val != '') {
                    cleanParams[replaceKey] = val
                }
            }
        }
        return cleanParams
    }

    def fetchFaq(params){
        def req="https://spreadsheets.google.com/feeds/list/1MkdJafdr8oKOiXU7-6XOmSyCtaHYaibQXTxHplyE2Ak/";
        def appendReq="/public/values?alt=json";
        URL url = new URL(req+""+params.catId+""+appendReq);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(240000); //120 seconds connection timeout
        int responseCode = con.getResponseCode();
//        System.out.println("Sending get request : " + url);
//        System.out.println("Response code : " + responseCode);
//
        BufferedReader ins = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String output;
        StringBuilder response = new StringBuilder();

        while ((output = ins.readLine()) != null) {
            response.append(output);
        }
        ins.close();
        con.disconnect();
//        System.out.println(response);

        def data= new JSONObject((response).toString());
        return data["feed"]["entry"];
    }


    /*def getClientName(String requestHost, String parameters) {
        def requestedClient
        try{
            if(requestHost==grailsApplication.config.grails.adminAccessDomain){
                def splittedParameters = parameters.split('clientId=')
                def clientId = splittedParameters[1].split('&')[0]
                def client = Client.findByClientId(clientId)
                requestHost = client.domain
            }

            requestedClient = requestHost.substring(0, requestHost.indexOf("."))
            if (requestedClient.indexOf("-") > 0) {
                requestedClient = requestedClient.split("-")[0]
            }
            if(requestHost.contains("www")) {
                requestedClient = requestHost.split("\\.")[1]
            }
        }catch(e){
            requestedClient = ''
        }
        requestedClient
    }*/
}
