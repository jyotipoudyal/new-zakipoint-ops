package com.deerwalk.z5

import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class DashBoardChartService {

    def searchService
    def fetchService
    def helperService

    Map setDynamicReportParams(params,session,dates) {
        LinkedHashMap dynamicReportParams = [:]
            dynamicReportParams['reportingFrom'] =params.reportingFrom// dates.getAt('ReportingFirst')
//            def newDate = new Date("${dates.getAt('ReportingSecond').format('MM')}/01/${dates.getAt('ReportingSecond').format('yyyy')}")
//            dynamicReportParams['reportingToFirstDay'] = newDate
            dynamicReportParams['reportingTo'] = params.reportingTo//dates.getAt('ReportingSecond')
//            dynamicReportParams['comparisonFrom'] = dates.getAt('ReportingFirst')
//            dynamicReportParams['comparisonToFirstDay'] = newDate
//            dynamicReportParams['comparisonTo'] = dates.getAt('ReportingSecond')
            dynamicReportParams['basis'] = params.reportingBasis
//            dynamicReportParams['reportingThrough'] = dates.getAt('ReportingSecond')
//            dynamicReportParams['comparisonThrough'] = dates.getAt('ReportingSecond')
//            dynamicReportParams['reportingMonth'] = dates.getAt('ReportingMonth')
            /*dynamicReportParams['phiCSDate'] = "01-01-2010"
            dynamicReportParams['phiCEDate'] ="06-30-2015"*/

//            dynamicReportParams['phiCSDate'] = "10-01-2010"
//            dynamicReportParams['phiCEDate'] ="09-30-2015"
            dynamicReportParams['phiCSDate'] =  params.csDate
            dynamicReportParams['phiCEDate'] = params.ceDate


//        dynamicReportParams['userId'] = session.user.id
//        dynamicReportParams['clientId'] = params.clientId
        dynamicReportParams['clientId'] = helperService.fetchClientId()
        dynamicReportParams['trend'] = params.trend
//        dynamicReportParams['clientId'] = "z5dev"
        dynamicReportParams['udf17Id'] = params.mbrRegionId
//        dynamicReportParams['reportingBasis'] = "PaidDate"
//        dynamicReportParams["loaParams"]  = loaParams
        def eligibilityType = searchService.getNeededEligibilityType(session.client)
        dynamicReportParams['eligibilityType'] = eligibilityType
        dynamicReportParams['reportingPaidThrough'] = "2015-09-30"
        dynamicReportParams['exclude'] = params.exclude
        dynamicReportParams['range'] = params?.range
        dynamicReportParams['isExclude'] = params?.isExclude
        dynamicReportParams['recordTypes'] = ['Medical','Pharmacy','Eligibility','Biometrics','HRA','Episodes','Utilization','Participation'];

        dynamicReportParams.includes=params.includes;
        dynamicReportParams.benchmarkKey=params.benchmarkKey;

        if(!params.showBob)
        dynamicReportParams['groups']= searchService.getGroups()
//        dynamicReportParams["switchedUserId"] = session.switchedUserId
//        dynamicReportParams.locale = session.locale


        return dynamicReportParams
    }


    def getDashBoardChartScript(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        try{
            json = fetchChartData(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);

            rawJSON=parsedJSON.getAt("reporting");
        }catch(e){
            log.error(e)
            log.error('No Data received')
            return '';
        }
        return rawJSON
    }

    def getPopChartScript(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        try{
            json = fetchChartData(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);

            rawJSON=parsedJSON;
        }catch(e){
            log.error(e)
            log.error('No Data received')
            return '';
        }
        return rawJSON
    }

    def getDashBoardChartScriptWBenchmark(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        String module="";
        try{
            dynamicReportParams['includes'] = "benchmark"
            json = fetchChartData(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);
            rawJSON=parsedJSON;
        }catch(e){
            log.error(e)
            log.error('No Data received for population risk')
            return '';
        }
        return rawJSON
    }




    private def fetchChartData(report, params) {
        def module="";
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def parameters
        //def module='dashboard'


        if (params && params['reportingTo'] && params['reportingFrom'] && params['basis'] && params['clientId']) {
            def loaParams = params["loaParams"]
            /*if(session.user.userAccess){
                loaParams = helperService.checkSelectedLoaInUserAccess(loaParams, session.user.userAccess);
            }*/
            if(report.equals('provider:erVisit') || report.equals('provider:snapshot') || report.equals('provider:thirtyDayReadmit')){
//                module = 'provider'
                parameters = "clientId=${params['clientId']}${fetchService.getLoaRequestParameterSectionForProviderDashBoard(loaParams)}"
            }else{
                parameters = "clientId=${params['clientId']}"
            }
            if(params["switchedUserId"]){
                parameters += "&userId="+params["switchedUserId"]
            }
            parameters = parameters + "&reportingBasis=${params['basis']}&reportingTo=${params['reportingTo']}&reportingFrom=${params['reportingFrom']}&report=${report}"
            parameters = parameters + "&eligibilityType=${params.getAt('eligibilityType')}"
            if (params['basis'] == 'ServiceDate') {
//                parameters = parameters + "&reportingPaidThrough=${params['reportingThrough']}"
            }
            if (params['participation']) {
                parameters = parameters + "&isParticipation=${params['participation']}"
                if (params && params['program_name']) {
                    parameters = parameters + "&program_name=${params['program_name']}"
                }
                if (params && params['program_type']) {
                    parameters = parameters + "&program_type=${params['program_type']}"
                }
            }

            if(params['dataView']){
                parameters = parameters + "&dataView=${params['dataView']}"
            }

            if(params['phiCSDate']){
                parameters = parameters + "&phiCSDate=${params['phiCSDate']}"
            }

            if(params['phiCEDate']){
                parameters = parameters + "&phiCEDate=${params['phiCEDate']}"
            }

            if(params && params["trendingField"]){
                parameters = parameters + "&trendingField=${params['trendingField']}"
            }

            if(report=="populationRisk"){
                module="dashboard"
                parameters = parameters + "&recordTypes=${params.getAt('recordTypes')}"
            }
            if(params['udf17Id']){
                parameters = parameters + "&udf17Id=${params['udf17Id']}"
            }

            if(params['includes']){
                parameters = parameters + "&includes=${params['includes']}"
            }

            if(params['benchmarkKey']){
                parameters = parameters + "&benchmarkKey=${params['benchmarkKey']}"
            }

            if(params.getAt('groups')){
                parameters = parameters + "&group=${params.getAt('groups')}"
            }
            if(params.getAt('isExclude')){
                parameters = parameters + "&isExclude=${params.getAt('isExclude')}"
            }
            if(params.getAt('range')){
                parameters = parameters + "&range=${params.getAt('range')}"
            }
            if(params.getAt('exclude')){
                parameters = parameters + "&exclude=${params.getAt('exclude')}"
            }
            if(params.getAt('trend')){
                parameters = parameters + "&trend=${params.getAt('trend')}"
            }
            parameters=parameters+"&dataFetchFrom=true"
//            parameters=parameters+"&includes=benchmark"


            /*def client = Client.findByClientId(params['clientId'])
            def clientRecordTypes = frontEndService.getClientRecordType(client)
            def mainString="&recordTypes="
            StringBuilder arrayElements=new StringBuilder()
            clientRecordTypes.each{
                arrayElements.append(it.toString()).append(",")
            }
            def temp=arrayElements.toString().lastIndexOf(",")
            def finalArrayElements=arrayElements.toString().substring(0,temp)
            mainString=mainString+"["+arrayElements+"]"

            parameters = parameters + mainString

            if (params['cohortId']) {
                parameters = parameters + '&cohortId=' + params['cohortId'];
            }
            if(params['reportConfig']){
                parameters = parameters + '&reportConfig=' + params['reportConfig'];
            }
            if(params['keyField']){
                parameters = parameters + '&keyField=' + params['keyField'];
            }*/

//            println "parameters = $parameters"
//            parameters="clientId=2000&reportingBasis=PaidDate&reportingTo=2015-06-30&reportingFrom=2014-07-01&eligibilityType=[medical]&report=medicalClaimsPmpm&phiCSDate=01-01-2010&phiCEDate=06-30-2015"
            return fetchService.fetchData(parameters, module)
        } else {
            return null
        }
    }
}
