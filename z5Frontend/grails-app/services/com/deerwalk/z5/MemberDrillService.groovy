package com.deerwalk.z5

import org.springframework.web.context.request.RequestContextHolder

class MemberDrillService {

    def searchService
    def fetchService
    def helperService

    def getPopulationExplorer(repDates,active=false){
        def sessions = RequestContextHolder.currentRequestAttributes().getSession()

        def drillMap = [:]
        def repFrom=repDates.priorFrom
        def repTo=repDates.currentTo
        drillMap.put('queryType','query')
      /*drillMap.put('comp.eligibleMonths','Between')
        drillMap.put('eligibleMonths',repFrom)
        drillMap.put('eligibleMonths-to',repTo)*/
        drillMap.put('statusEligibilityType', "[medical]")
        drillMap.put('comp.statusToDate','Greater than equals')
        drillMap.put('comp.statusFromDate','Smaller than equals')
        drillMap.put('statusToDate',repDates.pastMonth)
        drillMap.put('statusFromDate',repTo)
        if(active){
            drillMap.put('currentStatus', "Active")
            commonDrillOverall(drillMap,repFrom,repTo,true)
        }else{
//            drillMap.put('currentStatus', "Overall")
            commonDrillOverall(drillMap,repFrom,repTo)
        }

        drillMap.put("expPop",true)
        return drillMap
    }

    def commonDrillOverall(drillMap,repFrom,repTo,allParams=false){

        drillMap.put('reportingFrom',repFrom)
        drillMap.put('reportingTo',repTo)
        drillMap.put('isZakipoint',"true")


        def group=getGroups()


        if(group){
            if(allParams){
                drillMap.put('groupIdFromDate',repTo)
                drillMap.put('groupIdToDate',repTo)
                drillMap.put('comp.groupIdToDate','Greater than equals')
                drillMap.put('comp.groupIdFromDate','Smaller than equals')
                drillMap.put('groupIdEligibilityType','[medical]')
                drillMap.put('groupId',group[0] )
            }else{
                drillMap.put('groupIdEligibilityType','[medical]')
                drillMap.put('groupId',group[0] )
            }
//            drillMap.putAll(['groupIdFromDate': repTo,  'groupIdEligibilityType': "[medical]",'groupIdToDate':repTo,'groupId':searchService.getGroups()[0] ])
//            drillMap.put('comp',['groupIdToDate':'Greater than equals', 'groupIdFromDate': 'Smaller than equals'])

        }

    }

    protected def getDrillParams(value,isReporting,code,parameters,isParticipating='', programType='', programName='') {

        def drillMap = [:]
        def repFrom = parameters.reportingFrom
        def repTo = parameters.reportingTo
        drillMap.put('queryType','query')
        drillMap.put('comp.eligibleMonths','Between')
        drillMap.put('eligibleMonths',repFrom)
        drillMap.put('eligibleMonths-to',repTo)
        drillMap.put('comp.qmToDate','Greater than equals')
        drillMap.put('comp.qmFromDate','Smaller than equals')
        drillMap.put('qmFromDate',repTo)
        drillMap.put('qmToDate',repTo)
        drillMap.put('totalAmount','0')
        drillMap.put('qmMeasure',value.getAt('name').toString().trim())
        drillMap.put('qmNumerator',(value.getAt('metricType').toString()=="positive")?0:1)
        commonDrill(drillMap,repFrom,repTo,true)


        return drillMap
    }

    protected def getRealTimeDrillParamsForQm(value,isReporting,code,parameters,isParticipating='', programType='', programName='') {

      /*  http://localhost:8080/DasTest/memberSearch?table=msZakipoint&page=1&pageSize=15&clientId=3078&range=0
         &isExclude=false&reportingTo=2017-01-31&reportingFrom=2016-02-01&isZakipoint=true&phiCSDate=02-01-2012
        &phiCEDate=01-31-2017&requestIndex=1&order=totalPaidAmount:desc
        &domainTask=gapInCareRealtime
        &task=reportFromDomain
        &groupId=STUTSMANS
        &qmName=LDL<100mg/dL  //pull this from description in the sample response.
        &criteriaMetFlag=0

*/
        def drillMap = [:]
        def repFrom = parameters.reportingFrom
        def repTo = parameters.reportingTo
        drillMap.put('reportingFrom', repFrom)
        drillMap.put('reportingTo', repTo)
        drillMap.put('domainTask', "gapInCareRealtime")
        drillMap.put('task', "reportFromDomain")
        drillMap.put('qmId', value.qmId)
        drillMap.put('criteriaMetFlag', "0")
        def group=getGroups()
        drillMap.put('groupName',group[0])

//        commonDrill(drillMap,repFrom,repTo,true)


        return drillMap
    }

    def getDiabetesCostsDrillParams(value,isReporting,code,paramaters,isParticipating='', programType='', programName='') {

        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','query')
        drillMap.put('comp.eligibleMonths','Between')
        drillMap.put('eligibleMonths',repFrom)
        drillMap.put('eligibleMonths-to',repTo)
        drillMap.put('comp.qmToDate','Greater than equals')
        drillMap.put('comp.qmFromDate','Smaller than equals')
        drillMap.put('qmFromDate',repTo)
        drillMap.put('qmToDate',repTo)
        drillMap.put('qmMeasure',value.getAt('chronicInfo').getAt("description").toString().trim())

        drillMap.put('qmNumerator','[1,0]')
        commonDrill(drillMap,repFrom,repTo,true)
        return drillMap
    }

    def getRisksDrillParams(value,isReporting,code,paramaters,isParticipating) {

        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','query')
        drillMap.put('comp.eligibleMonths','Between')
        drillMap.put('eligibleMonths',repFrom)
        drillMap.put('eligibleMonths-to',repTo)
        drillMap.put('comp.qmToDate','Greater than equals')
        drillMap.put('comp.qmFromDate','Smaller than equals')
        drillMap.put('qmFromDate',repTo)
        drillMap.put('qmToDate',repTo)
        drillMap.put('qmMeasure',value.getAt("metricDescription").toString().trim())

        drillMap.put('qmNumerator','[1,0]')
        commonDrill(drillMap,repFrom,repTo,true)

        value.members_drill=drillMap

        def drill2 = drillMap.clone();
        drill2.put('queryType','dataSearchQuery')
        drill2.put('report','popilationRiskZakipoint')
        drill2.put('threeOrOne','one')
        drill2.put('chronicCode',value.getAt("metricCode"))
        value.members_drill2=drill2


        def drill3 = drillMap.clone();
        drill3.put('queryType','dataSearchQuery')
        drill3.put('report','popilationRiskZakipoint')
        drill3.put('threeOrOne','three')
        drill3.put('chronicCode',value.getAt("metricCode"))
        value.members_drill3=drill3

    }

    def getDiabetesAlongOtherChronicDrillParams(value,isReporting,code,paramaters,isParticipating='', programType='', programName='') {

        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','query')
        drillMap.put('comp.eligibleMonths','Between')
        drillMap.put('eligibleMonths',repFrom)
        drillMap.put('eligibleMonths-to',repTo)
        drillMap.put('comp.qmToDate','Greater than equals')
        drillMap.put('comp.qmFromDate','Smaller than equals')
        drillMap.put('qmFromDate',repTo)
        drillMap.put('qmToDate',repTo)
        drillMap.put('qmMeasure',value.getAt('rootCondition').trim())
        drillMap.put('qmNumerator','[1,0]')
        drillMap.put('chronicCode',value.rootConditionCode)
        if(value.otherConditionDescription=="Only")
        drillMap.put('comorbidCode',"0")
        else
        drillMap.put('comorbidCode',value.otherConditionCode)

        drillMap.put('report','msZakipoint')


        commonDrill(drillMap,repFrom,repTo,true)


        return drillMap
    }

    def getErUtilizationTrendDrillParams(value,isReporting,code,paramaters,includePercentage=false,isParticipating='', programType='', programName='') {
        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','dataSearchQuery')
        if (paramaters.reportingBasis != 'PaidDate') {
            if (isReporting) {
//                drillMap.putAll(['serviceDate': repFrom, 'serviceDate-to': repTo, 'comp.serviceDate': 'Between'])
                drillMap.put('comp.serviceDate','Between')
                drillMap.put('serviceDate',repFrom)
                drillMap.put('serviceDate-to',repTo)
            }
        }

        drillMap.put('comp.paidDate','Between')
        drillMap.put('paidDate',repFrom)
        drillMap.put('paidDate-to',repTo)
        drillMap.put('isPaidStart',"1")
        drillMap.put('totalAmount',"0")
        drillMap.put('comp.totalAmount',"Not equal")
        drillMap.put('comp.erVisit',"Not equal")
        drillMap.put('erVisit',"0")
        drillMap.put('recordTypes',"visitadm")
        drillMap.put('report',"erZakipoint")

        commonDrill(drillMap,repFrom,repTo)

        value.members_drill=drillMap
        def newDrill=drillMap.clone();
        if(includePercentage){
            newDrill.put('avoidableFlag',"1")
            value.members_drill_percent=newDrill
        }
    }


    def getPopulationRiskDashParams(value,isReporting,code,paramaters,includePercentage=false,isParticipating='', programType='', programName='') {

        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo
        def pastMonth = paramaters.pastMonth

        drillMap.put('queryType','query')
        if (paramaters.reportingBasis != 'PaidDate') {
            if (isReporting) {
//                drillMap.putAll(['serviceDate': repFrom, 'serviceDate-to': repTo, 'comp.serviceDate': 'Between'])
                drillMap.put('comp.serviceDate','Between')
                drillMap.put('serviceDate',repFrom)
                drillMap.put('serviceDate-to',repTo)
            }
        }

        drillMap.put('statusEligibilityType','[medical]')
        drillMap.put('currentStatus','Active')
        drillMap.put('comp.statusToDate','Greater than equals')
        drillMap.put('comp.statusFromDate','Smaller than equals')


        drillMap.put('statusToDate',pastMonth)
        drillMap.put('statusFromDate',repTo)


        commonDrill(drillMap, repFrom, repTo,true)



        def v = value.default;

        def highDrill = drillMap.clone();
        def avgDrill = drillMap.clone();
        def normDrill = drillMap.clone();
        def lowDrill = drillMap.clone();

        v.totalSD+=1.01;

        highDrill.put('comp.riskScore', 'Greater than')
        highDrill.put('riskScore', v.totalSD )

        avgDrill.put('comp.riskScore', 'Between')
        avgDrill.put('riskScore', 1.01)
        avgDrill.put('riskScore-to', v.totalSD)


        normDrill.put('comp.riskScore', 'Between')
        normDrill.put('riskScore', v.bottomSD)
        normDrill.put('riskScore-to', 1.0)


        lowDrill.put('comp.riskScore', 'Smaller than')
        lowDrill.put('riskScore', v.bottomSD)



        v.members_high_drill = highDrill
        v.members_avg_drill = avgDrill
        v.members_norm_drill = normDrill
        v.members_low_drill = lowDrill


    }

    def getNewBiometricsDrillParams1(value,isReporting,code,paramaters,includePercentage=false,isParticipating='', programType='', programName='') {
        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','dataSearchQuery')
        if (paramaters.reportingBasis != 'PaidDate') {
            if (isReporting) {
//                drillMap.putAll(['serviceDate': repFrom, 'serviceDate-to': repTo, 'comp.serviceDate': 'Between'])
                drillMap.put('comp.serviceDate','Between')
                drillMap.put('serviceDate',repFrom)
                drillMap.put('serviceDate-to',repTo)
            }
        }


        drillMap.put('comp.measuredDate','Between')
        drillMap.put('measuredDate',repFrom)
        drillMap.put('measuredDate-to',repTo)
        drillMap.put('recordTypes',"Z5Biometric")
        drillMap.put('report',"erZakipoint")
        drillMap.put('useMemberId',"true")

        commonDrill(drillMap,repFrom,repTo)

        //value.members_drill=drillMap
        def levels;
        value.each{bKey,bVal->
            def v=bVal;
            v.levels.each{items->
                def addDrill=drillMap.clone();
                def maxa=v['rangeEnd_'+items];
                def minm=v['rangeBegin_'+items];
                /*def temp=0.0;

                if(maxa<minm){
                    temp=maxa;
                    maxa=minm;
                    minm=temp;
                }*/

                addDrill.put('comp.biometricValue','Between')
                addDrill.put('biometricValue',minm)
                addDrill.put('biometricValue-to',maxa)
                addDrill.put('biometricName',v.biometricName)
                bVal["drill_"+items]=addDrill
            }
        }



    }

    def getNewBiometricsDrillParams(value,drillType,params,period) {


        LinkedHashMap drillMap = [:]
        drillMap['reportingFrom'] =params.reportingFrom
        drillMap['reportingTo'] = params.reportingTo
        drillMap['basis'] =params.reportingBasis
        drillMap['eligibilityType'] = "[medical]"
        drillMap['domainTask'] = params?.domainTask
        drillMap['task'] = params?.task
        drillMap['drillType'] = drillType

        //value.members_drill=drillMap

        value.each{bKey,bVal->
            def v=bVal;
            v.levels.each{items->
                def addDrill=drillMap.clone();

                if(drillType.equals('sediment')){
                    addDrill['period'] = period;
                    addDrill['level'] = items;
                    addDrill['biometricName'] = v?.biometricId
                }
                else{
//                    addDrill['source'] = data?.source
//                    addDrill['target'] = data?.target
                }

                bVal["drill_"+items]=addDrill
            }
        }



    }



    Map setSankeyParams(value,isReporting,drillType,paramaters){







        return dynamicReportParams;



    }



    def getBiometricsDrillParams(value,isReporting,code,paramaters,includePercentage=false,isParticipating='', programType='', programName='') {
        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','dataSearchQuery')
        if (paramaters.reportingBasis != 'PaidDate') {
            if (isReporting) {
//                drillMap.putAll(['serviceDate': repFrom, 'serviceDate-to': repTo, 'comp.serviceDate': 'Between'])
                drillMap.put('comp.serviceDate','Between')
                drillMap.put('serviceDate',repFrom)
                drillMap.put('serviceDate-to',repTo)
            }
        }


        drillMap.put('comp.measuredDate','Between')
        drillMap.put('measuredDate',repFrom)
        drillMap.put('measuredDate-to',repTo)
        drillMap.put('recordTypes',"Z5Biometric")
        drillMap.put('report',"erZakipoint")
        drillMap.put('useMemberId',"true")

        commonDrill(drillMap,repFrom,repTo)

        //value.members_drill=drillMap

        value.each{
            def v=it.value;

            def highDrill=drillMap.clone();
            def medDrill=drillMap.clone();
            def lowDrill=drillMap.clone();

            highDrill.put('comp.biometricValue','Greater than')
            highDrill.put('biometricValue',v.endRange)
            highDrill.put('biometricName',v.description)

            medDrill.put('comp.biometricValue','Between')
            medDrill.put('biometricValue',v.beginRange)
            medDrill.put('biometricValue-to',v.endRange)
            medDrill.put('biometricName',v.description)


            lowDrill.put('comp.biometricValue','Smaller than')
            lowDrill.put('biometricValue',v.beginRange)
            lowDrill.put('biometricName',v.description)



            it.value.members_high_drill=highDrill
            it.value.members_med_drill=medDrill
            it.value.members_low_drill=lowDrill
        }

    }

    def getRxHistoricPredictiveDrillParams(value,isReporting,code,paramaters,includePercentage="",isParticipating='', programType='', programName='') {
        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo
        drillMap.put('queryType','dataSearchQuery')

        if (paramaters.reportingBasis != 'PaidDate') {
            if (isReporting) {
//                drillMap.putAll(['serviceDate': repFrom, 'serviceDate-to': repTo, 'comp.serviceDate': 'Between'])
                drillMap.put('comp.serviceDate','Between')
                drillMap.put('serviceDate',repFrom)
                drillMap.put('serviceDate-to',repTo)
            }
        }

        drillMap.put('comp.paidDate','Between')
        drillMap.put('paidDate',repFrom)
        drillMap.put('paidDate-to',repTo)
        drillMap.put('recordTypes',"srx")
        drillMap.put('report',"erZakipoint")

        commonDrill(drillMap,repFrom,repTo)

        value.members_drill=drillMap

        def newDrill=drillMap.clone();

        if(includePercentage=="brand"){
            newDrill.put('genericFlag',"N")
            newDrill.put('comp.udf30_pharmacy',"Exists")
            newDrill.put('udf30_pharmacy',"yes")
            value.members_drill_percent=newDrill
        }else if(includePercentage=="mail"){
            newDrill.put('comp.mailRetailSavings',"Greater than equals")
            newDrill.put('mailRetailSavings',"0")
            value.members_drill_percent=newDrill

        }else if(includePercentage=="speciality"){
            newDrill.put('udf5_pharmacy',"S")
            value.members_drill_percent=newDrill

        }
    }

    def cohortDrill(data,params,allParams=false) {
        def drillMap = [:]
        def repFrom = params.reportingFrom
        def repTo = params.reportingTo
        drillMap.put('reportingFrom', repFrom)
        drillMap.put('reportingTo', repTo)
        drillMap.put('cohortKey', data.cohortId)

        data.members_drill=drillMap


    }


    def commonDrill(drillMap,repFrom,repTo,allParams=false){

        drillMap.put('reportingFrom',repFrom)
        drillMap.put('reportingTo',repTo)
        drillMap.put('isZakipoint',"true")


        def group=getGroups()


        if(group){
            if(allParams){
                drillMap.put('groupIdFromDate',repTo)
                drillMap.put('groupIdToDate',repTo)
                drillMap.put('comp.groupIdToDate','Greater than equals')
                drillMap.put('comp.groupIdFromDate','Smaller than equals')
                drillMap.put('groupIdEligibilityType','[medical]')
                drillMap.put('groupId',group[0] )
            }else{
                drillMap.put('groupId',group[0] )
            }
//            drillMap.putAll(['groupIdFromDate': repTo,  'groupIdEligibilityType': "[medical]",'groupIdToDate':repTo,'groupId':searchService.getGroups()[0] ])
//            drillMap.put('comp',['groupIdToDate':'Greater than equals', 'groupIdFromDate': 'Smaller than equals'])
        }

    }


    def getChronicMemberUseDrillParams(value,isReporting,code,paramaters,includePercentage=false,isParticipating='', programType='', programName='') {


        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','dataSearchQuery')
        if (paramaters.reportingBasis != 'PaidDate') {
            if (isReporting) {
                drillMap.put('comp.serviceDate','Between')
                drillMap.put('serviceDate',repFrom)
                drillMap.put('serviceDate-to',repTo)
            }
        }

        drillMap.put('comp.paidDate','Between')
        drillMap.put('paidDate',repFrom)
        drillMap.put('paidDate-to',repTo)
        drillMap.put('isPaidStart',"1")
        drillMap.put('totalAmount',"0")
        drillMap.put('comp.totalAmount',"Not equal")
        drillMap.put('comp.erVisit',"Not equal")
        drillMap.put('erVisit',"0")
        drillMap.put('recordTypes',"visitadm")
        drillMap.put('report',"erZakipoint")
        drillMap.put('chronicCode',value.total?.chronicCode)
        drillMap.put('feature',"useOfErByChronicMembers")

        commonDrill(drillMap,repFrom,repTo)

        value['total'].members_drill=drillMap

    }


    def getRxBrandMemberUseDrillParams(value,isReporting,code,paramaters,includePercentage=false,isParticipating='', programType='', programName='') {

        def drillMap = [:]
        def repFrom = paramaters.reportingFrom
        def repTo = paramaters.reportingTo

        drillMap.put('queryType','dataSearchQuery')
        if (paramaters.reportingBasis != 'PaidDate') {
            if (isReporting) {
                drillMap.put('comp.serviceDate','Between')
                drillMap.put('serviceDate',repFrom)
                drillMap.put('serviceDate-to',repTo)
            }
        }

        drillMap.put('comp.paidDate','Between')
        drillMap.put('paidDate',repFrom)
        drillMap.put('paidDate-to',repTo)
        drillMap.put('brandName',value.drugName)
        drillMap.put('recordTypes',"srx")
        drillMap.put('report',"erZakipoint")

        if(includePercentage=="rxSpecialityCostDetail") drillMap.put('udf5_pharmacy',"S")


        commonDrill(drillMap,repFrom,repTo)


        value.members_drill=drillMap

    }

    def getGroups(){
        def sessions = RequestContextHolder.currentRequestAttributes().getSession()
        def arrayElement = []
        sessions.group.each{key,val->
            if(val=="1"){
                arrayElement.add(key)
            }
        }
        return arrayElement
    }





}
