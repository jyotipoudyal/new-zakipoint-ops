package com.deerwalk.z5

import com.z5.Module
import com.z5.RecordType


class UrlMappingService {

    def grailsApplication


    String getModuleUrl(module){
        String defaultURL = grailsApplication.config.grails.backEndURL
//        String defaultURL = "http://192.168.1.102:8080/DasTest/"
        String url = defaultURL + "memberSearch"
        if(module==Module.DynamicReport || module == 'highCostMember' ||module == 'membershipDistribution' ){
            url = defaultURL+"esReport"
        }else if (module == Module.QualityOfCare) {
            url = defaultURL+"report"
        }else if (module == "Zakipoint") {
            url = defaultURL+"zakiPoint"
        }else if(module == "cohort"){
            url = defaultURL+"zakiPoint/cohort"
//        }else if(module == 'networkComparison' || module == 'erDiagnosisReport'){
        }else if(module == "cohortTrack"){
            url = defaultURL+"zakiPoint/cohortTrack"
        }else if(module == "reportFromDomain"){
            url = defaultURL+"zakiPoint/reportFromDomain"
        }else if(module == 'report'){
            url = defaultURL+"report"
        }else if(module == 'dccChronic' ){
            url = defaultURL+"dcc"
        }else if(module==Module.MemberSearch || module==Module.MemberProfile || module == Module.CustomReport || module==Module.RiskAnalysis || module == 'patientSearch' || module=='ParticipationPTPN'){
            url = defaultURL + "memberSearch"
        }else if(module==Module.QualityOfCareDrillDown){
            url = defaultURL+"esReport"+'/esDrill'
        }else if (module == Module.QualityMetricDrillDown) {
            url = defaultURL+"summaryDrillDown"
        }else if (module == Module.AutoComplete){
            url = defaultURL + "memberSearch"+ '/autoComplete'
        }else if (module == 'OutcomeReport') {
            url = defaultURL+"ddhc"
        }else if (module == 'summation'){
            url = defaultURL + "memberSearch"+ '/summation'
        }else if(module == 'trendingComparison'){
            url = defaultURL+"trendingReport"
        }else if(module == 'selectedProcedure'){
            url = defaultURL+"report"
        }else if(module == 'version'){
            url = defaultURL + "memberSearch"+ '/version'
        }else if(module == 'accoladeReport'){
            url = defaultURL+"accoladeReport"+ '/report'
        }else if(module == 'expenseDistributionDrillDown'){
            url = defaultURL+"esReport"+'/esDrill'
        }else if (module == 'memberDistributionDrill') {
            url = defaultURL+"summaryDrillDown"+'/esDrill'
        }else if (module == 'BusinessLevel') {
            url = defaultURL + "memberSearch"+ '/loa'
        }else if (module == 'cohortCreate') {
            url = defaultURL+"cohort"+ '/create'
        }else if (module == 'cohortSearch') {
            url = defaultURL+"cohort"+ '/search'
        }else if (module == 'cohortUpdate') {
            url = defaultURL+"cohort"+'/update'
        }else if (module == 'cohortDelete') {
            url = defaultURL+"cohort"+'/delete'
        }else if (module == 'cohortSync') {
            url = defaultURL+"cohort"+'/sync'
        }else if (module == 'cohortReprocess') {
            url = defaultURL+"cohort"+'/reprocess'
        }else if (module == 'monitorStatus') {
            url = defaultURL+"cohort"+'/monitorStatus'
        }else if (module == 'cohortMigrate') {
            url = defaultURL+"cohort"+'/migrate'
        }else if (module == 'cohortRefreshMemberId') {
            url = defaultURL+"cohort"+'/refreshMemberId'
        }else if (module == 'clusterHealth') {
            url = defaultURL + "clusterHealth"
        }else if (module == 'saveClaim') {
            url = defaultURL+'insert'
        }else if (module == 'updateClaim') {
            url = defaultURL+'update'
        }else if(module=='contractReport'){
            url = defaultURL + 'contractReport'
        }else if(module=='CCEReports'){
            url = defaultURL+"cceReport"
        }else if (module == 'dashboard') {
            url = defaultURL+"dashboard"
        }else if (module == 'loadTables') {
            url = defaultURL + "memberSearch"+ '/loadTables'
        }else if(module=='dcc'){
            url = defaultURL + "dcc"
        }else if(module=='provider'){
            url = defaultURL + "provider"
        }else if(module=='indexBackupProcess'){
            url = defaultURL + "cohort/copyToBackup"
        }else if(module=='programEvaluator'){
            url=defaultURL+'report'
        }else if(module.toString().startsWith('ClusterSwitch')){
            def changeServerContextUrlSplit = module.toString().split("-");
            url=grailsApplication.config.grails.dasips.getAt(changeServerContextUrlSplit[1])+'ClusterConfig/clusterSwitch'
        }else if(module=='clientSetting'){
            url=defaultURL+'dasClientSetting/updateClientSetting'
        }else if(module=='getBenchmark'){
            url=defaultURL+'dasClientSetting/getClientBenchmarkingSetting'
        }else if(module=='benchmark'){
            url=defaultURL+'das/benchmark'
        }else if(module.toString().startsWith('changeServerContext')){
            def changeServerContextUrlSplit =module.toString().split("-");
            url=grailsApplication.config.grails.dasips.getAt(changeServerContextUrlSplit[1])+'changeServerContext'
        }else if(module=="AWSFetchReport"){
            url = defaultURL + "AWSFileManager/fetchReport"
        }else if(module=="AWSDownloadUrl"){
            url = defaultURL + "AWSFileManager/presignedUrl"
        }else if(module=='AWSDelete'){
            url = defaultURL + "AWSFileManager/delete"
        }else if(module=='AWSSaveMetaData'){
            url=defaultURL+'AWSFileManager/saveMetaData';
        }else if(module=='dynamicHighCost'){
            url = defaultURL+"report"
        }else if(module=='benefitPlan'){
            url = defaultURL+"holmesMurphy"
        }else if(module=='afterDownload'){
            url = defaultURL+"AWSFileManager/afterDownload"
        }else if(module=='memberSyncRequest'){
            url = defaultURL+"esCrud"+"/sync"
        }else if(module=='monitorSyncRequest'){
            url = defaultURL+"esCrud"+"/monitorSync"
        }
        return url
    }

    String getBackendTableForRecordType(recordType){
        String table = 'ms'
        if (recordType == RecordType.Medical.getId()) {
            table = 'smc'
        } else if (recordType == RecordType.Eligibility.getId()) {
            table = 'seg'
        } else if (recordType == RecordType.Lab.getId()) {
            table = 'sl'
        } else if (recordType == RecordType.Case_Audit.getId()) {
            table = 'smc'
        } else if (recordType == RecordType.Pharmacy.getId()) {
            table = 'srx'
        } else if (recordType == RecordType.Demographics.getId()) {
            table = 'dem'
        }else if (recordType == RecordType.Regimen_Usage.getId()) {
            table = 'reg'
        }else if (recordType == RecordType.Care_Plan.getId()){
            table = 'pof'
        }else if (recordType == RecordType.Diagnosis.getId()){
            table = 'diag'
        }else if (recordType == RecordType.Lab_Results.getId()){
            table = 'labres'
        }else if (recordType == RecordType.Biometrics.getId()){
            table = 'biometrics'
        }else if (recordType == RecordType.HRA.getId()){
            table = 'hra'
        }else if (recordType == RecordType.KYN.getId()){
            table = 'kyn'
        }else if (recordType == RecordType.Participation.getId()){
            table = 'participation'
        }else if (recordType == 'providerSearch') {
            table = 'prs'
        } else if (recordType == 'memberClaims') {
            table = '[smc,srx]'
        } else if (recordType == 'memberProfile') {
            table = 'emp'
        } else if (recordType == 'memberProfileForCareAlerts') {
            table = 'mpra'
        }else if (recordType == 'riskAnalysisIndex' || recordType == 'groupRiskDistribution' || recordType == 'groupRiskDistributionForAges') {
            table = null
        } else if (recordType == 'riskMemberDrillDown') {
            table = 'rad'
        }else if (recordType == 'physicianSearch') {
            table = 'ps'
        }else if (recordType == 'patientSearch') {
            table = 'pas'
        }else if (recordType == 'PharmacyReport') {
            table = 'phr'
        }else if (recordType == 'PhysicianSearchDrilldown') {
            table = 'zzPhysicianSearh'
        }else if (recordType == RecordType.Workers_Comp.getId()){
            table = 'wrc'
        }else if (recordType == RecordType.Vision.getId()){
            table = 'vision'
        }else if (recordType == RecordType.Dental.getId()){
            table= 'dental'
        }else if(recordType==RecordType.Contracts.getId()){
            table = 'ctrct'
        }else if(recordType==RecordType.Vendor.getId()){
            table = 'vendor'
        }else if(recordType==RecordType.Utilization.getId() || recordType==RecordType.ER_Visit.getId()){
            table = 'visitadm'
        }else if(recordType==RecordType.OSHA.getId()){
            table = 'osha'
        }else if(recordType == 'cohort'){
            table = 'cohort'
        }else if(recordType == RecordType.Episodes.getId()){
            table = 'meg'
        }else if(recordType == 'memberSearchParticipation'){
            table = 'mpraParticipation'
        }else if(recordType == 'procedureSubGrouper'){
            table = 'masterprocedure'
        }else if(recordType == 'diagnosisGrouperCode' || recordType == 'diagnosisGrouperId1' || recordType == 'PEdiagnosisGrouperCode'){
            table = 'masterdiagnosis'
        }else if(recordType == 'EMR_Lab'){
            table = 'LabResult'
        }else if(recordType == 'Vitals'){
            table = 'Vital'
        }else if(recordType == 'DiagnosisEMR'){
            table = 'DiagnosisEmr'
        }else if(recordType == 'ProcedureEMR'){
            table = 'ProcedureEmr'
        }else if(recordType == 'PrescriptionEmr'){
            table = 'PrescriptionEmr'
        }else if(recordType == 'AppointmentsEmr'){
            table = 'AppointmentEmr'
        }else if(recordType == 'VisitEmr'){
            table = 'VisitEmr'
        }else if(recordType == 'Disability'){
            table = 'disability'
        }else if(recordType == 'ChronicCondition'){
            table = 'chronic'
        }else if(recordType == 'Immunization'){
            table = 'immunization'
        }else if(recordType == 'Allergy'){
            table = 'allergy'
        }else if(recordType == 'ExamScreening'){
            table = 'examscreening'
        }
        return table
    }
}
