package com.deerwalk.z5

import report.FaqReport
import report.GapInCareReport
import report.HighOutNetworkUsageReport
import report.ReportGenerator
import report.TopOonProviderLocationReport
import report.TopOonProviderSpecialityReport
import report.TopOutOfNetworkProviderReport

class DynamicParamsGeneratorService {

    def fetchService
    def exportService
    def helperService
    def searchService

    Map setCohortParams(params,session,dates) {
        LinkedHashMap dynamicReportParams = [:]
        dynamicReportParams['domain'] =params.domain
        dynamicReportParams['clientId'] = helperService.fetchClientId()
        if(!params.comparatorId)
        dynamicReportParams['groups']= searchService.getGroups()
        dynamicReportParams['showInIdentity']= params.showInIdentity
        dynamicReportParams['comparatorId']= params.comparatorId
        return dynamicReportParams
    }

    Map setHighOutNetworkReportParams(params,session,dates) {
        LinkedHashMap dynamicReportParams = [:]
        dynamicReportParams['reportingFrom'] =params.reportingFrom
        dynamicReportParams['reportingTo'] = params.reportingTo
        dynamicReportParams['basis'] =params.reportingBasis
        dynamicReportParams['phiCSDate'] = params.csDate
        dynamicReportParams['phiCEDate'] =params.ceDate
        dynamicReportParams['udf17Id'] =params.regional
        dynamicReportParams['page'] =params.page
        dynamicReportParams['pageSize'] =params.pageSize
        dynamicReportParams['clientId'] = helperService.fetchClientId()
        dynamicReportParams['eligibilityType'] =  "[medical]"
        dynamicReportParams['reportingPaidThrough'] = params.csDate
        dynamicReportParams['order'] = params.sortOrder
        dynamicReportParams['brandName'] = params.brandName
        dynamicReportParams['exclude'] = params.exclude
        dynamicReportParams['range'] = params?.range
        dynamicReportParams['isExclude'] = params?.isExclude
        dynamicReportParams['category'] = params?.category
//        dynamicReportParams['metricCode'] = params?.metricCode
        dynamicReportParams['biometricName'] = params?.biometricName
        dynamicReportParams['groups']= searchService.getGroups()
        return dynamicReportParams
    }





    Map setSankeyParams(params,data,drillType){


        LinkedHashMap dynamicReportParams = [:]
        dynamicReportParams['reportingFrom'] =params.reportingFrom
        dynamicReportParams['reportingTo'] = params.reportingTo
        dynamicReportParams['basis'] =params.reportingBasis
        dynamicReportParams['eligibilityType'] = "[medical]"
        dynamicReportParams['domainTask'] = params?.domainTask
        dynamicReportParams['task'] = params?.task
        dynamicReportParams['drillType'] = drillType

        if(drillType.equals('sediment')){
            dynamicReportParams['period'] = data?.year
            dynamicReportParams['level'] = data?.name
            dynamicReportParams['biometricName'] = params?.biometricName

        }
        else{
            dynamicReportParams['source'] = data?.source
            dynamicReportParams['target'] = data?.target
            dynamicReportParams['biometricName'] = params?.biometricName

        }

        return dynamicReportParams;



    }




    ReportGenerator getReportGeneratorInstance(reportId, params, dynamicReportParams) {
        ReportGenerator rGenerator
        switch (reportId) {
            case "gapInCare":
//                rGenerator = new GapInCareReport.ReportGeneratorBuilder(dynamicReportParams, params).build();
                rGenerator = new GapInCareReport.ReportGeneratorBuilder(dynamicReportParams, params).build();
                break;
            case "highOutNetworkUsage":
                rGenerator = new HighOutNetworkUsageReport.ReportGeneratorBuilder(dynamicReportParams, params).build();
                break;
            case "topOutOfNetworkProvider":
                rGenerator = new TopOutOfNetworkProviderReport.ReportGeneratorBuilder(dynamicReportParams, params).build();
                break;
            case "topOonProviderLocation":
                rGenerator = new TopOonProviderLocationReport.ReportGeneratorBuilder(dynamicReportParams, params).build();
                break;
            case "topOonProviderSpeciality":
                rGenerator = new TopOonProviderSpecialityReport.ReportGeneratorBuilder(dynamicReportParams, params).build();
                break;
            case "faq":
                rGenerator = new FaqReport.ReportGeneratorBuilder(dynamicReportParams, params).build();
                break;

        }

        return rGenerator
    }



}
