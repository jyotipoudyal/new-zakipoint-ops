package com.deerwalk.z5

import com.deerwalk.security.DwClient
import com.deerwalk.security.UserDwClient
import com.deerwalk.security.UserPassword

class AccountSettingsService {


    public def assignedRolesToLoggedUser() {
        com.deerwalk.security.User.findById(Long.parseLong(springSecurityService.principal.id.toString())).authorities*.authority
    }

    public def updateUserPassword(com.deerwalk.security.User user, String password){
        new UserPassword(user: user, password:password.encodeAsMD5()).save(flush: true,failOnError: true)
    }

   /* public boolean isOldPassword(String clientName,com.deerwalk.security.User user, String password){
        def maxNoOfPassword = DwClient.findByName(clientName).maxNoOfPasswords
        def oldPasswordList = UserPassword.findAllByUser(user,[max: maxNoOfPassword, sort: "id", order: "desc"])
        return oldPasswordList*.password.contains(password.encodeAsMD5())
    }

    public boolean isMaximumPasswordAge(com.deerwalk.security.User user){
        def passwordAge            = UserDwClient.executeQuery("select max(dw.passwordAge)from UserDwClient udc inner join udc.clientDw dw where udc.userDw=?", [user])[0]
        def maxUpdatedPasswordDate = UserPassword.executeQuery("select max(dateCreated) from UserPassword where user=? ",[user])[0]
        def duration
        if (maxUpdatedPasswordDate){
            use(groovy.time.TimeCategory) { duration = new Date() - maxUpdatedPasswordDate }
            return duration.days < passwordAge
        }else {
            return false
        }
    }*/
}
