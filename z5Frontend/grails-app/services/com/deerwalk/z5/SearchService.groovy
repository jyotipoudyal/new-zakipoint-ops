package com.deerwalk.z5

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.context.request.RequestContextHolder

class SearchService {

    def urlMappingService
    def frontEndService
    def helperService
    static transactional = false
    List queryOperators =  ['and','or','not']


    def getNeededEligibilityType(client){
        def arrayElement = []
        arrayElement.add('medical')
        /*def clientRecordType = frontEndService.getClientRecordType(client)
        if(clientRecordType.contains(RecordType.Vision)){
            arrayElement.add('vision')
        }
        if(clientRecordType.contains(RecordType.Dental)){
            arrayElement.add('dental')
        }*/
        return arrayElement
    }

    def getGroups(){
        def sessions = RequestContextHolder.currentRequestAttributes().getSession()
        def arrayElement = []
        sessions.group.each{key,val->
            if(val=="1"){
                arrayElement.add(key)
            }
        }
        return arrayElement
    }


    String createSearchQuery(params,map,  pageSize, pageNumber, client, recordType, module, backendTable=null,queryType,sort,order){
//        println "pageSize = $pageSize"
//        println "pageNumber = $pageNumber"
        params = params.clone()
        String parameters = "table=${backendTable}"
        if (pageNumber) {
            parameters = parameters + "&page=${pageNumber}&pageSize=${pageSize}"
        }
        if (client && client != '') {
            parameters = parameters + "&clientId=${client}"
        }

        parameters = addNecessaryParameters(parameters,params)
        parameters = excludeFromQuery(parameters,params,client)
        if (client){
//            forceParameterValuesForSearch(params,client,module)
        }
        String queryPortion = generateAdvancedQuery(params)
        if (queryPortion!="")
            parameters = parameters + "&${queryType}=${queryPortion}"

//        parameters = parameters + "&forwardToMemberSearch=yes"
//        parameters = parameters + "&phiCSDate=10-01-2010"
//        parameters = parameters + "&phiCEDate=09-30-2015"

        map.each{k,v->
            parameters = parameters + "&${k}=${v}"
        }
        /*parameters = parameters + "&reportingTo="+map['reportingTo']
        parameters = parameters + "&reportingFrom="+map['reportingFrom']
        parameters = parameters + "&range="+map['range']
        parameters = parameters + "&reportingBasis="+map['reportingBasis']*/
        parameters = parameters + "&order=${sort}:${order}"
        return parameters
    }



    public Object processQuery(Object obj,client=null) {
        if (obj instanceof JSONObject) {
            return processMap((JSONObject) obj,client);
        } else if (obj instanceof JSONArray) {
            return processList((JSONArray) obj,client);
        } else {
            return obj;
        }
    }

    public JSONArray processList(JSONArray list,client=null) {
        final JSONArray newList = new JSONArray();
        for (Object o : list) {
            Object listRet = processQuery(o,client);
            try{
                if(listRet.keySet().toList().contains('currentStatus.eq')){
                    def params = [currentStatus: listRet['currentStatus.eq']]
                    includeCurrentStatusDate(params,client)
                    includeParticipationParams(params,client)
                    String queryPortion = generateAdvancedQuery(params)
                    def reqMap = JSON.parse(queryPortion).getAt('and')[0]//.getAt('and')
                    newList.addAll(reqMap)
                    listRet = null
                }
            }catch (e){
                e.printStackTrace()
            }
            if (listRet != null) {
                newList.add(listRet);
            }
        }
        return newList;
    }



    def buildQuerySection(params,operator,parentRoot,parentOperator){
        if(params['selected_pcp']){
            params['udf23Id'] = params['selected_pcp']
            params.remove('selected_pcp')
        }
        def comparator = params["comp"]
        Map root = [:]
        List operators = queryOperators
        String grouperPrefix = "grp_"
        params.each{key,val->
            //Handles operators like and or not
            if(operators.contains(key)){
                def branch = buildQuerySection(val,key,root,operator)
                if ( branch && branch.size()>0){
                    if (root[operator]){
                        root[operator].add(branch)
                    }else{
                        root[operator] = [branch]
                    }
                }
            }else{

                //Handles actual fields
                if(!key.contains(".") && !key.contains(grouperPrefix) && !key.equals("comp") && !key.equals("andOr") && !key.toString().endsWith("-to")){
                    def row = [:]
                    //println "COMP" +  translateComparatorString(comparator[key])
                    String keyWithComp
                    String compKey = "eq"
                    if (comparator){//Handles Comparators
//                        println "compKey = "+comparator[key]
//                        println "key = "+key
                        compKey = translateComparatorString(comparator[key])
                        //Handles Between
                        if (compKey.equals("between") && params["${key}-to"] && params["${key}-to"]!=""){
                            val = [val,params["${key}-to"]]
                        }else if (params.getAt('andOr')?.getAt(key)=="and"){
//                            val = frontEndService.getTrimmedArrayStringWithAnd(key,val)
                        }
                        if (comparator[key]=="and"){
                            keyWithComp = "${key}.eq"
                        }else{
                            keyWithComp = "${key}.${compKey}"
                        }

                    }else{
                        keyWithComp = "${key}.${compKey}"
                    }

                    val = val.toString().decodeURL()
                    row.put(keyWithComp,val.encodeAsURL())
                    if (root[operator]){
                        root[operator].add(row)
                    }else{
                        root[operator] = [row]
                    }
                }else{
                    //Handles field groupers
                    if (key.toString().startsWith(grouperPrefix) && key.toString().count(".")==0){
                        def branch = buildQuerySection(val,operator,root,null)
                        if (branch && branch.size()>0){
                            if (parentRoot!=null && parentOperator){
                                if (parentRoot[parentOperator]){
                                    parentRoot[parentOperator].add(branch)
                                }else{
                                    parentRoot[parentOperator] = [branch]
                                }
                            }else{
                                if (root[operator]){
                                    root[operator].add(branch)
                                }else{
                                    root[operator] = [branch]
                                }
                            }
                        }
                    }
                }
            }
        }
        return root
    }


    String translateComparatorString(String frontCompString){
        String result = "eq"
        if (!frontCompString){
            return result;
        }
        switch (frontCompString.decodeURL()) {
            case "Equals":
                result = "eq"
                break
            case "Between":
                result = "between"
                break
            case "Smaller than":
                result = "lt"
                break
            case "Greater than":
                result = "gt"
                break
            case "Smaller than equals":
                result = "lte"
                break
            case "Greater than equals":
                result = "gte"
                break
            case "Not equal":
                result = "neq"
                break
            case "Contains":
                result = "contains"
                break
            case "and":
                result = "and"
                break
            case "Missing":
                result = "missing"
                break
            case "Contain":
                result = "contain"
                break;
            case "Exists":
                result = "exists"
                break
            default:
                result ="eq"
                break
        }
        return result
    }

    String translateComparatorStringReverse(String frontCompString){
        String result = "Equals"
        if (!frontCompString){
            return result;
        }
        switch (frontCompString.decodeURL()) {
            case "eq":
                result = "Equals"
                break
            case "between":
                result = "Between"
                break
            case "lt":
                result = "Smaller than"
                break
            case "gt":
                result = "Greater than"
                break
            case "lte":
                result = "Smaller than equals"
                break
            case "gte":
                result = "Greater than equals"
                break
            case "neq":
                result = "Not equal"
                break
            case "contains":
                result = "Contains"
                break
            case "and":
                result = "and"
                break
            case "missing":
                result = "Missing"
                break
            default:
                result ="Equals"
                break
        }
        return result
    }



    String generateAdvancedQuery(params) {
        def cleanedParams = cleanParams(params)
        def result =  buildQuerySection(cleanedParams,'and',null,null)
        String returnStr = ""
        if(result.size()>0){
            returnStr = (result as  grails.converters.JSON).toString()
        }
        return returnStr
    }

    def cleanParams(params){
        def dupParams = params.clone()
        def cleanParams = [:]
        dupParams.remove('action')
        dupParams.remove('controller')
        dupParams.remove('button')
        dupParams.remove('recordType')
        dupParams.remove('id')
        dupParams.remove('ajax')
        dupParams.remove('page')
        dupParams.remove('pageSize')
        dupParams.remove('ascdsc')
        dupParams.remove('isAjax')
        dupParams.remove('format')
        dupParams.remove('module')
        dupParams.remove('exportLimit')
        dupParams.remove('parameters')
        dupParams.remove('cohortCompare')
        def excludeQuery=dupParams.remove('excludeQuery');
        excludeQuery.each{key,val ->
            if(val && val!=''){
                cleanParams["excludeQuery.${key}"]=val
            }
            dupParams.remove("excludeQuery.${key}");
        }
        dupParams.each {key, val ->
            if (!key.contains('multiselect')) {
                if (val && val != '') {
                    cleanParams[key] = val
                }
            }
        }
        cleanParams.sort{it.key}
        return cleanParams
    }

    public JSONObject processMap(JSONObject map,client=null) {
        JSONObject newMap = new JSONObject();
        for (Object s : map.keySet()) {
            String key = (String) s;
            Object val;
            val = processQuery(map.get(key),client);
            newMap.put(key, val);
        }
        return newMap.length() > 0 ? newMap : null;
    }


    def excludeFromQuery(parameters,params,client=null){
        def excludeQuery=params.remove('excludeQuery')
        excludeQuery?.remove('comp')
        excludeQuery.each{key,value ->
            if(key=='advancedQuery'){
                JSONObject paramsValue = new JSONObject(value.toString().decodeURL());
                def changedValue = processMap(paramsValue,client)
                value = changedValue as JSON
            }
            if (!key.contains('comp.') && !key.contains('-to')) {
                if(value && value!=''){
                    if(value instanceof  String){
                        parameters=parameters+"&${key}=${value.toString().decodeURL()}"
                    }else{
                        parameters=parameters+"&${key}=${helperService.getTrimmedArrayString(value.toString())}"
                    }
                    params.remove("excludeQuery.${key}")
                }
            }else{
                params.remove("excludeQuery.${key}")
            }
        }
        return parameters;

    }

    def includeParticipationParams(params,client){
        if(params['isParticipation'] || params['program_type'] || params['program_name'] || params['participationStartDate'] || params['participationEndDate'] || params['participationProgramCountFromDate'] || params['participationProgramCountToDate'] || params['participationProgramCountCount']){
            def comparator=params.getAt('comp');
            def root = [:]
            def participationGroup = [:]
            def andGroup = [:]
            def compMap = [:]
            params["program_type"] ? andGroup["program_type"] = params["program_type"] : ""
            compMap["program_type"]=comparator['program_type']
            params["program_name"] ? andGroup["program_code"] = params["program_name"] : ""
            compMap["program_code"]=comparator['program_name']
            def sdComp = params["comp.participationStartDate"] ? params["comp.participationStartDate"] : "Smaller than equals"
            compMap["participationStartDate"] = sdComp
            def edComp = params["comp.participationEndDate"] ? params["comp.participationEndDate"] : "Greater than equals"
            compMap["participationEndDate"] = edComp
            if (params["participationStartDate"]){
                andGroup["participationStartDate"] = params["participationStartDate"]
            }else{
                andGroup["participationStartDate"] = client.cycleEndDate.format('MM-dd-yyyy')
            }
            if (params["participationStartDate-to"]){
                andGroup["participationStartDate-to"] = params["participationStartDate-to"]
            }
            if (params["participationEndDate"]){
                andGroup["participationEndDate"] = params["participationEndDate"]
            }else{
                andGroup["participationEndDate"] = client.cycleStartDate.format('MM-dd-yyyy')
            }
            if (params["participationEndDate-to"]){
                andGroup["participationEndDate-to"] = params["participationEndDate-to"]
            }
            def pcfdComp = params["comp.participationProgramCountFromDate"] ? params["comp.participationProgramCountFromDate"] : "Smaller than equals"
            compMap["participationProgramCountFromDate"] = pcfdComp

            def pctdComp = params["comp.participationProgramCountToDate"] ? params["comp.participationProgramCountToDate"] : "Smaller than equals"
            compMap["participationProgramCountToDate"] = pctdComp

            def pcccComp = params["comp.participationProgramCountCount"] ? params["comp.participationProgramCountCount"] : "Greater than"
            compMap["participationProgramCountCount"] = pcccComp

            if (params['participationProgramCountFromDate']){
                andGroup["participationProgramCountFromDate"] = params["participationProgramCountFromDate"]
            }
            if (params['participationProgramCountToDate']){
                andGroup["participationProgramCountToDate"] = params["participationProgramCountToDate"]
            }
            if (params['participationProgramCountCount']){
                andGroup["participationProgramCountCount"] = params["participationProgramCountCount"]
            }
            participationGroup["grp_part"] = andGroup
            andGroup["comp"] = compMap
            def rootOperator = "and"
            if (params["isParticipation"] && params["isParticipation"]=="n"){
                rootOperator = "not"
                root["and"] = participationGroup
            }else{
                root = participationGroup
            }

            if (params[rootOperator]){
                params[rootOperator].put("grp_part",root)
            }else{
                params[rootOperator] = root
            }

            params.remove("participationProgramCountFromDate")
            params.remove("participationProgramCountToDate")
            params.remove("participationProgramCountCount")
            params.remove("isParticipation")
            params.remove("program_type")
            params.remove("program_name")
            params.remove("participationStartDate")
            params.remove("participationStartDate-to")
            params.remove("participationEndDate")
            params.remove("participationEndDate-to")
        }
        return params
    }


    def includeCurrentStatusDate(params,client){
//		if (params["currentStatus"] || params['statusFilter']){
        def root = [:]
        def compMap = [:]
        def andGroup = [:]
        def participationGroup = [:]
        def statusToDateComp = "Equals"
        def statusFromDateComp = "Equals"
        def comp = params["comp"]
        if (comp){
            statusToDateComp = comp['terminationDate']
            statusFromDateComp = comp['effectiveDate']
        }

        Calendar calendar = Calendar.instance
        calendar.setTime(client.cycleEndDate)
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);

        def eligibilityType
//			if(params['no-eligible']){
        if (params['eligibilityType']){
            eligibilityType = params['eligibilityType']
        }else{
            eligibilityType = getNeededEligibilityType(client)
        }
        andGroup["statusEligibilityType"] = eligibilityType

        if(params["terminationDate"] && params["currentStatus"]){
            if (statusToDateComp == "Between"){
                andGroup["statusToDate"] = params["terminationDate"]
                andGroup["statusToDate-to"] = params["terminationDate-to"]
                statusToDateComp = "Between"
            }else if (statusToDateComp == "Equals"){
                andGroup["statusToDate"] = params["terminationDate"]
                statusToDateComp = "Greater than equals"
            }else{
                andGroup["statusToDate"] = params["terminationDate"]
            }
        }else if(params["terminationDate"] || params["effectiveDate"]){

        }else if(params["qmFromDate"] || params["qmToDate"]){
            statusToDateComp = "Greater than equals"
            andGroup["statusToDate"] = params["eligibleMonths-to"]
        }else{
            if(params['statusToDate']){
                andGroup["statusToDate"] = params['statusToDate']
                statusToDateComp = "Greater than equals"
            }else{
                calendar.set(year,month,calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                andGroup["statusToDate"] = helperService.getDateString(calendar.getTime(),"MM-dd-yyyy")
                statusToDateComp = "Greater than equals"
            }
        }

        if(params["effectiveDate"]){
            if(params["currentStatus"]){
                Date tempStatusFrmDate = helperService.getFormatedDateTime(params["effectiveDate"],"MM-dd-yyyy")
                if(tempStatusFrmDate < client.cycleStartDate){
                    tempStatusFrmDate = client.cycleStartDate
                }
                if (statusFromDateComp == "Between"){
                    andGroup["statusFromDate"] = params["effectiveDate"]
                    andGroup["statusFromDate-to"] = params["effectiveDate-to"]
                    statusFromDateComp = "Between"
                }else if(statusFromDateComp == "Equals"){
                    andGroup["statusFromDate"] = helperService.getDateString(tempStatusFrmDate,"MM-dd-yyyy")
                    statusFromDateComp = "Smaller than equals"
                }else{
                    andGroup["statusFromDate"] = helperService.getDateString(tempStatusFrmDate,"MM-dd-yyyy")
                }
            }
        }else if(params["terminationDate"] || params["effectiveDate"]){

        }else if(params["qmFromDate"] || params["qmToDate"]){
            statusFromDateComp = "Smaller than equals"
            andGroup["statusFromDate"] = params["eligibleMonths-to"]
        }else{
            if(params['statusFromDate']){
                andGroup["statusFromDate"] = params['statusFromDate']
                statusFromDateComp = "Smaller than equals"
            }else{
                calendar.set(year,month,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                andGroup["statusFromDate"] = helperService.getDateString(calendar.getTime(),"MM-dd-yyyy") //effDate -> statusFromDate
                statusFromDateComp = "Smaller than equals"
            }
        }

        compMap.put("statusToDate",statusToDateComp)
        compMap.put("statusFromDate",statusFromDateComp)
        andGroup["comp"] = compMap
        if(params["currentStatus"]){
            andGroup["currentStatus"] = 'Active'
        }

        participationGroup["grp_currrentStatus"] = andGroup
        def rootOperator = "and"
        if (params["currentStatus"] && params["currentStatus"]=="Termed"){
            rootOperator = "not"
            root["and"] = participationGroup
        }else{
            root = participationGroup
        }
        if (params[rootOperator]){
            params[rootOperator].putAll(root)
        }else{
            params[rootOperator] = root
        }
        params.remove("currentStatus")
        params.remove("statusToDate")
        params.remove("statusFromDate")
        params.remove("statusFilter")
//		}
//		else{
//			params.remove("frontCohortId")
//			params.remove("cohortId");
//		}
    }

    def addNecessaryParameters(parameters,params){
        if (params['order']){
            parameters=parameters+"&order=${params['order']}"
            params.remove('order')
        }
        if(params['defaultColumns']){
            parameters=parameters+"&fields=${params['defaultColumns']}"
            params.remove('defaultColumns')
        }
        if (params['report']) {
            parameters = parameters + "&report=${params['report']}"
            params.remove('report')
        }
        if (params['recordTypes']) {
            def recordTypeTableName = urlMappingService.getBackendTableForRecordType(params['recordTypes'])
            parameters = parameters + "&recordTypes=${recordTypeTableName}"
            params.remove('recordTypes')
        }
        if (params['drill']) {
            parameters = parameters + "&drill=${params['drill']}"
            params.remove('drill')
        }
        if (params['comparisonQuery']) {
            parameters = parameters + "&comparisonQuery=${params['comparisonQuery'].decodeURL()}"
            params.remove('comparisonQuery')
        }
        if (params['parentTable']) {
            parameters = parameters + "&parentTable=${params['parentTable']}"
            params.remove('parentTable')
        }
        if (params['parentQuery']) {
            parameters = parameters + "&parentQuery=${params['parentQuery'].decodeURL()}"
            params.remove('parentQuery')
        }
        if(params['reportingEligibleQuery']){
            parameters = parameters + "&reportingEligible=${params.remove('reportingEligibleQuery').decodeURL()}"
        }
        if (params['amount']) {
            parameters = parameters + "&amount=${params['amount']}"
            params.remove('amount')
        }
        if (params['reportingFrom']) {
            parameters = parameters + "&reportingFrom=${params['reportingFrom']}"
            params.remove('reportingFrom')
        }
        if (params['reportingTo']) {
            parameters = parameters + "&reportingTo=${params['reportingTo']}"
            params.remove('reportingTo')
        }
        if (params['reportingPaidThrough']) {
            parameters = parameters + "&reportingPaidThrough=${params['reportingPaidThrough']}"
            params.remove('reportingPaidThrough')
        }
        return parameters
    }
}
