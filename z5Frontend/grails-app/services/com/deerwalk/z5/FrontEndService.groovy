package com.deerwalk.z5

import com.deerwalk.security.DwClient
import com.deerwalk.security.DwClientGroup
import com.deerwalk.security.DwLevelOfAggregation
import com.deerwalk.security.User
import com.deerwalk.security.UserPhiField
import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH
import org.springframework.web.context.request.RequestContextHolder


class FrontEndService {

    def helperService
    def sessionFactory
    def dataSource
    def grailsApplication

    def getBusinessLevel(resultSetsJson,clientConfig,cycleDates){
        def businessRelation = []
        Set businessLevel = []
        int i=0;
        resultSetsJson.each{keys,val ->
            def row = [:]
            for ( index in 1..15 ) {
                def rowLevels = [:]
                def lvl=val.getAt('lvl'+index)
                if (lvl){
                    row['level'+index]=lvl
                    rowLevels['lvlDesc']=clientConfig.getAt('lvl'+index)
                    rowLevels['code']=lvl
                    rowLevels['desc']=val.getAt('lvl'+index+'_desc')?val.getAt('lvl'+index+'_desc'):lvl;
                    /**
                     * TODO remove comment to insert parentCode and parentLevel in DwClientGroup
                     */
                    //rowLevels['parentLevel']=clientConfig.getAt('lvl'+(index-1))
                    //rowLevels['parentCode']=val.getAt('lvl'+(index-1))

                    def dates=cycleDates[lvl+'_lvl'+index]
                    if(dates?.getAt('cycleEndDate')){
                        Date date=helperService.getFormatedDateTime(dates.getAt('cycleEndDate'),'yyyy-MM-dd')
                        rowLevels['cycleEndDate']=date
                    }else{
                        rowLevels['cycleEndDate']=null
                    }
                    if(dates?.getAt('cycleStartDate')){
                        Date date=helperService.getFormatedDateTime(dates.getAt('cycleStartDate'),'yyyy-MM-dd')
                        rowLevels['cycleStartDate']=date
                    }else{
                        rowLevels['cycleStartDate']=null
                    }
                    businessLevel.add(rowLevels)
                }else{
                    row['level'+index]=null
                }
            }
            businessRelation.add(row)
        }
        return ["businessRelation":businessRelation,"businessLevel":businessLevel]
    }

    def getTrimmedArrayStringWithAnd(key,value){
        def newString = ['and']
        def fixedValue = FixedValue.findAll("from FixedValue as f where f.code in (:value)",[value:value])
        fixedValue.sort{it.parent}
        def parent
        def groupMap=[:]
        fixedValue.each{
            if(!groupMap.containsKey(it.parent)){
                groupMap.put(it.parent,[])
            }
            groupMap.get(it.parent).add(it.code);
        }
        groupMap.values().each {
            newString.add(helperService.getTrimmedArrayString(it.toString()))
        }
        return helperService.getTrimmedArrayString(newString.toString())
    }

    def updateDB(dbRows,dbTable,client,DwClient dwClient=null){
        def saved = 0
        def failed = 0
        def existed=0
        def isloaPopup = false
        if (client){
            dbRows.each{row->
                def bl = null
                if (dbTable.equals('BusinessLevelEntry')){
                    def values=[code:row.getAt('code').toString(),description:row.getAt('desc').toString(), level:row.getAt('lvlDesc').toString(),client:client]
                    values['cycleEndDate']=row.getAt('cycleEndDate')
                    values['cycleStartDate']=row.getAt('cycleStartDate')
                    bl = new  BusinessLevelEntry(values)
                }
                if (dbTable.equals('BusinessRelation')){
                    bl = new BusinessRelation(level1:row.getAt('level1'),level2:row.getAt('level2'),level3:row.getAt('level3'),level4:row.getAt('level4'),level5:row.getAt('level5'),level6:row.getAt('level6'),level7:row.getAt('level7'),level8:row.getAt('level8'),level9:row.getAt('level9'),level10:row.getAt('level10'),level11:row.getAt('level11'),level12:row.getAt('level12'),level13:row.getAt('level13'),level14:row.getAt('level14'),level15:row.getAt('level15'), client:client);

                }
                if (dbTable.equals('LevelOfAggregation')){
                    bl = new LevelOfAggregation(fieldName: row["FIELD"],level: row["LEVEL"], client:client, displayName: row['DISPLAY'],parentLevel: row['PARENT'], secondaryField: row['DESC'], isLOAPopup: row['ISLOAPOPUP'])
                }
                /*if (dbTable.equals('program_type')){
                    //bl = new ClientParams(name:row.key,value: row.value, client:client)
                    bl = new FixedValue(code: row['program_code'],displayName: row['program_name'],parent: row['parent'],fieldName: "program_name",client: client)
                }*/

                if (bl?.save()){
                    saved++
                }else{
                    log.error bl.errors.allErrors.join(' \n')
                    failed++
                }
            }
        }




        if(dwClient){
            def flushCount=0
            println "{dwClient.id} = ${dwClient.id}"
            dbRows.each{row->
                try{
                    flushCount++;
                    def bl = null
                    if(dbTable.equals(DwClientGroup.class.getSimpleName())){
                        def rCode=row.getAt('code'),
                            rLevel=row.getAt('lvlDesc'),
                            rDescription=row.getAt('desc'),
                            rParentCode=row.getAt('parentCode'),
                            rParentLevel=row.getAt('parentLevel')
//						def query=null
//						if(rParentCode){
//                        println "rCode = $rCode"
//                        println "rLevel = $rLevel"
//                        println "rDescription = $rDescription"

                        def clientInfo;
                        def clientCode;

                        if(rLevel=="divisionId"){
                            def br=BusinessRelation.findByLevel2(rCode);
                            if(br.level1=="00052750"){
                                br.level1="webindustries";
                            }
                            clientInfo=DwClient.findByName(br.level1.toLowerCase());
                        }else{
                            if(rCode=="00052750"){
                                clientCode="webindustries";
                            }else{
                                clientCode=rCode;
                            }
                            def isDev=grailsApplication.config.grails.navigationUrl


                            if(isDev.toString().equals("z5_dev")){
                                clientCode=clientCode+"_dev";
                            }

                            clientInfo=DwClient.findByName(clientCode.toLowerCase());
                        }

                        dwClient=clientInfo;




                        def 	query = DwClientGroup.where {
                            client==dwClient && code==rCode && level==rLevel /*&& parentCode==rParentCode && parentLevel==rParentLevel*/
                        }
//						}else{
//							query = DwClientGroup.where {
//								client==dwClient && softDelete == 0L && code==rCode && level==rLevel && isNull(parentCode) && isNull(parentLevel)
//							}
//						}
                        def list=query.findAll()
                        if(list.size()==0){
                            bl=new DwClientGroup(code:rCode,level: rLevel, name: rDescription,client:dwClient,isMakalu: true ) /*,parentCode:rParentCode,parentLevel: rParentLevel*/
                            if(rLevel=="groupId") {
                                print"**group values**"
                                print"::group="+rCode
                                print"::rLevel="+rLevel
                                println"::dwClient=="+dwClient
                                print"::name=="+rDescription
                            }
                        }else{
                            bl=list.get(0)
                            existed++
//							bl=dwClientGroup
                            bl.isMakalu=true
                        }
                    }
                    if(dbTable.equals(DwLevelOfAggregation.class.getSimpleName())){
                        bl = new DwLevelOfAggregation(fieldName: row["FIELD"],level: row["LEVEL"], dwClient:dwClient, displayName: row['DISPLAY'],parentLevel: row['PARENT'])
                    }
                    if (bl?.save()){
                        saved++
                    }else if(bl){
//                        e.printStackTrace()
//
                        log.error("Fail to insert ${bl.class} with row ${row} : ${dbTable}")
                        failed++
                    }
                }catch(e){
//					print bl
                    e.printStackTrace()
                }
                if(flushCount%100==0){
                    sessionFactory.currentSession.clear();
                }
            }
        }


        return ['updated':saved,'failed':failed,existed:existed]
    }


    public DwClient findDwClientByDomain(String domain) {
        String hostUrl =domain
        def clientName
        String[] urlParts

        if (hostUrl.contains("-")) {
            urlParts = hostUrl.split("-")
        } else {
            urlParts    = new String[2]
            int index   = hostUrl.indexOf(".")
            urlParts[0] = hostUrl.substring(0,index)

            if (urlParts[0].equals("www")) {
                int secondDotIndex = hostUrl.indexOf(".", index +1)
                urlParts[0]        = hostUrl.substring(index+1,secondDotIndex)
            }
            urlParts[1] = hostUrl.substring(index+1,hostUrl.length())
        }

        clientName = urlParts[0]
        println "clientName============ = $clientName"

        log.debug  "clientName -> " + clientName
        def dwClient = DwClient.findByName(clientName) ?: null
//        def dwClient = DwClient.find("FROM DwClient dw where dw.name = ?", [clientName])
        log.debug "dwClient->" + dwClient
        dwClient ?: null
    }


    def getDataForLOA(clientConfig,oldLoas = null){
        def data = []
        def row = [:]
        def isLevel = false
        clientConfig.each { keys,val ->
            val.each{ key,value ->
                isLevel = false
                row = [:]
                switch (key){
                    case "lvl1":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl1_display'] && val['lvl1_display']!="") ? val['lvl1_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = 0
                        row['LEVEL'] = 1
                        row['DESC'] = val['lvl1_desc']
                        isLevel = true
                        break
                    case "lvl2":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl2_display'] && val['lvl2_display']!="") ? val['lvl2_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl2'] && val['parent_lvl2']=="true") ? 1 : 0
                        row['LEVEL'] = 2
                        row['DESC'] = val['lvl2_desc']
                        isLevel = true
                        break
                    case "lvl3":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl3_display'] && val['lvl3_display']!="") ? val['lvl3_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl3'] && val['parent_lvl3']=="true") ? 2 : 0
                        row['LEVEL'] = 3
                        row['DESC'] = val['lvl3_desc']
                        isLevel = true
                        break
                    case "lvl4":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl4_display'] && val['lvl4_display']!="") ? val['lvl4_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl4'] && val['parent_lvl4']=="true") ? 3 : 0
                        row['LEVEL'] = 4
                        row['DESC'] = val['lvl4_desc']
                        isLevel = true
                        break
                    case "lvl5":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl5_display'] && val['lvl5_display']!="") ? val['lvl5_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl5'] && val['parent_lvl5']=="true") ? 4 : 0
                        row['LEVEL'] = 5
                        row['DESC'] = val['lvl5_desc']
                        isLevel = true
                        break
                    case "lvl6":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl6_display'] && val['lvl6_display']!="") ? val['lvl6_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl6'] && val['parent_lvl6']=="true") ? 5 : 0
                        row['LEVEL'] = 6
                        row['DESC'] = val['lvl6_desc']
                        isLevel = true
                        break
                    case "lvl7":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl7_display'] && val['lvl7_display']!="") ? val['lvl7_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl7'] && val['parent_lvl7']=="true") ? 6 : 0
                        row['LEVEL'] = 7
                        row['DESC'] = val['lvl7_desc']
                        isLevel = true
                        break
                    case "lvl8":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl8_display'] && val['lvl8_display']!="") ? val['lvl8_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl8'] && val['parent_lvl8']=="true") ? 7  : 0
                        row['LEVEL'] = 8
                        row['DESC'] = val['lvl8_desc']
                        isLevel = true
                        break
                    case "lvl9":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl9_display'] && val['lvl9_display']!="") ? val['lvl9_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl9'] && val['parent_lvl9']=="true") ? 8 : 0
                        row['LEVEL'] = 9
                        row['DESC'] = val['lvl9_desc']
                        isLevel = true
                        break
                    case "lvl10":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl10_display'] && val['lvl10_display']!="") ? val['lvl10_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl10'] && val['parent_lvl10']=="true") ? 9 : 0
                        row['LEVEL'] = 10
                        row['DESC'] = val['lvl10_desc']
                        isLevel = true
                        break
                    case "lvl11":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl11_display'] && val['lvl11_display']!="") ? val['lvl11_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl11'] && val['parent_lvl11']=="true") ? 10 : 0
                        row['LEVEL'] = 11
                        row['DESC'] = val['lvl11_desc']
                        isLevel = true
                        break
                    case "lvl12":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl12_display'] && val['lvl12_display']!="") ? val['lvl12_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl12'] && val['parent_lvl12']=="true") ? 11 : 0
                        row['LEVEL'] = 12
                        row['DESC'] = val['lvl12_desc']
                        isLevel = true
                        break
                    case "lvl13":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl13_display'] && val['lvl13_display']!="") ? val['lvl13_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl13'] && val['parent_lvl13']=="true") ? 12 : 0
                        row['LEVEL'] = 13
                        row['DESC'] = val['lvl13_desc']
                        isLevel = true
                        break
                    case "lvl14":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl14_display'] && val['lvl14_display']!="") ? val['lvl14_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl14'] && val['parent_lvl14']=="true") ? 13 : 0
                        row['LEVEL'] = 14
                        row['DESC'] = val['lvl14_desc']
                        isLevel = true
                        break
                    case "lvl15":
                        row['FIELD'] = value
                        row['DISPLAY'] = (val['lvl15_display'] && val['lvl15_display']!="") ? val['lvl15_display'] : helperService.camelToStandardCase(value)
                        row['PARENT'] = (val['parent_lvl15'] && val['parent_lvl15']=="true") ? 14 : 0
                        row['LEVEL'] = 15
                        row['DESC'] = val['lvl15_desc']
                        isLevel = true
                        break
                }
                if(isLevel){
                    if(oldLoas) row['ISLOAPOPUP'] = oldLoas.find{it.fieldName == row["FIELD"]}?.isLOAPopup?:false
                    else row['ISLOAPOPUP'] = false
                }
                if(row.size()>0){
                    data.add(row)
                }
            }
        }
        return data
    }



}
