package com.deerwalk.z5

import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class PopulationRiskService {

    def searchService
    def fetchService
    def helperService

    Map setDynamicReportParams(params,session,dates) {
        LinkedHashMap dynamicReportParams = [:]
            dynamicReportParams['reportingFrom'] =params.reportingFrom// dates.getAt('ReportingFirst')
            dynamicReportParams['reportingTo'] = params.reportingTo//dates.getAt('ReportingSecond')
            dynamicReportParams['basis'] = params.reportingBasis
            dynamicReportParams['phiCSDate'] =  params.csDate
            dynamicReportParams['phiCEDate'] = params.ceDate
        dynamicReportParams['clientId'] = helperService.fetchClientId()
        dynamicReportParams['trend'] = params.trend
        dynamicReportParams['udf17Id'] = params.mbrRegionId
        def eligibilityType = searchService.getNeededEligibilityType(session.client)
        dynamicReportParams['eligibilityType'] = eligibilityType
        dynamicReportParams['reportingPaidThrough'] = "2015-09-30"
        dynamicReportParams['exclude'] = params.exclude
        dynamicReportParams['isExclude'] = params?.isExclude
        dynamicReportParams['range'] = params.range
        dynamicReportParams['chronicCode'] = params.chronicCode
//        dynamicReportParams['metric'] = params.metric

        dynamicReportParams['recordTypes'] = ['Medical','Pharmacy','Eligibility','Biometrics','HRA','Episodes','Utilization','Participation'];

        if(!params.showBob)
        dynamicReportParams['groups']= searchService.getGroups()
//        dynamicReportParams["switchedUserId"] = session.switchedUserId
//        dynamicReportParams.locale = session.locale


        return dynamicReportParams
    }


    def getPopulationRiskScript(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        try{
            json = fetchChartData(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);
            if(report=="gapInCareRealtime")
                rawJSON=parsedJSON
            else
            rawJSON=parsedJSON.getAt('reporting');
        }catch(e){
            log.error(e)
            log.error('No Data received for population risk')
            return '';
        }
        return rawJSON
    }

    def getPopulationRiskScriptWBenchmark(dynamicReportParams,report){

        def rawJSON=null;
        def json;
        try{
            dynamicReportParams['includes'] = ["truvenBenchmark"]
            dynamicReportParams['benchmarkKey'] = "chronicConditionUtilization"
            json = fetchChartData(report,dynamicReportParams)
            def parsedJSON=JSON.parse(json);
            rawJSON=parsedJSON;
        }catch(e){
            log.error(e)
            log.error('No Data received for population risk')
            return '';
        }
        return rawJSON
    }






    private def fetchChartData(report, params) {
        def module=""
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def parameters


        if (params && params['reportingTo'] && params['reportingFrom'] && params['basis'] && params['clientId']) {
            def loaParams = params["loaParams"]
            /*if(session.user.userAccess){
                loaParams = helperService.checkSelectedLoaInUserAccess(loaParams, session.user.userAccess);
            }*/
            if(report.equals('provider:erVisit') || report.equals('provider:snapshot') || report.equals('provider:thirtyDayReadmit')){
//                module = 'provider'
                parameters = "clientId=${params['clientId']}${fetchService.getLoaRequestParameterSectionForProviderDashBoard(loaParams)}"
            }else{
                parameters = "clientId=${params['clientId']}"
            }
            if(params["switchedUserId"]){
                parameters += "&userId="+params["switchedUserId"]
            }
            parameters = parameters + "&reportingBasis=${params['basis']}&reportingTo=${params['reportingTo']}&reportingFrom=${params['reportingFrom']}&report=${report}"
            parameters = parameters + "&eligibilityType=${params.getAt('eligibilityType')}"
            if (params['basis'] == 'ServiceDate') {
//                parameters = parameters + "&reportingPaidThrough=${params['reportingThrough']}"
            }
            if (params['participation']) {
                parameters = parameters + "&isParticipation=${params['participation']}"
                if (params && params['program_name']) {
                    parameters = parameters + "&program_name=${params['program_name']}"
                }
                if (params && params['program_type']) {
                    parameters = parameters + "&program_type=${params['program_type']}"
                }
            }

            if(params['dataView']){
                parameters = parameters + "&dataView=${params['dataView']}"
            }

            if(params['phiCSDate']){
                parameters = parameters + "&phiCSDate=${params['phiCSDate']}"
            }

            if(params['phiCEDate']){
                parameters = parameters + "&phiCEDate=${params['phiCEDate']}"
            }

            if(params && params["trendingField"]){
                parameters = parameters + "&trendingField=${params['trendingField']}"
            }

            if(report=="populationRisk"){
                parameters = parameters + "&recordTypes=${params.getAt('recordTypes')}"
            }
            if(params['udf17Id']){
                parameters = parameters + "&udf17Id=${params['udf17Id']}"
            }

            if(params['includes']){
                parameters = parameters + "&includes=${params['includes']}"
            }

            if(params.getAt('groups')){
                parameters = parameters + "&group=${params.getAt('groups')}"
            }
            if(params.getAt('isExclude')){
                parameters = parameters + "&isExclude=${params.getAt('isExclude')}"
            }
            if(params.getAt('range')){
                parameters = parameters + "&range=${params.getAt('range')}"
            }
            if(params.getAt('exclude')){
                parameters = parameters + "&exclude=${params.getAt('exclude')}"
            }
            if(params.getAt('trend')){
                parameters = parameters + "&trend=${params.getAt('trend')}"
            }
            /*if(params.getAt('metric')){
                parameters = parameters + "&metric=${params.getAt('metric')}"
            }*/
            if(params.getAt('benchmarkKey')){
                parameters = parameters + "&benchmarkKey=${params.getAt('benchmarkKey')}"
            }
            if(params.getAt('chronicCode')){
                parameters = parameters + "&chronicCode=${params.getAt('chronicCode')}"
            }
            parameters=parameters+"&dataFetchFrom=true"
//            parameters=parameters+"&includes=benchmark"


            /*def client = Client.findByClientId(params['clientId'])
            def clientRecordTypes = frontEndService.getClientRecordType(client)
            def mainString="&recordTypes="
            StringBuilder arrayElements=new StringBuilder()
            clientRecordTypes.each{
                arrayElements.append(it.toString()).append(",")
            }
            def temp=arrayElements.toString().lastIndexOf(",")
            def finalArrayElements=arrayElements.toString().substring(0,temp)
            mainString=mainString+"["+arrayElements+"]"

            parameters = parameters + mainString

            if (params['cohortId']) {
                parameters = parameters + '&cohortId=' + params['cohortId'];
            }
            if(params['reportConfig']){
                parameters = parameters + '&reportConfig=' + params['reportConfig'];
            }
            if(params['keyField']){
                parameters = parameters + '&keyField=' + params['keyField'];
            }*/

//            println "parameters = $parameters"
//            parameters="clientId=2000&reportingBasis=PaidDate&reportingTo=2015-06-30&reportingFrom=2014-07-01&eligibilityType=[medical]&report=medicalClaimsPmpm&phiCSDate=01-01-2010&phiCEDate=06-30-2015"
            if(report=="gapInCareRealtime"){
                module="reportFromDomain"
            }

            return fetchService.fetchData(parameters, module)
        } else {
            return null
        }
    }
}
