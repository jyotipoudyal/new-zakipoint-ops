package com.deerwalk.z5


import grails.util.Environment
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.cas.authentication.CasAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.switchuser.SwitchUserGrantedAuthority

class DwProxyService {

    /**
     * @param url       Generate proxy ticket for this URL
     * @param attempts  Count to keep track of the number of attempts to get the proxy ticket. Maximum attempts can be 5
     * @return          The proxy ticket generated for the URL passed as parameter
     */
    public String getProxyTicket(String url, int attempts) {
        if (Environment.getCurrent().equals(Environment.DEVELOPMENT)) {
            println "########## this is dev ############"
            return null;
        }
        final String proxyTicket
        if (SecurityContextHolder.getContext().getAuthentication() instanceof CasAuthenticationToken) {
            println "cas authentication = cas authentication Token"
            final CasAuthenticationToken token = (CasAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()

            println "token1 = $token"
            proxyTicket = token.getAssertion().getPrincipal().getProxyTicketFor(url)
        } else if (SecurityContextHolder.getContext().getAuthentication() instanceof UsernamePasswordAuthenticationToken) {
            println "UsernamePasswordAuthenticationToken = UsernamePasswordAuthenticationToken"
            final UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()
            println "token2 = $token"

            for(GrantedAuthority auth:token.getAuthorities()){
                if(auth instanceof SwitchUserGrantedAuthority){
                    proxyTicket = auth.getSource().getAssertion().getPrincipal().getProxyTicketFor(url)
                }
            }
        }

        println("Proxy ticket 1 from attempt number ${attempts} : ${proxyTicket}")

        attempts += 1
        proxyTicket = attempts <= 5 && proxyTicket == null ? getProxyTicket(url, attempts) : proxyTicket

        proxyTicket
    }

    /**
     * @param url       Generate proxy ticket for this URL
     * @param attempts  Count to keep track of the number of attempts to get the proxy ticket. Maximum attempts can be 5
     * @return          The proxy ticket generated for the URL passed as parameter
     */
    public String getProxyTicket(CasAuthenticationToken token, String url, int attempts) {

        println("CasAuthenticationToken : ${token}")
        final String proxyTicket  = token.getAssertion().getPrincipal().getProxyTicketFor(url)
        println("Proxy ticket 2 from attempt number ${attempts} : ${proxyTicket}")

        attempts += 1
        proxyTicket = attempts <= 5 && proxyTicket == null ? getProxyTicket(url, attempts) : proxyTicket

        proxyTicket
    }


}
