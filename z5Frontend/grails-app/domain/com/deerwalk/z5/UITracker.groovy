package com.deerwalk.z5

class UITracker {
    def springSecurityService
    String name
    String content
    String prevContent
    String url

    Date dateCreated
    BigInteger lastUpdatedBy


    static constraints = {
    }

    def beforeInsert() {
        lastUpdatedBy = springSecurityService.getCurrentUser().id
    }

}
