package com.deerwalk.z5


class LogAnalytics {
    String userId
    String client
    String controllerId
    String actionId
    String parameters
    String uri
    Date timeStamp=new Date();
    String duration

    static constraints = {
        userId(blank: false , nullable: false)
        client(blank: false , nullable: false)
        controllerId(blank: false , nullable: false)
        actionId(blank: false , nullable: false)
        parameters(blank: false , nullable: false)
        uri(blank: false , nullable: false)
    }

    static mapping = {
        parameters type: 'text'
        uri type: 'text'
        sort timeStamp: "desc"
    }
}
