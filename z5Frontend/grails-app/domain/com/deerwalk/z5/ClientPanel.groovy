package com.deerwalk.z5

class ClientPanel {
    String client
    String uId
    boolean isBob
    ClientPanel parent
    String clientId
    boolean eligibility
    boolean biometrics

    static constraints = {
        client(blank: false,nullable: false)
        clientId(blank: true,nullable: true)
        parent(nullable: true)
    }

    String toString(){
        return client
    }
    
    


}
