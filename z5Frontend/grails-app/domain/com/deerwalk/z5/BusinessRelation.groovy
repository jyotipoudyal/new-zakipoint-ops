package com.deerwalk.z5

class BusinessRelation implements Serializable {
	String level1
	String level2
	String level3
	String level4
	String level5
	String level6
	String level7
	String level8
	String level9
	String level10
	String level11
	String level12
	String level13
	String level14
	String level15


	static belongsTo = [client:Client]

	static constraints = {
		level1(nullable:true,blank:true,maxSize:255)
		level2(nullable:true,blank:true,maxSize:255)
		level3(nullable:true,blank:true,maxSize:255)
		level4(nullable:true,blank:true,maxSize:255)
		level5(nullable:true,blank:true,maxSize:255)
		level6(nullable:true,blank:true,maxSize:255)
		level7(nullable:true,blank:true,maxSize:255)
		level8(nullable:true,blank:true,maxSize:255)
		level9(nullable:true,blank:true,maxSize:255)
		level10(nullable:true,blank:true,maxSize:255)
		level11(nullable:true,blank:true,maxSize:255)
		level12(nullable:true,blank:true,maxSize:255)
		level13(nullable:true,blank:true,maxSize:255)
		level14(nullable:true,blank:true,maxSize:255)
		level15(nullable:true,blank:true,maxSize:255)
		client(nullable:false,blank:false,unique:['level1','level2','level3','level4','level5','level6','level7','level8','level9','level10','level11','level12','level13','level14','level15'])

		/*client validator: {val, obj->
			def br = BusinessRelation.createCriteria()
			def results = br.list{
				eq("client", obj.client)
				if(obj.level1){
					eq("level1",obj. level1)
				}else{
					isNull("level1")
				}
				if(obj.level2){
					eq("level2", obj.level2)
				}else{
					isNull("level2")
				}
				if(obj.level3){
					eq("level3", obj.level3)
				}else{
					isNull("level3")
				}
				if(obj.level4){
					eq("level4", obj.level4)
				}else{
					isNull("level4")
				}
				if(obj.level5){
					eq("level5", obj.level5)
				}else{
					isNull("level5")
				}
				if(obj.level6){
					eq("level6", obj.level6)
				}else{
					isNull("level6")
				}
				if(obj.level7){
					eq("level7",obj. level7)
				}else{
					isNull("level7")
				}
				if(obj.level8){
					eq("level8", obj.level8)
				}else{
					isNull("level8")
				}
				if(obj.level9){
					eq("level9", obj.level9)
				}else{
					isNull("level9")
				}
				if(obj.level10){
					eq("level10", obj.level10)
				}else{
					isNull("level10")
				}
				if(obj.level11){
					eq("level11", obj.level11)
				}else{
					isNull("level11")
				}
				if(obj.level12){
					eq("level12", obj.level12)
				}else{
					isNull("level12")
				}
				if(obj.level13){
					eq("level13", obj.level13)
				}else{
					isNull("level13")
				}
				if(obj.level14){
					eq("level14", obj.level14)
				}else{
					isNull("level14")
				}
				if(obj.level15){
					eq("level15", obj.level15)
				}else{
					isNull("level15")
				}
			}
			return results?false:true
		}*/
	}
}
