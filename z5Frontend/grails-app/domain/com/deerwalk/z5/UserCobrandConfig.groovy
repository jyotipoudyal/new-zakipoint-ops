package com.deerwalk.z5

import com.deerwalk.security.User


class UserCobrandConfig {

    User user
    Cobrand brand

    static constraints = {
    }

    static mapping = {
        datasource 'usermanagement'
    }
}
