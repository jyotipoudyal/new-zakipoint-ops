package com.deerwalk.z5

class SaveGoals {

    BigInteger user
    String clientName
    String page
    String content

    Date dateCreated
    Date lastUpdated


    static constraints = {
    }

    static mapping = {
        content type: 'text'
    }
}
