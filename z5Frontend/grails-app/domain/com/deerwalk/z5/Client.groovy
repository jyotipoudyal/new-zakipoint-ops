package com.deerwalk.z5

import grails.util.Holders

class Client {
	static auditable = true

	String domain
	String name
	String clientId  //Should be same as backend client ID
	String dwClientId //Should be same across all deployments
	Date cycleStartDate
	Date cycleEndDate
	Date dataReceivedDate
	Date applicationPublishedDate
	Boolean employeeGroupProcessing = false
	String companyURL
	String logoFileName
	String faviconImage
	String copyRightMessage
//	Theme theme
	String redirectUrl
	String dashBoardAction
	Boolean showBenchMarkingValues = false
	Boolean showSecondBenchmark = false
//	ClientOwner clientOwner
	String showParticipation
	Boolean sendEmail = false
	Boolean showQAVerified = false
	Boolean removeVideo= false
	Boolean removeBackground= false
	Boolean removeGroupCodeReport= false
	String lastUpdatedBy
	//Use multiselect or popup for LOA default : false multiSelect
	boolean isLOAPopup=false
	boolean isAdvanceQuery=false
	Boolean identicalCycleDate = false
	boolean isProvider=false
	boolean isParent = false
	Client parent = null
//	static hasMany=[utilizationMetrics: UtilizationMetric,chronicConditions:ChronicCondition,qualityMetrics:QualityMetric, reports:Report, customReports:CustomReport, modules:String, moduleDashBoard:ModuleDashBoard, recordType:String, dashBoardChart:DashBoardChart]
//	static hasOne = [companyAddress:Address]

	static constraints = {
		domain(blank: true,nullable:true)
		dwClientId(blank:true,nullable: true,maxSize: 30)
		name(blank: true,nullable:true)
		clientId(blank: false,nullable:false,unique: true)
		cycleStartDate(blank: true,nullable:true)
		cycleEndDate(blank: true,nullable:true)
		dataReceivedDate(blank: true,nullable:true)
		applicationPublishedDate(blank: true,nullable:true)
		employeeGroupProcessing(blank: true,nullable:true)
		companyURL(blank: true,nullable:true)
//		companyAddress(blank: true,nullable:true)
		logoFileName(blank: true,nullable:true)
		faviconImage(blank: true,nullable:true)
		copyRightMessage(blank: true,nullable:true)
//		theme(blank: true , nullable: true)
		redirectUrl(blank: true , nullable: true)
//		moduleDashBoard(blank:true,nullable: true)
//		recordType(blank:true,nullable: true)
		dashBoardAction(blank:true,nullable: true)
		showBenchMarkingValues(blank:true,nullable: true)
		showSecondBenchmark (blank:true,nullable: true)
//		clientOwner(blank:true,nullable: true)
		showParticipation(blank:true,nullable: true)
		sendEmail(blank:true,nullable: true)
		showQAVerified(blank:true,nullable: true)
		removeVideo(blank:true,nullable:true)
		removeBackground(blank:true,nullable:true)
		removeGroupCodeReport(blank:true,nullable:true)
		lastUpdatedBy(blank:true,nullable:true)
		isLOAPopup(blank:true,nullable:true)
		isAdvanceQuery(blank:true,nullable:true)
		identicalCycleDate(blank:true,nullable:true)
		isProvider(blank:true,nullable:true)
		isParent(blank:true,nullable:true)
		parent(blank:true,nullable:true)
	}

	/*static fetchMode = [theme: 'eager']

	static mapping = {
		recordType lazy: false
	}

	def String toString(){
		return name
	}

	def String nameDomain(){
		return name +" - "+domain;
	}*/

	def getAuditLogUri = {
		def version = Holders.config.grails.applicationVersion
		version as String
	}
}
