package com.deerwalk.z5

class BiometricsData {

    String uId
    String name
    String description
    String info
    String action


    static mapping = {
        info type: 'text'
        action type: 'text'
        description type: 'text'
    }

    static constraints = {
    }
}
