package com.deerwalk.z5

import com.deerwalk.security.DwClient


class ClientCobrandConfig {

    DwClient client
    Cobrand brand

    static constraints = {
    }

    static mapping = {
        datasource 'usermanagement'
    }
}
