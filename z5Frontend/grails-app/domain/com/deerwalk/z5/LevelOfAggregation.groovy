package com.deerwalk.z5

class LevelOfAggregation {
	Client client
	int level
	String fieldName
	String displayName
	String secondaryField
	int parentLevel
	boolean isLOAPopup

    static constraints = {
	    client(blank:false, nullable:false)
	    level(blank:false, nullable:false,unique:['client'])
	    fieldName(blank:false, nullable:false)
	    displayName(blank:false, nullable:false)
	    secondaryField(blank:true, nullable:true)
	    parentLevel(blank:true, nullable:true)
	    isLOAPopup(blank:true, nullable:true)
    }

	String oldParamName(){
		return "LEVEL${level}-NAME"
	}

	String secondaryLevelName(){
		return "level${level}Value"
	}

	String toString(){
		return displayName
	}

}
