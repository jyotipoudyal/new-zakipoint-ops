package com.deerwalk.z5

class BusinessLevelEntry implements Serializable {
      String level
      String description
      String code
      Client client
	Date cycleStartDate
	Date cycleEndDate


      static constraints = {
            client(nullable:false,blank:false)
            level(nullable:false,blank:false)
            description(nullable:false,blank:false)
	      cycleStartDate(blank: true,nullable:true)
	      cycleEndDate(blank: true,nullable:true)
            code(nullable:false,blank:false,unique:['client','level'])
      }

      def String toString(){
            return description
      }
}
