package com.deerwalk.z5

class Cobrand {

    String brand
    byte[] logoFile;


    static constraints = {


    }

    static mapping = {
        datasource 'usermanagement'
        logoFile sqlType: "blob"
    }
}
