import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils

class LogoutController {

	/**
	 * Index action. Redirects to the Spring security logout uri.
	 */
	def index = {
//		session.invalidate()
		// TODO put any pre-logout code here
		redirect uri: SpringSecurityUtils.securityConfig.logout.filterProcessesUrl // '/j_spring_security_logout'
	}



	def invalidateSession(){
		redirect uri: SpringSecurityUtils.securityConfig.logout.filterProcessesUrl // '/j_spring_security_logout'
//		session.invalidate()
		render true
	}

	def ajaxKeepAlive = {
		render("SESSION KEPT ALIVE!!!")
	}

	def accessDenied = {
//		render("You are not authorized to view this page")
		render(view:'/accessDenied')
	}

	def denied() {
			redirect action: 'steptwo'

	}

	def steptwo(){
		render "Two way access"
	}
}
