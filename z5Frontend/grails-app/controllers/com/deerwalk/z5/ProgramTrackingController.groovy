package com.deerwalk.z5

import grails.converters.JSON
import org.apache.poi.xslf.usermodel.XSLFTextParagraph
import report.PptBuilder
import sun.misc.BASE64Decoder

import java.awt.Rectangle

import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.poi.xslf.usermodel.XSLFPictureData
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFPictureData;
import org.apache.poi.xslf.usermodel.XSLFPictureShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;




class ProgramTrackingController {

    def dynamicParamsGeneratorService;
    def reportFetchService;
    def helperService;
    def searchService
    def fetchService


    def pt() {
        render(view:"pt")

    }

    def index(){

        return;



        //creating a presentation
       /* XMLSlideShow ppt = new XMLSlideShow();

        //creating a slide in it
        XSLFSlide slide = ppt.createSlide();

        //reading an image
        File image=new File("image.png");
        println "image = $image"

        //converting it into a byte array
        *//*byte[] picture = IOUtils.toByteArray(new FileInputStream(image));

        //adding the image to the presentation
//        int idx = ppt.addPicture(picture, XSLFPictureData.PICTURE_TYPE_PNG);
        int idx = ppt.addPicture(picture, PictureData.PictureType.PNG);

        //creating a slide with given picture on it
        XSLFPictureShape pic = slide.createPicture(idx);*//*

        byte[] pictureData = IOUtils.toByteArray(new FileInputStream(image));

        XSLFPictureData pd = ppt.addPicture(pictureData, PictureData.PictureType.PNG);
        XSLFPictureShape pic = slide.createPicture(pd);

        //creating a file object
        File file=new File("addingimage.pptx");
        FileOutputStream out = new FileOutputStream(file);

        //saving the changes to a file
        ppt.write(out)
        System.out.println("image added successfully");
        out.close();*/

//        PptBuilder ppts=new PptBuilder(index:0,slideName: "slide1",slideType: "chart",parameter:params.imagePath_1);
//        def message=ppts.createSlide();



        def sourceData,parts,imageString;
        byte[] imageByte;
        BASE64Decoder decoder
        def imageMap=[:]

        sourceData = params.imagePath_1;
        parts = sourceData.tokenize(",");
        imageString = parts[1];
        decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);

        XMLSlideShow ppt = new XMLSlideShow();

        // XSLFSlide#createSlide() with no arguments creates a blank slide
//        XSLFSlide blankSlide =  ppt.createSlide();


        /*XSLFSlideMaster master = ppt.getSlideMasters().get(0);

        XSLFSlideLayout layout1 = master.getLayout(SlideLayout.TITLE);
        XSLFSlide slide1 = ppt.createSlide(layout1) ;
        XSLFTextShape[] ph1 = slide1.getPlaceholders();
        XSLFTextShape titlePlaceholder1 = ph1[0];
        titlePlaceholder1.setText("This is a title");
        XSLFTextShape subtitlePlaceholder1 = ph1[1];
        subtitlePlaceholder1.setText("this is a subtitle");

        XSLFSlideLayout layout2 = master.getLayout(SlideLayout.TITLE_AND_CONTENT);
        XSLFSlide slide2 = ppt.createSlide(layout2) ;
        XSLFTextShape[] ph2 = slide2.getPlaceholders();
        XSLFTextShape titlePlaceholder2 = ph2[0];
        titlePlaceholder2.setText("This is a title");
        XSLFTextShape bodyPlaceholder = ph2[1];
        // we are going to add text by paragraphs. Clear the default placehoder text before that
        bodyPlaceholder.clearText();
        XSLFTextParagraph p1 = bodyPlaceholder.addNewTextParagraph();
        p1.setIndentLevel(0);
        p1.addNewTextRun().setText("Level1 text");
        XSLFTextParagraph p2 = bodyPlaceholder.addNewTextParagraph();
        p2.setIndentLevel(1);
        p2.addNewTextRun().setText("Level2 text");
        XSLFTextParagraph p3 = bodyPlaceholder.addNewTextParagraph();
        p3.setIndentLevel(2);
        p3.addNewTextRun().setText("Level3 text");*/
        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
//        XSLFSlideLayout layout3 = master.getLayout(SlideLayout.TITLE);


        XSLFSlide slide3 =   ppt.createSlide();
//        File image=new File("image.png");
//        byte[] pictureData = IOUtils.toByteArray(new FileInputStream(image));
//        byte[] pictureData = imageByte
        XSLFPictureData pd = ppt.addPicture(imageByte, org.apache.poi.sl.usermodel.PictureData.PictureType.PNG);
        XSLFPictureShape pic = slide3.createPicture(pd);
        pic.setAnchor(new Rectangle(80, 80, 750,500));
//      slide3.addShape(pic);





//-----------------------------------------------------
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        ppt.write(output);
        ppt.close();

        response.addHeader("Content-Type", "application/ppt");
        response.addHeader("Content-Disposition", "attachment; filename=Chart.pptx");
        response.getOutputStream().write(output.toByteArray());

    }

    def pTracking(){

    }


    def cohortComparison(){

        if(params.id){
            params.comparatorId=params.id
            params.domain="cohortsByComparator"

            def dynamicReportParam,data;
            dynamicReportParam = dynamicParamsGeneratorService.setCohortParams(params, session, [:])
            data = reportFetchService.getCohortComparatorList(dynamicReportParam, "list")
            data=data.sort{it.order};

            def clientId=helperService.fetchClientId()
            def dataComparison=new LinkedHashMap();
            def dataComparison2=new LinkedHashMap();
            def map=[:];


            def dataContainer=new LinkedList();

            def reportingDates=helperService.getReportingDates(session.ceDate)
            def counter=0;


            data.eachWithIndex{ v,i->
                    counter++;
                    map.cohortKey=v.cohortId
                    map.reportingFrom=reportingDates.priorFrom
                    map.reportingTo=reportingDates.currentTo
                    map.phiCSDate=session.csDate
                    map.phiCEDate=session.ceDate
                    map.range =0
                    map.isExclude ="false"
                    map.isZakipoint="true"
//                    map['requestIndex']=2

                def parsedJSON=[:],drillParams,jsonData;


                    (2..6).each{
                        map['requestIndex']=it;
                        drillParams = searchService.createSearchQuery([cohortCompare:true],map,15, 1, clientId, null,null, "msZakipoint","","totalPaidAmount","desc")
                        jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
                        def newJSON =JSON.parse(jsonData);

                        newJSON.each{key,val->
                            if(!parsedJSON[key]){
                                parsedJSON[key]=newJSON[key]
                            }
                        }
                    }

                map['requestIndex']=8;
                map['group']=searchService.getGroups()
                drillParams = searchService.createSearchQuery([cohortCompare:true],map,15, 1, clientId, null,null, "msZakipoint","","totalPaidAmount","desc")
                jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
                def newJSON =JSON.parse(jsonData);
                newJSON=newJSON["newMetrics"]
                newJSON.each{key,val->
                    if(!parsedJSON[key]){
                        parsedJSON[key]=newJSON[key]
                    }
                }

//                    def drillParams = searchService.createSearchQuery([cohortCompare:true],map,15, 1, clientId, null,null, "msZakipoint","","totalPaidAmount","desc")
//                    def jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
//                    def parsedJSON=JSON.parse(jsonData);
                    parsedJSON.cohortName=v.name;
                    parsedJSON.members_drill= ["cohortKey":v.cohortId,"reportingFrom":reportingDates.priorFrom,"reportingTo":reportingDates.currentTo];
                    dataComparison[i]=parsedJSON;
                    dataContainer.add(dataComparison);

            }



            if (counter>7){
                render(view: "../cohort/cohortCompare",model: [data:dataComparison,isEditable: false])
            }
            else{
                render(view: "../cohort/cohortVisCompare",model: [data:dataComparison as JSON,isEditable: false,reportingDates:reportingDates])

            }







        }else{
            params.domain="cohortComparator"

            def dynamicReportParam,data;
            dynamicReportParam = dynamicParamsGeneratorService.setCohortParams(params, session, [:])

            data = reportFetchService.getCohortComparatorList(dynamicReportParam, "list")

            if(!data){
                flash.message="Data not available for ${controllerName.toString().capitalize()}."
                redirect(action: 'index',controller: "dashboard")
                return false
            }

            data=data.sort{it.displayOrder};

            [list:data,isEditable:false]
        }




    }


    def pTrackingDetails(){


    }

    def explorer(){
        forward(controller: "cohort",action: "index")
//        render(view:"oeTest")
    }

    def explorers(){
        render(view:"oeTest")
    }

    def explorer1(){
        render(view:"oeTest1")
    }



}
