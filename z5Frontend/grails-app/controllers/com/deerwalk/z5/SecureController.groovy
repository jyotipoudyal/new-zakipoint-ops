package com.deerwalk.z5

class SecureController {

    def receptor = {
        def message = "In secure receptor..."
        response.status = 200  //OK
        render (message : message)
    }


}
