package com.deerwalk.z5

import com.deerwalk.security.ClientLogo
import com.deerwalk.security.ClientLogoConfiguration
import com.deerwalk.security.DwClient
import com.deerwalk.security.User
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

//@Mixin(HighPharmacyCostController)
class DashboardController {

    def springSecurityService
    def messageSource
    def localeResolver
    def displayService
    def fetchService
    def dashBoardChartService
    def helperService
    def accountSettingsService
    def grailsApplication
    def populationRiskService
    def memberDrillService
    def mailService
    def dynamicParamsGeneratorService
    def groovyPageRenderer

    /*def client(){
        session.client="ims"
        render "session=>"+session.client
    }*/



    def reportingDetail(){
//        def currentMap=servletContext.getAt("parameters");
        def reportingDates=helperService.getReportingDates(session.ceDate)
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
//        params.range=params.range
//        params.reportingBasis=params.reportingBasis
//        params.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"featureInfo")
        params.eligibleMemberCount=dataGraph.featureInfo.eligibleMemberCount
        params.overAllMemberCount=dataGraph.featureInfo.overAllMemberCount
//        params.isExclude=(params.isExclude=="true")?"Excluding":"Including"
        params.isExclude=(params.isExclude=="true")?true:false
//        currentMap.range=params.range
//        currentMap.isExclude=params.isExclude
        if(session.clientname){
            def client=DwClient.findByName(session.clientname)
//            params.groupName=client.description

        }

        render(template: "reportingDetails",model:[parameters:params])
    }



    def index() {
//        applicationContext.getBean("displayService");


        def dashBoardParams = session.dashBoardParams?:[:]
        dashBoardParams.reportingBasis="PaidDate"
        def reportingDates;
        def data
        if(session.ceDate){
            dashBoardParams.csDate=session.csDate
            dashBoardParams.ceDate=session.ceDate
            reportingDates=helperService.getReportingDates(dashBoardParams.ceDate)
            dashBoardParams.reportingFrom=reportingDates.currentFrom
            dashBoardParams.reportingTo=reportingDates.currentTo
//            dashBoardParams.clientId=servletContext.getAt("clientIdentity")
            def dynamicReportParams = dashBoardChartService.setDynamicReportParams(dashBoardParams,session,[:])
            data = dashBoardChartService.getDashBoardChartScript(dynamicReportParams,"groupMemberCountView")
            if(!data) data=[:]


            if(session.clientname){
                def client=DwClient.findByName(session.clientname)
                if(data){
                    data.groupDetail.groupName=client.description
                }
            }
        }else{
            data=[:]
        }
        [isEditable:false,group:data?.groupDetail]
    }

    def getChartScriptForDashBoard(){
        def chartType=params.chart
        def dashBoardParams = session.dashBoardParams?:[:]


        dashBoardParams.reportingBasis="PaidDate"
        dashBoardParams.csDate=session.csDate
        dashBoardParams.ceDate=session.ceDate

        def reportingDates=helperService.getReportingDates(dashBoardParams.ceDate)
        dashBoardParams.reportingFrom=reportingDates.priorFrom
        dashBoardParams.reportingTo=reportingDates.currentTo
        dashBoardParams.range="0"
        dashBoardParams.isExclude="false"
//        dashBoardParams.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParams = dashBoardChartService.setDynamicReportParams(dashBoardParams,session,[:])

        def data;
        try{
            if(chartType=="render1") data = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParams,"pharmacyClaimsPmpm")
            if(chartType=="render2") data = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParams,"medicalClaimsPmpm")
            if(chartType=="render3"){
                dashBoardParams.reportingFrom=reportingDates.currentFrom
                dashBoardParams.reportingTo=reportingDates.currentTo
                dynamicReportParams=dashBoardChartService.setDynamicReportParams(dashBoardParams,session,[:])
                data = dashBoardChartService.getDashBoardChartScript(dynamicReportParams,"populationRisk")
                dashBoardParams.pastMonth=reportingDates.pastMonth
                memberDrillService.getPopulationRiskDashParams(data,true,'member',dashBoardParams)


            }
            if(chartType=="render4"){
                dashBoardParams.reportingFrom=reportingDates.currentFrom
                dashBoardParams.reportingTo=reportingDates.currentTo
//                dynamicReportParams=dashBoardChartService.setDynamicReportParams(dashBoardParams,session,[:])
//                data = dashBoardChartService.getDashBoardChartScript(dynamicReportParams,"populationRiskByTopChronicCondition")
            }

            if(chartType=="render1"){
                def dataPh=data.getAt('reporting')
                def dataBm=data.getAt('benchmark').getAt("Pharmacy Claims PMPM")
                data=dataPh
                data.benchmark=dataBm
            }

            if(chartType=="render2"){
                def dataPh=data.getAt('reporting')
                def dataBm=data.getAt('benchmark').getAt("Medical Claims PMPM")
                data=dataPh
                data.benchmark=dataBm
            }
            if(chartType=="render3"){
                data.fromDate=reportingDates.currentFrom
                data.toDate=reportingDates.currentTo
            }

            render data as JSON

        }catch(e){
            log.error(e.getMessage())
            render status: 204;
        }

    }

    def changePasswd(){}

    def setClient(){
        //only enabled for navigation client
        /* String requestHost = request.getHeader('Host')

         def clientName=requestHost.toString().split("\\.")[0];
         if(clientName!=grailsApplication.config.grails.navigationUrl){
             redirect(controller: "dashboard",action: "index")
         }*/
        if(params.client){
//            println "springSecurityService = "+springSecurityService?.principal?.id

            def user=User.findById(springSecurityService?.principal?.id)
//          springSecurityService.reauthenticate()
            def dwClients=params.client
            session.clientname=dwClients
            if(!params.url || params.client=="usermanagement"|| params.client=="usermanagement_dev"){
                params.url="";
            }
//            SpringSecurityUtils.reauthenticate user.username, null
            def url="${grailsApplication.config.grails.protocol}://"+dwClients+"${grailsApplication.config.grails.hostPath}"+params.url;
            redirect(url: url)
        }else{
            def user=User.findById(springSecurityService?.principal?.id)
            ArrayList<DwClient> clients=user.clients.sort{it.name}
            def bob=clients.find{it.name==grailsApplication.config.grails.navigationUrl}
            if(bob){
                clients.remove(bob)
            }

            if(clients.size()==1){
                def url="${grailsApplication.config.grails.protocol}://"+clients[0].name.toLowerCase()+"${grailsApplication.config.grails.hostPath}";
                redirect(url: url)
            }else{
                [clients:clients,isEditable:false]
            }

        }

    }


    def viewForClientGroup(){
        /*String requestHost = request.getHeader('Host')
        println "requestHost = $requestHost"
        def clientName=requestHost.toString().split("\\.")[0];
        def clientView=false;
        if(clientName==grailsApplication.config.grails.bobUrl){
            clientView=true;
        }
        def user=User.findById(springSecurityService?.principal?.id)
        def appendList="";
        if(clientView){
            def clients=user.clients.sort{it.name}
            appendList="<ul>";
//            clients.each {
                appendList+= "<li class=\"clientSelect\" onclick=\"clientSelect(this)\"><a href=\"#\">Group I</a></li>\n";
                appendList+= "<li class=\"clientSelect\" onclick=\"clientSelect(this)\"><a href=\"#\">Group J</a></li>\n";
//            }
            appendList+="</ul>";
        }else{
            appendList="";

            *//*" <ul>\n" +
                    "        <li class=\"groupSelect ${session.group['Group I'] == '1' ? 'active' : ''}\"><a href=\"#\">Group I</a></li>\n" +
                    "        <li class=\"groupSelect ${session.group['Group J'] == '1' ? 'active' : ''}\"><a href=\"#\">Group J</a></li>\n" +
                    "        </ul>"*//*
        }*/

        def user=User.findById(springSecurityService?.principal?.id)
        ArrayList<DwClient> clients=user.clients.sort{it.name}
        def bob=clients.find{it.name==grailsApplication.config.grails.navigationUrl}
        if(bob){
            clients.remove(bob)
        }
//        def clients=user.clients.sort{it.name}
        render(template: "clientBox",model:[clients:clients,url:params.url.toString().trim()])
    }

    def selectGroup(){
        session.group[params.id]=params.flag
        session.group.each{key,val->
            if(key==params.id){
                session.group[key]=params.flag
            }else{
                session.group[key]=0
            }
        }
        render true
    }

    def selectClient(){

    }

    def clientLogo(){
        def clientList = DwClient.findByName(session.clientname)
        def logoInfo = ClientLogoConfiguration.findByClient(clientList)
        def clientLogo = ClientLogo.get(logoInfo?.clientLogo?.id)
        response.setHeader("Content-disposition", "attachment; filename=logo")
        response.contentType = 'image/png'
        response.outputStream << clientLogo.logoFile
        response.outputStream.flush()
        return;
    }

    def sendCohortMail() {
        def user=springSecurityService.currentUser
        def client=DwClient.findByName(session.clientname)

            try {
//                MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request
//                MultipartFile file = multiRequest.getFile('cohortIcon')
//                if (!file.empty) {
                    mailService.sendMail {
                        to "support@zakipoint.com"
                        from user.username
                        subject "New Cohort Request"
                        html g.render(template:"cohortMailTemplate1",model:[user:user.username,client: client.description, name:user.fullName,cohortName:params.cohortName,cohortDesc:params.cohortDesc])
//                        attachBytes 'cohortLogo.jpg','image/jpg', file.getBytes()
                    }

                render true

            } catch (e) {
                println(e.toString())
                render false
            }



    }

    def brandLogo(){
        def userBrand=UserCobrandConfig.findByUser(springSecurityService.currentUser)
            response.setHeader("Content-disposition", "attachment; filename=logo")
            response.contentType = 'image/png'
            response.outputStream << userBrand.brand.logoFile
            response.outputStream.flush()
            return;


    }

    def clientBrandLogo(){
        def clientBrand=ClientCobrandConfig.findByClient(DwClient.findByName(session.clientname))
        response.setHeader("Content-disposition", "attachment; filename=logo")
        response.contentType = 'image/png'
        response.outputStream << clientBrand.brand.logoFile
        response.outputStream.flush()
        return;


    }


    def authenticate() {
        def userInstance = User.findById(springSecurityService?.principal?.id)
        def curPassword  = params.currentPassword
        def newPassword1 = params.newPassword1
        def newPassword2 = params.newPassword2
        def oldPassword  = userInstance?.password
        def passwordMatched = oldPassword == curPassword?.encodeAsMD5()
        def newEncoded  = newPassword1.encodeAsMD5()

//        def passwordAge = UserDwClient.executeQuery("select max(dw.passwordAge)from UserDwClient udc inner join udc.clientDw dw where udc.userDw=?", [userInstance])[0]

        if (newPassword1 != newPassword2) {
            flash.message = "The new passwords do not match"
            redirect(action: "changePasswd")
        } else if (!passwordMatched) {
            flash.message = "Current password is incorrect"
            redirect(action: "changePasswd")
        } else if (!validatePassword(newPassword1)) {
            flash.message = "The password has to contain at least one digit, one special character, one lowercase letter and one uppercase letter.\n The password length should be greater than 8 characters"
            redirect(action: "changePasswd")
        }/*else if(accountSettingsService.isMaximumPasswordAge(userInstance)){
            flash.message = "You cannot change your password for ${passwordAge} more days."
            redirect(action: "changePassword")
        }else if(accountSettingsService.isOldPassword(session.currentClient ,userInstance, newPassword1)){
            flash.message = "ERROR! You cannot set your old password. Please enter a new password"
            redirect(action: "changePassword")
        }*/
        else {
            userInstance.password = newEncoded
            userInstance.passwordExpirationDate = new Date() + 30
            userInstance.temporaryPassword=null

            try {
                userInstance.merge(flush: true, validate: true, failOnError: true)
                accountSettingsService.updateUserPassword(userInstance,newPassword1)
//                flash.message = "Password has been changed successfully."
                flash.success = true
                flash.error=true

            } catch (e) {
                flash.message = "Password was not Changed!"
                flash.error=false
            }
            redirect(action: "changePasswd")
        }
    }

    /*
     Taken from: http://www.zorched.net/2009/05/08/password-strength-validation-with-regular-expressions/
    */
    Boolean validatePassword(password) {
        (password != null) && (password ==~ /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])(?=.*[\d]).*$/)
    }


    def checkUrl = {
        if (servletContext.getAttribute('USER_URL_LOGGING')) {
            session.setAttribute("LOG-URL", true)
            def urls
            if (params.clearAll == 'true') {
                session.setAttribute("LOGGED-URL", [])
            }
            urls = session.getAttribute("LOGGED-URL")
            if (urls) {
                [urls: urls]
            } else {
                [:]
            }
        } else {
            flash.message = "Check Url is not enabled"
            redirect(controller: 'dashboard', action: 'index')
        }
    }


    def  export(){
//        List reportDetails = makeUpSomeDemoData()
//        List reportDetails=[new ExamplePersonForReport(name: 'Amy', email: 'amy@example.com',points: 2),
//                  new ExamplePersonForReport(name: 'asas', email: 'ram@example.com',points: 45)];
//        chain(controller: 'jasper', action: 'index', model: [data: reportDetails], params: params)
        /*JasperReportDef rep = jasperService.buildReportDefinition(params,request.locale,reportDetails)
        ByteArrayOutputStream stream = jasperService.generateReport(rep)
        response.setHeader("Content-disposition", "attachment; filename=" + 'fileName' + ".pdf")
        response.contentType = "application/pdf"
        response.outputStream << stream.toByteArray()*/
    }


    def gapInCare(){
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.range=params.range?params.range:"0"
        params.isExclude=params.isExclude?params.isExclude:"false"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        def dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"gapInCare")
       /* GapInCareReport rg = (GapInCareReport)dynamicParamsGeneratorService.getReportGeneratorInstance("gapInCare",params,dynamicReportParam);
        def columns = rg.getGapInCareBenchmarksColumns();
        def data1;
        try {
            data1 = rg.getFinalData();
            if(!data1)
                throw new Exception("No data received")
        } catch (Exception e){
            e.printStackTrace()
            flash.message = "No data received for specified parameters!"
//            redirect(action: "index")
            return
        }*/

        def dataRealTime = populationRiskService.getPopulationRiskScript(dynamicReportParam,"gapInCareRealtime")


        dataRealTime.each{k,v->
            /*def findVal=dataGraph.getAt(k)
            if(findVal)
                v=findVal;*/
            dataGraph[k]=v;
        }

        List<String> arr=new ArrayList<String>();
        for(val in dataGraph){
            if(!arr.contains(val.value.category.toString().trim())){
                arr.push(val.value.category.toString().trim())
            }
        }

        def List<Object> careFor = new ArrayList<Object>();
        def hoverItems=populateGapInCareHoverItems();
        dataGraph.each { id, datas ->

            if(dataRealTime[id]){
                datas['members_drill']=memberDrillService.getRealTimeDrillParamsForQm(datas,true,'member',params)
            }else{
                datas['members_drill']=memberDrillService.getDrillParams(datas,true,'member',params)
            }

            def titleData=hoverItems.find{it['Name'].trim().equals(datas.name.trim())}
            if(datas.name=="additionalGaps.1")
            println "titleData = $titleData"
            if(titleData)
            datas.title=titleData?.Numerator+"&#010;________________________________________________________&#010;&#010;"+titleData?.Denominator
            else datas.title="*No data"
            careFor.add(datas)
        }







        if((request.xhr)){
            render(template: "gapInCareTable",model:[hoverItems:hoverItems,isEditable:false,careFor:dataGraph.values().toArray(),defaultSort:"sortable sorted desc",arr:arr.sort(),careForData: (dataGraph.values() as JSON)])
        }else{
            render(view:"gapInCare",model:[hoverItems:hoverItems,isEditable:false,careFor:dataGraph.values().toArray(),defaultSort:"sortable sorted desc",arr:arr.sort(),careForData: (dataGraph.values() as JSON)])
        }
    }



    def populateGapInCareHoverItems(){
        List<Map> data=new ArrayList<HashMap>();



        data = [

                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Aged 65 years and older on high risk drug",
                            "metricType": "negative",
                            "Name": "additionalGaps.1",
                            "category": "Additional Gaps",
                            "QM_ID": "X01",
                            "Denominator": "Aged 65 years and older",
                            "Numerator": "on high risk drug",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "On statin drug without both an ALT and an AST in the past 12 months",
                            "metricType": "negative",
                            "Name": "additionalGaps.2",
                            "category": "Additional Gaps",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "No monthly PT/INR for warfarin users",
                            "metricType": "negative",
                            "Name": "additionalGaps.3",
                            "category": "Additional Gaps",
                            "QM_ID": "G01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "No PCP visit last 12 months",
                            "metricType": "negative",
                            "Name": "additionalGaps.4",
                            "category": "Additional Gaps",
                            "QM_ID": "G01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Return to hospital with same asthma diagnosis within 30 days following inpatient discharge",
                            "metricType": "negative",
                            "Name": "asthma.1",
                            "category": "Asthma",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently eligible and does not have a diagnosis of COPD or Emphysema and has had an inpatient admission with a diagnosis of asthma within the last 6 months of the report period.",
                            "Correct numerator": "A claim for an inpatient admission for an asthma diagnosis <= 30 days from previous asthma inpatient stay discharge date.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Visit to an ED/Urgent care office for asthma in the past 6 months",
                            "metricType": "negative",
                            "Name": "asthma.2",
                            "category": "Asthma",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema.",
                            "Correct numerator": "A claim for an asthma related Emergency Department or urgent care office visit in last 180 days of the report period",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Asthma and a routine provider visit in the last 12 months",
                            "metricType": "positive",
                            "Name": "asthma.3",
                            "category": "Asthma",
                            "QM_ID": "E03",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema.",
                            "Numerator": "A claim for a routine provider visit in last 365 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Children with asthma-related acute visit in the past two months",
                            "metricType": "negative",
                            "Name": "asthma.4",
                            "category": "Asthma",
                            "QM_ID": "E09",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is less than 19 years old, is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema.",
                            "Correct numerator": "A claim for an asthma related Emergency Department or urgent care office visit in last 60 days of the data set/measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members with asthma currently taking a prescription medication for asthma",
                            "metricType": "positive",
                            "Name": "asthma.5",
                            "category": "Asthma",
                            "QM_ID": "G01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema.",
                            "Correct numerator": "A pharmacy claim for an asthma medication in the last 180 days of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Asthma with pneumococcal vaccination",
                            "metricType": "positive",
                            "Name": "asthma.6",
                            "category": "Asthma",
                            "QM_ID": "E06",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema.",
                            "Numerator": "A claim for a pneumococcal vaccination in the claims data.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Two or more asthma related ER Visits in the last 6 months",
                            "metricType": "negative",
                            "Name": "asthma.7",
                            "category": "Asthma",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema",
                            "Correct numerator": "2 or more occurrences of Emergency Room visit in the last 6 months of the data/measurement period for an asthma related diagnosis.   ER Visit claims with the same date of service will count as one",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Asthma related admit in last 12 months",
                            "metricType": "negative",
                            "Name": "asthma.8",
                            "category": "Asthma",
                            "QM_ID": "E03",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema.",
                            "Numerator": "A claim for a routine provider visit in last 365 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "Claims for an inpatient admission for asthma diagnosis in the last 12 months of the report period.   [An asthma diagnosis must be listed on the inpatient claim]",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Asthma with influenza vaccination in last 12 months",
                            "metricType": "positive",
                            "Name": "asthma.9",
                            "category": "Asthma",
                            "QM_ID": "E03",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema.",
                            "Numerator": "A claim for a routine provider visit in last 365 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Correct numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Persistent asthma with annual pulmonary function test",
                            "metricType": "positive",
                            "Name": "asthma.10",
                            "category": "Asthma",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of persistent asthma and does not have a diagnosis of COPD or Emphysema",
                            "Correct numerator": "A claim for a pulmonary function test in the last 12 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Received control inhaler (long acting) in last 12 months",
                            "metricType": "positive",
                            "Name": "asthma.11",
                            "category": "Asthma",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of persistent asthma and does not have a diagnosis of COPD or Emphysema",
                            "Correct numerator": "A claim for a long acting control inhaler in the last 12 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Received rescue inhaler (short acting) in last 12 months",
                            "metricType": "positive",
                            "Name": "asthma.12",
                            "category": "Asthma",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of persistent asthma and does not have a diagnosis of COPD or Emphysema",
                            "Correct numerator": "A claim for a short acting rescue inhaler in the last 12 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "canister short-acting inhaled beta agonist/months",
                            "metricType": "negative",
                            "Name": "asthma.13",
                            "category": "Asthma",
                            "QM_ID": "E09",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. The member also has claims for an asthma medication in the last 110 days of the report period.",
                            "Correct numerator": "Greater than one pharmacy claim for a short-acting beta agonist in the last 30 days of the measurement period.  (NOTE: If a member receives more than one pharmacy claim for a short-acting inhaled beta-2 agonist on the same date of service it should only count as one canister for this metric --- i.e. member might get one for use at home and 1 for use at work at the same time.)",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual lipid profile",
                            "metricType": "positive",
                            "Name": "CAD.1",
                            "category": "CAD",
                            "QM_ID": "A01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of CAD and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "On anti-platelet medication",
                            "metricType": "positive",
                            "Name": "CAD.2",
                            "category": "CAD",
                            "QM_ID": "F04",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hypertension and member's age is between 18 and 90 years and member has been eligible for the last 12 months.",
                            "Numerator": "A claim for a serum creatinine test in the last 13 months of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of CAD and member's age is between 18 and 75 years",
                            "Correct numerator": "One or more fills of an anti-platelet drug in the last 120 days of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "On lipid lowering medication",
                            "metricType": "positive",
                            "Name": "CAD.3",
                            "category": "CAD",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of CAD and member's age is between 18 and 75 years.",
                            "Correct numerator": "One or more fills of a lipid lowering drug in the last 120 days of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "On anti-platelets medication",
                            "metricType": "positive",
                            "Name": "CAD.4",
                            "category": "CAD",
                            "QM_ID": "F04",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hypertension and member's age is between 18 and 90 years and member has been eligible for the last 12 months.",
                            "Numerator": "A claim for a serum creatinine test in the last 13 months of the measurement period.",
                            "Correct denominator": "Duplicate - same as line 20",
                            "Correct numerator": "Same as line 20",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Re-admission to hospital with COPD diagnosis within 30 days following a COPD inpatient stay discharge",
                            "metricType": "negative",
                            "Name": "COPD.1",
                            "category": "COPD",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and has had an Inpatient hospital admission for a diagnosis of COPD in last 6 months of the report period.",
                            "Correct numerator": "A claim for an inpatient admission for a COPD diagnosis <= 30 days from previous COPD inpatient stay discharge date.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members with COPD who have had a visit to an ER for COPD related diagnosis in the past 90 days",
                            "metricType": "negative",
                            "Name": "COPD.2",
                            "category": "COPD",
                            "QM_ID": "G01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of COPD.",
                            "Correct numerator": "A claim for an Emergency Room visit in last 90 days of the report period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Exacerbations in last 12 months",
                            "metricType": "negative",
                            "Name": "COPD.3",
                            "category": "COPD",
                            "QM_ID": "D08",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years, plus either member has diagnosis of CVD or member's age is greater than or equal to 40 years.",
                            "Numerator": "No pharmacy encounters for Statin drug in last 100 days of measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of COPD.",
                            "Correct numerator": "An exacerbation of a member’s COPD during the last 365 days of the measurement period.  An “exacerbation “ is defined as an acute inpatient stay or an ER Visit with a primary diagnosis of COPD.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Individuals over the age of 20 years with diagnosed COPD are on bronchodilator therapy",
                            "metricType": "positive",
                            "Name": "COPD.4",
                            "category": "COPD",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member's age is greater than or equal to 20. Member is currently enrolled/eligible and has a diagnosis of COPD.",
                            "Correct numerator": "Dispensed prescription for a bronchodilator medication in the last 180 days of the data.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members with COPD who had an annual spirometry test",
                            "metricType": "positive",
                            "Name": "COPD.5",
                            "category": "COPD",
                            "QM_ID": "G01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of COPD.",
                            "Correct numerator": "One or more encounters for spirometry in the last 395 days of the report period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual flu vaccination",
                            "metricType": "positive",
                            "Name": "COPD.6",
                            "category": "COPD",
                            "QM_ID": "W02",
                            "Denominator": "Member is currently enrolled/eligible and member's age is greater than or equal to 50 years but less than 65 years and member has no encounters for flu vaccination exclusion in their data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and has a diagnosis of COPD. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Exacerbation with inhaled bronchodilator/ corticosteroid last 12 months",
                            "metricType": "positive",
                            "Name": "COPD.7",
                            "category": "COPD",
                            "QM_ID": "D08",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years, plus either member has diagnosis of CVD or member's age is greater than or equal to 40 years.",
                            "Numerator": "No pharmacy encounters for Statin drug in last 100 days of measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible. Member's age is greater than or equal to 40 years. Member has had an inpatient admission or an ER Visit with a primary diagnosis of COPD in the last 12 months of the report period.",
                            "Correct numerator": "Dispensed prescription for corticosteroid and or a bronchodilator within 30 days of discharge from COPD related admission or the date of service of a COPD related ER Visit in the last 365 days of the data.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual dilated eye exam",
                            "metricType": "positive",
                            "Name": "diabetes.1",
                            "category": "Diabetes",
                            "QM_ID": "D01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for eye during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual foot exam",
                            "metricType": "positive",
                            "Name": "diabetes.2",
                            "category": "Diabetes",
                            "QM_ID": "D02",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for foot exam during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual HbA1c test done",
                            "metricType": "positive",
                            "Name": "diabetes.3",
                            "category": "Diabetes",
                            "QM_ID": "D03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for HgA1c test during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual lipid profile",
                            "metricType": "positive",
                            "Name": "diabetes.4",
                            "category": "Diabetes",
                            "QM_ID": "D04",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Claims for home glucose testing supplies in last 12 months",
                            "metricType": "positive",
                            "Name": "diabetes.5",
                            "category": "Diabetes",
                            "QM_ID": "E09",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Correct numerator": "Member has at least one claim for glucose testing supplies/equipment during the last 365 days of the measurement period",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual microalbumin urine screen",
                            "metricType": "positive",
                            "Name": "diabetes.6",
                            "category": "Diabetes",
                            "QM_ID": "D06",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Numerator": "Urine albumin/protein screening test ormedical claim for nephropathy in last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "Urine albumin/protein screening test or medical claim for nephropathy in last 380 days of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual LDL-C screening",
                            "metricType": "positive",
                            "Name": "diabetes.7",
                            "category": "Diabetes",
                            "QM_ID": "D07",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for LDL-C test during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Diabetes with CVD or >40 yrs with CVD risks not on statin",
                            "metricType": "negative",
                            "Name": "diabetes.8",
                            "category": "Diabetes",
                            "QM_ID": "E09",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years,  plus either member has diagnosis of CVD or member's age is greater than or equal to 40 years.",
                            "Correct numerator": "No pharmacy encounters for Statin drug in last 100 days of measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "LDL<100mg/dL",
                            "metricType": "positive",
                            "Name": "diabetes.9",
                            "category": "Diabetes",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Correct numerator": "A claim indicating that the LDL was <100 mg/dL by specific code or a LDL value of <100 mg/dL on an HRA in the last 12 months of the data.  NOTE: if more than one claim or HRA exists for the member in the last 12 months, use the most recent result for determination of whether the member meets or does not meet the numerator.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "LDL<130mg/dL",
                            "metricType": "positive",
                            "Name": "diabetes.10",
                            "category": "Diabetes",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years",
                            "Correct numerator": "A claim indicating that the LDL was <130 mg/dL by specific code or a LDL value of <130 mg/dL on an HRA or biometric value in the last 12 months of the data.  NOTE: if more than one claim or HRA exists for the member in the last 12 months, use the most recent result for determination of whether the member meets or does not meet the numerator.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "BP<130/80 mmHg",
                            "metricType": "positive",
                            "Name": "diabetes.11",
                            "category": "Diabetes",
                            "QM_ID": "E09",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Correct numerator": "A claim indicating that the BP was <130/80 mm Hg by specific code or a BP value of <130/80 mm Hg on an HRA or in biometrics in the last 12 months of the data.  NOTE: if more than one claim, HRA or biometric BP value exists for the member in the last 12 months, use the most recent result for determination of whether the member meets or does not meet the numerator.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "BP<140/90 mmHg",
                            "metricType": "positive",
                            "Name": "diabetes.12",
                            "category": "Diabetes",
                            "QM_ID": "E09",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Correct numerator": "A claim indicating that the BP was <140/90 mm Hg by specific code or a BP value of <140/90 mm Hg on an HRA or in biometrics in the last 12 months of the data.  NOTE: if more than one claim, HRA or biometric BP value exists for the member in the last 12 months, use the most recent result for determination of whether the member meets or does not meet the numerator.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "HbA1c<7.0%",
                            "metricType": "positive",
                            "Name": "diabetes.13",
                            "category": "Diabetes",
                            "QM_ID": "D08",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years, plus either member has diagnosis of CVD or member's age is greater than or equal to 40 years.",
                            "Numerator": "No pharmacy encounters for Statin drug in last 100 days of measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years and member has had one or more HbA1c test done in the past 24 months.",
                            "Correct numerator": "A claim indicating that the HbA1c was <7.0% by specific code or an HbA1c value of 6.9 or less biometric or HRA result in the last 12 months of the data.  NOTE: if more than one claim or biometric exists for the member in the last 12 months, use the most recent result for the member.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "HbA1c<8.0%",
                            "metricType": "positive",
                            "Name": "diabetes.14",
                            "category": "Diabetes",
                            "QM_ID": "D14",
                            "Denominator": "Diabetes",
                            "Numerator": "HbA1c<8.0%",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years and member has had one or more HbA1c test done in the past 24 months.",
                            "Correct numerator": "A claim indicating that the HbA1c was <8.0% by specific code or an HbA1c value of 7.9 or less biometric or HRA in the last 12 months of the data.  NOTE: If more than one claim or biometric exists for the member in the last 12 months, use the most recent result for the member.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "HbA1c>9.0%",
                            "metricType": "negative",
                            "Name": "diabetes.15",
                            "category": "Diabetes",
                            "QM_ID": "D15",
                            "Denominator": "Diabetes",
                            "Numerator": "HbA1c>9.0%",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years and member has had one or more HbA1c test done in the past 24 months.",
                            "Correct numerator": "A claim indicating that the HbA1c was >9.0% by specific code or an HbA1c value of 9.0 or greater biometric or HRA in the last 12 months of the data.  NOTE: if more than one claim or biometric exists for the member in the last 12 months, use the most recent result for the member.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Smoking Status/Cessation Advice/Treatment",
                            "metricType": "positive",
                            "Name": "diabetes.16",
                            "category": "Diabetes",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years.",
                            "Correct numerator": "One or more encounters for smoking status, advice or treatment during the last 24 months.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Heart failure and atrial fibrillation on warfarin therapy",
                            "metricType": "positive",
                            "Name": "CHF.1",
                            "category": "CHF",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible. Member's age is greater than or equal to 18 years. Member has a diagnosis of Heart Failure and a diagnosis of Atrial Fibrillation.",
                            "Correct numerator": "One or more claims for Warfarin therapy in the last 120 days of the measurement period/data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Heart failure and LVSD on ACE/ARB",
                            "metricType": "positive",
                            "Name": "CHF.2",
                            "category": "CHF",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible. Member's age is greater than or equal to 18 years. Member has a diagnosis of Heart Failure and a diagnosis of LVSD.",
                            "Correct numerator": "One or more claims for an ACE inhibitor or ARB drug in the last 120 days of the measurement period/data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Heart failure and LVSD on beta-blocker",
                            "metricType": "positive",
                            "Name": "CHF.3",
                            "category": "CHF",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible. Member's age is greater than or equal to 18 years. Member has a diagnosis of Heart Failure and a diagnosis of LVSD.",
                            "Correct numerator": "One or more claims for Beta Blocker therapy in the last 120 days of the measurement   period/data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Re-admission to hospital with Heart Failure diagnosis within 30 days following a HF inpatient stay discharge",
                            "metricType": "negative",
                            "Name": "CHF.4",
                            "category": "CHF",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has an Inpatient Hospital admission for a diagnosis of Heart Failure in last 6 months of the report period.",
                            "Correct numerator": "A claim for an inpatient admission for a Heart Failure diagnosis <= 30 days from previous Heart Failure inpatient stay discharge date.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "ER Visit for Heart Failure in last 90 days",
                            "metricType": "negative",
                            "Name": "CHF.5",
                            "category": "CHF",
                            "QM_ID": "E09",
                            "Denominator": "Member is currently enrolled/eligible and has a diagnosis of asthma and does not have a diagnosis of COPD or Emphysema. There are also no encounters for flu vaccination exclusion in the member's data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Heart Failure.",
                            "Correct numerator": "A claim for an Emergency Department visit in last 90 days of the data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Follow-up OV within 4 weeks of discharge from HF admission",
                            "metricType": "positive",
                            "Name": "CHF.6",
                            "category": "CHF",
                            "QM_ID": "D08",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years, plus either member has diagnosis of CVD or member's age is greater than or equal to 40 years.",
                            "Numerator": "No pharmacy encounters for Statin drug in last 100 days of measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has an Inpatient Hospital admission for a diagnosis of Heart Failure in last 6 months of the report period.",
                            "Correct numerator": "One or more encounters for follow-up visit within 28 days after hospital discharge date.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Hyperlipidemia Annual lipid profile",
                            "metricType": "positive",
                            "Name": "hyperlipidemia.1",
                            "category": "Hyperlipidemia",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has an Inpatient Hospital admission for a diagnosis of Heart Failure in last 6 months of the report period.",
                            "Correct numerator": "One or more encounters for follow-up visit within 28 days after hospital discharge date.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "On lipid-lowering medication",
                            "metricType": "positive",
                            "Name": "hyperlipidemia.2",
                            "category": "Hyperlipidemia",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years.",
                            "Correct numerator": "One or more fills of prescription for antihyperlipidemic medication in the last 120 days of the measurement period",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Prescribed statin and gaps in prescription refills",
                            "metricType": "negative",
                            "Name": "hyperlipidemia.3",
                            "category": "Hyperlipidemia",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years and member has had a fill of a statin drug in the last 120 days of the report period.",
                            "Correct numerator": "For this metric you will look at the last statin drug refill date; add the “days supply” for that pharmacy claim to that date; then if the next refill of the statin drug is more than 5 days past the expected fill date, it is a gap.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Two or more ER Visits in the last 6 months",
                            "metricType": "negative",
                            "Name": "utilization.1",
                            "category": "Utilization",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible.",
                            "Correct numerator": "2 or more occurrences of Emergency Room visit in the last 6 months of the data/measurement period.   ER Visit claims with the same date of service will count as one occurrence.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Hospital readmission within 30 days of discharge",
                            "metricType": "negative",
                            "Name": "utilization.2",
                            "category": "Utilization",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a Hospital admission in the last 180 days of the report period.",
                            "Correct numerator": "A claim for an inpatient admission with a date of service <= 30 days from previous acute inpatient stay discharge date.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Office visit within 30 days of inpatient discharge",
                            "metricType": "positive",
                            "Name": "utilization.3",
                            "category": "Utilization",
                            "QM_ID": "G01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hyperlipidemia and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a Hospital admission in the last 6 months of the report period.",
                            "Correct numerator": "One or more encounters for follow-up visit within 30 days after hospital discharge date.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "3 or more ER Visits in the last 6 months",
                            "metricType": "negative",
                            "Name": "utilization.4",
                            "category": "Utilization",
                            "QM_ID": "U04",
                            "Denominator": "Member is currently enrolled/eligible.",
                            "Numerator": "3 or more occurrences of Emergency Room visit in the last 6 months of the data/measurement period. Multiple ER Visit claims with the same date of service will count as one occurrence.",
                            "Correct denominator": "Both are correct but spacing issue. Fixed it.",
                            "Correct numerator": "Both are correct but spacing issue. Fixed it.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "On antihypertensive medication",
                            "metricType": "positive",
                            "Name": "hypertension.1",
                            "category": "Hypertension",
                            "QM_ID": "F01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hypertension and member's age is greater than or equal to 18 years.",
                            "Numerator": "One or more fills of antihypertensive medication in last 120 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual lipid profile",
                            "metricType": "positive",
                            "Name": "hypertension.2",
                            "category": "Hypertension",
                            "QM_ID": "A01",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of CAD and member's age is between 18 and 75 years.",
                            "Numerator": "One or more encounters for lipid profile during the last 380 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Hypertension diagnosis and prescribed statin and gaps in prescription refills",
                            "metricType": "negative",
                            "Name": "hypertension.3",
                            "category": "Hypertension",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hypertension and member's age is between 18 and 75 years and member has had a fill of a statin drug in the last 120 days of the report period.",
                            "Correct numerator": "For this metric you will look at the last statin drug refill date in the claims data; add the “days supply” for that pharmacy claim to that date to get an expected next fill date; then if the next refill of the statin drug is more than 5 days past the expected fill date, it is a gap.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual serum creatinine test",
                            "metricType": "positive",
                            "Name": "hypertension.4",
                            "category": "Hypertension",
                            "QM_ID": "F04",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hypertension and member's age is between 18 and 90 years and member has been eligible for the last 12 months.",
                            "Numerator": "A claim for a serum creatinine test in the last 13 months of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Postpartum visit 21 to 56 days after delivery",
                            "metricType": "positive",
                            "Name": "pregnancy.1",
                            "category": "Pregnancy",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member's gender is female and member is currently enrolled and member has had a live birth in the report period. Member must have been continuously eligible with no gaps in coverage from 43 days prior to delivery and 56 days after delivery.",
                            "Correct numerator": "A claim for postpartum visit on or between 21 and 56 days after delivery.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Timeliness of Prenatal Care",
                            "metricType": "positive",
                            "Name": "pregnancy.2",
                            "category": "Pregnancy",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member's gender is female and member is currently enrolled and member has had a live birth in the report period. Member must have been continuously eligible with no gaps in coverage from 43 days prior to delivery and 56 days after delivery.",
                            "Correct numerator": "A claim for postpartum visit on or between 21 and 56 days after delivery.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Age 50 to 64 with annual flu vaccination",
                            "metricType": "positive",
                            "Name": "wellness.2",
                            "category": "Wellness",
                            "QM_ID": "W02",
                            "Denominator": "Member is currently enrolled/eligible and member's age is greater than or equal to 50 years but less than 65 years and member has no encounters for flu vaccination exclusion in their data.",
                            "Numerator": "One or more encounters for flu vaccination during the last 395 days of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Age 50 to 75 years with colorectal cancer screening",
                            "metricType": "positive",
                            "Name": "wellness.3",
                            "category": "Wellness",
                            "QM_ID": "W03",
                            "Denominator": "Member is currently enrolled/eligible and member's age is greater than or equal to 50 years but less than or equal to 75 years and member does not meet the criteria for colorectal cancer screening exclusion.",
                            "Numerator": "One or more encounters for colorectal cancer screening in the last 24 months of the data.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Women age 21 years and older with cervical cancer screen last 24 months",
                            "metricType": "positive",
                            "Name": "wellness.4",
                            "category": "Wellness",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member is female and member's age is greater than or equal to 21 years and member does not meet the criteria for cervical cancer screening exclusion.",
                            "Correct numerator": "One or more encounters for cervical cancer screening during the last 24 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Males age greater than 49 with PSA test in last 24 months",
                            "metricType": "positive",
                            "Name": "wellness.5",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "",
                            "Correct numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Women age 65 and older with screening for osteoporosis",
                            "metricType": "positive",
                            "Name": "wellness.6",
                            "category": "Wellness",
                            "QM_ID": "X04",
                            "Denominator": "Women age 65 and older",
                            "Numerator": "Screening for osteoporosis",
                            "Correct denominator": "Member is currently enrolled/eligible and member is female and member's age is greater than or equal to 65 years during the report period.",
                            "Correct numerator": "One or more encounters for osteoporosis screening in the data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Routine exam in last 24 months",
                            "metricType": "positive",
                            "Name": "wellness.7",
                            "category": "Wellness",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible.",
                            "Correct numerator": "One or more outpatient claims with a preventive/routine office visit in the last 24 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Women age 40 to 69 with a screening mammogram last 24 months",
                            "metricType": "positive",
                            "Name": "wellness.8",
                            "category": "Wellness",
                            "QM_ID": "W08",
                            "Denominator": "Member is currently enrolled/eligible and member is female and member's age is between (inclusive) 40 and 64 years and there are no encounters for mastectomy exclusion during the measurement period.",
                            "Numerator": "One or more encounters for screening mammogram during the last 24 months of the data set.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Lead Screening in Children",
                            "metricType": "positive",
                            "Name": "wellness.9",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is 2 years.",
                            "Correct numerator": "A claim for a lead blood test on or before his/her second birthday.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual well-child exam (age 2 to 6 yrs)",
                            "metricType": "positive",
                            "Name": "wellness.10",
                            "category": "Wellness",
                            "QM_ID": "W10",
                            "Denominator": "Member is currently enrolled/eligible and member's age is from 2 to 6 years.",
                            "Numerator": "A claim for at least one well-child exam in last 12 months of the reporting period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual well-child exam (age 7 to 12 yrs)",
                            "metricType": "positive",
                            "Name": "wellness.11",
                            "category": "Wellness",
                            "QM_ID": "W11",
                            "Denominator": "Member is currently enrolled/eligible and member's age is from 7 to 12 years.",
                            "Numerator": "A claim for at least one well-child exam in last 12 months of the reporting period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Annual well-child exam (age 13 to 21 yrs)",
                            "metricType": "positive",
                            "Name": "wellness.12",
                            "category": "Wellness",
                            "QM_ID": "W12",
                            "Denominator": "Member is currently enrolled/eligible and member's age is from 13 to 21 years.",
                            "Numerator": "A claim for at least one well-child exam in last 12 months of the reporting period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Age 2 - 6 yrs with recommended immunizations",
                            "metricType": "positive",
                            "Name": "wellness.13",
                            "category": "Wellness",
                            "QM_ID": "W13",
                            "Denominator": "Member is currently enrolled/eligible and member had second birthday prior to the start of the report period and member's age is less than or equal to 6 at the end of the report period and member does not been the criteria for immunization exclusion.",
                            "Numerator": "Claims in the data for all of the following: DTaP: At least 1 DTaP vaccination \n IPV: At least 1 IPV vaccinations\n MMR: At least 1 MMR vaccination \n Varicella: At least 1 varicella vaccination",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Age 13 to 21 yrs with recommended immunizations",
                            "metricType": "positive",
                            "Name": "wellness.14",
                            "category": "Wellness",
                            "QM_ID": "W14",
                            "Denominator": "Member is currently enrolled/eligible and member had 13th birthday prior to the start of the report period and member's age is less than or equal to 21 years at the end of the report period and member does not been the criteria for immunization exclusion.",
                            "Numerator": "Claims for at least 1 Tdap vaccination during the report period or1 tetanus, diphtheria toxoids vaccine (Td)and at least 1 meningococcal vaccination during the report period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Age 13 yrs with recommended immunizations",
                            "metricType": "positive",
                            "Name": "wellness.15",
                            "category": "Wellness",
                            "QM_ID": "W15",
                            "Denominator": "Member is currently enrolled/eligible and member had 13th birthday during the report period and member has been continuously enrolled for 12 months prior to their 13th birthday and member does not been the criteria for immunization exclusion.",
                            "Numerator": "Claims for at least 1 Tdap vaccination during the report period or1 tetanus, diphtheria toxoids vaccine (Td)and at least 1 meningococcal vaccination during the report period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Age 2 yrs with recommended immunizations",
                            "metricType": "positive",
                            "Name": "wellness.16",
                            "category": "Wellness",
                            "QM_ID": "W16",
                            "Denominator": "Member is currently enrolled/eligible and member had 2nd birthday during the report period and member has been continuously enrolled for 12 months prior to their 2nd birthday and member does not been the criteria for immunization exclusion.",
                            "Numerator": "All of the following: DTaP: At least 4 DTaP vaccinations, with different dates of service on or before the child?s second birthday. Do not count a vaccination administered prior to 42 days after birth. \n IPV: At least 3 IPV vaccinations, with different dates of service on or before the child?s second birthday. Do not count a vaccination administered prior to 42 days after birth.\n MMR: At least 1 MMR vaccination, with a date of service falling on or before the child?s second birthday.\n HiB: At least 3 Hib vaccinations, with different dates of service on or before the child?s second birthday. HiB administered prior to 42 days after birth cannot be counted. \n Hepatitis B: At least 3 hepatitis B vaccinations, with different dates of service on or before the child?s second birthday. \n VZV: At least one VZV vaccination, with a date of service falling on or before the child?s second birthday.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Well Child Visit - 15 months",
                            "metricType": "positive",
                            "Name": "wellness.17",
                            "category": "Wellness",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member turned 15 months old during the report period.",
                            "Correct numerator": "A claim for at least one well-child exam in first 15 months of life.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Infant - 1 or more Well Child Visit",
                            "metricType": "positive",
                            "Name": "wellness.18v",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is greater than 1 month but less than 13 months.",
                            "Correct numerator": "A claim for at least one well-child exam in last 12 months of the reporting period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Infant - Non-Well Child Visit Only",
                            "metricType": "negative",
                            "Name": "wellness.19",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is greater than 1 month but less than 13 months and member has no claims for a well child exam.",
                            "Correct numerator": "A claim for at least one non-well child office visit in last 12 months of the reporting period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Infant - Well & Non-Well Office Visit",
                            "metricType": "positive",
                            "Name": "wellness.20",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is greater than 1 month but less than 13 months and member has a claim for a well child exam.",
                            "Correct numerator": "A claim for at least one non-well child office visit in last 12 months of the reporting period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Routine office visit in last 6 months",
                            "metricType": "positive",
                            "Name": "wellness.21",
                            "category": "Wellness",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible.",
                            "Correct numerator": "A claim for a routine provider visit in last 6 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Women age 21 years and older with cervical cancer screen last 36 months",
                            "metricType": "positive",
                            "Name": "wellness.22",
                            "category": "Wellness",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member is female and member's age is greater than or equal to 21 years and member does not meet the criteria for cervical cancer screening exclusion.",
                            "Correct numerator": "One or more encounters for cervical cancer screening during the last 36 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Glaucoma screening in last 24 months (age 65 yrs. & older)",
                            "metricType": "positive",
                            "Name": "wellness.23",
                            "category": "Wellness",
                            "QM_ID": "D08",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Diabetes and member's age is between 18 and 75 years, plus either member has diagnosis of CVD or member's age is greater than or equal to 40 years.",
                            "Numerator": "No pharmacy encounters for Statin drug in last 100 days of measurement period.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is greater than or equal to 65 years and member does not meet the glaucoma screening exclusion criteria.",
                            "Correct numerator": "A claim for glaucoma screening eye exam in the last 24 months of the report period by an eye care specialist. [ophthalmologist",
                            "": "optometrist]"
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members aged 19 years to 39 with preventive visit in last 24 months",
                            "metricType": "positive",
                            "Name": "wellness.24",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is 19 to 39 years in the report period.",
                            "Correct numerator": "One or more outpatient claims with a preventive/routine office visit in the last 24 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members aged 40 years to 64 years with preventive visit in last 24 months",
                            "metricType": "positive",
                            "Name": "wellness.25",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is 40 to 64 years in the report period.",
                            "Correct numerator": "One or more outpatient claims with a preventive/routine office visit in the last 24 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Women age 21-65 with recommended cervical cancer screening",
                            "metricType": "positive",
                            "Name": "wellness.26",
                            "category": "Wellness",
                            "QM_ID": "W05",
                            "Denominator": "Member is currently enrolled/eligible and member is male and member's age is greater than or equal to 50 years and member does not meet the criteria for PSA test exclusion.",
                            "Numerator": "One or more encounters for PSA test during the last 24 months of the data set.",
                            "Correct denominator": "Member is currently enrolled/eligible and member is female and member's age is greater than or equal to 21 years and less than or equal to 65 years and member does not meet the criteria for cervical cancer screening exclusion.",
                            "Correct numerator": "One or more encounters for pap smear during the last 3 years of the measurement period in women 21-65 years of age, or one or more encounters for both pap smear and human papillomavirus (HPV) during the last 5 years of the measurement period in women 30-65 years of age.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members age 19 to 39 years with cholesterol screening is done",
                            "metricType": "positive",
                            "Name": "wellness.27",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is 19 to 39 years in the report period.",
                            "Correct numerator": "One or more encounters for cholesterol screening in the data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members age 40 to 64 years with cholesterol screening is done",
                            "metricType": "positive",
                            "Name": "wellness.28",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is 40-64 years in the report period.",
                            "Correct numerator": "One or more encounters for cholesterol screening in the data set.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "Members aged 65 years and older with an annual preventive visit",
                            "metricType": "positive",
                            "Name": "wellness.29",
                            "category": "Wellness",
                            "QM_ID": "H03",
                            "Denominator": "Member is currently enrolled/eligible and member has a diagnosis of Hepatitis C and member's age is greater than or equal to 18 years and member has claim for antiviral medication in last 12 months of the report period.",
                            "Numerator": "A claim for a HCV RNA test within 180 days prior to first fill (initiating) of anti-viral medication.",
                            "Correct denominator": "Member is currently enrolled/eligible and member's age is greater than or equal to 65 in the report period",
                            "Correct numerator": "One or more outpatient claims with a preventive/routine office visit in the last 12 months of the measurement period.",
                            "": ""
                        ],
                        [
                            "description": "",
                            "metricType": "",
                            "Name": "",
                            "category": "",
                            "QM_ID": "",
                            "Denominator": "",
                            "Numerator": "",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ],
                        [
                            "description": "On Disease-Modifying Anti-Rheumatic Drugs (DMARD)",
                            "metricType": "positive",
                            "Name": "rheumatoidArthritis.1",
                            "category": "RA",
                            "QM_ID": "R01",
                            "Denominator": "Member is currently enrolled/eligible and member's age is greater than or equal to 18 years and member has a diagnosis of Rheumatoid Arthritis and member does not have an exclusion condition.",
                            "Numerator": "One or more fills of a DMARD in the last 12 months of the measurement period.",
                            "Correct denominator": "",
                            "Correct numerator": "",
                            "": ""
                        ]
                ]



        return data;
    }


    def imagingServices(){
        def list=new ArrayList();
        def procedure=[

                [
                        "Procedure Group": "Cholecystectomy Laparoscopic",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 50000,
                        "Average Cost Per Event (Annual Increase %)": "\$2,550 - \$9,500 (5%)",
                        "Max":9500,
                        "Standard Cost": 5000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:5],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:5]]
                ],

                [
                        "Procedure Group": "Cardiac Cath",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 200000,
                        "Average Cost Per Event (Annual Increase %)": "\$11,000 - \$41,000 (5%)",
                        "Max":41000,
                        "Standard Cost": 20000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:10],[x:"\$300-350",y:5],[x:"\$350-400",y:4],[x:"\$400+",y:4]]
                ],

                [
                        "Procedure Group": "Back Surgery",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 1000000,
                        "Average Cost Per Event (Annual Increase %)": "\$50,000 - \$150,000 (5%)",
                        "Max":150000,
                        "Standard Cost": 100000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:2],[x:"\$300-350",y:2],[x:"\$350-400",y:14],[x:"\$400+",y:7]]
                ],

                [
                        "Procedure Group": "Lumpectomy",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 150000,
                        "Average Cost Per Event (Annual Increase %)": "\$10,000 - \$20,000 (5%)",
                        "Max":20000,
                        "Standard Cost": 15000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]

                ],

                [
                        "Procedure Group": "CABG",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 1200000,
                        "Average Cost Per Event (Annual Increase %)": "\$30,000 - \$200,000 (5%)",
                        "Max":200000,
                        "Standard Cost": 120000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]
                ],

                [
                        "Procedure Group": "Bilateral Mastectomy",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 300000,
                        "Average Cost Per Event (Annual Increase %)": "\$15,000 - \$55,000 (5%)",
                        "Max":55000,
                        "Standard Cost": 30000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]

                ],

                [
                        "Procedure Group": "Prostatectomy",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 750000,
                        "Average Cost Per Event (Annual Increase %)": "\$10,000 - \$135,000 (5%)",
                        "Max":135000,
                        "Standard Cost": 75000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]
                ],

                [
                        "Procedure Group": "Total Knee Replacement",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 220000,
                        "Average Cost Per Event (Annual Increase %)": "\$20,212 - \$23,581 (5%)",
                        "Max":23581,
                        "Standard Cost": 22000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]

                ],

                [
                        "Procedure Group": "Total Hip Replacement",
                        "Provider Type": "Surgery",
                        "Number of Events": 10,
                        "Total Cost": 210000,
                        "Average Cost Per Event (Annual Increase %)": "\$20,212 - \$23,581 (5%)",
                        "Max":23581,
                        "Standard Cost": 21000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]
                ],

                [
                        "Procedure Group": "MRI",
                        "Provider Type": "Imaging",
                        "Number of Events": 200,
                        "Total Cost": 280000,
                        "Average Cost Per Event (Annual Increase %)": "\$525 - \$1,600 (5%)",
                        "Max":1600,
                        "Standard Cost": 1400,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]

                ],

                [
                        "Procedure Group": "CT Scan",
                        "Provider Type": "Imaging",
                        "Number of Events": 200,
                        "Total Cost": 600000,
                        "Average Cost Per Event (Annual Increase %)": "\$270 - \$4,800 (5%)",
                        "Max":4800,
                        "Standard Cost": 3000,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]
                ],

                [
                        "Procedure Group": "X-ray",
                        "Provider Type": "Imaging",
                        "Number of Events": 200,
                        "Total Cost": 64000,
                        "Average Cost Per Event (Annual Increase %)": "\$260 - \$460 (5%)",
                        "Max":460,
                        "Standard Cost": 320,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]
                ],

                [
                        "Procedure Group": "USG",
                        "Provider Type": "Imaging",
                        "Number of Events": 200,
                        "Total Cost": 100000,
                        "Average Cost Per Event (Annual Increase %)": "\$100 - \$1000 (5%)",
                        "Max":1000,
                        "Standard Cost": 500,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]

                ],

                [
                        "Procedure Group": "Nuclear medicine",
                        "Provider Type": "Imaging",
                        "Number of Events": 200,
                        "Total Cost": 260000,
                        "Average Cost Per Event (Annual Increase %)": "\$500 - \$2,000 (5%)",
                        "Max":2000,
                        "Standard Cost": 1300,
                        "Top Providers": "Provider1, Provider2, Provider3, Provider4, Provider5, Provider6, Provider7, Provider8, Provider9, Provider10",
                        "Graph":[[x:"\$250-300",y:12],[x:"\$300-350",y:8],[x:"\$350-400",y:4],[x:"\$400+",y:2]]
                ]

        ]


        
        
        def providerLists=[
                [
                    "Provider ID": "Provider 1",
                    "Provider Name": "Provider 1",
                    "Provider Location": "Cambridge, MA",
                    "Number of Events": 54,
                    "Total Cost": "77814",
                    "Cost per proecdure": "1441"
                ],
                [
                    "Provider ID": "Provider 2",
                    "Provider Name": "Provider 2",
                    "Provider Location": "Cambridge, MA",
                    "Number of Events": 11,
                    "Total Cost": "32043",
                    "Cost per proecdure": "2913"
                ],
                [
                    "Provider ID": "Provider 3",
                    "Provider Name": "Provider 3",
                    "Provider Location": "Newton, MA",
                    "Number of Events": 49,
                    "Total Cost": "285670",
                    "Cost per proecdure": "5830"
                ],
                [
                    "Provider ID": "Provider 4",
                    "Provider Name": "Provider 4",
                    "Provider Location": "Wellesley, MA",
                    "Number of Events": 69,
                    "Total Cost": "448776",
                    "Cost per proecdure": "6504"
                ],
                [
                    "Provider ID": "Provider 5",
                    "Provider Name": "Provider 5",
                    "Provider Location": "Wellesley, MA",
                    "Number of Events": 88,
                    "Total Cost": "263472",
                    "Cost per proecdure": "2994"
                ],
                [
                    "Provider ID": "Provider 6",
                    "Provider Name": "Provider 6",
                    "Provider Location": "Wellesley, MA",
                    "Number of Events": 89,
                    "Total Cost": "144091",
                    "Cost per proecdure": "1619"
                ],
                [
                    "Provider ID": "Provider 7",
                    "Provider Name": "Provider 7",
                    "Provider Location": "Wellesley, MA",
                    "Number of Events": 87,
                    "Total Cost": "579507",
                    "Cost per proecdure": "6661"
                ],
                [
                    "Provider ID": "Provider 8",
                    "Provider Name": "Provider 8",
                    "Provider Location": "Waltham, MA",
                    "Number of Events": 50,
                    "Total Cost": "240900",
                    "Cost per proecdure": "4818"
                ],
                [
                    "Provider ID": "Provider 9",
                    "Provider Name": "Provider 9",
                    "Provider Location": "Arlington, MA",
                    "Number of Events": 47,
                    "Total Cost": "286230",
                    "Cost per proecdure": "6090"
                ],
                [
                    "Provider ID": "Provider 10",
                    "Provider Name": "Provider 10",
                    "Provider Location": "Belomnt, MA",
                    "Number of Events": 83,
                    "Total Cost": "438655",
                    "Cost per proecdure": "5285"
                ],
                [
                    "Provider ID": "Provider 11",
                    "Provider Name": "Provider 11",
                    "Provider Location": "Woburn, MA",
                    "Number of Events": 61,
                    "Total Cost": "187514",
                    "Cost per proecdure": "3074"
                ],
                [
                    "Provider ID": "Provider 12",
                    "Provider Name": "Provider 12",
                    "Provider Location": "Arlington, MA",
                    "Number of Events": 91,
                    "Total Cost": "206,570",
                    "Cost per proecdure": "2270"
                ],
                [
                    "Provider ID": "Provider 13",
                    "Provider Name": "Provider 13",
                    "Provider Location": "Alriogton, MA",
                    "Number of Events": 21,
                    "Total Cost": "96915",
                    "Cost per proecdure": "4615"
                ],
                [
                    "Provider ID": "Provider 14",
                    "Provider Name": "Provider 14",
                    "Provider Location": "Western, MA",
                    "Number of Events": 26,
                    "Total Cost": "143676",
                    "Cost per proecdure": "5526"
                ],
                [
                    "Provider ID": "Provider 15",
                    "Provider Name": "Provider 15",
                    "Provider Location": "Western, MA",
                    "Number of Events": 28,
                    "Total Cost": "166824",
                    "Cost per proecdure": "5958"
                ]
        ]
        render(view:"imaging",model: [isEditable: false,data:procedure,providerLists:providerLists,providerListsGraph:providerLists.clone()  as JSON]);
    }

}
