package com.deerwalk.z5

import org.springframework.dao.DataIntegrityViolationException

class LogAnalyticsController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "logsAnalytics", params: params)
    }

    def logsAnalytics(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        def client=ClientPanel.findByClient(session.clientname)
        def clientlist=new ArrayList();
        if(client.isBob){
            clientlist=ClientPanel.createCriteria().list {
                projections {
                    property('client')
                }
                eq ('parent', client)
            }
        }
            clientlist.add(client.client);



        def userLogs=LogAnalytics.createCriteria().list(params){
//            eq "client",session.clientname
            'in'("client",clientlist)
        }


        def logSize=LogAnalytics.countByClientInList(clientlist)

        [logAnalyticsInstanceList: userLogs, logAnalyticsInstanceTotal: logSize]
    }

    def create() {
        [logAnalyticsInstance: new LogAnalytics(params)]
    }

    def save() {
        def logAnalyticsInstance = new LogAnalytics(params)
        if (!logAnalyticsInstance.save(flush: true)) {
            render(view: "create", model: [logAnalyticsInstance: logAnalyticsInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), logAnalyticsInstance.id])
        redirect(action: "show", id: logAnalyticsInstance.id)
    }

    def show(Long id) {
        def logAnalyticsInstance = LogAnalytics.get(id)
        if (!logAnalyticsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), id])
            redirect(action: "logAnalytics")
            return
        }

        [logAnalyticsInstance: logAnalyticsInstance]
    }

    def edit(Long id) {
        def logAnalyticsInstance = LogAnalytics.get(id)
        if (!logAnalyticsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), id])
            redirect(action: "logAnalytics")
            return
        }

        [logAnalyticsInstance: logAnalyticsInstance]
    }

    def update(Long id, Long version) {
        def logAnalyticsInstance = LogAnalytics.get(id)
        if (!logAnalyticsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), id])
            redirect(action: "logsAnalytics")
            return
        }

        if (version != null) {
            if (logAnalyticsInstance.version > version) {
                logAnalyticsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'logAnalytics.label', default: 'LogAnalytics')] as Object[],
                        "Another user has updated this LogAnalytics while you were editing")
                render(view: "edit", model: [logAnalyticsInstance: logAnalyticsInstance])
                return
            }
        }

        logAnalyticsInstance.properties = params

        if (!logAnalyticsInstance.save(flush: true)) {
            render(view: "edit", model: [logAnalyticsInstance: logAnalyticsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), logAnalyticsInstance.id])
        redirect(action: "show", id: logAnalyticsInstance.id)
    }

    def delete(Long id) {
        def logAnalyticsInstance = LogAnalytics.get(id)
        if (!logAnalyticsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), id])
            redirect(action: "logsAnalytics")
            return
        }

        try {
            logAnalyticsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), id])
            redirect(action: "logsAnalytics")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'logAnalytics.label', default: 'LogAnalytics'), id])
            redirect(action: "show", id: id)
        }
    }
}
