package com.deerwalk.z5

import grails.converters.JSON
import grails.util.Holders
import report.HighOutNetworkUsageReport
import report.TopOonProviderLocationReport
import report.TopOonProviderSpecialityReport
import report.TopOutOfNetworkProviderReport

class NetworkUseController {

    def helperService
    def dynamicParamsGeneratorService
    def reportFetchService
    def dashBoardChartService




    def index() {
        [isEditable:false]
    }

    def getScriptForNetwork(){

//        def dash=Holders.getGrailsApplication().getMainContext().getBean("dashBoardChartService");
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.range=params.range?params.range:"0"
        params.isExclude=params.isExclude?params.isExclude:"false"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        //initialization of variables
        def dynamicReportParam,data,columns;
        //initialization complete

//        if(!params.module.equals("networkTrendingSankey")) return false;

        if(params.module.equals("highOutNetworkUsage")) {

            params.sort=params.sort?params.sort:"medicalOutNetworkPaid"
            params.order=params.order?params.order:"desc"
            params.sortOrder=params.sort+":"+params.order
            params.offset=params.offset?params.offset.toInteger():0;
            params.max=params.max?params.max.toInteger():5;

            if(params.offset==0){
                params.page=1
            }else if(params.offset>1){
                params.page=(params.offset/params.max)+1
            }
            params.page=params.page;
            params.pageSize=params.max

            dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params, session,[:])
            HighOutNetworkUsageReport rg = (HighOutNetworkUsageReport)dynamicParamsGeneratorService.getReportGeneratorInstance("highOutNetworkUsage", params,dynamicReportParam);
            columns= rg.getHighOutNetworkUsageColumns();
            def total;
            try {
                data = rg.getFinalData();
                if(!data)
                    throw new Exception("No data received")

                total = rg.getTotalCount()
                data.remove("metaInfo")
                if(params.order=="desc"){
                   data= data.sort{it[params.sort]}.reverse();
                }
                else{
                    data=data.sort{it[params.sort]}
                }
            } catch (Exception e){
                e.printStackTrace()
            }
            render(template: "highOutNetworkUsage",model:[data: data, columns: columns,size:total,defaultSort:params.sort,order:params.order,module:params.module])
            return

         }
        else if(params.module.equals("networkTrendingSankey")) {
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params, session, [:])
            def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam, "networkTrendingSankey")

            if(!dataGraph)
                throw new Exception("No data received")

            data = dataGraph

            def newMap=["prior_low":0,"prior_medium":1,"prior_high":2,
                        "last_low":3,"last_medium":4,"last_high":5,
                        "current_low":6,"current_medium":7,"current_high":8
                        ]

            def yearMap=["prior":reportingDates.priorTo.split("-")[0],"last":reportingDates.pastTo.split("-")[0],"current":reportingDates.currentTo.split("-")[0]]


            def dataErr=true;

            data.each{k,v->
                if(k.equals("nodes")){
                    v.each{

                        if(it['total']!=0){
                            dataErr=false;
                        }else{
                            it['total']=0.49;
                        }
                        it.each{key,val->
                            if(key=="node"){
                                it.put(key,newMap.getAt(val))
                            }
                        }
                    }
                }

                if(k.equals("links")){
                    v.each{
                        it.each{key,val->
                            if(key=="source"){
                                it.put(key,newMap.getAt(val))
                            }else if(key=="target"){
                                it.put(key,newMap.getAt(val))
                            }

                            if(it['value']==0){
                                it['value']=0.001;
                            }
                        }
                    }
                }
            }
            def newData=[:]
            if(dataErr){
                newData=null;
                throw new Exception("No data received")
                return
            }

            data.nodes.sort{it.node}
            newData.data=data;
            newData.year=yearMap;

            render newData as JSON
            return;

        }
        else if(params.module.equals("networkUsage")){
            def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            def datanew=[:]
            def dataPh=[]

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"networkUsage")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            dataPh.add(datanew)

            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"networkUsage")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            dataPh.add(datanew)

            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"networkUsage")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            dataPh.add(datanew)

            render dataPh as JSON
            return




        }
        else if(params.module.equals("topOutOfNetworkProvider")){
            params.sort=params.sort?params.sort:"totalPaidAmount"
            params.order=params.order?params.order:"desc"

            dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params,session,[:])
            TopOutOfNetworkProviderReport tn = (TopOutOfNetworkProviderReport)dynamicParamsGeneratorService.getReportGeneratorInstance("topOutOfNetworkProvider",params,dynamicReportParam);
            columns = tn.getTopOutOfNetworkProviderColumns();
            try {
                data = tn.getFinalData();
                if(!data)
                    throw new Exception("No data received")

                def sortField=columns[params.sort]['type'].toString()

                if(params.order=="desc"){
                    if(sortField=="Number" ||  sortField=="Amount"){
                        data= data.sort{ a,b -> a[params.sort].toInteger() <=> b[params.sort].toInteger()}.reverse();

                    }else{
                        data= data.sort{ a,b -> a[params.sort] <=> b[params.sort]}.reverse();
                    }

                }
                else{
                    if(sortField=="Number" ||  sortField=="Amount"){
                        data= data.sort{ a,b -> a[params.sort].toInteger() <=> b[params.sort].toInteger()};

                    }else{
                        data= data.sort{ a,b -> a[params.sort] <=> b[params.sort]};
                    }
                }

            } catch (Exception e){
                e.printStackTrace()
            }

        } else if(params.module.equals("topOonProviderLocation")){



            params.sort=params.sort?params.sort:"totalPaidAmount"
            params.order=params.order?params.order:"desc"


            dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params,session,[:])
            TopOonProviderLocationReport tn = (TopOonProviderLocationReport)dynamicParamsGeneratorService.getReportGeneratorInstance("topOonProviderLocation",params,dynamicReportParam);
            columns = tn.getTopOonProviderLocationColumns();
            try {
                data = tn.getFinalData();
                if(!data)
                    throw new Exception("No data received")

                def sortField=columns[params.sort]['type'].toString()

                if(params.order=="desc"){
                    if(sortField=="Number" ||  sortField=="Amount"){
                        data= data.sort{ a,b -> a.total[params.sort].toInteger() <=> b.total[params.sort].toInteger()}.reverse();

                    }else{
                        data= data.sort{ a,b -> a.total[params.sort] <=> b.total[params.sort]}.reverse();
                    }

                }
                else{
                    if(sortField=="Number" ||  sortField=="Amount"){
                        data= data.sort{ a,b -> a.total[params.sort].toInteger() <=> b.total[params.sort].toInteger()};

                    }else{
                        data= data.sort{ a,b -> a.total[params.sort] <=> b.total[params.sort]};
                    }
                }

            } catch (Exception e){
                e.printStackTrace()
            }



            /*if(params.order=="desc"){
                data= data.sort{it[params.sort]}.reverse();
            }
            else{
                data=data.sort{it[params.sort]}
            }*/

            render(template: "highOutNetworkUsage",model:[oondata: data, columns: columns,module:params.module,defaultSort:params.sort])
            return

        } else if(params.module.equals("topOonProviderSpeciality")){
            params.sort=params.sort?params.sort:"totalPaidAmount"
            params.order=params.order?params.order:"desc"
            dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params,session,[:])
            TopOonProviderSpecialityReport sr = (TopOonProviderSpecialityReport)dynamicParamsGeneratorService.getReportGeneratorInstance("topOonProviderSpeciality",params,dynamicReportParam);
            columns = sr.getTopOonProviderSpecialityColumns();
            try {
                data = sr.getFinalData();
                if(!data)
                    throw new Exception("No data received")

                def sortField=columns[params.sort]['type'].toString()

                if(params.order=="desc"){
                    if(sortField=="Number" ||  sortField=="Amount"){
                        data= data.sort{ a,b -> a.total[params.sort].toInteger() <=> b.total[params.sort].toInteger()}.reverse();

                    }else{
                        data= data.sort{ a,b -> a.total[params.sort] <=> b.total[params.sort]}.reverse();
                    }

                }
                else{
                    if(sortField=="Number" ||  sortField=="Amount"){
                        data= data.sort{ a,b -> a.total[params.sort].toInteger() <=> b.total[params.sort].toInteger()};

                    }else{
                        data= data.sort{ a,b -> a.total[params.sort] <=> b.total[params.sort]};
                    }
                }
            } catch (Exception e){
                e.printStackTrace()
            }
            render(template: "highOutNetworkUsage",model:[oondata: data, columns: columns,module:params.module,defaultSort:params.sort])
            return
        }

        render(template: "highOutNetworkUsage",model:[data: data, columns: columns,module:params.module])
        return

    }


}
