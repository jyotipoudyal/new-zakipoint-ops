package com.deerwalk.z5

/*import com.aspose.words.Document
import com.deerwalk.dwreports.MemberProfileReport
import com.deerwalk.reports.Reports*/
import grails.converters.JSON
import grails.util.Holders
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

//class MemberSearchController extends BaseController {
class MemberSearchController {
//	def beforeInterceptor = [action: this.&checkSession()]

	def fetchService
	def frontEndService
	def exportService
	def helperService
	def chartService
	def reportingService
	def memberProfileService
	def searchService
	def memberDrillService
	def pathService
//	def root = Holders.getGrailsApplication().getMainContext().getResource("/").getFile().getAbsolutePath()
//	def exportKey = System.currentTimeMillis()
	def criteriaService
	def reportFactoryService
	def dynamicParamsGeneratorService

	def index = {
		session.memberSearch=[:];
		session.memberParameter=(params.size()>2)?params:session.memberParameter;
//		println "it = "+session.memberParameter
		def parameters=session.memberParameter.clone();

		def expclass;
		if(parameters.expPop){
			if(parameters.getAt("currentStatus")=="Active"){
				expclass="active"
			}else{
				expclass="overall"
			}
		}


		if(!session.memberParameter){
			redirect(action: "index",controller: "dashboard")
			flash.message="Please apply drill for the current group"
			return
		}

		def dupParams=parameters.clone();
		def map=[:]


		if(parameters.range) {
			map.range = parameters.range
		}else{
			map.range =0
		}

		if(parameters.isExclude) {
			map.isExclude = parameters.isExclude
		}else{
			map.isExclude ="false"
		}

		map.reportingTo=parameters.reportingTo
		map.reportingFrom=parameters.reportingFrom
		map.isZakipoint=parameters.isZakipoint
		def queryType=parameters.queryType
		dupParams.remove('range')
		dupParams.remove('isExclude')
		dupParams.remove('queryType')
		dupParams.remove('reportingTo')
		dupParams.remove('reportingFrom')
		dupParams.remove('isZakipoint')

		if(parameters.expPop){
			dupParams.remove('expPop');
		}

		setQueryParameters(parameters,dupParams,map);

		// sankey end


		def group=	memberDrillService.getGroups()
		if(group){
			map.group=group
		}
		map.phiCSDate=session.csDate
		map.phiCEDate=session.ceDate
		def clientId=helperService.fetchClientId()

		def drillParams = searchService.createSearchQuery(dupParams,map, 15, 1, clientId, null,null, "msZakipoint",queryType,"totalPaidAmount","desc")
		def jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
		def parsedJSON=JSON.parse(jsonData);
		def rawJSON=parsedJSON.getAt('result_sets');
		def demoGraphics=parsedJSON;

//		rawJSON=rawJSON.sort{it.value.totalPaidAmount}
		rawJSON=rawJSON.sort{ a,b -> b.value.totalPaidAmount <=> a.value.totalPaidAmount}

//		params.ceDate=session.ceDate
		def reportingDates=helperService.getReportingDates(map.phiCEDate)
//		params.reportingFrom=reportingDates.currentFrom
//		params.reportingTo=reportingDates.currentTo

		def datesMap=["prior":reportingDates.priorTo,"last":reportingDates.pastTo,current:reportingDates.currentTo]

		def total=parsedJSON.getAt('summary').getAt('totalCounts');
//		def benchMark=parsedJSON.getAt('benchmark');



		//create inner drill params
		def expPopOverall=memberDrillService.getPopulationExplorer(helperService.getReportingDates(session.ceDate))
		def expPopActive=memberDrillService.getPopulationExplorer(helperService.getReportingDates(session.ceDate),true)


//		&escapeResultSet=true=======================================&escapeResultSet=true
		def newmap=map.clone();
		newmap['escapeResultSet']=true

		def drillParams2 = searchService.createSearchQuery(dupParams,newmap,15, 1, clientId, null,null, "msZakipoint",queryType,"totalPaidAmount","desc")
		def jsonData2 = fetchService.fetchMemberData(drillParams2, "membersearch")
		def parsedJSON2=JSON.parse(jsonData2);

		render (view: "memberSearch",model:[expPop:parameters.expPop,expclass:expclass,defaultSort:"sortable sorted desc",isEditable:false,data:rawJSON,data2:parsedJSON2,total: total,sn:0,demoGraphics:demoGraphics,datesMap:datesMap as JSON,expPopOverall:expPopOverall,expPopActive:expPopActive] )

	}

	def setQueryParameters(parameters,dupParams,map){

		if(parameters.cohortKey){
			dupParams.remove('cohortKey');
			map.cohortKey=parameters.cohortKey;
		}

		if(parameters.domainTask){
			dupParams.remove('domainTask');
			map.domainTask=parameters.domainTask;
		}
		if(parameters.task){
			dupParams.remove('task');
			map.task=parameters.task;
		}
		if(parameters.qmId){
			dupParams.remove('qmId');
			map.qmId=parameters.qmId;
		}

		if(parameters.criteriaMetFlag){
			dupParams.remove('criteriaMetFlag');
			map.criteriaMetFlag=parameters.criteriaMetFlag;
		}
		if(parameters.groupName){
			dupParams.remove('groupName');
			map.groupName=parameters.groupName;
		}


		if(parameters.recordTypes){
			dupParams.remove('recordTypes');
			map.recordTypes=parameters.recordTypes;
		}
		if(parameters.report){
			dupParams.remove('report');
			map.report=parameters.report
		}
		if(parameters.chronicCode){
			dupParams.remove('chronicCode');
			map.chronicCode=parameters.chronicCode
		}
		if(parameters.comorbidCode){
			dupParams.remove('comorbidCode');
			map.comorbidCode=parameters.comorbidCode
		}
		if(parameters.feature){
			dupParams.remove('feature');
			map.feature=parameters.feature
		}
		if(parameters.requestIndex){
			dupParams.remove('requestIndex');
			map.requestIndex=parameters.requestIndex
		}
		if(parameters.useMemberId){
			dupParams.remove('useMemberId');
			map.useMemberId=parameters.useMemberId
		}

		if(parameters.threeOrOne){
			dupParams.remove('threeOrOne');
			map.threeOrOne=parameters.threeOrOne
		}

		//for sankey drills

		if(parameters.period){
			dupParams.remove('period');
			map.period=parameters.period
		}
		if(parameters.level){
			dupParams.remove('level');
			map.level=parameters.level
		}

		if(parameters.drillType) {
			dupParams.remove('drillType');
			map.drillType = parameters.drillType
		}
		if(parameters.source) {
			dupParams.remove('source');
			map.source = parameters.source
		}
		if(parameters.target) {
			dupParams.remove('target');
			map.target = parameters.target
		}

		if(parameters.basis) {
			dupParams.remove('basis');
			map.basis = parameters.basis
		}
		if(parameters.exclude) {
			dupParams.remove('exclude');
			map.exclude = parameters.exclude
		}

		if(parameters.eligibilityType) {
			dupParams.remove('eligibilityType');
			map.eligibilityType = parameters.eligibilityType
		}

		if(parameters.category) {
			dupParams.remove('category');
			map.category = parameters.category
		}

		if(parameters.biometricName) {
			dupParams.remove('biometricName');
			map.biometricName = parameters.biometricName
		}
	}



	def searchResult = {
		def start = System.currentTimeMillis()

		session.memberSearch=[:];
		def paramsSize=params.size();
		if(params.areasToTarget || params.demographics){
			paramsSize=1;
		}

		session.memberParameter=(paramsSize>2)?params:session.memberParameter;
//		println "it = "+session.memberParameter
		def parameters=session.memberParameter.clone();



		def expclass;
		if(parameters.expPop){
			if(parameters.getAt("currentStatus")=="Active"){
				expclass="active"
			}else{
				expclass="overall"
			}
		}


		if(!session.memberParameter){
			redirect(action: "index",controller: "dashboard")
			flash.message="Please apply drill for the current group"
			return
		}

		def dupParams=parameters.clone();
		def map=[:]


		if(parameters.range) {
			map.range = parameters.range
		}else{
			map.range =0
		}

		if(parameters.isExclude) {
			map.isExclude = parameters.isExclude
		}else{
			map.isExclude ="false"
		}

		map.reportingTo=parameters.reportingTo
		map.reportingFrom=parameters.reportingFrom
		map.isZakipoint=parameters.isZakipoint
		def queryType=parameters.queryType
		dupParams.remove('range')
		dupParams.remove('isExclude')
		dupParams.remove('queryType')
		dupParams.remove('reportingTo')
		dupParams.remove('reportingFrom')
		dupParams.remove('isZakipoint')

		if(parameters.expPop){
			dupParams.remove('expPop');
		}

		//set parameters to append to query

		setQueryParameters(parameters,dupParams,map);



		// sankey end



		def group=	memberDrillService.getGroups()
		if(group){
			map.group=group
		}
		map.phiCSDate=session.csDate
		map.phiCEDate=session.ceDate
		def clientId=helperService.fetchClientId()



//		params.ceDate=session.ceDate
		def reportingDates=helperService.getReportingDates(map.phiCEDate)

		def datesMap=["prior":reportingDates.priorTo,"last":reportingDates.pastTo,current:reportingDates.currentTo]

		//create inner drill params
		def expPopOverall=memberDrillService.getPopulationExplorer(helperService.getReportingDates(session.ceDate))
		def expPopActive=memberDrillService.getPopulationExplorer(helperService.getReportingDates(session.ceDate),true)


		//		areas to target================================
		//		areas to target================================


		//demographics


		if((request.xhr)){
			if(params.areasToTarget){
				def newmap=map.clone();
				def drillParams2,jsonData2,areasToTarget=[:];
//				(2..6).each{
					newmap['requestIndex']=params.requestIndex;
					drillParams2 = searchService.createSearchQuery(dupParams,newmap,15, 1, clientId, null,null, "msZakipoint",queryType,"totalPaidAmount","desc")
					jsonData2 = fetchService.fetchMemberData(drillParams2, "membersearch")
					if(params.requestIndex=="9"){
						jsonData2=setSankeyParameters(jsonData2);
						areasToTarget["areas_${params.requestIndex}"] =jsonData2;

					}else{
						areasToTarget["areas_${params.requestIndex}"] =JSON.parse(jsonData2);

					}

//				}

				areasToTarget.datesMap=datesMap;


				render areasToTarget as JSON
			}

			if(params.demographics){
				def demomap=map.clone();
//				demomap['requestIndex']=3
				def drillParams3,jsonData3,demographics=[:];
//				(7..9).each{
					demomap['requestIndex']=params.requestIndex;
					drillParams3 = searchService.createSearchQuery(dupParams,demomap,15, 1, clientId, null,null, "msZakipoint",queryType,"totalPaidAmount","desc")
					jsonData3 = fetchService.fetchMemberData(drillParams3, "membersearch")
					demographics["demographics_${params.requestIndex}"]=JSON.parse(jsonData3);
//				}

				demographics.datesMap=datesMap;


				render demographics as JSON
			}


		}else{
			def populationMap=map.clone();
			if(!populationMap['requestIndex'])
			populationMap['requestIndex']=1
			def drillParams = searchService.createSearchQuery(dupParams,populationMap, 15, 1, clientId, null,null, "msZakipoint",queryType,"totalPaidAmount","desc")
			def jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
			def parsedJSON=JSON.parse(jsonData);
            //			def rawJSON=parsedJSON.getAt('result_sets');
			def rawJSON=parsedJSON;
			//		rawJSON=rawJSON.sort{it.value.totalPaidAmount}

			rawJSON=rawJSON.sort{ a,b -> b.value.totalPaidAmount <=> a.value.totalPaidAmount}
			def total=parsedJSON.getAt('summary').getAt('totalCounts');
			def now = System.currentTimeMillis()
			def time1=now - start
			println "time1 = $time1"

//			render (view: "memberSearch",model:[expPop:parameters.expPop,expclass:expclass,defaultSort:"sortable sorted desc",data2:areasToTarget,isEditable:false,data:rawJSON,data3:demographics,total: total,benchMark:benchMark,sn:0,datesMap:datesMap as JSON,expPopOverall:expPopOverall,expPopActive:expPopActive] )
			render (view: "memberSearch",model:[expPop:parameters.expPop,expclass:expclass,defaultSort:"sortable sorted desc",isEditable:false,data:rawJSON,total: total,sn:0,datesMap:datesMap as JSON,expPopOverall:expPopOverall,expPopActive:expPopActive] )

		}

		return;


	}


	def setSankeyParameters(def sankeyData){

		def data=JSON.parse(sankeyData);


		def reportingDates=helperService.getReportingDates(session.ceDate)

		def levelList=["High","Medium","Normal","Low"]
		def lev=new ArrayList();
		levelList.each{it->
			lev.push(it.trim());
		}

		def newMap=[:];


		def yearMap=["last":reportingDates.pastTo.split("-")[0],"current":reportingDates.currentTo.split("-")[0]]


		def dataErr=true;




		data['MARA_RISK_LEVEL']['reporting'].each { k, v ->
			if(k.equals("nodes")){
				v.each{
					if (it['total'] != 0) {
						dataErr = false;
					} else {
						it['total'] = 0.49;
					}

					newMap.put(it.node, it.id);
					it.put("node", it.id);

				}
			}

			if(k.equals("links")){
				v.each{
					it.each{key,val->
						if(key=="source"){
							it.put(key,newMap.getAt(val))
						}else if(key=="target"){
							it.put(key,newMap.getAt(val))
						}


						if(it['value']<=0){
							it['value']=0.001;
						}


					}
				}
			}
		}

		// hs data
		data['HS_RISK_LEVEL']['reporting'].each { k, v ->

			if(k.equals("nodes")){
				v.each{

					if (it['total'] != 0) {
						dataErr = false;
					} else {
						it['total'] = 0.49;
					}

					newMap.put(it.node, it.id);
					it.put("node", it.id);

				}
			}

			if(k.equals("links")){
				v.each{
					it.each{key,val->
						if(key=="source"){
							it.put(key,newMap.getAt(val))
						}else if(key=="target"){
							it.put(key,newMap.getAt(val))
						}


						if(it['value']==0){
							it['value']=0.001;
						}


					}
				}
			}
		}


		/*if(dataErr){
			diagramMap=null;
			throw new Exception("No data received !!!!!")
			return
		}*/

//		data.nodes.sort{it.node};


		yearMap=yearMap
		lev=lev.sort{it}


		data.levels=lev;
		data.years=yearMap;

		return data
	}




	def sortResult(){
		session.memberSearch.sort=params.sort
		session.memberSearch.order=params.order

		session.memberSearch.max=session.memberSearch.max?session.memberSearch.max:15
		session.memberSearch.page=session.memberSearch.page?session.memberSearch.page:1

		params.offset=0
		params.max=session.memberSearch.max


		def parameters=session.memberParameter.clone()

		def dupParams=parameters.clone();
		def map=[:]


		if(parameters.range) {
			map.range = parameters.range
		}else{
			map.range =0
		}
		if(parameters.isExclude) {
			map.isExclude = parameters.isExclude
		}else{
			map.isExclude ="false"
		}
		map.reportingTo=parameters.reportingTo
		map.reportingFrom=parameters.reportingFrom
		map.isZakipoint=parameters.isZakipoint
		def queryType=parameters.queryType

		dupParams.remove('range')
		dupParams.remove('queryType')
		dupParams.remove('reportingTo')
		dupParams.remove('reportingFrom')
		dupParams.remove('isZakipoint')


		setQueryParameters(parameters,dupParams,map);


		def group=	memberDrillService.getGroups()
		if(group){
			map.group=group
		}
		map.phiCSDate=session.csDate
		map.phiCEDate=session.ceDate
		def clientId=helperService.fetchClientId()
		map['requestIndex']=1

		def drillParams = searchService.createSearchQuery(dupParams,map,session.memberSearch.max, 1, clientId, null,null, "msZakipoint",queryType,params.sort,params.order)
		def jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
		def parsedJSON=JSON.parse(jsonData);
		def rawJSON=parsedJSON.getAt('result_sets');
		def total=parsedJSON.getAt('summary').getAt('totalCounts');

		if(params.order=="desc"){
			rawJSON=rawJSON.sort{ a,b -> b.value[params.sort] <=> a.value[params.sort]}
		}
		else{
			rawJSON=rawJSON.sort{ a,b -> a.value[params.sort] <=> b.value[params.sort]}
		}


		render(template: "tableDataDetails", model: [data:rawJSON,total:total,sn:0])
	}



	def remotePagination(){
		session.memberSearch.sort=session.memberSearch.sort?session.memberSearch.sort:"totalPaidAmount"
		session.memberSearch.order=session.memberSearch.order?session.memberSearch.order:"desc"
		params.sort=session.memberSearch.sort
		params.order=session.memberSearch.order


		params.offset=params.offset.toInteger()
		params.max=params.max.toInteger()
		session.memberSearch.max=params.max
		if(params.offset==0){
			session.memberSearch.page=1
		}else if(params.offset>1){
			session.memberSearch.page=(params.offset/params.max)+1
		}
		params.remotePaginate=true
		def parameters=session.memberParameter.clone()

		def dupParams=parameters.clone();
		def map=[:]


		if(parameters.range) {
			map.range = parameters.range
		}else{
			map.range =0
		}
		if(parameters.isExclude) {
			map.isExclude = parameters.isExclude
		}else{
			map.isExclude ="false"
		}
		map.reportingTo=parameters.reportingTo
		map.reportingFrom=parameters.reportingFrom
		map.isZakipoint=parameters.isZakipoint
		def queryType=parameters.queryType
		dupParams.remove('range')
		dupParams.remove('queryType')
		dupParams.remove('reportingTo')
		dupParams.remove('reportingFrom')
		dupParams.remove('isZakipoint')

		setQueryParameters(parameters,dupParams,map);

		// sankey end


		def group=	memberDrillService.getGroups()
		if(group){
			map.group=group
		}

		map.phiCSDate=session.csDate
		map.phiCEDate=session.ceDate
		def clientId=helperService.fetchClientId()
		map['requestIndex']=1

		def drillParams = searchService.createSearchQuery(dupParams,map, session.memberSearch.max, session.memberSearch.page, clientId, null,null, "msZakipoint",queryType,session.memberSearch.sort,session.memberSearch.order)
		def jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
		def parsedJSON=JSON.parse(jsonData);
		def rawJSON=parsedJSON.getAt('result_sets');
		def total=parsedJSON.getAt('summary').getAt('totalCounts');

		if(session.memberSearch.order=="desc"){
			rawJSON=rawJSON.sort{ a,b -> b.value[session.memberSearch.sort] <=> a.value[session.memberSearch.sort]}
		}
		else{
			rawJSON=rawJSON.sort{ a,b -> a.value[session.memberSearch.sort] <=> b.value[session.memberSearch.sort]}
		}

		render(template: "tableDataDetails", model: [data:rawJSON,total:total,sn:params.offset])

	}


	def exportPopulation(){
		session.memberSearch.sort=session.memberSearch.sort?session.memberSearch.sort:"totalPaidAmount"
		session.memberSearch.order=session.memberSearch.order?session.memberSearch.order:"desc"
		params.sort=session.memberSearch.sort
		params.order=session.memberSearch.order


		params.offset=0;
		params.max=params.total

		session.memberSearch.max=params.max

		params.remotePaginate=true
		def parameters=session.memberParameter.clone()

		def dupParams=parameters.clone();
		def map=[:]


		if(parameters.range) {
			map.range = parameters.range
		}else{
			map.range =0
		}
		if(parameters.isExclude) {
			map.isExclude = parameters.isExclude
		}else{
			map.isExclude ="false"
		}
		map.reportingTo=parameters.reportingTo
		map.reportingFrom=parameters.reportingFrom
		map.isZakipoint=parameters.isZakipoint
		def queryType=parameters.queryType
		dupParams.remove('range')
		dupParams.remove('queryType')
		dupParams.remove('reportingTo')
		dupParams.remove('reportingFrom')
		dupParams.remove('isZakipoint')

		setQueryParameters(parameters,dupParams,map);

		// sankey end


		def group=	memberDrillService.getGroups()
		if(group){
			map.group=group
		}

		map.phiCSDate=session.csDate
		map.phiCEDate=session.ceDate
		def clientId=helperService.fetchClientId()
		map['requestIndex']=1

		def drillParams = searchService.createSearchQuery(dupParams,map, params.max, 1, clientId, null,null, "msZakipoint",queryType,session.memberSearch.sort,session.memberSearch.order)
		def jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
		def parsedJSON=JSON.parse(jsonData);
		def rawJSON=parsedJSON.getAt('result_sets');
		def total=parsedJSON.getAt('summary').getAt('totalCounts');

		if(session.memberSearch.order=="desc"){
			rawJSON=rawJSON.sort{ a,b -> b.value[session.memberSearch.sort] <=> a.value[session.memberSearch.sort]}
		}
		else{
			rawJSON=rawJSON.sort{ a,b -> a.value[session.memberSearch.sort] <=> b.value[session.memberSearch.sort]}
		}


		params.format = "csv"
		response.contentType = grailsApplication.config.grails.mime.types["csv"]
		response.setHeader("Content-disposition", "attachment; filename=PopulationDetails.csv")

		def fields = [
				"unblindMemberId",
				"memberDOB",
				"memberAge",
				"memberGenderId",
				"relationshipFlag",
				"riskScore",
				"medTotalPaidAmount",
				"rxTotalPaidAmount",
				"totalPaidAmount",
				"numberOfChronicConditions",
				"chronicConditionList",
				"gapInCareCount",
				"gapInCareList",
				"memberFullName",
				"groupId"
		]

		def labels = ["unblindMemberId": "Member ID",
				  "memberDOB": "DOB",
				  "memberAge": "Age",
				  "memberGenderId": "Gender",
				  "relationshipFlag": "Relationship FLag",
                  "currentStatus": "Current Status",
				  "riskScore":"Risk Score",
				  "medTotalPaidAmount": "Med Paid Amount",
				  "rxTotalPaidAmount": "RX  Paid Amount",
				  "totalPaidAmount": "Total Paid Amount",
				  "numberOfChronicConditions": "# Of Chronic Conditions",
				  "chronicConditionList": "Chronic Conditions Name",
				  "gapInCareCount": "# Of Gap In Care",
				  "gapInCareList": "Gap In Care List",
				  "memberFullName": "Name",
				  "groupId": "Group"
		]

		def pmeters = ["column.widths": [0.1,0.40, 0.40, 0.40,0.40,0.40, 0.40, 0.40,0.40,0.40,0.40,0.80,0.40,0.80,0.40,0.40]]


		/*def fDate = { domain, value ->
			if (value)
				return value.format("yyyy")
		}

		def formatter = [memberDOB: fDate]*/

		ArrayList<String> list = new ArrayList<String>();
		JSONArray jsonArray = (JSONArray)rawJSON.values();
		for(int i=0;i < jsonArray.length();i++){
			def data=jsonArray.getJSONObject(i);
			data.groupId=session.clientname?session.clientname:"BOB";
			list.add(data);
		}


		exportService.export(params.format, response.outputStream, list, fields, labels, null, pmeters)
//		render(template: "tableDataDetails", model: [data:rawJSON,total:total,sn:params.offset])

	}



}

