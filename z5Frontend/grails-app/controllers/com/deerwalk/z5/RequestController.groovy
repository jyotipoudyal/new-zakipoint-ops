package com.deerwalk.z5

import grails.converters.JSON
import grails.util.Environment
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.springframework.security.cas.authentication.CasAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.request.RequestContextHolder

import javax.servlet.http.HttpServletRequest

class RequestController {
    def dwProxyService
    def grailsApplication
    def helperService
    def dashBoardChartService
    def searchService
    def index = {}


    def fetchData(){
        if(params.report){
            def dashBoardParams = session.dashBoardParams?:[:]

            dashBoardParams.reportingBasis=params.reportingBasis
            dashBoardParams.isExclude=params.isExclude
            dashBoardParams.range=params.range
            dashBoardParams.csDate=session.csDate
            dashBoardParams.ceDate=session.ceDate

            def reportingDates=helperService.getReportingDates(dashBoardParams.ceDate)
//            dashBoardParams.reportingFrom=reportingDates.currentFrom
//            dashBoardParams.reportingTo=reportingDates.currentTo
            dashBoardParams.reportingFrom=params.reportingFrom
            dashBoardParams.reportingTo=params.reportingTo
//            dashBoardParams.clientId=servletContext.getAt("clientIdentity")
            def dynamicReportParams = dashBoardChartService.setDynamicReportParams(dashBoardParams,session,[:])
            def data = dashBoardChartService.getDashBoardChartScript(dynamicReportParams,params.report)
            def url=session.currentUrl

            dashBoardParams.report=params.report
            dashBoardParams.group=searchService.getGroups()

            render(contentType: 'text/json') {[
                    'data':data,
                    'url':url,
                    'parameter':dashBoardParams.sort()
            ]}


        }else{
            def map=[:]
            map.put("rxMailRetailCost","RX Mail Retail Cost");
            map.put("rxBrandGenericCost","RX Brand Generic Cost");
            map.put("erVisitsLikelyToIncrease","ER visits are likely to increase");
            map.put("topChronicConditionErDetail","Use Of ER By Population By Top Chronic Conditions");
            map.put("rxSpecialityCost","Rx Speciality Cost");
            map.put("rxHistoricPredictive","RX Historic Predictive");
            map.put("pharmacyClaimsPmpm","Pharmacy Claims Pmpm");
            map.put("medicalClaimsPmpm","Medical Claims Pmpm");
            map.put("populationRisk","Population Risk");
            map.put("populationRiskByTopChronicCondition","Population Risk By Top Chronic Condition");
            map.put("rxHistoricPredictive","RX Historic Predictive");
            map.put("rxSpecialityCost","RX Speciality Cost");
            map.put("groupMemberCountView","Group Member Count View");

            [requests:map.sort(),isEditable:false]
        }


    }


    def viewResponse = {
        if(servletContext.getAttribute('USER_URL_LOGGING')){
            String mainUrl      = params.url?.decodeURL() ?: null
            String printQuery   = params.printQuery       ?: null

            if (printQuery) {
                mainUrl += "&printQuery=" + printQuery
            }
            cleanParams()
            if (mainUrl == null)  {
                def finalJson = new JSONObject()
                finalJson.put("Error ","Please specify a URL as a parameter!")
                render finalJson as JSON
            }

            String moduleUrl = mainUrl?.split("\\?")[0]
            def parameter = mainUrl?.split("\\?")[1]
            String backendJson = ''

            def connection
            def url = new URL(moduleUrl)
            try {

                connection = url.openConnection()
                connection.setRequestMethod("POST")
                connection.doOutput = true
                String ticket       = null
                Writer wr           = new OutputStreamWriter(connection.outputStream)
                if(Environment.DEVELOPMENT != Environment.getCurrent()){
                    ticket       = dwProxyService.getProxyTicket(grailsApplication.config.grails.backEndMAINURL, 1)
                }

                String clientName   = getClientName(getRequestHost())
                parameter           = getProxyTicketAndClient(ticket, clientName, parameter)
                log.info "FINAL URL :  " + moduleUrl+"?" +parameter

                wr.write(parameter)
                wr.flush()
                wr.close()
                connection.connect();
                def finalJson = new JSONObject()
                backendJson = connection.content.text
                finalJson.put("data",JSON.parse(backendJson))
                render finalJson as JSON
            } catch (ex) {
                ex.printStackTrace()
                def finalJson = new JSONObject()

                if(connection){
                    def header=[:]
                    def map = connection.getHeaderFields()
                    header.putAll(map)
                    header.put("Status Code",connection.getResponseCode())

                    finalJson.put("Header", header)
                }

                finalJson.put("Error ","An exception occurred while processing your request")
                finalJson.put("BackendData ",backendJson)
                finalJson.prettyPrint = true
                finalJson.render response
//                render finalJson as JSON
//                render(contentType: 'text/json') {finalJson}
            }
        } else {
            redirect(action: index)
        }

    }
    def private cleanParams () {

        params.remove("controller")
        params.remove("action")
        params.remove("url")
        params.remove("clientId")
//        params.setProperty("clientId",session.client.clientId)
        if (params.printQuery) params.remove("printQuery")

    }


    public String getProxyTicketAndClient(String ticket, String clientName, String parameter) {
        parameter = parameter + "&ticket=" + ticket + "&" + "clientName=" + clientName
        parameter
    }

    def private getRequestHost() {
        HttpServletRequest request  = RequestContextHolder.currentRequestAttributes().getRequest()
        String requestHost          = request.getHeader('Host')

        if (requestHost.indexOf(":") > 0) {
            requestHost = requestHost.split(":")[0]
        }
        requestHost
    }

    def private getClientName(String requestHost) {
        def requestedClient = grailsApplication.config.grails.navigationUrl
        /*def requestedClient = requestHost.substring(0, requestHost.indexOf("."))
        if (requestedClient.indexOf("-") > 0) {
            requestedClient = requestedClient.split("-")[0]
        }
        requestedClient*/
    }

    /**
     * @param url   Generate proxy ticket for this URL
     * @return      The proxy ticket generated for the URL passed as parameter
     */
    public String getProxyTicket(String url) {
        final CasAuthenticationToken token = (CasAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()
        final String proxyTicket           = token.getAssertion().getPrincipal().getProxyTicketFor(url)
        proxyTicket
    }
}
