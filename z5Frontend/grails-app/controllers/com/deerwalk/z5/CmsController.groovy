package com.deerwalk.z5




class CmsController {
    def displayService

    def index() {
        render (view: "../dashboard/index",model:[isEditable:true] )
    }

    def highPharmacy() {
        render (view: "../highPharmacyCost/index",model:[isEditable:true] )
    }
    def drugsDetails(){
        render (view: "../highPharmacyCost/drugsDetails",model:[isEditable:true,type:params.type] )
    }
    def erUtilization() {
        render (view: "../erUtilization/index",model:[isEditable:true] )
    }
    def populationRisk() {
        render (view: "../populationRisk/index",model:[isEditable:true] )
    }

    def setClient(){
        render (view: "../dashboard/setClient",model:[isEditable:true] )
    }


    def uiTracker() {
        render (view: "../UITracker/list",model:[isEditable:true] )
    }

    def costDriversDetails(){
        render (view: "../erUtilization/costDriversDetails",model:[isEditable:true] )
    }
    def topChronicDetails(){
        render (view: "../erUtilization/topChronicDetails",model:[isEditable:true] )
    }
    def telemedicine(){
        render (view: "../erUtilization/telemedicine",model:[isEditable:true] )
    }

    def costSharing(){
        render (view: "../erUtilization/costSharing",model:[isEditable:true] )
    }

    def communicateOptions(){
        render (view: "../erUtilization/communicateOptions",model:[isEditable:true] )
    }
    def expandUrgent(){
        render (view: "../erUtilization/expandUrgent",model:[isEditable:true] )
    }

    def gapInCare(){
        render (view: "../dashboard/gapInCare",model:[isEditable:true] )

    }

    def networkUse(){
        render (view: "../networkUse/index",model:[isEditable:true] )

    }
    def imagingService(){
        render (view: "../dashboard/imaging",model:[isEditable:true] )

    }

    def updateData(){
        try{
            displayService.insertContent(params.code,params.value,params.url)
        }catch(Exception e){
            println "Error occured while updating config file;e="+e.toString()
        }
        render true
    }

    def strategicPage(){
        render (view: "../highPharmacyCost/strategicPage",model:[isEditable:true] )

    }
}
