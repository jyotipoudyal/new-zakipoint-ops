package com.deerwalk.z5

import com.deerwalk.security.Requestmap
import org.apache.catalina.startup.Bootstrap
import org.codehaus.groovy.grails.commons.DefaultGrailsDomainClass
import org.springframework.dao.DataIntegrityViolationException

class UITrackerController {
    def grailsLinkGenerator

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def springSecurityService

    def index(Integer max) {



        params.max =Math.min(max ?: 10, 100)
//        params.sort=params.sort?params.sort:"id";
        params.sort="id";
        params.order="desc";
        render(view: "list",model:[uit: UITracker.list(max: 10, offset: params.offset,sort:params.sort, order:params.order), UITrackerInstanceTotal: UITracker.count(),isEditable:false] )
    }



    /*def list(Integer max) {
        params.max =Math.min(max ?: 10, 100)
        params.sort=params.sort?params.sort:"dateCreated";
        params.order=params.order?params.order:"desc";
//        println "max = "+UITracker.count()
        [UITrackerInstanceList: UITracker.list(max: 8, offset: params.offset, sort: params.sort, order:params.order), UITrackerInstanceTotal: UITracker.count(),isEditable:false]
    }*/

    def refrehRequestMap(){

            /*def servletCtx = org.codehaus.groovy.grails.web.context.ServletContextHolder.servletContext
            def myBootstrapArtefact = grailsApplication.getArtefacts('Bootstrap')[-1]
            myBootstrapArtefact.referenceInstance.init(servletCtx)*/

        /*def servletCtx = org.codehaus.groovy.grails.web.context.ServletContextHolder.servletContext
        def myBootstrapArtefact = grailsApplication.getArtefacts('Bootstrap')[-1]
        Bootstrap.metaClass.environments = grails.util.Environment.&executeForCurrentEnvironment
        myBootstrapArtefact.referenceInstance.init(servletCtx)*/
        def user=springSecurityService.currentUser
        def servletCtx = org.codehaus.groovy.grails.web.context.ServletContextHolder.servletContext
        def myBootstrapArtefact = grailsApplication.getArtefacts('Bootstrap')[-1]
        myBootstrapArtefact.referenceInstance.init(servletCtx)
        springSecurityService.reauthenticate user.username

//        servletContext.setAttribute('requestMap',Requestmap.findAll())
        forward(action: "index")
    }



    def create() {
        [UITrackerInstance: new UITracker(params)]
    }

    def save() {
        def UITrackerInstance = new UITracker(params)
        if (!UITrackerInstance.save(flush: true)) {
            render(view: "create", model: [UITrackerInstance: UITrackerInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'UITracker.label', default: 'UITracker'), UITrackerInstance.id])
        redirect(action: "show", id: UITrackerInstance.id)
    }

    def show(Long id) {
        def UITrackerInstance = UITracker.get(id)
        if (!UITrackerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'UITracker.label', default: 'UITracker'), id])
            redirect(action: "list")
            return
        }

        [UITrackerInstance: UITrackerInstance]
    }

    def edit(Long id) {
        def UITrackerInstance = UITracker.get(id)
        if (!UITrackerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'UITracker.label', default: 'UITracker'), id])
            redirect(action: "list")
            return
        }

        [UITrackerInstance: UITrackerInstance]
    }

    def update(Long id, Long version) {
        def UITrackerInstance = UITracker.get(id)
        if (!UITrackerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'UITracker.label', default: 'UITracker'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (UITrackerInstance.version > version) {
                UITrackerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'UITracker.label', default: 'UITracker')] as Object[],
                        "Another user has updated this UITracker while you were editing")
                render(view: "edit", model: [UITrackerInstance: UITrackerInstance])
                return
            }
        }

        UITrackerInstance.properties = params

        if (!UITrackerInstance.save(flush: true)) {
            render(view: "edit", model: [UITrackerInstance: UITrackerInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'UITracker.label', default: 'UITracker'), UITrackerInstance.id])
        redirect(action: "show", id: UITrackerInstance.id)
    }



    def delete(Long id) {
        def UITrackerInstance = UITracker.get(id)
        if (!UITrackerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'UITracker.label', default: 'UITracker'), id])
            redirect(action: "list")
            return
        }

        try {
            UITrackerInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'UITracker.label', default: 'UITracker'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'UITracker.label', default: 'UITracker'), id])
            redirect(action: "show", id: id)
        }
    }
}
