package com.deerwalk.z5

import org.codehaus.groovy.grails.web.json.JSONObject
import report.FaqReport

class FaqController {

    def fetchService
    def dynamicParamsGeneratorService

    def index() {
        def map=[:];
        map.catId=1;
        def columns;
        def category=fetchService.fetchFaq(map);

        def list=new ArrayList();
        def catMap=[:]

        (category).each{
            map.catId=it['gsx$categoryid']['$t']
            map.catName=it["gsx\$categoryname"]['$t']

            FaqReport rg = (FaqReport)dynamicParamsGeneratorService.getReportGeneratorInstance("faq",[:],map);
            columns= rg.getFaqColumns();
            def data;
            try {
                data = rg.getFinalData();
                catMap[it["gsx\$categoryname"]['$t']]=data;

            } catch (Exception e){
                e.printStackTrace()
            }

        }

        render(view: "list",model:  [data:catMap,columns:columns])


    }


}
