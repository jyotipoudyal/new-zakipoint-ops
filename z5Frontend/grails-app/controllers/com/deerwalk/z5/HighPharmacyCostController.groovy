package com.deerwalk.z5

import grails.converters.JSON
import groovy.sql.Sql
import org.apache.commons.io.IOUtils
import org.codehaus.groovy.grails.commons.ApplicationHolder
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.context.ServletContextHolder

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream;
import sun.misc.BASE64Decoder;


import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

class HighPharmacyCostController {
    def highPharmacyService
    def dashBoardChartService
    def fetchService
    def dataSource
    def pdfRenderingService
    def exportService
    def helperService
    def memberDrillService



    def indexS() {

     // http://192.168.0.136:8080/DasTest/zakiPoint?clientId=2000&reportingBasis=ServiceDate&reportingTo=2014-09-30
     // &reportingFrom=2013-10-01&&report=highPharmacyCost:default&eligibilityType=[medical,vision,dental]
     // &reportingPaidThrough=2014-09-30&page=1&pageSize=5&phiCSDate=10-01-2009&phiCEDate=09-30-2014&order=populationSize:Desc
        //////////////////////////////////

        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"pharmacyClaimsPmpm")
        def dataPh=dataGraph.getAt('reporting')
        def dataBm=dataGraph.getAt('benchmark').getAt("Pharmacy Claims PMPM")
        dataPh.benchmark=dataBm
        ////////////////////////////////////////////
        session.graph1=[:];
        session.table1=[:];
        session.table2=[:];

        def dashBoardParams =[:]
        dashBoardParams.page=1;
        dashBoardParams.order="genericBrandSavings:desc"
        dashBoardParams.reportingBasis="PaidDate"
        dashBoardParams.csDate=params.csDate
        dashBoardParams.ceDate=params.cEDate
        dashBoardParams.reportingFrom=reportingDates.currentFrom
        dashBoardParams.reportingTo=reportingDates.currentTo
        dashBoardParams.pageSize=5
        dashBoardParams.mbrRegionId=""
//        dashBoardParams.clientId=servletContext.getAt("clientIdentity")

        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams,session,[:])
        def data,total,size;
        def data1,total1,size1;
        def table1Type,table2Type;
        table1Type="default";
        table2Type="speciality";

        try{
            data = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,"highPharmacyCost",table1Type)
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }



        List<DrugsData> table1Data = new ArrayList<DrugsData>();
        data.each { id, datas ->
            DrugsData dgs=new DrugsData(datas)
            table1Data.add(dgs)
        }
        table1Data=table1Data.sort{it['genericBrandSavings']}.reverse()

        dashBoardParams.order="mailRetailSavings:desc"
        dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams,session,[:])

        try{
            data1 = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,"highPharmacyCost",table2Type)
            total1=data1.total;
            size1=data1.totalHits;
            data1.remove('total');
            data1.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }


        session.table1.size=size

        session.table2.size=size1


        List<DrugsData> table2Data = new ArrayList<DrugsData>();
        data1.each { id, datas ->
            datas.genericBrandSavings=datas.genericBrandSavings?datas.genericBrandSavings:0;
            DrugsData dg=new DrugsData(datas)
            table2Data.add(dg)
        }
        table2Data=table2Data.sort{it['mailRetailSavings']}.reverse()
        //{"total":{"totalAnnualCost":773689661,"totalmailRetailSavings":7737193.05,"totalPopulationSize":91917,"totalgenericBrandSavings":7737193.05},"Simvastatin":{"equvalentGenericDrug":"Simvastatin","genericBrandSavings":50535.2,"drugName":"Simvastatin","annualCost":50535.2,"mailRetailSavings":50535.2,"populationSize":2218},"totalHits":1434,"Azithromycin":{"equvalentGenericDrug":"Azithromycin","genericBrandSavings":35777.43,"drugName":"Azithromycin","annualCost":35777.02,"mailRetailSavings":35777.43,"populationSize":1870},"Hydrocodone-Acetaminophen":{"equvalentGenericDrug":"Hydrocodone-Acetaminophen","genericBrandSavings":17651.43,"drugName":"Hydrocodone-Acetaminophen","annualCost":17640.95,"mailRetailSavings":17651.43,"populationSize":2521},"Levothyroxine Sodium":{"equvalentGenericDrug":"Levothyroxine Sodium","genericBrandSavings":7558.11,"drugName":"Levothyroxine Sodium","annualCost":7536.86,"mailRetailSavings":7558.11,"populationSize":1890},"Lisinopril":{"equvalentGenericDrug":"Lisinopril","genericBrandSavings":19997.76,"drugName":"Lisinopril","annualCost":19987.52,"mailRetailSavings":19997.76,"populationSize":2353}}
        render(view: "index1",model: [defaultSort:"sortable sorted desc",isEditable:false,data:table1Data,total:total,size:size,page:dashBoardParams.page,dataPh:dataPh,data2:table2Data,total2:total1,size1:size1])
//      [isEditable:false,t1:[q:4,a:"\$319K"],t2:[q:3,a:"\$424K"]]
    }

    def index = {
        session.hr=[:]
        [isEditable:false]
    }

    def drugsDetails={

        session.graph1=[:];
        session.table1=[:];
        session.table2=[:];
        session.table3=[:];
        params.csDate=session.csDate
        params.ceDate=session.ceDate

        def reportingDates=helperService.getReportingDates(params.ceDate)

        def dashBoardParams =[:]
        dashBoardParams.page=1;
        def tableType;
        if(params.type=="brand"){
            dashBoardParams.order="savings:desc"
            tableType="rxBrandGenericCostDetail"
        }

        else if(params.type=="mail"){
            dashBoardParams.order="savings:desc"
            tableType="rxMailRetailCostDetail"

        }
        else{
            dashBoardParams.order="annualCost:desc"
            tableType="rxSpecialityCostDetail";
        }

        dashBoardParams.reportingBasis=session.hr.reportingBasis?session.hr.reportingBasis:"Paid"
        dashBoardParams.range=session.hr.range?session.hr.range:"0"
        dashBoardParams.isExclude=session.hr.isExclude?session.hr.isExclude:"false"

        dashBoardParams.reportingFrom=reportingDates.currentFrom
        dashBoardParams.reportingTo=reportingDates.currentTo
        dashBoardParams.pageSize=5
        dashBoardParams.mbrRegionId=""
//        dashBoardParams.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams,session,[:])
        def data,total,size;

        try{
            data = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,tableType)
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }



        List<Map> table1Data = new ArrayList<Map>();
        data.each { id, datas ->
            memberDrillService.getRxBrandMemberUseDrillParams(datas,true,'member',dashBoardParams,tableType)
//            DrugsData dgs=new DrugsData(datas)
//            dgs.membersDrill=memberDrillService.getRxBrandMemberUseDrillParams(dgs,true,'member',dashBoardParams)
            table1Data.add(datas)
        }
        table1Data=table1Data.sort{it[dashBoardParams.order.split(":")[0]]}.reverse()


        if(params.type=="brand"){
            session.table1.size=size
        }
        else if(params.type=="mail"){
            session.table2.size=size
        }
        else{
            session.table3.size=size
        }

        def defaultSort=params.type?params.type:"speciality"

        [isEditable:false,data:table1Data,total:total,size:size,type:params.type?params.type:"",defaultSort:defaultSort]

    }



    def getMaximum(a,b){
        def aMax=0;
        a.each{
            def val2=it.pharmacyPaidAmountPmpm>it.benchmark.toDouble()?it.pharmacyPaidAmountPmpm:it.benchmark.toDouble()
            if(val2>aMax)
                aMax=val2

        }
        def bMax=0;

        b.each{
            def val2=it.actual
            if(val2>bMax)
                bMax=val2
        }

        def val= aMax>bMax?aMax:bMax
        return val *1.1
    }

    def getScriptForPharmacy(){
//        params.fromDate=params.fromDate?params.fromDate:"current"
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.isExclude=params.isExclude?params.isExclude:"false"
        params.range=params.range?params.range:"0"
        session.hr.fromDate= params.fromDate
        session.hr.reportingBasis= params.reportingBasis
        session.hr.range= params.range
        session.hr.isExclude= params.isExclude
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParam;

        def topLineGraph=new ArrayList();

        if(params.module=="lineGraph1"){
            params.trend="yearly"
            reportingDates=helperService.getReportingDates(params.ceDate)

            println "reportingDates = $reportingDates"
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"rxHistoricPredictive")
            def datanew1=[:];
            def dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=dataGraph.getAt('benchmark').getAt("Pharmacy Claims PMPM")
            datanew1.pharmacyPaidAmountPmpm=dataPh3.getAt('pharmacyPaidAmountPmpm')
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew1,true,'member',params)

            topLineGraph.add(datanew1)


            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"rxHistoricPredictive")
            datanew1=[:];
            dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=dataGraph.getAt('benchmark').getAt("Pharmacy Claims PMPM")
            datanew1.pharmacyPaidAmountPmpm=dataPh3.getAt('pharmacyPaidAmountPmpm')
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew1,true,'member',params)

            topLineGraph.add(datanew1)

            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"rxHistoricPredictive")
            datanew1=[:];
            dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=dataGraph.getAt('benchmark').getAt("Pharmacy Claims PMPM")
            datanew1.pharmacyPaidAmountPmpm=dataPh3.getAt('pharmacyPaidAmountPmpm')
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew1,true,'member',params)

            topLineGraph.add(datanew1)


            //----------------2nd one

            datanew1=[:];
            def lineGraphR=new ArrayList();
            params.reportingFrom=params.reportingTo
            params.reportingTo=helperService.addYear(params.reportingFrom)
            datanew1.put('year',params.reportingFrom)
            datanew1.put('actual',dataPh3.getAt('pharmacyPaidAmountPmpm'))
            lineGraphR.add(datanew1)
            datanew1=[:];
            datanew1.put('year',params.reportingTo)
            datanew1.put('actual',dataPh3.getAt('pharmacyPaidAmountPmpmPredicted'))
            lineGraphR.add(datanew1)
            def getMax=getMaximum(topLineGraph,lineGraphR)
            def yPercentage=0
            def yTotal=dataPh3.getAt("percentOfPharmacyPaidAmountPmpmLikelyToIncrease")

            def map=[:]
            map.topLineGraph=topLineGraph
            map.max=getMax

            map.dm=lineGraphR
            map.yPercentage=yPercentage
            map.yTotal=yTotal

            render map as JSON

        }else if(params.module=="barGraphs"){

            def datanew=[:]
            def dataPh=new ArrayList();
            def dataPh4=new ArrayList();
            def dataPh5=new ArrayList();

            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxBrandGenericCost")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"brand")

            dataPh.add(datanew)
            datanew=[:];
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxMailRetailCost")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"mail")

            dataPh4.add(datanew)
            datanew=[:];
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxSpecialityCost")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"speciality")

            dataPh5.add(datanew)

            //another graph-------------------------------------------------------------

            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxBrandGenericCost")
            datanew=[:];
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"brand")
            dataPh.add(datanew)
            datanew=[:];
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxMailRetailCost")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"mail")

            dataPh4.add(datanew)
            datanew=[:];
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxSpecialityCost")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"speciality")
            dataPh5.add(datanew)


            //current graph==========================================================


            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxBrandGenericCost")
            datanew=[:];
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"brand")
            dataPh.add(datanew)
            datanew=[:];
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxMailRetailCost")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"mail")

            dataPh4.add(datanew)
            datanew=[:];
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"rxSpecialityCost")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getRxHistoricPredictiveDrillParams(datanew,true,'member',params,"speciality")
            dataPh5.add(datanew)

            def map=[:]
            map.b1=dataPh
            map.b2=dataPh4
            map.b3=dataPh5

            render map as JSON


        }




    }


    def remotePagination(){
        params.offset=params.offset.toInteger()
        params.max=params.max.toInteger()
        if(params.offset==0){
           params.page=1
        }else if(params.offset>1){
            params.page=(params.offset/params.max)+1
        }
        params.remotePaginate=true

        def tableType;
        if(params.type=="brand"){
            params.module="table1Data"
            tableType="rxBrandGenericCostDetail"
        }

        else if(params.type=="mail"){
            params.module="table2Data"
            tableType="rxMailRetailCostDetail"

        }
        else{
            params.module="table3Data"
            tableType="rxSpecialityCostDetail";
        }
        def dashBoardParams=fetchParams(params)
//        dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])
        def data,total,size;

        try{
            data =highPharmacyService.getHighPharmacyTable1(dynamicReportParams,tableType)
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }
        def s2=frontEndOrdering(data,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d,tableType)
        render(template: "table1Data", model: [data:s2,total:total,size:size,page:dashBoardParams.p.page,size1:size,type:params.type])
    }

    def frontEndOrdering(data,order,sort,dashBoardParams,tableType){
        /*List<DrugsData> s2 = new ArrayList<DrugsData>();
        data.each { id, datas ->
            DrugsData dgs=new DrugsData(datas)
            s2.add(dgs)
        }*/

        List<Map> s2 = new ArrayList<Map>();
        data.each { id, datas ->
            memberDrillService.getRxBrandMemberUseDrillParams(datas, true, 'member', dashBoardParams,tableType)
            s2.add(datas);
        }
        if(order=='asc')
            s2=s2.sort{it[sort]}
        else
            s2=s2.sort{it[sort]}.reverse()
        return s2
    }

    def sortResult(){
        params.page=1;
        params.sortResults=true
        def data,total,size;
        def tableType;
        if(params.type=="brand"){
            params.module="table1Data"
            tableType="rxBrandGenericCostDetail"
        }

        else if(params.type=="mail"){
            params.module="table2Data"
            tableType="rxMailRetailCostDetail"

        }
        else{
            params.module="table3Data"
            tableType="rxSpecialityCostDetail";
        }


        def dashBoardParams=fetchParams(params)
//        dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])

        try{
            data = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,tableType)
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }
        def s2=frontEndOrdering(data,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d,tableType)
        render(template: 'table1Data', model: [data:s2,total:total,size:size,size1:size,page:params.page,type:params.type])
    }

    def exportPdf(){
        def sourceData,parts,imageString;
        byte[] imageByte;
        BASE64Decoder decoder
        def imageMap=[:]

        sourceData = params.imagePath1;
        parts = sourceData.tokenize(",");
        imageString = parts[1];
        decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);
        imageMap.image1=imageByte

        sourceData = params.imagePath2;
        parts = sourceData.tokenize(",");
        imageString = parts[1];
        decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);
        imageMap.image2=imageByte

        def costClaimants=new File(ApplicationHolder.application.parentContext.servletContext.getRealPath("/images/icon-high-cost-claimants.png"))
        def cost=new File(ApplicationHolder.application.parentContext.servletContext.getRealPath("/images/icon-high-pharmacy-cost.png"))
        def heads=new File(ApplicationHolder.application.parentContext.servletContext.getRealPath("/images/logo-zakipoint-large.png"))
        renderPdf(template: "pdfPharmacy", model: [image:imageMap,cost:cost.bytes,costClaimants:costClaimants.bytes,heads:heads.bytes],
                                                   filename: "HighPharmacy.pdf")//, filename: "HighPharmacy.pdf"
    }

    def exportPdfOld(){
        def sourceData = params.imagePath1;
        def parts = sourceData.tokenize(",");
        def imageString = parts[1];
        BufferedImage image = null;
        byte[] imageByte;

        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);

        params.page=1;
        params.module="table1Data"
        params.export=true
        def dashBoardParams=fetchParams(params)
//        dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])
        def tableType=(params.module=="table1Data")?"default":"speciality";
        def data,total,size;
        try{
            data = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,"highPharmacyCost",tableType)
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }

        def s2=frontEndOrdering(data,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d)
        params.module="table2Data"
        def dashBoardParams2=fetchParams(params)
//        dashBoardParams2.d.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParams2 = highPharmacyService.setDynamicReportParams(dashBoardParams2.d,session,[:])
        def tableType2=(params.module=="table1Data")?"default":"speciality";
        def data2,total2,size2;
        try{
            data2 = highPharmacyService.getHighPharmacyTable1(dynamicReportParams2,"highPharmacyCost",tableType2)
            total2=data2.total;
            size2=data2.totalHits;
            data2.remove('total');
            data2.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }
        def s3=frontEndOrdering(data2,dashBoardParams2.p.order,dashBoardParams2.p.sort,dashBoardParams2.d,tableType2)
//        def paid = new File(ApplicationHolder.application.parentContext.servletContext.getRealPath("/images/icon-explore-population.png"))
        def costClaimants=new File(ApplicationHolder.application.parentContext.servletContext.getRealPath("/images/icon-high-cost-claimants.png"))
        def cost=new File(ApplicationHolder.application.parentContext.servletContext.getRealPath("/images/icon-high-pharmacy-cost.png"))
        def heads=new File(ApplicationHolder.application.parentContext.servletContext.getRealPath("/images/logo-zakipoint-large.png"))
        renderPdf(template: "pdfPharmacyOld", model: [data:s2,total:total,data1:s3,total2:total2,image:imageByte,
                                                   cost:cost.bytes,costClaimants:costClaimants.bytes,heads:heads.bytes,
                                                   act:params.act,exp:params.exp,g1Filter:params.g1Filter,g2Filter:params.g2Filter,g3Filter:params.g3Filter], filename: "HighPharmacy.pdf")//, filename: "HighPharmacy.pdf"
    }

    def exportCsv(){
        def data;
        Map labels,formatter,parameters;
        List fields;
        params.reportingBasis=session.hr.reportingBasis?session.hr.reportingBasis:"PaidDate"
        params.range=session.hr.range?session.hr.range:"0"
        params.isExclude=session.hr.isExclude?session.hr.isExclude:"false"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
//        params.clientId=servletContext.getAt("clientIdentity")
        switch(params.exportModule){
            case ("rxHistoricPredictive"):

                params.trend="monthly"
                def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.currentTo

                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"rxHistoricPredictive")

                List s2 = new ArrayList();
                def map = new LinkedHashMap();
                dataGraph.reporting.pharmacyClaims.each { id, datas ->
                    map=[:];
                    map.time=Date.parse('yyyy-MM',id)
                    map.pharmacyClaims=datas
                    map.memberCount=dataGraph.reporting.memberMonths[id]
                    map.benchMarkValue=dataGraph.benchmark["Pharmacy Claims PMPM"]
                    s2.add(map)
                }

                s2.sort{it.time}
                def fDate = { domain, value ->
                    if (value)
                        return value.format("yyyy-MM")
                }
                formatter = [time: fDate]

                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=RxHistoricPredictive.csv")

                fields = [
                        "time",
                        "pharmacyClaims",
                        "benchMarkValue",
                        "memberCount"
//                        "memberMonths"
                ]

                labels = ["time": "Time",
                              "pharmacyClaims": "Pharmacy Claims PMPM",
                              "benchMarkValue": "Benchmark Value PMPM",
                              "memberCount": "Members Count"
//                              "memberMonths": "Member Months"
                ]

                parameters = ["column.widths": [0.1,0.40, 0.40,0.40]
                ]
                data=s2;

                break;
            case ("topChronicConditionEr"):
                params.page=1;

                def tableType,exportTitle;
                if(params.type=="brand"){
                    params.module="table1Data"
                    params.exportCsv=session.table1.size
                    tableType="rxBrandGenericCostDetail"
                    exportTitle="GenericSavings.csv";
                }

                else if(params.type=="mail"){
                    params.module="table2Data"
                    params.exportCsv=session.table2.size
                    tableType="rxMailRetailCostDetail"
                    exportTitle="MailOrderSavings.csv";
                }
                else{
                    params.module="table3Data"
                    params.exportCsv=session.table3.size
                    tableType="rxSpecialityCostDetail";
                    exportTitle="SpecialitySavings.csv";
                }

                def dashBoardParams=fetchParams(params)


//                dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")

                def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])

                def data1,total1,size1;
                try{
//                    data1 = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,"highPharmacyCost",tableType)
                    data1 = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,tableType)

                    total1=data1.total;
                    size1=data1.totalHits;
                    data1.remove('total');
                    data1.remove('totalHits');
                }catch(e){
                    log.error(e.getMessage());
                    render status: 204;
                }
                def s2=frontEndOrdering(data1,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d,tableType)
                data=s2;

                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=${exportTitle}")

                fields = [
                        "drugName",
                        "genericOrBrand",
                        "equvalentGenericDrug",
                        "prescriptions",
                        "populationSize",
                        "paidAmountPmpm",
                        "annualCost",
                        "genericBrandSavings",
                        "mailRetailSavings"
                ]

                labels = ["drugName": pdfMessageCsv(code: "highPharmacy.container3.table1.head1"),
                          "genericOrBrand": "Drug Type",
                          "equvalentGenericDrug": pdfMessageCsv(code: "highPharmacy.container3.table1.head12"),
                          "prescriptions": pdfMessageCsv(code: "highPharmacy.container3.table1.head6"),
                          "populationSize": pdfMessageCsv(code: "highPharmacy.container3.table1.head61"),
                          "paidAmountPmpm": pdfMessageCsv(code: "highPharmacy.container3.table1.head7"),
//                          "benchmarkPmpm": pdfMessageCsv(code: "highPharmacy.container3.table1.head5"),
                          "annualCost":pdfMessageCsv(code: "highPharmacy.container3.table1.head4"),
                          "genericBrandSavings": pdfMessageCsv(code: "highPharmacy.container3.table1.head2"),
                          "mailRetailSavings": pdfMessageCsv(code: "highPharmacy.container3.table1.head3")
                ]

                parameters = ["column.widths": [0.1,0.40, 0.40, 0.40,0.40,0.40, 0.40, 0.40,0.40,0.40]
                ]
                break;

            case('barGraph'):
                List s2 = new ArrayList();
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                def datanew=[:]

                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,params.barModule)
                datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                datanew.year=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                s2.add(datanew);

                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                datanew=[:]
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,params.barModule)
                datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                datanew.year=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                s2.add(datanew);

                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                datanew=[:]
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,params.barModule)
                datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                datanew.year=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                s2.add(datanew);

                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=${params.barModule}.csv")

                fields = [
                        "year",
                        "paidAmtForTotal",
                        "paidAmtForSavingsScope",
                        "percentOfPaidAmtForSavingsScope"
                ]

                labels = ["year": "Time",
                          "paidAmtForTotal": "Paid Amount For Total",
                          "paidAmtForSavingsScope": "Paid Amount For Savings Scope",
                          "percentOfPaidAmtForSavingsScope": "Percent Of Paid Amount For Savings Scope"
                ]

                parameters = ["column.widths": [0.1,0.40, 0.40,0.40]
                ]
                def fDate = { domain, value ->
                    if (value)
                        return value.format("yyyy")
                }
                formatter = [year: fDate]

                data=s2;


                break;
            case ("table2Data"):
                params.page=1;
                params.exportCsv=session.table2.size
                params.module="table2Data"
                def dashBoardParams2=fetchParams(params)
//                dashBoardParams2.d.clientId=servletContext.getAt("clientIdentity")
                def dynamicReportParams2 = highPharmacyService.setDynamicReportParams(dashBoardParams2.d,session,[:])
                def tableType2=(params.module=="table1Data")?"default":"speciality";
                def data2,total2,size2;
                try{
                    data2 = highPharmacyService.getHighPharmacyTable1(dynamicReportParams2,"highPharmacyCost",tableType2)
                    total2=data2.total;
                    size2=data2.totalHits;
                    data2.remove('total');
                    data2.remove('totalHits');
                }catch(e){
                    log.error(e.getMessage());
                    render status: 204;
                }
                def s3=frontEndOrdering(data2,dashBoardParams2.p.order,dashBoardParams2.p.sort,dashBoardParams2.d,tableType2)
                data=s3;

                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=SpecialtySavings.csv")

                fields = [
                        "drugName",
                        "prescriptions",
                        "populationSize",
                        "paidAmountPmpm",
//                        "benchmarkPmpm",
                        "annualCost",
                        "mailRetailSavings"
                ]

                labels = ["drugName": pdfMessageCsv(code: "highPharmacy.container3.table1.head1"),
                          "prescriptions": pdfMessageCsv(code: "highPharmacy.container3.table1.head6"),
                          "populationSize": pdfMessageCsv(code: "highPharmacy.container3.table1.head61"),
                          "paidAmountPmpm": pdfMessageCsv(code: "highPharmacy.container3.table1.head7"),
//                          "benchmarkPmpm": pdfMessageCsv(code: "highPharmacy.container3.table1.head5"),
                          "annualCost": pdfMessageCsv(code: "highPharmacy.container3.table1.head4"),
                          "mailRetailSavings": pdfMessageCsv(code: "highPharmacy.container3.table1.head3")
                ]

                parameters = ["column.widths": [0.1,0.40, 0.40, 0.40,0.40,0.40,0.40]
                ]
                break;
        }


        exportService.export(params.format, response.outputStream, data, fields, labels, formatter, parameters)

    }


    def getChartScriptForPharmacy(){
        session.graph1.fromDate=params.fromDate?params.fromDate:"current"
        session.graph1.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        session.graph1.regional=params.regional

//        params.clientId=servletContext.getAt("clientIdentity")
//        params.mbrRegionId=params.list("regional[]")
        params.mbrRegionId=params.regional
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
       switch(params.fromDate){
           case ("current"):
               params.reportingFrom=reportingDates.currentFrom
               params.reportingTo=reportingDates.currentTo
               break;
           case ("past"):
               params.reportingFrom=reportingDates.pastFrom
               params.reportingTo=reportingDates.pastTo
               break;
           case ("prior"):
               params.reportingFrom=reportingDates.priorFrom
               params.reportingTo=reportingDates.priorTo
               break;
       }


        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"pharmacyClaimsPmpm")
        def dataPh=dataGraph.getAt('reporting')
        def dataBm=dataGraph.getAt('benchmark').getAt("Pharmacy Claims PMPM")
        dataPh.benchmark=dataBm

        //show bob data

        params.showBob=params.bob?params.bob:""

        if(params.showBob){
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"pharmacyClaimsPmpm")
            def dataPh2=dataGraph.getAt('reporting')
            dataPh.bobClaims=dataPh2.getAt('pharmacyClaims')
            dataPh.bobMemberMonths=dataPh2.getAt('memberMonths')
        }else{
            dataPh.bobClaims=null
            dataPh.bobMemberMonths=null
        }




        render dataPh

    }

    def getBobChartScriptForPharmacy(){
        session.graph1.fromDate=params.fromDate?params.fromDate:"current"
        session.graph1.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        session.graph1.regional=params.regional





//        params.clientId=servletContext.getAt("clientIdentity")
//        params.mbrRegionId=params.list("regional[]")
        params.mbrRegionId=params.regional
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        switch(params.fromDate){
            case ("current"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                break;
        }


        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"pharmacyClaimsPmpm")
        def dataPh=dataGraph.getAt('reporting')
        def dataBm=dataGraph.getAt('benchmark').getAt("Pharmacy Claims PMPM")
        dataPh.benchmark=dataBm

        ///////////////////////////////////////
        params.showBob=params.bob?params.bob:""

        if(params.showBob){
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"pharmacyClaimsPmpm")
            def dataPh2=dataGraph.getAt('reporting')
            dataPh.bobClaims=dataPh2.getAt('pharmacyClaims')
            dataPh.bobMemberMonths=dataPh2.getAt('memberMonths')
        }else{
            dataPh.bobClaims=null
            dataPh.bobMemberMonths=null
        }






        render dataPh

    }

    def getTableScriptForPharmacy(){
        params.page=1;
        params.tableScript=true;
        def dashBoardParams =fetchParams(params)
//        dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")

        def tableType=(params.module=="table1Data")?"default":"speciality";
        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])
        def data,total,size;
        try{
            data = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,"highPharmacyCost",tableType)
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }
        def s2=frontEndOrdering(data,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d,tableType)

        if(params.module=="table1Data") {
            session.table1.size=size
        }else if(params.module=="table2Data"){
            session.table2.size=size
        }

        render(template: params.module, model: [data:s2,total:total,size:size,page:dashBoardParams.p.page,size1: size])

    }

    /*def getFilterData(){

        def data = fetchService.fetchData("clientId=servletContext.getAt("clientIdentity")&report=pharmacyBrandNameList", "Zakipoint")
        def parsedJSON=JSON.parse(data);
        def rawJSON=parsedJSON.getAt('reporting').getAt("drugs");
        render(template: "filterContent",model: [data:rawJSON])

      //  http://das.deerwalk.local:8080/DasTest/zakiPoint?clientId=servletContext.getAt("clientIdentity")&report=pharmacyBrandNameList

    }*/


    def drugsFilter(){
        params.page=1
        params.drugSearch=true
        def tableType;
        if(params.type=="brand"){
            params.module="table1Data"
            tableType="rxBrandGenericCostDetail"
        }

        else if(params.type=="mail"){
            params.module="table2Data"
            tableType="rxMailRetailCostDetail"

        }
        else{
            params.module="table3Data"
            tableType="rxSpecialityCostDetail";
        }

        def dashBoardParams=fetchParams(params)
//        dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")

        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])
        def data,total,size;

        try{
            data = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,tableType)
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }
        def s2=frontEndOrdering(data,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d,tableType)
        if(params.module=="table1Data") {
            session.table1.size=size
        }else if(params.module=="table2Data"){
            session.table2.size=size
        }else if(params.module=="table3Data"){
            session.table3.size=size
        }
        render(template: 'table1Data', model: [data:s2,total:total,size:size,page:params.page,size1:size,type:params.type])
//        renderPdf(template: params.module, model: [data:s2,total:total,size:size,page:params.page,size1:size])
    }


    def strategicPage(){
        [isEditable:false]

    }

    def fetchDrugsList(){
        def detailAmt=  executeQuery("select dc.seq as id,dc.brand_name as name from dw_master_ndc as dc WHERE dc.brand_name LIKE '${params.q}%' group by dc.brand_name order by dc.brand_name asc limit 10 " );
        render detailAmt as JSON
    }

    def fetchParams(params){

        if(params.remotePaginate){
            if(params.module=="table1Data") {
                session.table1.pageSize =params.max
            }else if(params.module=="table2Data"){
                session.table2.pageSize =params.max
            }else if(params.module=="table3Data"){
                session.table3.pageSize =params.max
            }

        }else if(params.sortResults){
            if(params.module=="table1Data") {
                session.table1.sort=params.sort
                session.table1.order=params.order
            }else if(params.module=="table2Data"){
                session.table2.sort=params.sort
                session.table2.order=params.order
            }else if(params.module=="table3Data"){
                session.table3.sort=params.sort
                session.table3.order=params.order
            }

        }else if(params.drugSearch){
            if(params.module=="table1Data") {
                session.table1.brandName=params.brandName
            }else if(params.module=="table2Data"){
                session.table2.brandName=params.brandName
            }else if(params.module=="table3Data"){
                session.table3.brandName=params.brandName

            }

        }/*else if(params.tableScript){
            if(params.module=="table1Data") {
                session.table1.fromDate=params.fromDate
                session.table1.reportingBasis=params.reportingBasis
                session.table1.regional=params.regional
            }else if(params.module=="table2Data"){
                session.table2.fromDate=params.fromDate
                session.table2.reportingBasis=params.reportingBasis
                session.table2.regional=params.regional
            }else if(params.module=="table3Data"){
                session.table3.fromDate=params.fromDate
                session.table3.reportingBasis=params.reportingBasis
                session.table3.regional=params.regional
            }

        }*/


        if(params.module=="table1Data"){
            params.max=(session.table1.pageSize)?(session.table1.pageSize):5
            params.sort=(session.table1.sort)?(session.table1.sort):"savings"
            params.order=(session.table1.order)?(session.table1.order.toString().toLowerCase()):"desc"
            params.reportingBasis=(session.hr.reportingBasis)?(session.hr.reportingBasis):"PaidDate"
            params.range=(session.hr.range)?(session.hr.range):"0"
            params.isExclude=(session.hr.isExclude)?(session.hr.isExclude):"false"
            params.fromDate=(session.table1.fromDate)?(session.table1.fromDate):"current"
            params.regional=(session.table1.regional)?(session.table1.regional):""
            params.brandName=(session.table1.brandName)?(session.table1.brandName):""

        }else if(params.module=="table2Data"){
            params.max=(session.table2.pageSize)?(session.table2.pageSize):5
            params.sort=(session.table2.sort)?(session.table2.sort):"savings"
            params.order=(session.table2.order)?(session.table2.order.toString().toLowerCase()):"desc"
            params.reportingBasis=(session.hr.reportingBasis)?(session.hr.reportingBasis):"PaidDate"
            params.range=(session.hr.range)?(session.hr.range):"0"
            params.isExclude=(session.hr.isExclude)?(session.hr.isExclude):"false"
            params.fromDate=(session.table2.fromDate)?(session.table2.fromDate):"current"
            params.regional=(session.table2.regional)?(session.table2.regional):""
            params.brandName=(session.table2.brandName)?(session.table2.brandName):""
        }else if(params.module=="table3Data"){
            params.max=(session.table3.pageSize)?(session.table3.pageSize):5
            params.sort=(session.table3.sort)?(session.table3.sort):"annualCost"
            params.order=(session.table3.order)?(session.table3.order.toString().toLowerCase()):"desc"
            params.reportingBasis=(session.hr.reportingBasis)?(session.hr.reportingBasis):"PaidDate"
            params.range=(session.hr.range)?(session.hr.range):"0"
            params.isExclude=(session.hr.isExclude)?(session.hr.isExclude):"false"
            params.fromDate=(session.table3.fromDate)?(session.table3.fromDate):"current"
            params.regional=(session.table3.regional)?(session.table3.regional):""
            params.brandName=(session.table3.brandName)?(session.table3.brandName):""
        }




        if(params.export){
            params.max=20;
        }
        if(params.exportCsv){
            params.max=params.exportCsv;
        }

        def dashBoardParams =[:]
        dashBoardParams.page=params.page;
        dashBoardParams.order=params.sort+":"+params.order
        dashBoardParams.reportingBasis=params.reportingBasis
        dashBoardParams.regional=params.regional
        dashBoardParams.pageSize=params.max
        dashBoardParams.brandName=params.brandName
        dashBoardParams.range=params.range
        dashBoardParams.isExclude=params.isExclude
        dashBoardParams.csDate=session.csDate
        dashBoardParams.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(dashBoardParams.ceDate)
        dashBoardParams.reportingFrom=reportingDates.currentFrom
        dashBoardParams.reportingTo=reportingDates.currentTo

        /*switch(params.fromDate){
            case ("current"):
                dashBoardParams.reportingFrom=reportingDates.currentFrom
                dashBoardParams.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                dashBoardParams.reportingFrom=reportingDates.pastFrom
                dashBoardParams.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                dashBoardParams.reportingFrom=reportingDates.priorFrom
                dashBoardParams.reportingTo=reportingDates.priorTo
                break;
        }*/

        [p:params,d:dashBoardParams]

    }


    def executeQuery(String query) {
        def sql = new Sql(dataSource)
        def data = sql.rows(query)
        sql.close()
        return data
    }


    def slides(){
        String outputFileName = "HighPhamacy.pdf";
        // Create a document and add a page to it
        PDDocument document = new PDDocument();
//        PDPage page1 = new PDPage(PDPage.PAGE_SIZE_A4);
        // PDPage.PAGE_SIZE_LETTER is also possible
//        PDRectangle rect = page1.getMediaBox();
        // rect can be used to get the page width and height
//        document.addPage(page1);

        // Create a new font object selecting one of the PDF base fonts
        PDFont fontPlain = PDType1Font.HELVETICA;
        PDFont fontBold = PDType1Font.HELVETICA_BOLD;
        PDFont fontItalic = PDType1Font.HELVETICA_OBLIQUE;
        PDFont fontMono = PDType1Font.COURIER;

        // Start a new content stream which will "hold" the to be created content
        PDPage page1 = new PDPage(PDPage.PAGE_SIZE_A4);
        document.addPage(page1);
        PDPageContentStream cos = new PDPageContentStream(document, page1);
        cos = new PDPageContentStream(document, page1);

        /*int line = 0;

        // Define a text content stream using the selected font, move the cursor and draw some text
        cos.beginText();
        cos.setFont(fontPlain, 12);
        cos.moveTextPositionByAmount(100f, 700f);
        cos.drawString("Hello World");
        cos.endText();

        // Make sure that the content stream is closed:
        cos.close();*/



        // draw a red box in the lower left hand corner
       /* cos.setNonStrokingColor(Color.RED);
        cos.fillRect(10, 10, 100, 100);

        // add two lines of different widths
        cos.setLineWidth(1);
        cos.addLine(200, 250, 400, 250);
        cos.closeAndStroke();
        cos.setLineWidth(5);
        cos.addLine(200, 300, 400, 300);
        cos.closeAndStroke();*/

        // add an image
        //        def sourceData = 'image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAMAAABThUXgAAAByFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwtwJvAAAAl3RSTlMAAQIDBAYHCAkLDA0OEBESExUWFxkaGxwfICEjJCUnKCkrLi8wMzU2Nzg7PT4/QEFCQ0VGR0hLTU5PUFFSVFVWV1hZWltcXl9gYmNlZ2hrbG1ub3BxcnR1d3p7fH5/gIOEhYaMjY6PkJGTlJWXmJqbnZ6foKGmp6ipqq6wsbS3uLm7vL6/wMLExcfKzM/Q0tjf4Obr7e/yp/XLOwAABtJJREFUeAHs0DEVgDAQBbAqqqSKQAPd2c4u6z0G3mdgSyRkAAAAAAAA/O84H65qBl29GjRTVm7Jym1ZuZIVm7JyK89i51lUnMXMs1ifsu7yzva7iaIN4/e2SZ4Nj2kTa2sXMdaagE1hwcoLBEGBAosiaoImCESq1kUxgKyAsJbwQlEkkKCE+99Vl7Zy5gPndPb0mpnT68v0mz2/7f4muWcusSuPH1dsimKVHz3aaonrKlaWAMuqXMrZpz6hKJuujoxcLYnrKlaWAMtuThOVmjYRUfrSFNHU2ZS4rl5lCbCSs//AeO/D6Od1N/JE+V9HxXX1Kkt01viD2rpj6ejH0q0c0fCdCXFdxcoSYNGmx9eH6BmsVpYo2yqJ6ypWlgArfWzj5bnR/2Dl7xfFldBxCoRJYXmw+mvTZJ9+JvH8jXVEb8yNiiuh41UJk+ryYGVbU0TDF7NLu+HOsylxJXSCkDAJlwcr9c35XN/OWZv+zZbzg9HnKmGFp8ew/9DynDU48+TJzCA5d3/MUt+ezr0tFokrOgVmF6Ysue+GJbibXmCSBk5ZErD+t/lNizRJyDyvi7LYhA8/uiiLTfi+5mqiLDbh+5qPV5aRsNrMzG28skyEleEoGbyyDIRV5ihlvLIMhOVzFF8PZbH2ysJIKzQflsMMklbPfFgeM0ZaDpsPK+CFBKinYjCsLi+kC3gqAFig/dwBPBUALMx+7uGVZRiskBkjLQ8AC7ifd3VQFptynO5ooCw25Tjd00BZbMpxeqiBstiU4/SeBspi7ccziymoVxZrP55ZTFW9sljr8QxGWp75sDLMIGkF5sPymEHSaqNhAZ53A/AnDICF2aLmMbuuibAc2K/qA2Dhj1tc1cpijcczGGll2HxYPWaMtMoQWPgTYtXKYnOUxVxWrCw2qe3gK1YWm9R2aCtWFhtV0MqoVRZrO54BSauNhQW0rq9WWazreAYkrbL5sMrMIGn5cWDZ+2+3sqBCucQ4zoMXMl8Aa+Taz+NJ9YXyLgtZsVN8loeVu37aJiLVhXKHhazYKb4rD8uqXBsgIuWFco+FrNgpfkMe1iu3aj89/WVUeaE8YAZJa14eVrGzK5E7f21AdaG8xwySFsvDioBs6BQVF8oLzCBpuTFgTbRyRMN3S4BCufQF/oIyZYnOmtsQSRxQKJe+wF9VpyxhNzwYjqQbFQtQKJcWSahMWQIsShyN/pdQgEK5tEh6cGWpL5TLi6SAVpb6Qrm8SKrqlMU6jmdQ0mLjYZWZQdJy8bDgEyYXoCwMLMBQvKFMWaz77ZkVPMXvGQ/Lg/3KBZaHlVzrumuTOlz4A0mrKg0rd+b2uXr9TOvrYfUX/kDSCiVhWe9Mv0RR7D2bLe1uzwhpg5UlwCqVaCn5ona3Z8RkoMoSYKXGLVqKNZ4iZCTejTJUWQIsGnInUkSJt9bbJBvkdu5jlSXAOn7vszWU9v9oBHnlF/5Q0upJwvp/JU2UqIdDlJ5Oqu5jgqRVYDlY/ZM113WPPj3hum5tKqX4wh9IWlVJWMkjF+v1xoO7J+r1+sXjaxRf+ANJK5SERf1HP9h++ebr1F+sz9pajmeEdJHKEgWfcr/YmyZK7q+X9BzPiHGUKIvNGM+I8XDKEmEldjxn9fSehH4TZTEBTlkiLHp59xIt+9CQyvEMSlpdeVg0WDv4aoLIXnu4Nqh0PAOSlsNxnGWNfXWf+f7JvKV2PAOSlsemC95hRkkr0AcW4HH38MpSD0t6hyooUBbr3cdcoVN8Tyks/OgyRClLhNU3kE1q0cdESasbA1a21Slq0ccEScvhOLC+TNNCUn0qxzMgaXmxYE3RYqaymvUxV+IUP4gFa1d2IcMNZbB83NbUjQXrys2F3JnLajueEePClSW8htbBrPbjmdin+F48WNtoMZMD6ibKKGkF8WAFm5MaXPhDSasdC9bAhZtFUn97BiStDMeClZpw3TEiez3y+D6+dH3ICy/CWnOBb++m/G+dmR/y+o9n4p7i+/Fg0Y7tfWQ3H45R6sDW7/H/QrmsdDMIZYmw7AMJoo1/Viyi9+eatmShHH/mWYYpS/icZTfvjBAlZk81bQWF8gIzSlrlmLDS+/vp3acfWUQHGtNNW0GhvMqMkpYfExZNfr7v4bk0rf/ur08PN20FhfKQGSWtdlxY1tjxvWmi7VdmPj4SwUIXylkuZayymJ7L+Cai6DVEF8pdlkuAUZYIy/m2w53fOQq+UN5guXQxyhJew70nX0tQ33ClbEV/WehC+TxLxgEoS4T19qRFUUqFCBa4UC7vEQ+qrAhWcl+KniW171ATXyiX90gAVZY4z9qWjV0oR3qkC1UW/w28zVYHp2LWGAAAAABJRU5ErkJggg==';
        def sourceData = params.imagePath1;
// tokenize the data
        def parts = sourceData.tokenize(",");
        def imageString = parts[1];
// create a buffered image
        BufferedImage image = null;
        byte[] imageByte;

        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        image = ImageIO.read(bis);
        bis.close();

// write the image to a file
//        File outputfile = new File("images.png");
//        ImageIO.write(image, "png", outputfile);


        try {
//            BufferedImage awtImage = ImageIO.read(new File("Simple.jpg"));
            BufferedImage awtImage = image;
            PDXObjectImage ximage = new PDPixelMap(document, awtImage);
            float scale = 0.5f; // alter this value to set the image size
            cos.drawXObject(ximage, 15f, 350f, 450f, 250f);

            cos.beginText();
            cos.setFont(fontBold, 12);
            cos.moveTextPositionByAmount(15f, 750f);
            cos.drawString("Pharmacy Costs");
            cos.endText();

            cos.beginText();
            cos.setFont(fontPlain, 8);
            cos.moveTextPositionByAmount(15f, 730f);
            cos.drawString("Are prescription costs higher than they should be for my population?");
            cos.endText();

            cos.setLineWidth(1);
            cos.addLine(15, 726, 500, 726);
            cos.closeAndStroke();

            cos.beginText();
            cos.setFont(fontPlain, 8);
            cos.moveTextPositionByAmount(15f, 710f);
            cos.drawString("PHARMACY COST SUMMARYS");
            cos.endText();
            cos.beginText();
            cos.setFont(fontPlain, 8);
            cos.moveTextPositionByAmount(15f, 700f);
            cos.drawString(params.graphTitle1.toString().trim());
            cos.endText();

        } catch (FileNotFoundException fnfex) {
            System.out.println("No image for you");
        }

        // close the content stream for page 2
        cos.close();
        //page 2

        PDPage page2 = new PDPage(PDPage.PAGE_SIZE_A4);
        document.addPage(page2);
        cos = new PDPageContentStream(document, page2);

        def dParams=[:]
        dParams.module="table1Data"
        dParams.page=1
        dParams.max=30
        dParams.export=true
        def dashBoardParams=fetchParams(dParams)

/*
            params.sort=(session.table1.sort)?(session.table1.sort):"genericBrandSavings"
            params.order=(session.table1.order)?(session.table1.order.toString().capitalize()):"Desc"
            params.reportingBasis=(session.table1.reportingBasis)?(session.table1.reportingBasis):"PaidDate"
            params.fromDate=(session.table1.fromDate)?(session.table1.fromDate):"current"
            params.regional=(session.table1.regional)?(session.table1.regional):""
            params.brandName=(session.table1.brandName)?(session.table1.brandName):""


        def dashBoardParams =[:]
        dashBoardParams.page=params.page;
        dashBoardParams.order=params.sort+":"+params.order
        dashBoardParams.reportingBasis=params.reportingBasis
        dashBoardParams.regional=params.regional
        dashBoardParams.pageSize=params.max
        dashBoardParams.brandName=params.brandName
        switch(params.fromDate){
            case ("current"):
                dashBoardParams.reportingFrom="2014-07-01"
                dashBoardParams.reportingTo="2015-06-30"
                break;
            case ("past"):
                dashBoardParams.reportingFrom="2013-07-01"
                dashBoardParams.reportingTo="2014-06-30"
                break;
            case ("prior"):
                dashBoardParams.reportingFrom="2012-07-01"
                dashBoardParams.reportingTo="2013-06-30"
                break;
        }*/
//        dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")

        def dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])
        def data,total,size;

        try{
            data = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,"highPharmacyCost","default")
            total=data.total;
            size=data.totalHits;
            data.remove('total');
            data.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }
        def s2=frontEndOrdering(data,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d,"default")

        drawTable(page2, cos, 650, 15, s2,dParams.module);

        cos.beginText();
        cos.setFont(fontPlain, 8);
        cos.moveTextPositionByAmount(15f, 710f);
        String message=pdfMessage(code: "highPharmacy.container1.table1.mTitle1");
        cos.drawString(message);
//        cos.drawString("DRUGS WITH SAVING POTENTIAL");
        cos.endText();
        cos.setLineWidth(1);
        cos.addLine(15, 706, 500, 706);
        cos.closeAndStroke();
        cos.beginText();
        cos.setFont(fontPlain, 8);
        cos.moveTextPositionByAmount(15f, 690f);
        cos.drawString(params.graphTitle2.toString().trim());
        cos.endText();
        cos.close();





        PDPage page3 = new PDPage(PDPage.PAGE_SIZE_A4);
        document.addPage(page3);
        cos = new PDPageContentStream(document, page3);

        dParams.module="table2Data"
        dashBoardParams=fetchParams(dParams)
//        dashBoardParams.d.clientId=servletContext.getAt("clientIdentity")

        dynamicReportParams = highPharmacyService.setDynamicReportParams(dashBoardParams.d,session,[:])
        def data1,total1,size1;

        try{
            data1 = highPharmacyService.getHighPharmacyTable1(dynamicReportParams,"highPharmacyCost","speciality")
            total1=data.total;
            size1=data.totalHits;
            data1.remove('total');
            data1.remove('totalHits');
        }catch(e){
            log.error(e.getMessage());
            render status: 204;
        }

        def s3=frontEndOrdering(data1,dashBoardParams.p.order,dashBoardParams.p.sort,dashBoardParams.d,"rxSpecialityCostDetail")
        drawTable(page3, cos, 650, 15, s3,dParams.module);

        cos.beginText();
        cos.setFont(fontPlain, 8);
        cos.moveTextPositionByAmount(15f, 710f);
        cos.drawString("SPECIALITY DRUGS");
        cos.endText();
        cos.setLineWidth(1);
        cos.addLine(15, 706, 500, 706);
        cos.closeAndStroke();
        cos.beginText();
        cos.setFont(fontPlain, 8);
        cos.moveTextPositionByAmount(15f, 690f);
        cos.drawString(params.graphTitle3.toString().trim());
        cos.endText();
        cos.close();
        cos.close();

        // Save the results and ensure that the document is properly closed:
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        document.save(output);
        document.close();

        response.addHeader("Content-Type", "application/pdf");
        response.addHeader("Content-Disposition", "attachment; filename=HighPharmacy.pdf");
        response.getOutputStream().write(output.toByteArray());

    }


    public void drawTable(PDPage page, PDPageContentStream contentStream,
                                 float y, float margin,
                                 content,String table) throws IOException {
        final int rows = content.size()+1;
        int cols;
        if(table=="table1Data")
        cols = 7;
        else
        cols = 6;
        final float rowHeight = 20f;
        final float tableWidth = page.findMediaBox().getWidth()-(2*margin)+10f;
        final float tableHeight = rowHeight * rows;
        final float colWidth = (tableWidth/(float)cols);
        final float cellMargin=2f;

        //draw the rows
        float nexty = y ;
        contentStream.drawLine(margin,nexty,(margin+tableWidth).toFloat(),nexty);
        nexty-= (rowHeight);


        for (int i = 0; i <= content.size(); i++) {
            contentStream.drawLine(margin,nexty,(margin+tableWidth).toFloat(),nexty);
            nexty-= rowHeight;
        }
        //draw the columns
        float nextx = margin;
        for (int i = 0; i <= cols; i++) {
            contentStream.drawLine(nextx,y,nextx,(y-tableHeight).toFloat());
            nextx += colWidth;
        }
        //now add the text
        contentStream.setFont(PDType1Font.HELVETICA,7);
        float textx = margin+cellMargin;
        float texty = y-15;
//        def headers=["DRUG NAME","EST. SAVING IF GENERIC"],["EST. SAVING ","IF MAIL ORDER"],"PMPM COST","PMPM BENCHMARK","TOTAL COST",["TOTAL VOLUME ","POPULATION"]]
        def headers
        if(table=="table1Data")
            headers=["DRUG NAME","GENERIC SAVINGS","MAIL ORDER","PMPM COST","PMPM BENCHMARK","TOTAL COST","VOLUME/POPULATION"]
        else
            headers=["DRUG NAME","MAIL ORDER","PMPM COST","PMPM BENCHMARK","TOTAL COST","VOLUME/POPULATION"]
//        def keys=["drugName","genericBrandSavings","mailRetailSavings","paidAmountPmpm","annualCost","annualCost","prescriptions"]

        for(int k = 0 ; k < cols; k++){
            def text = headers[k];
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;
        }
            texty-=(rowHeight);
            textx = margin+cellMargin;
//        for(int i = 0; i < content.size(); i++){
            content.eachWithIndex { item, index->
                String text = item.drugName;
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;

                if(table=="table1Data"){
                    text = (float)item?.genericBrandSavings;
                    contentStream.beginText();
                    contentStream.moveTextPositionByAmount(textx,texty);
                    contentStream.drawString(text);
                    contentStream.endText();
                    textx += colWidth;
                }

                text = (float)item?.mailRetailSavings;
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;

                text = (float)item?.paidAmountPmpm;
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;

                text = (float)item?.annualCost;
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;

                text = (float)item?.annualCost;
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;


                text = item.prescriptions;
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx,texty);
                contentStream.drawString(text);
                contentStream.endText();

                texty-=rowHeight;
                textx = margin+cellMargin;
            }
    }






}

class HighPharmacyData {
    Date time
    BigDecimal benchMarkValue
    BigDecimal pharmacyClaims
    Integer memberMonths
}

class DrugsData {
    BigDecimal populationSize
//    BigDecimal mailRetailSavings
    BigDecimal savings
    BigDecimal annualCost
    BigDecimal paidAmountPmpm
    BigDecimal prescriptions
    BigDecimal rangeForNonReversal
    String drugName
    String genericOrBrand
    String equvalentGenericDrug
    String membersDrill


    /*public void setGenericBrandSavings(Integer num) {
        this.genericBrandSavings = setValue(num)
    }

    public void setMailRetailSavings(Integer num) {
        this.mailRetailSavings = setValue(num)
    }

    public void setAnnualCost(Integer num) {
        this.annualCost = setValue(num)
    }

    public void setPaidAmountPmpm(Integer num) {
        this.paidAmountPmpm = setValue(num)
    }

   def setValue(Integer num){
        if(num>=1000){ // only letters
            num = (num/1000);
        }
       return num
    }*/


}
