package com.deerwalk.z5

import au.com.bytecode.opencsv.CSVWriter
import com.z5.CareMetric
import grails.converters.JSON
import groovy.json.JsonSlurper

import javax.validation.constraints.NotNull
import java.nio.charset.Charset

class ErUtilizationController {

    def dashBoardChartService
    def highPharmacyService
    def grailsLinkGenerator
    def helperService
    def exportService
    def springSecurityService
    def populationRiskService
    def memberDrillService




    def index_1 = {
        def dataPh=new ArrayList();
        def dataPh2=new ArrayList();
        def datanew;
        def topCondition=[:];

        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
//        params.reportingFrom=reportingDates.priorFrom
//        params.reportingTo=reportingDates.priorTo
        def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
        params.reportingFrom=reportingDates.priorFrom
        params.reportingTo=reportingDates.priorTo

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")


        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0]
        dataPh.add(datanew)
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh2.add(datanew)
        def benchMarkValue=dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        def dataBm2=benchMarkValue.getAt('benchmark').getAt("ER Visits");



        params.reportingFrom=reportingDates.pastFrom
        params.reportingTo=reportingDates.pastTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh.add(datanew)
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh2.add(datanew)

        params.reportingFrom=reportingDates.currentFrom
//        params.reportingTo="${yearTo}-12-31"
        params.reportingTo=reportingDates.currentTo


        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh.add(datanew)
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh2.add(datanew)
        // http://das.deerwalk.local:8080/DasTest/zakiPoint?clientId=3000&reportingBasis=ServiceDate
        // &reportingTo=2014-06-30&reportingFrom=2013-07-01&report=overErUtilization
        // &eligibilityType=[medical]&phiCSDate=01-01-2010&phiCEDate=06-30-2015&dataFetchFrom=true&ticket=null&clientName=z5dev
        //date----------------------------
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        params.reportingBasis="PaidDate"
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def visitPer = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"overErUtilization").getAt("overUtilizingEr")
        visitPer.percent=visitPer.percentOfoverErUtilizor?(Math.round(visitPer?.percentOfoverErUtilizor * 100) / 100):0;
        visitPer.time="current"


        //http://das.deerwalk.local:8080/DasTest/zakiPoint?clientId=3000&reportingBasis=ServiceDate&reportingTo=2014-06-30&reportingFrom=2013-07-01
        //&report=topConditionByCostAvoidEr&eligibilityType=[medical]&phiCSDate=01-01-2010&phiCEDate=06-30-2015&dataFetchFrom=true&ticket=null&clientName=z5dev
        def otherCondition;
        params.reportingBasis="PaidDate"
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        /*dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topConditionByCostAllEr")
        def topCondition1=new ArrayList();
        BigDecimal totalTopCondition1=0;
        dataGraph.each { id, datas ->
            if(datas.condition!="other"){
                topCondition1.add(datas);
            }else{
                otherCondition=datas;
            }
            totalTopCondition1+=datas.totalAmount;
        }
        topCondition1.sort{
                -it.totalAmount
        }
        topCondition1.add(otherCondition);*/
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topConditionByCostAvoidEr")
        def topCondition2=new ArrayList();
        BigDecimal totalTopCondition2=0;
        dataGraph.each { id, datas ->
            if(datas.condition!="other"){
                topCondition2.add(datas);
            }else{
                otherCondition=datas;
            }
            totalTopCondition2+=datas.totalAmount;
        }
        topCondition2.sort{-it.totalAmount}
        topCondition2.add(otherCondition);

//        topCondition.graph1=topCondition1
//        topCondition.amount1=totalTopCondition1
        topCondition.graph2=topCondition2
        topCondition.amount2=totalTopCondition2

        //http://das.deerwalk.local:8080/DasTest/zakiPoint?clientId=3000&reportingBasis=PaidDate&reportingTo=2015-06-30
        // &reportingFrom=2014-07-01&report=savingsByAvoidableEr&eligibilityType=[medical]&phiCSDate=01-01-2010&phiCEDate=06-30-2015
        // &dataFetchFrom=true&ticket=null&clientName=z5dev
//        def dynamicReportParamGap = dashBoardChartService.setDynamicReportParams(params,session,[:])


        def dataGraphGap = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"savingsByAvoidableEr")
        def gapTotal=dataGraphGap.total;
        dataGraphGap.remove('total');

        /*dataGraphGap.each{
            def vals=it.value;
            vals.remove('uniqueMemberCount');
            vals.remove('erVisitCount');
            vals = vals.sort { it.key }
//            vals = vals.sort{a , b ->  a.key<=> b.key }
            vals.each {
//                if( (it.key!='uniqueMemberCount') && (it.key!='erVisitCount')){
                    println ">>cate----->>>>"+it.value.metricIdentifier
            }
        }*/


        [isEditable:false,topCondition:topCondition,dataPh:dataPh,dataPh2:dataPh2,dataBm2:dataBm2,visitPer:visitPer,gapTotal:gapTotal,dataGraphGap:dataGraphGap]
    }

    def costDriversDetails(){
        def datanew1=[:];
        def topCondition=[:];

        params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
        params.range=session.er.range?session.er.range:"0"
        params.isExclude=session.er.isExclude?session.er.isExclude:"false"


        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        def yearTo=reportingDates.currentTo.split("-")[0].toInteger()

        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")

        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo


        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])

//        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topConditionByCostAllEr")

        def topCondition1=new ArrayList();
        def otherCondition;

      /*  BigDecimal totalTopCondition1=0;
        dataGraph.each { id, datas ->
            if(datas.condition!="other"){
                topCondition1.add(datas);
            }else{
                otherCondition=datas;
            }
            totalTopCondition1+=datas.totalAmount;
        }
        topCondition1.sort{
            -it.totalAmount
        }
        topCondition1.add(otherCondition);*/


        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topConditionByCostAvoidEr");


        def topCondition2=new ArrayList();
        BigDecimal totalTopCondition2=0;
        dataGraph.remove('metaInfo')
        dataGraph.each { id, datas ->
            if(datas.condition!="other"){
                topCondition2.add(datas);
            }else{
                otherCondition=datas;
            }
            totalTopCondition2+=datas.totalAmount;
        }
        topCondition2.sort{-it.totalAmount}
        topCondition2.add(otherCondition);

//        topCondition.graph1=topCondition1
//        topCondition.amount1=totalTopCondition1
        topCondition.graph2=topCondition2
        topCondition.amount2=totalTopCondition2



        def dataPh2=new ArrayList();
        def dataPh4=new ArrayList();
        params.reportingFrom=reportingDates.priorFrom
        params.reportingTo=reportingDates.priorTo
        params.exclude=true;
        datanew1=[:]


        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableErVsUrgentVisit")
        datanew1.urgentCareVisitsFilter=dataGraph.getAt("urgentCareVisitsFilter").getAt("visitCountPer1000")
        datanew1.avoidableErVisit=dataGraph.getAt("avoidableErVisit").getAt("visitCountPer1000")
        datanew1.year=dynamicReportParam.reportingTo
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]

        dataPh2.add(datanew1)
        datanew1=[:]
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
        datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
        datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
        datanew1.year=dynamicReportParam.reportingTo
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]

        dataPh4.add(datanew1)

        //-----------------------------------------------------------------

        datanew1=[:]
        params.reportingFrom=reportingDates.pastFrom
        params.reportingTo=reportingDates.pastTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableErVsUrgentVisit")
        datanew1.urgentCareVisitsFilter=dataGraph.getAt("urgentCareVisitsFilter").getAt("visitCountPer1000")
        datanew1.avoidableErVisit=dataGraph.getAt("avoidableErVisit").getAt("visitCountPer1000")
        datanew1.year=dynamicReportParam.reportingTo
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]

        dataPh2.add(datanew1)
        datanew1=[:]
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
        datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
        datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
        datanew1.year=dynamicReportParam.reportingTo
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        dataPh4.add(datanew1)

        //-------------------------------------------------------------
        datanew1=[:]
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableErVsUrgentVisit")
        datanew1.urgentCareVisitsFilter=dataGraph.getAt("urgentCareVisitsFilter").getAt("visitCountPer1000")
        datanew1.avoidableErVisit=dataGraph.getAt("avoidableErVisit").getAt("visitCountPer1000")
        datanew1.year=dynamicReportParam.reportingTo
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]

        dataPh2.add(datanew1)
        datanew1=[:]
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
        datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
        datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
        datanew1.year=dynamicReportParam.reportingTo
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]

        dataPh4.add(datanew1)
//        def cycleEndDate=helperService.getCycleEndDate(params.ceDate,"${yearTo}-12-31")
        def cycleEndDate=reportingDates.currentTo

        [isEditable:false,topCondition:topCondition,dataPh2:dataPh2 as JSON,dataPh4:dataPh4 as JSON,cycleEndDate:cycleEndDate]

    }

    def topChronicDetails(){
        params.page=1;
        params.sortResults=true

        //==============================================
        params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
        params.range=session.er.range?session.er.range:"0"
        params.isExclude=session.er.isExclude?session.er.isExclude:"false"
        //==============================================

        params.sort=session.er.table1Sort?session.er.table1Sort:"percentOfnonErVisitor"
        params.order=session.er.table1Order?session.er.table1Order:"desc"

        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)

        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")

        params.mbrRegionId=""
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo


        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def  topChronicConditionEr = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topChronicConditionErDetail")
        topChronicConditionEr.remove("totalPaidAmountForAllCondition")

        def List<Object> erData = new ArrayList<Object>();
        topChronicConditionEr.each { id, datas ->
            memberDrillService.getChronicMemberUseDrillParams(datas,true,'member',params)
            erData.add(datas)
        }
        if(params.order=="asc"){
            erData=erData.sort{it.total[params.sort]}
        }else{

            erData=erData.sort{it.total[params.sort]}.reverse()

        }



        [topChronicConditionEr:erData,isEditable:false]

    }


    def index = {
        def dataPh=new ArrayList();
        def dataPh2=new ArrayList();
        def topLineGraph=new ArrayList();
        def datanew=[:];
        def datanew1=[:];
        def topCondition=[:];
        session.er=[:]
        /*params.trend="yearly"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
//        def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
        params.reportingFrom=reportingDates.priorFrom
        params.reportingTo=reportingDates.priorTo

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
        params.clientId=servletContext.getAt("clientIdentity")
        params.range="0"
        session.er.range=params.range


        def memberDrill1,memberDrill2;
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
        datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        memberDrillService.getErUtilizationTrendDrillParams(datanew,true,'member',params,true)

        dataPh.add(datanew)

        *//* //careAlertErVisit
        datanew=[:]
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
        datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
        datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
        datanew1.year=yearTo-2+"-01"
        dataPh2.add(datanew1)*//*

        dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
        datanew1=[:];
        def dataPh3=dataGraph.getAt('reporting')
        datanew1.benchmark=dataGraph.getAt('benchmark').getAt("ER Visits")
        datanew1.overAllTotalAmount=dataPh3.getAt('overAllTotalAmount').getAt("totalPaid")
        dataPh3.remove('overAllTotalAmount')
        datanew1.year=(dataPh3.keySet() as String[])[0];
        datanew1.erVisitPer1000PerMonth=dataPh3.getAt(datanew1.year).getAt("erVisitPer1000PerMonth")
        datanew1.totalPaidAmount=dataPh3.getAt(datanew1.year).getAt("totalPaid")
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        memberDrillService.getErUtilizationTrendDrillParams(datanew1,true,'member',params)
        topLineGraph.add(datanew1)



//        params.reportingFrom=reportingDates.pastFrom
//        params.reportingTo=reportingDates.pastTo

        params.reportingFrom=reportingDates.pastFrom
        params.reportingTo=reportingDates.pastTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
//        datanew.year=(dataGraph.keySet() as String[])[0];
        datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
        datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        memberDrillService.getErUtilizationTrendDrillParams(datanew,true,'member',params,true)
//        datanew['members_drill']=memberDrillService.getAvoidableToTotalErRatioDrillParams(datanew1,true,'member',params)
        dataPh.add(datanew)


         //careAlertErVisit
         *//*dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
         datanew1=[:];
         datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
         datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
         datanew1.year=yearTo-1+"-01"
         dataPh2.add(datanew1)*//*

        dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
        datanew1=[:];
        dataPh3=dataGraph.getAt('reporting')
        datanew1.benchmark=dataGraph.getAt('benchmark').getAt("ER Visits")
        datanew1.overAllTotalAmount=dataPh3.getAt('overAllTotalAmount').getAt("totalPaid")
        dataPh3.remove('overAllTotalAmount')
        datanew1.year=(dataPh3.keySet() as String[])[0];
        datanew1.erVisitPer1000PerMonth=dataPh3.getAt(datanew1.year).getAt("erVisitPer1000PerMonth")
        datanew1.totalPaidAmount=dataPh3.getAt(datanew1.year).getAt("totalPaid")
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        memberDrillService.getErUtilizationTrendDrillParams(datanew1,true,'member',params)
        topLineGraph.add(datanew1)


        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
//        params.reportingTo=helperService.getReportingEndDate(params.ceDate,"${yearTo}-12-31")

        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
//        datanew.year=(dataGraph.keySet() as String[])[0];
        datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
        datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        memberDrillService.getErUtilizationTrendDrillParams(datanew,true,'member',params,true)

        dataPh.add(datanew)

         //careAlertErVisit
         *//*datanew1=[:];
         dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
         datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
         datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
         datanew1.year=yearTo+"-01"
         dataPh2.add(datanew1)*//*
        dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
        datanew1=[:];
        dataPh3=dataGraph.getAt('reporting')
        datanew1.benchmark=dataGraph.getAt('benchmark').getAt("ER Visits")
        datanew1.overAllTotalAmount=dataPh3.getAt('overAllTotalAmount').getAt("totalPaid")
        dataPh3.remove('overAllTotalAmount')
        datanew1.year=(dataPh3.keySet() as String[])[0];
        datanew1.erVisitPer1000PerMonth=dataPh3.getAt(datanew1.year).getAt("erVisitPer1000PerMonth")
        datanew1.totalPaidAmount=dataPh3.getAt(datanew1.year).getAt("totalPaid")
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        memberDrillService.getErUtilizationTrendDrillParams(datanew1,true,'member',params)


//
//  datanew1.year=helperService.getReportingLast(params.ceDate,"${yearTo}-12")

        topLineGraph.add(datanew1)


        //=============================================================================
        /*//**********************erUtilizationTrend*************************************
        //=============================================================================

        *//*params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
        def dataPh3=dataGraph.getAt('reporting')
        def dataBm=dataGraph.getAt('benchmark').getAt("ER Visits")
        def prevTotal=dataPh3.getAt('previousYearsTotalAmount').getAt("totalPaidAmount")
        dataPh3.remove('previousYearsTotalAmount')*//*

        // http://das.deerwalk.local:8080/DasTest/zakiPoint?clientId=servletContext.getAt("clientIdentity")&reportingBasis=ServiceDate
        // &reportingTo=2014-06-30&reportingFrom=2013-07-01&report=overErUtilization
        // &eligibilityType=[medical]&phiCSDate=01-01-2010&phiCEDate=06-30-2015&dataFetchFrom=true&ticket=null&clientName=z5dev
        //date----------------------------

        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def  topChronicConditionEr = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topChronicConditionErDetail")
//        topChronicConditionEr.remove("total")
        topChronicConditionEr.remove("totalPaidAmountForAllCondition")
        topChronicConditionEr.remove("reportingDates")
        def List<Object> erData = new ArrayList<Object>();
        topChronicConditionEr.each { id, datas ->
            memberDrillService.getChronicMemberUseDrillParams(datas,true,'member',params)
            erData.add(datas)
        }

        erData=erData.sort{it.total['percentOfnonErVisitor']}.reverse()


        //erVisitsLikelyToIncrease
        //erVisitsLikelyToIncrease
        //erVisitsLikelyToIncrease

//        params.reportingTo=helperService.getReportingEndDate(params.ceDate,"${yearTo}-12-31")
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def erVisitsLikelyToIncrease ;
        def datanew3=[:];
        def lineGraphR=[];
        erVisitsLikelyToIncrease = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"erVisitsLikelyToIncrease")


        *//*params.reportingFrom=helperService.addDay(params.reportingTo)
        params.reportingTo=helperService.addYear(params.reportingFrom)*//*

        params.reportingFrom=params.reportingTo
        params.reportingTo=helperService.addYear(params.reportingFrom)


        datanew3.put('year',params.reportingFrom)
        datanew3.put('actual',erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"))
        lineGraphR.add(datanew3)
        datanew3=[:];
        datanew3.put('year',params.reportingTo)
        def endValue;
        try{
            endValue=erVisitsLikelyToIncrease.getAt("riskScoreEr").getAt("riskScoreEr")*erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000")
        }catch(Exception e){
            endValue=0
        }
        if(Double.isNaN(endValue))
            endValue=0;
        datanew3.put('actual',endValue)
        lineGraphR.add(datanew3)


        def getMax=getMaximum(topLineGraph,lineGraphR)
//        def getMax=200

        def yTotal=erVisitsLikelyToIncrease.getAt("percentLikelyToIncreaseOrDecrease")

        def yPercentage=0;
        if(Double.isNaN(yPercentage))
            yPercentage=0;*/

        //old logic for er likely to increase

        /*datanew3.put('year',params.reportingFrom)
        datanew3.put('actual',erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"))
        lineGraphR.add(datanew3)
        datanew3=[:];
        datanew3.put('year',params.reportingTo)
        def endValue;
        try{
            endValue=erVisitsLikelyToIncrease.getAt("riskScoreAll").getAt("riskScoreAll")*erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000")
        }catch(Exception e){
            endValue=0
        }
        if(Double.isNaN(endValue))
            endValue=0;
        datanew3.put('actual',endValue)
        lineGraphR.add(datanew3)

        def getMax=getMaximum(topLineGraph,lineGraphR)
//        def getMax=200

        def yPercentage;
        def yTotals=erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("totalPaid")
        try{
            yPercentage=((erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("totalPaid")*erVisitsLikelyToIncrease.getAt("riskScoreEr").getAt("riskScoreEr"))/(erVisitsLikelyToIncrease.getAt("overAllTotalAmount").getAt("totalPaid")*erVisitsLikelyToIncrease.getAt("riskScoreAll").getAt("riskScoreAll")))*100
        }catch(Exception e){
            yPercentage=0
        }
//        def yTotal=formatNumber(number: yTotals,type:"currency",currencyCode: "USD",maxFractionDigits: "0")
        def yTotal=formatZ5Currency(number: yTotals,type:"number",maxFractionDigits: "2")


        if(Double.isNaN(yPercentage))
            yPercentage=0;*/
        //old logic for er likely to increase


//        def cycleEndDate=helperService.getCycleEndDate(params.ceDate,"${yearTo}-12-31")

//        render(view:"index1",model: [isEditable:false,topCondition:topCondition,dataPh:dataPh,dataPh2:dataPh2,dataBm2:dataBm2,visitPer:visitPer,gapTotal:gapTotal,dataGraphGap:dataGraphGap] )
//        [defaultSort:"sortable sorted desc",getMax:getMax,isEditable:false,topCondition:topCondition,dataPh:dataPh as JSON,dataPh3:topLineGraph as JSON,topChronicConditionEr:erData,lineGraphR:lineGraphR as JSON,yPercentage:yPercentage,yTotal:yTotal]
        [isEditable:false]
    }

    def getMaximum(a,b){
        def aMax=0;
        a.each{
            def val2=it.erVisitPer1000PerMonth>it.benchmark.toDouble()?it.erVisitPer1000PerMonth:it.benchmark.toDouble()
            if(val2>aMax)
                aMax=val2

        }
        def bMax=0;

        b.each{
            def val2=it.actual
            if(val2>bMax)
                bMax=val2
        }

        def val= aMax>bMax?aMax:bMax
        return val *1.1
    }





    def mapGapInCare(){
        def code=params.metric.tokenize(".")
        def test2=CareMetric[code[0].toLowerCase()]
        def value=code[1].toInteger()
        String text = (value < 10 ? "0" : "") + value;
        def test1=test2.toString().concat(text)
        render (test1)
    }

    def getChartScriptForGraph1(){
        def dataPh=new ArrayList();
        def dataPh2=new ArrayList();
        def datas=[:];
        def datanew;
        def basis=params.reportingBasis?params.reportingBasis:"PaidDate"

        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
        params.reportingFrom=reportingDates.priorFrom
        params.reportingTo=reportingDates.priorTo

        params.reportingBasis=basis
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh.add(datanew)
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh2.add(datanew)

        def benchMarkValue=dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        def dataBm2=benchMarkValue.getAt('benchmark').getAt("ER Visits");

        params.reportingFrom=reportingDates.pastFrom
        params.reportingTo=reportingDates.pastTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh.add(datanew)
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh2.add(datanew)

        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh.add(datanew)
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
        datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
        datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
        dataPh2.add(datanew)

        datas.table1=dataPh
        datas.table2=dataPh2
        datas.bm2=dataBm2

        render datas as JSON
    }

    def getChartScriptTable3(){
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)

        switch(params.fromDate){
            case ("current"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                break;
        }
        def basis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.reportingBasis=basis
        def topCondition=[:];
        def otherCondition;
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topConditionByCostAllEr")


        def topCondition1=new ArrayList();
        BigDecimal totalTopCondition1=0;
        dataGraph.each { id, datas ->
            if(datas.condition!="other"){
                topCondition1.add(datas);
            }else{
                otherCondition=datas;
            }
            totalTopCondition1+=datas.totalAmount;
        }
        topCondition1.sort{-it.totalAmount}
        topCondition1.add(otherCondition);

        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topConditionByCostAvoidEr")

        def topCondition2=new ArrayList();
        BigDecimal totalTopCondition2=0;
        dataGraph.each { id, datas ->
            if(datas.condition!="other"){
                topCondition2.add(datas);
            }else{
                otherCondition=datas;
            }
            totalTopCondition2+=datas.totalAmount;
        }
        topCondition2.sort{-it.totalAmount}
        topCondition2.add(otherCondition);

        topCondition.graph1=topCondition1
        topCondition.amount1=totalTopCondition1
        topCondition.graph2=topCondition2
        topCondition.amount2=totalTopCondition2

        render(template: "topCondition",model: [topCondition: topCondition])
    }

    def sortResult(){
        params.page=1;
        params.sortResults=true
        params.fromDate=session.er.fromDate?session.er.fromDate:"current"
        params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
        params.range=session.er.range?session.er.range:"0"
        params.isExclude=session.er.isExclude?session.er.isExclude:"false"
        session.er.table1Order=params.order
        session.er.table1Sort=params.sort

        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)

        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
        switch(params.fromDate){
            case ("current"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                break;
        }
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])

            def  topChronicConditionEr = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topChronicConditionErDetail")
        topChronicConditionEr.remove("totalPaidAmountForAllCondition")
            def List<Object> erData = new ArrayList<Object>();
            topChronicConditionEr.each { id, datas ->
                memberDrillService.getChronicMemberUseDrillParams(datas,true,'member',params)
                erData.add(datas)
            }


        if(params.order=="asc"){
            erData=erData.sort{it.total[params.sort]}
        }else{

            erData=erData.sort{it.total[params.sort]}.reverse()

        }

            render(template: "tableDataDetails",model:[data:erData,show:params.show=='true'?true:false])

    }


    def getChartScriptTable4(){
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)

        switch(params.fromDate){
            case ("current"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                break;
        }


        def basis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.reportingBasis=basis
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraphGap = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"savingsByAvoidableEr")
        def gapTotal=dataGraphGap.total;
        dataGraphGap.remove('total');



        render(template: "gapInCare",model: [gapTotal: gapTotal,dataGraphGap:dataGraphGap])
    }


    def getChartScriptTable2(){
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)

        switch(params.fromDate){
            case ("current"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                break;
        }
        def basis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.reportingBasis=basis
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def visitPer = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"overErUtilization").getAt("overUtilizingEr")
        visitPer.percent=visitPer.percentOfoverErUtilizor?(Math.round(visitPer?.percentOfoverErUtilizor * 100) / 100):0;
        visitPer.time=params.fromDate
        render(template: "visitPercent",model: [visitPer:visitPer])



    }

    def getScriptForLineGraph(){

        params.fromDate=params.fromDate?params.fromDate:"current"
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.range=params.range?params.range:""
        params.exclude=params.excludes?params.excludes:""

        session.er.fromDate= params.fromDate
        session.er.reportingBasis= params.reportingBasis
        session.er.range= params.range
        session.er.exclude= params.excludes




        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)


        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
        switch(params.fromDate){
            case ("current"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                break;
        }

        reportingDates=helperService.getReportingDates(params.ceDate)
        def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
        def datanew1=[:]
        def dataPh2=[]
        params.reportingFrom=reportingDates.priorFrom
        params.reportingTo=reportingDates.priorTo
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
        datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
        datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
        datanew1.year=yearTo-2+"-01"
        dataPh2.add(datanew1)

        params.reportingFrom=reportingDates.pastFrom
        params.reportingTo=reportingDates.pastTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
        datanew1=[:];
        datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
        datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
        datanew1.year=yearTo-1+"-01"
        dataPh2.add(datanew1)


        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
        datanew1=[:];
        datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
        datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
        datanew1.year=yearTo+"-01"
        dataPh2.add(datanew1)
        render dataPh2 as JSON

    }

    def getScriptForER(){
        params.fromDate=params.fromDate?params.fromDate:"current"
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.isExclude=params.isExclude?params.isExclude:"false"
        params.range=params.range?params.range:"0"


        session.er.fromDate= params.fromDate
        session.er.reportingBasis= params.reportingBasis
        session.er.range= params.range
        session.er.isExclude= params.isExclude





        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)


        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")

        params.mbrRegionId=""
        switch(params.fromDate){
            case ("current"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                break;
            case ("past"):
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                break;
            case ("prior"):
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                break;
        }



        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def yPercentage

        def lineGraphR=new ArrayList();
        def topLineGraph=new ArrayList();

        if(params.module=="lineGraph"){
            params.trend="yearly"
            /*def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
            def dataPh3=dataGraph.getAt('reporting')
            def dataBm=dataGraph.getAt('benchmark').getAt("ER Visits")

            def prvTotal=dataPh3.getAt('previousYearsTotalAmount').getAt("totalPaidAmount")
            dataPh3.remove('previousYearsTotalAmount')
            def map=[:]
            map.da=dataPh3
            map.dm=dataBm
            map.pv=prvTotal

            render map as JSON*/

            reportingDates=helperService.getReportingDates(params.ceDate)
            def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
            def datanew1=[:];
            def dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=dataGraph.getAt('benchmark').getAt("ER Visits")
            datanew1.overAllTotalAmount=dataPh3.getAt('overAllTotalAmount').getAt("totalPaid")
            dataPh3.remove('overAllTotalAmount')
            datanew1.year=(dataPh3.keySet() as String[])[0]
            datanew1.erVisitPer1000PerMonth=dataPh3.getAt(datanew1.year).getAt("erVisitPer1000PerMonth")
            datanew1.totalPaidAmount=dataPh3.getAt(datanew1.year).getAt("totalPaid")
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getErUtilizationTrendDrillParams(datanew1,true,'member',params)
            topLineGraph.add(datanew1)

            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
            datanew1=[:];
            dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=dataGraph.getAt('benchmark').getAt("ER Visits")
            datanew1.overAllTotalAmount=dataPh3.getAt('overAllTotalAmount').getAt("totalPaid")
            dataPh3.remove('overAllTotalAmount')
            datanew1.year=(dataPh3.keySet() as String[])[0]
            datanew1.erVisitPer1000PerMonth=dataPh3.getAt(datanew1.year).getAt("erVisitPer1000PerMonth")
            datanew1.totalPaidAmount=dataPh3.getAt(datanew1.year).getAt("totalPaid")
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getErUtilizationTrendDrillParams(datanew1,true,'member',params)
            topLineGraph.add(datanew1)

            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")
            datanew1=[:];
            dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=dataGraph.getAt('benchmark').getAt("ER Visits")
            datanew1.overAllTotalAmount=dataPh3.getAt('overAllTotalAmount').getAt("totalPaid")
            dataPh3.remove('overAllTotalAmount')
            datanew1.year=(dataPh3.keySet() as String[])[0]
            datanew1.erVisitPer1000PerMonth=dataPh3.getAt(datanew1.year).getAt("erVisitPer1000PerMonth")
            datanew1.totalPaidAmount=dataPh3.getAt(datanew1.year).getAt("totalPaid")
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getErUtilizationTrendDrillParams(datanew1,true,'member',params)
            topLineGraph.add(datanew1)


            //----------------2nd one
            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            yearTo=params.reportingTo.split("-")[0].toInteger()


            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def erVisitsLikelyToIncrease ;

            erVisitsLikelyToIncrease = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"erVisitsLikelyToIncrease")

           /* params.reportingFrom=reportingDates.currentFrom
            params.reportingTo="${yearTo+1}-01-01"*/

            params.reportingFrom=reportingDates.currentTo
            params.reportingTo=helperService.addYear(params.reportingFrom)

            def datanew3=[:];
            datanew3.put('year',params.reportingFrom)
            datanew3.put('actual',erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"))
            lineGraphR.add(datanew3)
            datanew3=[:];
            datanew3.put('year',params.reportingTo)
            def endValue;
            try{
//                endValue=erVisitsLikelyToIncrease.getAt("riskScoreEr").getAt("riskScoreEr")*erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000")
                endValue=erVisitsLikelyToIncrease.getAt("predictedERVisitsPer1000InNext12Months")
            }catch(Exception e){
                endValue=0
            }
            if(Double.isNaN(endValue))
                endValue=0;
            datanew3.put('actual',endValue)
            lineGraphR.add(datanew3)

            def getMax=getMaximum(topLineGraph,lineGraphR)

            def yTotal=erVisitsLikelyToIncrease.getAt("percentLikelyToIncreaseOrDecrease")

            def getmax=getMaximum(topLineGraph,lineGraphR)
            yPercentage=0;

            def map=[:]
            map.topLineGraph=topLineGraph
            map.max=getmax

            map.dm=lineGraphR
            map.yPercentage=yPercentage
            map.yTotal=yTotal
            map.max=getmax

            render map as JSON

        }else if(params.module=="topChronicConditionEr"){
            params.sort=session.er.table1Sort?session.er.table1Sort:"percentOfnonErVisitor"
            params.order=session.er.table1Order?session.er.table1Order:"desc"

            switch(params.fromDate){
                case ("current"):
                    params.reportingFrom=reportingDates.currentFrom
                    params.reportingTo=reportingDates.currentTo
                    break;
                case ("past"):
                    params.reportingFrom=reportingDates.pastFrom
                    params.reportingTo=reportingDates.pastTo
                    break;
                case ("prior"):
                    params.reportingFrom=reportingDates.priorFrom
                    params.reportingTo=reportingDates.priorTo
                    break;
            }




            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def  topChronicConditionEr = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topChronicConditionErDetail")
            topChronicConditionEr.remove("totalPaidAmountForAllCondition")
            def List<Object> erData = new ArrayList<Object>();
            topChronicConditionEr.each { id, datas ->
                memberDrillService.getChronicMemberUseDrillParams(datas,true,'member',params)

                erData.add(datas)
            }

            if(params.order=="asc"){
                erData=erData.sort{it.total[params.sort]}
            }else{

                erData=erData.sort{it.total[params.sort]}.reverse()

            }
            def defSort="sortable sorted desc"
//            erData=erData.sort{it.total['percentOfnonErVisitor']}.reverse()

            render(template: "tableDataDetails",model:[data:erData,show:false])
        }else if(params.module=="erVisitsLikelyToIncrease"){

          /*  def yearTo=params.reportingTo.split("-")[0].toInteger()


            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def erVisitsLikelyToIncrease ;
            def datanew3=[:];
            erVisitsLikelyToIncrease = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"erVisitsLikelyToIncrease")


            params.reportingFrom="${yearTo}-09-01"
            params.reportingTo="${yearTo+1}-08-01"
            lineGraphR=[];
            datanew3=[:]
            datanew3.put('year',params.reportingFrom)
            datanew3.put('actual',erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"))
            lineGraphR.add(datanew3)
            datanew3=[:];
            datanew3.put('year',params.reportingTo)
            datanew3.put('actual',erVisitsLikelyToIncrease.getAt("riskScoreAll").getAt("riskScoreAll")*erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"))
            lineGraphR.add(datanew3)
            println "===================="
            println "===================="
            println "===================="+lineGraphR

            yPercentage=((erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("totalPaidAmount")*erVisitsLikelyToIncrease.getAt("riskScoreEr").getAt("riskScoreEr"))/(erVisitsLikelyToIncrease.getAt("overAllTotalAmount").getAt("totalPaidAmount")*erVisitsLikelyToIncrease.getAt("riskScoreAll").getAt("riskScoreAll")))*100
*/
            def arr=[:]
            arr.dm=lineGraphR
            arr.yPercentage=yPercentage
            arr.max=getMaximum(topLineGraph,lineGraphR)
            render arr as JSON


        }else if(params.module=="barGraph"){
            reportingDates=helperService.getReportingDates(params.ceDate)
            def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            def datanew=[:]
            def dataPh=[]

            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getErUtilizationTrendDrillParams(datanew,true,'member',params,true)
            dataPh.add(datanew)




            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getErUtilizationTrendDrillParams(datanew,true,'member',params,true)
            dataPh.add(datanew)


            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
            datanew=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            datanew.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            memberDrillService.getErUtilizationTrendDrillParams(datanew,true,'member',params,true)
            dataPh.add(datanew)

            render dataPh as JSON


        }else if(params.module=="lineBottom"){

            params.exclude=params.excludes?params.excludes:""



            reportingDates=helperService.getReportingDates(params.ceDate)
            def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
            def datanew1=[:]
            def dataPh2=[]
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
            datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
            datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            dataPh2.add(datanew1)

            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
            datanew1=[:];
            datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
            datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            dataPh2.add(datanew1)


            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
            dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
            datanew1=[:];
            datanew1.noPreventiveVisit=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
            datanew1.noRoutineVisit=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
            datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
            dataPh2.add(datanew1)
            render dataPh2 as JSON
        }




    }

    def exportCsv(){

        def data;
        Map labels,formatter,parameters;
        List fields;
        response.contentType = grailsApplication.config.grails.mime.types["csv"]
        response.setHeader("Content-disposition", "attachment; filename=${params.exportModule}.csv")
//        response.setContentType("application/ms-excel"); // or you can use text/csv
//        response.setHeader("Content-Disposition", "attachment; filename=output.csv");
        OutputStream out = response.getOutputStream();

        params.fromDate=params.exportFromDate?params.exportFromDate:"current"
        params.reportingBasis=params.exportReportingBasis?params.exportReportingBasis:"PaidDate"
        params.range=params.exportRange?params.exportRange:"0"
        params.isExclude=params.exportIsExclude?params.exportIsExclude:"false"
        params.module=params.exportModule
        params.trend="monthly"
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)

        switch(params.module){
            case ("erUtilization"):
                def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
                params.reportingFrom=reportingDates.priorFrom
//                params.reportingTo=helperService.getReportingEndDate(params.ceDate,"${yearTo}-12-31")
                params.reportingTo=reportingDates.currentTo
                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def dataGraph = dashBoardChartService.getDashBoardChartScriptWBenchmark(dynamicReportParam,"erUtilizationTrend")

                def dataPh3=dataGraph.getAt('reporting')
                def dataBm=dataGraph.getAt('benchmark').getAt("ER Visits")
                dataPh3.remove('overAllTotalAmount')
                dataPh3=dataPh3

                List<HashMap> graphData = new ArrayList<HashMap>();

                dataPh3.each { id, datas ->
                    Map dgs=[:]
                    dgs.time=Date.parse('yyyy-MM',id)
                    dgs.erVisitPer1000PerMonth=formatNumber(number: datas['erVisitPer1000PerMonth'],maxFractionDigits: 2)
                    dgs.totalPaidAmount=formatNumber(number: datas['totalPaid'],maxFractionDigits: 2)
                    dgs.benchMarkValue=formatNumber(number:dataBm,maxFractionDigits: 2)
                    graphData.add(dgs);
                }
                graphData.sort{it.time}

                fields = [
                        "Time",
                        "ER Spend",
                        "ER Spend Benchmark",
                        "Er Visit Per 1000 Per Month",
                        "Er Visit Per 1000 Benchmark"
                ]

                data=graphData;
                helperService.csvWriter(fields,data,out);

            //
            //
            //for 2nd line graph
            //
            //
            //


                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def erVisitsLikelyToIncrease ;
                def datanew3=[:];
                def lineGraphR=[];
                erVisitsLikelyToIncrease = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"erVisitsLikelyToIncrease")

                datanew3.put('year',Date.parse("yyyy-MM-dd", params.reportingTo))
                datanew3.put('actual1',formatNumber(number: erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("totalPaid"),maxFractionDigits: 2))
                datanew3.put('actual2',formatNumber(number: erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"),maxFractionDigits: 2))
                datanew3.put('projected2',erVisitsLikelyToIncrease.getAt("riskScoreEr").getAt("riskScoreEr"))

                lineGraphR.add(datanew3)

                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                erVisitsLikelyToIncrease = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"erVisitsLikelyToIncrease")
                datanew3=[:];
                datanew3.put('year',Date.parse("yyyy-MM-dd", params.reportingTo))
                datanew3.put('actual1',formatNumber(number: erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("totalPaid"),maxFractionDigits: 2))
                datanew3.put('actual2',formatNumber(number: erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"),maxFractionDigits: 2))
                datanew3.put('projected2',erVisitsLikelyToIncrease.getAt("riskScoreEr").getAt("riskScoreEr"))

                lineGraphR.add(datanew3)

                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                erVisitsLikelyToIncrease = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"erVisitsLikelyToIncrease")
                datanew3=[:];
                datanew3.put('year',Date.parse("yyyy-MM-dd", params.reportingTo))
                datanew3.put('actual1',formatNumber(number: erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("totalPaid"),maxFractionDigits: 2))
                datanew3.put('actual2',formatNumber(number: erVisitsLikelyToIncrease.getAt("totalErVisits").getAt("erVisitPer1000"),maxFractionDigits: 2))
                datanew3.put('projected2',erVisitsLikelyToIncrease.getAt("riskScoreEr").getAt("riskScoreEr"))

                lineGraphR.add(datanew3)

                fields = [
                        "Time",
                        "Annualized ER Cost Actual",
                        "Projected(for next 12 months)",
                        "Annualized ER Visits/1000",
                        "Projected(for next 12 months)"
                ]
                helperService.csvWriter(fields,lineGraphR,out);

                break;


            case('avoidableToTotalErRatio'):
                reportingDates=helperService.getReportingDates(params.ceDate)
                def yearTo=reportingDates.currentTo.split("-")[0].toInteger()
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                def datanew=[:]
                def dataPh=[]

                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
                datanew.erRatio=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
//                datanew.year=(dataGraph.keySet() as String[])[0].toInteger();
                datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
                datanew.erVisitRatio=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                dataPh.add(datanew)

                datanew=[:]
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
                datanew.erRatio=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
                datanew.erVisitRatio=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                dataPh.add(datanew)

                datanew=[:]
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
                datanew.erRatio=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                datanew.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)


                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidabletoTotalErVisitRatio")
                datanew.erVisitRatio=dataGraph.getAt((dataGraph.keySet() as String[])[0]);
                dataPh.add(datanew)
                fields = [
                        "Time",
                        "Total ER Spend",
                        "Avoidable ER Spend",
                        "% of total spend that was avoidable",
                        "Total ER Visits (per 1000)",
                        "Avoidable ER Visits  (per 1000)",
                        "% of total visits that were avoidable"
                ]

                helperService.csvWriter(fields,dataPh,out);

                break;
            case ("topChronicConditionEr"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                params.sort=session.er.table1Sort?session.er.table1Sort:"percentOfnonErVisitor"
                params.order=session.er.table1Order?session.er.table1Order:"desc"
                params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
                params.range=session.er.range?session.er.range:"0"
                params.isExclude=session.er.isExclude?session.er.isExclude:"false"

                def dateArr=[params.reportingFrom,params.reportingTo];
                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def  topChronicConditionEr = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topChronicConditionErDetail")
                topChronicConditionEr.remove("totalPaidAmountForAllCondition")
                def List<Object> erData = new ArrayList<Object>();
                topChronicConditionEr.each { id, datas ->
                    memberDrillService.getChronicMemberUseDrillParams(datas,true,'member',params)
                    erData.add(datas)
                }

                if(params.order=="asc"){
                    erData=erData.sort{it.total[params.sort]}
                }else{

                    erData=erData.sort{it.total[params.sort]}.reverse()

                }

                fields = [
                        "Date Start",
                        "Date End",
                        "Member Condition",
                        "Member Diagnosis for ER Visit",
                        "Chronic Condition Member Count  ",
                        "ER Visited Chronic Condition Member Count",
                        "Total cost per member with ER visit and condition",
                        "Count of members with condition and no ER visits",
                        "Count of members with condition and one ER visit",
                        "Count of members with condition and 2 or more ER visits"
                ]
                helperService.csvWriter(fields,erData,out,dateArr);
                break;
            case("topChronicConditionErBrief"):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                params.sort=session.er.table1Sort?session.er.table1Sort:"percentOfnonErVisitor"
                params.order=session.er.table1Order?session.er.table1Order:"desc"

                def dateArr=[params.reportingFrom,params.reportingTo];
                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def  topChronicConditionEr = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topChronicConditionErDetail")
                topChronicConditionEr.remove("totalPaidAmountForAllCondition")
                def List<Object> erData = new ArrayList<Object>();
                topChronicConditionEr.each { id, datas ->
                    memberDrillService.getChronicMemberUseDrillParams(datas,true,'member',params)
                    erData.add(datas)
                }

                if(params.order=="asc"){
                    erData=erData.sort{it.total[params.sort]}
                }else{

                    erData=erData.sort{it.total[params.sort]}.reverse()

                }
                fields = [
                "Date Start",
                "Date End",
                "Member Condition",
                "Chronic Condition Member Count",
                "Er Visited Chronic Condition Member Count",
//                "Count of Members with condition and ER Diagnosis",
                "Total cost per member with ER visit and condition",
                "Count of members with condition and no ER visits",
                "Count of members with condition and one ER visit",
                "Count of members with condition and 2 or more ER visits"
                        ]
                helperService.csvWriter(fields,erData,out,dateArr);
                break;

            case ("topConditionByCostAvoidEr"):

                def topCondition=[:];

                params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
                params.range=session.er.range?session.er.range:"0"
                params.isExclude=session.er.isExclude?session.er.isExclude:"false"


                params.csDate=session.csDate
                params.ceDate=session.ceDate
                reportingDates=helperService.getReportingDates(params.ceDate)
//                def yearTo=reportingDates.currentTo.split("-")[0].toInteger()

                params.mbrRegionId=""
//                params.clientId=servletContext.getAt("clientIdentity")

                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                def dateArr=[params.reportingFrom,params.reportingTo];
                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def dataGraph

                    def otherCondition;
                    dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topConditionByCostAvoidEr")
                    dataGraph.remove('metaInfo')
                    def topCondition2=new ArrayList();
                    BigDecimal totalTopCondition2=0;
                    dataGraph.each { id, datas ->
                        if(datas.condition!="other"){
                            topCondition2.add(datas);
                        }else{
                            otherCondition=datas;
                        }
                        totalTopCondition2+=datas.totalAmount;
                    }
                    topCondition2=topCondition2.sort{-it.totalAmount}
                    topCondition2.add(otherCondition);

                    fields = [
                            "To Date",
                            "From Date",
                            "Avoidable ER Diagnosis",
                            "Total ER spend for Avoidable Diagnosis",
                            "% of Total ER Spend",
                            "Total ER visits/1000 for Avoidable Diagnosis",
                            "% of Total ER visits/1000"
                    ]
                    helperService.csvWriter(fields,topCondition2,out,dateArr)

                    break;
            case ("avoidableErVsUrgentVisit"):
                def datanew1=[:];
                def dataPh2=new ArrayList();
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
                params.range=session.er.range?session.er.range:"0"
                params.isExclude=session.er.isExclude?session.er.isExclude:"false"
                params.exclude=true;
                datanew1=[:]
                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableErVsUrgentVisit")
                datanew1.urgent=dataGraph.getAt("urgentCareVisitsFilter").getAt("visitCountPer1000")
                datanew1.avoidable=dataGraph.getAt("avoidableErVisit").getAt("visitCountPer1000")
                datanew1.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataPh2.add(datanew1)


                datanew1=[:]
                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableErVsUrgentVisit")
                datanew1.urgent=dataGraph.getAt("urgentCareVisitsFilter").getAt("visitCountPer1000")
                datanew1.avoidable=dataGraph.getAt("avoidableErVisit").getAt("visitCountPer1000")
                datanew1.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataPh2.add(datanew1)


                datanew1=[:]
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableErVsUrgentVisit")
                datanew1.urgent=dataGraph.getAt("urgentCareVisitsFilter").getAt("visitCountPer1000")
                datanew1.avoidable=dataGraph.getAt("avoidableErVisit").getAt("visitCountPer1000")
                datanew1.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataPh2.add(datanew1)

                fields = [
                        "Time",
                        "Total Avoidable ER Visits/1000",
                        "Total Urgent Care Visits/1000"
                ]
                helperService.csvWriter(fields,dataPh2,out);
                break;

            case ("careAlertErVisit"):
                def datanew1=[:];
                def dataPh4=new ArrayList();
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
                params.range=session.er.range?session.er.range:"0"
                params.isExclude=session.er.isExclude?session.er.isExclude:"false"
                params.exclude=true;
                datanew1=[:]
                def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
                datanew1.avoidable=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
                datanew1.urgent=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
                datanew1.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataPh4.add(datanew1)


                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                datanew1=[:]
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
                datanew1.avoidable=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
                datanew1.urgent=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
                datanew1.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataPh4.add(datanew1)

                datanew1=[:]
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
                dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"careAlertErVisit")
                datanew1.avoidable=dataGraph.getAt("noPreventiveVisit").getAt("erVisitper1000")
                datanew1.urgent=dataGraph.getAt("noRoutineVisit").getAt("erVisitper1000")
                datanew1.year=helperService.formatMonthYearDate(dynamicReportParam.reportingTo)
                dataPh4.add(datanew1);


                fields = [
                        "Year",
                        "Total Avoidable ER Visits/1000 where member not getting preventive services through a physician",
                        "Total Avoidable ER Visits/1000 where member not getting routine care through physician"
                ]
                helperService.csvWriter(fields,dataPh4,out);


                break;
        }



        out.flush();


//        exportService.export(params.format, response.outputStream, data, fields, labels, formatter, parameters)
//        return

    }









    def zSolution(){

    }
    def solutionOverview(){

    }

    def saveGoals(){
        params.user=springSecurityService.getCurrentUser().id;
        params.clientName=session.clientname
        params.page="telemedicine"
        def content=new ArrayList();
        def  map=[:]

        params.percentage.each{it->
            map.put(it.key,it.value);
        }
        def val=map as JSON
        params.content=val.toString()
        def saveGoal=SaveGoals.findByUserAndClientName(new BigInteger(params.user),session.clientname)

        if(!saveGoal){
            saveGoal=new SaveGoals(params)
        }else{
            saveGoal.properties=params
        }

        if(saveGoal.save(flush: true)){
            flash.success="Your target savings goals have been set!"
        }else{
            flash.error="An error has occurred and your goals were not saved. Please try again."
        }
        redirect(action: "telemedicine")

    }

    def costSharing(){
        [isEditable: false]
    }

    def communicateOptions(){
        [isEditable: false]
    }
    def expandUrgent(){
        [isEditable: false]
    }

    def telemedicine(){

//        1) In Efficient Use Of Er....
// http://localhost:8080/DasTest/zakiPoint?clientId=servletContext.getAt("clientIdentity")&reportingBasis=PaidDate
// &reportingTo=2015-06-30&reportingFrom=2014-07-01&report=avoidableToTotalErRatio
// &eligibilityType=[medical]&phiCSDate=01-01-2010&phiCEDate=06-30-2015&dataFetchFrom=true&ticket=null&clientName=z5dev
//        2)Condition Management  ==
//        http://localhost:8080/DasTest/zakiPoint?clientId=servletContext.getAt("clientIdentity")&reportingBasis=PaidDate&reportingTo=2015-06-30&reportingFrom=2014-07-01&report=topChronicConditionEr&eligibilityType=[medical]&phiCSDate=01-01-2010&phiCEDate=06-30-2015&dataFetchFrom=true&ticket=null&clientName=z5dev

        def datanew;

        params.reportingBasis=session.er.reportingBasis?session.er.reportingBasis:"PaidDate"
        params.range=session.er.range?session.er.range:"0"
        params.isExclude=session.er.isExclude?session.er.isExclude:"false"

        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def dataGraph = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"avoidableToTotalErRatio")
        datanew=dataGraph?.getAt((dataGraph.keySet() as String[])[0]);
        def avoidable=datanew?.get('totalPaidAmountForAvoidableEr');
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        dynamicReportParam = dashBoardChartService.setDynamicReportParams(params,session,[:])
        def  topChronicConditionEr = dashBoardChartService.getDashBoardChartScript(dynamicReportParam,"topChronicConditionErDetail")
        def topVal= topChronicConditionEr?.get('totalPaidAmountForAllCondition');

        def userExists=SaveGoals.createCriteria().list([max:1]) {
            eq("user",new BigInteger(springSecurityService.getCurrentUser().id))
            eq("clientName",session.clientname)
            eq("page","telemedicine")

        }[0]
        def content;

        if(userExists){
            content=JSON.parse(userExists.content);
        }

        def map=[:]
        def zSolutionList=[];
        map.data=avoidable
        map.title="Inefficient ER Use"
        map.code="erUse"
        map.percent=content?content.getAt("erUse").toString().toInteger():50
        zSolutionList.add(map);
        map=[:]
        map.data=topVal
        map.title="Condition Management"
        map.code="condMgmt"
        map.percent=content?content.getAt("condMgmt").toString().toInteger():50
        zSolutionList.add(map);
        [zSolutionList:zSolutionList,isEditable: false]

    }







}



