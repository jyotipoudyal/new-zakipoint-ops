package com.deerwalk.z5

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.hibernate.StatelessSession
import org.json.simple.JSONArray

import javax.transaction.Transaction

class BiometricsController {

    def dynamicParamsGeneratorService
    def reportFetchService
    def helperService
    def memberDrillService


    def beforeInterceptor = [action: this.&auth, except: 'login']


    private auth() {
        if (!session.isBiometrics) {
            flash.message="Data not available for ${controllerName.toString().capitalize()}."
            redirect(action: 'index',controller: "dashboard")
            return false
        }
    }

    def indexs(){

        println "hereeeeeeeeeeeeeeeeeeeeeeeeeeeee = hereeeeeeeeeeeeeeeeeeeeeeeeeeeee"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""

        params.reportingBasis="PaidDate"
        def datesArr=new ArrayList();
        //initialization of variables
        def dynamicReportParam,data1,data2,data3;
        //initialization complete
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params, session,[:])
            data1=reportFetchService.getDataRequest(dynamicReportParam,"biometricSnapshot")
            memberDrillService.getNewBiometricsDrillParams(data1,true,'member',params)



            def keySet=data1.keySet().sort();

            datesArr.push(reportingDates.priorTo.split("-")[0])



            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params, session,[:])
            data2=reportFetchService.getDataRequest(dynamicReportParam,"biometricSnapshot")
            memberDrillService.getNewBiometricsDrillParams(data2,true,'member',params)
//            memberDrillService.getBiometricsDrillParams(data2,true,'member',params)

            datesArr.push(reportingDates.pastTo.split("-")[0])



            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params, session,[:])
            data3=reportFetchService.getDataRequest(dynamicReportParam,"biometricSnapshot")
            memberDrillService.getNewBiometricsDrillParams(data3,true,'member',params)

            datesArr.push(reportingDates.currentTo.split("-")[0])



        def max=0;

        data1.each{k,v->
            v.levels.each{lev->
                 max=(v[lev]>max)?v[lev]:max;
            }
        }

        data2.each{k,v->
            v.levels.each{lev->
                max=(v[lev]>max)?v[lev]:max;
            }
        }

        data3.each{k,v->
            v.levels.each{lev->
                max=(v[lev]>max)?v[lev]:max;
            }
        }





        def getBioData=BiometricsData.findAll();

        def map=[:]

        for(BiometricsData bd:getBioData){
            def newMap=[:]
            newMap.name=bd.name
            newMap.desc=bd.description
            newMap.info=bd.info.tokenize("---")
            newMap.action=bd.action.tokenize("---")
            map[bd.uId]=newMap
        }

        render(view:"biometrics",model: [mapp1:data1 as JSON,mapp2:data2 as JSON,mapp3:data3 as JSON,keySet:keySet as JSON,datesArr:datesArr,maxVal:max,bioData:map as JSON])

    }


    def index(){
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""

        params.reportingBasis="PaidDate"
        def datesArr=new ArrayList();
        //initialization of variables
        def dynamicReportParam,data1,data2,data3;
        //initialization complete
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        params['task'] = "reportFromDomain"
        params['domainTask'] = "biometricSankeyBuilderListener"



        dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params, session,[:])
        def data=reportFetchService.getBiometricDataRequest(dynamicReportParam,"biometricSnapshot")

        def metaInfo=data.metaInfo;



        //fetch data from main data
        data1=data.prior;

        data1.each{k,v->
            v<<metaInfo[k]
        }

        memberDrillService.getNewBiometricsDrillParams(data1,'sediment',params,"prior")
        def keySet=data1.keySet().sort();

        datesArr.push(reportingDates.priorTo.split("-")[0])



        data2=data.last;

        data2.each{k,v->
            v<<metaInfo[k]
        }
        memberDrillService.getNewBiometricsDrillParams(data2,'sediment',params,"last")
        datesArr.push(reportingDates.pastTo.split("-")[0])




        data3=data.current

        data3.each{k,v->
            v<<metaInfo[k]
        }
//        memberDrillService.getNewBiometricsDrillParams(data3,true,'member',params)
        memberDrillService.getNewBiometricsDrillParams(data3,'sediment',params,"current")
        datesArr.push(reportingDates.currentTo.split("-")[0])



        def max=0;
        data1.each{k,v->
            v.levels.each{lev->
                max=(v[lev]>max)?v[lev]:max;
            }

            /*a1= data1[k].high
            b1= data2[k].low
            c1= data3[k].medium

             if(a1>maxVal)
                 maxVal=a1;
             if(b1>maxVal)
                 maxVal=b1;
             if(c1>maxVal)
                 maxVal=c1;*/
        }

        data2.each{k,v->
            v.levels.each{lev->
                max=(v[lev]>max)?v[lev]:max;
            }}

        data3.each{k,v->
            v.levels.each{lev->
                max=(v[lev]>max)?v[lev]:max;
            }}





        def getBioData=BiometricsData.findAll();

        def map=[:]

        for(BiometricsData bd:getBioData){
            def newMap=[:]
            newMap.name=bd.name
            newMap.desc=bd.description
            newMap.info=bd.info.tokenize("---")
            newMap.action=bd.action.tokenize("---")
            map[bd.uId]=newMap
        }

        render(view:"biometrics",model: [mapp1:data1 as JSON,mapp2:data2 as JSON,mapp3:data3 as JSON,keySet:keySet as JSON,datesArr:datesArr,maxVal:max,bioData:map as JSON])

    }



    def biometricsDetails(){
        render(view:"stackedGraph")
    }


    def renderBiometricSankey(){
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.range=params.range?params.range:"0"
        params.isExclude=params.isExclude?params.isExclude:"false"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        params.biometricName=params.metricCode

        params['task'] = "reportFromDomain"
        params['domainTask'] = "biometricSankeyBuilderListener"

        //initialization of variables
        def dynamicReportParam,data,columns;

        dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params, session, [:])
//        def dataGraph = reportFetchService.getDataRequest(dynamicReportParam, "biometricTrendingSankey")
        def dataGraph = reportFetchService.getBiometricDataRequest(dynamicReportParam, "biometricSankeyBuilderListener")
        data = dataGraph

        /*def levelList=dataGraph.levels.replace("[","").replace("]","").tokenize(",");
        def lev=new ArrayList();
        levelList.each{it->
            lev.push(it.trim());
        }*/

        def lev=dataGraph.levels;

        /*def newMap=[
                    "prior_low":0,"prior_medium":1,"prior_high":2,
                    "last_low":3,"last_medium":4,"last_high":5,
                    "current_low":6,"current_medium":7,"current_high":8
        ]*/

        def newMap=[:];


        def yearMap=["prior":reportingDates.priorTo.split("-")[0],"last":reportingDates.pastTo.split("-")[0],"current":reportingDates.currentTo.split("-")[0]]


        def dataErr=true;


        data.each { k, v ->
            if(k.equals("nodes")){
                v.each{

                            if (it['total'] != 0) {
                                dataErr = false;
                            } else {
                                it['total'] = 0.49;
                            }

                            newMap.put(it.node, it.id);
                            it.put("node", it.id);
                            it['sedimentLink']= dynamicParamsGeneratorService.setSankeyParams(params,it,"sediment");

                        }
            }
        }





        data.each { k, v ->
            if(k.equals("links")){
                v.each{
                    it['flowLink']= dynamicParamsGeneratorService.setSankeyParams(params,it,"flow");
                    it.each{key,val->
                        if(key=="source"){
                            it.put(key,newMap.getAt(val))
                        }else if(key=="target"){
                            it.put(key,newMap.getAt(val))
                        }


                        if(it['value']==0){
                            it['value']=0.00001;
                        }
                    }
                }
            }
        }




        def newData=[:]
        if(dataErr){
            newData=null;
            throw new Exception("No data received")
            return
        }

        data.nodes.sort{it.node}

        newData.data=data;
        newData.levels=lev.sort{it}
        newData.year=yearMap;

//        newData=[data:[nodes:[[id:6, total:10, node:0, name:low, year:prior], [id:7, total:10, node:1, name:medium, year:prior], [id:8, total:10, node:2, name:high, year:prior], [id:3, total:10, node:3, name:low, year:last], [id:4, total:10, node:4, name:medium, year:last], [id:5, total:10, node:5, name:high, year:last], [id:0, total:251, node:6, name:low, year:current], [id:1, total:144, node:7, name:medium, year:current], [id:2, total:269, node:8, name:high, year:current]], links:[[source:2, target:5, value:1], [source:2, target:3, value:0], [source:2, target:4, value:1], [source:0, target:5, value:0], [source:0, target:3, value:1], [source:0, target:4, value:0], [source:1, target:5, value:1], [source:1, target:3, value:0], [source:1, target:4, value:1], [source:5, target:8, value:0], [source:5, target:6, value:1], [source:5, target:7, value:0], [source:3, target:8, value:0], [source:3, target:6, value:0], [source:3, target:7, value:0], [source:4, target:8, value:0], [source:4, target:6, value:0], [source:4, target:7, value:0]]], year:[prior:2014, last:2015, current:2016]]
//        newData=[data:[nodes:[[id:6, total:1815, node:0, name:low, year:prior], [id:7, total:325, node:1, name:medium, year:prior], [id:8, total:95, node:2, name:high, year:prior], [id:3, total:2593, node:3, name:low, year:last], [id:4, total:327, node:4, name:medium, year:last], [id:5, total:106, node:5, name:high, year:last], [id:0, total:2626, node:6, name:low, year:current], [id:1, total:200, node:7, name:medium, year:current], [id:2, total:61, node:8, name:high, year:current]], links:[[source:2, target:5, value:22], [source:2, target:3, value:20], [source:2, target:4, value:13], [source:0, target:5, value:15], [source:0, target:3, value:481], [source:0, target:4, value:57], [source:1, target:5, value:19], [source:1, target:3, value:74], [source:1, target:4, value:61], [source:5, target:8, value:23], [source:5, target:6, value:13], [source:5, target:7, value:31], [source:3, target:8, value:7], [source:3, target:6, value:597], [source:3, target:7, value:51], [source:4, target:8, value:14], [source:4, target:6, value:82], [source:4, target:7, value:48]]], year:[prior:2014, last:2015, current:2016]]

        render newData as JSON
        return;
    }
    
    
    def loadBioData(){


        def bioD=[

            "BMI100": [
            "name":"BMI",
            "desc":"Body mass index",
            "info":[
                    "This measurement is a reliable indicator of your body fat and is based on your height and weight. A high BMI indicates an increased risk of developing a number of conditions associated with being overweight.",
                    "Normal range is : 18.5 to 24"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program  - incentives",
                    "Healthly heart program  - incentives"
            ]
        ],
            "NICOTINE": [
            "name":"NICOTINE",
            "desc":"NICOTINE",
            "info":[
                    "NA"
            ],
            "action": [
                    "NA"
            ]
        ]
            ,

            "WAIST": [
            "name":"Waist",
            "desc":"Waist in inches",
            "info":[
                    "This test measures excess girth around your waist. Results may indicate potential risk for diabetes, heart disease, stroke and some cancers.",
                    "Normal range is: 25 to 36"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program  - incentives",
                    "Healthly heart program  - incentives"
            ]
        ]
            ,

            "BP_SYSTOLIC": [
            "name":"BP Systolic (mmHg)",
            "desc":"Systolic blood pressure",
            "info":[
                    "This reading consists of two numbers: “systolic” represents the pressure when the heart beats, and “diastolic” signifies the pressure when the heart is at rest. These two distinct values can indicate an increased risk for heart attack, stroke and other possible health complications.",
                    "Normal range is: <120 mmHg"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Healthy Diet program  - incentives",
                    "Tobacoo Cessation Program",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ]
            ,
            "BP_DIASTOLIC": [
            "name":"BP Diastolic (mmHg)",
            "desc":"Diastolic blood pressure",
            "info":[
                    "This reading consists of two numbers: “systolic” represents the pressure when the heart beats, and “diastolic” signifies the pressure when the heart is at rest. These two distinct values can indicate an increased risk for heart attack, stroke and other possible health complications.",
                    "Normal range is: <80 mmHg"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Healthy Diet program  - incentives",
                    "Tobacoo Cessation Program",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ]
            ,
            "TOTAL_CHOLESTEROL": [
            "name":"Total Cholesterol (mg/dL)",
            "desc":"Total Cholesterol",
            "info":[
                    "Cholesterol number includes readings for both HDL (“good” cholesterol) and LDL (“bad” cholesterol), which measures certain types of fats in your blood.",
                    "Normal range is: <200 mmHg"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program - incentives",
                    "Healthly heart program"
            ]
        ]
            ,
            "CHOLESTEROL_RATIO": [
            "name":"Cholesterol Ratio",
            "desc":"Total Cholesterol",
            "info":[
                    "The Total Cholesterol/HDL ratio can indicate potential risks for cardiovascular disease.",
                    "Normal range is: <3.4"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program  - incentives",
                    "Healthly heart program"
            ]
        ]
            ,
            "HDL_CHOLESTEROL": [
            "name":"HDL Cholesterol (mg/dL)",
            "desc":"Good Cholesterol",
            "info":[
                    "Good Cholesterol",
                    "Normal range is: >60 mg/dL"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Healthy Diet program  - incentives"
            ]
        ]
            ,
            "LDL_CHOLESTEROL": [
            "name":"LDL Cholesterol (mg/dL)",
            "desc":"Bad Cholesterol",
            "info":[
                    "Bad Cholesterol",
                    "Normal range is: <100 mg/dL"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program  - incentives",
                    "Tobacoo Cessation Program",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ]
            ,
            "TRIGLYCERIDE": [
            "name":"Triglycerides (mg/dL)",
            "desc":"The major form of fat stored by the body",
            "info":[
                    "An ester formed from glycerol and three fatty acid groups. Triglycerides are the main constituents of natural fats and oils, and high concentrations in the blood indicate an elevated risk of stroke.",
                    "Normal range is: <150 mg/dL"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program  - incentives",
                    "Get active program - incentives",
                    "Tobacoo Cessation Program",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ]
            ,
            "FASTING_BLOOD_SUGAR": [
            "name":"Fasting Blood Sugar (mg/dL)",
            "desc":"Glucose content in blood",
            "info":[
                    "Your glucose reading measures the amount of sugar in your blood, indicating whether you have or may be at risk for developing diabetes.",
                    "Normal range is: <100 mg/dL"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program  - incentives",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ]
            ,
            "NON_FASTING_BLOOD_SUGAR": [
            "name":"Nonfasting Blood Sugar",
            "desc":"Glucose content in blood",
            "info":[
                    "Your glucose reading measures the amount of sugar in your blood, indicating whether you have or may be at risk for developing diabetes.",
                    "Normal range is: <140"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program  - incentives",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ]
            ,
            "A1C": [
            "name":"A1C (%)",
            "desc":"Level of Hemoglobin",
            "info":[
                    "Level of hemoglobin A1c in the blood as a means of determining the average blood sugar concentrations",
                    "Normal range is: 4 to 5.6%"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Healthy Diet program  - incentives",
                    "Alochol Awareness Program"
            ]
        ]
            ,
            "PSA": [
            "name":"PSA (mg/dL)",
            "desc":"Prostate Specific Antigen",
            "info":[
                    "Prostate Specific Antigen - protein produced by the prostate gland. Elevated level means potential risk.",
                    "Normal range is: <4.0 mg/dL"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Healthy Diet program  - incentives"
            ]
        ]
            ,
            "WELLNESS_SCORE": [
            "name":"Wellness Score",
            "desc":"Overall Wellness Score",
            "info":[
                    "Proprietary logic to calculate overall wellness using all available data for that patient. Higher means good health.",
                    "Normal range is: >75"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program - incentives",
                    "Get active program - incentives",
                    "Tobacoo Cessation Program",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ]
            ,
            "BIOMETRIC_SCORE": [
            "name":"Biometric Score",
            "desc":"Overall Biometric Score",
            "info":[
                    "Proprietary logic to calculate overall biometric scorer using all available biometric readings for that patient. Higher means good health.",
                    "Normal range is: >75"
            ],
            "action": [
                    "Phone Coaching",
                    "Online Coaching",
                    "Weight loss program - incentives",
                    "Healthy Diet program - incentives",
                    "Get active program - incentives",
                    "Tobacoo Cessation Program",
                    "Alochol Awareness Program",
                    "Healthly heart program"
            ]
        ],
            "BODY_FAT": [
            "name":"Body Fat (%)",
            "desc":"Overall Body Fat",
            "info":[
                    "NA",
                    "Normal range is: 0-18"
            ],
            "action": [
                    "NA"
            ]
        ],"WEIGHT": [
            "name":"Weight",
            "desc":"Weight",
            "info":[
                    "NA",
                    "Normal range is: 5 to 180"
            ],
            "action": [
                    "NA"
            ]
        ]
        ]

        BiometricsData.executeUpdate("delete BiometricsData c where 1=1")

        if(BiometricsData.findAll().size()==0){
            bioD.each {k,v->
                BiometricsData bd=new BiometricsData();
                bd.uId=k;
                bd.name=v.name;
                bd.description=v.desc;
                bd.info=v.info.join("---");
                bd.action=v.action.join("---");
                println "bd = "+bd.action
                bd.save()
            }
        }





        }
    

}
