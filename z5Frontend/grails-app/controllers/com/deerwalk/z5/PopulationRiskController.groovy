package com.deerwalk.z5

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class PopulationRiskController {

    def populationRiskService
    def helperService
    def fetchService
    def reportFactoryService
    def memberDrillService
    def exportService
    def dashBoardChartService
    def searchService

    def dynamicParamsGeneratorService
    def reportFetchService

    def riskChart={
        def dashBoardParams = session.dashBoardParams?:[:]
        dashBoardParams.reportingBasis="PaidDate"
        dashBoardParams.csDate=session.csDate
        dashBoardParams.ceDate=session.ceDate

        def reportingDates=helperService.getReportingDates(dashBoardParams.ceDate)
        dashBoardParams.reportingFrom=reportingDates.priorFrom
        dashBoardParams.reportingTo=reportingDates.currentTo
        dashBoardParams.range="0"
        dashBoardParams.isExclude="false"
        dashBoardParams.includes="[truvenBenchmark]"
        dashBoardParams.benchmarkKey="chronicConditionUtilization"

        def dynamicReportParams = dashBoardChartService.setDynamicReportParams(dashBoardParams,session,[:])

        def data;
        def newDataList=[:];
        try{
                dashBoardParams.reportingFrom=reportingDates.currentFrom;
                dashBoardParams.reportingTo=reportingDates.currentTo;
                dynamicReportParams=dashBoardChartService.setDynamicReportParams(dashBoardParams,session,[:]);
                data = dashBoardChartService.getPopChartScript(dynamicReportParams,"populationRiskByTopChronicCondition");


            def newData=[:];

            JSONObject json=new JSONObject();
            if(params.modifiable){
                def modifiable=["1002","1005","1007","1008","1009","1011","1012","1016","1017","1021","1022","1023","metaInfo"]
                JSONObject map=new JSONObject();
                modifiable.each {
                    map[it]=data.reporting[it];
                }
                json['reporting']=map
                json['truvenBenchmark']=data['truvenBenchmark']
                data= json
            }

            data.reporting.each { id, datas ->
                if(id!="metaInfo")
                memberDrillService.getRisksDrillParams(datas,true,'member',dashBoardParams,"riskChart")
            }



//            datanew1['members_drill']=memberDrillService.getDiabetesCostsDrillParams(dataPh3,true,'member',params)
        }catch(e){
            log.error(e.getMessage())
            data=[status: 204];
        }

        [data:data,modifiable:params.modifiable]

    }


    def index() {
        session.zPop=[:]
        params.chronicCode=params.id?params.id:"1012"
        /*def topLineGraph=new ArrayList();
        def useOfErByChronicMembers=new ArrayList();
        def datanew1=[:];
        session.zPop=[:]
        params.trend="yearly"
//        params.metric="Diabetes"
//        params.chronicCode="1012"
        params.chronicCode=params.chronicCondition?params.chronicCondition:"1012"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.reportingFrom=reportingDates.priorFrom
        params.reportingTo=reportingDates.priorTo

        params.reportingBasis="PaidDate"
        params.mbrRegionId=""
        params.clientId=3000
        params.range="0"
        session.zPop.range=params.range
        def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        def dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
        def benchMarkValue=dataGraph.getAt('truvenBenchmark').getAt("Diabetes_pmpy")
        def neValue=benchMarkValue.toString().replace("\$","")
        neValue=neValue.toString().replace(",","")
        neValue=neValue.toDouble()
        datanew1=[:];
        def dataPh3=dataGraph.getAt('reporting')
        datanew1.benchmark=neValue
        datanew1.pmpy=dataPh3.getAt('chronicInfo').getAt("pmpy")
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        topLineGraph.add(datanew1)
        datanew1=[:];
        dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
//        dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"useOfErByChronicMembers")
        dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
        dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
//        useOfErByChronicMembers.add(dataGraph.erVisitInfoByChronicMembers)
        useOfErByChronicMembers.add(dataGraph.chronicConditions.total)


//        params.reportingFrom=reportingDates.pastFrom
//        params.reportingTo=reportingDates.pastTo

        params.reportingFrom=reportingDates.pastFrom
        params.reportingTo=reportingDates.pastTo
        dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")

        datanew1=[:];
        dataPh3=dataGraph.getAt('reporting')
        datanew1.benchmark=neValue
        datanew1.pmpy=dataPh3.getAt('chronicInfo').getAt("pmpy")
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]

        topLineGraph.add(datanew1)
        datanew1=[:];
        dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
        dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
        useOfErByChronicMembers.add(dataGraph.chronicConditions.total)


        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo

        dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
        datanew1=[:];
        dataPh3=dataGraph.getAt('reporting')
        datanew1.benchmark=neValue
        datanew1.pmpy=dataPh3.getAt('chronicInfo').getAt("pmpy")
        datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
        topLineGraph.add(datanew1)
        datanew1=[:];
        dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
        dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
        useOfErByChronicMembers.add(dataGraph.chronicConditions.total)

        datanew1=[:];
        def lineGraphR=[];
        params.reportingFrom=params.reportingTo
        params.reportingTo=helperService.addYear(params.reportingFrom)
        datanew1.put('year',params.reportingFrom)
        datanew1.put('actual',1000)
        lineGraphR.add(datanew1)
        datanew1=[:];
        datanew1.put('year',params.reportingTo)
        datanew1.put('actual',9900)
        lineGraphR.add(datanew1)
        def getMax=getMaximum(topLineGraph,lineGraphR)
        def yPercentage=0
        def yTotal=1

        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"diabetesAlongOtherChronic")

        def barGraph=dataGraph
       // http://localhost:8080/DasTest/zakiPoint?clientId=3000&reportingBasis=PaidDate&reportingTo=2015-06-30
        // &reportingFrom=2014-07-01&eligibilityType=[medical]&report=recommendedCareForDiabetes&phiCSDate=01-01-2010&phiCEDate=06-30-2015
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
        dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"recommendedCareForDiabetes")
        def carePercent=dataGraph.getAt('metaInfo').getAt("percentOfNonCompliant")
        dataGraph.remove("metaInfo")
        def List<Object> careFor = new ArrayList<Object>();
        dataGraph.each { id, datas ->
            datas['members_drill']=memberDrillService.getDrillParams(datas,true,'member',params)
            careFor.add(datas)
        }
        careFor.sort{it.notMeeting}.reverse()

        useOfErByChronicMembers.sort{it.year}.reverse()
*/
        def chronicLists=[
                '1012': 'Diabetes',
                '1008': 'Congestive Heart Failure',
                '1005': 'CAD',
                '1002': 'Asthma',
                '1009': 'COPD',
                '1001': 'Affective Psychosis',
                '1003': 'Atrial Fibrillation',
                '1004': 'Blood Disorders',
                '1006': 'Cancer',
                '1007': 'Chronic Pain',
                '1010': 'Demyelinating Diseases',
                '1011': 'Depression',
                '1014': 'CKD',
                '1013': 'Eating Disorders',
                '1015': 'HIV/AIDS',
                '1016': 'Hyperlipidemia',
                '1017': 'Hypertension',
                '1018': 'Immune Disorders',
                '1019': 'Inflammatory Bowel Disease',
                '1020': 'Liver Diseases',
                '1021': 'Morbid Obesity',
                '1022': 'Osteoarthritis',
                '1023': 'Peripheral Vascular Disease',
                '1024': 'Rheumatoid Arthritis'
        ]



        /*def drillParams = fetchService.getProviderParams(params, 'provider')
//        drillParams.put('comp.eligibleMonths','Between')
//        drillParams.put('eligibleMonths',eligibleDateFrom)
//        drillParams.put('eligibleMonths-to',eligibleDateTo)
        drillParams.put('comp.paidDate','Between')
        drillParams.put('paidDate',params.reportingFrom)
        drillParams.put('paidDate-to',params.reportingTo)
        drillParams.put('comp.serviceDate','Between')
        drillParams.put('serviceDate',params.reportingFrom)
        drillParams.put('serviceDate-to',params.reportingTo)
        drillParams.put('comp.totalAmount','Not Equal')
        drillParams.put('totalAmount','0')
        drillParams.put('erVisit','0')
        drillParams.put('comp.isPaidStart','Not Equal')
        drillParams.put('isPaidStart','1')*/


//        [defaultSort:"sortable sorted desc",chronicCode:params.chronicCode,drillParams:drillParams,chronicLists:chronicLists.sort{it.value},getMax:getMax,isEditable:false,dataPh3:topLineGraph as JSON,lineGraphR:lineGraphR as JSON,yTotal:yTotal,yPercentage:yPercentage,barGraph:barGraph as JSON,careFor:careFor,carePercent:carePercent,useOfErByChronicMembers:useOfErByChronicMembers]
        [chronicCode:params.chronicCode,chronicLists:chronicLists.sort{it.value}]

    }

    def getScriptForPopulationRisks(){
        params.fromDate=params.fromDate?params.fromDate:"current"
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.range=params.range?params.range:"0"
        params.isExclude=params.isExclude?params.isExclude:"false"


        session.zPop.fromDate= params.fromDate
        session.zPop.reportingBasis= params.reportingBasis
        session.zPop.range= params.range
        session.zPop.isExclude= params.isExclude


        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
//        params.metric="Diabetes"
        params.chronicCode=params.chronicCondition?params.chronicCondition:"1012"


        if(params.module=="lineGraph1"){
            def topLineGraph=new ArrayList();
            def datanew1=[:];
            params.trend="yearly"
            params.chronicTitle=params.chronicTitle?params.chronicTitle:"Diabetes";
            params.truvenBenchmark=(params.chronicTitle=='CKD')?"ESRD_pmpy":params.chronicTitle+"_pmpy"
            params.csDate=session.csDate
            params.ceDate=session.ceDate
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            def dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
            def benchMarkValue=dataGraph.getAt('truvenBenchmark').getAt(params.truvenBenchmark)

            def neValue=benchMarkValue.toString().replace("\$","")
            neValue=neValue.toString().replace(",","")
            neValue=neValue.toDouble()
            datanew1=[:];
            def dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=neValue
            datanew1.pmpy=dataPh3.getAt('chronicInfo').getAt("pmpy")
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            datanew1['members_drill']=memberDrillService.getDiabetesCostsDrillParams(dataPh3,true,'member',params)
            topLineGraph.add(datanew1)



//        params.reportingFrom=reportingDates.pastFrom
//        params.reportingTo=reportingDates.pastTo

            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
            datanew1=[:];
            dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=neValue
            datanew1.pmpy=dataPh3.getAt('chronicInfo').getAt("pmpy")
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            datanew1['members_drill']=memberDrillService.getDiabetesCostsDrillParams(dataPh3,true,'member',params)

            topLineGraph.add(datanew1)


            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo

            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
            datanew1=[:];
            dataPh3=dataGraph.getAt('reporting')
            datanew1.benchmark=neValue
            datanew1.pmpy=dataPh3.getAt('chronicInfo').getAt("pmpy")
            datanew1.reportingDates=["reportingFrom":dynamicReportParam.reportingFrom,"reportingTo":dynamicReportParam.reportingTo]
            datanew1['members_drill']=memberDrillService.getDiabetesCostsDrillParams(dataPh3,true,'member',params)
            topLineGraph.add(datanew1)

            datanew1=[:];
            def lineGraphR=[];
            params.reportingFrom=params.reportingTo
            params.reportingTo=helperService.addYear(params.reportingFrom)
            datanew1.put('year',params.reportingFrom)
            datanew1.put('actual',dataPh3.getAt('chronicInfo').getAt("pmpy"))
            datanew1.put('concurrentActual',dataPh3.getAt('chronicInfo').getAt("concurrentPmpy"))
            lineGraphR.add(datanew1)
            datanew1=[:];
            datanew1.put('year',params.reportingTo)
//            datanew1.put('actual',dataPh3.getAt('chronicInfo').getAt("projectCostPmpy"))
            datanew1.put('actual',dataPh3.getAt('chronicInfo').getAt("actualProspectiveCostPmpy"))
            datanew1.put('concurrentActual',dataPh3.getAt('chronicInfo').getAt("prospectivePmpy"))
            lineGraphR.add(datanew1)
            def getMax=getMaximum(topLineGraph,lineGraphR)
            def yPercentage=0
            def yTotal=dataPh3.getAt('chronicInfo').getAt("projectCostTobeChangeByPercent")
            def map=[:]
            map.topLineGraph=topLineGraph
            map.max=getMax

            map.dm=lineGraphR
            map.yPercentage=yPercentage
            map.yTotal=yTotal
            map.max=getMax
            map.populationRiskContribution=dataPh3.getAt('chronicInfo').getAt("populationRiskContribution")

            render map as JSON

        }else if(params.module=="barGraph1"){

            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            def dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"diabetesAlongOtherChronic")
            dataGraph?.chronicMergedWithOtherConditions.each{id, datas ->
                datas['members_drill']=memberDrillService.getDiabetesAlongOtherChronicDrillParams(datas,true,'member',params)
            }
            render dataGraph as JSON

        }else if(params.module=="barGraph2"){

            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            def dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"recommendedCareForDiabetes")
            def carePercent=dataGraph.getAt('metaInfo').getAt("percentOfNonCompliant")
            dataGraph.remove("metaInfo")
            def dataRealTime = populationRiskService.getPopulationRiskScript(dynamicReportParam,"gapInCareRealtime")



            def getCat=dataGraph.getAt((dataGraph.keySet() as String[])[0]);


            dataRealTime.each{k,v->
                if(v.category==getCat['category']){
                    if(!v.conditionWiseMemberMonth) v.conditionWiseMemberMonth=null
                    if(!v.costPmpy) v.costPmpy=null
                    dataGraph[k]=v;
                }
            }



            def List<Object> careFor = new ArrayList<Object>();
            dataGraph.each { id, datas ->
                if(dataRealTime[id]){
                    datas['members_drill']=memberDrillService.getRealTimeDrillParamsForQm(datas,true,'member',params)
                }else{
                    datas['members_drill']=memberDrillService.getDrillParams(datas,true,'member',params)
                }
                careFor.add(datas)
            }

            careFor=careFor.sort{it.percentMet}.reverse()
            render(template: "tableDataDetails",model: [careFor:careFor,chronicCode:params.chronicCode,carePercent:carePercent,defaultSort:"sortable sorted desc"])

        }else if(params.module=="barGraph3"){
            def  useOfErByChronicMembers=new ArrayList();
            def nume,deno;
            double percent;

            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            def datanew1=[:];
            def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            def dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
            dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
            memberDrillService.getChronicMemberUseDrillParams(dataGraph.chronicConditions,true,'member',params)

            useOfErByChronicMembers.add(dataGraph.chronicConditions.total)
            deno=dataGraph?.chronicConditions?.total?.erCost

            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            datanew1=[:];
            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
            memberDrillService.getChronicMemberUseDrillParams(dataGraph.chronicConditions,true,'member',params)
            dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
            useOfErByChronicMembers.add(dataGraph.chronicConditions.total)

            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            datanew1=[:];
            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
            dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
            memberDrillService.getChronicMemberUseDrillParams(dataGraph.chronicConditions,true,'member',params)

            useOfErByChronicMembers.add(dataGraph.chronicConditions.total)
            nume=dataGraph?.chronicConditions?.total?.erCost
            try{
                percent=((nume-deno)/deno)*100;
            }catch (Exception e ){
                percent=0;
            }

            render(template: "tableDataDetails2",model: [percent:percent,chronicCode:params.chronicCode,useOfErByChronicMembers:useOfErByChronicMembers,defaultSort:"sortable sorted desc"])

        }
    }

    def sortResult(){
        params.page=1;
        params.sortResults=true
        params.trend="yearly"
//        params.chronicCode="1012"
        params.chronicCode=params.chronicCode?params.chronicCode:"1012"

        params.reportingBasis=session.zPop.reportingBasis?session.zPop.reportingBasis:"PaidDate"
        params.range=session.zPop.range?session.zPop.range:"0"
        params.isExclude=session.zPop.isExclude?session.zPop.isExclude:"false"


        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        params.mbrRegionId=""
//        params.clientId=servletContext.getAt("clientIdentity")
        def dynamicReportParam ,dataGraph,carePercent;
        if(params.module=="recommendedCareForDiabetes"){
            session.zPop.table1Order=params.order
            session.zPop.table1Sort=params.sort
            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"recommendedCareForDiabetes")
            carePercent=dataGraph.getAt('metaInfo').getAt("percentOfNonCompliant")
            dataGraph.remove("metaInfo")
            def List<Object> careFor = new ArrayList<Object>();
            dataGraph.each { id, datas ->
                datas['members_drill']=memberDrillService.getDrillParams(datas,true,'member',params)
                careFor.add(datas)
            }

            if(params.order=="asc"){
                careFor=careFor.sort{it[params.sort]}
            }else{
                careFor=careFor.sort{it[params.sort]}.reverse()
            }

            render(template: "tableDataDetails",model:[careFor:careFor,chronicCode:params.chronicCode,carePercent:carePercent])
        }else if(params.module=="useOfErByChronicMembers"){
            def nume,deno,percent;
            session.zPop.table2Order=params.order
            session.zPop.table2Sort=params.sort
            def  useOfErByChronicMembers=new ArrayList();

            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.priorTo
            def datanew1=[:];
            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
            dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
            memberDrillService.getChronicMemberUseDrillParams(dataGraph.chronicConditions,true,'member',params)

            useOfErByChronicMembers.add(dataGraph.chronicConditions.total)
            deno=dataGraph?.chronicConditions?.total?.erCost

            params.reportingFrom=reportingDates.pastFrom
            params.reportingTo=reportingDates.pastTo
            datanew1=[:];
            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
            memberDrillService.getChronicMemberUseDrillParams(dataGraph.chronicConditions,true,'member',params)

            dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
            useOfErByChronicMembers.add(dataGraph.chronicConditions.total)

            params.reportingFrom=reportingDates.currentFrom
            params.reportingTo=reportingDates.currentTo
            datanew1=[:];
            dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
            dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
            dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
            memberDrillService.getChronicMemberUseDrillParams(dataGraph.chronicConditions,true,'member',params)

            useOfErByChronicMembers.add(dataGraph.chronicConditions.total)
            nume=dataGraph?.chronicConditions?.total?.erCost
            try{
                percent=((nume-deno)/deno)*100;
            }catch (Exception e ){
                percent=0;
            }

            if(params.order=="asc"){
                useOfErByChronicMembers=useOfErByChronicMembers.sort{it[params.sort]}
            }else{
                useOfErByChronicMembers=useOfErByChronicMembers.sort{it[params.sort]}.reverse()

            }

            render(template: "tableDataDetails2",model: [percent:percent,useOfErByChronicMembers:useOfErByChronicMembers,chronicCode:params.chronicCode])

        }





    }


    def getMaximum(a,b){
        def aMax=0;
        a.each{
            def val2=it.pmpy>it.benchmark.toDouble()?it.pmpy:it.benchmark.toDouble()
            if(val2>aMax)
                aMax=val2

        }
        def bMax=0;
        b.each{
            def val2=it.actual>it.concurrentActual?it.actual:it.concurrentActual
            if(val2>bMax)
                bMax=val2
        }
        def val= aMax>bMax?aMax:bMax
        return val *1.1
    }

    def exportCsv(){
        def data;
        Map labels,formatter,parameters;
        List fields;
        params.reportingBasis=params.exportReportingBasis;
        params.range=params.exportRange
        params.isExclude=params.exportIsExclude
        params.chronicCode=params.exportChronicCondition?params.exportChronicCondition:"1012"
        params.chronicTitle=params.exportChronicTitle?params.exportChronicTitle:"1012"

        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
//        params.clientId=servletContext.getAt("clientIdentity")
        switch(params.exportModule) {
            case ("diabetesCost"):
                def topLineGraph=new ArrayList();
                params.trend = "yearly"
                params.chronicTitle=params.chronicTitle?params.chronicTitle:"Diabetes";
                params.truvenBenchmark=(params.chronicTitle=='CKD')?"ESRD_pmpy":params.chronicTitle+"_pmpy"
                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                def dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
                def benchMarkValue=dataGraph.getAt('truvenBenchmark').getAt(params.truvenBenchmark)

                def neValue=benchMarkValue.toString().replace("\$","")
                neValue=neValue.toString().replace(",","")
                neValue=neValue.toDouble()
                def datanew1=[:];
                def dataPh3=dataGraph.getAt('reporting')
                datanew1.benchmark=formatNumber(number: neValue,maxFractionDigits: 2)
                datanew1.pmpy=formatNumber(number: dataPh3.getAt('chronicInfo').getAt("pmpy"),maxFractionDigits: 2)
                datanew1.reportingDates=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                topLineGraph.add(datanew1)

                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
                datanew1=[:];
                dataPh3=dataGraph.getAt('reporting')
                datanew1.benchmark=formatNumber(number: neValue,maxFractionDigits: 2)
                datanew1.pmpy=formatNumber(number: dataPh3.getAt('chronicInfo').getAt("pmpy"),maxFractionDigits: 2)
                datanew1.reportingDates=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                topLineGraph.add(datanew1)


                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo

                dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                dataGraph = populationRiskService.getPopulationRiskScriptWBenchmark(dynamicReportParam,"diabetesCost")
                datanew1=[:];
                dataPh3=dataGraph.getAt('reporting')
                datanew1.benchmark=formatNumber(number: neValue,maxFractionDigits: 2)
                datanew1.pmpy=formatNumber(number: dataPh3.getAt('chronicInfo').getAt("pmpy"),maxFractionDigits: 2)
                datanew1.reportingDates=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                topLineGraph.add(datanew1)


                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=DiabetesCost.csv")

                fields = [
                        "reportingDates",
                        "pmpy",
                        "benchmark"
//                        "memberMonths"
                ]

                labels = ["reportingDates"       : "Time",
                          "pmpy": "PMPY Actual",
                          "benchmark": "Benchmark Value PMPY"
                ]

                parameters = ["column.widths": [0.1, 0.40, 0.40, 0.40]
                ]
                def fDate = { domain, value ->
                    if (value)
                        return value.format("yyyy")
                }
                formatter = [reportingDates: fDate]

                data = topLineGraph;

                break;
            case('diabetesAlongOtherChronic'):
                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                def dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"diabetesAlongOtherChronic")
                List<HashMap> barData = new ArrayList<HashMap>();
                dataGraph?.chronicMergedWithOtherConditions.each {key,value->
                    println "it = "+value
                    println "it = "+value.percentOfPopulation
                    Map dgs=[:]
                    def appendAnd="";
                    dgs.time=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                    dgs.percent = formatNumber(number: value['percentOfPopulation'],maxFractionDigits: 1)
                    dgs.population = value['population'];
                    dgs.oName = value['otherConditionCode'];

                    if (value['otherConditionDescription'] == value['rootCondition']) {
                       dgs.name = "All "+value['otherConditionDescription'];
                    }
                    else {
                        appendAnd = value['otherConditionDescription'] == "Only" ? " " : " & "
                        dgs.name = value['rootCondition'] + "" + appendAnd + "" + value['otherConditionDescription'];
                    }

                    barData.add(dgs);

                }

                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=DiabetesCostWithOther.csv")

                fields = [
                        "time",
                        "name",
                        "percent",
                        "population"

//                        "memberMonths"
                ]

                labels = ["time"       : "Time",
                          "name"       : "Name",
                          "percent": "percent",
                          "population": "population"
                ]

                parameters = ["column.widths": [0.1, 0.40, 0.40, 0.40]
                ]
                def fDate = { domain, value ->
                    if (value)
                        return value.format("yyyy")
                }
                formatter = [time: fDate]



                barData=barData.sort{it.percent}.reverse();
                data = barData;
                break;
            case('recommendedCareForDiabetes'):

                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                def dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"recommendedCareForDiabetes")
                def carePercent=dataGraph.getAt('metaInfo').getAt("percentOfNonCompliant")
                dataGraph.remove("metaInfo")
                def List<Object> careFor = new ArrayList<Object>();
                dataGraph.each { id, datas ->
                    datas.time=Date.parse('yyyy-MM-dd',dynamicReportParam.reportingTo)
                    careFor.add(datas)
                }

                careFor=careFor.sort{it.percentMet}.reverse()

                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=RecommendedCareForDiabetes.csv")

                fields = [
                        "time",
                        "description",
                        "membersInGroup",
                        "percentMet",
                        "met",
                        "notMeeting",
                        "costPmpy"

//                        "memberMonths"
                ]

                labels = ["time"       : "Time",
                          "description"       : "Description",
                          "membersInGroup"       : "Members In Group (#)",
                          "percentMet": "(%) Compliance",
                          "met": "Met (#)",
                          "notMeeting": "Not Met (#)",
                          "costPmpy": "\$ Pmpy"
                ]

                parameters = ["column.widths": [0.1, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40]
                ]
                def fDate = { domain, value ->
                    if (value)
                        return value.format("yyyy")
                }
                //formatter = [time: fDate]

                def fNumber = { domain, value ->
                    if (value)
                        return  formatNumber(number: value,maxFractionDigits: 2)
                }
                formatter = [time: fDate,costPmpy:fNumber,percentMet:fNumber]


                data = careFor;

                break;

            case('newUseOfErByChronicMembers'):

                params.reportingFrom=reportingDates.priorFrom
                params.reportingTo=reportingDates.priorTo
                def datanew1=[:];
                def  useOfErByChronicMembers=new ArrayList();


                def dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                def dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
                dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
                useOfErByChronicMembers.add(dataGraph.chronicConditions.total)

                params.reportingFrom=reportingDates.pastFrom
                params.reportingTo=reportingDates.pastTo
                datanew1=[:];
                dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
                dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
                useOfErByChronicMembers.add(dataGraph.chronicConditions.total)

                params.reportingFrom=reportingDates.currentFrom
                params.reportingTo=reportingDates.currentTo
                datanew1=[:];
                dynamicReportParam = populationRiskService.setDynamicReportParams(params,session,[:])
                dataGraph = populationRiskService.getPopulationRiskScript(dynamicReportParam,"newUseOfErByChronicMembers")
                dataGraph.chronicConditions.total.year=Date.parse("yyyy-MM-dd", params.reportingTo)
                useOfErByChronicMembers.add(dataGraph.chronicConditions.total)

                println "useOfErByChronicMembers = "
                println "useOfErByChronicMembers = "
                println "useOfErByChronicMembers = $useOfErByChronicMembers"

                params.format = "csv"
                response.contentType = grailsApplication.config.grails.mime.types["csv"]
                response.setHeader("Content-disposition", "attachment; filename=UseOfErByChronicMembers.csv")

                fields = [
                        "year",
                        "member",
                        "erVisitor",
                        "erCost",
                        "percentOfnonErVisitor",
                        "percentOfoneTimeVisitor",
                        "percentOftwoOrMoreTimesVisitor"
                ]

                labels = ["year"       : "Time",
                          "member"       : "Chronic Condition Member Count",
                          "erVisitor"       : "Er Visited Chronic Condition Member Count",
                          "erCost": "ER Cost",
                          "percentOfnonErVisitor": "No Visit",
                          "percentOfoneTimeVisitor": "One Visit",
                          "percentOftwoOrMoreTimesVisitor": "Two Or More Visits"
                ]

                parameters = ["column.widths": [0.1, 0.40, 0.40, 0.40, 0.40, 0.40]
                ]
                def fDate = { domain, value ->
                    if (value)
                        return value.format("yyyy")
                }
                //formatter = [time: fDate]
                def fNumber = { domain, value ->
                    if (value)
                        return  formatNumber(number: value,maxFractionDigits: 2)
                }
                formatter = [year: fDate,percentOfnonErVisitor:fNumber,percentOfoneTimeVisitor:fNumber,percentOftwoOrMoreTimesVisitor:fNumber,erCost:fNumber]
                data = useOfErByChronicMembers;
                break;
        }

        exportService.export(params.format, response.outputStream, data, fields, labels, formatter, parameters)

    }


    def riskDetails(){
        render(view:"popRiskDash",model: [id:params.id])
    }


    def renderRiskSankey(){
        params.reportingBasis=params.reportingBasis?params.reportingBasis:"PaidDate"
        params.range=params.range?params.range:"0"
        params.isExclude=params.isExclude?params.isExclude:"false"
        params.csDate=session.csDate
        params.ceDate=session.ceDate
        def reportingDates=helperService.getReportingDates(params.ceDate)
        params.mbrRegionId=""
        params.reportingFrom=reportingDates.currentFrom
        params.reportingTo=reportingDates.currentTo
        params['domainTask'] = "populationRiskSankeyBuilderListener"
        params['task'] = "reportFromDomain"
        params.category= (params.type=="1")?"MARA_RISK_LEVEL":"HS_RISK_LEVEL"


        //initialization of variables
        def dynamicReportParam,data,columns;

        dynamicReportParam = dynamicParamsGeneratorService.setHighOutNetworkReportParams(params, session, [:])
        def dataGraph = reportFetchService.getDataRequest(dynamicReportParam, "populationRiskSankeyBuilderListener")
        data = dataGraph;

//        def levelList=dataGraph.levels.replace("[","").replace("]","").tokenize(",");
        def levelList=["High","Medium","Normal","Low"]
        def lev=new ArrayList();
        levelList.each{it->
            lev.push(it.trim());
        }

        def newMap=[:];


        def yearMap=["last":reportingDates.pastTo.split("-")[0],"current":reportingDates.currentTo.split("-")[0]]


        def dataErr=true;


        data.each { k, v ->
            if(k.equals("nodes")){
                v.each{
                    if (it['total'] != 0) {
                        dataErr = false;
                    } else {
                        it['total'] = 0.49;
                    }

                    newMap.put(it.node, it.id);
                    it.put("node", it.id);

                    it['sedimentLink']= dynamicParamsGeneratorService.setSankeyParams(params,it,"sediment");
                }
            }

            if(k.equals("links")){
                v.each{
                    it['flowLink']= dynamicParamsGeneratorService.setSankeyParams(params,it,"flow");
                    it.each{key,val->
                        if(key=="source"){
                            it.put(key,newMap.getAt(val))
                        }else if(key=="target"){
                            it.put(key,newMap.getAt(val))
                        }


                        if(it['value']==0){
                            it['value']=0.001;
                        }


                    }
                }
            }
        }








        def newData=[:]
        if(dataErr){
            newData=null;
            throw new Exception("No data received")
            return
        }

        data.nodes.sort{it.node}

        newData.data=data;
        newData.levels=lev.sort{it}
        newData.year=yearMap;

//        newData=[data:[nodes:[[id:6, total:10, node:0, name:low, year:prior], [id:7, total:10, node:1, name:medium, year:prior], [id:8, total:10, node:2, name:high, year:prior], [id:3, total:10, node:3, name:low, year:last], [id:4, total:10, node:4, name:medium, year:last], [id:5, total:10, node:5, name:high, year:last], [id:0, total:251, node:6, name:low, year:current], [id:1, total:144, node:7, name:medium, year:current], [id:2, total:269, node:8, name:high, year:current]], links:[[source:2, target:5, value:1], [source:2, target:3, value:0], [source:2, target:4, value:1], [source:0, target:5, value:0], [source:0, target:3, value:1], [source:0, target:4, value:0], [source:1, target:5, value:1], [source:1, target:3, value:0], [source:1, target:4, value:1], [source:5, target:8, value:0], [source:5, target:6, value:1], [source:5, target:7, value:0], [source:3, target:8, value:0], [source:3, target:6, value:0], [source:3, target:7, value:0], [source:4, target:8, value:0], [source:4, target:6, value:0], [source:4, target:7, value:0]]], year:[prior:2014, last:2015, current:2016]]
//        newData=[data:[nodes:[[id:6, total:1815, node:0, name:low, year:prior], [id:7, total:325, node:1, name:medium, year:prior], [id:8, total:95, node:2, name:high, year:prior], [id:3, total:2593, node:3, name:low, year:last], [id:4, total:327, node:4, name:medium, year:last], [id:5, total:106, node:5, name:high, year:last], [id:0, total:2626, node:6, name:low, year:current], [id:1, total:200, node:7, name:medium, year:current], [id:2, total:61, node:8, name:high, year:current]], links:[[source:2, target:5, value:22], [source:2, target:3, value:20], [source:2, target:4, value:13], [source:0, target:5, value:15], [source:0, target:3, value:481], [source:0, target:4, value:57], [source:1, target:5, value:19], [source:1, target:3, value:74], [source:1, target:4, value:61], [source:5, target:8, value:23], [source:5, target:6, value:13], [source:5, target:7, value:31], [source:3, target:8, value:7], [source:3, target:6, value:597], [source:3, target:7, value:51], [source:4, target:8, value:14], [source:4, target:6, value:82], [source:4, target:7, value:48]]], year:[prior:2014, last:2015, current:2016]]

        render newData as JSON
        return;
    }

}
