package com.deerwalk.z5

import grails.converters.JSON


class CohortController {
        def reportFetchService
        def dynamicParamsGeneratorService
        def memberDrillService
        def helperService
        def searchService
        def fetchService

//    def beforeInterceptor = [action: this.&auth]


    private auth() {
        if (!session.isBiometrics) {
            flash.message="Data not available for ${controllerName.toString().capitalize()}."
            redirect(action: 'index',controller: "dashboard")
            return false
        }
    }


    def index() {
            forward(action: "list")
    }


    def list(){



            def reportingDates=helperService.getReportingDates(session.ceDate)
            params.reportingFrom=reportingDates.priorFrom
            params.reportingTo=reportingDates.currentTo
            //initialization of variables
            def dynamicReportParam,data;
//            def dynamicParamsGenerator=applicationContext.getBean("dynamicParamsGeneratorService")
            dynamicReportParam = dynamicParamsGeneratorService.setCohortParams(params, session, [:])
            data = reportFetchService.getCohortList(dynamicReportParam, "list")

            if(!data){
                flash.message="Data not available for ${controllerName.toString().capitalize()}."
                redirect(action: 'index',controller: "dashboard")
                return false
            }

            def tableData=[];
            data.eachWithIndex{ val,id->
                    memberDrillService.cohortDrill(val,params)
                    tableData.add(val)
            }
            [list:tableData,isEditable:false]
    }


    def cohortCompare(){
        def clientId=helperService.fetchClientId()
        def dataComparison=new LinkedHashMap();
        def dataComparison2=new LinkedHashMap();
        def map=[:];

        def counter=0;

        def dataContainer=new LinkedList();

        def reportingDates=helperService.getReportingDates(session.ceDate)

        def parsedJSON=[:]
        params.sel.each{k,v->
            if(v=="on"){

                counter++;
                map=[:];
                def pmeters=JSON.parse(params.drill.getAt(k))
                pmeters.each {key,val->
                    if(key=="cohortKey"){
                        map[key]=params.cohortId.getAt(k)
                    }else
                    map[key]=val;
                }

                map.phiCSDate=session.csDate
                map.phiCEDate=session.ceDate
                map.range =0
                map.isExclude ="false"
                map.isZakipoint="true"
                map['requestIndex']=2

                parsedJSON=[:];
                def drillParams,jsonData;


                (2..6).each{
                    map['requestIndex']=it;
                    drillParams = searchService.createSearchQuery([cohortCompare:true],map,15, 1, clientId, null,null, "msZakipoint","","totalPaidAmount","desc")
                    jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
                    def newJSON =JSON.parse(jsonData);

                    newJSON.each{key,val->
                        if(!parsedJSON[key]){
                            parsedJSON[key]=newJSON[key]
                        }
                    }
                }

                map['requestIndex']=8;
                map['group']=searchService.getGroups()
                drillParams = searchService.createSearchQuery([cohortCompare:true],map,15, 1, clientId, null,null, "msZakipoint","","totalPaidAmount","desc")
                jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
                def newJSON =JSON.parse(jsonData);
                newJSON=newJSON["newMetrics"]
                newJSON.each{key,val->
                    if(!parsedJSON[key]){
                        parsedJSON[key]=newJSON[key]
                    }
                }






//                def drillParams = searchService.createSearchQuery([cohortCompare:true],map,15, 1, clientId, null,null, "msZakipoint","","totalPaidAmount","desc")
//                def jsonData = fetchService.fetchMemberData(drillParams, "membersearch")
//                def parsedJSON=JSON.parse(jsonData);

                parsedJSON.cohortName=params.cohortName.getAt(k)
                parsedJSON.members_drill= pmeters;
                dataComparison[k]=parsedJSON;
                dataContainer.add(dataComparison);

                //		&escapeResultSet=true=======================================&escapeResultSet=true
                /*def newmap=map.clone();
                newmap['escapeResultSet']=true

                def drillParams2 = searchService.createSearchQuery([cohortCompare:true],newmap,15, 1, clientId, null,null, "msZakipoint",'',"totalPaidAmount","desc")
                def jsonData2 = fetchService.fetchMemberData(drillParams2, "membersearch")
                dataComparison2[k]=JSON.parse(jsonData2);*/
            }
        }


//            render(view: "cohortCompare",model: [data:dataContainer,isEditable: false])



        if((request.xhr)){
            render(template:"cohortCompare",model: [data:dataComparison,isEditable: false,reportingDates:reportingDates,rowCount:parsedJSON.keySet().size()+2])

        }else if (counter>7){
            render(view: "cohortCompare",model: [data:dataComparison,isEditable: false,showInIdentity:params.showInIdentity,rowCount:parsedJSON.keySet().size()+2])
        }
        else{
            render(view: "cohortVisCompare",model: [data:dataComparison as JSON,isEditable: false,showInIdentity:params.showInIdentity,reportingDates:reportingDates])

        }


    }
}
