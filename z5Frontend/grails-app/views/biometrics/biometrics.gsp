<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 11/24/16
  Time: 12:56 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Biometrics</title>

</head>
<body>
<div class="contentWrapper">
    %{--<g:hiddenField name="showReportingDetails" value="false"/>--}%
    <div class="container double-arrow-back-top-container">

        <div class="col-md-6 mt25  double-arrow-back-top">
            <g:link controller="dashboard" action="index" class="text-link-upper double-arrow-left">
                <span class="isEditable"><g:editMessage code="hPharmacy.top.navigation"/></span>
            </g:link>
        </div>
    <div class="clearfix"></div>

    <div class="col-md-6">
            <h2 class="mb15 mt15">
                %{--<g:img uri="/images/zExplorer.png" class="heading-icon" style="width:53px;"/>--}%
                <span class="isEditable">Biometrics</span></h2>

        </div>
    <div class="clearfix"></div>

    %{--<div class="watermark mb15"><div style="display: inline;text-transform: capitalize">
        &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
    </div>--}%
    <div class="clearfix"></div>
    <div class="sep-border mb15" ></div>
        <div id="bio-box">
        %{--<div class="col-md-12" style="text-align: center;"><h2>LDL</h2></div>
        <div class="col-md-12" style="text-align: center;">"bad" Cholesterol</div>
            <div class="col-md-4">         <div id="bio-1-1"></div>      </div>
            <div class="col-md-4">        <div id="bio-1-2"></div>    </div>
            <div class="col-md-4">        <div id="bio-1-3"></div>
            <div style="text-align: right"><g:link action="biometricsDetails" href="#" class="btn btn-primary" title="Expand">Expand</g:link> </div>
        </div>
            <div class="clearfix"></div>--}%
        </div>
    </div>
</div>

<script>

    /*var bioData={
        "BMI100": {
            "name":"BMI",
            "desc":"Body mass index",
            "info":[
                "This measurement is a reliable indicator of your body fat and is based on your height and weight. A high BMI indicates an increased risk of developing a number of conditions associated with being overweight.",
                "Normal range is : 18.5 to 24"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program  - incentives",
                "Healthly heart program  - incentives"
            ]
        }
        ,

        "WAIST": {
            "name":"Waist",
            "desc":"Waist in inches",
            "info":[
                "This test measures excess girth around your waist. Results may indicate potential risk for diabetes, heart disease, stroke and some cancers.",
                "Normal range is: 25 to 36"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program  - incentives",
                "Healthly heart program  - incentives"
            ]
        }
        ,

        "BP_SYSTOLIC": {
            "name":"BP Systolic (mmHg)",
            "desc":"Systolic blood pressure",
            "info":[
                "This reading consists of two numbers: “systolic” represents the pressure when the heart beats, and “diastolic” signifies the pressure when the heart is at rest. These two distinct values can indicate an increased risk for heart attack, stroke and other possible health complications.",
                "Normal range is: <120 mmHg"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Healthy Diet program  - incentives",
                "Tobacoo Cessation Program",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        }
        ,
        "BP_DIASTOLIC": {
            "name":"BP Diastolic (mmHg)",
            "desc":"Diastolic blood pressure",
            "info":[
                "This reading consists of two numbers: “systolic” represents the pressure when the heart beats, and “diastolic” signifies the pressure when the heart is at rest. These two distinct values can indicate an increased risk for heart attack, stroke and other possible health complications.",
                "Normal range is: <80 mmHg"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Healthy Diet program  - incentives",
                "Tobacoo Cessation Program",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        }
        ,
        "TOTAL_CHOLESTEROL": {
            "name":"Total Cholesterol (mg/dL)",
            "desc":"Total Cholesterol",
            "info":[
                "Cholesterol number includes readings for both HDL (“good” cholesterol) and LDL (“bad” cholesterol), which measures certain types of fats in your blood.",
                "Normal range is: <200 mmHg"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program - incentives",
                "Healthly heart program"
            ]
        }
        ,
        "CHOLESTEROL_RATIO": {
            "name":"Cholesterol Ratio",
            "desc":"Total Cholesterol",
            "info":[
                "The Total Cholesterol/HDL ratio can indicate potential risks for cardiovascular disease.",
                "Normal range is: <3.4"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program  - incentives",
                "Healthly heart program"
            ]
        }
        ,
        "HDL_CHOLESTEROL": {
            "name":"HDL Cholesterol (mg/dL)",
            "desc":"Good Cholesterol",
            "info":[
                "Good Cholesterol",
                "Normal range is: >60 mg/dL"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Healthy Diet program  - incentives"
            ]
        }
        ,
        "LDL_CHOLESTEROL": {
            "name":"LDL Cholesterol (mg/dL)",
            "desc":"Bad Cholesterol",
            "info":[
                "Bad Cholesterol",
                "Normal range is: <100 mg/dL"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program  - incentives",
                "Tobacoo Cessation Program",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        }
        ,
        "TRIGLYCERIDE": {
            "name":"Triglycerides (mg/dL)",
            "desc":"The major form of fat stored by the body",
            "info":[
                "An ester formed from glycerol and three fatty acid groups. Triglycerides are the main constituents of natural fats and oils, and high concentrations in the blood indicate an elevated risk of stroke.",
                "Normal range is: <150 mg/dL"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program  - incentives",
                "Get active program - incentives",
                "Tobacoo Cessation Program",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        }
        ,
        "FASTING_BLOOD_SUGAR": {
            "name":"Fasting Blood Sugar (mg/dL)",
            "desc":"Glucose content in blood",
            "info":[
                "Your glucose reading measures the amount of sugar in your blood, indicating whether you have or may be at risk for developing diabetes.",
                "Normal range is: <100 mg/dL"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program  - incentives",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        }
        ,
        "NON_FASTING_BLOOD_SUGAR": {
            "name":"Nonfasting Blood Sugar",
            "desc":"Glucose content in blood",
            "info":[
                "Your glucose reading measures the amount of sugar in your blood, indicating whether you have or may be at risk for developing diabetes.",
                "Normal range is: <140"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program  - incentives",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        }
        ,
        "A1C": {
            "name":"A1C (%)",
            "desc":"Level of Hemoglobin",
            "info":[
                "Level of hemoglobin A1c in the blood as a means of determining the average blood sugar concentrations",
                "Normal range is: 4 to 5.6%"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Healthy Diet program  - incentives",
                "Alochol Awareness Program"
            ]
        }
        ,
        "PSA": {
            "name":"PSA (mg/dL)",
            "desc":"Prostate Specific Antigen",
            "info":[
                "Prostate Specific Antigen - protein produced by the prostate gland. Elevated level means potential risk.",
                "Normal range is: <4.0 mg/dL"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Healthy Diet program  - incentives"
            ]
        }
        ,
        "WELLNESS_SCORE": {
            "name":"Wellness Score",
            "desc":"Overall Wellness Score",
            "info":[
                "Proprietary logic to calculate overall wellness using all available data for that patient. Higher means good health.",
                "Normal range is: >75"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program - incentives",
                "Get active program - incentives",
                "Tobacoo Cessation Program",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        }
        ,
        "BIOMETRIC_SCORE": {
            "name":"Biometric Score",
            "desc":"Overall Biometric Score",
            "info":[
                "Proprietary logic to calculate overall biometric scorer using all available biometric readings for that patient. Higher means good health.",
                "Normal range is: >75"
            ],
            "action": [
                "Phone Coaching",
                "Online Coaching",
                "Weight loss program - incentives",
                "Healthy Diet program - incentives",
                "Get active program - incentives",
                "Tobacoo Cessation Program",
                "Alochol Awareness Program",
                "Healthly heart program"
            ]
        },
        "BODY_FAT": {
            "name":"Body Fat (%)",
            "desc":"Overall Body Fat",
            "info":[
                "NA",
                "Normal range is: 0-18"
            ],
            "action": [
                "NA"
            ]
        },"WEIGHT": {
            "name":"Weight",
            "desc":"Weight",
            "info":[
                "NA",
                "Normal range is: 5-180"
            ],
            "action": [
                "NA"
            ]
        }
    };*/




    var div = d3.select("body").append("div").attr("class", "toolTip");
    var colores_g = {"Critical":"#DC3912","High Risk":"#FF7F0E", "Moderate":"#E7BA52","Healthy":"#2CA02C"};

    $(document).ready(function() {

        var data1=${mapp1};
        var data2=${mapp2};
        var data3=${mapp3};
        var years=${datesArr};
        var maxVal=${maxVal};
        bioData=${bioData};

//        var tableContent=$("#table-content").html();

        $.each(${keySet},function(i,k){

            var biometricValue=bioData[k];
            if(!biometricValue){
                return true;
            }
//            k="BPDiastolic100";
            var tableContent='<div>' +
                    '<div  class="z5-table" style="margin-top: -84px;max-height: 242px;"><table width="100%"  border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">';
//                    '<th style="background: #A9D08E;">Info</th>';

            var newData=biometricValue?biometricValue["info"]:"--";


            if(newData){
                $.each(newData,function(k,v){
                    tableContent+='<tr><td>'+v+'</td></tr>';
                });
            }else{
                tableContent+='<tr><td >--</td></tr>';
            }


            tableContent+='<th style="background: #A9D08E;">Actions</th>';

            var newAction=biometricValue?biometricValue["action"]:[];


            if(newAction){
                $.each(newAction,function(k,v){
                    tableContent+='<tr><td >'+v+'</td></tr>';
                });
            }else{
                tableContent+='<tr><td >--</td></tr>';
            }

            tableContent+='</table></div></div>';

            var name=biometricValue?biometricValue["name"]:"--",
                    desc=biometricValue?biometricValue["desc"]:"--";;
            var bioName=biometricValue["name"];

            if(bioName.indexOf("%")>0){
                bioName = bioName.replace('%','%25');
            }


            var str='<div class="col-md-12" style="text-align: center;"><h3>'+name+'</h3>' + //k
                    '</div><div class="clearfix"></div><div class="col-md-12 mb15" style="text-align: center;">'+desc+'</div><div class="clearfix"></div><div id="metrics_'+i+'">' +
                    '<div class="col-md-1">&nbsp;</div>'+
                    '<div class="col-md-10">' +
                    '<div class="col-md-4 no-pad-l no-pad-r"><div id="bio-'+i+'-1"></div></div><div class="col-md-4  no-pad-l no-pad-r"><div id="bio-'+i+'-2"></div></div><div class="col-md-4  no-pad-l no-pad-r"><div id="bio-'+i+'-3"></div></div>' +
//                    '</div>'+
//                    '<div class="col-md-1" style="margin-top: 71px">' +
                    '<div class="clearfix"></div><div style="text-align: right" class="mt15"><a href="biometricsDetails?metricCode='+k+'" class="btn btn-primary" title="Expand">Expand</a> </div>' +
//                    '</div>' +
                    '</div>' +
                    '<div class="col-md-1"></div></div>'+
//                    '<div class="col-md-1 mt15 custom-tooltip">'+tableContent+'</div></div>'+
                    '<div class="clearfix"></div>' +
                    '<div class="sep-border mb15 mt15" ></div>' +
                    '';
            $("#bio-box").append(str);
//            showMetrics=false;

           renderBioGraph("bio-"+i+"-1",data1[k],years[0],maxVal);
           renderBioGraph("bio-"+i+"-2",data2[k],years[1],maxVal);
           renderBioGraph("bio-"+i+"-3",data3[k],years[2],maxVal);
            /*showMetrics=true;

            if(!showMetrics){
                var str='<div class=" col-md-8 watermark mb15" style="color:#000;text-align: center;opacity: .7;"><div style="display: inline;text-transform: capitalize;">' +
                        '&nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> No member found for this biometric.  &nbsp;&nbsp;</div>' +
                        '</div>'

                $("#metrics_"+i).html(str);
            }*/

        });






    });

    function custom_sort(a, b) {
        var aName = a.toLowerCase();
        var bName = b.toLowerCase();
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

//    var colCode=["red","green","orange","gray"];





    function renderBioGraph(id,data,year,maxVal){
            var newObj=new Object();
            var newData=[];

//        var colors = d3.scaleOrdinal(["#FF0000","#F48E9F","#A9D08E"]);

//            $.each(data.levels.sort(custom_sort),function(ind,val){
            $.each(colores_g,function(val,ind){
                var newDataMap=new Object();
                if(data.levels.includes(val)) {
                    newDataMap["value"] = data[val];
                    newDataMap["key"] = val;
                    var max = isNaN(data["rangeEnd_" + val])?null:(data["rangeEnd_" + val]);
                    var min = isNaN(data["rangeBegin_" + val])?null:(data["rangeBegin_" + val]);

                    newDataMap["highRange"] = max? d3.round(max,1):null;
                    newDataMap["lowRange"] = min? d3.round(min,1):null;
                    newDataMap.members_drill = data["drill_" + val];
                    newDataMap.col = colores_g[val];
//                newDataMap.col= colors(val.replace(/ .*/, ""));
                    newData.push(newDataMap);

                }
            });




            /*$.each(data,function(k,v){
                    var newMap=new Object();
                    if(k=="description" ||k=="beginRange" ||k=="endRange" )
                    return;

//                    newMap.label=data.description;
                    newMap.value=v;
                    newMap.key=k;
                    newMap.highRange= data.endRange;
                    newMap.lowRange= data.beginRange;
                    newMap.members_high_drill= data.members_high_drill;
                    newMap.members_med_drill= data.members_med_drill;
                    newMap.members_low_drill= data.members_low_drill;

                if(v>0){
                    showMetrics=true;
                }

                return;



                switch(k){
                    case('high'):
                        newData.splice(0,0,newMap)
                        break;
                    case('medium'):
                        newData.splice(1,0,newMap)
                        break;
                    case('low'):
                        newData.splice(1,0,newMap)
                        break;
                    default:
                        break;

                }

//                    newData.push(newMap)

            })*/

//            if(!showMetrics) return showMetrics;


            data=newData;


             /*data.sort(custom_sort);
           function custom_sort(a, b) {
                return a.value - b.value;
            }

           var newArr=[];

            $.each( data, function( k, v ){
                newArr.push(v);
            });
            var len=data.length;
            var start= 0,end=1;
            $.each( data, function( k, v ){
                var newIndex;
                if(k%2==0){
                    newIndex=start;
                    newArr.splice(newIndex,1, v)
                    start++;
                }else{
                    newIndex=len-end;
                    newArr.splice(newIndex,1, v)
                    end++;
                }
            });

            data=newArr;*/







            var axisMargin = 20,
                    margin = 120,
                    valueMargin = 18,
                    width = 300,
                    height = 360,
                    barHeight = (height-axisMargin-margin)* 0.4/data.length,
                    barPadding = (height-axisMargin-margin)*0.6/data.length,
                    bar, svg, scale, xAxis;

            barPadding=10;
            barHeight=15;

//            max = d3.max(data, function(d) { return d.value; });
            var max = maxVal;


            var heightBar=30*data.length;

            svg = d3.select('#'+id)
                    .append("svg")
                    .attr("width", width)
                    .attr("height", heightBar);


            bar = svg.selectAll("g")
                    .data(data)
                    .enter()
                    .append("g");

            bar.attr("class", "bar")
                    .attr("cx",0)
                    .attr("fill",function(d,i){
                        /*if(d.value<10) return "#FF0000";
                        if(d.value<20) return "#FFE699";
                        if(d.value>10) return "#A9D08E";*/
//                        if(d.key=="high") return "#EC3E40";
//                        if(d.key=="medium") return "#A9D08E";
//                        if(d.key=="low") return "#EC3E40";
                        return colores_g[d['key']];
                    })
                    .attr("transform", function(d, i) {
//                        return "translate(" + margin + "," + (i * (barHeight + barPadding) + barPadding) + ")";
                        return "translate(" + (margin) + "," + (i * (barHeight + barPadding) + barPadding) + ")";
                    }).style("cursor","pointer");

            bar.append("text")
                    .attr("class", "label")
                    .attr("y", barHeight / 2)
                    .attr("dy", ".35em") //vertical align middle
                    .attr("dx", "-.5em") //horizontal align middle
                    .attr("fill", "black")
                    .text(function(d){
//                        return d.label;

//                            if(d.key=="high") return "High (>"+ d.highRange+")";
//                            if(d.key=="medium") return "Med ("+d.lowRange+" - "+d.highRange+")";
//                            if(d.key=="low") return "Low (<"+d.lowRange+")";

                             if(!d.lowRange){
                                 return "(< "+d.highRange+")";
                             }else if(!d.highRange){
                                 return "(> "+d.lowRange+")";
                             }else
                             return "("+d.lowRange+" - "+d.highRange+")";


//                        return d.value
                    })
                    .attr("text-anchor", "end")



        bar.append("text")
                .attr("class", "label")
                .attr("y", barHeight / 2)
                .attr("dy", ".35em") //vertical align middle
                .attr("dx", "-11.5em") //horizontal align middle
                .attr("fill", "black")
                .text(function(d){

                        return d.key
                })
                .attr("text-anchor", "start")


            scale = d3.scale.linear()
                    .domain([0, max])
                    .range([0, width - margin-40 ]);

            /*xAxis = d3.svg.axis()
             .scale(scale)
             .tickSize(-height + 2*margin + axisMargin)
             .orient("bottom");*/

            var yScale = d3.scale.ordinal().range([heightBar, 0]);

            var yAxis = d3.svg.axis()
                    .scale(yScale)
                    .orient("left")
                    .tickSize(0);

            xAxis = d3.svg.axis()
                    .scale(scale)
                    .orient("bottom")
                    .tickSize(0);

            bar.append("svg:a")
                    .attr("data-href", function(d,i){

                        return createZ5Link(d.members_drill);
                    })
                    .attr("onclick", "postRequestForGet(this);return false;")
                    .append("rect")
//                    .attr("transform", "translate("+0+", 0)")
                    .attr("height", barHeight)
                    .attr("width", function(d){
                        return scale(d.value);
                    });

            bar.append("svg:a")
                    .attr("data-href", function(d,i){

                      return createZ5Link(d.members_drill);
                    })
                    .attr("onclick", "postRequestForGet(this);return false;").append("text")
//                    .attr("class", "label")
                    .attr("y", (barHeight / 2) +4)
//                    .attr("dx", valueMargin ) //margin right
//                    .attr("dy", ".10em") //vertical align middle
                    .attr("text-anchor", "start")
                    .text(function(d){
                        return (d.value);
                    })
                    .attr("x", function(d){
                        var width = this.getBBox().width;

                        return scale(d.value)+1;
//                        return Math.max(width + valueMargin);
                    }).attr("fill","grey").attr("class", "label");

            bar
                    .on("mousemove", function(d){
//                        d3.select(this).attr("opacity",1)
                        div.style("left", d3.event.pageX+10+"px");
                        div.style("top", d3.event.pageY-25+"px");
                        div.style("display", "inline-block");
//                        div.html((d.key.toUpperCase())+"<br>"+(d.value)+"");
                        div.html("<b>"+d.key+"</b><br>"+"Member Count: "+(d.value)+"");

                        var color="";

                        /*if(d.key=="high") color= "#FF0000";
                        if(d.key=="medium") color= "green";
                        if(d.key=="low") color= "#FF0000";*/

                        color= "steelblue";

                        d3.select(this).transition()
                                .ease("linear").attr("fill",color)/*.attr("transform", function(d, i) {
                                    return "translate(" + (margin) + "," + (i * (barHeight + barPadding) + barPadding - 3) + ")";
                                });

                        d3.select(this).select("rect").attr("height",barHeight+6 )*/


                    });
            bar
                    .on("mouseout", function(d){
                        div.style("display", "none");

                        var color="";

                        /*if(d.key=="high") color= "#EC3E40";
                        if(d.key=="medium") color= "#A9D08E";
                        if(d.key=="low") color= "#EC3E40";*/

                        color= d.col;

                        /*d3.select(this).transition()
                                .ease("linear").attr("fill",color)*/

                        d3.select(this).transition()
                                .ease("linear").attr("fill",color);


                    });

            /*svg.insert("g",":first-child")
             .attr("class", "axisHorizontal")
             .attr("transform", "translate(" + (margin + labelWidth) + ","+ (height - axisMargin - margin)+")")
             .style("stroke", "black")
             .style("fill", "none")
             .style("stroke-width", 1)
             .call(xAxis)*/


            svg.insert("g",":first-child")
                    .attr("transform", "translate(" + (margin -1) + ", " + 0 + ")")
                    .attr("text-anchor", "middle")
                    .attr("font-family", "sans-serif")
                    .attr("font-size", "10px")
                    .style("stroke", "black")
                    .style("fill", "none")
                    .style("stroke-width", 2)
                    .style("shape-rendering", "crispEdges")
                    .call(yAxis)
                    .selectAll("text")
                    .attr("stroke", "none")
                    .attr("fill", "black")

           $('#'+id).append("<div style='text-align: center;font-weight: bold;margin-left: 28px;'>"+year+"</div>");


//           return showMetrics;

        }

</script>

<style>
svg {
    width: 100%;
    height: 100%;
    /*position: center;*/
}

.toolTip {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    position: absolute;
    display: none;
    width: auto;
    height: auto;
    background: none repeat scroll 0 0 white;
    border: 0 none;
    border-radius: 8px 8px 8px 8px;
    box-shadow: -3px 3px 15px #888888;
    color: black;
    font: 12px sans-serif;
    padding: 5px;
    text-align: center;
}

text {
    /*font: 10px sans-serif;*/
    /*color: white;*/
}
text.value {
    /*font-size: 120%;*/
    /*fill: white;*/
}

.axisHorizontal path{
    /*fill: none;*/
    /*stroke-width: 1px !important;*/
}

.axisHorizontal .tick line {
    stroke-width: 1;
    stroke: rgba(0, 0, 0, 0.2);
}

.bar {
    /*fill: steelblue;*/
    fill-opacity: 1;
}
</style>
</body>
</html>