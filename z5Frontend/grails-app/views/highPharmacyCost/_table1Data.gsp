<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="z5-table">
    <g:hiddenField name="reportingBasis1" value="${session.hr.reportingBasis}"/>
    <g:hiddenField name="includeOn" value="${session.hr.isExclude}"/>
    <g:hiddenField name="range1" value="${session.hr.range}"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
        <thead>
        <tr>
            <th class="text-left isEditable"><g:editMessage code="highPharmacy.container3.table1.head1"/><g:editMessage code="highPharmacy.container3.table1.head12"/></th>
            <g:if test="${type=='brand'}">
                <util:remoteSortableColumn   class="text-center ${defaultSort=='brand'?'sortable sorted desc':''}"  property="savings" title="${editMessage(code:"highPharmacy.container3.table1.head2")}" update="dataHolder" action="sortResult" params="[type:type]" />
            </g:if>
            <g:if test="${type=='mail'}">
            <util:remoteSortableColumn    class="text-center  ${defaultSort=='mail'?'sortable sorted desc':''}"  property="savings" title="${editMessage(code:"highPharmacy.container3.table1.head3")}" update="dataHolder" action="sortResult" params="[type:type]" />
            </g:if>
            <util:remoteSortableColumn   class="text-center"  property="annualCost" title="${editMessage(code:"highPharmacy.container3.table1.head4")}" update="dataHolder" action="sortResult" params="[type:type]" />
            <util:remoteSortableColumn class="text-center ${defaultSort=='speciality'?'sortable sorted desc':''}"  property="paidAmountPmpm" title="${editMessage(code:"highPharmacy.container3.table1.head7")}" update="dataHolder" action="sortResult" params="[type:type]" />
            %{--<util:remoteSortableColumn class="text-center" property="pmpmBenchMark" title="${editMessage(code:"highPharmacy.container3.table1.head5")}" update="dataHolder" action="sortResult" />--}%
            <util:remoteSortableColumn   property="populationSize" class="text-right" defaultOrder="desc" title="${editMessage(code:'highPharmacy.container3.table1.head6')}${editMessage(code:'highPharmacy.container3.table1.head61')}" update="dataHolder" action="sortResult" params="[type:type]" />
            <th></th>
        </tr>

        <tr>
            <th class="text-left"><strong>Totals</strong></th>
<g:if test="${type=='brand'}">
            <th class="text-center "><strong><g:formatZ5Currency number="${total.totalSavings}" type="number" maxFractionDigits="2" />
            </strong></th>
    </g:if>

    <g:if test="${type=='mail'}">
            <th class="text-center "><strong><g:formatZ5Currency number="${total.totalSavings}" type="number" maxFractionDigits="2" />
            </strong></th>
    </g:if>
            <th class="text-center"><strong><g:formatZ5Currency number="${total.totalAnnualCost}" type="number" maxFractionDigits="2" />
            </strong></th>

            <th class="text-center"><strong>
                $<g:formatNumber number="${total.totalPaidAmountPmpm}" type="number" maxFractionDigits="2" /> PMPM
                <br> </strong></th>
            %{--<th class="text-center"><strong></strong></th>--}%

            <th class="text-right"><strong>
                <g:formatNumber number="${total.totalPrescriptions}" type="number" maxFractionDigits="2" /> prescriptions<br>
                <g:formatNumber number="${total.totalPopulationSize}" type="number" maxFractionDigits="2" /> members
            </strong></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${data}">

            <g:each in="${data}" var="data1" status="i">
                <tr>

                    <td >
                        %{--<g:if test="${data1.genericOrBrand=="brand"}">--}%
                            <strong style="text-transform: capitalize">${data1.drugName}</strong><br />
                            <span style="text-transform: lowercase">${data1.equvalentGenericDrug?data1.equvalentGenericDrug:'-'}</span>
                       %{-- </g:if><g:else>
                        <strong style="text-transform: lowercase">${data1.drugName}</strong><br />
                        <span style="text-transform: lowercase">${data1.equvalentGenericDrug}</span>
                        </g:else>--}%

                    </td>
                <g:if test="${type=='brand'}">
                    <td class="text-center  genericOrder">
                        <span id="barT1a_${i}" class="barGraph"></span><br>
                        <script>getBarGraph("barT1a_"+${i},${data1.savings},${total.totalSavings});</script>
                        <g:formatZ5Currency number="${data1.savings}" type="number" maxFractionDigits="2" /> </td>
                    </g:if>
                <g:if test="${type=='mail'}">
                    <td class="text-center  mailOrder">
                        <span id="barT1b_${i}" class="barGraph"></span><br>
                        <script>getBarGraph("barT1b_"+${i},${data1.savings},${total.totalSavings});</script>
                        <g:formatZ5Currency number="${data1.savings}" type="number" maxFractionDigits="2" />
                    </td>
                    </g:if>

                    <td class="text-center">
                        <g:formatZ5Currency number="${data1.annualCost}" type="number" maxFractionDigits="2" />
                    </td>

                    <td class="text-center">
                        $<g:formatNumber number="${Math.abs(data1.paidAmountPmpm)}" type="number" maxFractionDigits="2" />
                        PMPM
                    </td>
                    <td class="text-right">
                        %{--G<a href="#"><i class="fa fa-users iconLRmargin"></i>--}%
                        <g:formatNumber number="${Math.abs(data1.prescriptions)}" type="number" maxFractionDigits="2" /> prescriptions<br>
                        <g:formatNumber number="${data1.populationSize}" type="number" maxFractionDigits="2" /> members
                    </td>
                    <td class="iconExplorePopulation">
                        %{--<span class="iconExplorePopulation">
                            <g:z5Link disabledLink="false" controller="memberSearch" action="searchResult" params="${data1.membersDrill}" >
                                <g:img uri="/images/icon-explore-population.png" alt="Tilde" class="pull-left" /> </g:z5Link>
                            </span>--}%


                        <g:z5Link  disabledLink="false" controller="memberSearch" action="searchResult" params="${data1.members_drill}">
                            <g:img uri="/images/icon-explore-population.png" alt="Tilde" class="pull-left" /> </g:z5Link>
                        </td>


                        %{--<a href="#" class="iconExplorePopulation"><g:img uri="/images/icon-explore-population.png" alt="Tilde" class="pull-left" /></a></td>--}%
                </tr>
            </g:each>

        </g:if><g:else>
            <tr>
                <td colspan="8">
                    No records to display.
                </td>
            </tr>
        </g:else>


        </tbody>

    </table>
</div>
    %{--<g:if test="${!(size<=5)}">--}%
        <div class="z5-pagination">
            %{--<g:remotePaginate action="searchResult12" offset="${(page-1)*5}" total="${size}" update="dataHolder" maxsteps="5" max="${5}" />--}%
            <util:remotePaginate controller="highPharmacyCost" action="remotePagination" total="${size}" update="dataHolder" max="5" pageSizes="[5:'5 Records/Page', 20: '20 Records/Page', 50:'50 Records/Page']" params="[type:type]" alwaysShowPageSizes="true" />
            %{--<g:remotePaginate action="searchResult" offset="${(page-1)*pageSize}" total="${summary.getAt('totalCounts').toInteger()}" update="dataHolder" maxsteps="5" max="${pageSize}" pageSizes="[10:'10 Records/Page', 20: '20 Records/Page', 50:'50 Records/Page',100:'100 Records/Page']"/>--}%
            %{--<div class="showing">Showing 1-3 of 2 drugs</div>--}%
            %{--<div class="page-no"><a href="#"><i class="fa fa-chevron-left iconLRmargin"></i>Prev</a> Page <span class="active">2</span> of 5 <a href="#">Next<i class="fa fa-chevron-right iconLRmargin"></i></a></div>--}%
            %{--<div class="clearfix"></div>--}%
        </div>
    %{--</g:if>--}%

<script>



    <g:if test="${type=='brand'}">
    var amount=(${total.totalSavings})>=1000?"$ "+((${total.totalSavings})/1000).toFixed(2)+" K":"$ "+Math.round(${total.totalSavings});
    $("#tableHead1").html("Optimize generic drug use for a total of <strong>"+amount+"</strong> or more in potential savings");

    </g:if><g:elseif test="${type=='mail'}">
    var amount=(${total.totalSavings})>=1000?"$ "+((${total.totalSavings})/1000).toFixed(2)+" K":"$ "+Math.round(${total.totalSavings});
    $("#tableHead1").html("Optimize mail order use for a total of "+amount+" or more in potential savings");
    </g:elseif><g:else>
    $("#tableHead1").html("Optimize speciality drug pricing and cost sharing");
    </g:else>


    $( document ).ready(function() {
        var $table = $('table.z5-fixed-header');
        $table.floatThead({
            scrollContainer: function($table){
                return $table.closest('.z5-table');
            }
        });
    });

</script>


