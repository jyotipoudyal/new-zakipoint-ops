<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <g:each in="${data}" var="val">
                    ${val.key}
                    ${val.value}
                    <br>
                </g:each>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveData()">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            </div>
        </div>
</div>
