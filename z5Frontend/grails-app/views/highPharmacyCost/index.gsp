<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="layout" content="main">
    <title>High Pharmacy</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
%{--<link href="${resource(dir:"css",file: "layout.css")}" rel="stylesheet"/>--}%
</head>

<body>

<div>


    <div id="contentWrapper" class="contentWrapper">
        <div class="container double-arrow-back-top-container">

            <div style="position: absolute">
                <svg>
                    <pattern id="pattern-stripe"
                            width="5"
                            height="50"
                            patternUnits="userSpaceOnUse"
                            patternTransform="rotate(-45)">
                        <rect width="4"
                              height="100%"
                              fill="#034f96"></rect>

                        <g stroke="#b53322" stroke-width="45">
                            <path d="M25,0 25,100"></path>
                        </g>

                    </pattern>
                </svg>
            </div>

            <div class="col-md-6 double-arrow-back-top">
                <g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
                                        <span class="isEditable"><g:editMessage code="hPharmacy.top.navigation"/></span>
                </g:link>
            </div>

            <div class="col-md-6" style="display: none;" id="editContent">
                <li>
            %{--<g:render template="../dashboard/clientInfo"></g:render>--}%

                <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                    <g:if test="${!isEditable}">
                        <g:link controller="cms" action="highPharmacy" title="Edit Content" ><i
                                class="fa fa-edit"></i> Edit Content</g:link>
                    </g:if><g:else>
                    <g:link controller="highPharmacyCost" action="index" title="View Changes" ><i
                            class="fa fa-eye"></i> View Changes</g:link>
                </g:else>
                </sec:ifAllGranted>
                </li>
            </div>
            <%
                //        split page
            %>

            <div class="clearfix"></div>

            <div class="col-md-10">
                <h2 class="mb15">
                    <g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon"/>
                    <span class="isEditable"><g:editMessage code="hPharmacy.container1.graph1.title"/></span></h2>

                <h3 class="isEditable"><g:editMessage code="hPharmacy.container1.graph1.subTitle"/></h3>
            </div>

            <div class="col-md-2">
                <g:form action="exportPdf" name="exportPdfForm">
                %{--<g:form controller="programTracking" action="index" name="exportPdfForm">--}%
                    <g:hiddenField name="imagePath_1"/>
                    <g:hiddenField name="imagePath_2"/>
                    <g:hiddenField name="imagePath_3"/>
                    <g:hiddenField name="imagePath_4"/>
                    <g:hiddenField name="imagePath_5"/>
                    <g:hiddenField name="paidFiler"/>
                    <g:hiddenField name="rangeFilter"/>
                    <g:hiddenField name="excludeFilter"/>
                </g:form>
                <a href="javascript:void(0)" onclick="generatePdf()" class="custom-tooltip btn-link-upper btn-light-blue mb15 isEditable"><g:editMessage
                        code="hPharmacy.container1.graph1.option2"/></a>
            </div>

            <div class="clearfix"></div>

            <div class="sep-border mb15"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="hPharmacy.container1.graph1.title2"/></h1>
                %{--<h6 class="isEditable"><g:editMessage code="hPharmacy.container1.graph1.title3"/></h6>--}%
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="hPharmacy.explore.population"/></span><input type="hidden"
                                                                               value="hPharmacy.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('rxHistoricPredictive')"
                   class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden"
                                                                                                         value="hPharmacy.container3.table1.options2">
                </a>
            </div>


            <div class="col-md-12">
                <h6 class="has-filter">
                <span id="graph1Filter">
                    <g:if test="${!isEditable}">
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasis1"
                                                                          from="${['PaidDate': 'paid', 'ServiceDate': 'incurred']}"
                                                                          optionKey="key" optionValue="value"
                                                                          onchange="refreshReport()"/></span>
                    %{--cost to <span style="color: steelblue;"><g:select name="regionalg1" from="${['1':'NorthEast','2':'NorthCentral','3':'South']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')" noSelection="['':'regional']" /></span>--}%
                        claims to national benchmarks</span>
                    %{--<span style="color: steelblue;"> <g:select name="fromDate1" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value" onchange="refreshReport()" /></span>--}%
                        that <span style="color: steelblue;"><g:select style="width:75px;" name="includeOn"
                                                                       from="${['false': 'include', 'true': 'exclude']}"
                                                                       optionKey="key" optionValue="value"
                                                                       onchange="refreshReport()"/></span>%{--noSelection="['':'exclude']"--}%
                        cases over <span style="color: steelblue;"><g:select style="width:65px;" name="range1"
                                                                             from="${['0': '$0', '10000': '$10 k', '25000': '$25 k', '50000': '$50 k', '100000': '$100 k']}"
                                                                             optionKey="key" optionValue="value"
                                                                             onchange="refreshReport()"/></span>
                        per year
                    </g:if>
                    <span class="f-right">
                        <span class=" isEditable"><g:editMessage code="hPharmacy.container1.methodology"/></span>

                        <span class="fa fa-chevron-right " onclick="displayData('methodology1')"></span>
                    </span>
                </h6>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-7">
                <g:if test="${!isEditable}">
                    <div class="bdr-right">
                        <div id="lineGraph1title">&nbsp;</div>
                        %{--<strong style="font-size: 16px;"><span class=" isEditable" ><g:editMessage code="hPharmacy.container1.table2.head"/></span></strong>--}%
                        <div id="lineGraph1"></div>
                        <g:render template="../include/lineLegends"/>

                    </div>
                </g:if>
            </div>

            <div class="col-md-5">
                <g:if test="${!isEditable}">
                    <div id="lineGraphRtitle" class="col-md-12"></div>
                    <div id="lineGraphR" class="col-md-8"></div>


                </g:if>
            </div>

            <div class="clearfix"></div>

            <div class="sep-border mb25 pt15"></div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="hPharmacy.container1.table1.head"/></h1>
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)"  class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="hPharmacy.explore.population"/></span><input type="hidden"
                                                                               value="hPharmacy.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('barGraph','rxBrandGenericCost')"
                    class=" text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden"
                                                                                                         value="hPharmacy.container3.table1.options2">
                </a>

                %{--<a href="javascript:void(0)"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
            </div>

            <div class="col-md-12">
                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:editMessage code="highPharmacy.container1.methodology"/>
                            <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>

                        </span>

                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-7">
                <div class="bdr-right-dashed">
                    %{--<h1 class="isEditable"><g:editMessage code="hPharmacy.container1.table1.head"/></h1>--}%
                    <div class="sub-title"><span class="isEditable"><g:editMessage
                            code="hPharmacy.container1.table1.subhead"/></span>
                    </div>

                    <div id="barGraph1"></div>
                </div>

                <div class="clearfix"></div>

                %{--<div>
                    <g:link controller="highPharmacyCost" action="drugsDetails" params="[type:'brand']"  class="see-dtl-utilization">
                        <span class=" isEditable"><g:editMessage code="hPharmacy.detailedUtilization"/></span>

                    </g:link>
                </div>--}%
            </div>

            <%
                //        split page
            %>
            <div class="col-md-5">


                <p><span class=" isEditable custom-tooltip"><g:editMessage code="hPharmacy.leftbar2.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="hPharmacy.leftbar2.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <g:link controller="highPharmacyCost" action="drugsDetails" params="[type:'brand']"  class="btn-link-normal btn-blue btn-inline isEditable">
                        %{--<g:editMessage code="hPharmacy.evaluate"/>--}%See Detailed Utilization

                    </g:link>
                    %{--<a href="javascript:void(0)"  class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"> <g:editMessage code="hPharmacy.evaluate"/></a>--}%
                </div>

                <div class="col-md-8 no-pad-l mb25 custom-tooltip isEditable"><g:editMessage
                        code="hPharmacy.leftbar2.list2"/></div>

                <div class="col-md-4 no-pad-r text-right ">
                <a href="javascript:void(0)"  class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"> <g:editMessage code="hPharmacy.evaluate"/></a>
                       </div>




            </div>

            <div class="clearfix"></div>

            <div class="sep-border-dashed mb25 pt15"></div>
            <div class="clearfix"></div>

            <div class="col-md-6">
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="hPharmacy.explore.population"/></span><input type="hidden"
                                                                           value="hPharmacy.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('barGraph','rxMailRetailCost')"
                    class=" text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden"
                                                                                                     value="hPharmacy.container3.table1.options2">
                </a>

                %{--<a href="javascript:void(0)"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
            </div>
            <div class="col-md-12">
                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:editMessage code="highPharmacy.container1.methodology"/>
                            <span class="fa fa-chevron-right " onclick="displayData('methodology3')"></span>

                        </span>

                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="col-md-7">
                <div class="bdr-right-dashed">
                    %{--<h1 class="isEditable"><g:editMessage code="hPharmacy.container1.table1.head"/></h1>--}%
                    <div class="sub-title"><span class=" isEditable"><g:editMessage
                            code="hPharmacy.container2.table1.subhead"/></span>
                    </div>

                    <div id="barGraph2"></div>
                </div>

                <div class="clearfix"></div>

                %{--<div>
                    <g:link controller="highPharmacyCost" action="drugsDetails" params="[type:'mail']" class="see-dtl-utilization">
                        <span class=" isEditable"><g:editMessage code="hPharmacy.detailedUtilization"/></span>

                    </g:link>
                </div>--}%
            </div>

            <%
                //        split page
            %>
            <div class="col-md-5">


                <p><span class=" isEditable custom-tooltip"><g:editMessage code="hPharmacy.leftbar3.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="hPharmacy.leftbar3.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">

                    <g:link controller="highPharmacyCost" action="drugsDetails" params="[type:'mail']"  class="btn-link-normal btn-blue btn-inline isEditable">
                        %{--<g:editMessage code="hPharmacy.evaluate"/>--}%

                        See Detailed Utilization
                    </g:link>

                </div>

                <div class="col-md-8 custom-tooltip no-pad-l mb25 isEditable"><g:editMessage
                        code="hPharmacy.leftbar3.list2"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="javascript:void(0)"  class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable">
                        <g:editMessage code="hPharmacy.evaluate"/></a></div>

            </div>

            <div class="clearfix"></div>

            <div class="sep-border-dashed mb25 pt15"></div>
            <div class="col-md-6">
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="hPharmacy.explore.population"/></span><input type="hidden"
                                                                           value="hPharmacy.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('barGraph','rxSpecialityCost')"
                    class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden"
                                                                                                     value="hPharmacy.container3.table1.options2">
                </a>

                %{--<a href="javascript:void(0)"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
            </div>
            <div class="col-md-12">

                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:editMessage code="highPharmacy.container1.methodology"/>
                            <span class="fa fa-chevron-right " onclick="displayData('methodology4')"></span>
                        </span>
                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="col-md-7">
                <div class="bdr-right-dashed">
                    %{--<h1 class="isEditable"><g:editMessage code="hPharmacy.container1.table1.head"/></h1>--}%
                    <div class="sub-title"><span class=" isEditable"><g:editMessage
                            code="hPharmacy.container4.table1.subhead"/></span>
                    </div>

                    <div id="barGraph3"></div>
                </div>

                <div class="clearfix"></div>

                %{--<div>
                    <g:link controller="highPharmacyCost" action="drugsDetails"   class="see-dtl-utilization">
                        <span class=" isEditable"><g:editMessage code="hPharmacy.detailedUtilization"/></span>

                    </g:link>
                </div>--}%
            </div>

            <%
                //        split page
            %>
            <div class="col-md-5">
                                <p><span class=" isEditable custom-tooltip"><g:editMessage code="hPharmacy.leftbar4.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="hPharmacy.leftbar4.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    %{--<a href="javascript:void(0)" class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable">
                        <g:editMessage code="hPharmacy.evaluate"/></a>--}%

                    <g:link controller="highPharmacyCost" action="drugsDetails"  class="btn-link-normal btn-blue btn-inline isEditable">
                        %{--<g:editMessage code="hPharmacy.evaluate"/>--}%
                        See Detailed Utilization

                    </g:link>

                </div>



            </div>

        </div>


        <%
            //        split page
        %>
        <div id="methodology1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="hPharmacy.container1.methodology1"/></span>
                    </div>

                </div>
            </div>
        </div>

        <div id="methodology2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="hPharmacy.container1.methodology2"/></span>
                    </div>

                </div>
            </div>
        </div>

        <div id="methodology3" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="hPharmacy.container1.methodology3"/></span>
                    </div>
                </div>
            </div>
        </div>

        <div id="methodology4" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="hPharmacy.container1.methodology4"/></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%
        //        some comment
    %>


    <div id="barDetails" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>

                    <div class="isEditable" id="barContent">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <g:form action="exportCsv" name="exportForm" style="display: none;">

        <g:hiddenField name="exportReportingBasis"/>
        %{--<g:hiddenField name="exportFromDate"/>--}%
        <g:hiddenField name="exportRange"/>
        <g:hiddenField name="exportModule"/>
        <g:hiddenField name="barModule"/>
    </g:form>

    <style>


    .hbar {
        /*mask: url(#mask-stripe)*/
        /*fill: url(#mask-stripe);*/

        fill: url(#pattern-stripe) !important;
    }

    .bar {
        fill: steelblue;
    }

    .lineColor {
        fill: none;
        stroke: #1c94c4;
        stroke-width: 1px;
        stroke-dasharray: 6 3
    }

    .dbar {
        fill: #034f96;
    }

    .x.axis path {
        /*display: none;*/
    }

    </style>


    <script>

        $("span.bulb-lists").find("ul").addClass("bulb-lists color-blue");
        $("span.bulb-lists").find("li").prepend("<i class='fa fa-lightbulb-o'></i>");
    </script>
    <script>


        function refreshReport() {
            var reportingBasis, fromDate, range, isExclude;
            reportingBasis = $("#reportingBasis1").val();
//            fromDate = $("#fromDate1").val();
            isExclude = $("#includeOn").val();
            range = ($("#range1").val());

            jQuery.ajax({
                type: 'POST',
                data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'lineGraph1'},
                url: '<g:createLink controller="highPharmacyCost" action="getScriptForPharmacy"/>',
                success: function (resp) {
                    render2Data(resp.topLineGraph, resp.max)
                    render4Data(resp.dm,resp.yPercentage, resp.yTotal, resp.max);
                    $(".lineLegends").show();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                complete: function (XMLHttpRequest, textStatus) {
                }
            });


            jQuery.ajax({
                type: 'POST',
                data: {
                    reportingBasis: reportingBasis,
                    range: range,
                    isExclude: isExclude,
                    module: 'barGraphs'
                },
                url: '<g:createLink controller="highPharmacyCost" action="getScriptForPharmacy"/>',
                success: function (resp) {
                    renderData(resp.b1,"barGraph1");
                    renderData(resp.b2,"barGraph2");
                    renderData(resp.b3,"barGraph3");

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                complete: function (XMLHttpRequest, textStatus) {
                }
            });


        }


        function exportCsv(module,barType) {

            <g:if test="${!isEditable}">
            var reportingBasis, fromDate, range, isExclude;
            reportingBasis = $("#reportingBasis1").val();
//            fromDate = $("#fromDate1").val();
            isExclude = $("#includeOn").val();
            range = ($("#range1").val());

            $("#exportReportingBasis").val(reportingBasis);
//            $("#exportFromDate").val(fromDate);
            $("#exportRange").val(range);
            $("#exportExclude").val(isExclude);
            $("#exportModule").val(module);
            $("#barModule").val(barType);
            $("#exportForm").submit();
            </g:if>


        }






        function render4Data(da, yPercentage, yT, max) {
            $("#lineGraphR").html("");

            /*var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
             main_width = 500 - main_margin.left - main_margin.right,
             main_height =300 - main_margin.top - main_margin.bottom;*/

            /*var main_margin = {top: 20, right: 140, bottom: 50, left: 60},
                    main_width = 420 - main_margin.left - main_margin.right,
                    main_height = 300 - main_margin.top - main_margin.bottom;*/

            var main_margin = {top: 20, right: 55, bottom: 50, left: 60},
                    main_width = 420 - main_margin.left - main_margin.right,
                    main_height = 300 - main_margin.top - main_margin.bottom;

            var pharmacyClaims = da;
            //var benchMark = 3000;
            var memberMonths = da.value;
            var newVal;
            var data = [];
            var parseDate4 = d3.time.format("%Y-%m-%d").parse;


            $.each(pharmacyClaims, function (key, val) {
                newVal = new Object();
                newVal.time1 = parseDate4(val.year);
                newVal.actual = Math.round(val.actual)
                data.push(newVal);
            });
            data.sort(custom_sort);

//            var percent=Math.round(((data[1].actual-data[0].actual)/data[0].actual)*100)

            var percent = d3.format(".2r")(yT)
            if (percent < 0) {
                $("#lineGraphRtitle").html("Pharmacy Costs PMPM likely to <strong>decrease by " + -(percent) + "%</strong> in the next 12 months")
            } else if (percent > 0) {
                $("#lineGraphRtitle").html("Pharmacy Costs PMPM likely to <strong>increase by " + percent + "%</strong> in the next 12 months")
            } else {
                $("#lineGraphRtitle").html("Pharmacy Costs PMPM likely to be <strong>same</strong> in the next 12 months")
            }


            var formatDate = bisectDate = d3.bisector(function (d) {
                        return d.time1;
                    }).left,
                    formatOutput0 = function (d) {
                        return d3.format("$,")(Math.round(d.actual)) +" Pharmacy Costs ";
                    },
                    formatOutput1 = function (d) {
                        return "(" + Math.round(yPercentage) + " % Total spend projected) ";
                    };

            var main_x = d3.time.scale()
                    .range([0, main_width]);

            var main_y = d3.scale.linear()
                    .range([main_height, 0]);

            var main_xAxis = d3.svg.axis()
                    .scale(main_x)
                    .outerTickSize(0)
                    .ticks(d3.time.months, 1)
                    .tickFormat(d3.time.format('%b'))
                    .orient("bottom");

            /* var main_xAxis2 =d3.svg.axis()
             .scale(main_x)
             .ticks(d3.time.months,8)
             .tickFormat(d3.time.format("%Y %B"))
             .tickSize(5,0)
             .orient("bottom");*/


            var main_yAxisLeft = d3.svg.axis()
                    .scale(main_y)
                    .ticks(5)
                    .tickSize(-main_width)
                    .outerTickSize(0)
                    .orient("left");

            var main_line0 = d3.svg.line()
                    .x(function (d) {
                        return main_x(d.time1);
                    })
                    .y(function (d) {
                        return main_y(d.actual);
                    });

            var svg = d3.select("#lineGraphR").append("svg")
                    .attr("width", main_width + main_margin.left + main_margin.right)
                    .attr("height", main_height + main_margin.top + main_margin.bottom);

            svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("width", main_width)
                    .attr("height", main_height);

            var main = svg.append("g")
                    .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");


            %{--d3.csv(paths, function(error, data) {
                data.forEach(function(d) {
                    d.time1 = parseDate(d.time1);
                    d.expected = +d.expected;
                    d.actual = +d.actual;
                });--}%


            data.sort(function (a, b) {
                return a.time1 - b.time1;
            });


            var value1 = Math.round(data[data.length - 1].actual);
            var value2 = Math.round(data[data.length - 1].expected);


            main_x.domain([data[0].time1, data[data.length - 1].time1]);
            var array_y0 = d3.extent(data, function (d) {
                return d.actual;
            });
//            main_y.domain([0,d3.max(array_y0)]);
            main_y.domain([0, max]);

            var main_xAxis2 = d3.svg.axis()
                    .scale(main_x)
//                    .tickFormat(d3.time.format("%b %Y"))
                    .tickFormat(formatDates)
                    .tickValues(main_x.domain())
                    .orient("bottom");


            /*main.append("g")
             .attr("class", "x axis xaxisLeft")
             .attr("transform", "translate(0," + main_height + ")")
             .call(main_xAxis)
             .selectAll("path")
             .attr("stroke", "#ccc")*/

            main.append("g")
                    .attr("class", "x axis xaxisLeft")
                    .attr("transform", "translate(0," + (main_height) + ")")
                    .call(main_xAxis2)
                    .selectAll("path")
                    .attr("stroke", "#ccc");


            main.append("g")
                    .attr("class", "y axis yaxisLeft")
                    .call(main_yAxisLeft)
                    .selectAll("text")
                    .attr("x", -20)

            main.selectAll("line")
                    .attr("stroke", "#ccc");

            main.selectAll(".tick")
                    .attr("stroke-dasharray", "3 3")
                    .attr("opacity", ".5");
            /*var main_xAxis1 =d3.svg.axis()
             .scale(main_x)
             .tickValues([data[0].time1, data[data.length - 1].time1])
             .tickSize(0)
             //                    .tickFormat(d3.time.format("%b"))
             .tickFormat(function(d,i) { return dataset[i]; })
             .orient("bottom");*/

            /*main.append("g")
             .attr("class", "x axis xaxisLeft2")
             .attr("transform", "translate(0," + (main_height+18) + ")")
             .call(main_xAxis1);*/

            /*var doms=[];
             doms.push(main_x.domain()[0]);

             var main_xAxis = d3.svg.axis()
             .tickFormat(d3.time.format('%b'))
             .tickValues(doms)
             .orient("bottom");

             main.append("g")
             .attr("class", "x axis xaxisLeft2")
             .attr("transform", "translate(0," + (main_height+25) + ")")
             .call(main_xAxis);*/

            /*main.append("g")
             .attr("class", "y axis yaxisLeft")
             .call(main_yAxisLeft)
             .selectAll("path")
             .attr("stroke", "#ccc");*/

            main.selectAll("line")
                    .attr("stroke", "#ccc");
            /*
             main.selectAll(".tick")
             .attr("stroke-dasharray", "3 3")
             .attr("opacity", ".5");*/


            var path1 = main.append("path")
//                    .attr("clip-path", "url(#clip)")
                    .attr("class", "line line0")
                    .attr("d", main_line0(data))
                    .style("stroke", "red")
                    .style("fill", "none")
                    .style('stroke-width', '1.2px');


            var diff1=0;
            var diff2=0;
            var textLabel = main.append('g')
                    .classed('labels-group', true);

            textLabel.selectAll('.labelText1')
                    .data(data)
                    .enter()
                    .append('text')
                    .classed('labelText1', true)
                    .attr({
                        'x': function (d, i) {
                            if (i == 0){
                                diff1= d.actual
                                return main_x(d.time1) + 5;
                            }
                            else{
                                diff2= d.actual
                                return main_x(d.time1) -100;
                            }
                        },
                        'y': function (d, i) {
                            return main_y(d.actual) - 28;
                        }
                    })
                    .text(function (d, i) {
                        return formatOutput0(d)
                    })

            textLabel
                    .selectAll('.labelText2')
                    .data(data)
                    .enter()
                    .append('text')
                    .classed('labelText2', true)
                    .attr({
                        'x': function (d, i) {
                            if (i == 0)
                                return main_x(d.time1) + 5;
                            else
                                return main_x(d.time1) -100;
                        },
                        'y': function (d, i) {
                            return main_y(d.actual) - 15;
                        }
                    })
                    .text(function (d, i) {
                        if (i == 0)   return "PMPM actual";
                        else  return "PMPM projected";
                    })

            /*textLabel
                    .selectAll('.labelTextDiff')
                    .data(data)
                    .enter()
                    .append('text').attr("text-anchor", "middle")
                    .classed('labelTextDiff', true)
                    .attr({
                        'x': function (d, i) {
                            if (i == 1){
                                return 150;
                            }
                        },
                        'y': function (d, i) {
                            if (i == 1)  return main_y(d.actual)+25 ;
                        }
                    })
                    .text(function (d, i) {
                        if (i == 1) return "Difference is "+Math.abs(diff2-diff1)+"";
                    })*/
//            var prefix = d3.formatPrefix(yT);
//            var amountYt="$"+prefix.scale(Math.round(yT)).toFixed(2)+" k";
//            $("#lineGraphRTest").html("<strong>"+yT+"</strong> Total ER spend projected <br><strong>"+Math.round(yPercentage)+" %</strong> of total spend projected");
//            $("#lineGraphRTest").html("<strong>"+yT+"</strong> Total ER spend projected");


            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y0")
                    .attr("cx", function (d) {
                        return main_x(d.time1);
                    })
                    .attr("cy", function (d) {
                        return main_y(d.actual);
                    })
                    .attr("r", 4);


            /*var totalLength = path1.node().getTotalLength();

             path1
             .attr("stroke-dasharray", totalLength + " " + totalLength)
             .attr("stroke-dashoffset", totalLength)
             .transition()
             .duration(600)
             .ease("linear")
             .attr("stroke-dashoffset", 0);




             var focus = main.append("g")
             .attr("class", "focus")
             .style("display", "block");

             focus.append("line")
             .attr("class", "x")
             .attr("y1", 0);

             focus.append("circle")
             .attr("class", "y0")
             .attr("r", 3);

             focus.append("text")
             .attr("class", "y0")
             .attr("dy", "-1em");



             main.append("rect")
             .attr("class", "overlay")
             .attr("fill","none")
             .attr("pointer-events","all")
             .attr("width", main_width)
             .attr("height", main_height)
             .on("mouseover", function() { focus.style("display", null); })
             .on("mouseout", function() {
             div.transition()
             .duration(500)
             .style("opacity", 0);
             focus.style("display", "none");
             })
             .on("mousemove", mousemove);

             function mousemove() {
             var x0 = main_x.invert(d3.mouse(this)[0]),
             i = bisectDate(data, x0, 1),
             d0 = data[i - 1],
             d1 = data[i],
             d = x0 - d0.time1 > d1.time1 - x0 ? d1 : d0;
             focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");
             focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.actual)+")").attr("y2",main_height-main_y(d.actual));

             div.transition()
             .duration(100)
             .style("opacity", 1);
             div .html("<span class='formatOutput0'>"+formatOutput0(d)+"</span><br><span class='formatOutput0'>"+formatOutput1(d)+"</span>")
             .style("left", (d3.event.pageX +30) + "px")
             .style("top", (d3.event.pageY - 120) + "px");
             }*/
        }
        ;

        function render2Data(da, max) {
            $("#lineGraph1").html("");

            /*var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
             main_width = 500 - main_margin.left - main_margin.right,
             main_height =300 - main_margin.top - main_margin.bottom;*/

            var main_margin = {top: 20, right: 60, bottom: 50, left: 90},
                    main_width = 500 - main_margin.left - main_margin.right,
                    main_height = 300 - main_margin.top - main_margin.bottom;
            var pharmacyClaims = da;
            //var benchMark = 3000;
//            var memberMonths = da.value;
            var newVal;
            var data = [];
            var parseDate4 = d3.time.format("%Y-%m-%d").parse;


            $.each(pharmacyClaims, function (key, val) {

                newVal = new Object();
                newVal.time1 = parseDate4(val['reportingDates']['reportingTo'])
                newVal.time2 = parseDate4(val['reportingDates']['reportingFrom'])
//                newVal.time1=parseDate4(val['year'])
                newVal.actual = Math.round(val['pharmacyPaidAmountPmpm'])
                newVal.memberDrill = val['members_drill'];

                newVal.expected = Math.round(val['benchmark']) || 0;
                data.push(newVal);
            });

            var formatDate = d3.time.format("%d-%b-%y"),
                    parseDate = formatDate.parse,
                    bisectDate = d3.bisector(function (d) {
                        return d.time1;
                    }).left,
                    formatOutput0 = function (d) {
                        return d3.format("$,")(Math.round(d.actual)) + " PMPM actual";
                    },
                    formatOutput1 = function (d) {
                        return d3.format("$,")(Math.round(d.expected ))+ " PMPM benchmark";
                    };

            var main_x = d3.time.scale()
                    .range([0, main_width]);

            var main_y = d3.scale.linear()
                    .range([main_height, 0]);


            var main_yAxisLeft = d3.svg.axis()
                    .scale(main_y)
                    .ticks(5)
                    .tickSize(-main_width)
                    .outerTickSize(0)
                    .orient("left");

            var main_line0 = d3.svg.line()
                //.interpolate("basis")
                    .x(function (d) {
                        return main_x(d.time1);
                    })
                    .y(function (d) {
                        return main_y(d.actual);
                    });

            var main_line1 = d3.svg.line()
                //.interpolate("basis")
                    .x(function (d) {
                        return main_x(d.time1);
                    })
                    .y(function (d) {
                        return main_y(d.expected);
                    });

            var svg = d3.select("#lineGraph1").append("svg")
                    .attr("width", main_width + main_margin.left + main_margin.right)
                    .attr("height", main_height + main_margin.top + main_margin.bottom);

            svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("width", main_width)
                    .attr("height", main_height);

            var main = svg.append("g")
                    .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");


            %{--d3.csv(paths, function(error, data) {
                data.forEach(function(d) {
                    d.time1 = parseDate(d.time1);
                    d.expected = +d.expected;
                    d.actual = +d.actual;
                });--}%


            data.sort(function (a, b) {
                return a.time1 - b.time1;
            });


            var value1 = Math.round(data[data.length - 1].actual);
            var value2 = Math.round(data[data.length - 1].expected);


            main_x.domain([data[0].time1, data[data.length - 1].time1]);
            var array_y0 = d3.extent(data, function (d) {
                return d.actual;
            });
            var array_y1 = d3.extent(data, function (d) {
                return d.expected;
            });
            array_y0 = array_y0.concat(array_y1);
//            main_y.domain([0,d3.max(array_y0)]);
            main_y.domain([0, max]);

//            var dateTicks=data.map(function(d){return (d3.time.format("%m %Y")(d.time1)+" to "+d3.time.format("%m %Y")(d.time2)))});
            var dateTicks = data.map(function (d) {
                return d.time1
            });

            /*main.append("g")
             .attr("class", "x axis xaxisLeft")
             .attr("transform", "translate(0," + main_height + ")")
             .call(main_xAxis);*/

            var main_xAxis2 = d3.svg.axis()
                    .scale(main_x)
//                    .ticks(d3.time.years, 1)
//                    .ticks(d3.time.month, 9)
//                    .tickValues(data.time1)
                    .tickValues(dateTicks)
//                    .tickFormat(d3.time.format("%b-%Y"))
                    .tickFormat(formatDates)
                    .tickSize(5, 0)
                    .orient("bottom");


            main.append("g")
                    .attr("class", "x axis xaxisLeft")
                    .attr("transform", "translate(0," + (main_height) + ")")
                    .call(main_xAxis2);


            /*var main_xAxis1 =d3.svg.axis()
             .scale(main_x)
             .tickValues([data[data.length - 1].time1])
             .tickSize(0)
             //                    .tickFormat(d3.time.format("%b"))
             .tickFormat(function(d,i) { return dataset[i]; })
             .orient("bottom");*/

            /* main.append("g")
             .attr("class", "x axis xaxisLeft2")
             .attr("transform", "translate(0," + (main_height+18) + ")")
             .call(main_xAxis1);*/


            %{--.append("text")
            .attr("class", "text_x")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (main_width/2) +",30)");  // centre below axis
        //.text("TIME");--}%

            main.append("g")
                    .attr("class", "y axis yaxisLeft")
                    .call(main_yAxisLeft)
                    .selectAll("text")
                    .attr("x", -20)

            main.selectAll("line")
                    .attr("stroke", "#ccc");

            main.selectAll(".tick")
                    .attr("stroke-dasharray", "3 3")
                    .attr("opacity", ".5");

            main.append("text")
                    .attr("class", "text_y")
                    .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                    .attr("transform", "translate(" + (-50) + "," + (main_height / 2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                    .text("Costs ($ PMPM)");


            var path1 = main.append("path")
//                    .attr("clip-path", "url(#clip)")
                    .attr("class", "line line0")
                    .attr("d", main_line0(data))
                    .style("stroke", "red")
                    .style("fill", "none")
                    .style('stroke-width', '1.2px');


            var path2 = main.append("path")
//                    .attr("clip-path", "url(#clip)")
                    .attr("class", "line line1")
                    .attr("d", main_line1(data))
                    .style('stroke', 'gray')
                    .style("fill", "none")
                    .style('stroke-width', '1.2px');


            var totalLength = path1.node().getTotalLength();

            path1
                    .attr("stroke-dasharray", totalLength + " " + totalLength)
                    .attr("stroke-dashoffset", totalLength)
                    .transition()
                    .duration(600)
                    .ease("linear")
                    .attr("stroke-dashoffset", 0)
                    .each('end', function () {
                        //alert(value1);
//                        $("#actualValue").html("<span class='label-value'>$"+value1+"</span><span class='label-unit'>/1000</span><span class='label-title'>actual</span>");
                    });

            path2
                    .attr("stroke-dasharray", totalLength + " " + totalLength)
                    .attr("stroke-dashoffset", totalLength)
                    .transition()
                    .duration(600)
                    .ease("linear")
                    .attr("stroke-dashoffset", 0)
                    .each('end', function () {
//                        $("#expectedValue").html("<span class='label-value'>$"+value2+"</span><span class='label-unit'>/1000</span><span class='label-title'>benchmark</span>");
//                        $(".graph1-label").fadeIn();
                    });


            var focus = main.append("g")
                    .attr("class", "focus")
                    .style("display", "block");

            focus.append("line")
                    .attr("class", "x")
                    .attr("y1", 0);

            /*focus.append("circle")
                    .attr("class", "y0")
                    .attr("r", 3);

            focus.append("text")
                    .attr("class", "y0")
                    .attr("dy", "-1em");

            focus.append("circle")
                    .attr("class", "y1")
                    .attr("r", 3);

            focus.append("text")
                    .attr("class", "y1")
                    .attr("dy", "-1em");*/

            focus.style("visibility", "hidden");


            main.append("rect")
                    .attr("class", "overlay")
                    .attr("fill", "none")
                    .attr("cursor", "default")
                    .attr("pointer-events", "all")
                    .attr("width", main_width)
                    .attr("height", main_height)
                    .on("mouseover", function () {
                        focus.style("display", null);
                    })
                    .on("mouseout", function () {
                        div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        focus.style("display", "none");
                    })
                    .on("mousemove", mousemove);


            main.on("mouseover", function () {
                focus.style("display", null);
            }).on("mouseout", function () {
                        div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        focus.style("display", "none");
                    }).on("mousemove", mousemove);

            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y1")
                    .attr("cx", function (d) {
                        return main_x(d.time1);
                    })
                    .attr("cy", function (d) {
                        return main_y(d.expected);
                    })
                    .attr("r", 4);

            var outerCircle=main.append("g").attr("class", "outerCircle");

            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append("svg:a")
                    .attr("data-href", function(d,i){
                        if(d.actual>0)
                        return createZ5Link(d.memberDrill);
                    })
                    .attr("onclick", "postRequestForGet(this);return false;")
                    .append('circle')
                    .attr("class", "y0")
                    .attr("cx", function (d) {
                        return main_x(d.time1);
                    })
                    .attr("cy", function (d) {
                        return main_y(d.actual);
                    })
                    .attr("r", 4)
                    .on("mouseenter", function(d,i) {
                        d3.select(this).attr("r", 10)
                        outerCircle.append("circle").attr({
                            cx: main_x(d.time1),
                            cy: main_y(d.actual),
                            r: 12,
                            fill: "none",
                            shapeRendering:"optimizeSpeed",
                            stroke:"red",
                            strokeWidth:"2px",
                            class:"outer-circle"
                        });

                    }).on("mouseout", function(d) {
                        d3.select(this).attr("r", 4)
                        d3.select("g.outerCircle").remove();
                        outerCircle=main.append("g").attr("class", "outerCircle");
                    })



            function mousemove() {
                focus.style("visibility", "visible");
                var x0 = main_x.invert(d3.mouse(this)[0]),
                        i = bisectDate(data, x0, 1),
                        d0 = data[i - 1],
                        d1 = data[i],
                        d = x0 - d0.time1 > d1.time1 - x0 ? d1 : d0;
//                focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");
//                focus.select("text.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");//.text(formatOutput0(d));
//                focus.select("circle.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");
//                focus.select("text.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");//.text(formatOutput1(d));
                focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected > d.actual ? d.expected : d.actual) + ")").attr("y2", main_height - main_y(d.expected > d.actual ? d.expected : d.actual));
                //focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));

                div.transition()
                        .duration(100)
                        .style("opacity", .9);
                div.html("<span class='formatOutput0'>" + formatOutput0(d) + "</span><br><br><span class='formatOutput1'>" + formatOutput1(d) + "</span>")
                        .style("left", (d3.event.pageX + 30) + "px")
                        .style("top", (d3.event.pageY - 120) + "px");

                //focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.actual) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.expected) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(0, " + main_y(d.expected) + ")").attr("x1", main_x(d.time1));
            }

        }
        ;

        function wrap(text, width) {
            text.each(function() {
                var text = d3.select(this),
                        words = text.text().split(/\s+/).reverse(),
                        word,
                        line = [],
                        lineNumber = 0,
                        lineHeight = 1.1, // ems
                        y = text.attr("y"),
                        dy = parseFloat(text.attr("dy")),
                        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
                while (word = words.pop()) {
                    line.push(word);
                    tspan.text(line.join(" "));
                    if (tspan.node().getComputedTextLength() > width) {
                        line.pop();
                        tspan.text(line.join(" "));
                        line = [word];
                        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                    }
                }
            });
        }




        function renderData(data,ele) {
            $("#"+ele).html("")
            var margin = {top: 20, right: 10, bottom: 40, left: 20},
                    width = 620 - margin.left - margin.right,
                    height = 350 - margin.top - margin.bottom;

            var x = d3.scale.ordinal()
                    .rangeRoundBands([0, 300], .3);

            var y = d3.scale.linear()
                    .rangeRound([height, 0]);

            var color = d3.scale.ordinal()
                    .range(["#034f96", "#b53322"]);

            var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

            var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickFormat(d3.format(".2s"));

            var svg = d3.select("#"+ele).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + -25 + "," + margin.top + ")");

            color.domain(d3.keys(data[0]).filter(function (key) {
                return (key !== "year" && key !== "percentOfPaidAmtForSavingsScope" && key !== "reportingDates" && key !== "members_drill" && key !== "members_drill_percent");
            }));

            data.forEach(function (d) {
                var y0 = 0;
                d.ages = color.domain().map(function (name) {
//                    console.log(name)
                    return {name: name, y0: y0, y1: y0 += +d[name]};
                });
                d.total = d.ages[d.ages.length - 1].y1;
            });



            data.sort(function (a, b) {
                return b.order - a.order;
            });

            x.domain(data.map(function (d) {
                return d.year;
            }));
            var yearValues = data.map(function (d) {
                return d.year;
            });
            y.domain([0, d3.max(data, function (d) {
                return d.total * 1.2;
            })]);

            svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis)
                    .selectAll(".tick text")
                    .call(wrap, x.rangeBand());

            svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Population");

            var state = svg.selectAll(".state")
                    .data(data)
                    .enter().append("g")
                    .attr("class", "g")
                    .attr("transform", function (d) {
                        return "translate(" + x(d.year) + ",0)";
                    });

            state.selectAll("rect")
                    .data(function (d) {
                        return d.ages;
                    })
                    .enter().append("rect")
                    .attr("width", x.rangeBand())
                    .attr("y", function (d) {
                        return y(d.y1);
                    })
                    .attr("height", function (d) {
                        return y(d.y0) - y(d.y1);
                    })
//                    .style("fill", function(d) { return color(d.name); });
                /*   .style("fill",function(d) {
                 return color(d.name);

                 })*/
                    .attr("class", function (d) {
                        if (color(d.name) == "#034f96")
                            return "dbar"
                        else
                            return "hbar"

                    });

            var legend = svg.selectAll(".legend")
                    .data(color.domain().slice())
                    .enter().append("g")
                    .attr("class", "legend")
                    .attr("transform", function (d, i) {
                        return "translate(0," + (i * 22 - 14) + ")";
                    });

            var textAvoidable,rectPosition
            if(ele=="barGraph1"){
                textAvoidable = "Total spend on brand when generic available"
                rectPosition=10
            }
            else if(ele=="barGraph2"){
                textAvoidable = "Total filled at retail when mail order available"
                rectPosition=10
            }
            else if(ele=="barGraph3"){
//                textAvoidable = "of total spend on Speciality Medication"
                textAvoidable = "Speciality "
                rectPosition=80
            }


            legend.append("rect")
                    .attr("x", width - rectPosition)
                    .attr("width", 18)
                    .attr("height", 18)
                    .attr("class", function (d) {
                        if (d == "paidAmtForSavingsScope")
                            return "hbar"
                        else
                            return "dbar"

                    });
                    /*.style("fill", function (d) {
                        if (d == "paidAmtForSavingsScope")
                            d = "url(#pattern-stripe)"
                        else
                            d = "#034f96"
                        return d;
                    });*/
//                    .style("fill", color);

            legend.append("text")
                    .attr("x", width - 305)
                    .attr("y", 9)
                    .attr("dy", ".35em")
                    .style("text-anchor", "start")
//                    .style("fill",color)
                    .style("fill", function (d) {
                        if (d == "paidAmtForSavingsScope")
                            d = "#b53322"
                        else
                            d = "#034f96"
                        return d;
                    })
                    .attr("font-weight", "bold")
                    .text(function (d) {
                        if (d == "paidAmtForSavingsScope"){
                            d=textAvoidable
                        }
                        else
                            d = "Total Pharmacy spend"
                        return d;
                    });

            var textAppend = svg.selectAll(".text").data(data).enter()
                    .append("svg:a")
                    .attr("data-href", function(d,i){
                        if(d.percentOfPaidAmtForSavingsScope.toFixed(1)>0)
                            return createZ5Link(d.members_drill_percent);

                    })
                    .attr("onclick", "postRequestForGet(this);return false;")
                    .append("text")
                    .attr("class", "text")
                    .attr("transform", function (d, i) {
                        return "translate(" + ((i * 90) + 28) + "," + 24 + ")";
                    })
                    .text(function (d, i) {
//                        return d.percentOfPaidAmtForSavingsScope
                        return d.percentOfPaidAmtForSavingsScope.toFixed(1).replace(/\.0+$/,'') + "%"
                    }).attr("font-family", "sans-serif")
                    .attr("font-size", "13px")
                    .attr("font-weight", "bold")
                    .attr("fill", "#b53322");

            var textAppend2 = svg.selectAll(".text1").data(data).enter()
                    .append("svg:a")
                    .attr("data-href", function(d,i){
                        if(Math.round(d.paidAmtForTotal)>0)
                        return createZ5Link(d.members_drill);
                    })
                    .attr("onclick", "postRequestForGet(this);return false;")
                    .append("text")
                    .attr("class", "text1")
                    .attr("transform", function (d, i) {
                        return "translate(" + ((i * 90) + 30) + "," + 0 + ")";
                    })
                    .text(function (d, i) {
                        return d3.format("$,")(Math.round(d.paidAmtForTotal));
                        /*Math.round(d.totalPaidAmountForEr)*/
                    }).attr("font-family", "sans-serif")
                    .attr("font-size", "13px")
                    .attr("font-weight", "bold")
                    .attr("fill", "#034f96");
        };


        function custom_sort(a, b) {
            return a.date - b.date;
        }

        var div = d3.select(".contentWrapper").append("div")
                .attr("class", "tooltip")
                .style("opacity", 0);

        function formatDates(d) {
            var oldDate=new Date(d);
            var newDate2=oldDate;
            newDate2.setFullYear(newDate2.getFullYear() - 1);
            var leapCondition=false;

            if((newDate2.getFullYear() % 400 == 0) || ((newDate2.getFullYear() % 4 == 0) && (newDate2.getFullYear() % 100 != 0))){
                leapCondition=true;
            }


            if((newDate2.getMonth()+1)==2 && leapCondition){
                leapCondition=true;
            }else{
                leapCondition=false;
            }

            if(leapCondition){
                newDate2.setDate(newDate2.getDate() +2);

            }else{
                newDate2.setDate(newDate2.getDate() +1);
            }

            var dateString= (d3.time.format("%b %Y")(newDate2).toString()+" - "+d3.time.format("%b %Y")(d).toString())
            return dateString;
        }

        $(function(){
            <g:if test="${!isEditable}">
            var parseEndDate = d3.time.format("%Y-%m-%d").parse;
            refreshReport()
            %{--renderData(${dataPh},"barGraph1");--}%
             %{--renderData(${dataPh4},"barGraph2");--}%
             %{--renderData(${dataPh5},"barGraph3");--}%
             %{--render2Data(${dataPh3}, ${getMax});--}%
             %{--render4Data(${lineGraphR}, ${yPercentage}, "${yTotal}", ${getMax});--}%
            </g:if>
        });

        function generatePdf(){
            return;
//            $.each(['lineGraph1',"lineGraphR","barGraph1","barGraph2","barGraph3"],function(i,v){
            $.each(['lineGraph1'],function(i,v){
//            $.each(['barGraph3'],function(i,v){
                var count=i+1;
                var html,imgsrc,canvas,context,image,canvasdata;
                html = d3.select("#"+v+" svg")
                        .attr("version", 1.1)
                        .attr("xmlns", "http://www.w3.org/2000/svg")
                        .node().parentNode.innerHTML;


                imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
//                img = '<img src="'+imgsrc+'">';
//                canvas = $("#myCanvas"+count)[0]
                canvas = d3.select('body').append('canvas').node();
                canvas.width  = 800;
                canvas.height = 400;
                context = canvas.getContext("2d");
                image = new Image;
                image.src = imgsrc;
//                console.log(imgsrc)
                context.drawImage(image, 0, 0);
                canvasdata= canvas.toDataURL("image/png");
                $("#imagePath_"+count).val(canvasdata);
                canvas.remove();
//              console.log(canvasdata)
            });
            $("#excludeFilter").val($("#includeOn").val());
            $("#paidFiler").val($("#reportingBasis1").val());
            $("#rangeFiler").val($("#range1").val());
            $("#exportPdfForm").submit();
        }




    </script>





    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">
    <div style="display: none;">
        <canvas id="myCanvas1" width="800" height="400" style="border:1px solid #000000;"></canvas>
        <canvas id="myCanvas2" width="700" height="400" style="border:1px solid #000000;"></canvas>
    </div>
</body>
<%
    //        split page
%>
</html>