%{--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">--}%
<%@page defaultCodec="none" %>
<html>
<head>
    %{--<link rel="stylesheet" type="text/css" href="${grailsApplication.config.grails.serverURL}/css/print.css" media="all" />--}%
    <style type="text/css" media="all">


        .hbar {
            fill: url(#pattern-stripe) !important;
        }

        .bar {
            fill: steelblue;
        }

        .lineColor {
            fill: none;
            stroke: #1c94c4;
            stroke-width: 1px;
            stroke-dasharray: 6 3
        }

        .dbar {
            fill: #034f96;
        }

        .x.axis path {
            /*display: none;*/
        }
        /* Typography */
        .graph1 {
            width: 550px;
            height: 350px;
        }

        .graph1-label {
            width: auto;
            margin-top: 25px;
        }

        .graph1-label {
            position: relative;
            padding-left: 35px;
        }

        .graph1-label .legend {
            position: absolute;
            left: 0;
            width: 25px;
            margin-top: 20px;
        }

        .legend-red, .legend-gray, .legend-blue {
            height: 5px;
            line-height: 2px;
        }

        .legend-red {
            background: #ff0000;
        }

        .legend-gray {
            background: #ccc;
        }

        img.heading-icon {
            width: 35px;
            margin-right: 10px;
        }

        .z5-table table {
            width: 100%;
            border-collapse: collapse;
        }

        .z5-table table th {
            border-bottom: 1px solid #999;
        }

        .z5-table table td {
            border-bottom: 1px solid #ccc;
        }

        /* Fixers */
        .hr, .sep-border {
            border-bottom: 1px solid #ccc;
            margin-bottom: 15px;
        }

        .print-header {
            margin-bottom: 10px;
            border-bottom: 1px solid #ccc;
            padding-bottom: 20px;
        }

    </style>
</head>
<body>



    <div id="contentWrapper" class="contentWrapper">
        <div class="container double-arrow-back-top-container">



            <div class="col-md-6 double-arrow-back-top">
                <g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
                    <span class="isEditable"><g:pdfMessage code="hPharmacy.top.navigation"/></span>
                </g:link>
            </div>

            <div class="col-md-6" style="display: none;" id="editContent">

            </div>
            <%
                //        split page
            %>

            <div class="clearfix"></div>

            <div class="col-md-12">
                <h2 class="mb15">
                    <g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon"/>
                    <span class="isEditable"><g:pdfMessage code="hPharmacy.container1.graph1.title"/></span></h2>

                <h3 class="isEditable"><g:pdfMessage code="hPharmacy.container1.graph1.subTitle"/></h3>
            </div>



            <div class="clearfix"></div>

            <div class="sep-border mb15"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:pdfMessage code="hPharmacy.container1.graph1.title2"/></h1>
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:pdfMessage
                        code="hPharmacy.explore.population"/></span>
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('rxHistoricPredictive')"
                   class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:pdfMessage code="hPharmacy.export.csv"/></span>
                </a>
            </div>


            <div class="col-md-12">
                <h6 class="has-filter">
                <span id="graph1Filter">

                        Compare
                        claims to national benchmarks
                        cases over
                        per year</span>

                    <span class="f-right">
                        <span class=" isEditable"><g:pdfMessage code="hPharmacy.container1.methodology"/></span>

                        <span class="fa fa-chevron-right " ></span>
                    </span>
                </h6>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-7">
                    <div class="bdr-right">
                        <div id="lineGraph1title"></div>
                        <div id="lineGraph1">
                            <rendering:inlineJpeg bytes="${image.image1}" class="lineGraph1"/>
                        </div>
                    </div>
            </div>

            <div class="col-md-5">
                    <div id="lineGraphRtitle" class="col-md-12"></div>
                    <div id="lineGraphR" class="col-md-8">
                        <rendering:inlineJpeg bytes="${image.image2}" class="lineGraph1"/>

                    </div>
            </div>

            <div class="clearfix"></div>

            <div class="sep-border mb25 pt15"></div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:pdfMessage code="hPharmacy.container1.table1.head"/></h1>
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)"  class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:pdfMessage
                        code="hPharmacy.explore.population"/></span>
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('barGraph','rxBrandGenericCost')"
                   class=" text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:pdfMessage code="hPharmacy.export.csv"/></span>
                </a>

                %{--<a href="javascript:void(0)"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:pdfMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
            </div>

            <div class="col-md-12">
                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:pdfMessage code="highPharmacy.container1.methodology"/>
                            <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>

                        </span>

                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-7">
                <div class="bdr-right-dashed">
                    <div class="sub-title"><span class="isEditable"><g:pdfMessage
                            code="hPharmacy.container1.table1.subhead"/></span>
                    </div>

                    <div id="barGraph1"></div>
                </div>

                <div class="clearfix"></div>

                <div>
                    <g:link controller="highPharmacyCost" action="drugsDetails" params="[type:'brand']"  class="see-dtl-utilization">
                        <span class=" isEditable"><g:pdfMessage code="hPharmacy.detailedUtilization"/></span>

                    </g:link>
                </div>
            </div>

            <%
                //        split page
            %>
            <div class="col-md-5">


                <p><span class=" isEditable"><g:pdfMessage code="hPharmacy.leftbar2.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:pdfMessage
                        code="hPharmacy.leftbar2.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="javascript:void(0)"  class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"> <g:pdfMessage code="hPharmacy.evaluate"/></a>
                </div>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:pdfMessage
                        code="hPharmacy.leftbar2.list2"/></div>

                <div class="col-md-4 no-pad-r text-right ">
                    <a href="javascript:void(0)"  class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"> <g:pdfMessage code="hPharmacy.evaluate"/></a>
                </div>




            </div>

            <div class="clearfix"></div>

            <div class="sep-border-dashed mb25 pt15"></div>
            <div class="clearfix"></div>

            <div class="col-md-6">
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:pdfMessage
                        code="hPharmacy.explore.population"/></span>
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('barGraph','rxMailRetailCost')"
                   class=" text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:pdfMessage code="hPharmacy.export.csv"/></span>
                </a>

                %{--<a href="javascript:void(0)"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:pdfMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
            </div>
            <div class="col-md-12">
                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:pdfMessage code="highPharmacy.container1.methodology"/>
                            <span class="fa fa-chevron-right " onclick="displayData('methodology3')"></span>

                        </span>

                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="col-md-7">
                <div class="bdr-right-dashed">
                    %{--<h1 class="isEditable"><g:pdfMessage code="hPharmacy.container1.table1.head"/></h1>--}%
                    <div class="sub-title"><span class=" isEditable"><g:pdfMessage
                            code="hPharmacy.container2.table1.subhead"/></span>
                    </div>

                    <div id="barGraph2"></div>
                </div>

                <div class="clearfix"></div>

                <div>
                    <g:link controller="highPharmacyCost" action="drugsDetails" params="[type:'mail']" class="see-dtl-utilization">
                        <span class=" isEditable"><g:pdfMessage code="hPharmacy.detailedUtilization"/></span>

                    </g:link>
                </div>
            </div>

            <%
                //        split page
            %>
            <div class="col-md-5">


                <p><span class=" isEditable"><g:pdfMessage code="hPharmacy.leftbar3.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:pdfMessage
                        code="hPharmacy.leftbar3.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="javascript:void(0)"  class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable">
                        <g:pdfMessage code="hPharmacy.evaluate"/></a></div>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:pdfMessage
                        code="hPharmacy.leftbar3.list2"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="javascript:void(0)"  class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable">
                        <g:pdfMessage code="hPharmacy.evaluate"/></a></div>

            </div>

            <div class="clearfix"></div>

            <div class="sep-border-dashed mb25 pt15"></div>
            <div class="col-md-6">
            </div>

            <div class="col-md-6 text-right">
                <a href="javascript:void(0)" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:pdfMessage
                        code="hPharmacy.explore.population"/></span>
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('barGraph','rxSpecialityCost')"
                   class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:pdfMessage code="hPharmacy.export.csv"/></span>
                </a>

                %{--<a href="javascript:void(0)"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:pdfMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
            </div>
            <div class="col-md-12">
                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:pdfMessage code="highPharmacy.container1.methodology"/>
                            <span class="fa fa-chevron-right " onclick="displayData('methodology4')"></span>

                        </span>

                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="col-md-7">
                <div class="bdr-right-dashed">
                    <div class="sub-title"><span class=" isEditable"><g:pdfMessage
                            code="hPharmacy.container4.table1.subhead"/></span>
                    </div>

                    <div id="barGraph3"></div>
                </div>

                <div class="clearfix"></div>

                <div>
                    <g:link controller="highPharmacyCost" action="drugsDetails"   class="see-dtl-utilization">
                        <span class=" isEditable"><g:pdfMessage code="hPharmacy.detailedUtilization"/></span>

                    </g:link>
                </div>
            </div>

            <%
                //        split page
            %>
            <div class="col-md-5">
                <p><span class=" isEditable"><g:pdfMessage code="hPharmacy.leftbar4.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:pdfMessage
                        code="hPharmacy.leftbar4.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="javascript:void(0)" class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable">
                        <g:pdfMessage code="hPharmacy.evaluate"/></a></div>



            </div>

        </div>
        </div>


</body>
</html>