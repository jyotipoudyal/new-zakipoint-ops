<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://z5.deerwalk.local:9090/z5/css/layout.css" media="all" />
    <style>
    @page {

    size: 13.5in 19.2in;
    /*@bottom-center { content: element(footer);}*/
    /*@top-center { content: element(header); }*/
    }

    div.break {
        page-break-after:always;
    }
    </style>
</head>
<body>
<div class="mainWrapper">


<div  id="contentWrapper" class="contentWrapper">

    <div class="container">        
        <div class="clearfix"></div>
        <div class="col-md-10">
            <h2 class="mb15">
                <g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon" />
                <span class="isEditable"><g:pdfMessage code="highPharmacy.container1.graph1.title"/> </span></h2>
            <h3 class="isEditable"><g:pdfMessage code="highPharmacy.container1.graph1.subTitle"/></h3>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="hr mb15"></div>
        </div>
        <div class="col-md-6 mb25">
            <h1 class="isEditable"><g:pdfMessage code="highPharmacy.container1.graph1.title2"/></h1>
        </div>


        <div class="col-md-12"><h3 id="graph1Title">Current pharmacy costs are $291 per member per month more than expected</h3>

        </div>

           <div id="graph1" class="graph1"><rendering:inlineJpeg bytes="${image}" /></div>

    </div>
<div style="margin-top: 500px;"></div>

    <div class="container">
        <div class="sep-border"></div>
        <div class="col-md-6">
            <h3 class="isEditable"><g:pdfMessage code="highPharmacy.container2.title1" /></h3>
            <p class="isEditable"><g:pdfMessage code="highPharmacy.container2.subTitle1" /></p>
            <h3 class="isEditable"><g:pdfMessage code="highPharmacy.container2.title2" /></h3>

            <h4 class="color-blue m150"> <g:img uri="/images/icon-high-cost-claimants.png" class="heading-icon" /> <span class="isEditable">Highcost <strong>Claimants</strong> </span></h4>
            <h4 class="color-blue m150"> <g:img uri="/images/ineffieicnt-network-use.png" class="heading-icon" /> <span class="isEditable"><g:pdfMessage code="highPharmacy.container2.title2.subHeads1"/> </span></h4>
        </div>
        <div class="col-md-6">
            <h3 class="isEditable"><g:pdfMessage code="highPharmacy.container2.title3" /></h3>
            <span class="bulb-lists color-blue isEditable">
                <g:pdfMessage code="highPharmacy.container2.title3.subHeads1" />
            </span>
        </div>
    </div>


    <div class="break"/>

<div class="container">


    <div class="sep-border"></div>
    <div class="col-md-6 mb25">
        <h1 class="isEditable"><g:editMessage code="highPharmacy.container1.table1.mTitle1"/></h1>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12">

        <h3 class="mb25 isEditable" id="graph2Title"  >
            Optimize pharmacy <strong>benefit design</strong> for a total of <strong id="tableHead1"> <g:formatNumber number="${total.totalgenericBrandSavings+total.totalmailRetailSavings}" type="number" maxFractionDigits="2" /></strong>&nbsp;or more in&nbsp;potential savings
        %{--<g:editMessage code="highPharmacy.container3.table1.title" args="[formatNumber(number: total.totalgenericBrandSavings+total.totalmailRetailSavings+total.totalPaidAmountPmpm, type: number,maxFractionDigits: '2')  ]"/>--}%
        </h3>

        <div>
            <div id="dataHolder1">
                <div class="z5-table">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                        <thead>
                        <tr>
                            <th class="text-left isEditable" ><g:pdfMessage code="highPharmacy.container3.table1.head1"/></th>
                            <th class="text-left isEditable" ><g:pdfMessage code="highPharmacy.container3.table1.head2"/></th>
                            <th class="text-left isEditable" ><g:pdfMessage code="highPharmacy.container3.table1.head3"/></th>
                            <th class="text-left isEditable" ><g:pdfMessage code="highPharmacy.container3.table1.head7"/></th>
                            <th class="text-left isEditable" ><g:pdfMessage code="highPharmacy.container3.table1.head5"/></th>
                            <th class="text-left isEditable" ><g:pdfMessage code="highPharmacy.container3.table1.head4"/></th>
                            <th class="text-left isEditable" ><g:pdfMessage code="highPharmacy.container3.table1.head6"/></th>
                            <th></th>
                        </tr>

                        <tr>
                            <th class="text-left"><strong>Totals</strong></th>
                            <th class="text-center"><strong>
                                <g:formatZ5Currency number="${total.totalgenericBrandSavings}" type="number" maxFractionDigits="2"/>
                            </strong></th>
                            <th class="text-center"><strong>
                                <g:formatZ5Currency number="${total.totalmailRetailSavings}" type="number" maxFractionDigits="2"/>
                            </strong></th>

                            <th class="text-center"><strong>
                                <g:formatZ5Currency number="${total.totalPaidAmountPmpm}" type="number" maxFractionDigits="2"/>
                                PMPM</strong></th>
                            <th class="text-center"><strong></strong></th>
                            <th class="text-center"><strong>
                                <g:formatZ5Currency number="${total.totalAnnualCost}" type="number" maxFractionDigits="2"/>
                            </strong></th>
                            <th class="text-right"><strong>
                                <g:formatNumber number="${total.totalPrescriptions}" type="number" maxFractionDigits="2"/> prescriptions
                                <g:formatNumber number="${total.totalPopulationSize}" type="number" maxFractionDigits="2"/> members
                            </strong></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:if test="${data}">

                            <g:each in="${data}" var="data1" status="i">
                                <tr>
                                    <td>
                                        <g:if test="${data1.genericOrBrand == "brand"}">
                                            <strong style="text-transform: capitalize">${data1.drugName}</strong>

                                            <span style="text-transform: lowercase">${data1.equvalentGenericDrug}</span>

                                        </g:if><g:else>
                                        <strong style="text-transform: lowercase">${data1.drugName}</strong>
                                        <span style="text-transform: lowercase">${data1.equvalentGenericDrug}</span>
                                    </g:else>

                                    </td>
                                    <td class="text-center">
                                        <%
                                            def max,min,val;
                                            max=total.totalgenericBrandSavings;
                                            min=data1.genericBrandSavings
                                            try {
                                                val=(min/max)*100;
                                            } catch (ArithmeticException e) {
                                                val=0
                                            }
                                        %>

                                        <div style="height:12px;width:100px;background-color: #fff">
                                            <div style="height:12px;width:${val}px;background-color: black"></div>
                                        </div>
                                        <g:formatZ5Currency number="${data1.genericBrandSavings}" type="number" maxFractionDigits="2"/></td>
                                    <td class="text-center">
                                        <%
                                            max=total.totalmailRetailSavings;
                                            min=data1.mailRetailSavings
                                            try {
                                                val=(min/max)*100;
                                            } catch (ArithmeticException e) {
                                                val=0
                                            }
                                        %>

                                        <div style="height:12px;width:100px;background-color: #fff">
                                            <div style="height:12px;width:${val}px;background-color: black"></div>
                                        </div>

                                        <g:formatZ5Currency number="${data1.mailRetailSavings}" type="number" maxFractionDigits="2"/>
                                    </td>
                                    <td class="text-center">
                                        <g:formatZ5Currency number="${data1.paidAmountPmpm}" type="number" maxFractionDigits="2"/> PMPM
                                    </td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                        <g:formatZ5Currency number="${data1.annualCost}" type="number" maxFractionDigits="2"/>
                                    </td>
                                    <td class="text-right">
                                        <g:formatNumber number="${data1.prescriptions}" type="number" maxFractionDigits="2"/> prescriptions
                                        <g:formatNumber number="${data1.populationSize}" type="number" maxFractionDigits="2"/> members
                                    </td>
                                    <td><a href="#" class="iconExplorePopulation">
                                        <rendering:inlineJpeg bytes="${paidBytes}" />
                                    </a></td>
                                </tr>
                            </g:each>
                        </g:if><g:else>
                            <tr>
                                <td colspan="8">
                                    No records to display.
                                </td>
                            </tr>
                        </g:else>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

    <div class="break"/>

<div class="container">
    <div class="sep-border"></div>
    <div class="col-md-6 mb25">
        <h1 class="isEditable"><g:pdfMessage code="highPharmacy.container1.table2.mTitle1"/></h1>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12">

        <h3 class="mb25 isEditable" id="graph3Title">
            Implement <strong>specialty pharmacy</strong> cost management strategies for a total of <strong  id="tableHead2"><g:formatNumber number="${total1.totalgenericBrandSavings+total1.totalmailRetailSavings}" type="number" maxFractionDigits="2" /> </strong> in potential savings
        </h3>
        
        <div>
            <div id="dataHolder2">
                <div class="z5-table">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                        <thead>
                        <tr>
                            <th class="text-left" style="width: 200px;">Drug Name</th>
                            <th class="text-left isEditable" style="width: 220px;"><g:pdfMessage code="highPharmacy.container3.table1.head3"/></th>
                            <th class="text-left isEditable" style="width: 150px;"><g:pdfMessage code="highPharmacy.container3.table1.head7"/></th>
                            <th class="text-left isEditable"><g:pdfMessage code="highPharmacy.container3.table1.head5"/></th>
                            <th class="text-left isEditable" style="width: 150px;"><g:pdfMessage code="highPharmacy.container3.table1.head4"/></th>
                            <th class="text-left isEditable"><g:pdfMessage code="highPharmacy.container3.table1.head6"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th class="text-left"><strong>Totals</strong></th>
                            <th class="text-center"><strong><g:formatZ5Currency number="${total1.totalmailRetailSavings}" type="number" maxFractionDigits="2" /></strong></th>
                            <th class="text-center"><strong><g:formatZ5Currency number="${total1.totalPaidAmountPmpm}" type="number" maxFractionDigits="2" /> PMPM </strong></th>
                            <th class="text-center"></th>
                            <th class="text-center"><strong><g:formatZ5Currency number="${total1.totalAnnualCost}" type="number" maxFractionDigits="2" /> PMPM   </strong></th>

                            <th class="text-right"><strong>
                                <g:formatNumber number="${total1.totalPrescriptions}" type="number" maxFractionDigits="2" /> prescription
                                <g:formatNumber number="${total1.totalPopulationSize}" type="number" maxFractionDigits="2" /> members
                            </strong></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:if test="${data1}">
                            <g:each in="${data1}" var="data2" status="i">
                                <tr>
                                    <td>
                                        <g:if test="${data2.genericOrBrand=="brand"}">
                                            <strong style="text-transform: capitalize">${data2.drugName}</strong>
                                            <span style="text-transform: lowercase">${data2.equvalentGenericDrug}</span>
                                        </g:if>
                                        <g:else>
                                            <strong style="text-transform: lowercase">${data2.drugName}</strong>
                                            <span style="text-transform: lowercase">${data2.equvalentGenericDrug}</span>
                                        </g:else>
                                    </td>
                                    <td class="text-center">
                                        <%
                                            max=total1.totalmailRetailSavings;
                                            min=data2.mailRetailSavings
                                            try {
                                                val=(min/max)*100;
                                            } catch (ArithmeticException e) {
                                                val=0
                                            }
                                        %>

                                        <div style="height:12px;width:100px;background-color: #fff">
                                            <div style="height:12px;width:${val}px;background-color: black"></div>
                                        </div>
                                        <g:formatZ5Currency number="${data2.mailRetailSavings}" type="number" maxFractionDigits="2" /></td>
                                    <td class="text-center">
                                        <g:formatZ5Currency number="${data2.paidAmountPmpm}" type="number" maxFractionDigits="2" /> PMPM
                                    </td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                        <g:formatZ5Currency number="${data2.annualCost}" type="number" maxFractionDigits="2" /> PMPM
                                    </td>
                                    <td class="text-right">
                                        <g:formatNumber number="${data2.prescriptions}" type="number" maxFractionDigits="2" /> prescriptions
                                        <g:formatNumber number="${data2.populationSize}" type="number" maxFractionDigits="2" /> members
                                    </td>
                                    <td><a href="#" class="iconExplorePopulation">
                                        <rendering:inlineJpeg bytes="${paidBytes}" /></a></td>
                                </tr>
                            </g:each>

                        </g:if><g:else>
                            <tr>
                                <td colspan="8">
                                    No records to display.
                                </td>
                            </tr>
                        </g:else>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
    </div>
    </div>
</div>
</body>

</html>
