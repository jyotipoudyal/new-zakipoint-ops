<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Brand Details</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
    <script language="javascript" type="text/javascript" src="${resource(dir: 'js/dash', file: 'barGraph.js')}"></script>

    %{--<link href="${resource(dir:"css",file: "layout.css")}" rel="stylesheet"/>--}%
</head>

<body>


<div  id="contentWrapper" class="contentWrapper">

    <div class="container double-arrow-back-top-container">
        <div class="col-md-6 double-arrow-back-top">
            <g:link controller="highPharmacyCost" action="index" class="text-link-upper mb15 double-arrow-left">
                                <span class="isEditable"><g:editMessage code="hPharmacy.costDrivers.top.navigation"/> </span>
            </g:link>
        </div>
        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
            <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                    <g:link title="Edit Content" params="[type:type]" controller="cms" action="drugsDetails"><i class="fa fa-edit"></i> Edit Content</g:link>
                </g:if><g:else>
                <g:link title="View Changes" params="[type:type]" controller="highPharmacyCost" action="drugsDetails"><i class="fa fa-eye"></i> View Changes</g:link>
            %{--<a title="View Changes" class="switch-edit-mode"><i class="fa fa-eye"></i></a>--}%
            </g:else>
            </sec:ifAllGranted>
        </li>
        </div>

        <div class="clearfix"></div>

        <div class="sep-border mb25 pt15"></div>

        <%
            //        split page
        %>

        <div class="col-md-8">
        <h3 ><span id="tableHead1" class="isEditable"></span></h3>

        </div>
        <div class="col-md-4 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.explore.population"/></span><input type="hidden" value="hPharmacy.container3.table1.options1"></a>
            <g:link action="exportCsv" params="[exportModule:'topChronicConditionEr',type:type]"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></g:link>


            %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
        </div>

        <div class="col-md-12">
        <div class="has-filter">
            <span class="f-right custom-tooltip">
                <span class=" isEditable " ><g:editMessage code="highPharmacy.container1.methodology" /></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>
        </div>
        <g:if test="${!isEditable}">
            <div class="col-md-12  search-right mb5 mt15">
                <input type="text" id="selector1" />
            </div>
        </g:if>
        </div>
        <h1 class="text-center isEditable mb35">&nbsp;</h1>

        <div id="dataHolder" class="col-md-12">
            <g:hiddenField name="tableType" value="${type}"/>
            <g:if test="${!isEditable}">
                <g:render template="table1Data" model="[total: total, data: data,type:type]"/>
            </g:if>
            <g:else>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                <thead>
                <tr>
                    <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head1"/></span>
                        <span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head12"/></span>
                    </th>
                    <g:if test="${type=='brand'}">
                    <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head2"/></span></th>
                        </g:if>
                    <g:if test="${type=='mail'}">
                    <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head3"/></span></th>
                    </g:if>
                    <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head7"/></span></th>
                    <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head4"/></span></th>
                    <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head6"/></span>
                        <span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head61"/></span>
                    </th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <br>
            <br>
        </g:else>
        </div>

    </div>
    <%
        //        split page
    %>
</div>

<script>
    $(document).ready(function () {
        var url = "${createLink(controller: "highPharmacyCost", action: "fetchDrugsList")}";
        var tableType=$("#tableType").val();
        $("#selector1").tokenInput(url,
                {"preventDuplicates": true,
                    onAdd: function(item) {
                        if ($("#selector1").tokenInput("get").length > 1) {
                            $("#selector1").tokenInput("clear");
                        }
                        $("#selector1").tokenInput("add", item);

                        jQuery.ajax({
                            type:'POST',
                            async:false,
                            data:{brandName:item.name.toLowerCase(),type:tableType},
                            url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                            success:function(resp){
                                $('#dataHolder').html(resp);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});


                    },placeholder: "Search Drugs.."
                    ,onDelete:function(){
                    jQuery.ajax({
                        type:'POST',
                        async:false,
                        data:{module:'table1Data',type:tableType},
                        url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                        success:function(resp){
                            $('#dataHolder').html(resp);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){
                        },
                        complete:function(XMLHttpRequest,textStatus){
                        }});

                }


                });

        $("#selector2").tokenInput(url,
                {"preventDuplicates": true,
                    onAdd: function(item) {
                        if ($("#selector2").tokenInput("get").length > 1) {
                            $("#selector2").tokenInput("clear");
                        }
                        $("#selector2").tokenInput("add", item);

                        jQuery.ajax({
                            type:'POST',
                            async:false,
                            data:{brandName:item.name.toLowerCase(),module:'table2Data'},
                            url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                            success:function(resp){
                                $('#dataHolder2').html(resp);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});


                    },placeholder: "Search Drugs..",
                    onDelete:function(){
                        jQuery.ajax({
                            type:'POST',
                            async:false,
                            data:{module:'table2Data'},
                            url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                            success:function(resp){
                                $('#dataHolder2').html(resp);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});

                    }


                });
    });

    var $table =$(".z5-fixed-header");
    $.each($table, function() {
        var $this = $(this);

        var currentSort = $this.find("thead>tr th a.sorted").hasClass("desc");
        if (currentSort) {
            currentSort = "desc";
        } else {
            currentSort = "asc";
        }

        $.each($(this).find("thead>tr th a:not(.sorted)"), function () {
            $(this).addClass(currentSort+"-disabled");
//            $(this).addClass(currentSort);

        })
    });


</script>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>

</body>





</html>