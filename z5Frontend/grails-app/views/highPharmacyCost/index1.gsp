<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 9/24/2015
  Time: 2:53 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>High Pharmacy Cost</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
    <script language="javascript" type="text/javascript" src="${resource(dir: 'js/dash', file: 'barGraph.js')}"></script>
    <script type="text/javascript">

        function getMoreData(fieldName,value,pageNo){
                  var url = '<g:createLink controller="highPharmacyCost" action="searchResult"/>';
                  jQuery.ajax({
                        type:'POST',
                        data:{page:fieldName,order:"populationSize:Desc"},
                        url:url,
                        success:function(data,textStatus){
                              jQuery('#dataHolder').html(data);
                              //postPaginate();


  },
  error:function(XMLHttpRequest,textStatus,errorThrown){

  },
  complete:function(XMLHttpRequest,textStatus){

  }});
}

        %{--function getMoreData(fieldName,value,page){

                  $.blockUI();
                  jQuery.ajax({
                        type:'POST',
                        data:{page:fieldName},
                        asynchronous:false,
                        url:'<g:createLink controller="highPharmacyCost" action="searchResult"/> ',
                        success:function(data){
                              jQuery('#dataHolder').html(data);
                              $.unblockUI();
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){
                              $.unblockUI();
                        },
                        complete:function(XMLHttpRequest,textStatus){
                              $.unblockUI();
                        }});
            }--}%





    </script>
</head>

<body>
    %{--<div id="svg"></div>
    <button id="save">Save as Image</button>
    <div id="svgdataurl"></div>--}%
    %{-- <g:jasperReport
             jasper="sample-jasper-plugin1"
             format="PDF,HTML,XML,CSV,XLS,RTF,TEXT,ODT,ODS,DOCX,XLSX,PPTX"
             name="Parameter Example">
         Your name: <input type="text" name="name"/>
     </g:jasperReport>--}%


    <div  id="contentWrapper" class="contentWrapper">
    <div class="container mt10">
            <div class="col-md-6">
                <g:link controller="dashboard" action="index" class="text-link-upper mb15"><i class="fa fa-angle-double-left iconRmargin"></i>
                <span class="isEditable"><g:editMessage code="highPharmacy.top.navigation"/> </span>
                </g:link>
            </div>

        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
        <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                        <g:link controller="cms" action="highPharmacy" title="Edit Content" ><i class="fa fa-edit"></i> Edit Content</g:link>
                </g:if><g:else>
                        <g:link controller="highPharmacyCost" action="index" title="View Changes" ><i class="fa fa-eye"></i> View Changes</g:link>
                </g:else>
        </sec:ifAllGranted>
            </li>
                </div>

            <div class="clearfix"></div>
            <div class="col-md-10">
                <h2 class="mb15">
                    <g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon" />
                    <span class="isEditable"><g:editMessage code="highPharmacy.container1.graph1.title"/> </span></h2>
                <h3 class="isEditable"><g:editMessage code="highPharmacy.container1.graph1.subTitle"/></h3>
            </div>
            <div class="col-md-2">
                <g:form action="exportPdf" name="exportForm">
                    <g:hiddenField name="imagePath1"/>
                    <g:hiddenField name="graphTitle1"/>
                    <g:hiddenField name="act"/>
                    <g:hiddenField name="exp"/>
                    <g:hiddenField name="g1Filter"/>
                    <g:hiddenField name="g2Filter"/>
                    <g:hiddenField name="g3Filter"/>
                </g:form>
                %{--<a href="#" class="btn-link-upper btn-blue mb15 isEditable"><g:editMessage code="highPharmacy.container1.graph1.option1"/></a>--}%
                <a href="#" onclick="generatePdf()" class="btn-link-upper btn-light-blue mb15 isEditable"><g:editMessage code="highPharmacy.container1.graph1.option2"/></a>
                %{--<a href="#" class="btn-link-upper btn-gray mb15 isEditable"><g:editMessage code="highPharmacy.container1.graph1.option3"/></a> --}%
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="hr mb15"></div>
            </div>
            <div class="col-md-6 mb25">

                <h1 class="isEditable"><g:editMessage code="highPharmacy.container1.graph1.title2"/></h1>
            </div>
            <div class="col-md-6 text-right">
            <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="highPharmacy.explore.population"/></span><input type="hidden" value="highPharmacy.container3.table1.options1"></a>
                <g:link action="exportCsv" params="[module:'chartData']" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="highPharmacy.export.csv"/></span><input type="hidden" value="highPharmacy.container3.table1.options2"></g:link>
            </div>


            <div class="col-md-12"><h3 id="graph1Title"></h3>

            </div>
            <div class="col-md-12">
                <h6 class="has-filter">
                    <g:if test="${!isEditable}">
                        <span id="graph1Filter">
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasisg1" from="${['PaidDate':'paid','ServiceDate':'incurred']}" optionKey="key" optionValue="value" onchange="refreshReport('graph1')"/></span>
                    %{--cost to <span style="color: steelblue;"><g:select name="regionalg1" from="${['1':'NorthEast','2':'NorthCentral','3':'South']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')" noSelection="['':'regional']" /></span>--}%

                        <g:if test="${grailsApplication.config.grails.bobUrl!=session.clientname}">
                            cost to
                    <span style="color: steelblue;">
                            <g:select name="benchMark1" from="${['bob':'zph bob benchmark']}" noSelection="['':'national benchmark']"  optionKey="key" optionValue="value" onchange="refreshReport('graph1')"/>
                        </span>
                            </g:if>

                    %{--hide regional--}%

                            <span style="color: steelblue;display: none">
                    <g:select name="regionalg1" class="multiSelect" from="${['1':'NorthEast','2':'NorthCentral','3':'South','4':'West']}" optionKey="key" optionValue="value" noSelection="['':'national']"  onchange="refreshReport('graph1')" /></span>
                    %{--hide regional--}%

                        over <span style="color: steelblue;"> <g:select name="fromDateg1" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')"/></span>
                    </span>
                    </g:if>
                    <span class="f-right">
                    <span class=" isEditable" ><g:editMessage code="highPharmacy.container1.methodology" /></span>
                    <span class="fa fa-chevron-right " onclick="displayData('methodology1')"></span>
                        </span>
                </h6>



                <g:if test="${!isEditable}">  <div id="graph1" class="graph1"></div></g:if>

                <g:if test="${!isEditable}"> <div class="graph1-label" style="display: none;">

                    <div class="legend legend-red"></div>
                    <div id="actualValue"></div>
                    <div class="legend legend-gray"></div>
                    <div id="expectedValue"></div>
                </div></g:if>
            </div>




        </div>
        <div class="container">
            <div class="sep-border"></div>
            <div class="col-md-6">
                <h3 class="isEditable"><g:editMessage code="highPharmacy.container2.title1" /></h3>
                <p class="isEditable"><g:editMessage code="highPharmacy.container2.subTitle1" /></p>
                <h3 class="isEditable"><g:editMessage code="highPharmacy.container2.title2" /></h3>

                <h4 class="color-blue m150"> <g:img uri="/images/icon-high-cost-claimants.png" class="heading-icon" /> <span class="isEditable"><g:editMessage code="highPharmacy.container2.title2.subHeads01"/> </span></h4>
                <h4 class="color-blue m150"> <g:img uri="/images/ineffieicnt-network-use.png" class="heading-icon" /> <span class="isEditable"><g:editMessage code="highPharmacy.container2.title2.subHeads1"/> </span></h4>
            </div>
            <div class="col-md-6">
                <h3 class="isEditable"><g:editMessage code="highPharmacy.container2.title3" /></h3>
                <span class="bulb-lists color-blue isEditable">
                    <g:editMessage code="highPharmacy.container2.title3.subHeads1" />
                    %{--<g:pdfMessageRemoveA code="highPharmacy.container2.title3.subHeads1" />--}%
                </span>
                %{--<ul class="bulb-lists color-blue isEditable">
                    <li><i class="fa fa-lightbulb-o"></i>Update financial incentives to drive generic and/or mail order use</li>
                    <li><i class="fa fa-lightbulb-o"></i>Launch a targeted campaign around brand drugs where a generic drug is available</li>
                    <li><i class="fa fa-lightbulb-o"></i>Evaluate pharmacy benefit contract for renegotiation or carve-out potential</li>
                    <li><i class="fa fa-lightbulb-o"></i>Implement a speciality pharmacy cost management plan</li>
                </ul>--}%
            </div>

        </div>
    <%
        //        split page
    %>
        <div class="container">
            <div class="sep-border"></div>
            <div class="col-md-6 mb25">
                <h1 class="isEditable"><g:editMessage code="highPharmacy.container1.table1.mTitle1"/></h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><span class="isEditable"><g:editMessage code="highPharmacy.explore.population"/></span><input type="hidden" value="highPharmacy.container3.table1.options1"></span> </a>
                <g:link action="exportCsv" params="[module:'table1Data']" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="highPharmacy.export.csv"/></span><input type="hidden" value="highPharmacy.container3.table1.options2"></g:link>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12">

<g:if test="${!isEditable}">
                    <h2 class="mb25 isEditable" id="graph2Title"  >
                        Optimize pharmacy <strong>benefit design</strong> for a total of <strong id="tableHead1"> ${formatNumber(number: total.totalgenericBrandSavings+total.totalmailRetailSavings, type:'number' ,maxFractionDigits: '2') }</strong>&nbsp;or more in&nbsp;potential savings
                        %{--<g:editMessage code="highPharmacy.container3.table1.title" args="[formatNumber(number: total.totalgenericBrandSavings+total.totalmailRetailSavings+total.totalPaidAmountPmpm, type: number,maxFractionDigits: '2')  ]"/>--}%
                    </h2>
</g:if>s
    %{--<button onmousedown="filterShow()">Search</button>--}%
    <h6 class="has-filter">
<g:if test="${!isEditable}">
    <span id="graph2Filter">
                       Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasist1" from="${['PaidDate':'paid','ServiceDate':'incurred']}" optionKey="key" optionValue="value" onchange="refreshReport('table1Data')"/></span>
                        cost to national benchmark <span style="color: steelblue;display: none"">
                        <g:select name="regionalt1" class="multiSelect" from="${['1':'NorthEast','2':'NorthCentral','3':'South','4':'West']}" optionKey="key" optionValue="value" noSelection="['':'national']" onchange="refreshReport('table1Data')" /></span>
                        over <span style="color: steelblue;"> <g:select name="fromDatet1" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value"  onchange="refreshReport('table1Data')"/></span>
</span>
</g:if>

        <span class="f-right">
            <span class=" isEditable" ><g:editMessage code="highPharmacy.container1.methodology" /></span>
            <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
        </span>

    </h6>
<g:if test="${!isEditable}">
                <div class="col-md-12 search-right">
                    <input type="text" id="selector1" />
                </div>
</g:if>

                    <h1 class="text-center isEditable"><g:editMessage code="highPharmacy.container3.table1.subTitle"/></h1>

                    <div id="dataHolder">
                        <g:if test="${!isEditable}">
                            <g:render template="table1Data" model="[total: total, data: data]"/>
                        </g:if><g:else>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                             <thead>
                             <tr>
                                 <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head1"/></span>
                                     <span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head12"/></span>
                                 </th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head2"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head3"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head7"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head4"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head6"/></span>
                                    <span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head61"/></span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <br>
                        <br>

                        </g:else>
                    </div>

        </div>
        <%
            //        split page
        %>
        </div>
        <div class="container">
            <div class="sep-border"></div>
            <div class="col-md-6 mb25">
                <h1 class="isEditable"><g:editMessage code="highPharmacy.container1.table2.mTitle1"/></h1>
            </div>
            <div class="col-md-6 text-right">
                <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><span class=" isEditable" ><g:editMessage code="highPharmacy.explore.population" /></span><input type="hidden" value="highPharmacy.container3.table1.options1"></span> </a>
                <g:link action="exportCsv" params="[module:'table2Data']" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="highPharmacy.export.csv"/></span><input type="hidden" value="highPharmacy.container3.table1.options2"></g:link>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12">
    <g:if test="${!isEditable}">
                 <h2 class="mb25 isEditable" id="graph3Title">
                     Implement <strong>specialty pharmacy</strong> cost management strategies for a total of <strong  id="tableHead2"> ${formatNumber(number: total2.totalgenericBrandSavings+total2.totalmailRetailSavings, type: 'number',maxFractionDigits: '2') }</strong> in potential savings
                     %{--<g:editMessage code="highPharmacy.container3.table2.title" args="[formatNumber(number: total2.totalmailRetailSavings+total2.totalPaidAmountPmpm, type: number,maxFractionDigits: '2')  ]" />--}%
                 </h2>
    </g:if>
                <div> <h6 class="has-filter">
<g:if test="${!isEditable}">
    <span id="graph3Filter">
                    Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasist2" from="${['PaidDate':'paid','ServiceDate':'incurred']}" optionKey="key" optionValue="value" onchange="refreshReport('table2Data')"/></span>
                    cost to national benchmark <span style="color: steelblue;display: none">
                    <g:select name="regionalt2" class="multiSelect" from="${['1':'NorthEast','2':'NorthCentral','3':'South','4':'West']}" noSelection="['':'national']" optionKey="key" optionValue="value" onchange="refreshReport('table2Data')" /></span>
                    over <span style="color: steelblue;"> <g:select name="fromDatet2" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value"  onchange="refreshReport('table2Data')"/></span>
    </span>
</g:if>

            <span class="f-right">
                <span class=" isEditable" ><g:editMessage code="highPharmacy.container1.methodology" /></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology3')"></span>
            </span>
        </h6> <g:if test="${!isEditable}">
                    <div class="col-md-12 search-right">
                        <input type="text" id="selector2" />
                    </div>
                </g:if>
                <div class="col-md-6 mb25">
                    <h1 class="isEditable"><g:editMessage code="highPharmacy.container1.graph2.title2"/></h1>
                </div>
                    <%
                        //        split page
                    %>

                <div id="dataHolder2">
<g:if test="${!isEditable}">
                    <g:render template="table2Data" model="[total:total2,data:data2]"/>
    </g:if><g:else>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                            <thead>
                            <tr>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head1"/></span>
                                    <span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head12"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head3"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head7"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head4"/></span></th>
                                <th><span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head6"/></span>
                                    <span class="isEditable"><g:editMessage code="highPharmacy.container3.table1.head61"/></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                </g:else>
            </div>

            </div>
        </div>
    </div>

    <div id="methodology1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="isEditable"> <g:editMessage code="highPharmacy.container1.methodology1"/></span>
                </div>

            </div>
        </div>
    </div>
    <div id="methodology2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="isEditable"><g:editMessage code="highPharmacy.container1.methodology2"/></span>
                </div>

            </div>
        </div>
    </div>
    <div id="methodology3" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="isEditable"><g:editMessage code="highPharmacy.container1.methodology3"/></span>
                </div>
            </div>
        </div>
    </div>
    <div id="filteredList" class="modal fade" role="dialog">
    </div>
    <div style="display: none;">
        <canvas id="myCanvas" width="700" height="400" style="border:1px solid #000000;"></canvas>
    </div>
</div>


    


<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>


<g:javascript>

 $("span.bulb-lists").find("ul").addClass("bulb-lists color-blue");
    $("span.bulb-lists").find("li").prepend("<i class='fa fa-lightbulb-o'></i>");

function custom_sort(a, b) {
    return a.date - b.date;
}

var div = d3.select(".contentWrapper").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

 if(!${isEditable})
showHighPharmacyGraph(${dataPh})

function showHighPharmacyGraph(d){

     $("#graph1").html("");
     $(".graph1-label").fadeOut();
   %{-- var paths='${resource(dir:"dData",file: "multi.csv")}';--}%

    var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
            main_width = 700 - main_margin.left - main_margin.right,
            main_height =300 - main_margin.top - main_margin.bottom;

    %{--var formatDate = d3.time.format("%Y-%m").parse;
    var pharmacyClaims = d.pharmacyClaims;
    var memberMonths = d.memberMonths;
    var newVal;
    var data = [];
    $.each(pharmacyClaims,function(key,val) {
        newVal = new Object();
        newVal.date=parseDate4(key);
        newVal.close=(pharmacyClaims[key]/memberMonths[key])|| 0;
        data.push(newVal);
    });--}%

    var pharmacyClaims = d.pharmacyClaims;
    var bobClaims = d.bobClaims;
    var benchMark = (d.benchmark);
    //var benchMark = 3000;
    var memberMonths = d.memberMonths;
    var bobMemberMonths = d.bobMemberMonths;
    var newVal;
    var data = [];
    var parseDate4 = d3.time.format("%Y-%m").parse;


    $.each(pharmacyClaims,function(key,val) {
        newVal = new Object();
        newVal.time1=parseDate4(key);
        newVal.actual=Math.round((pharmacyClaims[key]/memberMonths[key]))|| 0;
        if(bobClaims)
        newVal.expected=Math.round((bobClaims[key]/bobMemberMonths[key]))|| 0;
        else{
        newVal.expected=Math.round(benchMark)|| 0;
        }

        data.push(newVal);
    });
    data.sort(custom_sort);
    var altValue='expected';
       if(bobClaims){
       altValue='zph bob';
       }


    var formatDate =d3.time.format("%d-%b-%y"),
            parseDate = formatDate.parse,
            bisectDate = d3.bisector(function(d) { return d.time1; }).left,
            //format0 = function(d) { return formatDate(d.time1) + " - $ " + d.actual; },
            //format1 = function(d) { return formatDate(d.time1) + " - $ " + d.expected; },
            formatOutput0 = function(d) { return "$"+Math.round(d.actual) +"/PMPM actual"; },
            formatOutput1 = function(d) { return "$"+Math.round(d.expected) +"/PMPM "+altValue; };


    var main_x = d3.time.scale()
            .range([0, main_width]);

    var main_y = d3.scale.linear()
                    .range([main_height, 0]);

    var main_xAxis = d3.svg.axis()
            .scale(main_x)
            //.tickFormat(d3.time.format("%Y%b"))
            .outerTickSize(0)
            .ticks(d3.time.months, 1)
        .tickFormat(d3.time.format('%b'))
            //.ticks(6)
            .orient("bottom");

    var main_xAxis2 =d3.svg.axis()
    .scale(main_x)
    .ticks(d3.time.month, 9)
    //.tickValues( main_x.domain())
    .tickFormat(d3.time.format("%Y"))
    .tickSize(5,0)
    .orient("bottom");

    var main_yAxisLeft = d3.svg.axis()
                    .scale(main_y)
                    .ticks(5)
                    .tickSize(-main_width)
                    .outerTickSize(0)
                    .orient("left");

    var main_line0 = d3.svg.line()
            //.interpolate("basis")
            .x(function(d) { return main_x(d.time1); })
            .y(function(d) { return main_y(d.actual); });

    var main_line1 = d3.svg.line()
            //.interpolate("basis")
            .x(function(d) { return main_x(d.time1); })
            .y(function(d) { return main_y(d.expected); });


    var svg = d3.select("#graph1").append("svg")
            .attr("width", main_width + main_margin.left + main_margin.right)
            .attr("height", main_height + main_margin.top + main_margin.bottom);

    svg.append("defs").append("clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", main_width)
            .attr("height", main_height);

    var main = svg.append("g")
            .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");


    %{--d3.csv(paths, function(error, data) {
        data.forEach(function(d) {
            d.time1 = parseDate(d.time1);
            d.expected = +d.expected;
            d.actual = +d.actual;
        });--}%



        data.sort(function(a, b) {
            return a.time1 - b.time1;
        });



      var value1=Math.round(data[data.length-1].actual);
      var value2=Math.round(data[data.length-1].expected);
      var diff;
      if(value1>value2){
        diff="Current pharmacy costs are <strong>$"+(value1-value2)+"</strong> per member per month <strong>more than</strong> "+altValue
      }else if(value1<value2){
        diff="Current pharmacy costs are $"+(value2-value1)+" per member per month <strong>less than</strong> "+altValue
      }else{
      diff="Current pharmacy costs are $"+(value1-value2)+" per member per month <strong>as</strong> "+altValue
      }
      $("#graph1Title").html(diff);

        main_x.domain([data[0].time1, data[data.length - 1].time1]);
        var array_y0=d3.extent(data, function(d) { return d.actual; });
        var array_y1=d3.extent(data, function(d) { return d.expected; });
        array_y0=array_y0.concat(array_y1);
        main_y.domain([0,d3.max(array_y0)]);


         main.append("g")
                .attr("class", "x axis xaxisLeft")
                .attr("transform", "translate(0," + main_height + ")")
                .call(main_xAxis);

                main.append("g")
                .attr("class", "x axis xaxisLeft2")
                .attr("transform", "translate(0," + (main_height+25) + ")")
                .call(main_xAxis2);
                %{--.append("text")
                .attr("class", "text_x")
                .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate("+ (main_width/2) +",30)");  // centre below axis
            //.text("TIME");--}%

        main.append("g")
                .attr("class", "y axis yaxisLeft")
                .call(main_yAxisLeft)
                .selectAll("path")
                .attr("stroke", "#ccc");

                main.selectAll("line")
                .attr("stroke", "#ccc");

                main.selectAll(".tick")
                .attr("stroke-dasharray", "3 3")
                .attr("opacity", ".5");

                main.append("text")
                .attr("class", "text_y")
                .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                .attr("transform", "translate("+ (-38) +","+(main_height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                .text("COST ($)");


            var path1=main.append("path")
                //.attr("clip-path", "url(#clip)")
                .attr("class", "line line0")
                .attr("d", main_line0(data))
                .style("stroke","red")
                .style("fill", "none")
                .style('stroke-width', '1.2px');


        var path2=main.append("path")
                //.attr("clip-path", "url(#clip)")
                .attr("class", "line line1")
                .attr("d", main_line1(data))
                .style('stroke', 'gray')
                .style("fill", "none")
                .style('stroke-width', '1.2px');





        var totalLength = path1.node().getTotalLength();

        path1
            .attr("stroke-dasharray", totalLength + " " + totalLength)
            .attr("stroke-dashoffset", totalLength)
            .transition()
            .duration(600)
            .ease("linear")
            .attr("stroke-dashoffset", 0)
            .each('end', function () {
                //alert(value1);
                $("#actualValue").html("<span class='label-value'>$"+value1+"</span><span class='label-unit'>/PMPM</span><span class='label-title'>actual cost</span>");
        });

        path2
            .attr("stroke-dasharray", totalLength + " " + totalLength)
            .attr("stroke-dashoffset", totalLength)
            .transition()
            .duration(600)
            .ease("linear")
            .attr("stroke-dashoffset", 0)
            .each('end', function () {
                $("#expectedValue").html("<span class='label-value'>$"+value2+"</span><span class='label-unit'>/PMPM</span><span class='label-title'>"+altValue+" cost</span>");
                $(".graph1-label").fadeIn();
        });








        var focus = main.append("g")
                .attr("class", "focus")
                .style("display", "block");

        focus.append("line")
                .attr("class", "x")
                .attr("y1", 0);

        focus.append("circle")
                .attr("class", "y0")
                .attr("r", 3);

        focus.append("text")
                .attr("class", "y0")
                .attr("dy", "-1em");

        focus.append("circle")
                .attr("class", "y1")
                .attr("r", 3);




        focus.append("text")
                .attr("class", "y1")
                .attr("dy", "-1em");

        focus.style("visibility", "hidden");




        main.append("rect")
                .attr("class", "overlay")
                .attr("fill","none")
                .attr("pointer-events","all")
                .attr("width", main_width)
                .attr("height", main_height)
                .on("mouseover", function() { focus.style("display", null); })
                .on("mouseout", function() {
                div.transition()
                .duration(500)
                .style("opacity", 0);
                focus.style("display", "none");
                 })
                .on("mousemove", mousemove);


        function mousemove() {

            focus.style("visibility", "visible");

            var x0 = main_x.invert(d3.mouse(this)[0]),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.time1 > d1.time1 - x0 ? d1 : d0;
            focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");
            //focus.select("text.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");//.text(formatOutput0(d));
            focus.select("circle.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");

            //focus.select("text.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");//.text(formatOutput1(d));

            focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));


            //focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));

            var popUpText="";
            div.transition()
                .duration(100)
                .style("opacity", .9);
            div .html(popUpText+"<span class='formatOutput0'>"+formatOutput0(d)+"</span><br><br><span class='formatOutput1'>"+formatOutput1(d)+"</span>")
                .style("left", (d3.event.pageX +30) + "px")
                .style("top", (d3.event.pageY - 60) + "px");

            //focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.actual) + ")").attr("x2", main_width + main_x(d.time1));
            //focus.select(".y1").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.expected) + ")").attr("x2", main_width + main_x(d.time1));
            //focus.select(".y1").attr("transform", "translate(0, " + main_y(d.expected) + ")").attr("x1", main_x(d.time1));
        }

}




%{--var width = 960,
    height = 500;

var m = 5, // number of series
    n = 90; // number of values

// Generate random data into five arrays.
var data = d3.range(m).map(function() {
  return d3.range(n).map(function() {
    return Math.random() * 100 | 0;
  });
});

var x = d3.scale.linear()
    .domain([0, n - 1])
    .range([0, width]);

var y = d3.scale.ordinal()
    .domain(d3.range(m))
    .rangePoints([0, height], 1);

var color = d3.scale.ordinal()
    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56"]);

var area = d3.svg.area()
    .interpolate("basis")
    .x(function(d, i) { return x(i); })
    .y0(function(d) { return -d / 2; })
    .y1(function(d) { return d / 2; });

var svg = d3.select("#svg").append("svg")
    .attr("width", width)
    .attr("height", height);

svg.selectAll("path")
    .data(data)
  .enter().append("path")
    .attr("transform", function(d, i) { return "translate(0," + y(i) + ")"; })
    .style("fill", function(d, i) { return color(i); })
    .attr("d", area);--}%




    d3.select("#save").on("click", function(){
      var html = d3.select("#graph1 svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .node().parentNode.innerHTML;

      //console.log(html);
      var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
      var img = '<img src="'+imgsrc+'">';
  d3.select("#svgdataurl").html(img);
});


    function refreshReport(module){
    var reportingBasis,regional, fromDate,tick;

    if(module=='graph1'){
     reportingBasis=$("#reportingBasisg1").val();
     regional=$("#regionalg1").val();
     fromDate=$("#fromDateg1").val();
      tick=$("#benchMark1").val();
                            jQuery.ajax({
                            type:'POST',
                            dataType: "json",
                            data:{reportingBasis:reportingBasis,regional:regional,fromDate:fromDate,bob:tick},
                            url:'<g:createLink controller="highPharmacyCost" action="getChartScriptForPharmacy"/>',
                            success:function(resp){
                            console.log(resp);
                                showHighPharmacyGraph(resp)
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});


    }else if(module=='table1Data'){
    reportingBasis=$("#reportingBasist1").val();
     regional=$("#regionalt1").val();
     fromDate=$("#fromDatet1").val();
                            jQuery.ajax({
                            type:'POST',
                            data:{reportingBasis:reportingBasis,regional:regional,fromDate:fromDate,module:module},
                            url:'<g:createLink controller="highPharmacyCost" action="getTableScriptForPharmacy"/>',
                            success:function(resp){
                                $('#dataHolder').html(resp);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});
    }else if(module=='table2Data'){
    reportingBasis=$("#reportingBasist2").val();
     regional=$("#regionalt2").val();
     fromDate=$("#fromDatet2").val();
                            jQuery.ajax({
                            type:'POST',
                            data:{reportingBasis:reportingBasis,regional:regional,fromDate:fromDate,module:module},
                            url:'<g:createLink controller="highPharmacyCost" action="getTableScriptForPharmacy"/>',
                            success:function(resp){
                                $('#dataHolder2').html(resp);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});
    }

    }

    $(function(){
            /*$('.multiSelect').multiselect({
            nonSelectedText: 'regional',
            allSelectedText: "All Regions Selected"
            });*/






});






    function generatePdf(){
    %{--var html = d3.select("#graph1 svg")
            .attr("version", 1.1)
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .node().parentNode.innerHTML;


      //console.log(html);
      var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
      //var imgsrc = btoa(html);
    $("#imagePath1").val(imgsrc);
    var img = '<img src="'+imgsrc+'">';
    d3.select("#actualValue").html(img);--}%


    var html = d3.select("#graph1 svg")
        .attr("version", 1.1)
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .node().parentNode.innerHTML;

  //console.log(html);
  var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
  var img = '<img src="'+imgsrc+'">';

  //var imgsrc = 'data:image/svg+xml;base64,'+ btoa(html);
  //var img = '<img src="'+imgsrc+'">';
  //d3.select("#actualValue").html(img);


  var canvas = document.querySelector("canvas"),
	  context = canvas.getContext("2d");

      var image = new Image;
      image.src = imgsrc;

	  context.drawImage(image, 0, 0);
	  var canvasdata = canvas.toDataURL("image/png");
    $("#imagePath1").val(canvasdata);
    $("#graphTitle1").val($("#graph1Title").text());
    $("#act").val($("#actualValue").html());
    $("#exp").val($("#expectedValue").html());
    var reportingBasis,regional, fromDate;
    reportingBasis=$("#reportingBasisg1 option:selected").text();
     regional=$("#regionalg1 option:selected").text();
     fromDate=$("#fromDateg1 option:selected").text();
    $("#g1Filter").val("Compare "+reportingBasis+" cost to  "+regional+" benchmark over "+fromDate);
     reportingBasis=$("#reportingBasist1 option:selected").text();
     regional=$("#regionalt1 option:selected").text();
     fromDate=$("#fromDatet1 option:selected").text();
    $("#g2Filter").val("Compare "+reportingBasis+" cost to  "+regional+" benchmark over "+fromDate);
     reportingBasis=$("#reportingBasist2 option:selected").text();
     regional=$("#regionalt2 option:selected").text();
     fromDate=$("#fromDatet2 option:selected").text();
    $("#g3Filter").val("Compare "+reportingBasis+" cost to  "+regional+" benchmark over "+fromDate);
    //$("#graphTitle2").val($("#graph2Title").text());
    //$("#graphTitle3").val($("#graph3Title").text());
    $("#exportForm").submit();

	 %{-- var pngimg = '<img src="'+canvasdata+'">';
  	  d3.select("#pngdataurl").html(pngimg);

	  var a = document.createElement("a");
	  a.download = "sample.png";
	  a.href = canvasdata;
	  a.click();--}%






    //
            %{--jQuery.ajax({
                type:'POST',
                contentType: "application/x-www-form-urlencoded",
                async: true,
                cache: false,
                url:'<g:createLink controller="highPharmacyCost" action="slides"/> ',
                success:function(data){

                },
                error:function(XMLHttpRequest,textStatus,errorThrown){

                },
                complete:function(XMLHttpRequest,textStatus){

                }});--}%
        }


%{--</g:javascript>

<script type="text/javascript">--}%
    $(document).ready(function () {
        var url = "${createLink(controller: "highPharmacyCost", action: "fetchDrugsList")}";
        $("#selector1").tokenInput(url,
                {"preventDuplicates": true,
                    onAdd: function(item) {
                        if ($("#selector1").tokenInput("get").length > 1) {
                            $("#selector1").tokenInput("clear");
                        }
                        $("#selector1").tokenInput("add", item);

                        jQuery.ajax({
                            type:'POST',
                            async:false,
                            data:{brandName:item.name.toLowerCase(),module:'table1Data'},
                            url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                            success:function(resp){
                                $('#dataHolder').html(resp);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});


                    },placeholder: "Search Drugs.."
                    ,onDelete:function(){
                    jQuery.ajax({
                        type:'POST',
                        async:false,
                        data:{module:'table1Data'},
                        url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                        success:function(resp){
                            $('#dataHolder').html(resp);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){
                        },
                        complete:function(XMLHttpRequest,textStatus){
                        }});

                }


                });

        $("#selector2").tokenInput(url,
                {"preventDuplicates": true,
                    onAdd: function(item) {
                        if ($("#selector2").tokenInput("get").length > 1) {
                            $("#selector2").tokenInput("clear");
                        }
                        $("#selector2").tokenInput("add", item);

                        jQuery.ajax({
                            type:'POST',
                            async:false,
                            data:{brandName:item.name.toLowerCase(),module:'table2Data'},
                            url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                            success:function(resp){
                                $('#dataHolder2').html(resp);
                            },
                            error:function(XMLHttpRequest,textStatus,errorThrown){
                            },
                            complete:function(XMLHttpRequest,textStatus){
                            }});


                    },placeholder: "Search Drugs..",
                    onDelete:function(){
                    jQuery.ajax({
                        type:'POST',
                        async:false,
                        data:{module:'table2Data'},
                        url:'<g:createLink controller="highPharmacyCost" action="drugsFilter"/>',
                        success:function(resp){
                            $('#dataHolder2').html(resp);
                        },
                        error:function(XMLHttpRequest,textStatus,errorThrown){
                        },
                        complete:function(XMLHttpRequest,textStatus){
                        }});

                }


                });
    });
</g:javascript>

    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">
%{--
<style>


div.tooltip {
    position: absolute;
    text-align: center;
    width: 150px;
    text-align: left; padding: 15px 5px;
    line-height: 24px;
    font: 12px sans-serif;
    background: #ffffff;
    background: lightsteelblue;
    border: 4px solid #c0c0c0;
    border-radius: 8px;
    pointer-events: none;
}
.formatOutput0{
    color: red;
    opacity: 1;
}
.formatOutput1{
    color: #8c8c8c;
    opacity: 1;
}

#graph1 {
    font: 12px sans-serif;
}

.axis path,
.axis line {
    fill: none;
    stroke: #ccc;
    shape-rendering: crispEdges;
    /*shape-rendering: geometricPrecision;*/
}




.x.axis path {
    display: none;
}


.line {
    stroke: black;
    fill: none;
    stroke-width: 0.75px;
}


.line.line0 {
    fill: none;
    stroke: url(#line-gradient);
    stroke-width: 1.2px;
}
.line.line0 {
    stroke: red;
    stroke-width: 1.2px;
}

.line.line1 {
    stroke: indianred;
    stroke: gray;
    stroke-width: 1.2px;
}

/*.overlay {
    fill: none;
    pointer-events: all;
}*/

.focus circle {
    fill: none;
}

.focus circle.y0 {
    fill: red;
}

.focus circle.y1 {
    fill: gray;
}

.focus line {
    stroke: purple;
    shape-rendering: crispEdges;
}


.focus line.x {
    stroke: gray;
    stroke-width: 1.3px;
    stroke-dasharray: 3 3;
    opacity: .5;
}
.focus line.y0 {
    stroke: steelblue;
    stroke-dasharray: 3 3;
    opacity: .5;
}

.focus line.y1 {
    stroke: indianred;
    stroke-dasharray: 3 3;
    opacity: .5;
}

.brush .extent {
    stroke: #fff;
    fill-opacity: .125;
    shape-rendering: crispEdges;
}
svg{
    cursor: pointer;
}

.yaxisLeft .tick{
    stroke-dasharray: 3 3;
    opacity: .5;
}
</style>--}%
</body>
</html>