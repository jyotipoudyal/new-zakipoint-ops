<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="z5-table">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
        <thead>
        <tr>
            <th class="text-left" ><g:editMessage code="highPharmacy.container3.table1.head1"/><g:editMessage code="highPharmacy.container3.table1.head12"/></th>
            <util:remoteSortableColumn  class="text-center ${defaultSort}"  property="mailRetailSavings" title="${editMessage(code:"highPharmacy.container3.table1.head3")}" update="dataHolder2" action="sortResult" params="[module:'table2Data']"/>
            <util:remoteSortableColumn  class="text-center"  property="paidAmountPmpm" title="${editMessage(code:"highPharmacy.container3.table1.head7")}" update="dataHolder2" action="sortResult" params="[module:'table2Data']"/>
            %{--<util:remoteSortableColumn  class="text-center" property="pmpmBenchMark" title="${editMessage(code:"highPharmacy.container3.table1.head5")}" update="dataHolder" action="sortResult" params="[module:'table2Data']"/>--}%
            <util:remoteSortableColumn  class="text-center"  property="annualCost" title="${editMessage(code:"highPharmacy.container3.table1.head4")}" update="dataHolder2" action="sortResult" params="[module:'table2Data']"/>
            <util:remoteSortableColumn property="prescriptions"   style="width: 150px;" class="text-right" title="${editMessage(code:"highPharmacy.container3.table1.head6")}${editMessage(code:"highPharmacy.container3.table1.head61")}" update="dataHolder2" action="sortResult" params="[module:'table2Data']"/>
            <th></th>
        </tr>

       %{-- <tr>

            <util:remoteSortableColumn property="genericBrandSavings" title="${editMessage(code:"highPharmacy.container3.table1.head2")}" update="dataHolder" action="sortResult"/>
            <util:remoteSortableColumn property="mailRetailSavings" title="${editMessage(code:"highPharmacy.container3.table1.head3")}" update="dataHolder" action="sortResult"/>
                <util:remoteSortableColumn property="paidAmountPmpm" title="${editMessage(code:"highPharmacy.container3.table1.head4")}" update="dataHolder" action="sortResult"/>
                <util:remoteSortableColumn property="annualCost" title="${editMessage(code:"highPharmacy.container3.table1.head5")}" update="dataHolder" action="sortResult"/>
                <util:remoteSortableColumn property="populationSize" title="${editMessage(code:"highPharmacy.container3.table1.head6")}" update="dataHolder" action="sortResult"/>
        </tr>--}%
        <tr>
            <th class="text-left"><strong>Totals</strong></th>
            %{--<th class="text-center"><strong><g:formatZ5Currency number="${total.totalgenericBrandSavings}" type="number" maxFractionDigits="2" /></strong></th>--}%
            <th class="text-center"><strong><g:formatZ5Currency number="${total.totalmailRetailSavings}" type="number" maxFractionDigits="2" /></strong></th>
            <th class="text-center"><strong><g:formatZ5Currency number="${total.totalPaidAmountPmpm}" type="number" maxFractionDigits="2" /> PMPM<br> </strong></th>
            %{--<th class="text-center"></th>--}%
            <th class="text-center"><strong><g:formatZ5Currency number="${total.totalAnnualCost}" type="number" maxFractionDigits="2" /> PMPM<br>   </strong></th>

            <th class="text-right"><strong>
                <g:formatNumber number="${total.totalPrescriptions}" type="number" maxFractionDigits="2" /> prescription<br>
                <g:formatNumber number="${total.totalPopulationSize}" type="number" maxFractionDigits="2" /> members
            </strong></th>
        <th></th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${data}">
        <g:each in="${data}" var="data1" status="i">
            <tr>
                <td >

                    %{--<g:if test="${data1.genericOrBrand=="brand"}">--}%
                        <strong style="text-transform: capitalize">${data1.drugName}</strong><br />
                        <span style="text-transform: lowercase">${data1.equvalentGenericDrug}</span>
                    %{--</g:if>
                    <g:else>
                    <strong style="text-transform: lowercase">${data1.drugName}</strong><br />
                    <span style="text-transform: lowercase">${data1.equvalentGenericDrug}</span>
                </g:else>--}%
                </td>
                %{--<td class="text-center">
                    <span id="barT2a_${i}" class="barGraph"></span><br>
                    <script>getBarGraph("barT2a_"+${i},${data1.genericBrandSavings},${total.totalgenericBrandSavings});</script>
                    <g:formatZ5Currency number="${data1.genericBrandSavings}" type="number" maxFractionDigits="2" /></td>--}%
                <td class="text-center">
                    <span id="barT2b_${i}" class="barGraph"></span><br>
                    <script>getBarGraph("barT2b_"+${i},${data1.genericBrandSavings},${total.totalmailRetailSavings});</script>
                    <g:formatZ5Currency number="${data1.mailRetailSavings}" type="number" maxFractionDigits="2" /></td>

                <td class="text-center">
                    <g:formatZ5Currency number="${data1.paidAmountPmpm}" type="number" maxFractionDigits="2" /> PMPM
                </td>
                %{--<td class="text-center"></td>--}%
                <td class="text-center">
                    <g:formatZ5Currency number="${data1.annualCost}" type="number" maxFractionDigits="2" /> PMPM
                </td>
                <td class="text-right">
                    %{--G<a href="#"><i class="fa fa-users iconLRmargin"></i>--}%
                    <g:formatNumber number="${data1.prescriptions}" type="number" maxFractionDigits="2" /> prescriptions<br>
                    <g:formatNumber number="${data1.populationSize}" type="number" maxFractionDigits="2" /> members
                </td>
                <td><a href="#" class="iconExplorePopulation"><g:img uri="/images/icon-explore-population.png" alt="Tilde" class="pull-left" /></a></td>
            </tr>
        </g:each>

        </g:if><g:else>
            <tr>
                <td colspan="7">
                    No records to display.
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>

</div>
%{--<g:if test="${!(size1<=5)}">--}%
<div class="z5-pagination">
    %{--<g:remotePaginate action="searchResult12" offset="${(page-1)*5}" total="${size}" update="dataHolder" maxsteps="5" max="${5}" />--}%
    <util:remotePaginate controller="highPharmacyCost" action="remotePagination" total="${size1}" update="dataHolder2" max="5" pageSizes="[5:'5 Records/Page', 20: '20 Records/Page', 50:'50 Records/Page']" params="[module:'table2Data']" alwaysShowPageSizes="true" />
    %{--<g:remotePaginate action="searchResult" offset="${(page-1)*pageSize}" total="${summary.getAt('totalCounts').toInteger()}" update="dataHolder" maxsteps="5" max="${pageSize}" pageSizes="[10:'10 Records/Page', 20: '20 Records/Page', 50:'50 Records/Page',100:'100 Records/Page']"/>--}%
    %{--<div class="showing">Showing 1-3 of 2 drugs</div>--}%
    %{--<div class="page-no"><a href="#"><i class="fa fa-chevron-left iconLRmargin"></i>Prev</a> Page <span class="active">2</span> of 5 <a href="#">Next<i class="fa fa-chevron-right iconLRmargin"></i></a></div>--}%
    %{--<div class="clearfix"></div>--}%
</div>
%{--</g:if>--}%

<script>
    var amount=(${total.totalgenericBrandSavings+total.totalmailRetailSavings})>=1000?"$ "+(Math.round(${total.totalgenericBrandSavings+total.totalmailRetailSavings}))/1000+" K":"$ "+Math.round(${total.totalgenericBrandSavings+total.totalmailRetailSavings});

    $("#tableHead2").html(amount);

    $( document ).ready(function() {
        var $table = $('table.z5-fixed-header');
        $table.floatThead({
            scrollContainer: function($table){
                return $table.closest('.z5-table');
            }
        });
    });
</script>


