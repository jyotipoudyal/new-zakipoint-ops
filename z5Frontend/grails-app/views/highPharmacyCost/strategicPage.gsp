<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 11/4/2015
  Time: 3:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Strategic Page</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<body>
<div class="mainWrapper">
    <div class="contentWrapper">
        <div class="col-md-10" style="display: none;" id="editContent">
        <li>
        <sec:ifAllGranted roles="ROLE_Z5ADMIN">
        <g:if test="${!isEditable}">

                <g:link controller="cms" action="strategicPage" title="Edit Content" ><i class="fa fa-edit"></i> Edit Content</g:link>
        </g:if><g:else>

                <g:link controller="highPharmacyCost" action="strategicPage" title="View Changes" ><i class="fa fa-eye"></i> View Changes</g:link>

        </g:else></sec:ifAllGranted></li></div><br>
        <div class="container">
            <div class="col-md-10">

                <h2 class="mb15"><span class="isEditable"><g:editMessage code="strategicpage.heading"/> </span></h2>
                <h3><span class="isEditable"><g:editMessage code="strategicpage.subheading"/> </span></h3>
            </div>
            <div class="col-md-2"> <a href="#" class="btn-link-upper btn-blue mb15"><span class="isEditable"><g:editMessage code="strategicpage.contact"/> </span></a> </div>
            <div class="clearfix"></div>
        </div>
        <div class="container">
            <div class="col-md-12">
                <div class="hr mt15 mb15"></div>
                <h2><span class="isEditable"><g:editMessage code="strategicpage.container1.heading"/> </span></h2>
                <h1><span class="isEditable"><g:editMessage code="strategicpage.container1.subheading"/> </span></h1>
            </div>
            <div class="col-md-6">
                <span class="isEditable"><g:editMessage code="strategicpage.container1.body"/> </span>
               %{-- <ul class="number-list">
                    <li><span class="num">1</span>With your permission, Zakipoint Health will send your de-identified pharmacy claims data to up to foru PBM service provider alternatives to get you quotes.</li>
                    <li><span class="num">2</span>Quotes will be compared to current PBM contract terms and fees. </li>
                    <li><span class="num">3</span>New service provider proposals and contracts will be obtained for final client decision. </li>
                </ul>--}%
            </div>
            <div class="col-md-6">
                <p class="text-itallic">
                    <span class="isEditable"><g:editMessage code="strategicpage.container1.body.info"/> </span>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="col-md-12">
                <div class="hr mt15 mb15"></div>
                <span class="isEditable"><g:editMessage code="strategicpage.container2.heading"/> </span></h1>
            </div>
            <div class="col-md-1">
                <div class="photo"><span class="isEditable"><g:editMessage code="strategicpage.container2.prefix"/> </span></div>
            </div>
            <div class="col-md-11">
                <span class="isEditable"><g:editMessage code="strategicpage.container2.postfix"/> </span>

            </div>
        </div>
        <div class="container">
            <div class="hr mt15 mb15"></div>
            <div class="col-md-12 text-center"> <a href="#" class="btn-link-upper btn-blue inline-block mb15"><span class="isEditable"><g:editMessage code="strategicpage.contact"/> </span></a>
                <p> <span class="isEditable"><g:editMessage code="strategicpage.message.info"/> </span></p>
            </div>
        </div>
    </div>

</div>

</body>
</html>