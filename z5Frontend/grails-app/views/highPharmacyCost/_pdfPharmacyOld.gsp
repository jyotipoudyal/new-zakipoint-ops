%{--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">--}%
<%@page defaultCodec="none" %>
<html>
<head>
    %{--<link rel="stylesheet" type="text/css" href="${grailsApplication.config.grails.serverURL}/css/print.css" media="all" />--}%
    <style type="text/css" media="all">
        /* Typography */
        .graph1 {
            width: 550px;
            height: 350px;
        }

        .graph1-label {
            width: auto;
            margin-top: 25px;
        }

        .graph1-label {
            position: relative;
            padding-left: 35px;
        }

        .graph1-label .legend {
            position: absolute;
            left: 0;
            width: 25px;
            margin-top: 20px;
        }

        .legend-red, .legend-gray, .legend-blue {
            height: 5px;
            line-height: 2px;
        }

        .legend-red {
            background: #ff0000;
        }

        .legend-gray {
            background: #ccc;
        }

        img.heading-icon {
            width: 35px;
            margin-right: 10px;
        }

        .z5-table table {
            width: 100%;
            border-collapse: collapse;
        }

        .z5-table table th {
            border-bottom: 1px solid #999;
        }

        .z5-table table td {
            border-bottom: 1px solid #ccc;
        }

        /* Fixers */
        .hr, .sep-border {
            border-bottom: 1px solid #ccc;
            margin-bottom: 15px;
        }

        .print-header {
            margin-bottom: 10px;
            border-bottom: 1px solid #ccc;
            padding-bottom: 20px;
        }

    </style>
</head>
<body>
<div>
    <div>
        <div class="print-header">
            <div>
                %{--<g:img uri="${grailsApplication.config.grails.serverURL}/images/logo-zakipoint-large.png" alt="" />--}%
                <rendering:inlineJpeg bytes="${heads}"/>
            </div>
        </div>
        <div>
            <div>
                <h2>
                    <rendering:inlineJpeg bytes="${cost}" class="heading-icon"/>
                    %{--<g:img uri="${grailsApplication.config.grails.serverURL}/images/icon-high-pharmacy-cost.png" alt=""  class="heading-icon"/>--}%
                    <span ><g:pdfMessage code="highPharmacy.container1.graph1.title"/></span></h2>
                <h3><g:pdfMessage code="highPharmacy.container1.graph1.subTitle"/></h3>
            </div>
            <div class="sep-border"></div>
            <h1><g:pdfMessage code="highPharmacy.container1.graph1.title2"/></h1>
            <div><h3 id="graph1Title">${graphTitle1}</h3></div>
            <div>${g1Filter}</div>
            <table>
                <tr>
                    <td rowspan="2"><div id="graph1" class="graph1"><rendering:inlineJpeg bytes="${image}" class="graph1"/></div></td>
                    <td>
                        <div class="graph1-label"><div class="legend legend-red"></div>
                            <div>${act}</div></div>
                    </td>
                </tr>
                <tr>
                    <td><div class="graph1-label" style="margin-top: -140px;"><div class="legend legend-gray"></div>
                        <div>${exp}</div></div></td>
                </tr>
            </table>
        </div>
        <div class="sep-border" style="margin-top: -60px;"/>
        <div>
            <table style="width: 700px;">
                <tr>
                    <td style="width: 350px;">
                        <h3 ><g:pdfMessage code="highPharmacy.container2.title1"/></h3>
                        <p ><g:pdfMessage code="highPharmacy.container2.subTitle1"/></p>
                        <h3 ><g:pdfMessage code="highPharmacy.container2.title2"/></h3>
                        <h4 >
                            <rendering:inlineJpeg bytes="${costClaimants}" class="heading-icon"/>
                            %{--<g:img uri="${grailsApplication.config.grails.serverURL}/images/icon-high-cost-claimants.png" alt=""  class="heading-icon"/>--}%

                            <span>Highcost <strong>Claimants</strong></span></h4>
                        <h4>
                            <rendering:inlineJpeg bytes="${cost}" class="heading-icon"/>
                            %{--<g:img uri="${grailsApplication.config.grails.serverURL}/images/icon-high-pharmacy-cost.png" alt=""  class="heading-icon"/>--}%
                            <span><g:pdfMessage code="highPharmacy.container2.title2.subHeads1"/></span></h4>
                    </td>
                    <td style="padding-left: 10px;"><div style="margin-top: -80px">
                        <h3 style="margin-top: -40px;"><g:pdfMessage code="highPharmacy.container2.title3"/></h3>
                        <span>
                            <g:pdfMessageRemoveA code="highPharmacy.container2.title3.subHeads1"/>
                        </span>
                    </div>
                    </td>
                </tr>
            </table>
        </div>
        %{--<br style="page-break-before: always"></br>--}%
        <div style="margin-top: 25px;">
            <div>
                <h1><g:editMessage code="highPharmacy.container1.table1.mTitle1"/></h1>
                <div class="sep-border"></div>
            </div>
            <div></div>
            <div>
                <h3>
                    Optimize pharmacy <strong>benefit design</strong> for a total of $ <strong
                        id="tableHead1"><g:formatNumber
                            number="${total.totalgenericBrandSavings + total.totalmailRetailSavings}" type="number"
                            maxFractionDigits="2"/></strong> or more in potential savings
                </h3>
                <div>${g2Filter}</div>
                <div>
                    <div>
                        <div class="z5-table" style="margin-top: 30px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                <tr>
                                    <th><g:pdfMessage
                                            code="highPharmacy.container3.table1.head1"/></th>
                                    <th><g:pdfMessage
                                            code="highPharmacy.container3.table1.head2"/></th>
                                    <th><g:pdfMessage
                                            code="highPharmacy.container3.table1.head3"/></th>
                                    <th style="width: 100px;"><g:pdfMessage
                                            code="highPharmacy.container3.table1.head7"/></th>
                                   %{-- <th><g:pdfMessage
                                            code="highPharmacy.container3.table1.head5"/></th>--}%
                                    <th style="width: 100px;"><g:pdfMessage
                                            code="highPharmacy.container3.table1.head4"/></th>
                                    <th style="width: 110px;"><g:pdfMessage
                                            code="highPharmacy.container3.table1.head6"/></th>
                                </tr>
                                <tr>
                                    <th><strong>Totals</strong></th>
                                    <th><strong>
                                        <g:formatZ5Currency number="${total.totalgenericBrandSavings}" type="number"
                                                            maxFractionDigits="2"/>
                                    </strong></th>
                                    <th><strong>
                                        <g:formatZ5Currency number="${total.totalmailRetailSavings}" type="number"
                                                            maxFractionDigits="2"/>
                                    </strong></th>
                                    <th><strong>
                                        <g:formatZ5Currency number="${total.totalPaidAmountPmpm}" type="number"
                                                            maxFractionDigits="2"/>
                                        PMPM</strong></th>
                                    %{--<th><strong></strong></th>--}%
                                    <th><strong>
                                        <g:formatZ5Currency number="${total.totalAnnualCost}" type="number"
                                                            maxFractionDigits="2"/>
                                    </strong></th>
                                    <th><strong>
                                        <g:formatNumber number="${total.totalPrescriptions}" type="number"
                                                        maxFractionDigits="2"/> prescriptions
                                        <g:formatNumber number="${total.totalPopulationSize}" type="number"
                                                        maxFractionDigits="2"/> members
                                    </strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:if test="${data}">
                                    <g:each in="${data}" var="data1" status="i">
                                        <tr>
                                            <td>
                                                <g:if test="${data1.genericOrBrand == "brand"}">
                                                    <strong style="text-transform: capitalize">${data1.drugName}</strong>
                                                    <br></br>
                                                    <span style="text-transform: lowercase">${data1.equvalentGenericDrug}</span>
                                                </g:if><g:else>
                                                <strong style="text-transform: lowercase">${data1.drugName}</strong><br></br>
                                                <span style="text-transform: lowercase">${data1.equvalentGenericDrug}</span>
                                            </g:else>

                                            </td>
                                            <td>
                                                <%
                                                    def max, min, val;
                                                    max = total.totalgenericBrandSavings;
                                                    min = data1.genericBrandSavings
                                                    try {
                                                        val = (min / max) * 50;
                                                    } catch (ArithmeticException e) {
                                                        val = 0
                                                    }
                                                %>
                                                <div style="height:10px;width:50px;background-color: #fff">
                                                    <div style="height:10px;width:${val}px;background-color: black"></div>
                                                </div>
                                                <g:formatZ5Currency number="${data1.genericBrandSavings}" type="number"
                                                                    maxFractionDigits="2"/></td>
                                            <td>
                                                <%
                                                    max = total.totalmailRetailSavings;
                                                    min = data1.mailRetailSavings
                                                    try {
                                                        val = (min / max) * 50;
                                                    } catch (ArithmeticException e) {
                                                        val = 0
                                                    }
                                                %>
                                                <div style="height:10px;width:50px;background-color: #fff">
                                                    <div style="height:10px;width:${val}px;background-color: black"></div>
                                                </div>
                                                <g:formatZ5Currency number="${data1.mailRetailSavings}" type="number"
                                                                    maxFractionDigits="2"/>
                                            </td>
                                            <td>
                                                <g:formatZ5Currency number="${data1.paidAmountPmpm}" type="number"
                                                                    maxFractionDigits="2"/> PMPM
                                            </td>
                                            %{--<td></td>--}%
                                            <td>
                                                <g:formatZ5Currency number="${data1.annualCost}" type="number"
                                                                    maxFractionDigits="2"/>
                                            </td>
                                            <td>
                                                <g:formatNumber number="${data1.prescriptions}" type="number"
                                                                maxFractionDigits="2"/> prescriptions<br></br>
                                                <g:formatNumber number="${data1.populationSize}" type="number"
                                                                maxFractionDigits="2"/> members
                                            </td>
                                        </tr>
                                    </g:each>
                                </g:if><g:else>
                                    <tr>
                                        <td colspan="8">
                                            No records to display.
                                        </td>
                                    </tr>
                                </g:else>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div></div>
        <div>
            <div style="padding-top: 35px;">
                <h1><g:pdfMessage code="highPharmacy.container1.table2.mTitle1"/></h1>
                <div class="sep-border"></div>
            </div>
            <div></div>
            <div>
                <h3>
                    Implement <strong>specialty pharmacy</strong> cost management strategies for a total of $ <strong><g:formatNumber
                            number="${total2.totalmailRetailSavings}" type="number"
                            maxFractionDigits="2"/></strong> in potential savings
                </h3>
                <div>${g3Filter}</div>
                <div>
                    <div class="z5-table" style="margin-top: 25px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <thead>
                            <tr>
                                <th>Drug Name</th>
                                <th><g:pdfMessage
                                        code="highPharmacy.container3.table1.head3"/></th>
                                <th><g:pdfMessage
                                        code="highPharmacy.container3.table1.head7"/></th>
                                %{--<th><g:pdfMessage
                                        code="highPharmacy.container3.table1.head5"/></th>--}%
                                <th><g:pdfMessage
                                        code="highPharmacy.container3.table1.head4"/></th>
                                <th><g:pdfMessage
                                        code="highPharmacy.container3.table1.head6"/></th>
                            </tr>
                            <tr>
                                <th><strong>Totals</strong></th>
                                <th><strong><g:formatZ5Currency
                                        number="${total2.totalmailRetailSavings}" type="number"
                                        maxFractionDigits="2"/></strong></th>
                                <th><strong><g:formatZ5Currency
                                        number="${total2.totalPaidAmountPmpm}" type="number"
                                        maxFractionDigits="2"/> PMPM</strong></th>
                                %{--<th></th>--}%
                                <th><strong><g:formatZ5Currency
                                        number="${total2.totalAnnualCost}" type="number"
                                        maxFractionDigits="2"/> PMPM</strong></th>

                                <th><strong>
                                    <g:formatNumber number="${total2.totalPrescriptions}" type="number"
                                                    maxFractionDigits="2"/> prescription<br></br>
                                    <g:formatNumber number="${total2.totalPopulationSize}" type="number"
                                                    maxFractionDigits="2"/> members
                                </strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:if test="${data1}">
                                <g:each in="${data1}" var="data2" status="i">
                                    <tr>
                                        <td>
                                            <g:if test="${data2.genericOrBrand == "brand"}">
                                                <strong style="text-transform: capitalize">${data2.drugName}</strong><br></br>
                                                <span style="text-transform: lowercase">${data2.equvalentGenericDrug}</span>
                                            </g:if>
                                            <g:else>
                                                <strong style="text-transform: lowercase">${data2.drugName}</strong><br></br>
                                                <span style="text-transform: lowercase">${data2.equvalentGenericDrug}</span>
                                            </g:else>
                                        </td>
                                        <td>
                                            <%
                                                max = total2.totalmailRetailSavings;
                                                min = data2.mailRetailSavings
                                                try {
                                                    val = (min / max) * 50;
                                                } catch (ArithmeticException e) {
                                                    val = 0
                                                }
                                            %>
                                            <div style="height:10px;width:50px;background-color: #fff">
                                                <div style="height:10px;width:${val}px;background-color: black"></div>
                                            </div>
                                            <g:formatZ5Currency number="${data2.mailRetailSavings}" type="number"
                                                                maxFractionDigits="2"/></td>
                                        <td>
                                            <g:formatZ5Currency number="${data2.paidAmountPmpm}" type="number"
                                                                maxFractionDigits="2"/> PMPM
                                        </td>
                                        %{--<td></td>--}%
                                        <td>
                                            <g:formatZ5Currency number="${data2.annualCost}" type="number"
                                                                maxFractionDigits="2"/> PMPM
                                        </td>
                                        <td>
                                            <g:formatNumber number="${data2.prescriptions}" type="number"
                                                            maxFractionDigits="2"/> prescriptions<br></br>
                                            <g:formatNumber number="${data2.populationSize}" type="number"
                                                            maxFractionDigits="2"/> members
                                        </td>
                                    </tr>
                                </g:each>
                            </g:if><g:else>
                                <tr>
                                    <td colspan="8">
                                        No records to display.
                                    </td>
                                </tr>
                            </g:else>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>