<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main" />
</head>
<body>
<div class="contentWrapper" >
    <head>
        <meta content="main" name="layout" />
        <title>Change Password</title>
        <script type="text/JavaScript">
            $(document).ready(function() {
                $.validator.addMethod('regex', function(value, element, param) {
                    return this.optional(element) ||
                            value.match(typeof param == 'string' ? new RegExp(param) : param);
                });
                $("#validateForm").validate({
                    rules: {
                        currentPassword:{
                            required: {
                                depends:function(){
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            }
                        },
                        newPassword1:{
                            required: {
                                depends:function(){
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            },
                            regex:/^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\W])(?=.*[\d]).*$/

                        },
                        newPassword2:{
                            required: {
                                depends:function(){
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            },
                            equalTo: "#newPassword1"
                        }

                    },
                    messages: {
                        currentPassword:{
                            required:"You must provide current password."
                        },
                        newPassword1:{
                            required:"You must provide new password.",
                            regex :"The password has to contain at least one digit, one special character, one lowercase letter and one uppercase letter.\nThe password length should be greater than 8 characters"
                        } ,
                        newPassword2: {
                            required: "You must enter confirm password",
                            equalTo: "Please enter the same password as above"
                        }
                    }
                });






            });

            function forceLogin(){
                jQuery.ajax({
                    type:'POST',
                    async:false,
                    url:'<g:createLink controller="logout" action="invalidateSession"/>',
                    success:function(resp){
                        $("#relogin").modal({i
                            backdrop: 'static',
                            keyboard: false
                        });
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                    },
                    complete:function(XMLHttpRequest,textStatus){
                    }});

            }
        </script>
    </head>

    <body>

    %{--<div class="container">--}%
    <div style="width:700px;margin-left: auto;margin-right: auto">
        <h3 class="weltitle">Change Password</h3>
        <g:if test="${flash.success}">
            <g:javascript>forceLogin();</g:javascript>
        </g:if>
        <g:if test="${flash.message}" >
            <div class="alert alert-warning" >${flash.message}</div>
        </g:if>
        <g:if test="${!flash.error}">
        <div id="wrapper_bg">
            <div id="wrapper">


                %{--<div style="color: #08c;">
                    The password has to contain at least one digit, one special character, one lowercase letter and one uppercase letter.<br> The password length should be greater than 8 characters
                </div>--}%
                <br>

                <g:form name="validateForm" action="authenticate" id="validateForm">
                    <table cellpadding="0" cellspacing="0" border="0" class="form-grid width-half">

            <tr> <td style="width:200px;"> <b>Old Password:<span class="errMsg">*</span> </b></td>

                    <td><g:passwordField name="currentPassword" class="required" id="currentPassword"/></td>
                    </tr>
                    <td><b>New Password:<span class="errMsg">*</span> </b></td>

                    <td><g:passwordField name="newPassword1" class="required" id="newPassword1"/></td>
                    </tr>
                    <td><b>Re-type New Password:<span class="errMsg">*</span> </b></td>
                    <td><g:passwordField name="newPassword2" class="required" id="newPassword2"/></td>

                    </tr>
                    <tr>
                        <td></td>
                        <td><div class="btns" style="float:left;"><input style="color: white;" type="submit" name="button" value="Change Password" class="btn btn-password" /></div></td>
                    </tr>
                    </table>
                </g:form>
            </div>
        </div>
        </g:if>

    </div>
</div>

<div id="relogin" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="sessionText">
                Password has been changed successfully.<br>
                Please login with your new password to continue.
            </div>
            <div class="modal-footer">
                <g:form controller="logout" action="index">
                    <button type="submit" class="btn btn-primary">Login</button>
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>





