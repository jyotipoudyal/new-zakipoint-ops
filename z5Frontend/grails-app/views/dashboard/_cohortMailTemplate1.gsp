<%@ page contentType="text/html" %>
<html>
<head>
    <title>Mail</title>
    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'layout.css')}">
    <link rel="stylesheet" type="text/css" href="${resource(dir: 'bootstrap/css', file: 'bootstrap.min.css')}">
</head>

<body>
<div class="mainWrapper">
    <div class="container">
        <div class="col-md-12">

            Hello Team,<br>
            This is a new cohort request from ${name} for group - ${client}. The details are as follows:
            <br>
            <br>



    <div class="z5-table">
        <table width="80%" border="0" cellspacing="5" cellpadding="5">

            <tbody>

            <tr>
                <td width="20%"></td>
                <td width="15%" style="text-align: right">User</td>
                <td ><a href="mailto:${user}" target="_top">${user}</a></td>
            </tr>

            <tr>
                <td ></td>
                <td style="text-align: right">Cohort Name</td>
                <td >${cohortName}</td>
            </tr>


            <tr>
                <td></td>
                <td style="text-align: right">Cohort Desc</td>
                <td>${cohortDesc}</td>
            </tr>

            </tbody>

        </table>
        <br>
        <br>
            Sincerly,<br> Z5 Support
    </div>
    </div>
    </div>

</div>




</body>
</html>