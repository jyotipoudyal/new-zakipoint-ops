<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
    <r:require module="graph"/>

</head>
<body>

<div class="contentWrapper">
    <div class="container">
        <div class="col-md-12">
            <g:if test="${flash.message}">
                <div class="alert alert-success text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    ${flash.message}
                </div>
            </g:if>
        </div>
        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
        <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                        <g:link controller="cms" action="index" title="Edit Content" ><i class="fa fa-edit"></i> Edit Content</g:link>
                </g:if><g:else>
                    <g:link controller="dashboard" action="index" title="View Changes" ><i class="fa fa-eye"></i> View Changes</g:link>
                </g:else>
        </sec:ifAllGranted>
            </li>
        </div>
        <div class="clearfix"></div>
        %{--<div class="col-sm-12 col-md-12 col-lg-6 col-sm-offset-0 col-md-offset-5 col-lg-offset-5 centered">--}%
            <div class="col-md-6 ">
                <img class="pull-right client-logo-adjustment" src="${createLink(controller:'dashboard', action:'clientLogo')}" />
            </div>
            <div class="col-md-3">
                <g:if test="${!isEditable}">
                                <h3 style="text-transform: capitalize">
                                    ${group?.groupName}
                                </h3>
                                <div>
                                %{--${group?.employeeCount} employees<br>--}%
                                ${group?.memberCount} active members
                                </div>
                </g:if>
            </div>

        <div class="clearfix"></div>
        %{--</div>--}%
    </div>
        <div class="container mt25" style="height: 140px;">
        <h1 class="hr-bg text-center "><span class="isEditable color-gray"><g:editMessage code="dashboard.container0.title"/></span></h1>
        <br>
        <div class=" col-md-4 " %{--onclick="fetchChartScript('render2')"--}%>
            <div class="isEditable col-md-5" ><g:editMessage code="dashboard.graph2.title"/>
               %{-- <div>
                    <label class="radio-inline"><input id="medPMPM" type="radio" name="medradio" checked="checked">PMPM</label>
                    <label class="radio-inline"><input id="medPaid" type="radio" name="medradio">Paid</label>
                </div>--}%

                <div style="    margin-top: -18px;">
                <div style="float: left">
                    <input type="radio" id="medPMPM" name="medradio" class="radio" checked="checked"/>
                    <label for="medPMPM">PMPM</label>
                </div>
                <div style="float: left;margin-left: 14px;">
                    <input id="medPaid" type="radio" name="medradio" class="radio"/>
                    <label for="medPaid">Paid</label>
                </div>
                </div>




            </div>
            <div class="col-md-7 pull-right graph-label-box">
                <div id="graph2Legend1" class=" col-md-12 graph-label formatOutput0"><span class="pull-right" ></span></div>
                <div id="graph2Legend2" class=" col-md-12  graph-label formatOutput1"><span  class="pull-right" ></span></div>
            </div>
            <div class="clearfix"></div><br><br>


            <div  id="graph2"class="col-sm-12 col-md-12 col-lg-8 graph"></div>
        </div>

        <div class=" col-md-4 border-lr"  %{--onclick="fetchChartScript('render1')"--}%>
            <div class="isEditable col-md-5" style="padding:0 0 0 0" ><g:editMessage code="dashboard.graph1.title"/>
                %{--<div>
                    <label class="radio-inline"><input id="pharPMPM" type="radio" name="pharradio" checked="checked">PMPM</label>
                    <label class="radio-inline"><input id="pharPaid" type="radio" name="pharradio">Paid</label>
                </div>--}%

                <div style="    margin-top: -18px;">
                    <div style="float: left">
                        <input type="radio" id="pharPMPM" name="pharradio" class="radio" checked="checked"/>
                        <label for="pharPMPM">PMPM</label>
                    </div>
                    <div style="float: left;margin-left: 14px;">
                        <input id="pharPaid" type="radio" name="pharradio" class="radio"/>
                        <label for="pharPaid">Paid</label>
                    </div>
                </div>

            </div>
            <div class=" col-md-7 graph-label-box">
                <div id="graph1Legend1" class="col-md-12 graph-label formatOutput0"><span  class="pull-right" ></span></div>
                <div id="graph1Legend2" class="col-md-12 graph-label formatOutput1"><span  class="pull-right" ></span></div>
            </div>
            <div class="clearfix"></div>
            <br>
            <br>

            <div id="graph1" class=" col-md-12  graph" style="padding-right:0px;padding-left: 0px;"></div>

        </div>

        <div class="col-md-4" %{--onclick="fetchChartScript('render3')"--}%>
            <div class="isEditable " ><g:editMessage code="dashboard.graph3.title"/>
            <span class="btn btn-success" style="margin-top: -3px;padding: 0 5px 0 5px;font-size: 10px;">
                <g:link controller="populationRisk" action="riskDetails"  params="[id:'1']" title="MARA RISK">MARA &nbsp;</g:link> |
                <g:link controller="populationRisk" action="riskDetails"  params="[id:'2']" title="HS RISK"> &nbsp; HS</g:link>
            </span>

            </div>
            <div class="" style="font-size: 11px;" >Prospective
            </div>

            <div class="clearfix"></div>
            <div id="graph3" class="col-sm-12 col-md-6 col-lg-8 graph"><span></span></div>
            <div class="col-sm-12 col-md-6 col-lg-4 graph-label pull-right" style="    margin-top: -32px;font-size: 13px !important;text-align: right; padding:0 0 0 0;">
                <div id="graph3-0" style="margin-top: -2px;" class="color-red graph3Legend "> </div>
                <div id="graph3-1" style="margin-top: 2px;" class="color-orange graph3Legend "> </div>
                <div id="graph3-2" style="margin-top: 2px;" class="color-gray graph3Legend "> </div>
                <div id="graph3-3" style="margin-top: 2px;" class="color-green graph3Legend "> </div>
            </div>
            <div class="col-md-2">&nbsp;</div>
        </div>
        <div class="clearfix"></div>
    </div>


    <div class="container mt15" >
    <div class="col-md-12" style="text-align: center">
<g:if test="${!isEditable}">
        <div> <g:render template="dashLogo"/></div>
</g:if><g:else>
        <g:render template="../dashboard/dashLogo"/>
</g:else>
    </div>
    </div>
   %{-- <div id="map"></div>
    <script>
        function initMap() {
            var uluru = {lat: -25.363, lng: 131.044};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjdcaHcc8vNr57PF3qYN1aEbNrzib2hpU&callback=initMap">
    </script>--}%




    %{-- <div class="container mt35" id="populationRisks">

         <h1 class="hr-bg text-center color-gray">
             <span><g:img uri="/images/icon-decline-in-health.png" class="heading-icon"/><span class="isEditable"><g:editMessage code="dashboard.container02.title"/></span></span></h1>
 <div class="col-md-2"></div>
     <div class="col-md-8 text-center" >
         <h1 class="isEditable" ><g:editMessage code="dashboard.graph4.title"/></h1>
         <div id="graph4" class="graph"></div>
     </div>
         <div class="col-md-2"></div>

     </div>

     <div class="container mt25 ">

         <h1 class="hr-bg text-center "><span class="isEditable color-gray">Program Tracking</span></h1>

         <div class="watermark"><div style="display: inline;text-transform: capitalize">
             &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
         </div>

         <br>
         <div class=" col-md-4">
             <div class="clearfix"></div>
             <div  id="pt1Graph" ></div>
             <div  id="pt1GraphContent"></div>
         </div>
         <div class=" col-md-4">
             <div class="clearfix"></div>
             <div  id="pt2Graph"></div>
             <div  id="pt2GraphContent"></div>
         </div>
         <div class=" col-md-4">
             <div class="clearfix"></div>
             <div  id="pt3Graph"></div>
             <div  id="pt3GraphContent"></div>
         </div>

     </div>--}%

    <div class="container mt25 dashboard-tiles"  id="costDrivers">
   %{-- <div >*****************</div>
    <div >*****************</div>
    <div id="map"></div>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script>
    var dataSourceUrl = "https://docs.google.com/spreadsheet/ccc?key=0Au4D8Alccn4xdHNJdXJmeTdYcEtpRXE1QXRucWtEN3c";
    var onlyInfoWindow;

    function getData() {
        alert("here")
        var query = new google.visualization.Query(dataSourceUrl);
        query.send(handleQueryResponse);
    }

    google.load("visualization", "1");
    google.load("maps", "3", {other_params:"sensor=false"});

    google.setOnLoadCallback(getData);

    function handleQueryResponse(response) {
        if (response.isError()) {
            alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
            return;
        }

        var data = response.getDataTable();

        var mapping = {
            disableDefaultUI:false,
            scrollWheel:false,
            zoom: 13,
            center: new google.maps.LatLng(40.785611,-73.946056),
            mapTypeId: google.maps.MapTypeId.TERRAIN

        };

        var map = new google.maps.Map(document.getElementById("map"), mapping);

  /*      var bubbleMap = new Bubble(data);

        bubbleMap.setMap(map);*/


        var mapStyles = [
            {
                featureType: "road",
                stylers: [
                    {visibility: "on"},
                    {lightness: +20},
                ]
            }

        ];

        map.setOptions({styles: mapStyles});

        var schoolDistricts = new google.maps.LatLng(41.850033, -87.6500523);

        var layer = new google.maps.FusionTablesLayer({
                    query: {
                        select: 'geometry',
                        from: '3621394'
                    },
                    styles: [{
                        polygonOptions: {
                            fillColor: "#FAFBFF",
                            fillOpacity: 0
                        }
                    }
                    ]}
        );

        layer.setMap(map);
    }

 /*   var CustomTileLayer = new google.maps.ImageMapType({
        getTileUrl: function(coord, zoom) {
            return "http://tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        isPng: true
    });

    map.overlayMapTypes.push(CustomTileLayer);

    var input = document.getElementById('search');
    mapCanvas.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var options = {
        types: ['establishment']
    };

    var autocomplete = new google.maps.places.Autocomplete(input,
            options);
    google.maps.event.addListener(autocomplete, "place_changed", function()
    {
        var place = autocomplete.getPlace();
        mapCanvas.panTo(place.geometry.location);
    });*/


    </script>--}%

    %{--<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'overlay_bubble.js')}"></script>--}%

    <h1 class="hr-bg text-center "><span class="isEditable color-gray"><g:editMessage code="dashboard.container.title"/></span></h1>
        <h2 class="text-center mb35 mt15 isEditable"><g:editMessage code="dashboard.container.subTitle"/></h2>



%{--==================================Cohort========================================--}%

    %{--<g:if test="${!session.isBiometrics}">
        <g:if test="${!isEditable}">
            <div class="not-active">
        </g:if><g:else>
        <div class="not-active-edit">
    </g:else>
        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            <div class="box-gray  color-blue">
                <p class="text-center" ><g:img style="height: 78px;" uri="/images/icon-explore-population.png" /></p>
                <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content11.title"/></h3>
                <p class="box-content isEditable"><g:editMessage code="dashboard.container.content11.desc"/></p>
            </div>
        </div>
        </div>
    </g:if><g:else>--}%

    %{--</g:else>--}%

    %{--//===================================Pharmacy =============================================--}%

            <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
        <g:link controller="highPharmacyCost">
                <div class="box-blue color-blue">
                    <p class="text-center "><g:img uri="/images/icon-high-pharmacy-cost.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content1.title"/></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content1.desc"/></p>
                </div>
        </g:link>
            </div>

    %{--//===================================use of er=================================================--}%


        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
        <g:link controller="erUtilization">
            <div class="box-blue color-blue">
                <p class="text-center"><g:img uri="/images/icon-overuse-er.png" /></p>
                <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content2.title"/></h3>
                <p class="box-content isEditable"><g:editMessage code="dashboard.container.content2.desc"/></p>
            </div>
        </g:link>
        </div>


    %{--//===================================population risks================================================--}%

        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
        <g:link controller="populationRisk" action="riskChart">
        <div class="box-blue  color-blue">
                <g:if test="${session.clientname=='empb' || session.clientname=='empb_dev'}">
                <div style=" float: right;margin-top: -39px;margin-right: -33px;" title="Newly Identified Members">
                    <svg width="40" height="40" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality;">
                        <g transform="translate(21,25)">
                            <circle r="14" fill="red"></circle>
                            <text dy="4" stroke="#fff"  text-anchor="middle"  >153</text>
                        </g>
                    </svg>
                </div>
                </g:if>

                <p class="text-center"><g:img uri="/images/icon-decline-in-health.png" /></p>
                <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content3.title"/></h3>
                <p class="box-content isEditable"><g:editMessage code="dashboard.container.content3.desc"/></p>
            </div>
        </g:link>
        </div>

%{--//===================================gaps in care==================================================--}%


        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            <g:link controller="dashboard" action="gapInCare">
                <div class="box-blue color-blue">
                    <p class="text-center autoImageDash"><g:img uri="/images/icon-high-cost-claimants.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content6.title"/></h3>
                    <p class="box-content isEditable "><g:editMessage code="dashboard.container.content6.desc"/></p>
                </div>
            </g:link>
        </div>

%{--//==================================network use================================================--}%



        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            <g:link controller="networkUse" action="index">
                    <div class="box-blue color-blue">
                        <p class="text-center autoImageDash"><g:img uri="/images/inefficientnetwork_icon.png" /></p>
                        <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content5.title"/></h3>
                        <p class="box-content isEditable"><g:editMessage code="dashboard.container.content5.desc"/></p>
                    </div>
            </g:link>
        </div>

%{--==========================biometrics================================================--}%

    %{--<g:if test="${!session.isBiometrics}">
        <g:if test="${!isEditable}">
            <div class="not-active">
        </g:if><g:else>
        <div class="not-active-edit">
    </g:else>
        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            <div class="box-gray  color-blue">
                <p class="text-center"><g:img uri="/images/icon-decline-in-health.png" /></p>
                <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content10.title"/></h3>
                <p class="box-content isEditable"><g:editMessage code="dashboard.container.content10.desc"/></p>
            </div>
        </div>
        </div>
    </g:if><g:else>--}%
        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            <g:link controller="biometrics" action="index">
                <div class="box-blue  color-blue">
                    <p class="text-center"><g:img uri="/images/icon-decline-in-health.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content10.title"/></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content10.desc"/></p>
                </div>
            </g:link>
        </div>
    %{--</g:else>--}%

    %{--===============================z5 population Explorer==============================================--}%


    <%
        def memberDrillService = grailsApplication.classLoader.loadClass('com.deerwalk.z5.MemberDrillService').newInstance()
        def helperService = grailsApplication.classLoader.loadClass('com.deerwalk.z5.HelperService').newInstance()
        def expPop=memberDrillService.getPopulationExplorer(helperService.getReportingDates(session.ceDate))
    %>

    <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
        <g:z5Link data-exp="true"  disabledLink="false" controller="memberSearch" action="searchResult" params="${expPop}">
            <div class="box-blue  color-blue">
                <p class="text-center" >
                    <g:img style="height: 78px;" uri="/images/icon-explore-population.png" />
                </p>
                <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content12.title"/></h3>
                <p class="box-content isEditable"><g:editMessage code="dashboard.container.content12.desc"/></p>
            </div>
        </g:z5Link>
    </div>

    <sec:ifAllGranted roles="ROLE_Z5ADMIN">


        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            <g:link controller="cohort" action="index">
                <div class="box-blue  color-blue">
                    <p class="text-center" ><g:img style="height: 78px;" uri="/images/icon-explore-population.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content11.title"/></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content11.desc"/></p>
                </div>
            </g:link>
        </div>
    </sec:ifAllGranted>



%{--//===================================procedure cost- imaging service==================================================--}%


    <div class="custom-tooltip">



    <g:if env="production">
        <g:if test="${!isEditable}">
            <div class="not-active">
        </g:if><g:else>
        <div class="not-active-edit">
        </g:else>
        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            %{--<g:link controller="dashboard" action="imagingServices">--}%
                <div class="box-gray color-blue">
                    <p class="text-center autoImageDash"><g:img uri="/images/imaging_icon.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content8.title"/></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content8.desc"/></p>
                </div>
            %{--</g:link>--}%
        </div>
        </div>
      </g:if>
    <g:else>
        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
            <g:link controller="dashboard" action="imagingServices">
                <div class="box-blue color-blue">
                    <p class="text-center autoImageDash"><g:img uri="/images/imaging_icon.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content8.title"/></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content8.desc"/></p>
                </div>
            </g:link>
        </div>
    </g:else>



%{--//=================================high cost claimants=============================================--}%


    <g:if test="${!isEditable}">
        <div class="not-active">
    </g:if><g:else>
        <div class="not-active-edit">
    </g:else>
    <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
        <div class="box-gray color-blue">
            <p class="text-center autoImageDash"><g:img uri="/images/gapsincare_icon.png" /></p>
            <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content4.title"/></h3>
            <p class="box-content isEditable"><g:editMessage code="dashboard.container.content4.desc"/></p>
        </div>
    </div>

        </div>



            <g:if test="${!isEditable}">
                <div class="not-active">
            </g:if><g:else>
                <div class="not-active-edit">
            </g:else>
            <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
                <div class="box-gray color-blue">
                    <p class="text-center autoImageDash"><g:img uri="/images/spousefamily_icon.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content7.title"/></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content7.desc"/></p>
                </div>
            </div>

    </div>



    %{--===========================================low program engagement=====================================================--}%



    <g:if test="${!isEditable}">
        <div class="not-active">
    </g:if><g:else>
        <div class="not-active-edit">
    </g:else>
            <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault">
                <div class="box-gray color-blue">
                    <p class="text-center autoImageDash"><g:img uri="/images/program_icon.png" /></p>
                    <h3 class="mt15 mb15 text-center isEditable"><g:editMessage code="dashboard.container.content9.title"/></h3>
                    <p class="box-content isEditable "><g:editMessage code="dashboard.container.content9.desc"/></p>
                </div>
            </div>
        </div>
    </div>




        %{--=======================================================================================--}%


        <div class="col-sm-12 col-md-4 col-lg-4 mb35 preventDefault"></div>


    </div>



    %{--<div class="container mt25">
        <h1 class="hr-bg text-center"><span class="isEditable"><g:editMessage code="dashboard.container2.title"/></span></h1>
        <div class="text-center mb15 isEditable"><a href="javascript:void(0)" class="custom-tooltip btn-link-gray"><g:editMessage code="dashboard.container2.button"/></a></div>
        <div class="text-center text-itallic mb35 isEditable"><g:editMessage code="dashboard.container2.alt"/></div>
        </div>--}%
   %{-- <div class="container mt25">
        <h1 class="hr-bg text-center"><span class="isEditable"><g:editMessage code="dashboard.container2.title"/></span></h1>
        <h2 class="text-center mb35 mt15 isEditable"><g:editMessage code="dashboard.container2.subtitle"/></h2>
        <div class="text-center mb15 isEditable"><a href="#" class="btn-link-upper"><g:editMessage code="dashboard.container2.button"/></a></div>
        <div class="text-center text-itallic mb35 isEditable"><g:editMessage code="dashboard.container2.alt"/></div>

        <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
            <h3 class="isEditable"><g:editMessage code="dashboard.container2.content1.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content1.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content1.li2"/></a>

            <h3 class="isEditable"><g:editMessage code="dashboard.container2.content2.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content2.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content2.li2"/></a><br />
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content2.li3"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content2.li4"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content2.li5"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content2.li6"/></a>
        </div>

        <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
            <h3 class="isEditable"><g:editMessage code="dashboard.container2.content3.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content3.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content3.li2"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content3.li3"/></a>

            <h3 class="isEditable" ><g:editMessage code="dashboard.container2.content4.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content4.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content4.li2"/></a>
        </div>

        <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
            <h3 class="isEditable"><g:editMessage code="dashboard.container2.content5.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content5.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content5.li2"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content5.li3"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content5.li4"/></a>

            <h3 class="isEditable"><g:editMessage code="dashboard.container2.content6.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content6.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content6.li2"/></a><br />
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content6.li3"/></a>
        </div>

        <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
            <h3 class="isEditable"><g:editMessage code="dashboard.container2.content7.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content7.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content7.li2"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content7.li3"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content7.li4"/></a><br/>
            <a class="isEditable" href="#" class="text-link-upper"><g:editMessage code="dashboard.container2.content7.li5"/></a>

            <h3 class="isEditable" ><g:editMessage code="dashboard.container2.content8.title"/></h3>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content8.li1"/></a><br/>
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content8.li2"/></a><br />
            <a class="isEditable" href="#"><g:editMessage code="dashboard.container2.content8.li3"/></a><br/>
        </div>
    </div>--}%










</div>

    <style>


    #credit {
        position: absolute;
        right: 4px;
        bottom: 4px;
        color: #ddd;
    }

    #credit a {
        color: #fff;
        font-weight: bold;
    }

    </style>

<g:javascript>

function fetchChartScript(val,type){
           var ajaxRequest=jQuery.ajax({
                type:'POST',
                data:{chart:val},
                async:false,
                url:'<g:createLink controller="dashboard" action="getChartScriptForDashBoard"/>',
                success:function(resp){
                if(resp){
                if(val=="render3"){
                render3(resp)
                }else{
                   window[val](resp,type);
                }

                }
                    //render4(resp);
                },
                error:function(XMLHttpRequest,textStatus,errorThrown){

                },
                complete:function(XMLHttpRequest,textStatus){

                }});

    }

    $(function(){
     if(!${isEditable}){
    $.each(['render1',"render2","render3"],function(i,v){
    fetchChartScript(v,"PMPM");
    });

        //renderFunnelBar(data1,"pt1Graph")
        //renderFunnelBar(data2,"pt2Graph")
        //renderFunnelBar(data3,"pt3Graph")

    }









    //fetchChartScript("renderFunnelBar","pt1Graph");
    //fetchChartScript("renderFunnelBar","pt2Graph");
    //fetchChartScript("renderFunnelBar","pt3Graph");


    });

    var data1= [
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Identified",
            "MemberCount": 1000,
            "Risk": "15",
            "PMPM": "18",
            "GapInCare": "15",
            "ERUtil": "15",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "15",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "15",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Enrolled",
            "MemberCount": 800,
            "Risk": "12",
            "PMPM": "10",
            "GapInCare": "12",
            "ERUtil": "2",
            "Admission": "2",
            "Biometric": "2",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "12",
            "RxUtilization": "2"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Not Enrolled",
            "MemberCount": 200,
            "Risk": "16",
            "PMPM": "8",
            "GapInCare": "16",
            "ERUtil": "16",
            "Admission": "16",
            "Biometric": "16",
            "MedAdherence": "6",
            "ClaimsOver10K": "6",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Engaged",
            "MemberCount": 600,
            "Risk": "-20",
            "PMPM": "-8",
            "GapInCare": "-20",
            "ERUtil": "-20",
            "Admission": "-2",
            "Biometric": "-20",
            "MedAdherence": "-10",
            "ClaimsOver10K": "-20",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-20"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Not Engaged",
            "MemberCount": 200,
            "Risk": "18",
            "PMPM": "4",
            "GapInCare": "18",
            "ERUtil": "8",
            "Admission": "18",
            "Biometric": "18",
            "MedAdherence": "18",
            "ClaimsOver10K": "18",
            "ClaimsUnder10K": "18",
            "RxUtilization": "8"
        }
    ]

    var data2=[
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Coaching",
    "Status": "Identified",
    "MemberCount": 600,
    "Risk": "15",
            "PMPM": "18",
            "GapInCare": "15",
            "ERUtil": "15",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "15",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "15",
            "RxUtilization": "15"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Coaching",
    "Status": "Enrolled",
    "MemberCount": 500,
    "Risk": "12",
            "PMPM": "10",
            "GapInCare": "12",
            "ERUtil": "2",
            "Admission": "2",
            "Biometric": "2",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "12",
            "RxUtilization": "2"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Coaching",
    "Status": "Not Enrolled",
    "MemberCount": 100,
    "Risk": "16",
            "PMPM": "8",
            "GapInCare": "16",
            "ERUtil": "16",
            "Admission": "16",
            "Biometric": "16",
            "MedAdherence": "16",
            "ClaimsOver10K": "6",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Coaching",
    "Status": "Engaged",
    "MemberCount": 400,
    "Risk": "-10",
    "PMPM": "-8",
    "GapInCare": "-2",
    "ERUtil": "-2",
    "Admission": "-2",
    "Biometric": "-20",
    "MedAdherence": "-5",
    "ClaimsOver10K": "-5",
    "ClaimsUnder10K": "-20",
    "RxUtilization": "-5"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Coaching",
    "Status": "Not Engaged",
    "MemberCount": 100,
    "Risk": "81",
    "PMPM": "4",
    "GapInCare": "5",
    "ERUtil": "15",
    "Admission": "8",
    "Biometric": "12",
    "MedAdherence": "18",
    "ClaimsOver10K": "1",
    "ClaimsUnder10K": "18",
    "RxUtilization": "8"
  }
]

    var data3=[
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Diabetes Management",
    "Status": "Identified",
    "MemberCount": 900,
    "Risk": "15",
    "PMPM": "18",
    "GapInCare": "15",
    "ERUtil": "51",
    "Admission": "15",
    "Biometric": "15",
    "MedAdherence": "18",
    "ClaimsOver10K": "15",
    "ClaimsUnder10K": "19",
    "RxUtilization": "15"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Diabetes Management",
    "Status": "Enrolled",
    "MemberCount": 700,
    "Risk": "12",
    "PMPM": "10",
    "GapInCare": "2",
    "ERUtil": "1",
    "Admission": "1",
    "Biometric": "1",
    "MedAdherence": "12",
    "ClaimsOver10K": "12",
    "ClaimsUnder10K": "1",
    "RxUtilization": "12"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Diabetes Management",
    "Status": "Not Enrolled",
    "MemberCount": 200,
   "Risk": "16",
            "PMPM": "8",
            "GapInCare": "16",
            "ERUtil": "6",
            "Admission": "16",
            "Biometric": "6",
            "MedAdherence": "6",
            "ClaimsOver10K": "26",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Diabetes Management",
    "Status": "Engaged",
    "MemberCount": 500,
    "Risk": "-20",
    "PMPM": "-8",
    "GapInCare": "-20",
    "ERUtil": "-20",
    "Admission": "-20",
    "Biometric": "-20",
    "MedAdherence": "-5",
    "ClaimsOver10K": "-5",
    "ClaimsUnder10K": "-20",
    "RxUtilization": "-20"
  },
  {
    "GroupId": "C001",
    "ClientID": "G001",
    "ProgramID": "P002",
    "ProgramName": "Diabetes Management",
    "Status": "Not Engaged",
    "MemberCount": 90,
    "Risk": "8",
    "PMPM": "4",
    "GapInCare": "81",
    "ERUtil": "8",
    "Admission": "28",
    "Biometric": "8",
    "MedAdherence": "8",
    "ClaimsOver10K": "25",
    "ClaimsUnder10K": "8",
    "RxUtilization": "28"
  }
]




    $(".watermark").show();








           %{-- render(${graph?.data1})--}%
           %{-- render2(${graph?.data2})--}%
          %{--  render3(${graph?.data3})--}%
           %{--  executeGraph('${resource(dir:"dData",file: "data1.json")}','${resource(dir:"dData",file: "data2.json")}','${resource(dir:"dData",file: "data3.json")}');--}%


    $("input[name=medradio]:radio").change(function () {
    if(this.id=="medPaid"){
            fetchChartScript("render2","Paid");
    }else{
            fetchChartScript("render2","PMPM");
    }

    });

    $("input[name=pharradio]:radio").change(function () {


    if(this.id=="pharPaid"){
        fetchChartScript("render1","Paid");
    }else{
        fetchChartScript("render1","PMPM");
    }

    });

</g:javascript>

    <style>

    .tick text{
        font-size:9px !important;
    }

    .text_y{
        font-size: 9px;;
    }

    .redfill{
        fill:#EC3E40;

    }

    .greenfill{
        fill:#01A46D
    }

    .bluefill{
        fill: #5E9BCD;
    }


.axis path,
.axis line {
    fill: none;
    stroke: #000;
    shape-rendering: crispEdges;


}

    .dashboard-tiles,.dashboard-tiles:hover,.dashboard-tiles:-webkit-any-link{
        text-decoration: no-underline;
    }


    .graph3Legends:hover {
        font-size: 14px;
        cursor:pointer;
    }

    .graph-label-box {
        margin-top: 8px;
    }

    /* hide input */
    input.radio:empty {
        margin-left: -999px;
    }

    /* style label */
    input.radio:empty ~ label {
        position: relative;
        float: left;
        line-height: 14px;
        text-indent: 18px;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        color: #777;
    }

    input.radio:empty ~ label:before {
        position: absolute;
        display: block;
        top: 0;
        bottom: 0;
        left: 0;
        content: '';
        width: 15px;
        background: #D1D3D4;
        border-radius: 4px 4px 4px 4px;

    }

    /* toggle hover */
    input.radio:hover:not(:checked) ~ label:before {
        content:'\2714';
        text-indent: 3px;
        color: #C2C2C2;
    }

    input.radio:hover:not(:checked) ~ label {
        color: #000;
    }

    /* toggle on */
    input.radio:checked ~ label:before {
        content:'\2714';
        text-indent: 3px;
        color: #fff;
        background-color: steelblue;
    }

    input.radio:checked ~ label {
        color: #000;
    }

        .xaxisLeft path{
            display:none;
        }

    </style>

    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">

</body>
</html>