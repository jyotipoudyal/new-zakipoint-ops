<html xmlns="http://www.w3.org/1999/xhtml">
<div class="col-md-12"><div class="reporting-detail-wrapper"><table border="0" class="reportingD_details" width="100%">
    <tr>
        <td class="report-label" style="width: 195px;">Reporting Period :</td>
        <td style="width: 140px;"><g:formatDate format="MMM yyyy" date="${Date.parse('yyyy-MM-dd', parameters.reportingFrom)}"/> to <g:formatDate format="MMM yyyy" date="${Date.parse('yyyy-MM-dd', parameters.reportingTo)}"/> </td>
        %{--<td class="report-label">Reporting Through :</td>  <td><g:formatDate format="MMM yyyy" date="${Date.parse('yyyy-MM-dd', parameters.reportingTo)}"/> </td>--}%
        <td class="report-label" style="width: 116px;" >Reporting Basis :</td> <td style="width: 60px;">${parameters.reportingBasis=="PaidDate"?"Paid":"Incurred"}</td>

        <td class="report-label" style="width: 270px;">Excluding population with cost greater than : </td>
        <td style="width:50px;">
        <g:if test="${parameters?.range}">
                <g:if test="${parameters.range.toInteger()<=0 || !parameters.isExclude}">
                    None
                </g:if><g:else>
                $ ${parameters.range.toInteger()/1000} k
            </g:else>
        </g:if><g:else>
            None
        </g:else>

        </td>

        <td rowspan="2" class="viewing-sep" style="width: 223px;">
                Viewing Client :<br>

                %{--<g:editMessage code="client.${session.clientname}"/>--}%
            %{--<img class="autoImage profile-img" src="${createLink(controller:'dashboard', action:'clientLogo')}" />--}%
            ${params.groupName}
            <a href="#" class="reporting-detail-toggle toggle-expanded">
                &nbsp;<i class="fa fa-chevron-up"></i>&nbsp;
            </a>
        </td>
        </tr>
    <tr>
        <td class="report-label">Members with Enrollment Info :</td><td>${parameters.overAllMemberCount}</td>

        <td class="report-label">Active Members :</td><td> ${parameters.eligibleMemberCount}</td>
                <td colspan="2"></td>


        %{-- <td colspan="2" class="text-left">
             Viewing Client : <g:editMessage code="client.${session.clientname}"/>


         </td>--}%
    </tr>

</table></div></div>

</html>
%{--

--}%
