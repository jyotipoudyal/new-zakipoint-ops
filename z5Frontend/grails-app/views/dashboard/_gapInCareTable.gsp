<div id="gapInCareSec"  class=" z5-table put-border mb25" style="max-height: 450px;min-height:450px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id="recommendedCareForDiabetes">
<thead>
<tr style="font-weight: bold;color: #044f96">
<th>Category</th>
<th>Members In Group (#)</th>
<th>% Compliance</th>
<th>Met (#)</th>
<th>Not Met (#)</th>
%{--<th>$ PMPY</th>--}%
        %{-- <util:remoteSortableColumn  class="text-center"  property="membersInGroup" title="Members In Group (#)" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />
         <util:remoteSortableColumn  class="text-center ${defaultSort}"  property="percentMet" title="% Compliance" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />
         <util:remoteSortableColumn  class="text-center"  property="met" title="Met (#)" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />
         <util:remoteSortableColumn  class="text-center"  property="notMeeting" title="Not Met (#)" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />
         <util:remoteSortableColumn style="width:110px;" class="text-center"  property="costPmpy" title="\$ PMPY" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />--}%
    </tr>
    </thead>
    <tbody>
    <g:each in="${arr}" var="cat" status="i">
        <%
            Integer rowNum=(i%2);
            def sortedData=careFor.findAll{it.category==cat}.sort{it.description};
//                   sortedData.sort{ a,b -> (((a.name =~ /\d+/)[-1] as Integer)  <=> ((b.name =~ /\d+/)[-1] as Integer))}//for sorting alphanumeric
        %>
        <tr class="parent-row z5-row-${rowNum}" id="parent-row-${i}" name="${cat.split(" ").join("-")}"  style="cursor: pointer;">
            <td colspan="6">
                <b>${cat}</b></td>
        </tr>
        <g:each in="${sortedData}" var="val">
            <tr class="z5-row-2 child-row-${i}" title="${val.title}" >
                <td>${val?.description.toString().capitalize()}</td>
                <td class="text-right">
                    ${val?.membersInGroup}  -- ${val?.name}
                </td>
                <td class="text-right"><g:formatNumber number="${val?.percentMet}" type="number" maxFractionDigits="1" />%</td>
                <td class="text-right">${val?.met} </td>
                <td class="text-right ">
                        <g:z5Link class="color-red" disabledLink="${(val?.notMeeting)<=0?'true':'false'}" controller="memberSearch" action="searchResult" params="${val?.members_drill}">
                    ${val?.notMeeting}
                    </g:z5Link>
                </td>
                %{--<td  class="text-right">
                    <g:formatZ5Currency number="${val.costPmpy?val.costPmpy:0}" type="number" maxFractionDigits="2" />
                </td>--}%
            </tr>
        </g:each>
    </g:each>
    </tbody>
</table>
</div>
<div class="clearfix"></div>

<div class="sep-border mb25 "></div>
<div class=" mb25 "></div>
%{--<div>
    <h6 class="has-filter"><g:select id="gapInCareRows" name="gapInCareRows" from="${arr}" onchange="renderScatterPlot(this.value)" /></h6>
    <div id="graph4"></div>
</div>--}%

<script>
    $(function(){
        $(".z5-row-2").hide();
        $(".parent-row-0").addClass("row-highlight");
        $(".child-row-0").show();
        $(".parent-row").on("click",function(){
            var $this=$(this);
            var text=$this.find('b').text();

            /*$.each($(".parent-row"),function(){
                $(this).removeClass("row-highlight");
            })*/

            var id=this.id.split("-")[2];
            $(".z5-row-2").hide();
            $(".child-row-"+id).slideDown();
            $("#gapInCareRows").val(text);
//            renderScatterPlot(text);
//            $this.addClass("row-highlight");
        })
    })
    var arrays=${careForData};

//    renderScatterPlot("Additional Gaps")

    function renderScatterPlot(ele){
        var newArr=arrays.filter(function(k,v){
            return ele == k.category
        })


        $("#graph4").html("");

        var main_margin = {top: 20, right: 50, bottom: 50, left: 95},
                main_width = 850 - main_margin.left - main_margin.right,
                main_height = 400 - main_margin.top - main_margin.bottom;
        var newVal;
        var data = [];
        var parseDate4 = d3.time.format("%Y-%m-%d").parse;


        $.each(newArr, function (key, val) {
            newVal = new Object();
            newVal.time1 =+val.membersInGroup;
            newVal.actual =+val.percentMet
            newVal.population =+val.percentMet
            newVal.radius =4
            newVal.cost =+val.costPmpy
            newVal.name =val.category
//            newVal.metricCode =val.name
            newVal.metricCode =val.code
            newVal.description =val.description
            newVal.met =val.met
            newVal.notMeeting =val.notMeeting
            data.push(newVal);

        });



        var main_x = d3.scale.linear()
                .range([0, main_width]);

        var actualDatas=data;
        actualDatas.sort(function(a, b) { return d3.ascending(a.time1, b.time1); });

        main_x.domain([0, actualDatas[actualDatas.length - 1].time1*1.2]);

        var main_radius=d3.scale.linear().range([6,24])

        var main_y = d3.scale.linear()
                .range([main_height, 0]);

        var main_xAxis = d3.svg.axis()
                .scale(main_x)
                .outerTickSize(0)
                .tickFormat(function(d,i){ return "k"; })
                .orient("bottom");

        /*var colorScale = d3.scale.quantize()
         .domain([0,data.length])
         .range(colors);*/



        var main_yAxisLeft = d3.svg.axis()
                .scale(main_y)
                .ticks(5)
                .tickSize(-main_width)
            //.tickFormat(function(d){return d.actual/1000+"k";})
                .outerTickSize(0)
                .orient("left");

        var main_line0 = d3.svg.line()
                .x(function (d) {
                    return main_x(d.time1);
                })
                .y(function (d) {
                    return main_y(d.population);
                });

        var svg = d3.select("#graph4").append("svg")
                .attr("width", main_width + main_margin.left + main_margin.right)
                .attr("height", main_height + main_margin.top + main_margin.bottom);

        svg.append("defs").append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("width", main_width)
                .attr("height", main_height);

        var main = svg.append("g")
                .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");

        var value1 = Math.round(data[data.length - 1].population);


        main_radius.domain(d3.extent(data, function (d) {
            return d.radius;
        }));

        var array_y0 = d3.extent(data, function (d) {
            return d.population;
        });
        main_y.domain([0,d3.max(array_y0)*1.2]);

        var main_xAxis2 = d3.svg.axis()
                .scale(main_x)
                .outerTickSize(0)
                .ticks(5)
                .tickFormat(function(d,i){ return d })
                .orient("bottom");

        main.append("g")
                .attr("class", "x axis xaxisLeft")
                .attr("transform", "translate(0,"+ (main_height) + ")")
                .call(main_xAxis2)
                .selectAll("path")
                .attr("stroke", "#ccc");


        main.append("g")
                .attr("class", "y axis yaxisLeft")
            //.attr("transform", "translate("+ (-20) + ",0)")
                .call(main_yAxisLeft)
            //.selectAll("line")
            //.attr("stroke", "#ccc")
                .selectAll("text")
            //.attr("y", 6)
                .attr("x", -35)
        //.style("text-anchor", "start");

        main.selectAll(".tick")
                .attr("stroke-dasharray", "3 3")
                .attr("opacity", ".5");

        main.append("text")
                .attr("class", "text_y")
                .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                .attr("transform", "translate(" + (-80) + "," + 157 + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                .text("% Of Meeting Criteria");

        main.append("text")
                .attr("class", "text_y")
                .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                .attr("transform", "translate(" + 316 + "," + (main_height+38 ) + ")")  // text is drawn off the screen top left, move down and out and rotate
                .text("Members In Group");



        main.selectAll("line")
                .attr("stroke", "#ccc");


        var div2 = d3.select(".contentWrapper").append("div")
                .attr("class", "tooltip3")
                .style("opacity", 0)
                .style("width","auto")

        var outerCircle=main.append("g").attr("class", "outerCircle");



        main.append('g')
                .selectAll('circle')
                .data(data)
                .enter()
                /*.append("svg:a")
                .attr("xlink:href", function(d,i){return "index/"+ d.metricCode;})*/
                .append('circle')
                .attr("class", "y2")
                .attr("id",function(d,i){
                    return "y2_"+d.metricCode;
                })
                .attr("fill",function(d,i){
                    return "steelblue"
                })
                .attr("stroke",function (d) {

                    return "black";})
                .attr("stroke-width","1px")
                .attr("cx", function (d) {
                    return main_x(d.time1);
                })
                .attr("cy", function (d) {
                    return main_y(d.population);
                })
                .attr("r", function (d) {
                    return 0;
                }).on("mouseenter", mouseIn).on("mouseout",mouseOut).transition()
                .duration(1000)
                .attr("r", function (d) {
                    return main_radius(d.radius);
                })

        function mouseOut(d) {
            d3.select("#y2_"+d.metricCode).attr("class", "y2").attr("fill","steelblue");
            div2.transition()
                    .duration(100)
                    .style("opacity", 0);
            d3.select("g.outerCircle").remove();
            outerCircle=main.append("g").attr("class", "outerCircle");
        }

        function mouseIn(d,i) {
            //d3.select(this).attr("class", "y0").attr("r",main_radius(d.radius)+1)
            //var prefix = d3.formatPrefix(Math.round(d.radius));
            d3.select("#y2_"+d.metricCode).attr("class", "y2-hover").attr("fill","steelblue");
            var p = $("#y2_"+d.metricCode);
            var position = p.offset();
            var left = position.left;
            var top = position.top;
            div2.transition()
                    .duration(100)
                    .style("opacity",1)
                    .style("color","black")
                    .style("border","1px solid steelblue");

            var topPosition=205;

            var table="<table class='tooltip-table'>" +
                    "<tr><td class='text-center formatOutput3' colspan='2'><strong>"+ d.description+"</strong></td></tr>" +
                    "<tr><td class='text-rights'>Members count</td><td> "+d.time1+"</td></tr>" +
                    "<tr><td class='text-rights'>% Compliance</td><td> "+d.actual.toFixed(1)+"</td></tr>";

            table+="<tr><td class='text-rights'>Met</td><td> "+d.met+" </td></tr>"+
            "<tr><td class='text-rights'>Not Met</td><td> "+d.notMeeting+" </td></tr>"+
//            "<tr><td class='text-rights'>Total Paid - Predicted for Next Year</td><td> $"+(Math.round(d.projectedCostActual)/1000).toFixed(2)+" K</td></tr>"+
            "</table>"

            div2.html(table)
                    .style("left", (left+10) + "px")
                    .style("top", (top-topPosition) + "px");





            outerCircle.append("circle").attr({
                cx: main_x(d.time1),
                cy: main_y(d.population),
                r: main_radius(d.radius)+4,
//                fill: "none",
                shapeRendering:"optimizeSpeed",
                stroke: "rgba("+colorOpa[1]+", "+colorOpa[2]+", "+colorOpa[3]+", 1)",
                strokeWidth:"2"
            });

        }


        /*var datass=data.sort(function(a, b) { return d3.ascending(a.name, b.name); });
        var legend = main.selectAll(".legend")
                .data(datass)
                .enter()
                .append("svg:a")
                .attr("xlink:href", function(d,i){return "index/"+ d.metricCode;})
                .append("g")
                .attr("class", "legend")
                .attr("width", 100)
                .attr("transform", function (d, i) {
                    return "translate(20," + (i * 30) + ")";
                }).on("mouseenter", mouseIn).on("mouseout",mouseOut);

        legend.append("rect").attr("class", "legend")
                .attr("x", main_width)
                .attr("width", 18)
                .attr("height", 18)
                .style("fill", function (d) {
                    return "steelblue"
                })

        legend.append("text")
                .attr("x", main_width+25)
                .attr("y", 14)
            //.attr("dy", ".35em")
                .style("text-anchor", "start")
                .style("fill", function (d) {
                    return "steelblue"
                })
                .attr("font-size", "11px")
                .text(function (d) {
                    return d.name;
                });*/
    }
</script>

