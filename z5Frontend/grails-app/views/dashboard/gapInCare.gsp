<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Gap In Care</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<body>


<div  id="contentWrapper" class="contentWrapper">

    <div class="container double-arrow-back-top-container">
        <div class="col-md-6 double-arrow-back-top">
        <span class="isEditable"> <g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
                                <g:editMessage code="gapInCare.allCostDrivers"/>
            </g:link></span>
        </div>
        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
            <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                    <g:link title="Edit Content"  controller="cms" action="gapInCare"><i class="fa fa-edit"></i> Edit Content</g:link>
                </g:if><g:else>
                <g:link title="View Changes"  controller="dashboard" action="gapInCare"><i class="fa fa-eye"></i> View Changes</g:link>
                </g:else>
            </sec:ifAllGranted>
            </li>
        </div>



        <%
            //        split page
        %>



        <div class="col-md-12">
            <h2 class="mb15">
                <g:img uri="/images/icon-high-cost-claimants.png" class="heading-icon"/>
                <span class="isEditable"><g:editMessage code="gapInCare.title"/></span></h2>
        </div>
        <div class="clearfix"></div>

        <div class="sep-border mb25 "></div>

        <div class="col-md-12 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="gapInCare.explore.population"/></span><input type="hidden" value="hPharmacy.container3.table1.options1"></a>
            <a href="#"  class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="gapInCare.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>
        </div>

            <div class="col-md-6">
                <h6 class="has-filter">
                <span id="graph1Filter">
                    <g:if test="${!isEditable}">
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasis1"
                                                                          from="${['PaidDate': 'paid', 'ServiceDate': 'incurred']}"
                                                                          optionKey="key" optionValue="value"
                                                                          onchange="refreshReport()"/></span>
                        claims </span>
                        that <span style="color: steelblue;"><g:select style="width:75px;" name="includeOn"
                                                                   from="${['false': 'include', 'true': 'exclude']}"
                                                                   optionKey="key" optionValue="value"
                                                                   onchange="refreshReport()"/></span>
                        cases over <span style="color: steelblue;"><g:select style="width:65px;" name="range1"
                                                                             from="${['0': '$0', '10000': '$10 k', '25000': '$25 k', '50000': '$50 k', '100000': '$100 k']}"
                                                                             optionKey="key" optionValue="value"
                                                                             onchange="refreshReport()"/></span>
                        per year
                    </g:if>
                </h6>
            </div>

        <div class="has-filter col-md-6">
            <span class="f-right custom-tooltip">
                <span class=" isEditable " ><g:editMessage code="gapInCare.container1.methodology" /></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>

        </div>



        <div  class="col-md-12">
            <g:if test="${!isEditable}">
                        <g:render template="gapInCareTable" />
            </g:if>
        </div>
        %{--<div  class=" z5-table put-border">

                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" >
                <thead>
                <tr>
                    <th>Category</th>
                    <th>Metric</th>
                    <th>Member In Group</th>
                    <th>Meeting Metric</th>
                    <th>Not Meeting Metric</th>
                    <th>%</th>

                    --}%%{--<th>Metric Type</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Population</th>
                    <th>Numerator</th>--}%%{--
                </tr>
                </thead>
                <tbody>
                <g:each  in="${careFor}" var="data">
                    <tr>
                        <td style="text-transform: capitalize">${data.value.metricType}</td>
                        <td>${data.value.name}</td>
                        <td>${data.value.description}</td>
                        <td>${data.value.category}</td>
                        <td>${data.value.population}</td>
                        <td>${data.value.numerator}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>--}%

    </div>
    <%
        //        split page
    %>
</div>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">

<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<script>
    $(document).ready(function() {
        var $table = $('table.z5-fixed-header');
        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.z5-table');
            }
        });

    });

    function refreshReport() {
        var reportingBasis, fromDate, range, isExclude;
        reportingBasis = $("#reportingBasis1").val();
//            fromDate = $("#fromDate1").val();
        isExclude = $("#includeOn").val();
        range = ($("#range1").val());

        jQuery.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'lineGraph1'},
            url: '<g:createLink controller="dashboard" action="gapInCare"/>',
            success: function (resp) {
                $("#gapInCareSec").html(resp);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            complete: function (XMLHttpRequest, textStatus) {
            }
        });

    }
</script>
</body>
</html>