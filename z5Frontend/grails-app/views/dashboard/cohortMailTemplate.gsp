<%@ page contentType="text/html" %>
<html>
<head>
    <title>Mail</title>
    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'layout.css')}">


</head>

<body>
<div class="mainWrapper">
    <div class="headerWrapper">
        <div class="container client-box-wrapper">
            <div class="col-sm-12 col-md-4 col-lg-4">
                <g:link controller="dashboard" title="zakipoint Health" class="logo">
                    <g:img dir="images" file="logo-zakipoint-large.png"/>
                %{--<img src="images/logo-zakipoint-large.png" class="img-responsive" alt="zakipoint Health" />--}%
                </g:link></div>
        </div>
    </div>

    <div class="z5-table">
        <table width="50%" border="0" cellspacing="0" cellpadding="0">

            <tbody>
            <tr>
                <td>Name</td>
                <td>${name}</td>
            </tr>

            <tr>
                <td>Cohort Name</td>
                <td>${cohortName}</td>
            </tr>

            <tr>
                <td>Cohort Desc</td>
                <td>${cohortDesc}</td>
            </tr>




            </tbody>

        </table>
    </div>

</div>


</body>
</html>