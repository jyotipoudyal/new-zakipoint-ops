<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main" />
</head>
<body>
<div class="contentWrapper" >
<ul style="margin-left: 25px;list-style: outside!important;">
    <g:if test="${urls}">
        <g:set value="${urls.size()}" var="numberOfUrls"></g:set>
        <g:each in="${urls}" var="url" status="i">
            <li style="list-style: decimal!important;">
                <g:if test="${(i+1)==(numberOfUrls)}">
                    <span style="color:#ff3300!important;">${url.split('::')[0]}::${url.split('::')[1]}::</span>
                    <g:link controller="request" action="viewResponse" style="color:#ff3300!important;" params="[url: url.split('::')[2]]">${url.split('::')[2]}</g:link>
                    <br><br>
                </g:if>
                <g:else>
                    <span style="color:#000!important;">${url.split('::')[0]}::${url.split('::')[1]}::</span>
                    <g:link controller="request" style="color:#000!important;" action="viewResponse" params="[url: url.split('::')[2]]">${url.split('::')[2]}</g:link>
                    <br><br>
                </g:else>
            </li>
        </g:each>
        </ul>
    </g:if>
    <div style="float:right; margin-right: 30px; margin-bottom: 15px;">
        <g:form action="checkUrl" method="post">
            <g:hiddenField name="clearAll" value="true"/>
            <input type="submit" value="Clear All">
        </g:form>
    </div>
</div>
</body>
</html>





