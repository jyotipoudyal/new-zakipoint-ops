<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Imaging Services</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'leaflet.css')}">
    <script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'leaflet.js')}"></script>
    %{--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />--}%
    %{--<script src="https://unpkg.com/leaflet@1.0.1/dist/leaflet.js"></script>--}%
    <script>


        function renderGraph(id,data,bigGraph,providerGraph,isCVG){
            $("#"+id).html("");
            if(isCVG){
                $("#enlarge-graph").modal("show");
            }

//            var margin = {top: 50, right: 20, bottom: 70, left: 40},
            var margin, x,width,height;
            if(bigGraph){
                margin = {top: 30, right: 30, bottom: 60, left: 50},
                        width = 600 - margin.left - margin.right,
                        height = 300 - margin.top - margin.bottom;

                x = d3.scale.ordinal().rangeRoundBands([0, width], .5);
            }else{
                margin = {top: 5, right: 2, bottom: 7, left: 4},
                        width = 150 - margin.left - margin.right,
                        height = 40 - margin.top - margin.bottom;

                x = d3.scale.ordinal().rangeRoundBands([0, width], .7);
            }






            var y = d3.scale.linear()
                    .rangeRound([height, 0]);



            var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom")
                    if(!bigGraph)
                        xAxis.tickFormat("");

            var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .ticks(4)
                    if(!bigGraph)
                    yAxis.tickFormat("")

           /* if(bigGraph)
                yAxis
                    .innerTickSize(-width)
                    .outerTickSize(0)
                    .tickPadding(10);*/

            var svg = d3.select("#"+id).append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform","translate(" + margin.left + "," + margin.top + ")");

            if(providerGraph){
                data=data.reduce(function(acc,item){
                    var mapp={};
                    mapp.x=item["Provider ID"];
                    mapp.y=item["Cost per proecdure"];
                    mapp.loc=item["Provider Location"];
                    acc.push(mapp);
                    return acc;
                },[]);

//                data=data.splice(0,10) // index,howmany

            }








            x.domain(data.map(function(d) { return d.x; }));
            y.domain([0, d3.max(data, function(d) {return d.y; })]);



                var xAxis=svg.append("g")
                        .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis)



                if(providerGraph){
                    xAxis.selectAll("text").style("text-anchor", "end")
                            .attr("dx", "-.8em")
                            .attr("dy", ".15em")
                            .attr("transform", function(d) {
                                return "rotate(-55)"
                            });
                }



//                        .style("text-anchor", "end")
//                        .attr("dx", ".4em")
//                        .attr("dy", ".55em")
//                        .attr("transform", "rotate(-90)" );

                svg.append("g")
                        .attr("class", "y axis")
                        .call(yAxis)
//                        .append("text")
//                        .attr("transform", "rotate(-90)")
//                        .attr("y", 6)
//                        .attr("dy", ".71em")
//                        .style("text-anchor", "end")
//                        .text("Value ($)");



            var mainGraph=svg.selectAll("bar")
                    .data(data)
                    .enter().append("rect")
                    .style("fill", "steelblue")
                    .attr("x", function(d) { return x(d.x); })
                    .attr("width", x.rangeBand())
                    .attr("y", function(d) { return y(d.y); })
                    .attr("height", function(d) { return height - y(d.y); })


            if(bigGraph){
                mainGraph.on("mouseout", function () {
                    div2.transition()
                            .duration(500)
                            .style("opacity", 0);
                }).on("mousemove", function(d){
                            div2.transition()
                                    .duration(100)
                                    .style("opacity", 1)
                                    .style("color", "black")

                    var text="<span class='formatOutput'>"+ d.x+" &nbsp;:&nbsp; "+ d.y+"</span>";
                    if(providerGraph){
                        text="Cost per procedure:<span class='formatOutput'>$"+ d.y+"</span><br>Location:<span class='formatOutput'>"+ d.loc+"</span>";
                    }

                            div2.html(text)/*<span class='formatOutput'>"+ d.x+":</span>&nbsp;&nbsp;*/
                                    .style("left", (d3.event.pageX - 10) + "px")
                                    .style("top", (d3.event.pageY -100) + "px");
                        });
            }
        }
    </script>
</head>
<body>
<div  id="contentWrapper" class="contentWrapper">

    <div class="container double-arrow-back-top-container">
        <div class="col-md-6 double-arrow-back-top">
            <g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
                <span class="isEditable"><g:editMessage code="procCost.allCostDrivers"/></span>
            </g:link>
        </div>
        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
                <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                    <g:if test="${!isEditable}">
                        <g:link title="Edit Content"  controller="cms" action="imagingService"><i class="fa fa-edit"></i> Edit Content</g:link>
                    </g:if><g:else>
                    <g:link title="View Changes"  controller="dashboard" action="imagingServices"><i class="fa fa-eye"></i> View Changes</g:link>
                </g:else>
                </sec:ifAllGranted>
            </li>
        </div>

        <div class="clearfix"></div>
        <div class="col-md-12">
            <h2 class="mb15">
                <span class="isEditable"><g:editMessage code="procCost.title"/></span></h2>
            <div class="watermark mb15"><div style="display: inline;text-transform: capitalize">
                &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
            </div>
            <div class="sep-border mb15"></div>

        </div>


        %{--<div class="sep-border mb25 pt15"></div>--}%

        <%
            //        split page
        %>

        <div class="col-md-8">
            <h3 ><span id="tableHead1" class="isEditable"></span></h3>

        </div>
        <div class="col-md-4 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="procCost.explore.population"/></span><input type="hidden" value="hPharmacy.container3.table1.options1"></a>
            <a href="#"  class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="procCost.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>


        %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
        </div>


        <div class="col-md-12 mb15">
            <div class="has-filter">
                <span class="f-right">
                    <span class=" isEditable custom-tooltip" ><g:editMessage code="procCost.container1.methodology" /></span>
                    <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
                </span>

            </div>

        </div>
        <g:if test="${!isEditable}">

        <div class="col-md-4"></div>
        <div class="col-md-8 mb15" >
            <div style="float: right;">
                Filter By
                <button class="btn">Age > 41</button>
                <button class="btn">Gender</button>
                <button class="btn">Location</button>
                <button class="btn">Relationship</button>
                <button class="btn">Division</button>
            </div>
        </div>


        <div  class=" z5-table put-border"  style="min-height: 500px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" >
                <thead>
                <tr>
                    <th><span class="isEditable">Procedure Group</span>
                    </th>
                    <th><span class="isEditable">Procedure Type</span></th>
                    <th><span class="isEditable">Number of Events</span></th>
                    <th><span class="isEditable">Total Costs</span></th>
                    <th><span class="isEditable">Average Cost/Event (annual increase)</span></th>
                    <th><span class="isEditable">Top Providers</span></th>
                    <th style="width: 150px;"><span class="isEditable" >Potential Savings</span></th>
                    <th><span class="isEditable">Cost Variance Graph</span></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${data}" var="val" status="i">
                    <tr>
                        <td>
                            <g:hiddenField name="st-costs-${i}" value="${val["Standard Cost"]}"/>
                            <g:hiddenField name="max-costs-${i}" value="${val["Max"]}"/>
                            ${val["Procedure Group"]}</td>
                        <td>${val["Provider Type"]}</td>
                        <td><g:hiddenField name="events-num-${i}" value=" ${val["Number of Events"]}"/>
                            ${val["Number of Events"]}</td>
                        <td>
                        <g:formatZ5Currency number="${val["Total Cost"]}" type="number" maxFractionDigits="2" />
                        </td>
                        <td>${val["Average Cost Per Event (Annual Increase %)"]}</td>
                        <td>
                            <a href="#" onclick="showProviders()" %{--data-toggle="modal" data-target="#providerModal"--}% title=" ${val["Top Providers"]}">
                                ${val["Top Providers"].toString().substring(0,31)}...
                            </a>

                        </td>
                        <td>

                            <div class="data-row">
                                <span class="data-label"> </span> <a href='#' rel="shareit" id="${i}">
                                <i class="fa fa-sliders " aria-hidden="true"></i>
                                <span id="tile${i}">
                                    <g:formatZ5Currency number="${(val["Number of Events"])*(val["Standard Cost"])}" type="number" maxFractionDigits="2" />
                                    Simulate</span>
                            </a>
                            </div>
                        </td>
                        <td>
                            <div id="img_${i}" href="#" data-toggle="modal" onclick='renderGraph("show-lg-graph",${val["Graph"] as JSON},"isBig",null,"isCVG" )'>
                                <script>renderGraph("img_"+${i},${val["Graph"] as JSON} )</script>
                            </div>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        </g:if>
    </div>
    <%
        //        split page
    %>
</div>
<div id="shareit-box">
    <div id="shareit-body">
        <div id="shareit-blank">
            <button type="button" id="closeButton" class="close" style="top: 22px;margin-right: 29px;color: black;" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div id="shareit-icon">

        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="providerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Providers Lists</h4>
            </div>
            <div class="modal-body">


                <div  class=" z5-table put-border"  style="min-height: 200px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="providers-table"  >
                        <thead>
                        <tr>
                            <th class="same-width-th "> &nbsp;Provider ID</th>
                            <th class="same-width-th">  &nbsp;Provider Name</th>
                            <th class="same-width-th">&nbsp;Provider Location</th>
                            <th class="same-width-th">&nbsp;Number of Events</th>
                            <th class="same-width-th">&nbsp;Total Cost</th>
                            <th class="same-width-th">&nbsp;Cost per procedure</th>
                        </tr>
                        </thead>
                        <tbody>
                <g:if test="${!isEditable}">

                <g:each in="${providerLists.sort{it['Cost per proecdure']}.reverse()}" var="provider">
                    <tr>
                    <td >${provider['Provider ID']}</td>
                    <td >${provider['Provider Name']}</td>
                    <td >${provider['Provider Location']}</td>
                    <td >${provider['Number of Events']}</td>
                    <td >$ <g:formatNumber number="${provider['Total Cost']}" type="number" maxFractionDigits="0" /></td>
                    <td >$ <g:formatNumber number="${provider['Cost per proecdure']}" type="number" maxFractionDigits="0" /></td>
                    </tr>
                </g:each>
                </g:if>
                        </tbody>
            </table>
                </div>
                <div class="modal-header">

                    <h4 class="modal-title mt15 mb5" >Cost Per Procedure</h4>

                </div>
                <div id="provider_graph"  style="text-align: center;">
                    <script>renderGraph("provider_graph",${providerListsGraph},"isBig","isProvider" )</script>
                </div>
                <div class="modal-header">

                    <h4 class="modal-title mt15 mb5" >Geographical Variance</h4>

                </div>
                <div id="provider_map">
                    <div class="mt15" style="text-align: center;">
                        %{--<g:img uri="/images/geoPlot.png" alt="image" />--}%
                        <div id="mapId"></div>


                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                %{--<button type="button" class="btn btn-primary">Save changes</button>--}%
            </div>
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" id="enlarge-graph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" >Cost Variance Graph</h4>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div id="show-lg-graph">

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                %{--<button type="button" class="btn btn-primary">Save changes</button>--}%
            </div>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<script>

    //=================================================================
    //google map
    //=================================================================




    /*var circle = L.circle([38.15184, -99.95361], {
        color: 'blue',
        fillColor: 'blue',
        fillOpacity: 0.5,
        radius: 100
    }).addTo(mymap);
    circle.bindPopup("Test A");

    var circleb = L.circle([40.07807, -101.68945], {
        color: 'blue',
        fillColor: 'blue',
        fillOpacity: 0.5,
        radius: 100
    }).addTo(mymap);
    circleb.bindPopup("Test B");*/


    //=================================================================
    //google map
    //=================================================================

    var $table = $('table.z5-fixed-header');
    $table.floatThead({
        scrollContainer: function ($table) {
            return $table.closest('.z5-table');
        }
    });

    function showProviders(){
        $("#providerModal").modal("show");
        setTimeout(function(){
            var $tables = $('#providers-table');
            $tables.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.z5-table');
                }
            });

            $(".floatThead-floatContainer.floatThead-container").css("background-color","#e9e9e9");



            if (mymap != undefined) { mymap.remove(); }

            var myCenter = new L.LatLng(39.11581, -97.77712);
            var mymap = new L.Map('mapId', {center: myCenter, zoom: 12});

            // var mymap = L.map('mapid').setView([39.20672, -97.95135], 13);


            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {

                maxZoom: 15,
                minZoom:4,
                /*attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                 '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                 'Imagery © <a href="http://mapbox.com">Mapbox</a>',*/
                id: 'mapbox.streets'
            }).addTo(mymap);

            $.each([[39.12074, -97.79386], [39.12413, -97.76381], [39.121, -97.77231], [39.12054, -97.77892], [39.11994, -97.78828], [39.11608, -97.77163], [39.11235, -97.75566], [39.124, -97.74416], [39.12946, -97.73635], [39.1242, -97.73884], [39.11961, -97.75051], [39.13272, -97.76828], [39.13246, -97.77806], [39.11475, -97.76244], [39.11448, -97.75661], [39.11475, -97.78433], [39.11168, -97.78544], [39.10729, -97.77411], [39.10329, -97.75755], [39.10289, -97.74656], [39.10928, -97.76158], [39.10349, -97.76261], [39.11501, -97.77755], [39.11415, -97.77918], [39.10635, -97.76854]],function(i,v){
                var radii=Math.floor(Math.random()*(250-50+1)+50);
                var circle = L.circle([v[0], v[1]], {
                    color: 'steelblue',
                    fillColor: '#84b3de',
                    fillOpacity: 0.7,
                    radius: radii
                }).addTo(mymap);
                circle.bindPopup("Lat-Lng: "+v[0]+","+v[1]);
            })
        },300)


    }

    $(document).ready(function () {




//        $('.tooltip-main').tooltip({html: true})

        var globalId = "";
        var tileSaving;
        document.cookie = 'prob-cookies-0=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-0=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'prob-cookies-1=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-1=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'prob-cookies-2=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-2=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'prob-cookies-3=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-3=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'prob-cookies-4=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-4=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

         document.cookie = 'prob-cookies-5=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-5=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

         document.cookie = 'prob-cookies-6=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-6=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

         document.cookie = 'prob-cookies-7=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-7=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

         document.cookie = 'prob-cookies-8=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
         document.cookie = 'mem-cookies-8=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie = 'prob-cookies-9=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie = 'mem-cookies-9=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie = 'prob-cookies-10=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie = 'mem-cookies-10=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie = 'prob-cookies-11=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie = 'mem-cookies-11=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie = 'prob-cookies-12=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie = 'mem-cookies-12=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie = 'prob-cookies-13=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie = 'mem-cookies-13=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

//        $('a[rel=shareit], #shareit-box').mouseenter(function() {
        $('a[rel=shareit]').mouseenter(function () {
            var ids = $(this).attr('id');

           /* var problem = $("#tile-" + ids + "-problem").val();
            var member = $("#tile-" + ids + "-member").val();*/
            var problem = 50;
            var member=100;
            var sliderMem = getCookie("mem-cookies-" + ids)
            var sliderPoten = getCookie("prob-cookies-" + ids)
            tileSaving = $("#tile" + ids).text();
            var value = Math.round(problem / member);

            var stCosts=$("#st-costs-"+ids).val();
            var maxCosts=$("#max-costs-"+ids).val();
            var eventNum=$("#events-num-"+ids).val();
            sliderMem = sliderMem ? sliderMem : stCosts;
            sliderPoten = sliderPoten ? sliderPoten : (100);


            value = 100
            member = 500

            var height = $(this).height();
            var top = $(this).offset().top;
            var left = $(this).offset().left + ($(this).width() / 2) - ($('#shareit-box').width() / 2);
            $('#shareit-header').height(height);

            var text = '<input type="hidden" id="hiddenIdVal" value="' + ids + '" /><div class="toolBody">' +
                    '<span class="tooltip-label">% of Procedures&nbsp;&nbsp;</span><div style="margin-top:5px">' +
                    '<input id="subTile-1-2" class="proBars" data-slider-id="ex1Slider" type="text" data-slider-min="0" data-slider-max="' + value + '" data-slider-step="1" data-slider-value="' + sliderPoten + '"/></div>' +
                    '<span class="tooltip-label">Targeted Standard Cost&nbsp;&nbsp;&nbsp;</span><div  style="margin-top:5px">' +
                    '<input id="subTile-1-1" class="proBars" data-slider-id="ex2Slider" type="text" data-slider-min="'+stCosts+'" data-slider-max="' + maxCosts + '" data-slider-step="1" data-slider-value="' + sliderMem + '"/></div>' +
                    '<div>' +
                    '<input type="button" class="btn btn-primary btn-save" value="Save">' +
                    '</div></div>';

            $("#shareit-icon").html(text);
            $('#shareit-box').show();
            $('#shareit-box').css({'top': top + 14, 'left': left + 2});

            var sav = problem, mem = member;


            $.each($(".proBars"),function(){
                var $this=$(this);
                var id=this.id;
                var max=$this.attr("data-slider-max")
                var min=$this.attr("data-slider-min")
                var sliderType="standard"
                var sliderSign="$"
                if(id.toString().indexOf("-1-2")>0){
                    sliderType="procedure"
                    sliderSign=""
                }


                new Slider("#"+id, {
                    formatter: function(value) {

                        var actualValue
                        if(sliderType==="procedure"){
                            actualValue=Math.round((value/100)*eventNum);
                            return actualValue+" - "+value+"%";

                        }
                        else{
                            actualValue=Math.round((value/100)*stCosts);
                            return value;

                        }


                    }
                    ,ticks: [parseInt(min),parseInt(max)]
                    ,ticks_labels: [sliderSign+""+min, sliderSign+""+max]
                    ,ticks_snap_bounds: 1
                    ,step:1
                });



                $("#"+id).on("slide", function(slideEvt) {
                    sav=$("#subTile-1-2").val()/100;
                    mem=$("#subTile-1-1").val();

//                    var value=sav*stCosts*mem*eventNum;
                    var ps=(stCosts-(mem-stCosts))*(sav*eventNum)

                    $("#tile"+ids).text("$"+(Math.round(ps/1000))+"K Simulate");



//                    var newAmount=Math.round(value);
//                    var prefix = d3.formatPrefix(Math.round(newAmount));
//                    var amount="$"+prefix.scale(Math.round(newAmount)).toFixed(2)+" K";
//                    $("#tile"+ids).text(amount+" Simulate");
                });

                $("#"+id).on("change", function(slideEvt) {
                    sav=$("#subTile-1-2").val()/100;
                    mem=$("#subTile-1-1").val();
                    var ps=(stCosts-(mem-stCosts))*(sav*eventNum)

                    $("#tile"+ids).text("$"+(Math.round(ps/1000))+"K Simulate");

                });

            })

            $(".slider-tick").remove();

            $(".btn-save").on("click", function (event) {
                event.preventDefault();
                var conf = confirm('Do you want to proceed with save?');
                if (conf) {
                    setCookie("mem-cookies-" + ids, (mem), "prob-cookies-" + ids, (sav*100), 30)
                    $("#shareit-icon").html("");
                    $("#shareit-box").hide();
                    $.each($(".proBars"), function () {
                        $(this).destroy();

                    });
                }


            });


        });

        $('#shareit-box').mouseleave(function () {
            var $this = $(this);
            $('#shareit-field').val('');
            var ids = $("#hiddenIdVal").val();
            $("#tile" + ids).text(tileSaving);
            $("#shareit-icon").html("");
            $this.hide();
            $.each($(".proBars"), function () {
                $(this).destroy();

            });
        });


        $('#closeButton').click(function () {
            $("#shareit-box").hide();
            var ids = $("#hiddenIdVal").val()
            $("#tile" + ids).text(tileSaving);
            $("#shareit-icon").html("");
            $.each($(".proBars"), function () {
                $(this).destroy();

            });
        });
    });


    function setCookie(pro, proVal, mem, memVal, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = pro + "=" + proVal + "; " + expires;
        console.log(proVal)
        document.cookie = mem + "=" + memVal + "; " + expires;
        console.log(document.cookie)

    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    var div2 = d3.select(".contentWrapper").append("div")
            .attr("class", "tooltip3")
            .style("opacity", 0)
            .style("z-index", 9999)








</script>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-silder.css')}" type="text/css">
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">

<style>
.slider.slider-horizontal {
    width: 180px !important;
    height: 2px;
}

.slider-tick-label-container {
    margin-top: 7px !important;
}

#mapId{
    height:500px;
    width:600px;
}
</style>


</body>




</html>