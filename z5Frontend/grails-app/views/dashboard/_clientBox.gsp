<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="main" name="layout" />
    <title>Choose Clients</title>
    <script>
        function selectClients(){
            var client=$("#client").val();
            if(!client) return false

            $("#reloadForm").submit();
            /*jQuery.ajax({
             type:'POST',
             data:{client:client},
             async:false,
             url:'%{--<g:createLink controller="dashboard" action="reloadClient"/>--}%',
             success:function(resp){
             var url="http://"+resp+".deerwalk.local:9090/z5";
             alert(url);
             window.location.target=url
             },
             error:function(XMLHttpRequest,textStatus,errorThrown){
             },
             complete:function(XMLHttpRequest,textStatus){
             }});*/

        }
    </script>
</head>
<body>
<div class="contentWrapper" >



    <body>

    <div class="col-md-12 text-center">
        <h3 class="weltitle mb15">
            <span class="isEditable"><g:editMessage code="dashboard.setClient.head"/></span>
        </h3>
        <g:if test="${flash.message}" >
            <div class="alert alert-warning" >${flash.message}</div>
        </g:if>
        <g:if test="${!flash.error}">
            <g:form name="reloadForm"  action="setClient">
                <div id="wrapper_bg">
                    <span class="isEditable"><g:editMessage code="dashboard.setClient.title"/></span>
                    <g:hiddenField name="url" value="${url}"/>
                    :
                    %{--<g:select name="client" from="${clients}" optionKey="name" optionValue="name" noSelection="['':'Select Clients']" onchange="selectClients()"/>--}%
                    <g:select name="client" class="clientSelect" from="${clients}" optionKey="name" optionValue="description" noSelection="['':'Select One']"/>

                    &nbsp;
                    <button name="saveG" value="Go" class="btn btn-link-upper btn-primary" style="margin-top: -3px;" onclick="selectClients()">
                        <span class="isEditable">
                            Go
                        </span>
                    </button>
                </div>
            </g:form>

        </g:if>

    </div>
</div>
</body>
</html>





