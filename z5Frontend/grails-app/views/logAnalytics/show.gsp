
<%@ page import="com.deerwalk.z5.LogAnalytics" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'logAnalytics.label', default: 'LogAnalytics')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-logAnalytics" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-logAnalytics" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list logAnalytics">
			
				<g:if test="${logAnalyticsInstance?.userId}">
				<li class="fieldcontain">
					<span id="userId-label" class="property-label"><g:message code="logAnalytics.userId.label" default="User Id" /></span>
					
						<span class="property-value" aria-labelledby="userId-label"><g:fieldValue bean="${logAnalyticsInstance}" field="userId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${logAnalyticsInstance?.client}">
				<li class="fieldcontain">
					<span id="client-label" class="property-label"><g:message code="logAnalytics.client.label" default="Client" /></span>
					
						<span class="property-value" aria-labelledby="client-label"><g:fieldValue bean="${logAnalyticsInstance}" field="client"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${logAnalyticsInstance?.controllerId}">
				<li class="fieldcontain">
					<span id="controllerId-label" class="property-label"><g:message code="logAnalytics.controllerId.label" default="Controller Id" /></span>
					
						<span class="property-value" aria-labelledby="controllerId-label"><g:fieldValue bean="${logAnalyticsInstance}" field="controllerId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${logAnalyticsInstance?.actionId}">
				<li class="fieldcontain">
					<span id="actionId-label" class="property-label"><g:message code="logAnalytics.actionId.label" default="Action Id" /></span>
					
						<span class="property-value" aria-labelledby="actionId-label"><g:fieldValue bean="${logAnalyticsInstance}" field="actionId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${logAnalyticsInstance?.parameters}">
				<li class="fieldcontain">
					<span id="parameters-label" class="property-label"><g:message code="logAnalytics.parameters.label" default="Parameters" /></span>
					
						<span class="property-value" aria-labelledby="parameters-label"><g:fieldValue bean="${logAnalyticsInstance}" field="parameters"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${logAnalyticsInstance?.uri}">
				<li class="fieldcontain">
					<span id="uri-label" class="property-label"><g:message code="logAnalytics.uri.label" default="Uri" /></span>
					
						<span class="property-value" aria-labelledby="uri-label"><g:fieldValue bean="${logAnalyticsInstance}" field="uri"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${logAnalyticsInstance?.timeStamp}">
				<li class="fieldcontain">
					<span id="timeStamp-label" class="property-label"><g:message code="logAnalytics.timeStamp.label" default="Time Stamp" /></span>
					
						<span class="property-value" aria-labelledby="timeStamp-label"><g:formatDate date="${logAnalyticsInstance?.timeStamp}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${logAnalyticsInstance?.id}" />
					<g:link class="edit" action="edit" id="${logAnalyticsInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
