
<%@ page import="com.deerwalk.z5.LogAnalytics" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'logAnalytics.label', default: 'LogAnalytics')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="contentWrapper">
		<div class="container">
		<div>
			<div class="col-md-12">
				<h2 class="isEditable" >Log Analytics</h2>
				<div class="sep-border mb15"></div>

			</div>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<div class="z5-table" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id="memberTable" >
					<thead>
					<tr>
						<th>S.N</th>
						<th>User</th>
						<th>Client</th>
						<th>URL</th>
						%{--<th>Controller</th>--}%
						%{--<th>Action</th>--}%
						<th>Time</th>
						<th>Duration</th>

						%{--<g:sortableColumn property="userId" title="${message(code: 'logAnalytics.userId.label', default: 'User')}" />
					
						<g:sortableColumn property="client" title="${message(code: 'logAnalytics.client.label', default: 'Client')}" />
						<g:sortableColumn property="uri" title="${message(code: 'logAnalytics.uri.label', default: 'Uri')}" />


						<g:sortableColumn property="controllerId" title="${message(code: 'logAnalytics.controllerId.label', default: 'Controller')}" />
					
						<g:sortableColumn property="actionId" title="${message(code: 'logAnalytics.actionId.label', default: 'Action')}" />

						<g:sortableColumn property="uri" title="${message(code: 'logAnalytics.uri.label', default: 'Time')}" />--}%




					</tr>
				</thead>
				<tbody>
				<g:each in="${logAnalyticsInstanceList}" status="i" var="logAnalyticsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${i+1+(params.offset?params.offset.toInteger():0)}</td>

						<td>
							%{--<g:link action="show" id="${logAnalyticsInstance.id}">--}%
								${fieldValue(bean: logAnalyticsInstance, field: "userId")}
							%{--</g:link>--}%
						</td>
					
						<td>${fieldValue(bean: logAnalyticsInstance, field: "client")}</td>

						<td>${fieldValue(bean: logAnalyticsInstance, field: "uri")}</td>


						%{--<td>${fieldValue(bean: logAnalyticsInstance, field: "controllerId")}</td>--}%
					
						%{--<td>${fieldValue(bean: logAnalyticsInstance, field: "actionId")}</td>--}%
					

						<td>
							<g:formatDate format="MM/dd/yyyy HH:mm:ss" date="${logAnalyticsInstance.timeStamp}" />
						</td>

						<td>${logAnalyticsInstance.duration}</td>


					</tr>
				</g:each>
				</tbody>
			</table>

			</div>
			<div class="pagination">
				<g:paginate total="${logAnalyticsInstanceTotal}" />
			</div>
		</div>
		</div>
		</div>
	<style>
		.z5-table{
			min-height: 50px;
			max-height: 400px;
		}

		.z5-table tr td{
			text-align: center;
		}
		.pagination a,.pagination span{
			padding-right: 4px;
		}
	</style>
	</body>
</html>
