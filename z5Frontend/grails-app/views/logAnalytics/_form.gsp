<%@ page import="com.deerwalk.z5.LogAnalytics" %>



<div class="fieldcontain ${hasErrors(bean: logAnalyticsInstance, field: 'userId', 'error')} required">
	<label for="userId">
		<g:message code="logAnalytics.userId.label" default="User Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="userId" required="" value="${logAnalyticsInstance?.userId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logAnalyticsInstance, field: 'client', 'error')} required">
	<label for="client">
		<g:message code="logAnalytics.client.label" default="Client" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="client" required="" value="${logAnalyticsInstance?.client}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logAnalyticsInstance, field: 'controllerId', 'error')} required">
	<label for="controllerId">
		<g:message code="logAnalytics.controllerId.label" default="Controller Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="controllerId" required="" value="${logAnalyticsInstance?.controllerId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logAnalyticsInstance, field: 'actionId', 'error')} required">
	<label for="actionId">
		<g:message code="logAnalytics.actionId.label" default="Action Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="actionId" required="" value="${logAnalyticsInstance?.actionId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logAnalyticsInstance, field: 'parameters', 'error')} required">
	<label for="parameters">
		<g:message code="logAnalytics.parameters.label" default="Parameters" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="parameters" required="" value="${logAnalyticsInstance?.parameters}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logAnalyticsInstance, field: 'uri', 'error')} required">
	<label for="uri">
		<g:message code="logAnalytics.uri.label" default="Uri" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="uri" required="" value="${logAnalyticsInstance?.uri}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logAnalyticsInstance, field: 'timeStamp', 'error')} ">
	<label for="timeStamp">
		<g:message code="logAnalytics.timeStamp.label" default="Time Stamp" />
		
	</label>
	<g:datePicker name="timeStamp" precision="day"  value="${logAnalyticsInstance?.timeStamp}" default="none" noSelection="['': '']" />
</div>

