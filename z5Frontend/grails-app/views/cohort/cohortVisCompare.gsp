<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 1/6/17
  Time: 2:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Opportunity Comparison</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<body>

<div   class="contentWrapper">

    <div class="container double-arrow-back-top-container">
        <div class="col-md-6 double-arrow-back-top">
            <g:if test="${params.id}">
                <g:link controller="programTracking" action="cohortComparison" class="text-link-upper mb15 double-arrow-left">
                    Opportunity Comparison List
                </g:link>

            </g:if><g:elseif test="${showInIdentity}">
                <g:link controller="programTracking" action="explorer" params="[showInIdentity:'1']" class="text-link-upper mb15 double-arrow-left">
                    Opportunity List
                </g:link>
            </g:elseif><g:else>
                <g:link controller="cohort" action="index" class="text-link-upper mb15 double-arrow-left">
                    Opportunity List
                </g:link>
            </g:else>

        </div>



        <div class="col-md-12">
            <h2 >Program Tracking - Comparison </h2>
        </div>
        %{--<div class="watermark mb15"><div style="display: inline;text-transform: capitalize">
            &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
        </div>--}%
        <div class="clearfix"></div>
        <div class="sep-border"></div>
        <div class="col-md-12 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.explore.population"/></span><input type="hidden" value="hPharmacy.container3.table1.options1"></a>
            <a action="#"  class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>
        %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
        </div>

        <div class="col-md-12 mb15">

            <div class="col-md-6">

                <g:checkBox class="myCheckbox" id="toggleText" title="Show Text Label" name="toggleText" style="display: inline-block;margin-left: 17px;"></g:checkBox>

                <a href="javascript:void(0)" style="margin-top: 5px;" >Show % change</a>

                <g:checkBox class="myCheckbox" id="showDetails" title="Show Text Label" name="showDetails" style="display: inline-block;margin-left: 17px;"></g:checkBox>
                <a href="#" style="margin-top: 5px;" >Show Details</a>

            </div>
            <div class="has-filter col-md-6">
                <span class="f-right custom-tooltip">
                    <span class=" isEditable " ><g:editMessage code="highPharmacy.container1.methodology" /></span>
                    <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
                </span>

            </div>
        </div>


        <div id="cohort-vis"></div>
        <div id="cohort-visContent"></div>

        <div>
            Current Period: <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.currentFrom)}" format="MMM yyyy"/> - <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.currentTo)}" format="MMM yyyy"/>
            <br>
            Previous Period: <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.pastFrom)}" format="MMM yyyy"/> - <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.pastTo)}" format="MMM yyyy"/>

        </div>

        <div class="col-md-12 " style="padding-left: 0px;">
            <div class="legend mb10" style="float:left;"></div>

            <div style="float:left;margin-top: -4px;font-size: 11px;color: grey;">&nbsp;&nbsp;*Mean trending (10%) in the wrong direction from last year</div>

        </div>
    </div>




</div>

<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">

<style>/*
.squaredFour {
    width: 20px;
    position: relative;
    margin: 20px auto;
label {
    width: 20px;
    height: 20px;
    cursor: pointer;
    position: absolute;
    top: 0;
    left: 0;
    background: #fcfff4;
    background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
    border-radius: 4px;
    box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);
&:after {
     content: '';
     width: 9px;
     height: 5px;
     position: absolute;
     top: 4px;
     left: 4px;
     border: 3px solid #333;
     border-top: none;
     border-right: none;
     background: transparent;
     opacity: 0;
     transform: rotate(-45deg);
 }
&:hover::after {
     opacity: 0.5;
 }
}
input[type=checkbox] {
    visibility: hidden;
&:checked + label:after {
     opacity: 1;
 }
}
}*/

.legend{
    height: 15px;width: 35px;background-color:rgb(236, 62, 64);border: 1px solid black;
}
</style>

<script>





    $( document ).ready(function() {
        var $table = $('table.z5-fixed-header');
        $table.floatThead({
            scrollContainer: function($table){
                return $table.closest('.z5-table');
            }
        });
    });

    var data1=${data};

    refreshReport(0);

    function refreshReport(showDetails){
        renderFunnelBar(data1,"cohort-vis",showDetails);
    }




    function renderFunnelBar(datas,content,showDetails){
        $('#galleryImages').show();

        $("#"+content).html("");
        $("#"+content+"Content").html("");
        var arr=["pharmacyPmpm","medicalPmpm","changeInPmpm","concurrentRisk","prospectiveRisk","claimsOver10k","claimsBelow500","mailOrder","genericOrder","specialityOrder","erVisitper1000","thirtyDayReadmitper1000","totalAdmissionsper1000","averageLengthOfStay","formulary"];//.reverse();

        var metricsMap={"pharmacyPmpm":"Pharmacy PMPM","medicalPmpm":"Medical PMPM","changeInPmpm":"Total Paid PMPM",
                "mailOrder":"Prescription Count Filled By Mail Order","genericOrder":"Prescription Count Filled By Generic Drugs","specialityOrder":"Prescription Count Filled By Speciality Order",
                "erVisitper1000":"ER Visits per 1000","thirtyDayReadmitper1000":"30 Days Re-Admissions per 1000","totalAdmissionsper1000":"Total Admissions per 1000",
                "averageLengthOfStay":"Average Length Of Stay","formulary":"Prescription Filled By Formulary",concurrentRisk:"Concurrent Risk",prospectiveRisk:"Prospective Risk",claimsOver10k:"Claims Over $10K per 1000",claimsBelow500:"Claims Below $500 per 1000","gapInCare":"Gap In Care",wellness:"Engaged in wellness",coaching:"Engaged in coaching",bms:"Aggregate Biometric Improvement Score",proSav:"Potential Saving (Predicted-Expected)-1",pimpact:"Calculated Program Impact" ,hsrisk:"Members Improving HS Risk Stratification Level"};

        var newArr = [];
        var gapincareFlag=false,wellnessFlag=false,coachingFlag=false;
        var bmsFlag=false,proSavFlag=false,pimpactFlag=false,hsriskFlag=false;

        $.each(datas, function (key, val) {
            newVal = new Object();

            newVal.MemberCount = +val.summary.totalCounts
//            newVal.Status = key;
            newVal.Status = val.cohortName;
            newVal.Member_Drill =val.members_drill;

            newVal.pharmacyPmpm =d3.round(val.pharmacyPmpm.current.totalPaidAmountPmpm,2);
            newVal.pharmacyPmpmLast =d3.round(val.pharmacyPmpm.last.totalPaidAmountPmpm,2);
            newVal.pharmacyPmpmChange =d3.round(((val.pharmacyPmpm.current.totalPaidAmountPmpm-val.pharmacyPmpm.last.totalPaidAmountPmpm)/val.pharmacyPmpm.last.totalPaidAmountPmpm)*100,2);
            newVal.pharmacyPmpmDirection="-"

            newVal.medicalPmpm =d3.round(val.medicalPmpm.current.totalPaidAmountPmpm,2);
            newVal.medicalPmpmLast =d3.round(val.medicalPmpm.last.totalPaidAmountPmpm,2);
            newVal.medicalPmpmChange =d3.round(((val.medicalPmpm.current.totalPaidAmountPmpm-val.medicalPmpm.last.totalPaidAmountPmpm)/val.medicalPmpm.last.totalPaidAmountPmpm)*100,2);
            newVal.medicalPmpmDirection="-"

            newVal.changeInPmpm =d3.round(val.changeInPmpm.current.totalPaidAmountPmpm,1);
            newVal.changeInPmpmLast =d3.round(val.changeInPmpm.last.totalPaidAmountPmpm,1);
            newVal.changeInPmpmChange =d3.round(((val.changeInPmpm.current.totalPaidAmountPmpm-val.changeInPmpm.last.totalPaidAmountPmpm)/val.changeInPmpm.last.totalPaidAmountPmpm)*100,2);
            newVal.changeInPmpmDirection="-"

            newVal.mailOrder =d3.round(val.mailAndGenericOrderPrescription.current.mailOrder,1);
            newVal.mailOrderLast =d3.round(val.mailAndGenericOrderPrescription.last.mailOrder,1);
            newVal.mailOrderChange =d3.round(((val.mailAndGenericOrderPrescription.current.mailOrder-val.mailAndGenericOrderPrescription.last.mailOrder)/val.mailAndGenericOrderPrescription.last.mailOrder)*100,2);
            newVal.mailOrderDirection="+"

            newVal.genericOrder =d3.round(val.mailAndGenericOrderPrescription.current.genericOrder,1);
            newVal.genericOrderLast =d3.round(val.mailAndGenericOrderPrescription.last.genericOrder,1);
            newVal.genericOrderChange =d3.round(((val.mailAndGenericOrderPrescription.current.genericOrder-val.mailAndGenericOrderPrescription.last.genericOrder)/val.mailAndGenericOrderPrescription.last.genericOrder)*100,2);
            newVal.genericOrderDirection="+"

            newVal.specialityOrder =d3.round(val.mailAndGenericOrderPrescription.current.specialityOrder,1);
            newVal.specialityOrderLast =d3.round(val.mailAndGenericOrderPrescription.last.specialityOrder,1);
            newVal.specialityOrderChange =d3.round(((val.mailAndGenericOrderPrescription.current.specialityOrder-val.mailAndGenericOrderPrescription.last.specialityOrder)/val.mailAndGenericOrderPrescription.last.specialityOrder)*100,2);
            newVal.specialityOrderDirection="-"

            newVal.erVisitper1000 =d3.round(val.erVisitAndAdmissionUsage.current.erVisitper1000,1);
            newVal.erVisitper1000Last =d3.round(val.erVisitAndAdmissionUsage.last.erVisitper1000,1);
            newVal.erVisitper1000Change =d3.round(((val.erVisitAndAdmissionUsage.current.erVisitper1000-val.erVisitAndAdmissionUsage.last.erVisitper1000)/val.erVisitAndAdmissionUsage.last.erVisitper1000)*100,2);
            newVal.erVisitper1000Direction="-"


            newVal.thirtyDayReadmitper1000 =d3.round(val.erVisitAndAdmissionUsage.current.thirtyDayReadmitper1000,1);
            newVal.thirtyDayReadmitper1000Last =d3.round(val.erVisitAndAdmissionUsage.last.thirtyDayReadmitper1000,1);
            newVal.thirtyDayReadmitper1000Change =d3.round(((val.erVisitAndAdmissionUsage.current.thirtyDayReadmitper1000-val.erVisitAndAdmissionUsage.last.thirtyDayReadmitper1000)/val.erVisitAndAdmissionUsage.last.thirtyDayReadmitper1000)*100,2);
            newVal.thirtyDayReadmitper1000Direction="-"

            newVal.totalAdmissionsper1000 =d3.round(val.erVisitAndAdmissionUsage.current.totalAdmissionsper1000,1);
            newVal.totalAdmissionsper1000Last =d3.round(val.erVisitAndAdmissionUsage.last.totalAdmissionsper1000,1);
            newVal.totalAdmissionsper1000Change =d3.round(((val.erVisitAndAdmissionUsage.current.totalAdmissionsper1000-val.erVisitAndAdmissionUsage.last.totalAdmissionsper1000)/val.erVisitAndAdmissionUsage.last.totalAdmissionsper1000)*100,2);
            newVal.totalAdmissionsper1000Direction="-"


            newVal.averageLengthOfStay =d3.round(val.erVisitAndAdmissionUsage.current.averageLengthOfStay,1);
            newVal.averageLengthOfStayLast =d3.round(val.erVisitAndAdmissionUsage.last.averageLengthOfStay,1);
            newVal.averageLengthOfStayChange =d3.round(((val.erVisitAndAdmissionUsage.current.averageLengthOfStay-val.erVisitAndAdmissionUsage.last.averageLengthOfStay)/val.erVisitAndAdmissionUsage.last.averageLengthOfStay)*100,2);
            newVal.averageLengthOfStayDirection="-"


            newVal.formulary =d3.round(val.mailAndGenericOrderPrescription.current.formulary,1);
            newVal.formularyLast =d3.round(val.mailAndGenericOrderPrescription.last.formulary,1);
            newVal.formularyChange =d3.round(((val.mailAndGenericOrderPrescription.current.formulary-val.mailAndGenericOrderPrescription.last.formulary)/val.mailAndGenericOrderPrescription.last.formulary)*100,2);
            newVal.formularyDirection="+"

            newVal.concurrentRisk =d3.round(val.concurrentRisk.current.avgScore,1);
            newVal.concurrentRiskLast =d3.round(val.concurrentRisk.last.avgScore,1);
            newVal.concurrentRiskChange =d3.round(((val.concurrentRisk.current.avgScore-val.concurrentRisk.last.avgScore)/val.concurrentRisk.last.avgScore)*100,2);
            newVal.concurrentRiskDirection="-"

            newVal.prospectiveRisk =d3.round(val.prospectiveRisk.current.avgScore,1);
            newVal.prospectiveRiskLast =d3.round(val.prospectiveRisk.last.avgScore,1);
            newVal.prospectiveRiskChange =d3.round(((val.prospectiveRisk.current.avgScore-val.prospectiveRisk.last.avgScore)/val.prospectiveRisk.last.avgScore)*100,2);
            newVal.prospectiveRiskDirection="-"

            newVal.claimsOver10k =d3.round(val.claimsOver10k.current.totalClaimsPer1000,1);
            newVal.claimsOver10kLast =d3.round(val.claimsOver10k.last.totalClaimsPer1000,1);
            newVal.claimsOver10kChange =d3.round(((val.claimsOver10k.current.totalClaimsPer1000-val.claimsOver10k.last.totalClaimsPer1000)/val.claimsOver10k.last.totalClaimsPer1000)*100,2);
            newVal.claimsOver10kDirection="-"

            newVal.claimsBelow500 =d3.round(val.claimsBelow500.current.totalClaimsPer1000,1);
            newVal.claimsBelow500Last =d3.round(val.claimsBelow500.last.totalClaimsPer1000,1);
            newVal.claimsBelow500Change =d3.round(((val.claimsBelow500.current.totalClaimsPer1000-val.claimsBelow500.last.totalClaimsPer1000)/val.claimsBelow500.last.totalClaimsPer1000)*100,2);
            newVal.claimsBelow500Direction="+"


            if(val['GAP_IN_CARE']){
                newVal.gapInCare =d3.round(val['GAP_IN_CARE'].currentmetric,1);
                newVal.gapInCareLast =d3.round(val['GAP_IN_CARE'].lastmetric,1);
                newVal.gapInCareChange =d3.round(((val['GAP_IN_CARE'].currentmetric-val['GAP_IN_CARE'].lastmetric)/val['GAP_IN_CARE'].lastmetric)*100,2);
                newVal.gapInCareDirection=val['GAP_IN_CARE'].expectedDirection

                gapincareFlag=true;

            }

            if(val['WELLNESS_ENGAGED']){
                newVal.wellness =d3.round(val['WELLNESS_ENGAGED'].currentmetric,1);
                newVal.wellnessLast =d3.round(val['WELLNESS_ENGAGED'].lastmetric,1);
                newVal.wellnessChange =d3.round(((val['WELLNESS_ENGAGED'].currentmetric-val['WELLNESS_ENGAGED'].lastmetric)/val['WELLNESS_ENGAGED'].lastmetric)*100,2);
                newVal.wellnessDirection=val['WELLNESS_ENGAGED'].expectedDirection

                wellnessFlag=true;

            }


            if(val['COACHING_ENGAGED']){
                newVal.coaching =d3.round(val['COACHING_ENGAGED'].currentmetric,1);
                newVal.coachingLast =d3.round(val['COACHING_ENGAGED'].lastmetric,1);
                newVal.coachingChange =d3.round(((val['COACHING_ENGAGED'].currentmetric-val['COACHING_ENGAGED'].lastmetric)/val['COACHING_ENGAGED'].lastmetric)*100,2);
                newVal.coachingDirection=val['COACHING_ENGAGED'].expectedDirection

                coachingFlag=true;
            }

            if(val['BIOMETRIC_SCORE']){
                newVal.bms =d3.round(val['BIOMETRIC_SCORE'].currentmetric,1);
                newVal.bmsLast =d3.round(val['BIOMETRIC_SCORE'].lastmetric,1);
                newVal.bmsChange =d3.round(((val['BIOMETRIC_SCORE'].currentmetric-val['BIOMETRIC_SCORE'].lastmetric)/val['BIOMETRIC_SCORE'].lastmetric)*100,2);
                newVal.bmsDirection=val['BIOMETRIC_SCORE'].expectedDirection

                bmsFlag=true;
            }

            if(val['PROJECTED_SAVINGS']){
                newVal.proSav =d3.round(val['PROJECTED_SAVINGS'].currentmetric,1);
                newVal.proSavLast =d3.round(val['PROJECTED_SAVINGS'].lastmetric,1);
                newVal.proSavChange =d3.round(((val['PROJECTED_SAVINGS'].currentmetric-val['PROJECTED_SAVINGS'].lastmetric)/val['PROJECTED_SAVINGS'].lastmetric)*100,2);
                newVal.proSavDirection=val['PROJECTED_SAVINGS'].expectedDirection

                proSavFlag=true;
            }



            if(val['HS_RISK_STRATIFICATION']){
                newVal.hsrisk =d3.round(val['HS_RISK_STRATIFICATION'].currentmetric,1);
                newVal.hsriskLast =d3.round(val['HS_RISK_STRATIFICATION'].lastmetric,1);
                newVal.hsriskChange =d3.round(((val['HS_RISK_STRATIFICATION'].currentmetric-val['HS_RISK_STRATIFICATION'].lastmetric)/val['HS_RISK_STRATIFICATION'].lastmetric)*100,2);
                newVal.hsriskDirection=val['HS_RISK_STRATIFICATION'].expectedDirection

                hsriskFlag=true;
            }


            if(val['PROGRAM_IMPACT_IN_AMOUNT']){
                newVal.pimpact =d3.round(val['PROGRAM_IMPACT_IN_AMOUNT'].currentmetric,1);
                newVal.pimpactLast =d3.round(val['PROGRAM_IMPACT_IN_AMOUNT'].lastmetric,1);
                newVal.pimpactChange =d3.round(((val['PROGRAM_IMPACT_IN_AMOUNT'].currentmetric-val['PROGRAM_IMPACT_IN_AMOUNT'].lastmetric)/val['PROGRAM_IMPACT_IN_AMOUNT'].lastmetric)*100,2);
                newVal.pimpactDirection=val['PROGRAM_IMPACT_IN_AMOUNT'].expectedDirection

                pimpactFlag=true;
            }


             newArr.push(newVal);

        });

        if(gapincareFlag) arr.push("gapInCare")
        if(wellnessFlag) arr.push("wellness")
        if(coachingFlag) arr.push("coaching")
        if(bmsFlag) arr.push("bms")
        if(proSavFlag) arr.push("proSav")
        if(hsriskFlag) arr.push("hsrisk")
        if(pimpactFlag) arr.push("pimpact")

        arr.reverse();



        var len=newArr.length;

        var data=newArr;


        var mainWidth=350+(len*140);

        var margin = {top: 60, right: 8, bottom: 8, left: 220},
                width = mainWidth - margin.left - margin.right,
                height = 380 - margin.top - margin.bottom;


        var y0 = Math.max(Math.abs(d3.min(data.map(function (d) {
            return d.MemberCount;
        }))), Math.abs(d3.max(data.map(function (d) {
            return d.MemberCount;
        }))));

        //var colors =["#FDE895", "#B3C5E9", "#B3C5E9", "#A8D18D", "#A8D18D", "#0099c6"];
        var colors =["#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5"];


        var y = d3.scale.linear()
                .domain([-y0/2, y0/2])
                .range([height,0])
                .nice();

        var x = d3.scale.ordinal()
                .domain(data.map(function (d) {
                    return d.Status;;
                }))
                .rangeRoundBands([0, width], .45);


        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

        var svg = d3.select("#"+content).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + (margin.left) + "," + margin.top + ")");


        var mainBars=svg.selectAll(".bar")
                .data(data)
                .enter().append("svg:a")
                .attr("data-href", function(d,i){
                    return createZ5Link(d.Member_Drill);
                })
                .attr("onclick", "postRequestForGet(this);return false;").append("rect")


        mainBars.attr("class","bar positive")
                .attr("fill",function(d,i){return colors[i];})
                .attr("y", function(d) {
                    return y(Math.max(0, -d.MemberCount));
                })
                .attr("height", function (d) {
                    return 0;
                })
                .attr("x", function(d) { return x(d.Status); })
                .attr("width", x.rangeBand())
                .transition()
                .duration(1000)
                .attr("y", function (d) {
                    return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
                }).attr("height", function (d) {
                    return Math.abs(y(-d.MemberCount) - y(0));
                })
                /*.attr("stroke","black")
                .attr("stroke-width","1")*/

        mainBars.append("svg:title").text(function(d,i){
            return "Member Count : "+Math.abs(d.MemberCount);//d.Status
        })






        var mainHeight=820;

        if(showDetails){
            mainHeight=1410;
        }


        var height2 = mainHeight - margin.top - margin.bottom;

        var svg2 = d3.select("#"+content+"Content").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height2 + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + (margin.left) + "," + margin.top + ")");

        arr=arr.reverse()
        var space=0;



        for(i=0;i<arr.length;i++){

            var key=arr[i];
            var container=svg2.append("g");
            var bars=container.selectAll(".rect1")
                    .data(data)
                    .enter()
//                    .append("svg:a")
//                    .attr("xlink:href", function(d,i){return "/programTracking/pt?metrics="+key.toLowerCase();})

            var putTitle=bars.append("g");

            putTitle.append("rect").attr("y", function(d) {
                //return (height2-(i*17-10));
                return space;
            })
                    .attr("class",function(d){
                        var value= isNaN(d[key+'Change'])?"N/A":d[key+'Change'];
                        value=isFinite(value)?value:"N/A";

                        if(value>10 && (d[key+'Direction']=="-"))
                            return "redfill"
                        else if(value<-10 && (d[key+'Direction']=="+")){
                            return "redfill"
                        }
                        else
                            return "grayfill"

                    })
                    .attr("x", function(d) { return x(d.Status); })
                    .attr("height",0)
                    .attr("width", x.rangeBand())
                    .transition()
                    .delay((i+1)*70)
                    .duration(300)
                    .attr("height",16)






            putTitle.append("text").attr("class","text-class textToggle")
                    .attr("y", function(d) {
                        return space+12;
                    }).attr("x", function(d) {
                        return x(d.Status)+(x.rangeBand()/2) })
                    .attr("text-anchor", "middle")
                    .text(function(d){
                        var value= isNaN(d[key+'Change'])?"N/A":d[key+'Change'];
                        value=isFinite(value)?value:"N/A";

                        if(value<0)
                            return  "("+value+" %)"
                        else if(value>0)
                            return  "(+"+value+" %)"
                        else{
                            return  "("+value+" )"
                        }

                    })


            // description start




            if(mainHeight==1410) {


                putTitle.append('g').append("text").attr("class", "text-class1 textToggle1")
                        .attr("y", function (d) {
                            return space + 30;
                        }).attr("x", function (d) {
                            return x(d.Status)
                        })
                        .attr("text-anchor", "start")
                        .text(function (d) {
                            var valuePer = isNaN(d[key + 'Change']) ? "N/A" : d[key + 'Change'];
                            valuePer = isFinite(valuePer) ? valuePer : "N/A";

                            if (valuePer < 0)
                                valuePer = valuePer + " %"
                            else if (valuePer > 0)
                                valuePer = "+" + valuePer + " %"
                            else {
                                valuePer = valuePer
                            }

                            var str = "Current period value : " + d[key];
                            return str;
                        })

                putTitle.append('g').append("text").attr("class", "text-class1 textToggle1")
                        .attr("y", function (d) {
                            return space + 42;
                        }).attr("x", function (d) {
                            return x(d.Status)
                        })
                        .attr("text-anchor", "start")
                        .text(function (d) {
                            var valuePer = isNaN(d[key + 'Change']) ? "N/A" : d[key + 'Change'];
                            valuePer = isFinite(valuePer) ? valuePer : "N/A";

                            if (valuePer < 0)
                                valuePer = valuePer + " %"
                            else if (valuePer > 0)
                                valuePer = "+" + valuePer + " %"
                            else {
                                valuePer = valuePer
                            }

                            var str = "Last period value : " + d[key + "Last"];
                            return str;
                        })


                putTitle.append('g').append("text").attr("class", "text-class1 textToggle1")
                        .attr("y", function (d) {
                            return space + 53;
                        }).attr("x", function (d) {
                            return x(d.Status)
                        })
                        .attr("text-anchor", "start")
                        .text(function (d) {
                            var valuePer = isNaN(d[key + 'Change']) ? "N/A" : d[key + 'Change'];
                            valuePer = isFinite(valuePer) ? valuePer : "N/A";

                            if (valuePer < 0)
                                valuePer = valuePer + " %"
                            else if (valuePer > 0)
                                valuePer = "+" + valuePer + " %"
                            else {
                                valuePer = valuePer
                            }

                            var str = "Percent change :" + valuePer;
                            return str;
                        })

            }

            // description end



            putTitle.append("svg:title").attr("class","svgTitle")
                    .text(function(d, i) {

                        var valuePer= isNaN(d[key+'Change'])?"N/A":d[key+'Change'];
                        valuePer=isFinite(valuePer)?valuePer:"N/A";

                        if(valuePer<0)
                            valuePer=  valuePer+" %"
                        else if(valuePer>0)
                            valuePer=  "+"+valuePer+" %"
                        else{
                            valuePer=valuePer
                        }

                        var str=metricsMap[key]+"\nCurrent period value : "+d[key]+"\nLast period value : "+d[key+"Last"]+"\nPercent change : "+valuePer;
                        return str; })
//                    .text(function(d, i) { return metricsMap[key]+" : " + d[key+"Change"]+"%"; })
//                    .text(function(d, i) { return key.substr(0,1).toUpperCase()+key.substr(1)+" : " + d[key+"Change"]+"%"; })


            container.append("rect").attr("y", function(d) {
                return space-2;
            })
                    .attr("class",function(d){
                        return "greenfill"
                    })
                    .attr("x", function(d) { return -218; })
                    .attr("height",20)
                    .attr("width", 240 )
                    .attr("stroke","black")
                    .attr("stroke-width","1")

            container.append("text")
                    .attr("class", "text_y")
                    .attr("text-anchor", "end")  // this makes it easy to centre the text as the transform is applied to the anchor
                    .attr("transform", "translate(" + (20) + "," + ((space+12)) + ")")  // text is drawn off the screen top left, move down and out and rotate
                    .text("")
                    .transition()
                    .duration(300)
                    .delay((i+1)*90)
                    .text(""+metricsMap[key]+"");


            if(mainHeight==1410) {
                space = space + 70;
            }else{
                space = space + 32;
            }

        }



        var xAxis = d3.svg.axis()
                .scale(x)
                .tickSize(0)
                .orient("top");

        var  ax2=svg.append("g")
                .attr("class", "x axis funnel topText")
                .attr("transform", "translate(0,-40)")
                .call(xAxis)
                .selectAll(".tick text")
                .call(wrap, x.rangeBand())
                .style("fontSize","10px");

        var mTicks = data.map(function (d) {
            return d.Status
        });






        var main_xAxis2 = d3.svg.axis()
                .scale(x)
                .tickSize(0)
                .tickValues(mTicks)
                .tickFormat(formatTicks)
                .orient("bottom");

        var ax1=svg.append("g")
                .attr("class", "x axis funnel")
                .attr("transform", "translate(0," + (y(0)) + ")")
                .call(main_xAxis2)
                .selectAll(".tick text")
                .call(wrap, x.rangeBand());


        function wrap(text, width) {
            text.each(function() {
                var text = d3.select(this),
                        words = text.text().split(/\s+/).reverse(),
                        word,
                        line = [],
                        lineNumber = 0,
                        lineHeight = 1.1, // ems
                        y = text.attr("y"),
                        dy = parseFloat(text.attr("dy")),
                        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y-6).attr("dy", dy + "em");
                while (word = words.pop()) {
                    line.push(word);
                    tspan.text(line.join(" "));
                    if (tspan.node().getComputedTextLength() > width) {
                        line.pop();
                        tspan.text(line.join(" "));
                        line = [word];
                        tspan = text.append("tspan").attr("x", 0).attr("y", y-6).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                    }
                }
            });
        }

        function formatTicks(d) {
            var tickFormat=$.grep(data, function(e){ return e.Status === d; })
            return tickFormat[0].MemberCount
        }

        $("#toggleText").on("change",function(){


            if($(this).is(":checked")){
                $(".text-class").attr("class",'text-class');
            }else{
                $(".text-class").attr("class",'text-class textToggle');
            }

           /* $.each($(".text-class"),function(){
                $(this).attr("class","").addClass('text-class');
            })*/



        })


        $("#showDetails").on("change",function(){
            $("#toggleText").on("change");

            if($(this).is(":checked")){
                refreshReport(1);
             //   $(".text-class").attr("class",'text-class textToggle');


            }else{
               refreshReport(0);
               // $(".text-class").attr("class",'text-class');

            }

          //  $("#toggleText").change();

            /* $.each($(".text-class"),function(){
             $(this).attr("class","").addClass('text-class');
             })*/



        })

        $('#galleryImages').hide();
//        $(("#"+content)).prepend("<div style='text-align: center;font-size: 28px;' class='mb25'><a href='javascript:void(0)'>"+data[0].key+"</a></div>")

    }


</script>

<style>

    .text-class1{
        font-size: 11px;
    }

.z5-table table td  {
    padding: 0px !important;
}
 .tick text{
     font-size:12px !important;
 }
 .funnel{
pointer-events: none;

 }

.text_y{
    font-size: 10px;;
    text-transform: capitalize;
}

.redfill{
    fill:#C42A22;

}
.greenfill{
    fill:#A9D08E;

}

.grayfill{
    /*fill:#07B144*/
    fill:#9CA1A5;
}

    .positive{
        fill: "#9CA1A5" !important;
    }


.textToggle{
    display: none;
}


</style>



</body>
</html>