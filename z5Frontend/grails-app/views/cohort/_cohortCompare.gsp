<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 1/6/17
  Time: 2:20 PM
--%>

    <div class="double-arrow-back-top-container mb10">

        <div class="col-md-12 text-center" >
            <h3>Summary Utilization </h3>
        </div>

        <div class="clearfix"></div>


            <div class="z5-table" style="min-height: 389px;">


                <g:hiddenField name="reportingBasis1" value="${session.hr.reportingBasis}"/>
                <g:hiddenField name="includeOn" value="${session.hr.isExclude}"/>
                <g:hiddenField name="range1" value="${session.hr.range}"/>
                <table width="100%"  border="0" cellspacing="0" cellpadding="0" >
                    <thead>
                    <tr>
                        <th width="12%" class="text-center"><strong>Cohort Name</strong></th>
                        <th width="12%" class="text-center"><strong>Member Count</strong></th>
                        <th class="text-center"><strong>Metrics</strong></th>
                        <th class="text-center"><strong>Value</strong></th>
                        <th class="text-center"><strong>Change From Last Year</strong></th>
                        %{--<th class="text-center"><strong>Total Paid PMPM</strong></th>--}%
                        %{--<th class="text-center"><strong>ER Visit/1000</strong></th>--}%
                        %{--<th class="text-center"><strong>Total Admissions/1000</strong></th>--}%
                    </tr>
                    </thead>
                    <tbody>
                    <g:if test="${data}">

                        <g:each in="${data}" var="cohort" status="i">
                            <tr class="bb">
                                <th class="text-center cohort-${i%2}" rowspan="${rowCount}" >
                                    <strong>${cohort.value.getAt("cohortName")}</strong><br>
                                    %{--<div class="cohort-info">Owner Name:John Smith</div>--}%
                                </th>
                                <th class="text-center cohort-${i%2}" rowspan="${rowCount}"><strong>${cohort.value.getAt("summary").getAt("totalCounts")}</strong></th>
                                <td class="text-left"><strong>Pharmacy PMPM</strong></td>
                                <%
                                    def oldVal=cohort.value.getAt("pharmacyPmpm").getAt("last").getAt("totalPaidAmountPmpm");
                                    def newVal=cohort.value.getAt("pharmacyPmpm").getAt("current").getAt("totalPaidAmountPmpm");
                                    def change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    %{--<g:formatNumber number="${data2.getAt(cohort.key).getAt("pharmacyPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>--}%
                                    <g:formatZ5Currency number="${cohort.value.getAt("pharmacyPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="2" />

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>

                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Medical PMPM</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("medicalPmpm").getAt("last").getAt("totalPaidAmountPmpm");
                                    newVal=cohort.value.getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if(Double.isNaN(change))change=0;
                                    if(Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    %{--<g:formatNumber number="${data2.getAt(cohort.key).getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>--}%
                                    <g:formatZ5Currency number="${cohort.value.getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="2" />
                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>

                                <td class="text-left"><strong>Total Paid PMPM</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("changeInPmpm").getAt("last").getAt("totalPaidAmountPmpm");
                                    newVal=cohort.value.getAt("changeInPmpm").getAt("current").getAt("totalPaidAmountPmpm");
                                    change=((newVal-oldVal)/oldVal)*100;

                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;

                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    %{--<g:formatNumber number="${data2.getAt(cohort.key).getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>--}%
                                    <g:formatZ5Currency number="${cohort.value.getAt("changeInPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="2" />

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Concurrent Risk</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("concurrentRisk").getAt("last").getAt("avgScore");
                                    newVal=cohort.value.getAt("concurrentRisk").getAt("current").getAt("avgScore");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                  <g:formatNumber number="${cohort.value.getAt("concurrentRisk").getAt("current").getAt("avgScore")}" type="number" maxFractionDigits="2" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Prospective Risk</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("prospectiveRisk").getAt("last").getAt("avgScore");
                                    newVal=cohort.value.getAt("prospectiveRisk").getAt("current").getAt("avgScore");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("prospectiveRisk").getAt("current").getAt("avgScore")}" type="number" maxFractionDigits="2" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>


                            <tr>
                                <td class="text-left"><strong>Claims Over $10K (per 1000)</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("claimsOver10k").getAt("last").getAt("totalClaimsPer1000");
                                    newVal=cohort.value.getAt("claimsOver10k").getAt("current").getAt("totalClaimsPer1000");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("claimsOver10k").getAt("current").getAt("totalClaimsPer1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Claims Under $500 (per 1000)</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("claimsBelow500").getAt("last").getAt("totalClaimsPer1000");
                                    newVal=cohort.value.getAt("claimsBelow500").getAt("current").getAt("totalClaimsPer1000");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change<-10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("claimsBelow500").getAt("current").getAt("totalClaimsPer1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Prescription Count Filled By Mail Order</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("mailOrder");
                                    newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("mailOrder");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change<-10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("mailOrder")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">


                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Prescription Count Filled By Generic Drugs</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("genericOrder");
                                    newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("genericOrder");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change<-10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("genericOrder")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Prescription Count Filled By Speciality Order</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("specialityOrder");
                                    newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("specialityOrder");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("specialityOrder")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>ER Visits per 1000</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("erVisitper1000");
                                    newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("erVisitper1000");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("erVisitper1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>30 Days Re-Admissions per 1000</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("thirtyDayReadmitper1000");
                                    newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("thirtyDayReadmitper1000");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("thirtyDayReadmitper1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-left"><strong>Total Admissions per 1000</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("totalAdmissionsper1000");
                                    newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("totalAdmissionsper1000");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;

                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("totalAdmissionsper1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Average Length Of Stay</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("averageLengthOfStay");
                                    newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("averageLengthOfStay");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("averageLengthOfStay")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-left"><strong>Prescription Filled By Formulary</strong></td>
                                <%
                                    oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("formulary");
                                    newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("formulary");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change<-10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("formulary")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>

                           <g:if test="${cohort.value.getAt('GAP_IN_CARE')}">
                            <tr>
                                <td class="text-left"><strong>${cohort.value.getAt("GAP_IN_CARE").getAt("metricName")}</strong></td>
                                <%
                                    oldVal=cohort.value.getAt('GAP_IN_CARE').getAt("lastmetric");
                                    newVal=cohort.value.getAt("GAP_IN_CARE").getAt("currentmetric");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("GAP_IN_CARE").getAt("currentmetric")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>
                                </g:if>

                         <g:if test="${cohort.value.getAt('WELLNESS_ENGAGED')}">
                            <tr>
                                <td class="text-left"><strong>${cohort.value.getAt("WELLNESS_ENGAGED").getAt("metricName")}</strong></td>
                                <%
                                    oldVal=cohort.value.getAt('WELLNESS_ENGAGED').getAt("lastmetric");
                                    newVal=cohort.value.getAt("WELLNESS_ENGAGED").getAt("currentmetric");
                                    change=((newVal-oldVal)/oldVal)*100;
                                    if (Double.isNaN(change))change=0;
                                    if (Double.isInfinite(change))change=0;
                                %>
                                <td class="text-right ${change>10?'col-red':''}">
                                    <g:formatNumber number="${cohort.value.getAt("WELLNESS_ENGAGED").getAt("currentmetric")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                                </td>
                                <td class="text-right">
                                    <g:if test="${change instanceof java.lang.String}">
                                        ${change}
                                    </g:if><g:else>
                                        <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                    </g:else>
                                </td>
                            </tr>
                        </g:if>

                        <g:if test="${cohort.value.getAt('COACHING_ENGAGED')}">

                        <tr>
                            <td class="text-left"><strong>${cohort.value.getAt("COACHING_ENGAGED").getAt("metricName")}</strong></td>
                            <%
                                oldVal=cohort.value.getAt('COACHING_ENGAGED').getAt("lastmetric");
                                newVal=cohort.value.getAt("COACHING_ENGAGED").getAt("currentmetric");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                                if (Double.isInfinite(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("COACHING_ENGAGED").getAt("currentmetric")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                            </td>
                            <td class="text-right">
                                <g:if test="${change instanceof java.lang.String}">
                                    ${change}
                                </g:if><g:else>
                                    <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                </g:else>
                            </td>
                        </tr>
                            </g:if>

                            <g:if test="${cohort.value.getAt('PROJECTED_SAVINGS')}">

                        <tr>
                            <td class="text-left"><strong>${cohort.value.getAt("PROJECTED_SAVINGS").getAt("metricName")}</strong></td>
                            <%
                                oldVal=cohort.value.getAt('PROJECTED_SAVINGS').getAt("lastmetric");
                                newVal=cohort.value.getAt("PROJECTED_SAVINGS").getAt("currentmetric");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                                if (Double.isInfinite(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("PROJECTED_SAVINGS").getAt("currentmetric")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                            </td>
                            <td class="text-right">
                                <g:if test="${change instanceof java.lang.String}">
                                    ${change}
                                </g:if><g:else>
                                    <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                </g:else>
                            </td>
                        </tr>
                            </g:if>

                            <g:if test="${cohort.value.getAt('BIOMETRIC_SCORE')}">

                        <tr>
                            <td class="text-left"><strong>${cohort.value.getAt("BIOMETRIC_SCORE").getAt("metricName")}</strong></td>
                            <%
                                oldVal=cohort.value.getAt('BIOMETRIC_SCORE').getAt("lastmetric");
                                newVal=cohort.value.getAt("BIOMETRIC_SCORE").getAt("currentmetric");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                                if (Double.isInfinite(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("BIOMETRIC_SCORE").getAt("currentmetric")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                            </td>
                            <td class="text-right">
                                <g:if test="${change instanceof java.lang.String}">
                                    ${change}
                                </g:if><g:else>
                                    <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                </g:else>
                            </td>
                        </tr>
                            </g:if>

                            <g:if test="${cohort.value.getAt('BIOMETRIC_SCORE')}">

                                <tr>
                                    <td class="text-left"><strong>${cohort.value.getAt("PROGRAM_IMPACT_IN_AMOUNT").getAt("metricName")}</strong></td>
                                    <%
                                        oldVal=cohort.value.getAt('PROGRAM_IMPACT_IN_AMOUNT').getAt("lastmetric");
                                        newVal=cohort.value.getAt("PROGRAM_IMPACT_IN_AMOUNT").getAt("currentmetric");
                                        change=((newVal-oldVal)/oldVal)*100;
                                        if (Double.isNaN(change))change=0;
                                        if (Double.isInfinite(change))change=0;
                                    %>
                                    <td class="text-right ${change>10?'col-red':''}">
                                        <g:formatNumber number="${cohort.value.getAt("PROGRAM_IMPACT_IN_AMOUNT").getAt("currentmetric")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                                    </td>
                                    <td class="text-right">
                                        <g:if test="${change instanceof java.lang.String}">
                                            ${change}
                                        </g:if><g:else>
                                            <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                        </g:else>
                                    </td>
                                </tr>
                            </g:if>

                            <g:if test="${cohort.value.getAt('BIOMETRIC_SCORE')}">

                                <tr>
                                    <td class="text-left"><strong>${cohort.value.getAt("HS_RISK_STRATIFICATION").getAt("metricName")}</strong></td>
                                    <%
                                        oldVal=cohort.value.getAt('HS_RISK_STRATIFICATION').getAt("lastmetric");
                                        newVal=cohort.value.getAt("HS_RISK_STRATIFICATION").getAt("currentmetric");
                                        change=((newVal-oldVal)/oldVal)*100;
                                        if (Double.isNaN(change))change=0;
                                        if (Double.isInfinite(change))change=0;
                                    %>
                                    <td class="text-right ${change>10?'col-red':''}">
                                        <g:formatNumber number="${cohort.value.getAt("HS_RISK_STRATIFICATION").getAt("currentmetric")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>
                                    </td>
                                    <td class="text-right">
                                        <g:if test="${change instanceof java.lang.String}">
                                            ${change}
                                        </g:if><g:else>
                                            <g:formatNumber number="${change}" type="number" maxFractionDigits="2" />%
                                        </g:else>
                                    </td>
                                </tr>
                            </g:if>

                        </g:each>

                    </g:if>
                    %{--// HS_RISK_STRATIFICATION PROGRAM_IMPACT_IN_AMOUNT--}%

                    <g:else>
                        <tr>
                            <td colspan="8">
                                No records to display.
                            </td>
                        </tr>
                    </g:else>
                    </tbody>
                </table>
            </div>

        <div class="clearfix"></div>


        <div  style="padding-left: 0px;">
            Current Period: <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.currentFrom)}" format="MMM yyyy"/> - <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.currentTo)}" format="MMM yyyy"/>
            <br>
            Previous Period: <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.pastFrom)}" format="MMM yyyy"/> - <g:formatDate date="${Date.parse('yyyy-MM-dd',reportingDates.pastTo)}" format="MMM yyyy"/>
        </div>

        <div  style="padding-left: 0px;">
            <div class="legend mb10" style="float:left;"></div>
            <div style="float:left;margin-top: -4px;font-size: 11px;color: grey;">&nbsp;&nbsp;*Mean trending (>10%) in the wrong direction from last year</div>
        </div>

    </div>


<style>

.z5-table table td  {
    padding: 0px 4px 0px 4px !important;
}

    .col-red,.col-red:hover{
        background: rgb(236, 62, 64) !important;
        border:1px solid #ccc !important;
    }

    .legend{
        height: 15px;width: 35px;background-color:rgb(236, 62, 64);border: 1px solid black;
    }


</style>
