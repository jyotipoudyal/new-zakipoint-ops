<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>
<div class="opp-explorer-wrapper" >

        <g:if test="${list}">

            <g:each in="${list}" var="cohort" status="i">


                <div class="col-md-6 no-pad-l">
                    <div class="opp-block">


                        <div class="opp-block-title">
                                <g:hiddenField name="drill.${cohort.id}" value="${cohort.members_drill}"/>
                                <g:hiddenField name="cohortId.${cohort.id}" value="${cohort.cohortId}"/>
                                <g:hiddenField name="cohortName.${cohort.id}" value="${cohort.cohortName}"/>
                                <g:checkBox class="checkbox_grid"  name="sel.${cohort.id}" id="grid_sel_${cohort.id}" style="display: inline-block;" />
                                    %{--${cohort.id}. --}%${cohort.cohortDesc}
                                <a href="#" data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation">

                            <sec:ifAllGranted roles="ROLE_Z5ADMIN">


                                <g:img uri="/images/providers.png" alt="Tilde" />
                            </sec:ifAllGranted>
                                </a>
                       </div>
                        <div class="opp-block-content">

                            <div class="col-md-5 no-pad-l">
                                %{--<g:hiddenField name="tile-${i}-problem" value="${cohort?.problemSizeFormula}"/>--}%
                                <%
                                    def val=(cohort?.problemSizeFormula.tokenize( '*' ))?(cohort?.problemSizeFormula.tokenize( '*' )[1].toInteger()):0;
                                    def formatVal=0,potenVal=0;
                                    if(val){
                                        formatVal=formatZ5Currency(number:(val*cohort?.numberOfMembers) )
                                    }

                                    if(cohort?.saving_max){
                                        potenVal=formatZ5Currency(number:((cohort?.saving_max.toInteger())*(cohort.numberOfMembers)) )
                                    }

                                %>

                                <g:hiddenField name="tile-${i}-problem" value="${val}"/>
                                <g:hiddenField name="tile-${i}-name" value="${cohort.cohortName}"/>
                                <g:hiddenField name="tile-${i}-member" value="${cohort?.numberOfMembers}"/>
                                <g:hiddenField name="tile-${i}-saving-min" value="${cohort?.saving_min}"/>
                                <g:hiddenField name="tile-${i}-saving-max" value="${cohort?.saving_max}"/>


                                %{--<div class="data-row"><span class="data-label">Owner: </span> <a href='javascript:void(0)' >John Smith</a></div>
                                <div class="data-row"><span class="data-label">Created Date: </span> <a href='javascript:void(0)' ><em> ${cohort.createdDate.split("T")[0]}</em></a></div>
                                <div class="data-row"><span class="data-label">Modified Date: </span> <a href='javascript:void(0)' ><em> ${cohort.createdDate.split("T")[0]}</em></a></div>
                          --}%
                                <g:if test="${cohort?.problemSizeFormula}">

                                <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a   class="problem" href='#' >${formatVal}</a></div>
                                </g:if>


                                <div class="data-row">

                                    %{--<span class="data-label">Member Count</span> <a href='#' >${cohort?.numberOfMembers}</a>--}%

                                    <g:z5Link style="width:220px;" class="btn btn-gray icon-provider  iconExplorePopulation"  disabledLink="false" controller="memberSearch" action="searchResult" params="${cohort.members_drill}">
                                        <g:img uri="/images/icon-explore-population.png" style="margin-top: -5px;" alt="Member Count"/>
                                        <span class="data-label">Member Count: &nbsp; &nbsp;<span >${cohort?.numberOfMembers}</span></span>
                                    </g:z5Link>

                                </div>

                                    <g:if test="${cohort?.problemSizeFormula }">

                                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="${i}"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                                    </g:if>


                            </div>

                            <div class="col-md-7 no-pad-r">
                                <div class="data-row data-actions">

                <g:if test="${cohort?.problemSizeFormula}">
                                        <span class="data-row data-label sub-head-prob">Potential Savings</span> <a href='#' id="tile${i}" class="poten" >${potenVal}</a>
                </g:if>
                                        %{--<g:z5Link style="width:183px;margin-left: 10px;" class="btn btn-gray icon-provider  iconExplorePopulation"  disabledLink="false" controller="memberSearch" action="searchResult" params="${cohort.members_drill}">
                                            <g:img uri="/images/icon-explore-population.png" style="margin-top: -25px;" alt="Member Count"/>
                                            <span class="data-label">Member Count <br>${cohort?.numberOfMembers}</span>
                                        </g:z5Link>--}%

                                        <a href="javascript:void(0)" style="width:220px;text-align: center;" onclick="submitCohort('${cohort.id}','cohortGrid')" class="btn btn-gray icon-provider mb15"><span class="data-label">Summary Utilization</span></a>
                                        <g:if test="${cohort?.actions}">
                                            <%
                                                def actions=cohort?.actions.tokenize("---")
                                            %>
                                            <span class="data-label">Actions</span>
                                            <ul>
                                                <g:each in="${actions}" var="act">
                                                    <li>${act}</li>
                                                </g:each>
                                            </ul>
                                        </g:if>
                                </div>


                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </g:each>
        </g:if>









    </div>



    <div id="providersBox"  class="modal fade bd-modal-lg" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                %{--<div class="modal-header">
                    <h3 class="modal-title">Providers</h3>
                </div>--}%
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 1px;"><span aria-hidden="true">&times;</span></button>

                    <p><g:img uri="/images/providersList.png" style="width:680px;" /></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


<div id="shareit-box">
    <div id="shareit-body">
        <div id="shareit-blank">
            <button type="button" id="closeButton" class="close" style="top: 22px;margin-right: 29px;color: black;" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div id="shareit-icon">

        </div>
    </div>
</div>


<script>

    $(document).ready(function() {
//        $('.tooltip-main').tooltip({html: true})

        var globalId = "";
        var tileSaving;


        document.cookie =  'prob-cookie-1=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-1=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'prob-cookie-2=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-2=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'prob-cookie-3=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-3=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'prob-cookie-4=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-4=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-5=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-5=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-6=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-6=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-7=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-7=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-8=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-8=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-9=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-9=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-10=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-10=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-11=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-11=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-12=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-12=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

//        $('a[rel=shareit], #shareit-box').mouseenter(function() {
        $('a[rel=shareit]').mouseenter(function() {
            var ids=$(this).attr('id');

            var problem=$("#tile-"+ids+"-problem").val();
            var tileName=$("#tile-"+ids+"-name").val();
            var member=$("#tile-"+ids+"-member").val();
            var sliderMem=getCookie("mem-cookie-"+ids)
            var sliderPoten=getCookie("prob-cookie-"+ids)




            tileSaving=$("#tile"+ids).text();

//            var value= Math.round(problem/member);
            var value= $("#tile-"+ids+"-saving-max").val();
            var value0= $("#tile-"+ids+"-saving-min").val();

            sliderMem=sliderMem?sliderMem:value;
            sliderPoten=sliderPoten?sliderPoten:problem;


            var height = $(this).height();
            var top = $(this).offset().top;
            var left = $(this).offset().left + ($(this).width() /2) - ($('#shareit-box').width() / 2);
            $('#shareit-header').height(height);

            var text='<input type="hidden" id="hiddenIdVal" value="'+ids+'" /><div class="toolBody">' +
                    '<span class="tooltip-label">Savings/Member&nbsp;&nbsp;</span><div style="margin-top:5px">' +
                    '<input id="subTile-1-2" class="proBar" data-slider-id="ex1Slider" type="text" data-slider-min="'+value0+'" data-slider-max="'+value+'" data-slider-step="1" data-slider-value="'+sliderPoten+'"/></div>'+
                    '<span class="tooltip-label">Engagement Rate&nbsp;&nbsp;&nbsp;</span><div  style="margin-top:5px">' +
                    '<input id="subTile-1-1" class="proBar" data-slider-id="ex2Slider" type="text" data-slider-min="0" data-slider-max="'+member+'" data-slider-step="1" data-slider-value="'+sliderMem+'"/></div>' +
                    '<div>' +
            '<input type="button" class="btn btn-primary btn-save" value="Save">' +
            '</div></div>';

            $("#shareit-icon").html(text);

            $('#shareit-box').show();
            $('#shareit-box').css({'top':top-118, 'left':left-188});


            var sav=problem,mem=member;

            $.each($(".proBar"),function(){
                var $this=$(this);
                var id=this.id;
                var max=$this.attr("data-slider-max")
                var min=$this.attr("data-slider-min")
                var sliderType=""
                if(id.toString().indexOf("-1-2")>0){
                    sliderType="$"
                }


                new Slider("#"+id, {
                    formatter: function(value) {
                        var percent=Math.round((value/max)*100);

                            return value+" - "+percent+"%";

                    }
                    ,ticks: [parseInt(min),parseInt(max)]
                    ,ticks_labels: [sliderType+''+min, sliderType+''+max]
                    ,ticks_snap_bounds: 1
//                    ,tooltip_position:"bottom"
                    ,step:1
                });

                $("#"+id).on("slide", function(slideEvt) {

                    //                var percent=slideEvt.value;
                 var values=id.split("-")[1]
                    mem=$("#subTile-1-1").val()

                    sav=$("#subTile-1-2").val()
                    //setCookie("mem-cookie-"+ids,sav, "poten-cookie-"+ids, mem, 30)
//                 $("#tile"+ids).text("$"+(Math.round((mem*sav)/1000))+"K");
                    $("#tile"+ids).text("$"+(d3.round(((mem*sav)/1000),1))+"K");

//                    $(".tooltip-inner").css("white-space","pre-wrap !important")


                 });

                 $("#"+id).on("change", function(slideEvt) {
                 //                var percent=slideEvt.value.newValue;
                 var values=id.split("-")[1]
                 sav=$("#subTile-1-2").val()
                 mem=$("#subTile-1-1").val()
                     //setCookie("mem-cookie-"+ids,sav, "poten-cookie-"+ids, mem, 30)
                 $("#tile"+ids).text("$"+(d3.round(((mem*sav)/1000),1))+"K");
                 });

            })

            $(".slider-tick").remove();

            $(".btn-save").on("click",function(event) {
                event.preventDefault();
//                var conf=confirm('Do you want to proceed with save?');
//                if(conf){
                    setCookie("mem-cookie-"+ids,mem, "prob-cookie-"+ids, sav, 30,tileName)
                    $("#shareit-icon").html("");
                    $("#shareit-box").hide();
//            $(this).select();
                    $.each($(".proBar"),function(){
                        $(this).destroy();

                    });
//                }


            });




        });

        //onmouse out hide the shareit box
        $('#shareit-box').mouseleave(function () {
            var $this=$(this);

//            setTimeout(function(){
                $('#shareit-field').val('');
                var ids=$("#hiddenIdVal").val();
                $("#tile"+ids).text(tileSaving);
                $("#shareit-icon").html("");
                $this.hide();
                $.each($(".proBar"),function(){
                    $(this).destroy();

                });

//            },1500);



        });


        //hightlight the textfield on click event
        $('#closeButton').click(function () {
            $("#shareit-box").hide();
            var ids=$("#hiddenIdVal").val()
            $("#tile"+ids).text(tileSaving);
            $("#shareit-icon").html("");

//            $(this).select();
            $.each($(".proBar"),function(){
                $(this).destroy();

            });
        });
    });


   function setCookie(pro, proVal, mem,memVal, exdays,tileName) {
       var d = new Date();
       d.setTime(d.getTime() + (exdays*24*60*60*1000));
       var expires = "expires="+ d.toUTCString();
       document.cookie = pro + "=" + proVal + "; " + expires;
       document.cookie = mem + "=" + memVal + "; " + expires;
       $.notify(tileName+" Save successfull", "success");
   }

   function getCookie(cname) {
//       console.log(document.cookie)
       var name = cname + "=";
       var ca = document.cookie.split(';');
       for(var i = 0; i <ca.length; i++) {
           var c = ca[i];
           while (c.charAt(0)==' ') {
               c = c.substring(1);
           }
           if (c.indexOf(name) == 0) {
               return c.substring(name.length,c.length);
           }
       }
       return "";
   }




</script>

<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'notify.min.js')}"></script>




