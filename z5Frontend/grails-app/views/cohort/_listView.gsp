<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
    <thead>
    <tr>
        <th class="text-left"><g:checkBox id="select_all" name="select_all"></g:checkBox></th>
        <th class="text-left"><strong>Id</strong></th>
        <th class="text-left"><strong>Cohort Id</strong></th>

        <th class="text-center"><strong>Cohort Name</strong></th>

        <th class="text-center"><strong>Cohort Desc</strong></th>

        <th class="text-center"><strong>Logo Id</strong></th>
        <th class="text-center"><strong>Problem Size Formula</strong></th>
        <th class="text-center"><strong>Saving Min</strong></th>
        <th class="text-center"><strong>Saving Max</strong></th>
        <th class="text-center"><strong>Created Date</strong></th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${list}">

        <g:each in="${list}" var="cohort" status="i">
            <tr>
                <td>
                    <g:checkBox class="checkbox"  name="sel.${cohort.cohortId}" id="list_sel_${cohort.cohortId}" />
                    <g:hiddenField name="drill.${cohort.cohortId}" value="${cohort.members_drill}"/>
                </td>
                <td class="text-left">
                    ${cohort.id}
                </td>
                <td class="text-left">
                    <g:z5Link  disabledLink="false" controller="memberSearch" action="searchResult" params="${cohort.members_drill}">

                        ${cohort.cohortId}</g:z5Link></td>
                <td class="text-center">${cohort.cohortName}</td>
                <td class="text-center">${cohort.cohortDesc}</td>
                <td class="text-center">${cohort.logoId}</td>
                <td class="text-center">${cohort.problemSizeFormula}</td>
                <td class="text-center">${cohort.saving_min}</td>
                <td class="text-center">${cohort.saving_max}</td>
                <td class="text-center">
                    <em> ${cohort.createdDate.split("T")[0]}</em></td>
            </tr>
        </g:each>

    </g:if><g:else>
        <tr>
            <td colspan="8">
                No records to display.
            </td>
        </tr>
    </g:else>


    </tbody>

</table>