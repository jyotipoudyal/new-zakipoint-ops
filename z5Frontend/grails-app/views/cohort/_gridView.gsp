<div class="opp-explorer-wrapper">

<g:if test="${list}">

    <g:each in="${list}" var="cohort" status="i">



       <div class="col-md-6 no-pad-l">
            <div class="opp-block">
                <div class="opp-block-title">
                    <g:checkBox class="checkbox_grid"  name="sel.${cohort.id}" id="grid_sel_${cohort.id}" style="display: inline-block;" />
                    <g:hiddenField name="drill.${cohort.id}" value="${cohort.members_drill}"/>
                    <g:hiddenField name="cohortName.${cohort.id}" value="${cohort.cohortId}"/>
                    ${cohort.id}. ${cohort.cohortDesc} %{--${cohort.cohortId}--}%
                </div>
        <div class="opp-block-content">

            <div class="col-md-7 no-pad-l">
                <g:hiddenField name="tile-1-problem" value="45000"/>
                <g:hiddenField name="tile-1-member" value="183"/>
                <div class="data-row"><span class="data-label">Owner: </span> <a href='javascript:void(0)' ></a></div>
                <div class="data-row"><span class="data-label">Created Date: </span> <a href='javascript:void(0)' ><em> ${cohort.createdDate.split("T")[0]}</em></a></div>
                <div class="data-row"><span class="data-label">Modified Date: </span> <a href='javascript:void(0)' ><em> ${cohort.createdDate.split("T")[0]}</em></a></div>
            </div>

            <div class="col-md-5  no-pad-r">
                <div class="data-row data-actions">
                    <span class="data-row ">

                        <g:z5Link class="btn btn-gray icon-provider  iconExplorePopulation"  disabledLink="false" controller="memberSearch" action="searchResult" params="${cohort.members_drill}">
                            <g:img uri="/images/icon-explore-population.png" style="margin-top: -25px;" alt="Member Count"/>
                            <span class="data-label">Member Count <br>${cohort?.numberOfMembers}</span>
                        </g:z5Link>
                        <br>
                        <a href="javascript:void(0)" onclick="submitCohort('${cohort.id}','cohortGrid')" class="btn btn-gray icon-provider mb15"><span class="data-label">Summary Utilization</span></a></div>
            </div>
            <div class="clearfix"></div>

        </div>
</div>
</div>
        </g:each>
    </g:if>

</div>

