<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/29/16
  Time: 9:45 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Opportunity List</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<style>
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    height: 31px;
    line-height: 10px;
}

.nav>li>a, .nav>li>a {
    height: 31px;
    line-height: 10px;
    text-decoration: none;
    background-color: #eee;
}


</style>

<link href="${resource(dir:"css",file: "css-switch.css")}" rel="stylesheet"/>

<body>

<div class="contentWrapper">

    <div class="container double-arrow-back-top-container">
        <div class="col-md-6 double-arrow-back-top">
            <div class="col-md-6 ">
            <g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
                <span class="isEditable"><g:editMessage code="hPharmacy.top.navigation"/></span>
            </g:link>
                </div>
            <div class="col-md-6 ">
                <g:if test="${(grails.util.Environment.current.name.equals('development') ||
                        grails.util.Environment.current.name.equals('qc')) && params.showInIdentity}">
                <g:link controller="programTracking" action="explorers" class="text-link-upper">
                    <i class="fa fa-info-circle" aria-hidden="true"></i> Explorer
                </g:link>
                </g:if>
            </div>
        </div>

        <div class="col-md-12" >
            <h2 >Opportunity List</h2>

        </div>
        <div class="clearfix"></div>
        <div class="sep-border"></div>
        <div class="col-md-12 text-right">
            <a href="javascript:void(0)" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.explore.population"/></span><input type="hidden" value="hPharmacy.container3.table1.options1"></a>
            <a href="javascript:void(0)"  class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>
        %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
        </div>





        <div class="col-md-12 mb15">
            <div class="has-filter">
                <span class="f-right custom-tooltip">
                    <span class=" isEditable " ><g:editMessage code="highPharmacy.container1.methodology" /></span>
                    <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
                </span>
            </div>
        </div>

        <div class="col-md-12" style="margin-bottom: -40px;display: none;">
            <ul class="nav nav-pills" style="float: right">
                <li id="cohortGridTab" class="lists active">
                    <a href="#cohortGrid" onclick="toggleList('cohortGrid','cohortList')">&nbsp;&nbsp;&nbsp;Grid&nbsp;&nbsp;&nbsp;</a>
                </li>
                <li id="cohortListTab" class="lists ">
                    <a href="#cohortList" onclick="toggleList('cohortList','cohortGrid')">&nbsp;&nbsp;&nbsp;List&nbsp;&nbsp;&nbsp;</a>
                </li>

            </ul>
        </div>

        <div id="cohortList" style="display: none;">

        <g:form action="cohortCompare">
            <g:submitButton class="btn btn-primary" name="compare" value="Compare"/>
        <div class="z5-table mt15">
            <g:hiddenField name="reportingBasis1" value="${session.hr.reportingBasis}"/>
            <g:hiddenField name="includeOn" value="${session.hr.isExclude}"/>
            <g:hiddenField name="range1" value="${session.hr.range}"/>
                <g:render template="listView" model="[list:list]"/>
        </div>
        </g:form>
        </div>

        <div id="cohortGrid" >

            <g:form action="cohortCompare">
                <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:checkBox class="myCheckbox" id="select_grid_all" title="Select All" name="select_grid_all" style="display: inline-block;margin-left: 17px;"></g:checkBox>
                </sec:ifAllGranted>
                <span><g:submitButton style="margin-top: 5px;" class="btn btn-primary mb15" name="compare" value="Compare Selected"/></span>



                <div class="mt15">
                    <g:hiddenField name="reportingBasis1" value="${session.hr.reportingBasis}"/>
                    <g:hiddenField name="includeOn" value="${session.hr.isExclude}"/>
                    <g:hiddenField name="range1" value="${session.hr.range}"/>
                    <g:hiddenField name="showInIdentity" value="${params.showInIdentity}"/>
                    %{--<g:render template="gridView" model="[list:list]"/>--}%
                    <g:render template="cohortgrid" model="[list:list]"/>
                </div>
            </g:form>
        </div>
    </div>
    </div>

<div id="cohort_info" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div id="cohort_summary" >

                </div>


            </div>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-silder.css')}" type="text/css">


<style type="text/css">

.opp-explorer-wrapper{margin-top:20px;}
.opp-block{background:#e5f0fb; margin-bottom: 30px; padding: 15px; border:1px solid rgba(4,79,150,0.1);}
.opp-block-title{font-size: 14px; font-weight: bold; margin-bottom: 25px; color: #044f96; border-bottom: 1px solid rgba(4,79,150,0.2); padding-bottom: 15px; min-height: 50px;}
.opp-block-title a.icon-provider{float: right;}

.opp-block-content{}
.opp-block-content .container{padding:0 !important;}

.opp-block-content .data-row{ margin-bottom: 15px;}
.opp-block-content .data-actions{border-left: 1px solid rgba(4,79,150,0.2); padding-left:25px; min-height: 140px; }

.opp-block-content .data-row ul{margin: 0; padding: 0; list-style: none;}
.opp-block-content .data-row ul li{position: relative; padding-left: 15px;}
.opp-block-content .data-row ul li:before{content:'\f054'; font-family: fontAwesome; font-size: 10px; position: absolute; left: 0; top:0;}
.opp-block-content .data-row span.data-label{font-weight: bold;display: inline-block; padding-right: 5px;color: #044f96; display: inline-block;}
.no-pad-l{padding-left: 0 !important;;}
.no-pad-r{padding-right: 0 !important;;}
.sub-head-prob{font-size: 16px;}
.problem{font-size: 16px;color: #E95350 !important;font-weight: bold;}
.poten{font-size: 16px;color: #80C46F !important;font-weight: bold;}

.slider.slider-horizontal{
    width:180px !important;
    height:2px;
}

.slider-tick-label-container{
    margin-top: 7px !important;
}
</style>
</body>

<g:javascript>

    function toggleList(sh,hi){
        $(".lists").removeClass("active");
        $("#"+sh+"Tab").closest("li").addClass("active");
        $("#"+sh).show();
        $("#"+hi).hide();
    }

    $(function(){

            if(window.location.hash) {
              var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
              if(hash=="cohortList"){
              toggleList("cohortList","cohortGrid")
              }else{
               toggleList("cohortGrid","cohortList")
              }
          }




        $("#select_all").change(function(){
            var status = this.checked;
            $('.checkbox').each(function(){
                this.checked = status;
            });
        });

        $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){ //if this item is unchecked
                $("#select_all")[0].checked = false; //change "select all" checked status to false
            }

//check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length ){
                $("#select_all")[0].checked = true; //change "select all" checked status to true
            }
        });


        $("#select_grid_all").change(function(){
            var status = this.checked;
            $('.checkbox_grid').each(function(){
                this.checked = status;
            });
        });

        $('.checkbox_grid').change(function(){
            if(this.checked == false){
                $("#select_grid_all")[0].checked = false;
            }


            <sec:ifAllGranted roles="ROLE_Z5ADMINs">
            if ($('.checkbox_grid:checked').length == $('.checkbox_grid').length ){
                $("#select_grid_all")[0].checked = true;
            }
            </sec:ifAllGranted>
        });



    })



    function submitCohort(cohortId,form){
        $('.checkbox_grid').each(function(){this.checked = false;});
        $("#grid_sel_"+cohortId)[0].checked = true;

        <sec:ifAllGranted roles="ROLE_Z5ADMINs">
            $("#select_grid_all")[0].checked = false;
        </sec:ifAllGranted>

        var form=$("#"+form);


        var data=form.find("form").serialize();
        var promise=$.ajax({
            type: 'POST',
            data: data,
            url: '<g:createLink controller="cohort" action="cohortCompare"/>'
        });


        promise.done(function(resp){
        $("#cohort_summary").html(resp);
        $("#cohort_info").modal("show");
        }).fail(function(err){$("#cohort_info").html("No records to display");})

    }


</g:javascript>
</html>