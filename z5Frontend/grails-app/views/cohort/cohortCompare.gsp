<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 1/6/17
  Time: 2:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Opportunity Comparison</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<body>

<div   class="contentWrapper">

    <div class="container double-arrow-back-top-container">
        <div class="col-md-6 double-arrow-back-top">
            <g:if test="${params.id}">
                <g:link controller="programTracking" action="cohortComparison" class="text-link-upper mb15 double-arrow-left">
                    Program Tracking
                </g:link>

            </g:if><g:elseif test="${showInIdentity}">
                <g:link controller="programTracking" action="explorer" params="[showInIdentity:'1']" class="text-link-upper mb15 double-arrow-left">
                    Opportunity List
                </g:link>
            </g:elseif><g:else>
                <g:link controller="cohort" action="index" class="text-link-upper mb15 double-arrow-left">
                    Opportunity List
                </g:link>
            </g:else>
        </div>


        <div class="col-md-12">
            <h2 >Opportunity Comparison </h2>
        </div>
        <div class="clearfix"></div>
        <div class="sep-border"></div>
        <div class="col-md-12 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.explore.population"/></span><input type="hidden" value="hPharmacy.container3.table1.options1"></a>
            <g:link action="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></g:link>
        %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden" value="hPharmacy.container3.table1.options2"></a>--}%
        </div>

        <div class="col-md-12 mb15">
            <div class="has-filter">
                <span class="f-right">
                    <span class=" isEditable custom-tooltip" ><g:editMessage code="highPharmacy.container1.methodology" /></span>
                    <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
                </span>

            </div>
        </div>


            <div class="z5-table mt15" style="min-height: 200px;max-height: 800px;">


            <g:hiddenField name="reportingBasis1" value="${session.hr.reportingBasis}"/>
            <g:hiddenField name="includeOn" value="${session.hr.isExclude}"/>
            <g:hiddenField name="range1" value="${session.hr.range}"/>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0" >
                <thead>
                <tr>
                    <th width="15%" class="text-center"><strong>Cohort Name</strong></th>
                    <th width="15%" class="text-center"><strong>Member Count</strong></th>
                    <th class="text-center"><strong>Metrics</strong></th>
                    <th class="text-center"><strong>Value</strong></th>
                    <th class="text-center"><strong>Change</strong></th>
                    %{--<th class="text-center"><strong>Total Paid PMPM</strong></th>--}%
                    %{--<th class="text-center"><strong>ER Visit/1000</strong></th>--}%
                    %{--<th class="text-center"><strong>Total Admissions/1000</strong></th>--}%
                </tr>
                </thead>
                <tbody>
                <g:if test="${data}">

                    <g:each in="${data}" var="cohort" status="i">
                        <tr class="bb">
                            <th class="text-center cohort-${i%2}" rowspan="15" >
                                <strong>${cohort.value.getAt("cohortName")}</strong><br>
                                %{--<div class="cohort-info">Owner Name:John Smith</div>--}%
                            </th>
                            <th class="text-center cohort-${i%2}" rowspan="15"><strong>${cohort.value.getAt("summary").getAt("totalCounts")}</strong></th>
                            <td class="text-left"><strong>Pharmacy PMPM</strong></td>
                            <%
                                def oldVal=cohort.value.getAt("pharmacyPmpm").getAt("last").getAt("totalPaidAmountPmpm");
                                def newVal=cohort.value.getAt("pharmacyPmpm").getAt("current").getAt("totalPaidAmountPmpm");
                                def change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                %{--<g:formatNumber number="${data2.getAt(cohort.key).getAt("pharmacyPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>--}%
                                <g:formatZ5Currency number="${cohort.value.getAt("pharmacyPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="2" />

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Medical PMPM</strong></td>
                            <%
                                oldVal=cohort.value.getAt("medicalPmpm").getAt("last").getAt("totalPaidAmountPmpm");
                                newVal=cohort.value.getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                %{--<g:formatNumber number="${data2.getAt(cohort.key).getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>--}%
                                <g:formatZ5Currency number="${cohort.value.getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="2" />


                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>

                            <td class="text-left"><strong>Total Paid PMPM</strong></td>
                            <%
                                oldVal=cohort.value.getAt("changeInPmpm").getAt("last").getAt("totalPaidAmountPmpm");
                                newVal=cohort.value.getAt("changeInPmpm").getAt("current").getAt("totalPaidAmountPmpm");
                                change=((newVal-oldVal)/oldVal)*100;

                                if (Double.isNaN(change))change=0;

                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                %{--<g:formatNumber number="${data2.getAt(cohort.key).getAt("medicalPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>--}%
                                <g:formatZ5Currency number="${cohort.value.getAt("changeInPmpm").getAt("current").getAt("totalPaidAmountPmpm")}" type="number" maxFractionDigits="2" />

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Concurrent Risk</strong></td>
                            <%
                                oldVal=cohort.value.getAt("concurrentRisk").getAt("last").getAt("avgScore");
                                newVal=cohort.value.getAt("concurrentRisk").getAt("current").getAt("avgScore");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("concurrentRisk").getAt("current").getAt("avgScore")}" type="number" maxFractionDigits="2" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Prospective Risk</strong></td>
                            <%
                                oldVal=cohort.value.getAt("prospectiveRisk").getAt("last").getAt("avgScore");
                                newVal=cohort.value.getAt("prospectiveRisk").getAt("current").getAt("avgScore");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("prospectiveRisk").getAt("current").getAt("avgScore")}" type="number" maxFractionDigits="2" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>


                        <tr>
                            <td class="text-left"><strong>Claims Over $10K (per 1000)</strong></td>
                            <%
                                oldVal=cohort.value.getAt("claimsOver10k").getAt("last").getAt("totalClaimsPer1000");
                                newVal=cohort.value.getAt("claimsOver10k").getAt("current").getAt("totalClaimsPer1000");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("claimsOver10k").getAt("current").getAt("totalClaimsPer1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Claims Under $500 (per 1000)</strong></td>
                            <%
                                oldVal=cohort.value.getAt("claimsBelow500").getAt("last").getAt("totalClaimsPer1000");
                                newVal=cohort.value.getAt("claimsBelow500").getAt("current").getAt("totalClaimsPer1000");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("claimsBelow500").getAt("current").getAt("totalClaimsPer1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>



                        <tr>
                            <td class="text-left"><strong>Prescription Count Filled By Mail Order</strong></td>
                            <%
                                oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("mailOrder");
                                newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("mailOrder");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("mailOrder")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Prescription Count Filled By Generic Drugs</strong></td>
                            <%
                                oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("genericOrder");
                                newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("genericOrder");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("genericOrder")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Prescription Count Filled By Speciality Order</strong></td>
                            <%
                                oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("specialityOrder");
                                newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("specialityOrder");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("specialityOrder")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>ER Visits per 1000</strong></td>
                            <%
                                oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("erVisitper1000");
                                newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("erVisitper1000");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("erVisitper1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>30 Days Re-Admissions per 1000</strong></td>
                            <%
                                oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("thirtyDayReadmitper1000");
                                newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("thirtyDayReadmitper1000");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("thirtyDayReadmitper1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>
                        <tr>
                            <td class="text-left"><strong>Total Admissions per 1000</strong></td>
                            <%
                                oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("totalAdmissionsper1000");
                                newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("totalAdmissionsper1000");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;

                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("totalAdmissionsper1000")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Average Length Of Stay</strong></td>
                            <%
                                oldVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("last").getAt("averageLengthOfStay");
                                newVal=cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("averageLengthOfStay");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("erVisitAndAdmissionUsage").getAt("current").getAt("averageLengthOfStay")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>

                        <tr>
                            <td class="text-left"><strong>Prescription Filled By Formulary</strong></td>
                            <%
                                oldVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("last").getAt("formulary");
                                newVal=cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("formulary");
                                change=((newVal-oldVal)/oldVal)*100;
                                if (Double.isNaN(change))change=0;
                            %>
                            <td class="text-right ${change>10?'col-red':''}">
                                <g:formatNumber number="${cohort.value.getAt("mailAndGenericOrderPrescription").getAt("current").getAt("formulary")}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>

                            </td>
                            <td class="text-right">
                                <g:formatNumber number="${change}" type="number" maxFractionDigits="1" roundingMode="CEILING"/>%

                            </td>
                        </tr>



                    </g:each>

                </g:if><g:else>
                    <tr>
                        <td colspan="8">
                            No records to display.
                        </td>
                    </tr>
                </g:else>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 " style="padding-left: 0px;">
            <div class="legend mb10" style="float:left;"></div>

            <div style="float:left;margin-top: -4px;font-size: 11px;color: grey;">&nbsp;&nbsp;*Mean trending (>10%) in the wrong direction from last year</div>

        </div>


    </div>
</div>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<script>
    $( document ).ready(function() {
        var $table = $('table.z5-fixed-header');
        $table.floatThead({
            scrollContainer: function($table){
                return $table.closest('.z5-table');
            }
        });
    });
</script>
<style>

.z5-table table td  {
    padding: 0px 4px 0px 4px !important;
}

.col-red,.col-red:hover{
    background: rgb(236, 62, 64) !important;
    border:1px solid #ccc !important;
}

.legend{
    height: 15px;width: 35px;background-color:rgb(236, 62, 64);border: 1px solid black;
}
</style>

</body>
</html>