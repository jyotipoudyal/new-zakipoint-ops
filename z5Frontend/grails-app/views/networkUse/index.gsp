<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 11/28/16
  Time: 11:18 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="layout" content="main">
    <title>Network Usage</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<body>
<div id="contentWrapper" class="contentWrapper">
    <div class="container double-arrow-back-top-container">
        <div class="col-md-6 double-arrow-back-top">
        <span class="isEditable"><g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
            <g:editMessage code="networkUse.allCostDrivers"/>
            </g:link></span>
        </div>

        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
                <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                    <g:if test="${!isEditable}">
                        <g:link title="Edit Content" controller="cms" action="networkUse"><i
                                class="fa fa-edit"></i> Edit Content</g:link>
                    </g:if><g:else>
                    <g:link title="View Changes" controller="networkUse" action="index"><i
                            class="fa fa-eye"></i> View Changes</g:link>
                </g:else>
                </sec:ifAllGranted>
            </li>
        </div>

        <div class="col-md-12">
            <h2 class="mb15"><g:img uri="/images/inefficientnetwork_icon.png" class="heading-icon"/><span class="isEditable"><g:editMessage code="networkUse.head.title"/>
            </span></h2>
        </div>

        <div class="clearfix"></div>

        <div class="sep-border mb25 "></div>

        <div class="col-md-6">
            <h1><span><strong>
                <span class="isEditable"> <g:editMessage code="networkUse.container1.title"/> </span>


            </strong></span></h1>
        </div>

        <div class="col-md-6 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                    uri="/images/icon-explore-population.png" alt=""/>
                <span class="isEditable"> <g:editMessage code="networkUse.container1.explore.population"/> </span>
                <input type="hidden"  value="hPharmacy.container3.table1.options1"></a>
            <a href="#" class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png"
                                                                                    alt=""/><span
                    class="isEditable"><g:editMessage code="networkUse.container1.export.csv"/></span><input type="hidden"
                                                                                                 value="hPharmacy.container3.table1.options2">
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="has-filter col-md-12">
            <span class="f-right custom-tooltip">
                <span class=" isEditable"><g:editMessage
                        code="networkUse.container1.methodology"/></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>
        </div>

        <div class="col-md-12" id="networkUsage" >
        </div>
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        <div class="clearfix"></div>

        <div class="sep-border mb25 "></div>

        <div class="col-md-6">
            <h1><span><strong>
                <span class="isEditable"> <g:editMessage code="networkUse.container2.title"/> </span>


            </strong></span></h1>
        </div>

        <div class="col-md-6 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                    uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                    code="networkUse.container2.explore.population"/></span></a>
            <a href="#" class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png"
                                                                                    alt=""/><span
                    class="isEditable"><g:editMessage code="networkUse.container2.export.csv"/></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="has-filter col-md-12">
            <span class="f-right custom-tooltip">
                <span class=" isEditable "><g:editMessage
                        code="networkUse.container2.methodology"/></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>
        </div>

        <div class="col-md-6 mb25" id="highOutNetworkUsage">
            %{--<g:render template="highOutNetworkUsage" model="[data:highOutNetworkUsage,columns:highOutNetworkUsageColumns]"/>--}%
        </div>

        <div class="clearfix"></div>
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        <div class="clearfix"></div>

        <div class="sep-border mb25 "></div>

        <div class="col-md-6">
            <h1 ><span><strong><span class="isEditable"> <g:editMessage code="networkUse.container3.title"/> </span></strong></span></h1>
        </div>

        <div class="col-md-6 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                    uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                    code="networkUse.container3.explore.population"/></span></a>
            <a href="#" class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png"
                                                                                    alt=""/><span
                    class="isEditable"><g:editMessage code="networkUse.container3.export.csv"/></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="has-filter col-md-12">
            <span class="f-right custom-tooltip">
                <span class=" isEditable"><g:editMessage
                        code="networkUse.container3.methodology"/></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>
        </div>

        <div class="col-md-6 mb25" id="topOutOfNetworkProvider">

        </div>
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        <div class="clearfix"></div>

        <div class="sep-border mb25 "></div>

        <div class="col-md-6">
            <h1><span><strong><span class="isEditable"> <g:editMessage code="networkUse.container4.title"/> </span></strong></span></h1>
        </div>

        <div class="col-md-6 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                    uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                    code="networkUse.container4.explore.population"/></span></a>
            <a href="#" class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png"
                                                                                    alt=""/><span
                    class="isEditable"><g:editMessage code="networkUse.container4.export.csv"/></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="has-filter col-md-12">
            <span class="f-right custom-tooltip">
                <span class=" isEditable"><g:editMessage
                        code="networkUse.container4.methodology"/></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>
        </div>

        <div class="col-md-6 mb25" id="topOonProviderLocation">

        </div>
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        <div class="clearfix"></div>

        <div class="sep-border mb25 "></div>

        <div class="col-md-6">
            <h1><span><strong><span class="isEditable"> <g:editMessage code="networkUse.container5.title"/> </span></strong></span></h1>
        </div>

        <div class="col-md-6 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                    uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                    code="networkUse.container5.explore.population"/></span></a>
            <a href="#" class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png"
                                                                                    alt=""/><span
                    class="isEditable"><g:editMessage code="networkUse.container5.export.csv"/></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="has-filter col-md-12">
            <span class="f-right custom-tooltip">
                <span class=" isEditable "><g:editMessage
                        code="networkUse.container5.methodology"/></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>
        </div>

        <div class="col-md-6 mb25" id="topOonProviderSpeciality">

        </div>


        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        %{--===========================================================--}%
        <div class="clearfix"></div>

        <div class="sep-border mb25 "></div>

        <div class="col-md-6">
            <h1><span><strong><span class="isEditable"> <g:editMessage code="networkUse.container6.title"/> </span></strong></span></h1>
        </div>

        <div class="col-md-6 text-right">
            <a href="#" class=" custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                    uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                    code="networkUse.container6.explore.population"/></span></a>
            <a href="#" class="custom-tooltip text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png"
                                                                                    alt=""/><span
                    class="isEditable"><g:editMessage code="networkUse.container6.export.csv"/></span>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="has-filter col-md-12">
            <span class="f-right custom-tooltip">
                <span class=" isEditable "><g:editMessage
                        code="networkUse.container6.methodology"/></span>
                <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
            </span>
        </div>

        <div class="col-md-12 mb25" id="sankeyChart" >
            <g:if test="${!isEditable}">
                <g:render template="networkTrendingSankey"/>
            </g:if>
        </div>
    </div>

</div>

<script type="application/javascript">
    <g:if test="${!isEditable}">

    $(function(){
        refreshReport();
    })

    </g:if>


    var reportingBasis, fromDate, range, isExclude;
    function refreshReport() {

        reportingBasis = $("#reportingBasis1").val();
        isExclude = $("#includeOn").val();
        range = $("#range1").val();

        /*var def= $.Deferred();
        jQuery.ajax({
         type: 'POST',
         data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'networkTrendingSankey'},
         url: '%{--<g:createLink controller="networkUse" action="getScriptForNetwork"/>--}%',
         success: function (resp) {
             def.resolve(resp);
         },
         error: function (XMLHttpRequest, textStatus, errorThrown) {
             def.reject();

         },
         complete: function (XMLHttpRequest, textStatus) {
         }
         });

        def.done(function(value) {
            renderSankey(value)
        }).fail(function() {
            $("#networkTrendingSankey").html("No records to display");

        });

        var promise=$.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'networkUsage'},
            url: '%{--<g:createLink controller="networkUse" action="getScriptForNetwork"/>--}%'
        });

        promise.done(function(data){renderData(data);}).fail(function(err){$("#networkUsage").html("No records to display");})*/


        jQuery.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'networkTrendingSankey'},
            url: '<g:createLink controller="networkUse" action="getScriptForNetwork"/>',
            success: function (resp) {
                renderSankey(resp)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#networkTrendingSankey").html("No records to display");
            },
            complete: function (XMLHttpRequest, textStatus) {
            }
        });

        jQuery.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'networkUsage'},
            url: '<g:createLink controller="networkUse" action="getScriptForNetwork"/>',
            success: function (resp) {
                renderData(resp)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#networkUsage").html("No records to display");
            },
            complete: function (XMLHttpRequest, textStatus) {
            }
        });

        highOutNetworkUsage();
//        topOutOfNetworkProvider();
//        topOonProviderLocation();
//        topOonProviderSpeciality();


    }

    function highOutNetworkUsage(){

        jQuery.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'highOutNetworkUsage'},
            url: '<g:createLink controller="networkUse" action="getScriptForNetwork"/>',
            success: function (resp) {
                $("#highOutNetworkUsage").html(resp);



            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            complete: function (XMLHttpRequest, textStatus) {
                topOutOfNetworkProvider();
            }
        });
    }

    function topOutOfNetworkProvider(){

        jQuery.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'topOutOfNetworkProvider'},
            url: '<g:createLink controller="networkUse" action="getScriptForNetwork"/>',
            success: function (resp) {
                $("#topOutOfNetworkProvider").html(resp);


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            complete: function (XMLHttpRequest, textStatus) {
                topOonProviderLocation();
            }
        });
    }

    function topOonProviderLocation(){

        jQuery.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'topOonProviderLocation'},
            url: '<g:createLink controller="networkUse" action="getScriptForNetwork"/>',
            success: function (resp) {
                $("#topOonProviderLocation").html(resp);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            complete: function (XMLHttpRequest, textStatus) {
                topOonProviderSpeciality();
            }
        });
    }


    function topOonProviderSpeciality(){

        jQuery.ajax({
            type: 'POST',
            data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude, module: 'topOonProviderSpeciality'},
            url: '<g:createLink controller="networkUse" action="getScriptForNetwork"/>',
            success: function (resp) {
                $("#topOonProviderSpeciality").html(resp);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            },
            complete: function (XMLHttpRequest, textStatus) {
            }
        });
    }

    function formatDates(d) {
        var dDate=new Date(d);
        var oldDate=new Date(d);
        var newDate2=oldDate;
        newDate2.setFullYear(newDate2.getFullYear() - 1);
        newDate2.setDate(newDate2.getDate() +1);
        var dateString= (d3.time.format("%b %Y")(newDate2).toString()+" - "+d3.time.format("%b %Y")(dDate).toString())
        return dateString
    }


    function renderData(data) {
        $("#networkUsage").html("");

        var margin = {top: 20, right: 10, bottom: 40, left: 20},
                width = 500 - margin.left - margin.right,
                height = 350 - margin.top - margin.bottom;

        var x = d3.scale.ordinal()
                .rangeRoundBands([0, 300], .3);

        var y = d3.scale.linear()
                .rangeRound([height, 0]);

        var color = d3.scale.ordinal()
                .range(["grey","#5B9BD5","#ED7D31"]);



        var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom");

        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left")
                .tickFormat(d3.format(".2s"));

        var svg = d3.select("#networkUsage").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + -25 + "," + margin.top + ")");




        color.domain(d3.keys(data[0]).filter(function (key) {
            return (key !== "year" && key !== "inNetwork" && key !== "reportingDates" && key !== "totalPaidAmount"&& key !== "inNetwork" && key !== "unknown"&& key !== "outOfNetwork");
        }));



        data.forEach(function (d) {
            var y0 = 0;
            d.ages = color.domain().map(function (name) {
                return {name: name, y0: y0, y1: y0 += +d[name]};
            });
            d.total = d.ages[d.ages.length - 1].y1;
        });

        data.sort(function (a, b) {
            return b.order - a.order;
        });



        x.domain(data.map(function (d) {
            return d.year;
        }));

        var yearValues = data.map(function (d) {
            return d.year;
        });
        y.domain([0, d3.max(data, function (d) {
            return d.total * 1.4;
        })]);

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(30," + height + ")")
                .style("font-size", ".9em")
//                .call(xAxis)
                .call(d3.axisBottom(x))
                .selectAll(".tick text")
                .call(wrap, x.rangeBand());


        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Population");

        var state = svg.selectAll(".state")
                .data(data)
                .enter().append("g")
                .attr("class", "g")
                .attr("transform", function (d) {
                    return "translate(" + x(d.year) + ",0)";
                });

        state.selectAll("rect")
                .data(function (d) {
                    return d.ages;
                })
                .enter().append("rect")
                .attr("width", x.rangeBand())
                .attr("y", function (d) {
                    return y(d.y1);
                })
                .attr("height", function (d) {
                    return y(d.y0) - y(d.y1);
                })
                    .style("fill", function(d) {
                    return color(d.name); })

        var legend = svg.selectAll(".legend")
                .data(color.domain().slice().reverse())
                .enter().append("g")
                .attr("class", "legend")
                .attr("transform", function (d, i) {
                    return "translate(0," + (i * 22 - 15) + ")";
                });

        legend.append("rect")
                .attr("x", width - 18)
                .attr("width", 18)
                .attr("height", 18)
                .style("fill", function(d) {
                    return color(d);
                })

        legend.append("text")
                .attr("x", width - 165)
                .attr("y", 9)
                .attr("dy", ".35em")
                .style("text-anchor", "start")
                .style("fill", function(d) { return color(d); })
                .attr("font-weight", "bold")
                .attr("font-size", "12px")
                .text(function (d) {
                    var text;
                    if(d=="percentOfoutOfNetwork")
                        text="% Of Out Of Network"
                    if(d=="percentOfinNetwork")
                        text="% Of In Network"
                    if(d=="percentOfunknown")
                        text="% Of Unknown"
                    return text;
                });


        var textAppend = svg.selectAll(".text").data(data).enter()
                .append("text")
                .attr("class", "text")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 90) + 30) + "," + 40 + ")";
                })
                .text(function (d, i) {
                    return d3.round(d.percentOfunknown, 1)+ "%"
                }).attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .attr("font-weight", "bold")
                .attr("fill",function(d) { return color("percentOfunknown"); });

        var textAppend2 = svg.selectAll(".text1").data(data).enter()
                .append("text")
                .attr("class", "text1")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 90) + 30) + "," + 20 + ")";
                })
                .text(function (d, i) {
                    return d3.round(d.percentOfinNetwork, 1)+ "%"
                }).attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .attr("font-weight", "bold")
                .attr("fill", function(d) { return color("percentOfinNetwork"); });

        var textAppend3 = svg.selectAll(".text2").data(data).enter()
                .append("text")
                .attr("class", "text2")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 90) + 30) + "," + 0 + ")";
                })
                .text(function (d, i) {
                    return d3.round(d.percentOfoutOfNetwork, 1)+ "%"
                }).attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .attr("font-weight", "bold")
                .attr("fill", function(d){return color("percentOfoutOfNetwork")});
    }
    ;

    function wrap(text, width) {
        text.each(function() {
            var text = d3.select(this),
                    words = text.text().split(/\s+/).reverse(),
                    word,
                    line = [],
                    lineNumber = 0,
                    lineHeight = 1.1, // ems
                    y = text.attr("y"),
                    dy = parseFloat(text.attr("dy")),
                    tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
            }
        });
    }


    function custom_sort(a, b) {
        return a.date - b.date;
    }

    $( document ).ajaxStop(function() {

        var $table =$(".z5-fixed-header");
        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.z5-table');
            }

        });

    });




</script>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">
<style>
.x.axis path{
    /*display: none;*/
    /*stroke: black;*/
    fill: none;
    stroke-width: 1px;
    /*shape-rendering: crispEdges;*/
}
</style>

</body>
</html>