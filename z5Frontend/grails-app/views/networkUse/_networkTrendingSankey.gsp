<div class="col-md-12">
    %{--<div class="col-md-5"><h2>2015</h2></div>--}%
</div>
<div id="networkTrendingSankey">

</div>

<script src="https://d3js.org/d3.v4.min.js"></script>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js/dash', file: 'sankey.js')}"></script>

<script>

    function renderSankey(data){
        var data1=data.data;
        var units = "Member";

        // set the dimensions and margins of the graph
        var margin = {top: 50, right: 20, bottom: 50, left: 20},
                width = 800 - margin.left - margin.right,
                height = 400 - margin.top - margin.bottom;

        // format variables
        var formatNumber = d3.format(",.0f"),    // zero decimal places
                format = function(d) { return formatNumber(d) + " " + units; },
                color = d3.scaleOrdinal(["#01A46D","#FF9B2B","#EC3E40"]);

        // append the svg object to the body of the page
        var svg = d3.select("#networkTrendingSankey").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        // Set the sankey diagram properties
        var sankey = d3.sankey()
                .nodeWidth(26)
                .nodePadding(40)
                .size([width, height]);

        var path = sankey.link();

        // load the data
        //    d3.json("sankey.json", function(error, graph) {


        graph=data1;
        sankey
                .nodes(graph.nodes)
                .links(graph.links)
                .layout(32);

        // add in the links
        var link = svg.append("g").selectAll(".link")
                .data(graph.links)
                .enter().append("path")
                .attr("class",function(d){
                    /*var str="";
                     var src= d.source.node;

                     var reducedData=graph.nodes.reduce(function(acc,item){
                     if(item.node== src){ //filtering
                     acc=item.name; //mapping
                     return acc;
                     }
                     return acc;
                     },str);*/


//                   return ("link path"+reducedData);
                    return ("link");
                } )
                .style("stroke", function(d) {
                    return color(d.source.name.replace(/ .*/, "")); })
                .attr("d", path)
                .style("stroke-width", function(d) { return Math.max(0, d.dy); })
                .sort(function(a, b) { return b.dy - a.dy; });

        // add the link titles
        link.append("title")
                .text(function(d) {
                    return d.source.name + " → " +
                            d.target.name + "\n" + format(d.value); });

        // add in the nodes
        var node = svg.append("g").selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")"; })
                .call(d3.drag()
                        .subject(function(d) {
                            return d;
                        })
                        .on("start", function() {
                            this.parentNode.appendChild(this);
                        })
//                        .on("drag", dragmove)
        );

        // add the rectangles for the nodes
        node.append("rect")
                .attr("height", function(d) { return d.dy; })
                .attr("width", sankey.nodeWidth())
                .style("fill", function(d) {
                    return d.color = color(d.name.replace(/ .*/, ""));
                })
                .style("stroke", function(d) {
                    return d3.rgb(d.color).darker(2); })
                .append("title")
                .append("h6")
                .text(function(d) {
                    return /*d.name*/"Total:\n" + format(d.value)+ "\nNewly Enrolled:\n" + format(d.total-d.sumvalue); })


        // add in the title for the nodes
        node.append("text")
                .attr("x", -6)
                .attr("y", function(d) { return d.dy / 2; })
                .attr("dy", ".25em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .attr("class","textClass")
//                .style("text-transform","capitalize")
                .text(function(d) { return d.name; })
                .filter(function(d) { return d.x < width / 2; })
                .attr("x", 6 + sankey.nodeWidth())
                .attr("text-anchor", "start")


        var nodess = svg.append("g").selectAll(".nodes")
                .data([graph.nodes[0],graph.nodes[3],graph.nodes[6]])
                .enter().append("g").attr("class", "node").attr("transform", function(d) {
                    return "translate(" + (d.x+2) + "," + 0 + ")"; })

        nodess.append("text")
                .attr("x", 26)
                .attr("y", function(d) { return -30 ; })
                .attr("dy", ".85em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .attr("class","textClass1")
//                .style("text-transform","capitalize")
                .text(function(d) {

                    return data.year[d.year];
                })
//                .filter(function(d) { return d.x < width / 2; })
//                .attr("x", 6 + sankey.nodeWidth())
//                .attr("text-anchor", "start")

        // the function for moving the nodes
        function dragmove(d) {
            d3.select(this).attr("transform",
                    "translate(" + (
                            d.x = Math.max(0, Math.min(width - d.dx, d3.event.x))
                    )
                    + "," + (
                            d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))
                    ) + ")");
            sankey.relayout();
            link.attr("d", path);
        }
        //    });
    }





    /*var data2={
        "nodes":[
            {"node":0,"year":2013,"name":"Overweight","total":800},
            {"node":1,"year":2013,"name":"Underweight","total":700},
            {"node":2,"year":2013,"name":"Normal","total":200},
            {"node":3,"year":2014,"name":"Overweight","total":900},
            {"node":4,"year":2014,"name":"Underweight","total":450},
            {"node":5,"year":2014,"name":"Normal","total":1000},
            {"node":6,"year":2015,"name":"Overweight","total":900},
            {"node":7,"year":2015,"name":"Underweight","total":550},
            {"node":8,"year":2015,"name":"Normal","total":480}

        ],
        "links":[
            {"source":0,"target":3,"value":100},
            {"source":0,"target":4,"value":200},
            {"source":0,"target":5,"value":200},

            {"source":1,"target":3,"value":150},
            {"source":1,"target":4,"value":200},
            {"source":1,"target":5,"value":260},

            {"source":2,"target":3,"value":100},
            {"source":2,"target":4,"value":50},
            {"source":2,"target":5,"value":30},

            {"source":3,"target":6,"value":260},
            {"source":3,"target":7,"value":160},
            {"source":3,"target":8,"value":100},

            {"source":4,"target":6,"value":90},
            {"source":4,"target":7,"value":60},
            {"source":4,"target":8,"value":50},

            {"source":5,"target":6,"value":100},
            {"source":5,"target":7,"value":300},
            {"source":5,"target":8,"value":200}

        ]}*/





</script>


<style type="text/css">

.textClass{
    text-transform: capitalize;
}
.textClass1{
    font-weight: bold;
    font-size: 14px;
}
.node rect {
    cursor: move;
    fill-opacity: .9;
    shape-rendering: crispEdges;
}

.node text {
    pointer-events: none;
    text-shadow: 0 1px 0 #fff;
}

.link {
    fill: none;
    stroke: #000;
    stroke-opacity: .5;
}

.link:hover {
    stroke-opacity: .8;
}

.pathA{
    stroke:blue;
}
.pathB{
    stroke:steelblue;
}
.pathC{
    stroke:orange;
}

</style>