<%@ page import="com.z5.FieldContentType" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="z5-table put-border" style="min-height: 50px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
        <thead>
        <tr class="heading">
            <g:if test="${size}">
                %{--<g:each in="${columns}" var="column" status="i">
                    <g:if test="${column.key=='memberId'}">

                        <th style="text-align:center;" id="th_${column.key}" data-class="${column.key}">${column.value.getAt('displayName')}</th>

                    </g:if><g:else>--}%
                <th style="text-align:center;" >Member Id</th>
                <util:remoteSortableColumn  class="text-left ${defaultSort=='medicalPaid'?'sortable sorted desc':''}"
                                            property="medicalPaid" title="Medical Paid" update="highOutNetworkUsage"
                                            controller="networkUse" action="getScriptForNetwork" params="[module:'highOutNetworkUsage']"  />
                <util:remoteSortableColumn  class="text-left ${defaultSort=='medicalOutNetworkPaid'?'sortable sorted desc':''}"
                                                property="medicalOutNetworkPaid" title="Out Of Network Paid" update="highOutNetworkUsage"
                                                controller="networkUse" action="getScriptForNetwork" params="[module:'highOutNetworkUsage']"  />
                    %{--</g:else>
                </g:each>--}%
            </g:if><g:else>
            <g:each in="${columns}" var="column" status="i">
                <g:if test="${column.value.getAt('displayName') == "Speciality Desc" || column.value.getAt('displayName') == "City"}">
                    <th style="text-align:center;" id="th_${column.key}" data-class="${column.key}">${column.value.getAt('displayName')}</th>
                </g:if>
                <g:else>
                <util:remoteSortableColumn  class="text-left ${defaultSort==column.key?'sortable sorted desc':''}" id="th_${column.key}" data-class="${column.key}"
                                            property="${column.key}" title="${column.value.getAt('displayName')}" update="${module}"
                                            controller="networkUse" action="getScriptForNetwork" params="[module:module]"  />
                </g:else>
                %{--<th style="text-align:center;" id="th_${column.key}" data-class="${column.key}">${column.value.getAt('displayName')}</th>--}%
            </g:each>
            </g:else>
        </tr>
        </thead>
        <tbody>
        <g:if test="${data}">
            <g:each in="${data}" var="val" status="i">
            <tr>
            <g:each in="${columns}" var="col" status="j">
                <g:if test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Number.toString()}">
                    <td style="text-align: center">${val.getAt(col.key)}</td>
                </g:if>
                <g:elseif test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Amount.toString() || col.value.getAt('type').toString() == com.z5.FieldContentType.LargeAmount.toString()}">

                        <td style="text-align: right">
                            <strong><g:formatZ5Currency number="${val.getAt(col.key)}" type="number" maxFractionDigits="2" />
                            </strong>
                        </td>
                </g:elseif>

                <g:else>
                    <td style="text-align: left">${val.getAt(col.key)}</td>
                </g:else>
            </g:each>
            <tr>
                <tr>

                </tr>
            </g:each>
        </g:if>

        <g:if test="${oondata}">
            <g:each in="${oondata}" var="keyVal" status="i">
                <%
                    def cCount=i;
                %>
                    <g:set var="columnType" value="${keyVal.total.state?keyVal.total.state:keyVal.total.specialityCode}"/>
                    <tr class=" parent-row z5-row-${cCount%2} parent-row-no-${cCount}" id="parent-row-${columnType}" style="font-weight: bold;cursor: pointer;">
                    <g:each in="${columns}" var="col" status="j">
                            <g:if test="${col.value.getAt('displayName') == "Speciality Desc" || col.value.getAt('displayName') == "City"}">
                                <td style="text-align: center">--</td>
                            </g:if>
                            <g:elseif test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Number.toString()}">
                                <td style="text-align: center">${keyVal.total.getAt(col.key)}</td>
                            </g:elseif>
                        <g:elseif test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Amount.toString() || col.value.getAt('type').toString() == com.z5.FieldContentType.LargeAmount.toString()}">
                            <td style="text-align: right">
                                <strong><g:formatZ5Currency number="${keyVal.total.getAt(col.key)}" type="number" maxFractionDigits="2" />
                                </strong>
                            </td>
                        </g:elseif>
                            <g:else>
                                <td style="text-align: left">${keyVal.total.getAt(col.key)}</td>
                            </g:else>
                    </g:each>
                    </tr>
                    <g:if test="${keyVal.cat}">
                    <g:each in="${keyVal.cat}" var="vals">
                        <g:set var="columnType" value="${vals.state?vals.state:vals.specialityCode}"/>
                        <tr class="z5-row-${cCount%2} child-rows child-row-${columnType} child-row-no-${cCount}" >
                            <g:each in="${columns}" var="col" status="j">
                            <g:if test="${col.value.getAt('displayName') == "Speciality Code"  || col.value.getAt('displayName') == "State"}">
                                <td style="text-align: center"></td>
                            </g:if>
                            <g:elseif test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Number.toString()}">
                                <td style="text-align: center">${vals.getAt(col.key)}</td>
                            </g:elseif>
                            <g:elseif test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Amount.toString() || col.value.getAt('type').toString() == com.z5.FieldContentType.LargeAmount.toString()}">
                                <td style="text-align: right">
                                   <g:formatZ5Currency number="${vals.getAt(col.key)}" type="number" maxFractionDigits="2" />
                                </td>
                            </g:elseif>
                            <g:else>
                                <td style="text-align: left;overflow-wrap: break-word;word-wrap: break-word;">
                                    ${vals.getAt(col.key)}</td>
                            </g:else>
                        </g:each></tr>
                    </g:each>
                     </g:if>
                    <g:else>
                    <g:set var="columnType" value="${keyVal.total.state?keyVal.total.state:keyVal.total.specialityCode}"/>
                            <tr class="z5-row-${cCount%2} child-rows child-row-${columnType} child-row-no-${cCount}" >
                                <td colspan="5">No records to display.</td>
                            </tr>
                    </g:else>

                </g:each>
        </g:if>

    <g:if test="${!oondata && !data}">
        <tr>
            <td colspan="5">
                No records to display.
            </td>
        </tr>
    </g:if>
        </tbody>
    </table>
</div>
<g:if test="${size}">
    <div class="z5-pagination">
        <util:remotePaginate controller="networkUse" action="getScriptForNetwork" total="${size}" update="highOutNetworkUsage" max="5" pageSizes="[5:'5 Records/Page', 20: '20 Records/Page', 50:'50 Records/Page']" params="[module:'highOutNetworkUsage',sort:defaultSort,order:order]" alwaysShowPageSizes="true" />
    </div>
</g:if>

<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>

<script>
    $(document).ready(function() {
        $(".child-rows").hide();
        $(".child-row-no-0").show();
        $("table tr.parent-row").on("click",function(){
            var tables=$(this).closest('table');
            var id=this.id.split("-")[2];
            tables.find(".child-rows").hide();
            tables.find(".child-row-"+id).show();
        })

        %{--applyFloat('${module}');--}%


    });




    /*function applyFloat(ids){
        setTimeout(function(){
            var $table =$("#"+ids).find("table.z5-fixed-header");
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.z5-table');
                }

            });
        },10000);

    }*/

</script>






