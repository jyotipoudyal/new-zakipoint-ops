<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking</title>
    <style type="text/css">
        table.explorer-table{border-collapse: collapse;}
        table.explorer-table th{font-weight: normal}
        table.explorer-table th{border-collapse: collapse; color:#666; padding: 5px;}
        table.explorer-table th{text-align: center;}
        table.explorer-table td {border-collapse: collapse; border: 1px solid #ccc; padding: 5px;}
        table.explorer-table tbody td:nth-child(2){text-align: center;}
        table.explorer-table tbody td:nth-child(3){text-align: center;}
        table.explorer-table tr:nth-child(even) {background:#e7eceb}
        table.explorer-table tr:nth-child(odd) {background: #e5f0fb }
    </style>
</head>

<body>


<div class="contentWrapper">
    <g:hiddenField name="showReportingDetails" value="false"/>
    <div class="container">


    <div class="col-md-12">
        <h2 class="mb15">
            <g:img uri="/images/zExplorer.png" class="heading-icon" style="width:53px;"/>
            <span class="isEditable">zProgram - Opportunity Explorer</span></h2>
        <div class="sep-border mb15"></div>

    </div>

    <div class="">
        <div class="col-md-6">
            <div class="col-md-12"><g:checkBox name="1"/>Include members already in program</div>

            <div class="clearfix"></div>
            <div class="col-md-1"></div>
            <div class="col-md-10 "><g:checkBox name="1"/>Include engaged members</div>

            <div class="clearfix"></div>
            <div class="col-md-1"></div>
            <div class="col-md-10"><g:checkBox name="1"/>Include not engaged members</div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12"><g:checkBox name="1"/>Only include members identified in last 3 (<u>change</u>) months</div>
            <div class="col-md-12"><g:checkBox name="1"/>Exclude members with risk score &gt;5 (<u>change</u>)</div>
            <div class="col-md-12"><g:checkBox name="1"/>Exclude members with more than 3 (<u>change</u>) co-morbidities</div>
        </div>
        <div class="clearfix mb10"></div>

    </div>

    <div class="col-md-4">Sort By <g:select name="sel" class="clientSelect"  from="['Savings']"/></div>
    <div class="col-md-8" >
        <div style="float: right;">
            Filter By
            <button class="btn">Age > 41</button>
            <button class="btn">Gender</button>
            <button class="btn">Location</button>
            <button class="btn">Relationship</button>
            <button class="btn">Division</button>
            </div>
    </div>
    <div class="clearfix mb10"></div>

    <table class="explorer-table" cellpadding="0" cellspacing="0" border="0" width="100%">
        <thead>
            <tr style="background: #fff">
                <th>Problem</th>
                <th>Member Count</th>
                <th>Potential Impact</th>
                <th>Actions</th>
                <th>Providers</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><div style="float: left"><g:img uri="/images/icon-overuse-er.png" class="heading-icon"/></div><div style="float: left"><b>Overuse of ER</b><br><h4>$243,000</h4></div></td>
                <td><a href='#' >183</a></td>
                <td> <a href='#' >$78K</a></td>
                <td><ul><li><b>Implement telemedicine program </b><br></li><li><b>Modify plan design</b><br></li><li><b>Improve access to Urgent Care</b><br></li></ul></td>
                <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>
            </tr>


        <tr>
            <td><div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left"><b>Missing Age/Gender Appropriate Preventative Screenings</b><br><h4>$250,000</h4></div></td>
            <td><a href='#' >465</a></td>
            <td><a href='#' >$56K</a></td>
            <td><ul><li><b>Impose penalty in premium</b>,<br></li><li><b>Letter campaign</b><br></li><li><b>Email campaign </b><br></li><li><b>Incentive design</b></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>
        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left"><b>Unmanaged Diabetes</b><br><h4>$1,235,000</h4></div>
            </td>
            <td><a href='#' >95</a></td>
            <td><a href='#' >$185K</a></td>
            <td><ul><li><b>Diabetes management program</b><br></li><li><b>Wellness program specific to diabetes management</b><br></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>


        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left"><b>Pre-Diabetes Education</b><br><h4>$589,000</h4></div>

            </td>
            <td><a href='#' >137</a></td>
            <td><a href='#' >$22K</a></td>
            <td><ul><li><b>Start pre-diabetes education program</b><br></li><li><b>Add diabetes education to existing wellness program</b><br></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon"/></div><div style="float: left"><b>High Branded Drug Usage</b><br><h4>$889,000</h4></div>
            </td>
            <td><a href='#' >89</a></td>
            <td><a href='#' >$123K</a></td>
            <td><ul><li><b>Start Rx carve-out program</b></li><li><b>Drug tier change</b><br></li><li><b>Plan design change</b><br></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left"><b>Emerging High Risk Members</b><br><h4>$2,356,000</h4></div>
            </td>
            <td><a href='#' >367</a></td>
            <td><a href='#' >$67K</a></td>
            <td><ul><li><b>Wellness coaching</b></li><li><b>Plan design change to improve biometric monitoring</b><br></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                <b>High Gaps-in-Care: COPD</b><br><h4>$75,000</h4></div>
            </td>
            <td><a href='#' >127</a></td>
            <td><a href='#' >$91K</a></td>
            <td><ul><li><b>COPD care program</b></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                <b>Members not participating in 'Walking program'</b><br><h4></h4></div>

            </td>
            <td><a href='#' >200</a></td>
            <td><a href='#' >$45K</a></td>
            <td><ul><li><b>Start new Wellness Program</b><br></li><li><b>Modifiy incentives</b><br></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                <b>Saving from Pharmacy Benefit Carve-out not leveraged</b><br><h4></h4></div>
            </td>
            <td><a href='#' >200</a></td>
            <td><a href='#' >$200K</a></td>
            <td><ul><li><b>Start new Rx Carve-out solution</b><br></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                <b>Members Suitable for Risk Appropriate Coaching</b><br><h4>$865,000</h4></div>
            </td>
            <td><a href='#' >All Members</a></td>
            <td><a href='#' >$34K</a></td>
            <td><ul><li><b>Restart outreach campaigns and enroll</b></li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>




        </tbody>

    </table>

</div>
    </div>
</body>
</html>