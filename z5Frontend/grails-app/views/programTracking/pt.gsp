<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking</title>
    <r:require module="graph"/>

</head>
<body>

<div class="contentWrapper">
    <g:hiddenField name="showReportingDetails" value="false"/>
    <g:hiddenField name="metricsId" value="${params.metrics}"/>
    <div class="container">

    <button id="risk" class="btn btn-primary"   onclick="renderGraph('risk')">RISK</button>
    <button id="pmpm" class="btn btn-primary" onclick="renderGraph('pmpm')">PMPM</button>
    <button id="erutil" class="btn btn-primary" onclick="renderGraph('er')">Er VIsits</button>
    <button id="admission" class="btn btn-primary" onclick="renderGraph('admits')">Admits</button>
    <button id="claimsover10k" class="btn btn-primary" onclick="renderGraph('over')">Claims > 10k</button>
    <button id="claimsunder10k" class="btn btn-primary" onclick="renderGraph('below')">Claims < 10k</button>
    <br>
    <br>
    <div class="watermark"><div style="display: inline;text-transform: capitalize">
        &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
    </div><br>
        <div id="nick"></div>


    <div>
        <svg width="800" height="80">
            <g transform="translate(100,24)">
                <g><rect y="0" fill="grey" x="30" height="10" width="33"></rect>
                    <text class="text_y" text-anchor="end" transform="translate(20,8)">Baseline</text></g>
                </g>

            <g transform="translate(300,24)">
                <g><rect y="0" fill="#C42A22" x="30" height="10" width="33"></rect>
                    <text class="text_y" text-anchor="end" transform="translate(20,8)">Not in program</text></g>
            </g>
            <g transform="translate(500,24)">
                <g><rect y="0" fill="#E7A170" x="30" height="10" width="33"></rect>
                    <text class="text_y" text-anchor="end" transform="translate(20,8)">Not Engaged</text></g>
            </g>
            <g transform="translate(700,24)">
                <g><rect y="0" fill="#01A46D" x="30" height="10" width="33"></rect>
                    <text class="text_y" text-anchor="end" transform="translate(20,8)">Engaged</text></g>
            </g>


            </svg>

    </div>

        <div class="container mt25">


            <br>
            <div class=" col-md-4 ">
                <div class="clearfix"></div>
                <div  id="pt1Graph" ></div>
                <div  id="pt1GraphContent"></div>
            </div>
            <div class=" col-md-4 ">
                <div class="clearfix"></div>
                <div  id="pt2Graph"></div>
                <div  id="pt2GraphContent"></div>
            </div>
            <div class=" col-md-4 ">
                <div class="clearfix"></div>
                <div  id="pt3Graph"></div>
                <div  id="pt3GraphContent"></div>
            </div>

        </div>


        </div>
    </div>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">


<g:javascript>


    function renderMultiLineGraph(da){
        $("#nick").html("");
        var main_margin = {top: 20, right: 60, bottom: 50, left: 70},
                main_width = 900 - main_margin.left - main_margin.right,
                main_height = 400 - main_margin.top - main_margin.bottom;
        var claims = da;
        var newVal;
        var data = [];
        var parseDate4 = d3.time.format("%b-%y").parse;
        var title=";"

        var claimsName = d3.nest()
                .key(function(d) { return d.Status; })
                .entries(claims);


        var map=new Object();

        $.each(claims, function (key, val) {
            newVal = new Object();
            newVal.time1 = parseDate4(val['Date'])
            //newVal,status=val['Status']
            newVal.actual=val['Parameter Value']

            //if( map[parseDate4(val['Date'])] < val['Parameter Value'])
            //map[parseDate4(val['Date'])]=val['Parameter Value']


            data.push(newVal);
            title=val['Parameter'];
        });

        //console.log(map)


        var main_x = d3.time.scale()
                .range([0, main_width]);
        main_x.domain(d3.extent(data, function(d) { return d.time1; }));

        var main_y = d3.scale.linear()
                .range([main_height, 0]);
        main_y.domain([0, d3.max(data, function(d) { return d.actual; })*1.2]);
        //main_y.domain([d3.min(data, function(d) { return d.actual; }), d3.max(data, function(d) { return d.actual; })]);
        //main_y.domain([0,2]);

        var main_line = d3.svg.line()
                .x(function (d) {
                    return main_x(parseDate4(d['Date']));
                })
                .y(function (d) {
                    return main_y(d['Parameter Value']);
                });

        var svg = d3.select("#nick").append("svg")
                .attr("width", main_width + main_margin.left + main_margin.right)
                .attr("height", main_height + main_margin.top + main_margin.bottom);

        svg.append("defs").append("clipPath")
                .attr("id", "clip")
                .append("rect")
                .attr("width", main_width)
                .attr("height", main_height);

        var main = svg.append("g")
                .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");

        var main_xAxis2 = d3.svg.axis()
                    .scale(main_x)
                    .tickFormat(d3.time.format("%b-%y"))
                    .orient("bottom")



        main.append("g")
                .attr("class", "x axis xaxisLeft")
                .attr("transform", "translate(0," + (main_height) + ")")
                .call(main_xAxis2)
                .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", ".15em")
                .attr("transform", "rotate(-90)" );


        var main_yAxisLeft = d3.svg.axis()
                .scale(main_y)
                .ticks(5)
                .tickSize(-main_width)
                .outerTickSize(0)
                .orient("left");

        main.append("g")
                .attr("class", "y axis yaxisLeft")
                .call(main_yAxisLeft)
                .selectAll("text")
                .attr("x", -20)

        main.selectAll("line")
                .attr("stroke", "#ccc");

        main.selectAll(".tick")
                .attr("stroke-dasharray", "3 3")
                .attr("opacity", ".5");

        main.append("text")
                .attr("class", "text_y")
                .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                .attr("transform", "translate(" + (-50) + "," + (main_height / 2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                .text(title)
        .style("font-size","16px");



        var colors={"Baseline":"grey","Not in program":"#C42A22","Not Engaged":"#E7A170","Engaged":"#01A46D"};
        var repVal;
        $.each(claimsName, function (key, val) {
            var len=val['values'].length-1
            if(val.key=="Baseline"){
                repVal=val.values[len]
            }else{
                val.values[0]=repVal;
            }


            var path1=main.append("path")
                    .attr("class", "line line0")
                    .attr("d", main_line(val['values']))
                    .attr("fill", "none")
                    .style("stroke", colors[val.key])
                    .style('stroke-width', '2px');

            var totalLength = path1.node().getTotalLength();

            path1.attr("stroke-dasharray", totalLength + " " + totalLength)
                    .attr("stroke-dashoffset", totalLength)
                    .transition()
                    .duration(400)
                    .delay(function(d, i) {
                        if(key>0)
                        return 400;
                        else
                        return 0;
                    })
                    .ease("linear")
                    .attr("stroke-dashoffset", 0)

        });

       /* var focus = main.append("g")
                .attr("class", "focus")
                .style("display", "block");

        focus.append("line")
                .attr("class", "x")
                .attr("y1", 0);

        focus.style("visibility", "hidden");


        main.append("rect")
                .attr("class", "overlay")
                .attr("fill", "none")
                .attr("pointer-events", "all")
                .attr("cursor", "default")
                .attr("width", main_width)
                .attr("height", main_height)
                .on("mouseover", function () {
                    focus.style("display", null);
                })
                .on("mouseout", function () {
                    div.transition()
                            .duration(500)
                            .style("opacity", 0);
                    focus.style("display", "none");
                })
                .on("mousemove", mousemove);

        main.on("mouseover", function () {
            focus.style("display", null);
        }).on("mouseout", function () {
            div.transition()
                    .duration(500)
                    .style("opacity", 0);
            focus.style("display", "none");
        }).on("mousemove", mousemove);

        var formatDate = d3.time.format("%d-%b-%y"),
                parseDate = formatDate.parse,
                bisectDate = d3.bisector(function (d) {
                    return d.time1;
                }).left;

        function mousemove() {
            focus.style("visibility", "visible");
            var x0 = main_x.invert(d3.mouse(this)[0]),
                    i = bisectDate(data, x0, 1),
                    d0 = data[i - 1],
                    d1 = data[i],
                    d = x0 - d0.time1 > d1.time1 - x0 ? d1 : d0;
            focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")").attr("y2", main_height - main_y(d.actual));

            *//* div.transition()
                    .duration(100)
                    .style("opacity", .9);
            div.html("<span class='formatOutput0'>23</span><br><br><span class='formatOutput1'>23</span>")
                    .style("left", (d3.event.pageX + 30) + "px")
                    .style("top", (d3.event.pageY - 120) + "px");*//*


        } */


}

    function renderGraph(ele){
        if(ele=="risk"){
            renderMultiLineGraph(data1);
        }else if(ele=="pmpm"){
            renderMultiLineGraph(data2);
        }else if(ele=="er"){
            renderMultiLineGraph(data4Er);
        }else if(ele=="admits"){
            renderMultiLineGraph(data3Ad);
        }else if(ele=="over"){
            renderMultiLineGraph(data5Over);
        }else if(ele=="below"){
            renderMultiLineGraph(data6Below);
        }

    }

$(function(){
    var value=$("#metricsId").val();
    if(value){
        $("#"+value).click().focus();
    }


    renderMultiLineGraph(data1);
    $("#risks").focus();

    /*

    */
/*var dataset = []; //Initialize empty array
for (var i = 0; i < 25; i++) { //Loop 25 times
    var newNumber = Math.floor(Math.random() * 30); //New random number (0-30) random generates decimal,, floor converts to whole num
    dataset.push(newNumber); //Add new number to array
}*//*



var dataset = [ 5, 10, 15, 20, 25 ];
var w=500;
var h=100;
var barPadding=2;

var svg=d3.select("#nick").append("svg").attr("width",w).attr("height",h);
*/
/*var a=svg.selectAll("circle")
        .data(dataset)
        .enter()
        .append("circle")
        .attr("cx",function(d,i){return (i*50)+25;})
        .attr("cy",50)
        .attr("r",function(d){return d;})*//*


*/
/*var circles = svg.selectAll("circle")
        .data(dataset)
        .enter()
        .append("circle");

circles.attr("cx", function(d, i) {
    return (i * 50) + 25;
})
        .attr("cy", h/2)
        .attr("r", function(d) {
            return d;
        });*//*


var dataset1 = [ 5, 10, 13, 19, 21, 25, 22, 18, 15, 13,
    11, 12, 15, 20, 18, 17, 16, 18, 23, 25 ]



*/
/*svg.selectAll("rect")
        .data(dataset1)
        .enter()
        .append("rect")
        //.attr("x", function(d,i){return i* (w / dataset1.length);;})
        //.attr("y", function(d){return h-d;})
        //.attr("width", 20)
        //.attr("height", function(d){return d;})
        .attr({
            x: function(d, i) { return i * (w / dataset1.length); },
            y: function(d) { return h - (d * 4); },
            width: w / dataset1.length - barPadding,
            height: function(d) { return d * 4; },
            fill: function(d) { return "rgb(0, 0, " + (d * 10) + ")"; }
        });



svg.selectAll("text")
        .data(dataset1)
        .enter()
        .append("text").text(function(d) {
            return d;
        }).attr("x", function(d, i) {
         return i * (w / dataset1.length)+5;
        })
        .attr("y", function(d) {
            return h - (d * 4)+15;
        }) .attr("font-family", "sans-serif")
        .attr("font-size", "11px")
        .attr("fill", "white");*//*


//fetchChartScript("renderFunnelBar","pt1Graph");
//fetchChartScript("renderFunnelBar","pt2Graph");
//fetchChartScript("renderFunnelBar","pt3Graph");

//renderFunnelBar(data1,"pt1Graph")
//renderFunnelBar(data2,"pt2Graph")
//renderFunnelBar(data3,"pt3Graph")


        var dataset2 = [
            [ 5, 20 ],
            [ 480, 90 ],
            [ 250, 50 ],
            [ 100, 33 ],
            [ 330, 95 ],
            [ 410, 12 ],
            [ 475, 44 ],
            [ 103, 8],
            [ 25, 67 ],
            [ 85, 21 ]
            ];

svg.selectAll("circle")
        .data(dataset2)
        .enter()
        .append("circle")
        .attr({
            cx: function(d, i) { return d[0]; },
            cy: function(d) { return d[1]; },
            r: function(d) { return Math.sqrt(h - d[1]); }
        });

svg.selectAll("text")
        .data(dataset2)
        .enter()
        .append("text")
        .text(function(d){return d[0]+","+d[1]})
        .attr({
            x: function(d, i) { return d[0]+4; },
            y: function(d) { return d[1]; }
        });


*/




});

            var data6Below=[
                {
                    "Status": "Baseline",
                    "Date": "Jan-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3000
                },
                {
                    "Status": "Baseline",
                    "Date": "Feb-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3440
                },
                {
                    "Status": "Baseline",
                    "Date": "Mar-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3120
                },
                {
                    "Status": "Baseline",
                    "Date": "Apr-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4000
                },
                {
                    "Status": "Baseline",
                    "Date": "May-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2890
                },
                {
                    "Status": "Baseline",
                    "Date": "Jun-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4120
                },
                {
                    "Status": "Baseline",
                    "Date": "Jul-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3100
                },
                {
                    "Status": "Baseline",
                    "Date": "Aug-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4010
                },
                {
                    "Status": "Baseline",
                    "Date": "Sep-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3480
                },
                {
                    "Status": "Not in program",
                    "Date": "Sep-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3120
                },
                {
                    "Status": "Not in program",
                    "Date": "Oct-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4000
                },
                {
                    "Status": "Not in program",
                    "Date": "Nov-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2890
                },
                {
                    "Status": "Not in program",
                    "Date": "Dec-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4120
                },
                {
                    "Status": "Not in program",
                    "Date": "Jan-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3100
                },
                {
                    "Status": "Not in program",
                    "Date": "Feb-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4010
                },
                {
                    "Status": "Not in program",
                    "Date": "Mar-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3480
                },
                {
                    "Status": "Not in program",
                    "Date": "Apr-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3440
                },
                {
                    "Status": "Not in program",
                    "Date": "May-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4120
                },
                {
                    "Status": "Engaged",
                    "Date": "Sep-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3180
                },
                {
                    "Status": "Engaged",
                    "Date": "Oct-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3000
                },
                {
                    "Status": "Engaged",
                    "Date": "Nov-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3030
                },
                {
                    "Status": "Engaged",
                    "Date": "Dec-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2800
                },
                {
                    "Status": "Engaged",
                    "Date": "Jan-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2750
                },
                {
                    "Status": "Engaged",
                    "Date": "Feb-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2600
                },
                {
                    "Status": "Engaged",
                    "Date": "Mar-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2650
                },
                {
                    "Status": "Engaged",
                    "Date": "Apr-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2620
                },
                {
                    "Status": "Engaged",
                    "Date": "May-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 2600
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Sep-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4000
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Oct-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4440
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Nov-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4140
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Dec-15",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4000
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Jan-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4230
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Feb-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3880
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Mar-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 3660
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Apr-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4010
                },
                {
                    "Status": "Not Engaged",
                    "Date": "May-16",
                    "Parameter": "Claims < 10K",
                    "Parameter Value": 4440
                }
            ]


            var data5Over=[
                {
                    "Status": "Baseline",
                    "Date": "Jan-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2000
                },
                {
                    "Status": "Baseline",
                    "Date": "Feb-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2440
                },
                {
                    "Status": "Baseline",
                    "Date": "Mar-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2120
                },
                {
                    "Status": "Baseline",
                    "Date": "Apr-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3000
                },
                {
                    "Status": "Baseline",
                    "Date": "May-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1890
                },
                {
                    "Status": "Baseline",
                    "Date": "Jun-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3120
                },
                {
                    "Status": "Baseline",
                    "Date": "Jul-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2100
                },
                {
                    "Status": "Baseline",
                    "Date": "Aug-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3010
                },
                {
                    "Status": "Baseline",
                    "Date": "Sep-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2480
                },
                {
                    "Status": "Not in program",
                    "Date": "Sep-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2120
                },
                {
                    "Status": "Not in program",
                    "Date": "Oct-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3000
                },
                {
                    "Status": "Not in program",
                    "Date": "Nov-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1890
                },
                {
                    "Status": "Not in program",
                    "Date": "Dec-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3120
                },
                {
                    "Status": "Not in program",
                    "Date": "Jan-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2100
                },
                {
                    "Status": "Not in program",
                    "Date": "Feb-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3010
                },
                {
                    "Status": "Not in program",
                    "Date": "Mar-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2480
                },
                {
                    "Status": "Not in program",
                    "Date": "Apr-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2440
                },
                {
                    "Status": "Not in program",
                    "Date": "May-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3120
                },
                {
                    "Status": "Engaged",
                    "Date": "Sep-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2180
                },
                {
                    "Status": "Engaged",
                    "Date": "Oct-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2000
                },
                {
                    "Status": "Engaged",
                    "Date": "Nov-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2030
                },
                {
                    "Status": "Engaged",
                    "Date": "Dec-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1800
                },
                {
                    "Status": "Engaged",
                    "Date": "Jan-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1750
                },
                {
                    "Status": "Engaged",
                    "Date": "Feb-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1600
                },
                {
                    "Status": "Engaged",
                    "Date": "Mar-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1650
                },
                {
                    "Status": "Engaged",
                    "Date": "Apr-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1620
                },
                {
                    "Status": "Engaged",
                    "Date": "May-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 1600
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Sep-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3000
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Oct-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3440
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Nov-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3140
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Dec-15",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3000
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Jan-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3230
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Feb-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2880
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Mar-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 2660
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Apr-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3010
                },
                {
                    "Status": "Not Engaged",
                    "Date": "May-16",
                    "Parameter": "Claims > 10K",
                    "Parameter Value": 3440
                }
            ]

            var data4Er=[
                {
                    "Status": "Baseline",
                    "Date": "Jan-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 300
                },
                {
                    "Status": "Baseline",
                    "Date": "Feb-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 344
                },
                {
                    "Status": "Baseline",
                    "Date": "Mar-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 312
                },
                {
                    "Status": "Baseline",
                    "Date": "Apr-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 400
                },
                {
                    "Status": "Baseline",
                    "Date": "May-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 289
                },
                {
                    "Status": "Baseline",
                    "Date": "Jun-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 412
                },
                {
                    "Status": "Baseline",
                    "Date": "Jul-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 310
                },
                {
                    "Status": "Baseline",
                    "Date": "Aug-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 401
                },
                {
                    "Status": "Baseline",
                    "Date": "Sep-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 348
                },
                {
                    "Status": "Not in program",
                    "Date": "Sep-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 312
                },
                {
                    "Status": "Not in program",
                    "Date": "Oct-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 400
                },
                {
                    "Status": "Not in program",
                    "Date": "Nov-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 289
                },
                {
                    "Status": "Not in program",
                    "Date": "Dec-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 412
                },
                {
                    "Status": "Not in program",
                    "Date": "Jan-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 310
                },
                {
                    "Status": "Not in program",
                    "Date": "Feb-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 401
                },
                {
                    "Status": "Not in program",
                    "Date": "Mar-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 348
                },
                {
                    "Status": "Not in program",
                    "Date": "Apr-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 344
                },
                {
                    "Status": "Not in program",
                    "Date": "May-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 412
                },
                {
                    "Status": "Engaged",
                    "Date": "Sep-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 318
                },
                {
                    "Status": "Engaged",
                    "Date": "Oct-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 300
                },
                {
                    "Status": "Engaged",
                    "Date": "Nov-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 303
                },
                {
                    "Status": "Engaged",
                    "Date": "Dec-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 280
                },
                {
                    "Status": "Engaged",
                    "Date": "Jan-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 275
                },
                {
                    "Status": "Engaged",
                    "Date": "Feb-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 260
                },
                {
                    "Status": "Engaged",
                    "Date": "Mar-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 265
                },
                {
                    "Status": "Engaged",
                    "Date": "Apr-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 262
                },
                {
                    "Status": "Engaged",
                    "Date": "May-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 260
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Sep-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 400
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Oct-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 444
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Nov-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 414
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Dec-15",
                    "Parameter": "ER Visits",
                    "Parameter Value": 400
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Jan-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 423
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Feb-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 388
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Mar-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 366
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Apr-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 401
                },
                {
                    "Status": "Not Engaged",
                    "Date": "May-16",
                    "Parameter": "ER Visits",
                    "Parameter Value": 444
                }
            ]

            var data3Ad=[
                {
                    "Status": "Baseline",
                    "Date": "Jan-15",
                    "Parameter": "Admits",
                    "Parameter Value": 200
                },
                {
                    "Status": "Baseline",
                    "Date": "Feb-15",
                    "Parameter": "Admits",
                    "Parameter Value": 244
                },
                {
                    "Status": "Baseline",
                    "Date": "Mar-15",
                    "Parameter": "Admits",
                    "Parameter Value": 212
                },
                {
                    "Status": "Baseline",
                    "Date": "Apr-15",
                    "Parameter": "Admits",
                    "Parameter Value": 300
                },
                {
                    "Status": "Baseline",
                    "Date": "May-15",
                    "Parameter": "Admits",
                    "Parameter Value": 189
                },
                {
                    "Status": "Baseline",
                    "Date": "Jun-15",
                    "Parameter": "Admits",
                    "Parameter Value": 312
                },
                {
                    "Status": "Baseline",
                    "Date": "Jul-15",
                    "Parameter": "Admits",
                    "Parameter Value": 210
                },
                {
                    "Status": "Baseline",
                    "Date": "Aug-15",
                    "Parameter": "Admits",
                    "Parameter Value": 301
                },
                {
                    "Status": "Baseline",
                    "Date": "Sep-15",
                    "Parameter": "Admits",
                    "Parameter Value": 248
                },
                {
                    "Status": "Not in program",
                    "Date": "Sep-15",
                    "Parameter": "Admits",
                    "Parameter Value": 212
                },
                {
                    "Status": "Not in program",
                    "Date": "Oct-15",
                    "Parameter": "Admits",
                    "Parameter Value": 300
                },
                {
                    "Status": "Not in program",
                    "Date": "Nov-15",
                    "Parameter": "Admits",
                    "Parameter Value": 189
                },
                {
                    "Status": "Not in program",
                    "Date": "Dec-15",
                    "Parameter": "Admits",
                    "Parameter Value": 312
                },
                {
                    "Status": "Not in program",
                    "Date": "Jan-16",
                    "Parameter": "Admits",
                    "Parameter Value": 210
                },
                {
                    "Status": "Not in program",
                    "Date": "Feb-16",
                    "Parameter": "Admits",
                    "Parameter Value": 301
                },
                {
                    "Status": "Not in program",
                    "Date": "Mar-16",
                    "Parameter": "Admits",
                    "Parameter Value": 248
                },
                {
                    "Status": "Not in program",
                    "Date": "Apr-16",
                    "Parameter": "Admits",
                    "Parameter Value": 244
                },
                {
                    "Status": "Not in program",
                    "Date": "May-16",
                    "Parameter": "Admits",
                    "Parameter Value": 312
                },
                {
                    "Status": "Engaged",
                    "Date": "Sep-15",
                    "Parameter": "Admits",
                    "Parameter Value": 218
                },
                {
                    "Status": "Engaged",
                    "Date": "Oct-15",
                    "Parameter": "Admits",
                    "Parameter Value": 200
                },
                {
                    "Status": "Engaged",
                    "Date": "Nov-15",
                    "Parameter": "Admits",
                    "Parameter Value": 203
                },
                {
                    "Status": "Engaged",
                    "Date": "Dec-15",
                    "Parameter": "Admits",
                    "Parameter Value": 180
                },
                {
                    "Status": "Engaged",
                    "Date": "Jan-16",
                    "Parameter": "Admits",
                    "Parameter Value": 175
                },
                {
                    "Status": "Engaged",
                    "Date": "Feb-16",
                    "Parameter": "Admits",
                    "Parameter Value": 160
                },
                {
                    "Status": "Engaged",
                    "Date": "Mar-16",
                    "Parameter": "Admits",
                    "Parameter Value": 165
                },
                {
                    "Status": "Engaged",
                    "Date": "Apr-16",
                    "Parameter": "Admits",
                    "Parameter Value": 162
                },
                {
                    "Status": "Engaged",
                    "Date": "May-16",
                    "Parameter": "Admits",
                    "Parameter Value": 160
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Sep-15",
                    "Parameter": "Admits",
                    "Parameter Value": 300
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Oct-15",
                    "Parameter": "Admits",
                    "Parameter Value": 344
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Nov-15",
                    "Parameter": "Admits",
                    "Parameter Value": 314
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Dec-15",
                    "Parameter": "Admits",
                    "Parameter Value": 300
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Jan-16",
                    "Parameter": "Admits",
                    "Parameter Value": 323
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Feb-16",
                    "Parameter": "Admits",
                    "Parameter Value": 288
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Mar-16",
                    "Parameter": "Admits",
                    "Parameter Value": 266
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Apr-16",
                    "Parameter": "Admits",
                    "Parameter Value": 301
                },
                {
                    "Status": "Not Engaged",
                    "Date": "May-16",
                    "Parameter": "Admits",
                    "Parameter Value": 344
                }
            ]

            var data1= [
{
    "Status": "Baseline",            "Date": "Jan-15",            "Parameter": "Risk Score",            "Parameter Value": 1.1
},
{
    "Status": "Baseline",            "Date": "Feb-15",            "Parameter": "Risk Score",            "Parameter Value": 1.1
},
{
    "Status": "Baseline",            "Date": "Mar-15",            "Parameter": "Risk Score",            "Parameter Value": 1.12
},
{
    "Status": "Baseline",            "Date": "Apr-15",            "Parameter": "Risk Score",            "Parameter Value": 1.15
},
{
    "Status": "Baseline",            "Date": "May-15",            "Parameter": "Risk Score",            "Parameter Value": 1.15
},
{
    "Status": "Baseline",            "Date": "Jun-15",            "Parameter": "Risk Score",            "Parameter Value": 1.13
},
{
    "Status": "Baseline",            "Date": "Jul-15",            "Parameter": "Risk Score",            "Parameter Value": 1.14
},
{
    "Status": "Baseline",            "Date": "Aug-15",            "Parameter": "Risk Score",            "Parameter Value": 1.11
},
{
    "Status": "Baseline",            "Date": "Sep-15",            "Parameter": "Risk Score",            "Parameter Value": 1.12
},
{
    "Status": "Not in program",            "Date": "Sep-15",            "Parameter": "Risk Score",            "Parameter Value": 1.35
},
{
    "Status": "Not in program",            "Date": "Oct-15",            "Parameter": "Risk Score",            "Parameter Value": 1.25
},
{
    "Status": "Not in program",            "Date": "Nov-15",            "Parameter": "Risk Score",            "Parameter Value": 1.31
},
{
    "Status": "Not in program",            "Date": "Dec-15",            "Parameter": "Risk Score",            "Parameter Value": 1.25
},
{
    "Status": "Not in program",            "Date": "Jan-16",            "Parameter": "Risk Score",            "Parameter Value": 1.27
},
{
    "Status": "Not in program",            "Date": "Feb-16",            "Parameter": "Risk Score",            "Parameter Value": 1.3
},
{
    "Status": "Not in program",            "Date": "Mar-16",            "Parameter": "Risk Score",            "Parameter Value": 1.27
},
{
    "Status": "Not in program",            "Date": "Apr-16",            "Parameter": "Risk Score",            "Parameter Value": 1.32
},
{
    "Status": "Not in program",            "Date": "May-16",            "Parameter": "Risk Score",            "Parameter Value": 1.28
},
{
    "Status": "Engaged",            "Date": "Sep-15",            "Parameter": "Risk Score",            "Parameter Value": 1.07
},
{
    "Status": "Engaged",            "Date": "Oct-15",            "Parameter": "Risk Score",            "Parameter Value": 1.09
},
{
    "Status": "Engaged",            "Date": "Nov-15",            "Parameter": "Risk Score",            "Parameter Value": 1.05
},
{
    "Status": "Engaged",            "Date": "Dec-15",            "Parameter": "Risk Score",            "Parameter Value": 1.14
},
{
    "Status": "Engaged",            "Date": "Jan-16",            "Parameter": "Risk Score",            "Parameter Value": 1.11
},
{
    "Status": "Engaged",            "Date": "Feb-16",            "Parameter": "Risk Score",            "Parameter Value": 1.08
},
{
    "Status": "Engaged",            "Date": "Mar-16",            "Parameter": "Risk Score",            "Parameter Value": 1.13
},
{
    "Status": "Engaged",            "Date": "Apr-16",            "Parameter": "Risk Score",            "Parameter Value": 1.09
},
{
    "Status": "Engaged",            "Date": "May-16",            "Parameter": "Risk Score",            "Parameter Value": 1.14
},
{
    "Status": "Not Engaged",            "Date": "Sep-15",            "Parameter": "Risk Score",            "Parameter Value": 1.19
},
{
    "Status": "Not Engaged",            "Date": "Oct-15",            "Parameter": "Risk Score",            "Parameter Value": 1.2
},
{
    "Status": "Not Engaged",            "Date": "Nov-15",            "Parameter": "Risk Score",            "Parameter Value": 1.18
},
{
    "Status": "Not Engaged",            "Date": "Dec-15",            "Parameter": "Risk Score",            "Parameter Value": 1.17
},
{
    "Status": "Not Engaged",            "Date": "Jan-16",            "Parameter": "Risk Score",            "Parameter Value": 1.18
},
{
    "Status": "Not Engaged",            "Date": "Feb-16",            "Parameter": "Risk Score",            "Parameter Value": 1.2
},
{
    "Status": "Not Engaged",            "Date": "Mar-16",            "Parameter": "Risk Score",            "Parameter Value": 1.2
},
{
    "Status": "Not Engaged",            "Date": "Apr-16",            "Parameter": "Risk Score",            "Parameter Value": 1.2
},
{
    "Status": "Not Engaged",            "Date": "May-16",            "Parameter": "Risk Score",            "Parameter Value": 1.2
}
]

            var data2=[
                {
                    "Status": "Baseline",
                    "Date": "Jan-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 280
                },
                {
                    "Status": "Baseline",
                    "Date": "Feb-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 316.4
                },
                {
                    "Status": "Baseline",
                    "Date": "Mar-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 308
                },
                {
                    "Status": "Baseline",
                    "Date": "Apr-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 310.8
                },
                {
                    "Status": "Baseline",
                    "Date": "May-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 322
                },
                {
                    "Status": "Baseline",
                    "Date": "Jun-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 322
                },
                {
                    "Status": "Baseline",
                    "Date": "Jul-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 322
                },
                {
                    "Status": "Baseline",
                    "Date": "Aug-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 319.2
                },
                {
                    "Status": "Baseline",
                    "Date": "Sep-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 316.4
                },
                {
                    "Status": "Not in program",
                    "Date": "Sep-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 355.6
                },
                {
                    "Status": "Not in program",
                    "Date": "Oct-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 366.8
                },
                {
                    "Status": "Not in program",
                    "Date": "Nov-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 366.8
                },
                {
                    "Status": "Not in program",
                    "Date": "Dec-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 366.8
                },
                {
                    "Status": "Not in program",
                    "Date": "Jan-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 378
                },
                {
                    "Status": "Not in program",
                    "Date": "Feb-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 350
                },
                {
                    "Status": "Not in program",
                    "Date": "Mar-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 366.8
                },
                {
                    "Status": "Not in program",
                    "Date": "Apr-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 366.8
                },
                {
                    "Status": "Not in program",
                    "Date": "May-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 358.4
                },
                {
                    "Status": "Engaged",
                    "Date": "Sep-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 319.2
                },
                {
                    "Status": "Engaged",
                    "Date": "Oct-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 319.2
                },
                {
                    "Status": "Engaged",
                    "Date": "Nov-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 305.2
                },
                {
                    "Status": "Engaged",
                    "Date": "Dec-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 291.2
                },
                {
                    "Status": "Engaged",
                    "Date": "Jan-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 308
                },
                {
                    "Status": "Engaged",
                    "Date": "Feb-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 280
                },
                {
                    "Status": "Engaged",
                    "Date": "Mar-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 319.2
                },
                {
                    "Status": "Engaged",
                    "Date": "Apr-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 313.6
                },
                {
                    "Status": "Engaged",
                    "Date": "May-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 291.2
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Sep-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 333.2
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Oct-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 330.4
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Nov-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 324.8
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Dec-15",
                    "Parameter": "PMPM",
                    "Parameter Value": 327.6
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Jan-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 324.8
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Feb-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 324.8
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Mar-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 330.4
                },
                {
                    "Status": "Not Engaged",
                    "Date": "Apr-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 333.2
                },
                {
                    "Status": "Not Engaged",
                    "Date": "May-16",
                    "Parameter": "PMPM",
                    "Parameter Value": 322
                }
            ]


</g:javascript>


<style>

.tick text{
    font-size:9px !important;
}

.text_y{
    font-size: 9px;;
}

.redfill{
    fill:#C42A22;

}

.greenfill{
    fill:#07B144
}

.bluefill{
    fill: #5E9BCD;
}</style>

</body>
</html>