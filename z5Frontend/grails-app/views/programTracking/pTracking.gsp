<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking</title>
    <r:require module="graph"/>

</head>
<body>

<div class="contentWrapper">
    <g:hiddenField name="showReportingDetails" value="false"/>
    <g:hiddenField name="metricsId" value="${params.metrics}"/>
    <div class="container">
    <div class="col-md-12 mt15">
        <span class="isEditable"><g:link controller="programTracking" action="cohortComparison" class="text-link-upper mb15 double-arrow-left">
            Opportunity Comparison List
        </g:link></span>
    </div>
    <div class="mt25 ">
        <div>
            <h2>Program Tracking</h2>

            <div class="sep-border mb15"></div>

        </div>

        %{--<h1 class="hr-bg text-center "><span class="isEditable color-gray">Program Tracking</span></h1>--}%

        <div class="watermark"><div style="display: inline;text-transform: capitalize">
            &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
        </div>

        <br>
        <div class=" col-md-3" >
            <div class="box-blue  color-blue">
            <div  id="pt1Graph" ></div>
            <div  id="pt1GraphContent"></div>
            </div>
        </div>
        <div class=" col-md-3 ">
            <div class="box-blue  color-blue">
            <div  id="pt2Graph"></div>
            <div  id="pt2GraphContent"></div>
                </div>
        </div>
        <div class=" col-md-3">
            <div class="box-blue  color-blue">
            <div  id="pt3Graph"></div>
            <div  id="pt3GraphContent"></div>
                </div>
        </div>
        <div class="col-md-3" >
            <div class="box-blue  color-blue">

                <div  id="pt4Graph"></div>
            <div  id="pt4GraphContent"></div>
        </div>
        </div>

    </div>


        </div>
    </div>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">


<g:javascript>
    $(function(){

        renderShortFunnelBar(data1,"pt1Graph")
        renderShortFunnelBar(data2,"pt2Graph")
        renderShortFunnelBar(data3,"pt3Graph")
        renderShortFunnelBar(data4,"pt4Graph")
    })




    var data1= [
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Identified",
            "MemberCount": 1000,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "15",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "15",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "15",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Enrolled",
            "MemberCount": 800,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "12",
            "ERUtil": "2",
            "Admission": "2",
            "Biometric": "2",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "12",
            "RxUtilization": "2"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Not Enrolled",
            "MemberCount": 200,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "16",
            "Admission": "16",
            "Biometric": "16",
            "MedAdherence": "6",
            "ClaimsOver10K": "6",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Engaged",
            "MemberCount": 600,
            "Risk": "-20",
            "PMPM": "-8",
            "GapInCareReport": "-20",
            "ERUtil": "-20",
            "Admission": "-2",
            "Biometric": "-20",
            "MedAdherence": "-10",
            "ClaimsOver10K": "-20",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-20"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Not Engaged",
            "MemberCount": 200,
            "Risk": "18",
            "PMPM": "4",
            "GapInCareReport": "18",
            "ERUtil": "8",
            "Admission": "18",
            "Biometric": "18",
            "MedAdherence": "18",
            "ClaimsOver10K": "18",
            "ClaimsUnder10K": "18",
            "RxUtilization": "8"
        }
    ]

    var data2=[
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Identified",
            "MemberCount": 600,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "15",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "15",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "15",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Enrolled",
            "MemberCount": 500,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "12",
            "ERUtil": "2",
            "Admission": "2",
            "Biometric": "2",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "12",
            "RxUtilization": "2"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Not Enrolled",
            "MemberCount": 100,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "16",
            "Admission": "16",
            "Biometric": "16",
            "MedAdherence": "16",
            "ClaimsOver10K": "6",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Engaged",
            "MemberCount": 400,
            "Risk": "-10",
            "PMPM": "-8",
            "GapInCareReport": "-2",
            "ERUtil": "-2",
            "Admission": "-2",
            "Biometric": "-20",
            "MedAdherence": "-5",
            "ClaimsOver10K": "-5",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-5"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Not Engaged",
            "MemberCount": 100,
            "Risk": "81",
            "PMPM": "4",
            "GapInCareReport": "5",
            "ERUtil": "15",
            "Admission": "8",
            "Biometric": "12",
            "MedAdherence": "18",
            "ClaimsOver10K": "1",
            "ClaimsUnder10K": "18",
            "RxUtilization": "8"
        }
    ]

    var data3=[
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Identified",
            "MemberCount": 900,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "51",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "18",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "19",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Enrolled",
            "MemberCount": 700,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "2",
            "ERUtil": "1",
            "Admission": "1",
            "Biometric": "1",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "1",
            "RxUtilization": "12"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Not Enrolled",
            "MemberCount": 200,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "6",
            "Admission": "16",
            "Biometric": "6",
            "MedAdherence": "6",
            "ClaimsOver10K": "26",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Engaged",
            "MemberCount": 500,
            "Risk": "-20",
            "PMPM": "-8",
            "GapInCareReport": "-20",
            "ERUtil": "-20",
            "Admission": "-20",
            "Biometric": "-20",
            "MedAdherence": "-5",
            "ClaimsOver10K": "-5",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-20"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Not Engaged",
            "MemberCount": 90,
            "Risk": "8",
            "PMPM": "4",
            "GapInCareReport": "81",
            "ERUtil": "8",
            "Admission": "28",
            "Biometric": "8",
            "MedAdherence": "8",
            "ClaimsOver10K": "25",
            "ClaimsUnder10K": "8",
            "RxUtilization": "28"
        }
    ]

    var data4=[
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Identified",
            "MemberCount": 1000,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "51",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "18",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "19",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Enrolled",
            "MemberCount": 900,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "2",
            "ERUtil": "1",
            "Admission": "1",
            "Biometric": "1",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "1",
            "RxUtilization": "12"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Not Enrolled",
            "MemberCount": 700,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "6",
            "Admission": "16",
            "Biometric": "6",
            "MedAdherence": "6",
            "ClaimsOver10K": "26",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Engaged",
            "MemberCount": 400,
            "Risk": "-20",
            "PMPM": "-8",
            "GapInCareReport": "-20",
            "ERUtil": "-20",
            "Admission": "-20",
            "Biometric": "-20",
            "MedAdherence": "-5",
            "ClaimsOver10K": "-5",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-20"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Not Engaged",
            "MemberCount": 70,
            "Risk": "8",
            "PMPM": "4",
            "GapInCareReport": "81",
            "ERUtil": "8",
            "Admission": "28",
            "Biometric": "8",
            "MedAdherence": "8",
            "ClaimsOver10K": "25",
            "ClaimsUnder10K": "8",
            "RxUtilization": "28"
        }
    ]

</g:javascript>

<script>


    function renderShortFunnelBar(data,content){
        $("#"+content).html("")
        $("#"+content+"Content").html("")
        var arr=["RxUtilization","ClaimsUnder10K","ClaimsOver10K","MedAdherence","Admission","ERUtil","Biometric","GapInCareReport","PMPM","Risk"]

        var margin = {top: 24, right: 8, bottom: 8, left: 60},
                width = 500 - margin.left - margin.right,
                height = 130 - margin.top - margin.bottom;

        var subHeight=100;
        var subWidth=250;


        var y0 = Math.max(Math.abs(d3.min(data.map(function (d) {
            return d.MemberCount;
        }))), Math.abs(d3.max(data.map(function (d) {
            return d.MemberCount;
        }))));

        //var colors =["#FDE895", "#B3C5E9", "#B3C5E9", "#A8D18D", "#A8D18D", "#0099c6"];
        var colors =["#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5", "#9CA1A5"];


        var y = d3.scale.linear()
                .domain([-y0/2, y0/2])
                .range([height,0])
                .nice();

        var x = d3.scale.ordinal()
                .domain(data.map(function (d) {
                    return d.Status;
                }))
            //.domain(d3.range(data.length))
                .rangeRoundBands([0, width], .45);


        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

        var svg = d3.select("#"+content).append("svg")
                .attr("width", width + margin.left + margin.right-subWidth)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


        var mainBars=svg.selectAll(".bar")
                .data(data)
                .enter().append("rect")
        //.attr("class", function(d) { return -d.MemberCount < 0 ? "bar negative" : "bar positive"; })
        mainBars.attr("class","bar positive")
                .attr("fill",function(d,i){return colors[i];})
                .attr("y", function(d) {
                    //return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
                    return y(Math.max(0, -d.MemberCount));
                })
                .attr("height", function (d) {
                    return 0;
                })
                .attr("x", function(d) { return x(d.Status); })
                .attr("width", x.rangeBand())
                .transition()
                .duration(1000)
                .attr("y", function (d) {
                    return y(Math.max(0, -d.MemberCount))-(Math.abs(y(-d.MemberCount) - y(0))/2);
                }).attr("height", function (d) {
                    return Math.abs(y(-d.MemberCount) - y(0));
                })

        mainBars.append("svg:title").text(function(d,i){
            return d.Status+" : "+Math.abs(d.MemberCount);
        })







        var height2 = 180 - margin.top - margin.bottom;

        var svg2 = d3.select("#"+content+"Content").append("svg")
                .attr("width", width + margin.left + margin.right-subWidth)
                .attr("height", height2 + margin.top + margin.bottom-subHeight)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + 10 + ")");


        arr=arr.reverse()
        var space=0;

        for(i=0;i<arr.length;i++){
            var key=arr[i];
            var container=svg2.append("g");
            var bars=container.selectAll(".rect1")
                    .data(data)
                    .enter()
                    .append("svg:a")
                    .attr("xlink:href", function(d,i){return "programTracking/pt?metrics="+key.toLowerCase();})
                    .append("rect")
            bars.attr("y", function(d) {
                //return (height2-(i*17-10));
                return space;
            })
                    .attr("class",function(d){
                        if(d[key]>10)
                            return "redfill"
                        else if(d[key]>0)
                            return "bluefill"
                        else
                            return "greenfill"

                    })
                    .attr("x", function(d) { return x(d.Status); })
                //.attr("width", x.rangeBand())
                    .attr("height",0)
                    .attr("width", x.rangeBand() )
                    .transition()
                    .delay((i+1)*70)
                    .duration(300)
                    .attr("height",10)

            var keyElement=key;
            if(key=="Risk") key+=" (Prospective)"
            bars.append("svg:title")
                    .text(function(d, i) {return key+" : " + d[keyElement]; })






            if(key=="Risk") key+=" (Prospective)"



            container.append("text")
                    .attr("class", "text_y")
                    .attr("text-anchor", "end")  // this makes it easy to centre the text as the transform is applied to the anchor
                    .attr("transform", "translate(" + (20) + "," + ((space+8)) + ")")  // text is drawn off the screen top left, move down and out and rotate
                    .text("")
                    .transition()
                    .duration(300)
                    .delay((i+1)*90)
                    .text(""+key+"");

            space=space+18;

            /*container.append("text")
             .attr("class", "text_y")
             .attr("text-anchor", "end")  // this makes it easy to centre the text as the transform is applied to the anchor
             .attr("transform", "translate(" + (15) + "," + ((height2-(i*17)-2)) + ")")  // text is drawn off the screen top left, move down and out and rotate
             .transition()
             .duration(100)
             .delay(i*100)
             .text(""+key+"");*/

        }



        /* svg.append("g")
         .attr("class", "y axis")
         .call(yAxis);*/

        var xAxis = d3.svg.axis()
                .scale(x)
                .tickSize(0)
                .orient("top");

        var  ax2=svg.append("g")
                .attr("class", "x axis funnel")
                .attr("transform", "translate(0,-10)")
                .call(xAxis)

        var mTicks = data.map(function (d) {
            return d.Status
        });

        var main_xAxis2 = d3.svg.axis()
                .scale(x)
                .tickSize(0)
                .tickValues(mTicks)
                .tickFormat(formatTicks)
                .orient("bottom");

        var ax1=svg.append("g")
                .attr("class", "x axis funnel")
                .attr("transform", "translate(0," + (y(0)) + ")")
                .call(main_xAxis2)
                .selectAll(".tick text")
                .style("pointer-events", "none")
                .call(wrap, x.rangeBand());

        function formatTicks(d) {
            var arr=$.grep(data, function(e){ return e.Status === d; })
            return arr[0].MemberCount
        }

        $(("#"+content)).prepend("<div style='text-align: center;font-size: 16px;margin-bottom: 10px;'><a href='javascript:void(0)'>"+data[0].ProgramName+"</a></div>")
        $(("#"+content)).append("<div style='text-align: right;font-size: 12px;margin-right:15px;text-decoration: underline;'><em><a href='programTracking/pTrackingDetails/"+data[0].ProgramName+"' >+More</a></em></div>")
        $(("#"+content+"Content")).append("<div style='text-align: left;font-size: 12px;text-decoration: underline;'><em><a href='programTracking/pTrackingDetails/"+data[0].ProgramName+"'>+More</a></em></div>")

    }
</script>


<style>
.tick text{
    font-size:9px !important;
}
.text_y{
    font-size: 9px;;
}

.redfill{
    fill:#C42A22;

}

.greenfill{
    /*fill:#07B144*/
    fill:#9CA1A5;
}

.bluefill{
    /*fill: #FF9B2B;*/
    fill: #9CA1A5;
}</style>

</body>
</html>