<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking Details</title>
    <r:require module="graph"/>

</head>
<body>

<div class="contentWrapper">
    <g:hiddenField name="showReportingDetails" value="false"/>
    <g:hiddenField name="metricsId" value="${params.metrics}"/>
    <div class="container">
    <div class="col-md-12 mt15">
        <span class="isEditable"><g:link controller="programTracking" action="index" class="text-link-upper mb15 double-arrow-left">
            Program Tracking
        </g:link></span>
    </div>
        <div class="mt25 ">
            <div>
                <h2>Program Tracking</h2>

                <div class="sep-border mb15"></div>

            </div>

            %{--<h1 class="hr-bg text-center "><span class="isEditable color-gray">Program Tracking</span></h1>--}%

            <div class="watermark"><div style="display: inline;text-transform: capitalize">
                &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
            </div>

            <br>
            <div class=" col-md-12">
                <div class="clearfix"></div>
                <div  id="ptDetails" ></div>
                <div  id="ptDetailsContent"></div>
            </div>


        </div>


    </div>
</div>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">


<g:javascript>

    var id="${params.id}";
    $(function(){

    switch(id){
    case("Telemedicine"):
            renderFunnelBar(data1,"ptDetails")
            break;

            case("Coaching"):
            renderFunnelBar(data2,"ptDetails")
            break;

            case("Diabetes Management"):
            renderFunnelBar(data3,"ptDetails")
            break;

            case("CHF"):
            renderFunnelBar(data4,"ptDetails")
            break;

            default:
            break;

    }


        //renderFunnelBar(data1,"ptDetails")
        //renderFunnelBar(data2,"pt2Graph")
        //renderFunnelBar(data3,"pt3Graph")
        //renderFunnelBar(data4,"pt4Graph")
    })




    var data1= [
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Identified",
            "MemberCount": 1000,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "15",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "15",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "15",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Enrolled",
            "MemberCount": 800,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "12",
            "ERUtil": "2",
            "Admission": "2",
            "Biometric": "2",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "12",
            "RxUtilization": "2"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Not Enrolled",
            "MemberCount": 200,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "16",
            "Admission": "16",
            "Biometric": "16",
            "MedAdherence": "6",
            "ClaimsOver10K": "6",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Engaged",
            "MemberCount": 600,
            "Risk": "-20",
            "PMPM": "-8",
            "GapInCareReport": "-20",
            "ERUtil": "-20",
            "Admission": "-2",
            "Biometric": "-20",
            "MedAdherence": "-10",
            "ClaimsOver10K": "-20",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-20"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P001",
            "ProgramName": "Telemedicine",
            "Status": "Not Engaged",
            "MemberCount": 200,
            "Risk": "18",
            "PMPM": "4",
            "GapInCareReport": "18",
            "ERUtil": "8",
            "Admission": "18",
            "Biometric": "18",
            "MedAdherence": "18",
            "ClaimsOver10K": "18",
            "ClaimsUnder10K": "18",
            "RxUtilization": "8"
        }
    ]

    var data2=[
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Identified",
            "MemberCount": 600,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "15",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "15",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "15",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Enrolled",
            "MemberCount": 500,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "12",
            "ERUtil": "2",
            "Admission": "2",
            "Biometric": "2",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "12",
            "RxUtilization": "2"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Not Enrolled",
            "MemberCount": 100,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "16",
            "Admission": "16",
            "Biometric": "16",
            "MedAdherence": "16",
            "ClaimsOver10K": "6",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Engaged",
            "MemberCount": 400,
            "Risk": "-10",
            "PMPM": "-8",
            "GapInCareReport": "-2",
            "ERUtil": "-2",
            "Admission": "-2",
            "Biometric": "-20",
            "MedAdherence": "-5",
            "ClaimsOver10K": "-5",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-5"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Coaching",
            "Status": "Not Engaged",
            "MemberCount": 100,
            "Risk": "81",
            "PMPM": "4",
            "GapInCareReport": "5",
            "ERUtil": "15",
            "Admission": "8",
            "Biometric": "12",
            "MedAdherence": "18",
            "ClaimsOver10K": "1",
            "ClaimsUnder10K": "18",
            "RxUtilization": "8"
        }
    ]

    var data3=[
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Identified",
            "MemberCount": 900,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "51",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "18",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "19",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Enrolled",
            "MemberCount": 700,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "2",
            "ERUtil": "1",
            "Admission": "1",
            "Biometric": "1",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "1",
            "RxUtilization": "12"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Not Enrolled",
            "MemberCount": 200,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "6",
            "Admission": "16",
            "Biometric": "6",
            "MedAdherence": "6",
            "ClaimsOver10K": "26",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Engaged",
            "MemberCount": 500,
            "Risk": "-20",
            "PMPM": "-8",
            "GapInCareReport": "-20",
            "ERUtil": "-20",
            "Admission": "-20",
            "Biometric": "-20",
            "MedAdherence": "-5",
            "ClaimsOver10K": "-5",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-20"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "Diabetes Management",
            "Status": "Not Engaged",
            "MemberCount": 90,
            "Risk": "8",
            "PMPM": "4",
            "GapInCareReport": "81",
            "ERUtil": "8",
            "Admission": "28",
            "Biometric": "8",
            "MedAdherence": "8",
            "ClaimsOver10K": "25",
            "ClaimsUnder10K": "8",
            "RxUtilization": "28"
        }
    ]

    var data4=[
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Identified",
            "MemberCount": 1000,
            "Risk": "15",
            "PMPM": "18",
            "GapInCareReport": "15",
            "ERUtil": "51",
            "Admission": "15",
            "Biometric": "15",
            "MedAdherence": "18",
            "ClaimsOver10K": "15",
            "ClaimsUnder10K": "19",
            "RxUtilization": "15"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Enrolled",
            "MemberCount": 900,
            "Risk": "12",
            "PMPM": "10",
            "GapInCareReport": "2",
            "ERUtil": "1",
            "Admission": "1",
            "Biometric": "1",
            "MedAdherence": "12",
            "ClaimsOver10K": "12",
            "ClaimsUnder10K": "1",
            "RxUtilization": "12"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Not Enrolled",
            "MemberCount": 700,
            "Risk": "16",
            "PMPM": "8",
            "GapInCareReport": "16",
            "ERUtil": "6",
            "Admission": "16",
            "Biometric": "6",
            "MedAdherence": "6",
            "ClaimsOver10K": "26",
            "ClaimsUnder10K": "6",
            "RxUtilization": "16"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Engaged",
            "MemberCount": 400,
            "Risk": "-20",
            "PMPM": "-8",
            "GapInCareReport": "-20",
            "ERUtil": "-20",
            "Admission": "-20",
            "Biometric": "-20",
            "MedAdherence": "-5",
            "ClaimsOver10K": "-5",
            "ClaimsUnder10K": "-20",
            "RxUtilization": "-20"
        },
        {
            "GroupId": "C001",
            "ClientID": "G001",
            "ProgramID": "P002",
            "ProgramName": "CHF",
            "Status": "Not Engaged",
            "MemberCount": 70,
            "Risk": "8",
            "PMPM": "4",
            "GapInCareReport": "81",
            "ERUtil": "8",
            "Admission": "28",
            "Biometric": "8",
            "MedAdherence": "8",
            "ClaimsOver10K": "25",
            "ClaimsUnder10K": "8",
            "RxUtilization": "28"
        }
    ]

</g:javascript>



<style>
.tick text{
    font-size:12px !important;
}
.text_y{
    font-size: 12px;;
}

.redfill{
    fill:#C42A22;

}

.greenfill{
    /*fill:#07B144*/
    fill:#9CA1A5;
}

.bluefill{
    /*fill: #FF9B2B;*/
    fill: #9CA1A5;
}</style>

</body>
</html>