<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking</title>
    <style type="text/css">
        table.explorer-table{border-collapse: collapse;}
        table.explorer-table th{font-weight: normal}
        table.explorer-table th{border-collapse: collapse; color:#666; padding: 5px;}
        table.explorer-table th{text-align: center;}
        table.explorer-table td {border-collapse: collapse; border: 1px solid #ccc; padding: 5px;}
        table.explorer-table tbody td:nth-child(2){text-align: center;}
        table.explorer-table tbody td:nth-child(3){text-align: center;}
        table.explorer-table tr:nth-child(even) {background:#e7eceb}
        table.explorer-table tr:nth-child(odd) {background: #e5f0fb }
    </style>

</head>
<body>
<div class="contentWrapper">
    <g:hiddenField name="showReportingDetails" value="false"/>
    <div class="container">


    <div class="col-md-12">
        <h2 class="mb15">
            <g:img uri="/images/zExplorer.png" class="heading-icon" style="width:53px;"/>
            <span class="isEditable">zProgram - Opportunity Explorer</span></h2>
        <div class="sep-border mb15"></div>

    </div>

    <div class="">
        <div class="col-md-6">
            <div class="col-md-12"><g:checkBox name="1"/>Include members already in program</div>

            <div class="clearfix"></div>
            <div class="col-md-1"></div>
            <div class="col-md-10 "><g:checkBox name="1"/>Include engaged members</div>

            <div class="clearfix"></div>
            <div class="col-md-1"></div>
            <div class="col-md-10"><g:checkBox name="1"/>Include not engaged members</div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12"><g:checkBox name="1"/>Only include members identified in last 3 (<u>change</u>) months</div>
            <div class="col-md-12"><g:checkBox name="1"/>Exclude members with risk score &gt;5 (<u>change</u>)</div>
            <div class="col-md-12"><g:checkBox name="1"/>Exclude members with more than 3 (<u>change</u>) co-morbidities</div>
        </div>
        <div class="clearfix mb10"></div>

    </div>

    <div class="col-md-4">Sort By <g:select name="sel" class="clientSelect"  from="['Savings']"/></div>
    <div class="col-md-8" >
        <div style="float: right;">
            Filter By
            <button class="btn">Age > 41</button>
            <button class="btn">Gender</button>
            <button class="btn">Location</button>
            <button class="btn">Relationship</button>
            <button class="btn">Division</button>
            </div>
    </div>
    <div class="clearfix"></div>
    <style type="text/css">
        .opp-explorer-wrapper{margin-top:20px;}
        .opp-block{background:#e5f0fb; margin-bottom: 30px; padding: 15px; border:1px solid rgba(4,79,150,0.1);}
        .opp-block-title{font-size: 14px; font-weight: bold; margin-bottom: 25px; color: #044f96; border-bottom: 1px solid rgba(4,79,150,0.2); padding-bottom: 15px; min-height: 50px;}
        .opp-block-title a.icon-provider{float: right;}

        .opp-block-content{}
        .opp-block-content .container{padding:0 !important;}

        .opp-block-content .data-row{ margin-bottom: 15px;}
        .opp-block-content .data-actions{border-left: 1px solid rgba(4,79,150,0.2); padding-left:25px;}

        .opp-block-content .data-row ul{margin: 0; padding: 0; list-style: none;}
        .opp-block-content .data-row ul li{position: relative; padding-left: 15px;}
        .opp-block-content .data-row ul li:before{content:'\f054'; font-family: fontAwesome; font-size: 10px; position: absolute; left: 0; top:0;}
        .opp-block-content .data-row span.data-label{font-weight: bold;display: inline-block; padding-right: 5px;color: #044f96; display: inline-block; width: 115px;}

        .no-pad-l{padding-left: 0 !important;;}
        .no-pad-r{padding-right: 0 !important;;}


        .slider-selection {
            background: #0480be;
        }
        .slider-track-high{
            background: #BABABA;
        }

    </style>


    <div class="opp-explorer-wrapper">
       <div class="col-md-12 ">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/icon-overuse-er.png" class="heading-icon"/> Overuse of ER<a href="#" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                        <div class="col-md-4 no-pad-l">
                            <div class="data-row"><span class="data-label">Savings</span>  $2975</div>
                            <div class="data-row">
                                <span class="data-label">Member Count</span>
                                <a href='#' >150</a>
                            </div>
                            <div class="data-row">
                                <span class="data-label">Potential Impact</span>
                                <a href='#' id="tile1" class="btn btn-primary" >$90</a><i>  (click to save)</i>

                            </div>

                        </div>
                    <div class="col-md-4 no-pad-l">

                        <div>
                            Savings:&nbsp;&nbsp;&nbsp;&nbsp; <input id="subTile-1-1" class="proBar" data-slider-id='ex2Slider' type="text" data-slider-min="0" data-slider-max="${Math.round(90000/150)}" data-slider-step="1" data-slider-value="${Math.round(90000/150)}"/>
                        </div>
                        <br>
                        <div>
                            Member: &nbsp;&nbsp;&nbsp;&nbsp;<input id="subTile-1-2" class="proBar" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="150" data-slider-step="1" data-slider-value="150"/>
                        </div>
                    </div>

                        <div class="col-md-4  no-pad-r">
                           <div class="data-row data-actions"><span class="data-label">Actions</span>
                               <ul><li>Implement telemedicine program.</li><li>Modify plan design
                               </li><li>Improve access to Urgent Care</li></ul>
                           </div>

                        </div>
            <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-12 ">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/zExplorer.png" class="heading-icon"/> Missing Age/Gender Appropriate Preventative Screenings<a href="#" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-4 no-pad-l">
                        <div class="data-row"><span class="data-label">Savings</span>  $6745</div>


                    </div>
                    <div class="col-md-4 no-pad-l">

                        <div class="data-row">
                            <span class="data-label">Member Count</span>
                            <a href='#' >183</a>
                        </div>
                        <div class="data-row">
                            <span class="data-label">Potential Impact</span>
                            <a href='#' id="tile2" title="Click To Save" class="btn btn-primary" >$78</a><i>  (click to save)</i>
                        </div>
                        <div onclick="show('tileGrp2',this)"><i class="fa fa-chevron-down"></i></div>
                        <div style="visibility:hidden;" id="tileGrp2">
                        <div>
                            Savings:&nbsp;&nbsp;&nbsp;&nbsp; <input id="subTile-2-1" class="proBar" data-slider-id='ex2Slider' type="text" data-slider-min="0" data-slider-max="${Math.round(78000/183)}" data-slider-step="1" data-slider-value="${Math.round(78000/183)}"/>
                        </div>
                        <br>
                        <div>
                            Member: &nbsp;&nbsp;&nbsp;&nbsp;<input id="subTile-2-2" class="proBar" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="183" data-slider-step="1" data-slider-value="183"/>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-4  no-pad-r">
                        <div class="data-row data-actions"><span class="data-label">Actions</span>
                            <ul><li>Impose penalty in premium,</li><li>Letter campaign</li><li>Email campaign </li><li>Incentive design</li></ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>



    </div>


    %{--<table class="explorer-table" cellpadding="0" cellspacing="0" border="0" width="100%">
        <thead>
            <tr style="background: #fff">
                <th>Problem</th>
                <th>Member Count</th>
                <th>Potential Impact</th>
                <th>Actions</th>
                <th>Providers</th>
            </tr>
        </thead>
        <tbody>






        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon"/></div><div style="float: left">High Branded Drug Usage<h4>$889,000</h4></div>
            </td>
            <td><a href='#' >89</a></td>
            <td><a href='#' >$123K</a></td>
            <td><ul><li>Start Rx carve-out program</li><li>Drug tier change</li><li>Plan design change</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">Emerging High Risk Members<h4>$2,356,000</h4></div>
            </td>
            <td><a href='#' >367</a></td>
            <td><a href='#' >$67K</a></td>
            <td><ul><li>Wellness coaching</li><li>Plan design change to improve biometric monitoring</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                High Gaps-in-Care: COPD<h4>$75,000</h4></div>
            </td>
            <td><a href='#' >127</a></td>
            <td><a href='#' >$91K</a></td>
            <td><ul><li>COPD care program</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                Members not participating in 'Walking program'<h4></h4></div>

            </td>
            <td><a href='#' >200</a></td>
            <td><a href='#' >$45K</a></td>
            <td><ul><li>Start new Wellness Program</li><li>Modifiy incentives</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                Saving from Pharmacy Benefit Carve-out not leveraged<h4></h4></div>
            </td>
            <td><a href='#' >200</a></td>
            <td><a href='#' >$200K</a></td>
            <td><ul><li>Start new Rx Carve-out solution</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                Members Suitable for Risk Appropriate Coaching<h4>$865,000</h4></div>
            </td>
            <td><a href='#' >All Members</a></td>
            <td><a href='#' >$34K</a></td>
            <td><ul><li>Restart outreach campaigns and enroll</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>




        </tbody>

    </table>--}%

</div>
    </div>

<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-silder.css')}" type="text/css">
<script>


    function show(target, trigger){
//        document.getElementById(target).style.display = 'block';
        $("#"+target).slideDown()
        $("#"+target).css("visibility","visible");
        trigger.style.display = "none"
    }


    $(function(){




        $.each($(".proBar"),function(){
            var $this=$(this);
            var id=this.id;
            var max=$this.attr("data-slider-max")
            new Slider("#"+id, {
                formatter: function(value) {
                    return value;
                }
                ,ticks: [0,parseInt(max)]
                ,ticks_labels: ['0', ''+max]
                ,ticks_snap_bounds: 1
//                ,natural_arrow_keys: true
            });

            $("#"+id).on("slide", function(slideEvt) {
//                var percent=slideEvt.value;
                var values=id.split("-")[1]
                var sav=$("#subTile-"+values+"-1").val()
                var mem=$("#subTile-"+values+"-2").val()
                $("#tile"+values).text("$"+(Math.round((mem*sav)/1000)));
            });

            $("#"+id).on("change", function(slideEvt) {
//                var percent=slideEvt.value.newValue;
                var values=id.split("-")[1]
                var sav=$("#subTile-"+values+"-1").val()
                var mem=$("#subTile-"+values+"-2").val()
                $("#tile"+values).text("$"+(Math.round((mem*sav)/1000)));
            });

        })



        $(".slider-tick").remove();
        $("#tileGrp2").hide();

    });




</script>
</body>
</html>