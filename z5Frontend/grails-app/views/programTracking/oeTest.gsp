<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking</title>

</head>

<body>


<div class="contentWrapper">
    <g:hiddenField name="showReportingDetails" value="false"/>
    <div class="container">
    <div class="col-md-12 mt15">
        <g:link controller="programTracking" action="explorer" params="[showInIdentity:'1']" class="text-link-upper mb15 double-arrow-left">
            Opportunity List
        </g:link>
    </div>




    <div class="col-md-12">
        <h2 class="mb15">
            <g:img uri="/images/zExplorer.png" class="heading-icon" style="width:70px;"/>
            <span class="isEditable">zProgram - Opportunity Explorer</span></h2>
        <div class="watermark mb15"><div style="display: inline;text-transform: capitalize">
            &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Program data is not available. This is showing dummy data  &nbsp;&nbsp;</div>
        </div>
        <div class="sep-border mb15"></div>

    </div>


    <div>
        <div class="col-md-6">
            <div class="col-md-12"><g:checkBox name="1"/>Include members already in program</div>

            <div class="clearfix"></div>
            <div class="col-md-1"></div>
            <div class="col-md-10 "><g:checkBox name="1"/>Include engaged members</div>

            <div class="clearfix"></div>
            <div class="col-md-1"></div>
            <div class="col-md-10"><g:checkBox name="1"/>Include not engaged members</div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12"><g:checkBox name="1"/>Only include members identified in last 3 (<u>change</u>) months</div>
            <div class="col-md-12"><g:checkBox name="1"/>Exclude members with risk score &gt;5 (<u>change</u>)</div>
            <div class="col-md-12"><g:checkBox name="1"/>Exclude members with more than 3 (<u>change</u>) co-morbidities</div>
        </div>
        <div class="clearfix mb10"></div>

    </div>

    <div class="col-md-4">Sort By <g:select name="sel" class="clientSelect"  from="['Problem Size','Problems','Potential Savings','Member Count']"/></div>
    <div class="col-md-8" >
        <div style="float: right;">
            Filter By
            <button class="btn">Age > 41</button>
            <button class="btn">Gender</button>
            <button class="btn">Location</button>
            <button class="btn">Relationship</button>
            <button class="btn">Division</button>
            </div>
    </div>
    <div class="clearfix"></div>
    <style type="text/css">
        .opp-explorer-wrapper{margin-top:20px;}
        .opp-block{background:#e5f0fb; margin-bottom: 30px; padding: 15px; border:1px solid rgba(4,79,150,0.1);}
        .opp-block-title{font-size: 14px; font-weight: bold; margin-bottom: 25px; color: #044f96; border-bottom: 1px solid rgba(4,79,150,0.2); padding-bottom: 15px; min-height: 50px;}
        .opp-block-title a.icon-provider{float: right;}

        .opp-block-content{}
        .opp-block-content .container{padding:0 !important;}

        .opp-block-content .data-row{ margin-bottom: 15px;}
        .opp-block-content .data-actions{border-left: 1px solid rgba(4,79,150,0.2); padding-left:25px; min-height: 140px; }

        .opp-block-content .data-row ul{margin: 0; padding: 0; list-style: none;}
        .opp-block-content .data-row ul li{position: relative; padding-left: 15px;}
        .opp-block-content .data-row ul li:before{content:'\f054'; font-family: fontAwesome; font-size: 10px; position: absolute; left: 0; top:0;}
        .opp-block-content .data-row span.data-label{font-weight: bold;display: inline-block; padding-right: 5px;color: #044f96; display: inline-block; width: 160px;}

        .no-pad-l{padding-left: 0 !important;;}
        .no-pad-r{padding-right: 0 !important;;}

        .sub-head-prob{font-size: 18px;}
        .problem{font-size: 18px;color: #E95350 !important;font-weight: bold;}
        .poten{font-size: 18px;color: #80C46F !important;font-weight: bold;}





    </style>




    <div class="opp-explorer-wrapper">
       <div class="col-md-6 no-pad-l">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/icon-overuse-er.png" class="heading-icon"/> Overuse of ER<a href="#" data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                        <div class="col-md-5 no-pad-l">
                            <g:hiddenField name="tile-1-problem" value="45000"/>
                            <g:hiddenField name="tile-1-member" value="183"/>
                            <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a   class="problem" href='#' >$65K</a></div>
                            <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >183</a></div>
                            <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="1"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                        </div>

                        <div class="col-md-7  no-pad-r">
                            <div class="data-row data-actions">
                                <span class="data-row data-label sub-head-prob" sty>Potential Savings</span> <a href='#' id="tile1" class="poten" >$45k</a>
                                <span class="data-label">Actions</span>
                               <ul><li>Implement telemedicine program.</li><li>Modify plan design
                               </li><li>Improve access to Urgent Care</li></ul>
                           </div>
                        </div>
            <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-6 no-pad-r">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/zExplorer.png" class="heading-icon"/> Missing Age/Gender Appropriate Preventative Screenings<a href="#" data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-5 no-pad-l">
                        <g:hiddenField name="tile-2-problem" value="30000"/>
                        <g:hiddenField name="tile-2-member" value="234"/>
                        <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a   class="problem" href='#' >$90K</a></div>
                        <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >234</a></div>
                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="2"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                    </div>

                    <div class="col-md-7  no-pad-r">
                        <div class="data-row data-actions">
                            <div class="data-row data-label sub-head-prob"><span class="data-label">Potential Impact</span> <a href='#' id="tile2" class="poten" >$30k</a></div>

                            <span class="data-label">Actions</span>
                            <ul><li>Impose penalty in premium,</li><li>Letter campaign</li><li>Email campaign </li><li>Incentive design</li></ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6 no-pad-l">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/zExplorer.png" class="heading-icon"/> Unmanaged Diabetes<a href="#"  data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-5 no-pad-l">
                        <g:hiddenField name="tile-3-problem" value="20000"/>
                        <g:hiddenField name="tile-3-member" value="134"/>
                        <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a  class="problem" href='#' >$80K</a></div>
                        <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >134</a></div>
                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="3"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                    </div>

                    <div class="col-md-7  no-pad-r">
                        <div class="data-row data-actions">
                            <div class="data-row data-label sub-head-prob"><span class="data-label">Potential Impact</span> <a id="tile3"  href='#' class="poten" >$20k</a></div>

                            <span class="data-label">Actions</span>
                            <ul><li>Diabetes management program</li><li>Wellness program specific to diabetes management</li></ul>
                        </div>
                    </div>



                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-6 no-pad-r">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/zExplorer.png" class="heading-icon"/>Pre-Diabetes Education<a href="#" data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-5 no-pad-l">
                        <g:hiddenField name="tile-4-problem" value="10000"/>
                        <g:hiddenField name="tile-4-member" value="174"/>
                        <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a  class="problem" href='#' >$55K</a></div>
                        <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >174</a></div>
                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="4"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                    </div>

                    <div class="col-md-7  no-pad-r">
                        <div class="data-row data-actions">
                            <div class="data-row data-label sub-head-prob"><span class="data-label">Potential Impact</span> <a  id="tile4" href='#' class="poten" >$18k</a></div>

                            <span class="data-label">Actions</span>
                            <ul><li>Start pre-diabetes education program</li><li>Add diabetes education to existing wellness program</li></ul>
                        </div>
                    </div>


                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-6 no-pad-l">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon"/> High Branded Drug Usage<a href="#" data-toggle="modal" data-target="#providersBox"  title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-5 no-pad-l">
                        <g:hiddenField name="tile-5-problem" value="45000"/>
                        <g:hiddenField name="tile-5-member" value="183"/>
                        <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a   class="problem" href='#' >$65K</a></div>
                        <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >183</a></div>
                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="5"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                    </div>

                    <div class="col-md-7  no-pad-r">
                        <div class="data-row data-actions">
                            <span class="data-row data-label sub-head-prob" sty>Potential Savings</span> <a id="tile5" href='#' class="poten" >$45k</a>
                            <span class="data-label">Actions</span>
                            <ul><li>Start Rx carve-out program</li><li>Drug tier change</li><li>Plan design change</li></ul>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-6 no-pad-r">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/zExplorer.png" class="heading-icon"/>Emerging High Risk Member<a href="#" data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-5 no-pad-l">
                        <g:hiddenField name="tile-6-problem" value="15000"/>
                        <g:hiddenField name="tile-6-member" value="83"/>
                        <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a   class="problem" href='#' >$35K</a></div>
                        <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >83</a></div>
                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="6"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                    </div>

                    <div class="col-md-7  no-pad-r">
                        <div class="data-row data-actions">
                            <span class="data-row data-label sub-head-prob" sty>Potential Savings</span> <a id="tile6" href='#' class="poten" >$15k</a>
                            <span class="data-label">Actions</span>
                            <ul><li>Wellness coaching</li><li>Plan design change to improve biometric monitoring</li></ul>

                        </div>
                    </div>


                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-6 no-pad-l">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/zExplorer.png" class="heading-icon"/>Low mail order prescription usage<a href="#" data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-5 no-pad-l">
                        <g:hiddenField name="tile-7-problem" value="45000"/>
                        <g:hiddenField name="tile-7-member" value="83"/>
                        <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a  class="problem" href='#' >$55K</a></div>
                        <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >83</a></div>
                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="7"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                    </div>

                    <div class="col-md-7  no-pad-r">
                        <div class="data-row data-actions">
                            <span class="data-row data-label sub-head-prob" sty>Potential Savings</span> <a id="tile7"  href='#' class="poten" >$45k</a>

                            <span class="data-label">Actions</span>
                            <ul><li>Start mail order programs</li>
                                <li>Incentive for mail order usage</li>
                                <li>Streamlining refill process</li>
                                <li>Plan design change</li>
                            </ul>

                        </div>
                    </div>


                    <div class="clearfix"></div>

                </div>
            </div>
        </div>

        <div class="col-md-6 no-pad-r">
            <div class="opp-block">
                <div class="opp-block-title"><g:img uri="/images/zExplorer.png" class="heading-icon"/>High out-of-network usage<a href="#" data-toggle="modal" data-target="#providersBox" title="Providers" class="icon-provider iconExplorePopulation"><g:img uri="/images/providers.png" alt="Tilde" /></a>
                    <div class="clearfix"></div>
                </div>
                <div class="opp-block-content">

                    <div class="col-md-5 no-pad-l">
                        <g:hiddenField name="tile-8-problem" value="45000"/>
                        <g:hiddenField name="tile-8-member" value="56"/>
                        <div class="data-row"><span class="data-label sub-head-prob">Problem Size</span> <a  class="problem" href='#' >$95K</a></div>
                        <div class="data-row"><span class="data-label">Member Count</span> <a href='#' >56</a></div>
                        <div class="data-row"><span class="data-label">Savings Calculator </span> <a href='#' rel="shareit" id="8"><i class="fa fa-sliders " aria-hidden="true"></i></a></div>
                    </div>

                    <div class="col-md-7  no-pad-r">
                        <div class="data-row data-actions">
                            <span class="data-row data-label sub-head-prob" sty>Potential Savings</span> <a id="tile8"  href='#' class="poten" >$45k</a>
                            <span class="data-label">Actions</span>
                            <ul><li>Plan design change</li>
                                <li>Include more providers into the network</li>
                                <li> Incentives to shift</li>

                            </ul>

                        </div>
                    </div>


                    <div class="clearfix"></div>

                </div>
            </div>
        </div>



    </div>


    %{--<table class="explorer-table" cellpadding="0" cellspacing="0" border="0" width="100%">
        <thead>
            <tr style="background: #fff">
                <th>Problem</th>
                <th>Member Count</th>
                <th>Potential Impact</th>
                <th>Actions</th>
                <th>Providers</th>
            </tr>
        </thead>
        <tbody>






        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon"/></div><div style="float: left">High Branded Drug Usage<h4>$889,000</h4></div>
            </td>
            <td><a href='#' >89</a></td>
            <td><a href='#' >$123K</a></td>
            <td><ul><li>Start Rx carve-out program</li><li>Drug tier change</li><li>Plan design change</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">Emerging High Risk Members<h4>$2,356,000</h4></div>
            </td>
            <td><a href='#' >367</a></td>
            <td><a href='#' >$67K</a></td>
            <td><ul><li>Wellness coaching</li><li>Plan design change to improve biometric monitoring</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                High Gaps-in-Care: COPD<h4>$75,000</h4></div>
            </td>
            <td><a href='#' >127</a></td>
            <td><a href='#' >$91K</a></td>
            <td><ul><li>COPD care program</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                Members not participating in 'Walking program'<h4></h4></div>

            </td>
            <td><a href='#' >200</a></td>
            <td><a href='#' >$45K</a></td>
            <td><ul><li>Start new Wellness Program</li><li>Modifiy incentives</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                Saving from Pharmacy Benefit Carve-out not leveraged<h4></h4></div>
            </td>
            <td><a href='#' >200</a></td>
            <td><a href='#' >$200K</a></td>
            <td><ul><li>Start new Rx Carve-out solution</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>

        <tr>
            <td>
                <div style="float: left"><g:img uri="/images/zExplorer.png" class="heading-icon"/></div><div style="float: left">
                Members Suitable for Risk Appropriate Coaching<h4>$865,000</h4></div>
            </td>
            <td><a href='#' >All Members</a></td>
            <td><a href='#' >$34K</a></td>
            <td><ul><li>Restart outreach campaigns and enroll</li></ul></td>
            <td><div class="iconExplorePopulation" style="text-align: center"><g:img uri="/images/providers.png" alt="Tilde" /></div></td>

        </tr>




        </tbody>

    </table>--}%

    <div id="providersBox"  class="modal fade bd-modal-lg" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                %{--<div class="modal-header">
                    <h3 class="modal-title">Providers</h3>
                </div>--}%
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 1px;"><span aria-hidden="true">&times;</span></button>

                    <p><g:img uri="/images/providersList.png" style="width:680px;" /></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>

<div id="shareit-box">
    <div id="shareit-body">
        <div id="shareit-blank">
            <button type="button" id="closeButton" class="close" style="top: 22px;margin-right: 29px;color: black;" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div id="shareit-icon">

        </div>
    </div>
</div>



%{--<div id="providersBox" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">...
                --}%%{----}%%{--
                --}%%{--<g:img uri="/images/providersList.png" style="width:400px;" />--}%%{--
        </div>
    </div>
</div>--}%


<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-silder.css')}" type="text/css">

<script>

   /* $.each($(".proBar"),function(){
        var $this=$(this);
        var id=this.id;
        var max=$this.attr("data-slider-max")
        new Slider("#"+id, {
            formatter: function(value) {
                return value;
            }
            ,ticks: [0,parseInt(max)]
            ,ticks_labels: ['0', ''+max]
            ,ticks_snap_bounds: 1
        });

        $("#"+id).on("slide", function(slideEvt) {
//                var percent=slideEvt.value;
            var values=id.split("-")[1]
            var sav=$("#subTile-"+values+"-1").val()
            var mem=$("#subTile-"+values+"-2").val()
            $("#tile"+values).text("$"+(Math.round((mem*sav)/1000)));
        });

        $("#"+id).on("change", function(slideEvt) {
//                var percent=slideEvt.value.newValue;
            var values=id.split("-")[1]
            var sav=$("#subTile-"+values+"-1").val()
            var mem=$("#subTile-"+values+"-2").val()
            $("#tile"+values).text("$"+(Math.round((mem*sav)/1000)));
        });

    })*/










    $(document).ready(function() {
//        $('.tooltip-main').tooltip({html: true})

        var globalId = "";
        var tileSaving;
        document.cookie =  'prob-cookie-1=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-1=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'prob-cookie-2=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-2=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'prob-cookie-3=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-3=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'prob-cookie-4=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-4=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-5=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-5=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-6=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-6=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-7=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-7=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';

        document.cookie =  'prob-cookie-8=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie =  'mem-cookie-8=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
//        $('a[rel=shareit], #shareit-box').mouseenter(function() {
        $('a[rel=shareit]').mouseenter(function() {
            var ids=$(this).attr('id');


            /*globalId=(ids.indexOf('shareit-box')<0)?ids:globalId;
            console.log(">>>>>>>>>>>>>>>>>>>>>"+globalId)

            ids=globalId;

            console.log("=================="+ids)*/
            var problem=$("#tile-"+ids+"-problem").val();
            var member=$("#tile-"+ids+"-member").val();
            var sliderMem=getCookie("mem-cookie-"+ids)
            var sliderPoten=getCookie("prob-cookie-"+ids)
            sliderMem=sliderMem?sliderMem:member;
            sliderPoten=sliderPoten?sliderPoten:problem;

            tileSaving=$("#tile"+ids).text();

            var value= Math.round(problem/member);
            var height = $(this).height();
            var top = $(this).offset().top;
            var left = $(this).offset().left + ($(this).width() /2) - ($('#shareit-box').width() / 2);
            $('#shareit-header').height(height);

            var text='<input type="hidden" id="hiddenIdVal" value="'+ids+'" /><div class="toolBody">' +
                    '<span class="tooltip-label">Savings/Member&nbsp;&nbsp;</span><div style="margin-top:5px">' +
                    '<input id="subTile-1-2" class="proBar" data-slider-id="ex1Slider" type="text" data-slider-min="0" data-slider-max="'+value+'" data-slider-step="1" data-slider-value="'+sliderPoten+'"/></div>'+
                    '<span class="tooltip-label">Engagement Rate&nbsp;&nbsp;&nbsp;</span><div  style="margin-top:5px">' +
                    '<input id="subTile-1-1" class="proBar" data-slider-id="ex2Slider" type="text" data-slider-min="0" data-slider-max="'+member+'" data-slider-step="1" data-slider-value="'+sliderMem+'"/></div>' +
                    '<div>' +
            '<input type="button" class="btn btn-primary btn-save" value="Save">' +
//            '<i class="fa fa-times" title="Save"></i>' +
            '</div></div>';

            $("#shareit-icon").html(text);

            $('#shareit-box').show();
            $('#shareit-box').css({'top':top+14, 'left':left+2});


            var sav=problem,mem=member;

            $.each($(".proBar"),function(){
                var $this=$(this);
                var id=this.id;
                var max=$this.attr("data-slider-max")
                var sliderType=""
                if(id.toString().indexOf("-1-2")>0){
                    sliderType="$"
                }


                new Slider("#"+id, {
                    formatter: function(value) {
                        var percent=Math.round((value/max)*100);

//                        if(sliderType)
//                        return percent+"%";
//                        else
                            return value+" - "+percent+"%";

                    }
                    ,ticks: [0,parseInt(max)]
                    ,ticks_labels: [sliderType+'0', sliderType+''+max]
                    ,ticks_snap_bounds: 1
//                    ,tooltip_position:"bottom"
                    ,step:1
                });

                $("#"+id).on("slide", function(slideEvt) {

                    //                var percent=slideEvt.value;
                 var values=id.split("-")[1]
                    mem=$("#subTile-1-1").val()

                    sav=$("#subTile-1-2").val()
                    //setCookie("mem-cookie-"+ids,sav, "poten-cookie-"+ids, mem, 30)
                 $("#tile"+ids).text("$"+(Math.round((mem*sav)/1000))+"K");

//                    $(".tooltip-inner").css("white-space","pre-wrap !important")


                 });

                 $("#"+id).on("change", function(slideEvt) {
                 //                var percent=slideEvt.value.newValue;
                 var values=id.split("-")[1]
                 sav=$("#subTile-1-2").val()
                 mem=$("#subTile-1-1").val()
                     //setCookie("mem-cookie-"+ids,sav, "poten-cookie-"+ids, mem, 30)
                 $("#tile"+ids).text("$"+(Math.round((mem*sav)/1000))+"K");
                 });

            })

            $(".slider-tick").remove();

            $(".btn-save").on("click",function(event) {
                event.preventDefault();
                var conf=confirm('Do you want to proceed with save?');
                if(conf){
                    setCookie("mem-cookie-"+ids,mem, "prob-cookie-"+ids, sav, 30)
                    $("#shareit-icon").html("");
                    $("#shareit-box").hide();
//            $(this).select();
                    $.each($(".proBar"),function(){
                        $(this).destroy();

                    });
                }


            });




        });

        //onmouse out hide the shareit box
        $('#shareit-box').mouseleave(function () {
            var $this=$(this);

//            setTimeout(function(){
                $('#shareit-field').val('');
                var ids=$("#hiddenIdVal").val();
                $("#tile"+ids).text(tileSaving);
                $("#shareit-icon").html("");
                $this.hide();
                $.each($(".proBar"),function(){
                    $(this).destroy();

                });

//            },1500);



        });


        //hightlight the textfield on click event
        $('#closeButton').click(function () {
            $("#shareit-box").hide();
            var ids=$("#hiddenIdVal").val()
            $("#tile"+ids).text(tileSaving);
            $("#shareit-icon").html("");

//            $(this).select();
            $.each($(".proBar"),function(){
                $(this).destroy();

            });
        });
    });


   function setCookie(pro, proVal, mem,memVal, exdays) {
       var d = new Date();
       d.setTime(d.getTime() + (exdays*24*60*60*1000));
       var expires = "expires="+ d.toUTCString();
       document.cookie = pro + "=" + proVal + "; " + expires;
       document.cookie = mem + "=" + memVal + "; " + expires;
   }

   function getCookie(cname) {
//       console.log(document.cookie)
       var name = cname + "=";
       var ca = document.cookie.split(';');
       for(var i = 0; i <ca.length; i++) {
           var c = ca[i];
           while (c.charAt(0)==' ') {
               c = c.substring(1);
           }
           if (c.indexOf(name) == 0) {
               return c.substring(name.length,c.length);
           }
       }
       return "";
   }




</script>

<style>
.slider.slider-horizontal{
    width:180px !important;
    height:2px;
}

    .slider-tick-label-container{
        margin-top: 7px !important;
    }

/*.tooltip-inner {
    white-space: pre-line !important;
    max-width:34px !important;
    margin-top: -31px !important;
}*/
</style>

</body>
</html>