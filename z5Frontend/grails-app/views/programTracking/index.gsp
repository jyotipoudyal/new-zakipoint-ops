<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking</title>
    <script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'neighborhoods.js')}"></script>
</head>
<body>

<div class="contentWrapper">
    <div class="container">
        <g:hiddenField name="showReportingDetails" value="false"/>
    <div>
        <div id="map1">

        </div>


        <div id="map" style="width:50%;height:500px"></div>

       %{-- <script>
            function myMap() {
                var mapCanvas = document.getElementById("map");
                var mapOptions = {
                    center: new google.maps.LatLng(33,-101),
//                    center: new google.maps.LatLng(31.96, 91.90),
                    zoom: 6
                }
                var map = new google.maps.Map(mapCanvas, mapOptions);
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyDjdcaHcc8vNr57PF3qYN1aEbNrzib2hpU"></script>--}%


        <script type="text/javascript" src="http://www.google.com/jsapi"></script>
        <script type="text/javascript">



            var dataSourceUrl = "https://docs.google.com/spreadsheet/ccc?key=0Au4D8Alccn4xdHNJdXJmeTdYcEtpRXE1QXRucWtEN3c";
            var onlyInfoWindow;

            google.load("visualization", "1");
            google.load("maps", "3", {other_params:"sensor=false"});

            google.setOnLoadCallback(getData);



            function getData() {
                var query = new google.visualization.Query(dataSourceUrl);
                query.send(handleQueryResponse);
            }

            function handleQueryResponse(response) {
                if (response.isError()) {
                    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                    return;
                }

                // var data = response.getDataTable();

                var mapping = {
                    disableDefaultUI:false,
                    scrollWheel:false,
                    zoom: 13,
                    center: new google.maps.LatLng(40.785611,-73.946056),
                    mapTypeId: google.maps.MapTypeId.TERRAIN

                };

                var map = new google.maps.Map(document.getElementById("map"), mapping);

                /*var bubbleMap = new Bubble(data);

                 bubbleMap.setMap(map);


                 var mapStyles = [
                 {
                 featureType: "road",
                 stylers: [
                 {visibility: "on"},
                 {lightness: +20},
                 ]
                 }

                 ];

                 map.setOptions({styles: mapStyles});

                 var schoolDistricts = new google.maps.LatLng(41.850033, -87.6500523);

                 var layer = new google.maps.FusionTablesLayer({
                 query: {
                 select: 'geometry',
                 from: '3621394'
                 },
                 styles: [{
                 polygonOptions: {
                 fillColor: "#FAFBFF",
                 fillOpacity: 0
                 }
                 }
                 ]}
                 );

                 layer.setMap(map);*/
            }
</script>
            %{--<g:img uri="/images/programTracking.png" class="ptImage"/>--}%
    </div>
    </div>
    </div>
<g:javascript>



   /* $(function(){
        var width = 700,
                height = 580;

        var svg = d3.select( "#map1" )
                .append( "svg" )
                .attr( "width", width )
                .attr( "height", height );

        var g = svg.append( "g" );

        var albersProjection = d3.geo.albers()
                .scale( 190000 )
                .rotate( [71.057,0] )
                .center( [0, 42.313] )
                .translate( [width/2,height/2] );

        var geoPath = d3.geo.path()
                .projection( albersProjection );

        g.selectAll( "path" )
                .data( neighborhoods_json.features )
                .enter()
                .append( "path" )
                .attr( "fill", "#ccc" )
                .attr( "d", geoPath );
    })*/



/*

    var width = 700,
            height = 580;

    var svg = d3.select( "body" )
            .append( "svg" )
            .attr( "width", width )
            .attr( "height", height );

    var g = svg.append( "g" );

    var albersProjection = d3.geo.albers()
            .scale( 190000 )
            .rotate( [71.057,0] )
            .center( [0, 42.313] )
            .translate( [width/2,height/2] );

    var geoPath = d3.geo.path()
            .projection( albersProjection );

    g.selectAll( "path" )
            .data( neighborhoods_json.features )
            .enter()
            .append( "path" )
            .attr( "fill", "#ccc" )
            .attr( "d", geoPath );*/


    /*var animal=[{name:"cat"},{name:"cat"},{name:"dog"},{name:"cat"}]

    var newAni=animal.filter(function(a){return a.name=='cat'});
    //console.log("========================")
    //console.log(newAni)

    var newFilter=function(ani){return ani.name==='cat'}

    var doff=animal.find(newFilter);
    console.log(doff)


    var abc=function(x){return x*2;}
    var def=abc;
    console.log(def(6))

    function makeFunc() {
        var name = "Mozilla";
        function displayName() {
            alert(name);
        }
        return displayName;
    }

    var myFunc = makeFunc();
    myFunc();

    var dragon = function(a){
        var slice = function(b){
            var element=function(c){
                console.log(a,b,c);

            }

            return element;
        }
        return slice;


    }

    dragon("new dragon")("number 34")("comming soon")*/

</g:javascript>
</body>

</html>