<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main" >
    </g:else>

    <title>Choose Request</title>
    <script>

        var table;
        var tr,td;

        function loopObject(obj){
            $.each(obj,function(i,v){
                tr=$("<tr></tr>");
                if(typeof v =='object'){
                    td=$("<td colspan='2''></td>").css('backgroundColor',"lightgray")
                    td.append($("<strong></strong>").append(i));
                    table.append(tr.append(td));
                    loopObject(v);
                }else{
                    tr.append($("<td></td>").append(i));
                    tr.append($("<td></td>").append(v));
                    table.append(tr);
                }
            });
        }



      /*  function loopObject(obj){

            $.each(obj,function(i,v){
                tr=$("<tr></tr>");

                if(typeof v =='object'){
                    td
                    tr.append()
                    tr+="<tr><td colspan='2'>";
                    tr+="<strong>"+i+"</strong></td></tr>"

                    loopObject(v)
                }
                else{
                    tr+="<tr><td>";
                    tr+=i+"</td><td>"+v+"</td></tr>"
                }
            })
            $("#populateData").html(tr);


        }*/
        function selectRequest(){
            var req=$("#requests").val();
            var basis=$("#reportingBasis").val();
            var fromDate=$("#reportingFrom").val();
            var toDate=$("#reportingTo").val();
            var val=$( "#requests option:selected" ).text();
            var isExclude = $("#includeOn").val();
            var range = $("#range1").val();
            if(!req){
                alert("Specify Report Type")
                return false
            }


            jQuery.ajax({
             type:'POST',
             data:{report:req,reportingBasis:basis,reportingFrom:fromDate,reportingTo:toDate,isExclude:isExclude,range:range},
             async:false,
//                dataType: "json",
             url:'<g:createLink controller="request" action="fetchData"/>',
             success:function(resp){
//                 $("#populateData").html(resp);


//
                 table=$("<table width='100%' border='2' cellspacing='0' cellpadding='0' ></table>");
                 tr=$("<tr></tr>");
                 td=$("<td colspan='2''></td>").css({backgroundColor:"gray",textAlign:'center'});
                 td.append($("<strong></strong>").append(val));
                 table.append(tr.append(td));
                 loopObject(resp.data);
                 $("#populateData").html(table);

                 var ahref="${createLink(controller: 'request',action: 'viewResponse')}"+"?url=" + encodeURIComponent(resp.url);
//                 ahref+="?url="+resp.url

//
                 %{--var aa=${link(controller: 'request',action: 'viewResponse')}--}%
//                         console.log(aa);
                 $("#populateData").prepend("<br>");
                 $("#populateData").prepend(viewParams(resp.parameter));
                 $("#populateData").prepend("<br><strong>Parameters:</strong><br>");
                 $("#populateData").prepend("URL::<a target='_blank' href='"+ahref+"'>"+resp.url+"</a>");
             },
             error:function(XMLHttpRequest,textStatus,errorThrown){
                 alert("das error!!!!")
             },
             complete:function(XMLHttpRequest,textStatus){
             }});

        }

        function viewParams(p){
            var table=$("<table width='100%' border='0' cellspacing='0' cellpadding='0' ></table>");
            var tr1=tr=$("<tr></tr>");
            var tr2=tr=$("<tr></tr>");
            $.each(p,function(i,v){

                tr1.append($("<td></td>").append(i));
                tr2.append($("<td></td>").append(v));

            })
            table.append(tr1);
            table.append(tr2);
            return table;

        }
    </script>
</head>
<body>
<div class="contentWrapper" >


    <div class="container">

        <g:hiddenField name="showReportingDetails" value="false"/>
        %{--<sec:ifAllGranted roles="ROLE_Z5ADMIN">
            <g:if test="${!isEditable}">
                <g:link controller="cms" action="setClient" title="Edit Content" class="switch-edit-mode"><i class="fa fa-edit"></i></g:link>
            </g:if><g:else>
            <g:link controller="dashboard" action="setClient" title="View Changes" class="switch-edit-mode"><i class="fa fa-eye"></i></g:link>
        </g:else>
        </sec:ifAllGranted>--}%
        <div class="col-md-12 text-center">
        <g:link controller="levelOfAggregation" action="deleteAndFetch" class="btn btn-danger text-center">Refresh Client Config</g:link>
        <br>
            <h3 class="weltitle mb15">
                %{--<span class="isEditable"><g:editMessage code="dashboard.setClient.head"/></span>--}%
                </h3>
            <g:if test="${flash.message}" >
                <div class="alert alert-success text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    ${flash.message}</div>
            </g:if>
            <g:if test="${!flash.error}">

                    <div id="wrapper_bg">

                    <g:if test="${!isEditable}">
                        <div class="col-md-3">
                            Requests:
                            <g:select style="width: 100px;" name="requests" class="clientSelect" from="${requests}" optionValue="value" optionKey="key" noSelection="['':'Select']" />

                        </div>
                        <div class="col-md-3">
                            Reporting Basis:
                            <g:select class="clientSelect" name="reportingBasis" from="${['PaidDate': 'paid', 'ServiceDate': 'incurred']}" optionKey="key" optionValue="value"/>
                        </div>
                        <div class="col-md-3">
                            Include/Exclude:
                            <g:select style="width:75px;" name="includeOn" class="clientSelect"
                                      from="${['false': 'include', 'true': 'exclude']}"
                                      optionKey="key" optionValue="value"
                                      />
                        </div>
                        <div class="col-md-3">
                            Range:
                            <g:select style="width:65px;" name="range1" class="clientSelect"
                                      from="${['0': '$0', '10000': '$10 k', '25000': '$25 k', '50000': '$50 k', '100000': '$100 k']}"
                                      optionKey="key" optionValue="value"
                                      />
                        </div>
                        <div class="clearfix"></div>
                    <div class="mb15"></div>
                        <div class="col-md-3"></div>

                        <div class="col-md-3" id="sandbox-container1"><input type="text" id="reportingFrom" class="form-control" placeholder="Reporting From" value="2014-09-30"></div>
                        <div class="col-md-3" id="sandbox-container2"><input type="text" id="reportingTo" class="form-control" placeholder="Reporting To" value="2015-10-01"></div>
                        <div class="col-md-3"></div>

                        <div class="clearfix"></div>
                        <button name="saveG" value="Go" class="btn btn-link-upper btn-primary" style="margin-top: 10px;" onclick="selectRequest()">
                            <span class="isEditable">
                                Filter Records
                            </span>
                        </button>
                    </g:if>
                        %{--<g:select name="client" from="${clients}" optionKey="key" optionValue="value" noSelection="['':'---Select---']" onchange="selectClients()"/>--}%
                    </div>

            </g:if>
        </div>

        <div class="clearfix"/>
        %{--<div class="mb15"></div>--}%
        <div class="sep-border mb25 pt15"></div>

        <div class="put-border" id="populateData" style="height: auto;">

        </div>





    </div>




</div>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-datepicker.js')}"></script>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'bootstrap-datepicker.css')}">
<script>
    $('#sandbox-container1 input').datepicker({
        format: "yyyy-mm-dd",
        startView: 1,
        startDate: "2008-01-01",
        endDate: "2016-12-31"

    });
    $('#sandbox-container2 input').datepicker({
        format: "yyyy-mm-dd",
        startView: 1,
        startDate: "2008-01-01",
        endDate: "2016-12-31"
    });
</script>


</body>
</html>





