<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>zakipoint Health</title>
		<meta name="layout" content="mainError">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<g:if env="development"><link rel="stylesheet" href="${resource(dir: 'css', file: 'errors.css')}" type="text/css"></g:if>
	</head>
	<body>
	<div class="contentWrapper">
		<div class="container">
		<g:if env="development">
			<div class="text-center" style="margin-top: 150px;">
				<g:img style="display: block;
					   margin-left: auto;width: 558px;
					   margin-right: auto;" uri="/images/access-denied.png" class="ptImage"/>
			</div>
			<g:renderException exception="${exception}" />
		</g:if>
		<g:else>
			<div class="text-center" style="margin-top: 150px;">
				<g:img style="display: block;
					   margin-left: auto;width: 558px;
					   margin-right: auto;" uri="/images/access-denied.png" class="ptImage"/>
			</div>
		</g:else>
		</div>
		</div>
	</body>
</html>
