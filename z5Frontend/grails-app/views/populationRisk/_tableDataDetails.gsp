<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="mb10"><span>
    %{--Your population with <span class="chronic-title">Diabetes</span> did not comply with Recommended Care of the time--}%

    <strong><g:formatNumber number="${carePercent}" maxFractionDigits="2"/> % </strong> of members have one or more gaps in care specific to <span class="chronic-title">{{chrc}}</span>%{--, resulting as much as <strong>[X]%</strong> higher costs compared to members with no gaps--}%
</span>
</div>
<div  class=" z5-table put-border">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id="recommendedCareForDiabetes">
    <thead>
    <tr>
        <th class="text-left" style="height: 54px;">Category</th>
        <util:remoteSortableColumn  class="text-center"  property="membersInGroup" title="Members In Group (#)" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />
        <util:remoteSortableColumn  class="text-center ${defaultSort}"  property="percentMet" title="% Compliance" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />
        <util:remoteSortableColumn  class="text-center"  property="met" title="Met (#)" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />
        <util:remoteSortableColumn  class="text-center"  property="notMeeting" title="Not Met (#)" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />

        %{--<util:remoteSortableColumn   class="text-center ${defaultSort}"  property="notMeeting" title="% Not Meeting" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes']" />--}%
        %{--<util:remoteSortableColumn style="width:110px;" class="text-center"  property="costPmpy" title="\$ PMPY" update="barGraph2" action="sortResult" params="[module:'recommendedCareForDiabetes',chronicCode:chronicCode]" />--}%
    </tr>
    </thead>
    <tbody>
    <g:each in="${careFor}" var="val">

        <tr>

            <td>${val?.description}</td>
            <td class="text-right">
                    ${val?.membersInGroup}
            </td>
            <td class="text-right"><g:formatNumber number="${val?.percentMet}" type="number" maxFractionDigits="1" />%</td>
            <td class="text-right">${val?.met} </td>
            <td class="text-right ">
                <g:z5Link class="color-red" disabledLink="${(val?.notMeeting)<=0?'true':'false'}" controller="memberSearch" action="searchResult" params="${val?.members_drill}">
                ${val?.notMeeting}</g:z5Link>
                %{--${(val?.notMeeting)<=0?'true':'false'}--}%
             </td>

            %{--<td  class="text-right">
                <g:formatZ5Currency number="${val?.costPmpy}" type="number" maxFractionDigits="2" />
            </td>--}%




            %{--<td class="text-right">${val?.met} </td>
            <td class="text-right"><g:formatNumber number="${val?.percentMet}" type="number" maxFractionDigits="0" />%</td>
            <td class="text-right"><g:formatNumber number="${val?.percentNotMeeting}" type="number" maxFractionDigits="0" />%</td>
--}%


        </tr>
    </g:each>
    </tbody>
</table>
    <script>
        $(document).ready(function () {
            var $table = $('table#recommendedCareForDiabetes');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.z5-table');
                }
            });

            chronicTitle=$("#chronicCondition option:selected").text();
            $(".chronic-title").html(chronicTitle);
        });
    </script>
</div>








