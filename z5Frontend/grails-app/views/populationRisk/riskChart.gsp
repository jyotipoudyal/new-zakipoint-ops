<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Program Tracking</title>
    <r:require module="graph"/>

</head>
<body>


    <div id="contentWrapper" class="contentWrapper">
        <br>
        %{--<g:hiddenField name="showReportingDetails" value="false"/>--}%
        <div class="container double-arrow-back-top-container">
            <div class="col-md-6 double-arrow-back-top">
                <g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
                    <span class="isEditable"><g:editMessage code="erUtilization.top.navigation"/></span>
                </g:link>
            </div>


        <div class="col-md-3 mb15">
            <g:if test="${modifiable}">
                <g:link class="btn btn-link-upper btn-light-blue" controller="populationRisk" action="riskChart" >
                    %{--<g:checkBox name="ch" checked="true" onclick="return false;" onkeydown="return false;"></g:checkBox>--}%
                    <span>Show All</span>
                </g:link>
            </g:if><g:else>
                <g:link class="btn btn-link-upper btn-light-blue" controller="populationRisk" action="riskChart" params="[modifiable:'true']" >
                    %{--<g:checkBox name="ch" checked="false"  onclick="return false;" onkeydown="return false;" ></g:checkBox>--}%
                    Show modifiable disease
                </g:link>
            </g:else>

        </div>
        <div class="clearfix"></div>







    <div class="col-md-12">
        <h2 class="mb15">
            <g:img uri="/images/icon-decline-in-health.png" class="heading-icon"/>
           <g:editMessage code="dashboard.container02.title"/></h2>
        <div class="sep-border mb15"></div>

    </div>

    <div id="populationRisks">

        %{--<h1 class="hr-bg text-center color-gray"><span><g:img uri="/images/icon-decline-in-health.png" class="heading-icon"/><span class="isEditable"><g:editMessage code="dashboard.container02.title"/></span></span></h1>--}%
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center" >
            <h1 class="isEditable" >
                TOP 10 CHRONIC CONDITIONS DRIVING PREDICTED COST
                %{--<g:editMessage code="dashboard.graph4.title"/></h1>--}%
            <div id="graph4" class="graph"></div>
        </div>
        <div class="col-md-2"></div>

    </div>


        <div class="col-md-4"></div>
        <div class="col-md-12">
            <h2 class="mb15">
                All Chronic Diseases</h2>
            <div class="sep-border mb25"></div>

        </div>
        <div class="col-md-2"></div>
        <div class="col-md-8">
            %{--<strong style='text-transform:capitalize'>Top conditions</strong>--}%
            <div  id="topConditions"  class="graph-wrapper" style="max-height: 1100px;min-height: 300px;;overflow-y: auto;" ></div>
            <div id="topConditions_More"></div>
        </div>
        <div class="col-md-2"></div>




        <sec:ifAllGranted roles="ROLE_Z5ADMIN">


        <div class="col-md-12">
            <h2 class="mb15 mt15">
                Tabular View</h2>
            <div class="sep-border mb25"></div>

        </div>
        <div class="clear-fix"></div>

        <div class="col-md-12">
            <div class="z5-table" style="max-height: 600px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id="recommendedCareForDiabetes">
                    <thead>
                    <tr>
                     <th>Chronic Condition</th>
                    <th> # of members</th>
                    <th>Newly Diagnosed Members in last 3 months</th>
                    <th>Newly Diagnosed Members in last 1 months</th>
                    <th>Member Per K – Actual</th>
                    <th>Member Per K – Benchmark</th>
                    <th>Total Medical Paid</th>
                    <th>Prospective Cost</th>
                    </tr>
                    </thead>
                    <g:each in="${data.getAt("reporting")}" var="chronic" status="i">

                        <g:if test="${chronic.key!="metaInfo"}">
                            <%
                                def chr=chronic.value;
                                def val,benchmark;
                                val=chr.metricDisplay;
                                if(chr.metricDisplay=="CKD") val="ESRD";
                                benchmark=data['truvenBenchmark'][val];
                            %>
                    <tr>
                        <td class="text-left">
                            <g:link action="index" controller="populationRisk" id="${chr.metricCode}">
                                ${val}
                            </g:link>


                        </td>
                        <td class="text-right">

                            <g:z5Link disabledLink="${(chr.population)<=0?'true':'false'}" controller="memberSearch" action="searchResult" params="${chr?.members_drill}">
                                ${chr.population}
                            </g:z5Link>

                           </td>
                        <td class="text-right">
                            <g:z5Link disabledLink="${(chr.population)<=0?'true':'false'}" controller="memberSearch" action="searchResult" params="${chr?.members_drill2}">
                            ${chr.newPopInLastOneMonth}
                            </g:z5Link>

                        </td>
                        <td class="text-right">
                            <g:z5Link disabledLink="${(chr.population)<=0?'true':'false'}" controller="memberSearch" action="searchResult" params="${chr?.members_drill3}">

                                ${chr.newPopInLastThreeMonths}

                            </g:z5Link>
                        </td>
                        <td class="text-right">
                            <g:formatNumber number="${chr.populationPerThousand}" type="number" maxFractionDigits="2" />
                            </td>
                        <td class="text-right">



                            <g:formatNumber number="${benchmark}" type="number" maxFractionDigits="2" />


                        </td>
                        <td class="text-right"><g:formatZ5Currency number="${chr.cost}" type="number" maxFractionDigits="2" /></td>
                        <td class="text-right"><g:formatZ5Currency number="${chr.projectedCostProspective}" type="number" maxFractionDigits="2" /></td>

                    </tr></g:if>
                    </g:each>
                    <tbody>
                    </tbody>


                    </table>

            </div>

        </div>

        </sec:ifAllGranted>

</div>
</div>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">

<script language="javascript" type="text/javascript"
        src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<g:javascript>
    $(function(){
        render4(${data["reporting"]})
    })

    $(document).ready(function () {
            var $table = $('.z5-fixed-header');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.z5-table');
                }
            });
    });
        var gap,tConditions,tBenchMark,loc,dErVisit;

        tConditions=${data['reporting']};
        tBenchMark= ${data['truvenBenchmark']};
        renderDemographics(tConditions, 'topConditions',tBenchMark)

    function showTopCond(flag){
        renderDemographics(tConditions, 'topConditions',tBenchMark,flag)
    }


var div2 = d3.select(".contentWrapper").append("div")
            .attr("class", "tooltip2")
            .style("opacity", 0);

    function renderDemographics(d,ids,benchmark,showAll){

      function numeric_sort(a, b) {
        var aVal = a.val;
        var bVal = b.val;
        return ((aVal > bVal) ? -1 : ((aVal < bVal) ? 1 : 0));
    }






        $("#"+ids).html("");
        var bigData=[];
        var maximum=0;
        var title;
        var counter=0;
        var mainData=d;
        var bMarkVal=benchmark;


        $.each(d, function(i,v) {
                    title=v.metricDisplay
                    var newPop=v.newPopInLastThreeMonths
                    if(title=="CKD") title="ESRD";
                    var bm=parseFloat(benchmark[title]);
                    var pop=v.populationPerThousand
                    var max1;

                    if(pop>bm) {
                    max1=pop;
                    }else if(bm>pop){
                    max1=bm;
                    }

                    if(max1>maximum) maximum=max1;



                    if(i!="metaInfo")
                    bigData.push({ind:title,val:pop,title:title,bm:bm,newPop:newPop,cost:v.cost,popCount:v.population,metricCode:v.metricCode})


            counter++;

        });





            bigData.sort(numeric_sort);

            //if(!showAll)
            //bigData=bigData.slice(0,10)


        var string="";
        var val=0;
        var bm=0;




        string+="";
//        string+='<table  width="100%" border="0" cellspacing="0" cellpadding="0" id='+ids+'>';
        string+='<div>';
        string+='<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-graph-table z5-fixed-header" id='+ids+' >';
//        string+='<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id='+ids+' >';
        string+="<thead>";
        string+="<tr><th width='30%'><strong style='text-transform:capitalize'>"+ids.replace(/([A-Z1])/g, ' $1').trim()+"</strong></th><th width='40%' class='text-right'> # of members (new members) </th>" +
        "<th>Members";
        string+="</th><th>Cost</th></tr>";
        string+="</thead>";
        string+="<tbody>";

        $.each(bigData, function(i,v) {


            val = (v['val']) ;
            bm=(v['bm']);

            var dom=val>bm?val:bm;

            var main_x = d3.scale.linear()
                        .range([0, 100]);
            main_x.domain([0, maximum]);

            var main_x1 = d3.scale.linear()
                        .range([0, 100]);
            main_x1.domain([0, dom]);

            val=main_x(val)
            bm=main_x(bm)

            var val1=main_x1(val)
            var bm1=main_x1(bm)




            string+="<tr><td style='text-transform: capitalize'><a href=index/"+v['metricCode']+">"+v['title']/*v['ind']*/+"</a></td><td width='10%' class='color-blue' style='font-weight: bold;text-align: right;'>"+v['popCount'];/* title="+aTitle+"*/
            if(v['newPop'] && (v['newPop']>0)) string+=" <span class='color-red'>(+"+v['newPop']+")</span>";
            string+="</td><td width='40%'>";
            string+="<div class='demographics' id="+ids+"_"+i+" >";

            //if(v['bm']>=0) {


                var className="";
                if(bm1-val1>10){
                    className="dataBelowBm"
                }else if(val1-bm1>10){
                    className="dataAboveBm"
                }else{
                    className="dataNeutral"
                }
                string+='<div class='+className+'  style="width:'+Math.round(val)+'px;"></div>';
                string+='<div class="benchmark1"   style="width:' + Math.round(bm) + 'px;"></div>';
           %{-- }else{
                string+='<div class="dataNeutral"  style="width:'+val+'px;"></div>';
            }--}%
            string+='</div>';
            string+="</td><td class='text-right'>"+d3.format("$,")(Math.round(v['cost']))+"</td></tr>";
        });


        string+="</tbody>";
        string+="</table>";
        string+="</div>";
        var appendString="";

       %{-- if(counter>10){



        if(showAll){

            $("#"+ids).css({border:"1px solid #ccc"});

                appendString+="<div onclick='showTopCond(false)'><a href='javascript:void(0)' class='see-dtl-utilization'>Less <i class='fa fa-caret-up '></i></a></div>"


        }else{
            $("#"+ids).css({border:"1px solid #fff"});

                appendString+="<div onclick='showTopCond(true)'><a href='javascript:void(0)' class='see-dtl-utilization'>More <i class='fa fa-caret-down '></i></a></div>"


        }
        }--}%

        $("#"+ids).html(string);
        $("#"+ids+"_More").html(appendString);

        $.each(bigData, function(i,v) {
            var value= v.val;
            $("#"+ids+"_"+i).on('mousemove', function (d,i) {
                var $this=$(this);
                $this.find(".dataAboveBm").addClass("dataAboveBmH")
                $this.find(".dataBelowBm").addClass("dataBelowBmH")
                $this.find(".dataNeutral").addClass("dataNeutralH")
                var label="Members per 1000 :"


                var p = $(this);
                var members=value.toFixed(1).replace(/\.0+$/, '');
                var position = p.offset();
                var left = position.left;
                var top = position.top;
                div2.transition()
                        .duration(100)
                        .style("opacity", 1)
                        .style("color", "black")
//                        .style("background", "white")
                var tooltipHtml="<span class='formatOutput22'>"+label+" <strong>"+members+"</strong></span>";

                    //tooltipHtml+="<br><span class='formatOutput22'>% of members from total population: <strong>"+percentage+"</strong></span>";


                if(v.bm>=0) {
                    var bmValue=v.bm.toFixed(2);
                        bmValue+= " Members per 1000"

                    tooltipHtml+="<br><span class='formatOutput22'>National  Benchmark: <strong>"+bmValue+"</strong></span>";
                }
                div2.html(tooltipHtml)
                        .style("left", (left-60) + "px")
                        .style("top", (top-85) + "px");
            })
                    .on("mouseout", function () {
                        var $this=$(this);

                        $this.find(".dataAboveBm").removeClass("dataAboveBmH")
                        $this.find(".dataBelowBm").removeClass("dataBelowBmH")
                        $this.find(".dataNeutral").removeClass("dataNeutralH")
                        div2.transition()
                                .duration(500)
                                .style("opacity", 0);
                    })

        })

        var $tables = $('#'+ids+'.z5-fixed-header');
        $tables.floatThead({
            scrollContainer: function($table){
                return $tables.closest('.graph-wrapper');
            }
        });

    }





</g:javascript>

<style>

.dataBelowBmH {
    background-color: greenyellow  !important;
}

.dataNeutralH {
    background-color: #023583  !important;
}


.dataAboveBmH {
    background-color: red !important;
}




.dbarH{
    fill : #023583 !important;
}

.hbarH{
    fill : #D8DFFF !important ;
    /*fill : #BDD7EE !important ;*/

}

div.tooltip2 {
    position: absolute;
    text-align: center;
    max-width: 300px;
    min-width: 100px;
    text-align: left; padding: 5px 5px;
    line-height: 23px;
    font: 10px sans-serif;
    background:#e7e7e7;
    /*background: lightsteelblue;*/
    /*border: 1px solid white;*/
    border-radius: 5px;
    pointer-events: none;
    white-space: pre;
    z-index: 1000;
}

.hbar {
    fill: #D7DFE5;

}

.x.axis path{
    /*display: none;*/
    stroke: black;
    fill: none;
    stroke-width: 1px;
    /*shape-rendering: crispEdges;*/
}

.bar {
    fill: steelblue;
}

.lineColor {
    fill: none;
    stroke: #1c94c4;
    stroke-width: 1px;
    stroke-dasharray: 6 3
}

.dbar {
    fill: #023563;
}

.x.axis path {
    /*display: none;*/
}

.icon-provider{
    padding-left:8px;
}

</style>


</body>
</html>