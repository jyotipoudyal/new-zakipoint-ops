<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 7/21/16
  Time: 11:25 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Population Risks</title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-silder.css')}" type="text/css">

</head>
<body>
<div class="contentWrapper">
    <g:hiddenField name="showReportingDetails" value="false"/>
    <div class="container">
        <div class="col-md-12 mt15">
            <span class="isEditable"><g:link controller="dashboard" action="index" class="text-link-upper mb15 double-arrow-left">
                All Cost Drivers

            </g:link></span>
        </div>
        <div class="col-md-12">
            <h2 class="mb15"><span class="isEditable">Risks Details - ${(params.id=='1')?"MARA":"HS"}</span></h2>
            <div class="sep-border mb15" ></div>
        </div>
        <div class="clearfix"></div>
        %{--<h3 style="text-align: center;"></h3>--}%
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div id="stackedGraph"></div>
        </div>
        <div class="col-md-2"></div>
        <div class="clearfix"></div>

    </div>
</div>
<div id="galleryImages1" style="display: none;;">
    <g:img dir="images" file="ajax-loader.gif" style="position:relative; top:50%;"/>
</div>


<script src="https://d3js.org/d3.v4.min.js"></script>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js/dash', file: 'sankey.js')}"></script>

<script>


    refreshReport(${id});
//    var colores_g = {"highRisk":"#DC3912","medRisk":"#FF7F0E", "norRisk":"#E7BA52","lowRisk":"#2CA02C"};
    var colores_g = {"High":"#DC3912","Medium":"#FF7F0E", "Normal":"#E7BA52","Low":"#2CA02C"};


    function refreshReport(id) {
        $('#galleryImages1').show();
        var def = $.Deferred();
        jQuery.ajax({
            type: 'POST',
            data:{type:id},
            url: '<g:createLink controller="populationRisk" action="renderRiskSankey"/>',
            success: function (resp) {
                def.resolve(resp);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                def.reject();

//             $("networkTrendingSankey").html("No records to display");
            },
            complete: function (XMLHttpRequest, textStatus) {
            }
        });

        def.done(function (value) {
            $('#galleryImages1').hide();
            renderSankey(value)
        }).fail(function () {
            $('#galleryImages1').hide();
            $("#stackedGraph").html('<div style="text-transform: capitalize;text-align: center;" class="mt15">&nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> No records to display.  &nbsp;&nbsp;</div>');

        });

    }

    function custom_sort(a, b) {
        var aName = a.toLowerCase();
        var bName = b.toLowerCase();
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

    /*function colores_google(n) {
     var colores_g = ["#DC3912","#2CA02C", "#FF7F0E", "#E7BA52", "#990099","#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477"];
     return colores_g[n % colores_g.length];
     }*/



    function renderSankey(data){
        var data1=data.data;
        var units = "Member";

        var col=new Object();


        $.each(data.levels.sort(custom_sort),function(ind,val) {
                    col[val]=colores_g[val];
                }
        );




        // set the dimensions and margins of the graph
        var margin = {top: 50, right: 60, bottom: 50, left: 20},
                width = 800 - margin.left - margin.right,
                height = 400 - margin.top - margin.bottom;

        // format variables
        var formatNumber = d3.format(",.0f"),    // zero decimal places
                format = function(d) { return formatNumber(d) + " " + units; },
//                 color = d3.scaleOrdinal(["#01A46D","#FF9B2B","#EC3E40"]);
                color = d3.scaleOrdinal(["#FF0000","#F48E9F","#A9D08E"]);

        // append the svg object to the body of the page
        var svg = d3.select("#stackedGraph").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        // Set the sankey diagram properties
        var sankey = d3.sankey()
                .nodeWidth(26)
                .nodePadding(40)
                .size([width, height]);

        var path = sankey.link();

        // load the data
        //    d3.json("sankey.json", function(error, graph) {


        graph=data1;
        sankey
                .nodes(graph.nodes)
                .links(graph.links)
                .layout(32);

//         console.log(graph.links);

        // add in the links
        var link = svg.append("g").selectAll(".linkNodes")
                .data(graph.links)
                .enter().append("g").attr("class", "linkNodes")


        link.append("svg:a")
                .attr("data-href", function(d,i){
                    return createZ5Link(d.flowLink);
                })
                .attr("onclick", "postRequestForGet(this);return false;")
                .append("path")
                .attr("class", "link")
                .style("stroke", function(d) {
                    return col[d.source.name]
                })
                .attr("d", path)
                .style("stroke-width", function(d) { return Math.max(0, d.dy); })
//                .sort(function(a, b) { return b.dy - a.dy; });

        // add the link titles
        link.append("title")
                .text(function(d) {
                    return d.source.name + " → " +
                            d.target.name + "\n" + format(d.value);
                });

        // add in the nodes
        var node = svg.append("g").selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")"; });
                /*.call(d3.drag()
                        .subject(function(d) {
                            return d;
                        })
                        .on("start", function() {
                            this.parentNode.appendChild(this);
                        })
                        .on("drag", dragmove)
        );*/

        // add the rectangles for the nodes
        node.append("svg:a")
                .attr("data-href", function(d,i){
                    return createZ5Link(d.sedimentLink);
                })
                .attr("onclick", "postRequestForGet(this);return false;")
                .append("rect")
                .attr("height", function(d) {
                    return d.dy; })
                .attr("width", sankey.nodeWidth())
                .style("fill", function(d) {
                    return d.color = col[d.name];
//                     return d.color = color(d.name.replace(/ .*/, ""));
                })
                .style("stroke", function(d) {
                    return d3.rgb(d.color).darker(2); })
                .append("title")
                .append("h6")
                .text(function(d) {
                    return "Total:\n" + format(d.value)+ "\nNewly Enrolled:\n" + format(d.total-d.sumvalue)+"\n"
                });


        // add in the title for the nodes
        node.append("text")
                .attr("x", -6)
                .attr("y", function(d) { return d.dy / 2; })
                .attr("dy", ".25em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .attr("class","textClass")
//                .style("text-transform","capitalize")
                .text(function(d) { return d.name; })
                .filter(function(d) { return d.x < width / 2; })
                .attr("x", 6 + sankey.nodeWidth())
                .attr("text-anchor", "start")

        var years=["last"," ","current"];


        var nodess = svg.append("g").selectAll(".nodes")
//                 .data([graph.nodes[0],graph.nodes[3],graph.nodes[6]])
                .data([2,349,696])
                .enter().append("g").attr("class", "nodes").attr("transform", function(d) {
                    return "translate(" + (d) + "," + 0 + ")"; })

        nodess.append("text")
                .attr("x", 26)
                .attr("y", function(d) { return -30 ; })
                .attr("dy", ".85em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .attr("class","textClass1")
//                 .style("text-transform","capitalize")
                .text(function(d,i) {
                    return data.year[years[i]];
                })
//                .filter(function(d) { return d.x < width / 2; })
//                .attr("x", 6 + sankey.nodeWidth())
//                .attr("text-anchor", "start")

        // the function for moving the nodes
        function dragmove(d) {
            d3.select(this).attr("transform",
                    "translate(" + (
                            d.x = Math.max(0, Math.min(width - d.dx, d3.event.x))
                    )
                    + "," + (
                            d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))
                    ) + ")");
            sankey.relayout();
            link.attr("d", path);
        }

    }



</script>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">

<style type="text/css">

.textClass{
    text-transform: capitalize;
}
.textClass1{
    font-weight: bold;
    font-size: 14px;
}
.node rect {
    fill-opacity: .9;
    shape-rendering: crispEdges;
}

.node text {
    pointer-events: none;
    text-shadow: 0 1px 0 #fff;
}

.link {
    fill: none;
    stroke: #000;
    stroke-opacity: .5;
}

.link:hover {
    stroke-opacity: .8;
}

.pathA{
    stroke:blue;
}
.pathB{
    stroke:steelblue;
}
.pathC{
    stroke:orange;
}

</style>
</body>

</html>