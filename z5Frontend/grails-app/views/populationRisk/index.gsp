<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="layout" content="main">
    <title>Population Risks</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
%{--<link href="${resource(dir:"css",file: "layout.css")}" rel="stylesheet"/>--}%
</head>

<body>

<div>
    %{--<div style="position: absolute">
        <svg>
            <pattern
                    id="pattern-stripe"
                    width="5"
                    height="50"
                    patternUnits="userSpaceOnUse"
                    patternTransform="rotate(-45)">
                <rect width="4"
                      height="100%"
                      fill="#034f96"></rect>

                <g stroke="#b53322"
                   stroke-width="45">
                    <path d="M25,0 25,100"></path>
                </g>

            </pattern>
        </svg>
    </div>--}%

    <div id="contentWrapper" class="contentWrapper">
        <div class="container double-arrow-back-top-container">
            <div class="col-md-6 double-arrow-back-top">
                <g:link controller="populationRisk" action="riskChart" class="text-link-upper mb15 double-arrow-left">
                    <span class="isEditable"><g:editMessage code="populationRisk.top.navigation"/></span>
                </g:link>
            </div>


            <%
                //        split page
            %>

            <div class="clearfix"></div>

            <div class="col-md-10 ">
                <h4 class="has-filter pb5">
                    %{--<g:img uri="/images/icon-decline-in-health.png" class="heading-icon"/>
                    <g:select name="chronicCondition" class="clientSelect disable-focus"
                              from="${chronicLists}" value="${chronicCode}"
                              optionKey="key" optionValue="value"
                              onchange="refreshReport()"/>
                    <span id="chrTitle" class="selected-title">{{chrc}}</span>--}%
                    %{------------------------------------------------------------------------}%
                    %{------------------------------------------------------------------------}%
                %{--<div >
                    <nav id="primary_nav_wrap">
                        <ul class="selected-title" style="float:left !important">
                            <li class="dropMain">

                            <a href="#" style="width: 250px;" ></a>
                            <ul id="dropMenu" style="max-height: 300px;overflow-y: auto;overflow-x: hidden;">

                            </ul>
                        </li>
                    </ul>
                    </nav>
                </div>--}%

            <g:if test="${!isEditable}">
                <ul class="nav navbar-nav">
                    <li class="dropdown" >
                        <g:hiddenField name="chronicCondition" value="${chronicCode}"/>
                        <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle"><span id="chronicConditionValue" style="font-size: 24px;">${chronicLists[chronicCode]}</span> <b class="caret"></b></a>
                        <ul class="dropdown-menu" style="max-height: 400px;overflow-y: auto;overflow-x: hidden;">
                            <g:each in="${chronicLists}" var="chrc">
                                %{--<li class="divider"></li>--}%
                                <li class="chrc_${chrc.key} chrc_rm" onclick="selectCondition(${chrc.key},'${chrc.value.toString()}')" ><span >${chrc.value}</span></li>
                            </g:each>
                        </ul>
                    </li>
                </ul>
                </g:if><g:else>
                    <g:select name="chronicCondition" class="clientSelect"
                              from="['{{chrc}}']"/>
                </g:else>
                    %{------------------------------------------------------------------------}%
                    %{------------------------------------------------------------------------}%







           %{-- <g:if test="${!isEditable}">

               --}%%{-- <g:select name="chronicCondition" class="clientSelect select-arrow"
                          from="${chronicLists}" value="${chronicCode}"
                          optionKey="key" optionValue="value"
                          onchange="refreshReport()"/>--}%%{--
                <span onclick="runThis()" class="select-arrow"><i class="fa fa-chevron-down"></i></span>

                </g:if>
            <g:else>
                <g:select name="chronicCondition" class="clientSelect"
                          from="['{{chrc}}']"
                          />
                </g:else>--}%



                </h4>

                    %{--<span class="isEditable"><g:editMessage code="populationRisk.container1.graph1.title"/></span></h2>--}%

                %{--<h3 class="isEditable"><g:editMessage code="populationRisk.container1.graph1.subTitle"/></h3>--}%
            </div>

            <div class="col-md-2">
                %{--<a href="#" class="btn-link-upper btn-blue mb15 isEditable"><g:editMessage code="populationRisk.container1.graph1.option1"/></a>--}%
                <a href="#" class="custom-tooltip btn-link-upper btn-light-blue mb15 isEditable"><g:editMessage
                        code="populationRisk.container1.graph1.option2"/></a>

                %{--<a href="#" class="btn-link-upper btn-gray mb15 isEditable"><g:editMessage code="populationRisk.container1.graph1.option3"/></a> --}%
            </div>
            <div class="clearfix"></div>
            <div  class="col-md-12">
                <h3 id="populationRiskContribution">  </h3>
            </div>


            <div class="clearfix"></div>


            <div class="sep-border mb15"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="populationRisk.container1.graph1.title2"/></h1>
                %{--<h6 class="isEditable"><g:editMessage code="populationRisk.container1.graph1.title3"/></h6>--}%
            </div>

            <div class="col-md-6 text-right">
                <a href="#" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="populationRisk.explore.population"/></span><input type="hidden"
                                                                                value="populationRisk.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('diabetesCost')"
                   class=" text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden"
                                                                                                          value="populationRisk.container3.table1.options2">
                </a>


            </div>


            <div class="col-md-12">
                <h6 class="has-filter">
                <span id="graph1Filter">
                    <g:if test="${!isEditable}">
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasis1"
                                                                          from="${['PaidDate': 'paid', 'ServiceDate': 'incurred']}"
                                                                          optionKey="key" optionValue="value"
                                                                          onchange="refreshReport()"/></span>
                    %{--cost to <span style="color: steelblue;"><g:select name="regionalg1" from="${['1':'NorthEast','2':'NorthCentral','3':'South']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')" noSelection="['':'regional']" /></span>--}%
                        claims to national benchmarks</span>
                    %{--<span style="color: steelblue;"> <g:select name="fromDate1" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value" onchange="refreshReport()" /></span>--}%
                        that <span style="color: steelblue;"><g:select style="width:75px;" name="includeOn"
                                                                       from="${['false': 'include', 'true': 'exclude']}"
                                                                       optionKey="key" optionValue="value"
                                                                       onchange="refreshReport()"/></span>%{--noSelection="['':'exclude']"--}%
                        cases over <span style="color: steelblue;"><g:select style="width:65px;" name="range1"
                                                                             from="${['0': '$0', '10000': '$10 k', '25000': '$25 k', '50000': '$50 k', '100000': '$100 k']}"
                                                                             optionKey="key" optionValue="value"
                                                                             onchange="refreshReport()"/></span>
                        per year
                    </g:if>
                    <span class="f-right">


                        <span class=" isEditable"><g:editMessage code="higpopulationRisk.container1.methodology"/></span>
                        <span class="fa fa-chevron-right " onclick="displayData('methodology1')"></span>


                    </span>
                </h6>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <g:if test="${!isEditable}">
                    <div class="bdr-right" id="removeBorder">
                        <div id="lineGraph1title" class="isEditable"><g:editMessage
                                code="populationRisk.container1.graph1.lineGraphHead"/></div>

                        <div id="lineGraph1"></div>
                        <g:render template="../include/lineLegends"/>

                    </div>
                </g:if>
            </div>

            <div class="col-md-6" id="projectedCosts">
                <g:if test="${!isEditable}">
                    <div id="lineGraphRtitle" class="col-md-12"></div>

                    <div id="lineGraphR" class="col-md-8"></div>
                </g:if>
            </div>

            <div class="clearfix"></div>
            <br>
            <div class="custom-tooltip">

            <div class="sep-border mb15"></div>

            <div class="col-md-12 mb15">
                <h1>
                    <strong><span class=" isEditable"><g:editMessage code="higpopulationRisk.container2.table2.title"/></span></strong>
                </h1>

            </div>

            <div class="col-md-6">

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar2.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="#" class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"><g:editMessage
                            code="populationRisk.evaluate"/></a>
                </div>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar2.list2"/></div>

                <div class="col-md-4 no-pad-r text-right ">
                    <a href="#" class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"><g:editMessage
                            code="populationRisk.evaluate"/></a>
                </div>

            </div>

            <div class="col-md-6">

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar2.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="#" class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"><g:editMessage
                            code="populationRisk.evaluate"/></a>
                </div>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar2.list2"/></div>

                <div class="col-md-4 no-pad-r text-right ">
                    <a href="#" class="custom-tooltip btn-link-normal btn-blue btn-inline isEditable"><g:editMessage
                            code="populationRisk.evaluate"/></a>
                </div>

            </div>

            </div>
            <div class="clearfix"></div>
            <div id="topChronicCondition">
            <div class="sep-border mb25 pt15"></div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="populationRisk.container1.table1.head"/></h1>
            </div>

            <div class="col-md-6 text-right">
                <a href="#" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="populationRisk.explore.population"/></span><input type="hidden"
                                                                                value="populationRisk.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('diabetesAlongOtherChronic')"
                   class="  text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden"
                                                                                                          value="populationRisk.container3.table1.options2">
                </a>

                %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>--}%
            </div>


            <div class="col-md-12 mt15">
                <span id="barGraph1Info">
                    %{--<g:editMessage code="populationRisk.container1.table1.subhead"/>--}%</span>
                <h6 class="has-filter f-right">
                    <span class=" isEditable"><g:editMessage code="higpopulationRisk.container1.methodology"/></span>
                    <span class="fa fa-chevron-right " onclick="displayData('methodology2')"></span>
                </h6>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-7">
                    %{--<h1 class="isEditable"><g:editMessage code="populationRisk.container1.table1.head"/></h1>--}%

                    %{--<div style="background-color: #E4E4E4;width: 426px;">--}%
                    <div id="barGraph1" class="border-l-graph"></div>

                    <div id="barAxis"></div>
                    %{--</div>--}%




                <div class="clearfix"></div>

               %{-- <div>
                    <a class="see-dtl-utilization">
                        <span class=" isEditable"><g:editMessage code="populationRisk.detailedUtilization"/></span>

                    </a>
                </div>--}%
            </div>

            <%
                //        split page
            %>
            <div class="col-md-5">
                <br>
                %{--<div  class=" z5-table put-border">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                       <thead>
                       <th style="width:50%;;" class="text-center">$PMPY</th>
                       <th class="text-center">Benchmark $PMPY</th>
                       </thead>
                       <tbody>
                       <tr><td class="text-center">$XXX</td><td class="text-center">$YYY</td></tr>
                       <tr><td class="text-center">$XXX</td><td class="text-center">$YYY</td></tr>
                       <tr><td class="text-center">$XXX</td><td class="text-center">$YYY</td></tr>
                       <tr><td class="text-center">$XXX</td><td class="text-center">$YYY</td></tr>
                       <tr><td class="text-center">$XXX</td><td class="text-center">$YYY</td></tr>
                       <tr><td class="text-center">$XXX</td><td class="text-center">$YYY</td></tr>
                       <tr><td class="text-center">$XXX</td><td class="text-center">$YYY</td></tr>
                       </tbody>
                   </table>
                    </div>--}%

                %{--<p><span class=" isEditable"><g:editMessage code="populationRisk.leftbar2.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar2.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="#" class="btn-link-normal btn-blue btn-inline isEditable"> <g:editMessage code="populationRisk.evaluate"/></a>
                </div>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar2.list2"/></div>

                <div class="col-md-4 no-pad-r text-right ">
                <a href="#" class="btn-link-normal btn-blue btn-inline isEditable"> <g:editMessage code="populationRisk.evaluate"/></a>
                       </div>--}%

            </div>
            </div>

            <div class="clearfix"></div>
            <div id="recommendedCareForDiabetes">

            <div class="sep-border mb25 pt15"></div>

            <div class="clearfix"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="populationRisk.container2.table2.head"/></h1>
            </div>

            <div class="col-md-6 text-right">
                <a href="#" class="custom-tooltip  text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="populationRisk.explore.population"/></span><input type="hidden"
                                                                                value="populationRisk.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('recommendedCareForDiabetes')"
                   class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden"
                                                                                                          value="populationRisk.container3.table1.options2">
                </a>

                %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>--}%
            </div>

            <div class="col-md-12">
                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:editMessage code="higpopulationRisk.container1.methodology"/></span>
                        <span class="fa fa-chevron-right " onclick="displayData('methodology3')"></span>

                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="col-md-12">
                <div>
                %{--<h1 class="isEditable"><g:editMessage code="populationRisk.container1.table1.head"/></h1>--}%
                    <g:if test="${!isEditable}">

                        <div id="barGraph2">

                        %{--<g:render template="tableDataDetails"/>--}%


                </div>
                    </g:if>
                </div>

                <div class="clearfix"></div>

                %{--<div>
                    <a class="see-dtl-utilization">
                        <span class=" isEditable"><g:editMessage code="populationRisk.detailedUtilization"/></span>

                    </a>
                </div>--}%
            </div>
            </div>
            <%
                //        split page
            %>
            %{--<div class="col-md-5">
                --}%%{--<div class="text-right">
                    <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.explore.population"/></span><input type="hidden" value="populationRisk.container3.table1.options1"></a>
                    <a href="javascript:void(0)"  onclick="exportCsv('avoidableToTotalErRatio')" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>

                    --}%%{----}%%{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>--}%%{----}%%{--
                </div>--}%%{--

                <p><span class=" isEditable"><g:editMessage code="populationRisk.leftbar3.title"/></span></p>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar3.list1"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="#" class="btn-link-normal btn-blue btn-inline isEditable">
                        <g:editMessage code="populationRisk.evaluate"/></a></div>

                <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                        code="populationRisk.leftbar3.list2"/></div>

                <div class="col-md-4 no-pad-r text-right">
                    <a href="#" class="btn-link-normal btn-blue btn-inline isEditable">
                        <g:editMessage code="populationRisk.evaluate"/></a></div>

            </div>--}%

            <div class="clearfix"></div>

            <div class="sep-border mb25 pt15"></div>

            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="higpopulationRisk.container1.title1"/></h1>
            </div>

            <div class="col-md-6 text-right">
                <a href="#" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img
                        uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                        code="populationRisk.explore.population"/></span><input type="hidden"
                                                                                value="populationRisk.container3.table1.options1">
                </a>
                <a href="javascript:void(0)" onclick="exportCsv('newUseOfErByChronicMembers')"
                   class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden"
                                                                                                          value="populationRisk.container3.table1.options2">
                </a>

                %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>--}%
            </div>

            <div class="col-md-12">
                <h6 class="has-filter">
                    <span class="f-right">
                        <span class=" isEditable"><g:editMessage code="higpopulationRisk.container1.methodology"/></span>
                        <span class="fa fa-chevron-right " onclick="displayData('methodology4')"></span>

                    </span>
                </h6>

                <div class="clearfix"></div>
            </div>

            <div class="col-md-12">
                <div>
                    %{--<h1 class="isEditable"><g:editMessage code="populationRisk.container1.table1.head"/></h1>--}%
                <g:if test="${!isEditable}">


                    <div id="barGraph3">

                            %{--<g:render template="tableDataDetails2"/>--}%

                    </div></g:if>
                </div>

                <div class="clearfix"></div>

                %{--<div>
                    <a class="see-dtl-utilization">
                        <span class=" isEditable"><g:editMessage code="populationRisk.detailedUtilization"/></span>

                    </a>
                </div>--}%
            </div>

            <%
                //        split page
            %>
            %{-- <div class="col-md-5">
                 --}%%{--<div class="text-right">
                     <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.explore.population"/></span><input type="hidden" value="populationRisk.container3.table1.options1"></a>
                     <a href="javascript:void(0)"  onclick="exportCsv('avoidableToTotalErRatio')" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>

                     --}%%{----}%%{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>--}%%{----}%%{--
                 </div>--}%%{--

                 <p><span class=" isEditable"><g:editMessage code="populationRisk.leftbar4.title"/></span></p>

                 <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                         code="populationRisk.leftbar4.list1"/></div>

                 <div class="col-md-4 no-pad-r text-right">
                     <a href="#" class="btn-link-normal btn-blue btn-inline isEditable">
                         <g:editMessage code="populationRisk.evaluate"/></a></div>



             </div>--}%

            %{--last container--}%
            %{--last container--}%
            %{--last container--}%


            <div class="clearfix"></div>

            %{-- <div class="sep-border-dashed mb25 pt15"></div>
             <div class="col-md-6">
             </div>

             <div class="col-md-6 text-right">
                 <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img
                         uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage
                         code="populationRisk.explore.population"/></span><input type="hidden"
                                                                                 value="populationRisk.container3.table1.options1">
                 </a>
                 <a href="javascript:void(0)" onclick="exportCsv()"
                    class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                         class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden"
                                                                                                           value="populationRisk.container3.table1.options2">
                 </a>

                 --}%%{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>--}%%{--
             </div>
             <div class="col-md-12">
                 <h6 class="has-filter">
                     <span class="f-right">
                         <span class=" isEditable"><g:editMessage code="higpopulationRisk.container1.methodology"/>
                             <span class="fa fa-chevron-right "></span>
                         </span>

                     </span>
                 </h6>

                 <div class="clearfix"></div>
             </div>

             <div class="col-md-7">
                 <div class="bdr-right-dashed">
                     --}%%{--<h1 class="isEditable"><g:editMessage code="populationRisk.container1.table1.head"/></h1>--}%%{--
                     <div><span class=" isEditable"><g:editMessage
                             code="populationRisk.container4.table1.subhead"/></span>
                     </div>

                     <div id="barGraph3"></div>
                 </div>

                 <div class="clearfix"></div>

                 <div>
                     <a  class="see-dtl-utilization">
                         <span class=" isEditable"><g:editMessage code="populationRisk.detailedUtilization"/></span>

                     </a>
                 </div>
             </div>

             <%
                 //        split page
             %>
             <div class="col-md-5">
                 --}%%{--<div class="text-right">
                     <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.explore.population"/></span><input type="hidden" value="populationRisk.container3.table1.options1"></a>
                     <a href="javascript:void(0)"  onclick="exportCsv('avoidableToTotalErRatio')" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>

                     --}%%{----}%%{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="populationRisk.export.csv"/></span><input type="hidden" value="populationRisk.container3.table1.options2"></a>--}%%{----}%%{--
                 </div>--}%%{--

                 <p><span class=" isEditable"><g:editMessage code="populationRisk.leftbar5.title"/></span></p>

                 <div class="col-md-8 no-pad-l mb25 isEditable"><g:editMessage
                         code="populationRisk.leftbar5.list1"/></div>

                 <div class="col-md-4 no-pad-r text-right">
                     <a href="#" class="btn-link-normal btn-blue btn-inline isEditable">
                         <g:editMessage code="populationRisk.evaluate"/></a></div>





             </div>--}%

        </div>


        <%
            //        split page
        %>
        <div id="methodology1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="populationRisk.container1.methodology1"/></span>
                    </div>

                </div>
            </div>
        </div>

        <div id="methodology2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="populationRisk.container1.methodology2"/></span>
                    </div>
                </div>
            </div>
        </div>

        <div id="methodology3" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="populationRisk.container1.methodology3"/></span>
                    </div>
                </div>
            </div>
        </div>
    <div id="methodology4" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="populationRisk.container1.methodology4"/></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%
        //        some comment
    %>


    <div id="barDetails" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>

                    <div class="isEditable" id="barContent">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="display: none;" id="editContent">
        <li>
            <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                    <g:link controller="cms" action="populationRisk" title="Edit Content"
                            ><i
                            class="fa fa-edit"></i> Edit Content</g:link>
                </g:if><g:else>
                <g:link controller="populationRisk" action="index" title="View Changes" ><i
                        class="fa fa-eye"></i> View Changes</g:link>
            </g:else>
            </sec:ifAllGranted>
        </li>
    </div>

    <g:form action="exportCsv" name="exportForm" style="display: none;">

        <g:hiddenField name="exportReportingBasis"/>
        <g:hiddenField name="exportRange"/>
        <g:hiddenField name="exportIsExclude"/>
        <g:hiddenField name="exportModule"/>
        <g:hiddenField name="exportChronicTitle"/>
        <g:hiddenField name="exportChronicCondition"/>
    </g:form>

    <style>


    .hbar {
        /*mask: url(#mask-stripe)*/
        /*fill: url(#mask-stripe);*/

        fill: url(#pattern-stripe) !important;
    }

    .bar {
        fill: steelblue;
    }

    .lineColor {
        fill: none;
        stroke: #1c94c4;
        stroke-width: 1px;
        stroke-dasharray: 6 3
    }

    .dbar {
        fill: #034f96;
    }

    .x.axis path {
        /*display: none;*/
    }

    </style>


    <script>

        $("span.bulb-lists").find("ul").addClass("bulb-lists color-blue");
        $("span.bulb-lists").find("li").prepend("<i class='fa fa-lightbulb-o'></i>");
    </script>
    <script>


        function selectCondition(id,val){
            $("#chronicConditionValue").text(val)
            $("#chronicCondition").val(id)
//            $("#primary_nav_wrap ul li>ul").hide();
            $("#primary_nav_wrap ul ul").css('display',"none");
            refreshReport();
        }



        function refreshReport() {
            var reportingBasis, fromDate, range, isExclude,chronicCondition;
            reportingBasis = $("#reportingBasis1").val();
            fromDate = $("#fromDate1").val();
            isExclude = $("#includeOn").val();
            chronicCondition = $("#chronicCondition").val();

            range = $("#range1").val();
            var innerTitle=$("#chronicConditionValue").text().trim();
            chronicTitle=innerTitle;
            $(".chronic-title").html(innerTitle);
            $(".chrc_rm").css({backgroundColor:"",color:""});

            var $selectedChr=$(".chrc_"+chronicCondition);
            $selectedChr.css({backgroundColor:colorScale[chronicCondition],color:"#fff"});





            var arr=['1012','1008','1005','1002','1009'];
            var arr2=['1012','1008','1005','1002','1009','1016','1024','1017'];
            var arr3=['1004','1018','1016','1013','1007','1019'];


            jQuery.ajax({
                type: 'POST',
                data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude,chronicTitle:innerTitle,chronicCondition:chronicCondition, module: 'lineGraph1'},
                url: '<g:createLink controller="populationRisk" action="getScriptForPopulationRisks"/>',
                success: function (resp) {
                    render2Data(resp.topLineGraph, resp.max)

                    var value=resp.populationRiskContribution.toFixed(1).replace(/\.0+$/,'');
                    if(value=="0"){
                        $("#populationRiskContribution").html("");
                        $("#populationRiskContribution").hide();
                    }else{
                        $("#populationRiskContribution").html(innerTitle+" accounts for "+value+" % of your population risk.")
                        $("#populationRiskContribution").show()
                    }


                    if($.inArray(chronicCondition,arr3)==-1){
                        $("#removeBorder").addClass("bdr-right")
                        $("#projectedCosts").show()
                        render4Data(resp.dm, resp.yPercentage, resp.yTotal, resp.max,innerTitle);


                    }else{
//                        $("#removeBorder").removeClass("bdr-right")
//                        $("#projectedCosts").hide()
//                        $("#populationRiskContribution").hide()

                        $("#lineGraphRtitle").html("");
                                $("#lineGraphR").html('<div class="watermark"><div style="display: inline-block;text-transform: capitalize">' +
                        '<i class="fa fa-exclamation-triangle fa-4"></i>The prediction section is missing because MARA does not evaluate '+innerTitle+' </div>   </div>');
                    }

                    $(".chronic-title").html(innerTitle);
                    $(".lineLegends").show();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                },
                complete: function (XMLHttpRequest, textStatus) {
                }
            });

            if($.inArray(chronicCondition,arr)>=0){
                $("#topChronicCondition").show()
                jQuery.ajax({
                    type: 'POST',
                    data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude,chronicCondition:chronicCondition, module: 'barGraph1'},
                    url: '<g:createLink controller="populationRisk" action="getScriptForPopulationRisks"/>',
                    success: function (resp) {
                        renderStackedBars(resp)
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                    }
                });
            }else{
//                $("#topChronicCondition").hide()
                $("#barGraph1").hide()
                $("#barGraph1Info").html("")
                $("#barAxis").html('<div class="watermark"><div style="display: inline;text-transform: capitalize">' +
                '&nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> No members with comorbid conditions for '+innerTitle+'&nbsp;&nbsp;</div>   </div>');

            }

            if($.inArray(chronicCondition,arr)>=0){
                $("#recommendedCareForDiabetes").show()
            jQuery.ajax({
                type: 'POST',
                data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude,chronicCondition:chronicCondition, module: 'barGraph2'},
                url: '<g:createLink controller="populationRisk" action="getScriptForPopulationRisks"/>',
                success: function (resp) {
                    $("#barGraph2").html(resp)
                    $(".chronic-title").html(innerTitle);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                complete: function (XMLHttpRequest, textStatus) {
                }
            });}else{
//                $("#recommendedCareForDiabetes").hide();
                $("#barGraph2").html('<div class="watermark"><div style="display: inline;text-transform: capitalize">' +
                '&nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i> Care gaps are not applicable or defined for '+innerTitle+'&nbsp;&nbsp;</div>   </div>');
            }

            jQuery.ajax({
                type: 'POST',
                data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude,chronicCondition:chronicCondition, module: 'barGraph3'},
                url: '<g:createLink controller="populationRisk" action="getScriptForPopulationRisks"/>',
                success: function (resp) {
                    $("#barGraph3").html(resp)
                    $(".chronic-title").html(innerTitle);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                complete: function (XMLHttpRequest, textStatus) {
                }
            });


        }


        function exportCsv(module) {
            <g:if test="${!isEditable}">

            var reportingBasis, fromDate, range, include;
            reportingBasis = $("#reportingBasis1").val();
            include = $("#includeOn").val();
            range = $("#range1").val();

            var chronicCondition = $("#chronicCondition").val();
            var innerTitle=$("#chronicConditionValue").text().trim();

            $("#exportReportingBasis").val(reportingBasis);
            $("#exportChronicCondition").val(chronicCondition);
            $("#exportChronicTitle").val(innerTitle);
            $("#exportIsExclude").val(include);
            $("#exportRange").val(range);
            $("#exportModule").val(module);

            $("#exportForm").submit();
            </g:if>
        }


        <g:if test="${!isEditable}">
        var parseEndDate = d3.time.format("%Y-%m-%d").parse;
        %{--render2Data(${dataPh3}, ${getMax});--}%
        %{--render4Data(${lineGraphR}, ${yPercentage}, "${yTotal}", ${getMax});--}%
        </g:if>


        function render4Data(da, yPercentage, yT, max,innerTitle) {
            $("#lineGraphR").html("");

            /*var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
             main_width = 500 - main_margin.left - main_margin.right,
             main_height =300 - main_margin.top - main_margin.bottom;*/

            var main_margin = {top: 20, right: 120, bottom: 50, left: 60},
                    main_width = 540 - main_margin.left - main_margin.right,
                    main_height = 300 - main_margin.top - main_margin.bottom;
            var pharmacyClaims = da;
            //var benchMark = 3000;
            var memberMonths = da.value;
            var newVal;
            var data = [];
            var parseDate4 = d3.time.format("%Y-%m-%d").parse;


            $.each(pharmacyClaims, function (key, val) {
                newVal = new Object();
                newVal.time1 = parseDate4(val.year);
                newVal.actual = Math.round(val.actual)
                newVal.concurrentActual = Math.round(val.concurrentActual)
                data.push(newVal);
            });
            data.sort(custom_sort);
            var pmpyConcurrent=data[0].concurrentActual;

//            var percent=Math.round(((data[1].actual-data[0].actual)/data[0].actual)*100)

            var percent = d3.format(".2r")(yT)
            if (percent < 0) {
                $("#lineGraphRtitle").html(innerTitle+" related costs PMPY are likely to <strong>decrease by " + -(percent) + "%</strong> in the next 12 months")
            } else if (percent > 0) {
                $("#lineGraphRtitle").html(innerTitle+" related costs PMPY are likely to <strong>increase by " + percent + "%</strong> in the next 12 months")
            } else {
                $("#lineGraphRtitle").html(innerTitle+" related costs PMPY are likely to be <strong>same</strong> in the next 12 months")
            }


            var formatDate = bisectDate = d3.bisector(function (d) {
                        return d.time1;
                    }).left,
                    formatOutput0 = function (d) {
                        return d3.format("$,")(Math.round(d.actual))+" "+innerTitle +"";
                    }/*,formatOutput00 = function (d) {
                        return innerTitle +" members ";
                    }*/,formatOutput01 = function (d) {
                        return d3.format("$,")(Math.round(d.concurrentActual))+" "+innerTitle +"";
                    },
                    formatOutput1 = function (d) {
                        return "(" + Math.round(yPercentage) + " % of total spend projected) ";
                    };

            var main_x = d3.time.scale()
                    .range([0, main_width]);

            var main_y = d3.scale.linear()
                    .range([main_height, 0]);

            var main_xAxis = d3.svg.axis()
                    .scale(main_x)
                    .outerTickSize(0)
                    .ticks(d3.time.months, 1)
                    .tickFormat(d3.time.format('%b'))
                    .orient("bottom");

            /* var main_xAxis2 =d3.svg.axis()
             .scale(main_x)
             .ticks(d3.time.months,8)
             .tickFormat(d3.time.format("%Y %B"))
             .tickSize(5,0)
             .orient("bottom");*/


            var main_yAxisLeft = d3.svg.axis()
                    .scale(main_y)
                    .ticks(5)
                    .tickSize(-main_width)
                    .outerTickSize(0)
                    .orient("left");

            var main_line0 = d3.svg.line()
                    .x(function (d) {
                        return main_x(d.time1);
                    })
                    .y(function (d) {
                        return main_y(d.actual);
                    });

            var main_line01 = d3.svg.line()
                    .x(function (d) {
                        return main_x(d.time1);
                    })
                    .y(function (d) {
                        return main_y(d.concurrentActual);
                    });

            var svg = d3.select("#lineGraphR").append("svg")
                    .attr("width", main_width + main_margin.left + main_margin.right)
                    .attr("height", main_height + main_margin.top + main_margin.bottom);

            svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("width", main_width)
                    .attr("height", main_height);

            var main = svg.append("g")
                    .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");


            %{--d3.csv(paths, function(error, data) {
                data.forEach(function(d) {
                    d.time1 = parseDate(d.time1);
                    d.expected = +d.expected;
                    d.actual = +d.actual;
                });--}%


            data.sort(function (a, b) {
                return a.time1 - b.time1;
            });


            var value1 = Math.round(data[data.length - 1].actual);
            var value2 = Math.round(data[data.length - 1].expected);


            main_x.domain([data[0].time1, data[data.length - 1].time1]);
            var array_y0 = d3.extent(data, function (d) {
                return d.actual;
            });
//            main_y.domain([0,d3.max(array_y0)]);
            main_y.domain([0, max]);

            var main_xAxis2 = d3.svg.axis()
                    .scale(main_x)
//                    .tickFormat(d3.time.format("%b-%Y"))
                    .tickFormat(formatDates)
                    .tickValues(main_x.domain())
                    .orient("bottom");


            /*main.append("g")
             .attr("class", "x axis xaxisLeft")
             .attr("transform", "translate(0," + main_height + ")")
             .call(main_xAxis)
             .selectAll("path")
             .attr("stroke", "#ccc")*/

            main.append("g")
                    .attr("class", "x axis xaxisLeft")
                    .attr("transform", "translate(0," + (main_height) + ")")
                    .call(main_xAxis2)
                    .selectAll("path")
                    .attr("stroke", "#ccc");

            main.append("g")
                    .attr("class", "y axis yaxisLeft")
                    .call(main_yAxisLeft)
                    .selectAll("text")
                    .attr("x", -20)

            main.selectAll("line")
                    .attr("stroke", "#ccc");

            main.selectAll(".tick")
                    .attr("stroke-dasharray", "3 3")
                    .attr("opacity", ".5");

            /*var main_xAxis1 =d3.svg.axis()
             .scale(main_x)
             .tickValues([data[0].time1, data[data.length - 1].time1])
             .tickSize(0)
             //                    .tickFormat(d3.time.format("%b"))
             .tickFormat(function(d,i) { return dataset[i]; })
             .orient("bottom");*/

            /*main.append("g")
             .attr("class", "x axis xaxisLeft2")
             .attr("transform", "translate(0," + (main_height+18) + ")")
             .call(main_xAxis1);*/

            /*var doms=[];
             doms.push(main_x.domain()[0]);

             var main_xAxis = d3.svg.axis()
             .tickFormat(d3.time.format('%b'))
             .tickValues(doms)
             .orient("bottom");

             main.append("g")
             .attr("class", "x axis xaxisLeft2")
             .attr("transform", "translate(0," + (main_height+25) + ")")
             .call(main_xAxis);*/

            /*main.append("g")
             .attr("class", "y axis yaxisLeft")
             .call(main_yAxisLeft)
             .selectAll("path")
             .attr("stroke", "#ccc");*/

            main.selectAll("line")
                    .attr("stroke", "#ccc");
            /*
             main.selectAll(".tick")
             .attr("stroke-dasharray", "3 3")
             .attr("opacity", ".5");*/


            var path1 = main.append("path")
//                    .attr("clip-path", "url(#clip)")
                    .attr("class", "line line0")
                    .attr("d", main_line0(data))
                    .style("stroke", "red")
                    .style("fill", "none")
                    .style('stroke-width', '1.2px');

            if(pmpyConcurrent){
                var path2 = main.append("path")
                        .attr("class", "line line0")
                        .attr("d", main_line01(data))
                        .style("stroke", "steelblue")
                        .style("fill", "none")
                        .style('stroke-width', '1.2px');
            }




            var textLabel = main.append('g')
                    .classed('labels-group', true);

           /* textLabel.selectAll('.labelText5')
                    .data(data)
                    .enter()
                    .append('text')
                    .classed('labelText5', true)
                    .attr({
                        'x': function (d, i) {
                            return main_x(d.time1) +5;
                        },
                        'y': function (d, i) {
                            if((main_y(d.actual)-main_y(d.concurrentActual))<-33)
                                return main_y(d.actual) - 32;
                            else
                                return main_y(d.actual) - 52;
                        }
                    })
                    .text(function (d, i) {
                        return formatOutput0(d)
                    })*/

            var diff1=0;
            var diff2=0;
            textLabel.selectAll('.labelText1')
                    .data(data)
                    .enter()
                    .append('text')
                    .classed('labelText1', true)
                    .attr({
                        'x': function (d, i) {
                            if(i==1){
                                diff1= d.actual
                                return main_x(d.time1)-90 ;
                            }else{
                                diff2= d.actual
                                return main_x(d.time1)+5 ;
                            }
                        },
                        'y': function (d, i) {
                            if((main_y(d.actual)-main_y(d.concurrentActual))<-33)
                            return main_y(d.actual) - 22;
                            else
                            return main_y(d.actual) - 43;
                        }
                    })
                    .text(function (d, i) {
                        return formatOutput0(d)
                    })

            textLabel
                    .selectAll('.labelText2')
                    .data(data)
                    .enter()
                    .append('text')
                    .classed('labelText2', true)
                    .attr({
                        'x': function (d, i) {
                            if(i==1){
                                return main_x(d.time1)-90 ;
                            }else{
                                return main_x(d.time1)+5 ;
                            }

                        },
                        'y': function (d, i) {
                            if((main_y(d.actual)-main_y(d.concurrentActual))<-33)
                                return main_y(d.actual) - 12;
                            else
                                return main_y(d.actual) - 33;
                        }
                    })
                    .text(function (d, i) {
                        if (i == 0)   return "members cost total PMPY actual";
                        else  return "members cost total PMPY projected";
                    })


            /*textLabel
                    .selectAll('.labelTextDiff')
                    .data(data)
                    .enter()
                    .append('text').attr("text-anchor", "middle")
                    .classed('labelTextDiff', true)
                    .attr({
                        'x': function (d, i) {
                            if (i == 0){
                                return 150;
                            }
                        },
                        'y': function (d, i) {
                            if (i == 0)  return main_y(d.actual) + 45;
                        }
                    })
                    .text(function (d, i) {
                        if (i == 0) return "Members Difference is "+d3.format("$,")(Math.abs(diff2-diff1))+"";
                    })*/

//            var prefix = d3.formatPrefix(yT);
//            var amountYt="$"+prefix.scale(Math.round(yT)).toFixed(2)+" k";
//            $("#lineGraphRTest").html("<strong>"+yT+"</strong> Total ER spend projected <br><strong>"+Math.round(yPercentage)+" %</strong> of total spend projected");
//            $("#lineGraphRTest").html("<strong>"+yT+"</strong> Total ER spend projected");




            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y0")
                    .style("opacity", "0.6")
                    .attr("cx", function (d) {
                        return main_x(d.time1);
                    })
                    .attr("cy", function (d) {
                        return main_y(d.actual);
                    })
                    .attr("r",4);



            var diff3=0;
            var diff4=0;

            if(pmpyConcurrent) {

                main.append('g')
                        .selectAll('circle')
                        .data(data)
                        .enter()
                        .append('circle')
                        .attr("class", "y00")
                        .style("opacity", "0.6")
                        .attr("cx", function (d) {
                            return main_x(d.time1);
                        })
                        .attr("cy", function (d) {
                            return main_y(d.concurrentActual);
                        })
                        .attr("r", 4);


                textLabel.selectAll('.labelText3')
                        .data(data)
                        .enter()
                        .append('text')
                        .classed('labelText3', true)
                        .attr({
                            'x': function (d, i) {
                                if (i == 1) {
                                    return main_x(d.time1) - 90;
                                    diff3 = d.concurrentActual
                                } else {
                                    diff4 = d.concurrentActual;

                                    return main_x(d.time1) + 5;
                                }
                            },
                            'y': function (d, i) {
//                            if(d.actual> d.concurrentActual)
//                                return main_y(d.concurrentActual) + 14;
//                            else
                                return main_y(d.concurrentActual) - 20;
                            }
                        })
                        .text(function (d, i) {
                            return formatOutput01(d)
                        })

                textLabel
                        .selectAll('.labelText4')
                        .data(data)
                        .enter()
                        .append('text')
                        .classed('labelText4', true)
                        .attr({
                            'x': function (d, i) {
                                if (i == 1) {
                                    return main_x(d.time1) - 90;
                                } else {
                                    return main_x(d.time1) + 5;
                                }
                            },
                            'y': function (d, i) {
//                            if(d.actual<= d.concurrentActual)
//                                return main_y(d.actual) + 28;
//                            else
                                return main_y(d.concurrentActual) - 10;
                            }
                        })
                        .text(function (d, i) {
                            if (i == 0)   return "related cost PMPY actual";
                            else  return "related cost PMPY projected";
                        })

            }

            /*textLabel
                    .selectAll('.labelTextDiffs')
                    .data(data)
                    .enter()
                    .append('text').attr("text-anchor", "middle")
                    .classed('labelTextDiffs', true)
                    .attr({
                        'x': function (d, i) {
                            if (i == 0){
                                return 150;
                            }
                        },
                        'y': function (d, i) {
                            if (i == 0)  return main_y(d.concurrentActual) - 45;
                        }
                    })
                    .text(function (d, i) {
                        if (i == 0) return "Related Difference is " + d3.format("$,")(Math.abs(diff4 - diff3)) + "";
                    });*/


            /*textLabel
                    .selectAll('.labelText4')
                    .data(data)
                    .enter()
                    .append('text')
                    .classed('labelText4', true)
                    .attr({
                        'x': function (d, i) {
                            return main_x(d.time1) - 20;
                        },
                        'y': function (d, i) {
                            return main_y(d.concurrentActual) - 15;
                        }
                    })
                    .text(function (d, i) {
                        if (i == 0)   return " concurrent";
                        else  return " prospective";
                    })
*/

        }
        ;

        function render2Data(da, max) {
            $("#lineGraph1").html("");

            /*var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
             main_width = 500 - main_margin.left - main_margin.right,
             main_height =300 - main_margin.top - main_margin.bottom;*/

            var main_margin = {top: 20, right: 60, bottom: 50, left: 80},
                    main_width = 500 - main_margin.left - main_margin.right,
                    main_height = 300 - main_margin.top - main_margin.bottom;
            var pharmacyClaims = da;
            //var benchMark = 3000;
//            var memberMonths = da.value;
            var newVal;
            var data = [];
            var parseDate4 = d3.time.format("%Y-%m-%d").parse;


            $.each(pharmacyClaims, function (key, val) {

                newVal = new Object();
                newVal.time1 = parseDate4(val['reportingDates']['reportingTo'])
                newVal.time2 = parseDate4(val['reportingDates']['reportingFrom'])
//                newVal.time1=parseDate4(val['year'])

                newVal.actual = Math.round(val['pmpy'])
                newVal.membersDrill = val['members_drill']
                newVal.expected = Math.round(val['benchmark']) || 0;
                data.push(newVal);
            });

            var formatDate = d3.time.format("%d-%b-%y"),
                    parseDate = formatDate.parse,
                    bisectDate = d3.bisector(function (d) {
                        return d.time1;
                    }).left,
                    formatOutput0 = function (d) {
                        return d3.format("$,")(Math.round(d.actual)) + " PMPY actual";
                    },
                    formatOutput1 = function (d) {
                        return d3.format("$,")(Math.round(d.expected)) + " PMPY benchmark";
                    };

            var main_x = d3.time.scale()
                    .range([0, main_width]);

            var main_y = d3.scale.linear()
                    .range([main_height, 0]);


            var main_yAxisLeft = d3.svg.axis()
                    .scale(main_y)
                    .ticks(5)
                    .tickSize(-main_width)
                    .outerTickSize(0)
                    .orient("left");

            var main_line0 = d3.svg.line()
                //.interpolate("basis")
                    .x(function (d) {
                        return main_x(d.time1);
                    })
                    .y(function (d) {
                        return main_y(d.actual);
                    });

            var main_line1 = d3.svg.line()
                //.interpolate("basis")
                    .x(function (d) {
                        return main_x(d.time1);
                    })
                    .y(function (d) {
                        return main_y(d.expected);
                    });

            var svg = d3.select("#lineGraph1").append("svg")
                    .attr("width", main_width + main_margin.left + main_margin.right)
                    .attr("height", main_height + main_margin.top + main_margin.bottom);

            svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("width", main_width)
                    .attr("height", main_height);

            var main = svg.append("g")
                    .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");


            %{--d3.csv(paths, function(error, data) {
                data.forEach(function(d) {
                    d.time1 = parseDate(d.time1);
                    d.expected = +d.expected;
                    d.actual = +d.actual;
                });--}%


            data.sort(function (a, b) {
                return a.time1 - b.time1;
            });


            var value1 = Math.round(data[data.length - 1].actual);
            var value2 = Math.round(data[data.length - 1].expected);


            main_x.domain([data[0].time1, data[data.length - 1].time1]);
            var array_y0 = d3.extent(data, function (d) {
                return d.actual;
            });
            var array_y1 = d3.extent(data, function (d) {
                return d.expected;
            });
            array_y0 = array_y0.concat(array_y1);
//            main_y.domain([0,d3.max(array_y0)]);
            main_y.domain([0, max]);

//            var dateTicks=data.map(function(d){return (d3.time.format("%m %Y")(d.time1)+" to "+d3.time.format("%m %Y")(d.time2)))});
            var dateTicks = data.map(function (d) {
                return d.time1
            });

            /*main.append("g")
             .attr("class", "x axis xaxisLeft")
             .attr("transform", "translate(0," + main_height + ")")
             .call(main_xAxis);*/

            var main_xAxis2 = d3.svg.axis()
                    .scale(main_x)
//                    .ticks(d3.time.years, 1)
//                    .ticks(d3.time.month, 9)
//                    .tickValues(data.time1)
                    .tickValues(dateTicks)
//                    .tickFormat(d3.time.format("%b-%Y"))
                    .tickFormat(formatDates)
//                    .tickFormat(function(d,i) { return (d3.time.format("%b %Y")(dateTicks[i].setFullYear(dateTicks[i].getFullYear() - 1)).toString()+" to "+d3.time.format("%b %Y")(dateTicks[i]).toString()); })
                    .tickSize(5, 0)
                    .orient("bottom");


            main.append("g")
                    .attr("class", "x axis xaxisLeft")
                    .attr("transform", "translate(0," + (main_height) + ")")
                    .call(main_xAxis2);


            /*var main_xAxis1 =d3.svg.axis()
             .scale(main_x)
             .tickValues([data[data.length - 1].time1])
             .tickSize(0)
             //                    .tickFormat(d3.time.format("%b"))
             .tickFormat(function(d,i) { return dataset[i]; })
             .orient("bottom");*/

            /* main.append("g")
             .attr("class", "x axis xaxisLeft2")
             .attr("transform", "translate(0," + (main_height+18) + ")")
             .call(main_xAxis1);*/


            %{--.append("text")
            .attr("class", "text_x")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (main_width/2) +",30)");  // centre below axis
        //.text("TIME");--}%

            main.append("g")
                    .attr("class", "y axis yaxisLeft")
                    .call(main_yAxisLeft)
                    .selectAll("text")
                    .attr("x", -20)

            main.selectAll("line")
                    .attr("stroke", "#ccc");

            main.selectAll(".tick")
                    .attr("stroke-dasharray", "3 3")
                    .attr("opacity", ".5");

            main.append("text")
                    .attr("class", "text_y")
                    .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                    .attr("transform", "translate(" + (-70) + "," + (main_height / 2) + ")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
                    .text("Costs ($ PMPY)");


            var path1 = main.append("path")
//                    .attr("clip-path", "url(#clip)")
                    .attr("class", "line line0")
                    .attr("d", main_line0(data))
                    .style("stroke", "red")
                    .style("fill", "none")
                    .style('stroke-width', '1.2px');


            var path2 = main.append("path")
//                    .attr("clip-path", "url(#clip)")
                    .attr("class", "line line1")
                    .attr("d", main_line1(data))
                    .style('stroke', 'gray')
                    .style("fill", "none")
                    .style('stroke-width', '1.2px');


            var totalLength = path1.node().getTotalLength();

            path1
                    .attr("stroke-dasharray", totalLength + " " + totalLength)
                    .attr("stroke-dashoffset", totalLength)
                    .transition()
                    .duration(600)
                    .ease("linear")
                    .attr("stroke-dashoffset", 0)
                    .each('end', function () {
                        //alert(value1);
//                        $("#actualValue").html("<span class='label-value'>$"+value1+"</span><span class='label-unit'>/1000</span><span class='label-title'>actual</span>");
                    });

            path2
                    .attr("stroke-dasharray", totalLength + " " + totalLength)
                    .attr("stroke-dashoffset", totalLength)
                    .transition()
                    .duration(600)
                    .ease("linear")
                    .attr("stroke-dashoffset", 0)
                    .each('end', function () {
//                        $("#expectedValue").html("<span class='label-value'>$"+value2+"</span><span class='label-unit'>/1000</span><span class='label-title'>benchmark</span>");
//                        $(".graph1-label").fadeIn();
                    });


            var focus = main.append("g")
                    .attr("class", "focus")
                    .style("display", "block");

            focus.append("line")
                    .attr("class", "x")
                    .attr("y1", 0);

            focus.append("circle")
                    .attr("class", "y0")
                    .attr("stroke", "black")
                    .attr("r", 4);

            focus.append("text")
                    .attr("class", "y0")
                    .attr("dy", "-1em");

            focus.append("circle")
                    .attr("class", "y1")
                    .attr("stroke", "black")
                    .attr("r", 4);

            focus.append("text")
                    .attr("class", "y1")
                    .attr("dy", "-1em");

            focus.style("visibility", "hidden");


            main.append("rect")
                    .attr("class", "overlay")
                    .attr("fill", "none")
                    .attr("pointer-events", "all")
                    .attr("cursor", "default")
                    .attr("width", main_width)
                    .attr("height", main_height)
                    .on("mouseover", function () {
                        focus.style("display", null);
                    })
                    .on("mouseout", function () {
                        div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        focus.style("visibility", "hidden");

                    })
                    .on("mousemove", mousemove);

            main.on("mouseover", function () {
                focus.style("display", null);
            }).on("mouseout", function () {
                div.transition()
                        .duration(500)
                        .style("opacity", 0);
                focus.style("display", "none");
            }).on("mousemove", mousemove);

            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y1")
                    .attr("cx", function (d) {
                        return main_x(d.time1);
                    })
                    .attr("cy", function (d) {
                        return main_y(d.expected);
                    })
                    .attr("r", 4);

            var main_radius = d3.scale.linear().range([4, 15])
            main_radius.domain(d3.extent(data, function (d) {
                return d.actual;
            }));

            var outerCircle=main.append("g").attr("class", "outerCircle");

            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append("svg:a")
                    .attr("data-href", function(d,i){
                        if(d.actual>0)
                            return createZ5Link(d.membersDrill);
                    })
                    .attr("onclick", "postRequestForGet(this);return false;")
                    .append('circle')
                    .attr("class", function (d, i) {
                        return "y0 " + "circle" + i;
                    })
                    .attr("cx", function (d) {
                        return main_x(d.time1);
                    })
                    .attr("cy", function (d) {
                        return main_y(d.actual);
                    })
                    .attr("r", 4)
                    .on("mouseenter", function(d,i) {
                        d3.select(this).attr("r", 10)
                        outerCircle.append("circle").attr({
                            cx: main_x(d.time1),
                            cy: main_y(d.actual),
                            r: 12,
                            fill: "none",
                            shapeRendering:"optimizeSpeed",
                            stroke:"red",
                            strokeWidth:"2px",
                            class:"outer-circle"
                        });

                    }).on("mouseout", function(d) {
                        d3.select(this).attr("r", 4)
                        d3.select("g.outerCircle").remove();
                        outerCircle=main.append("g").attr("class", "outerCircle");
                    })

            function mousemove() {
                focus.style("visibility", "visible");
                var x0 = main_x.invert(d3.mouse(this)[0]),
                        i = bisectDate(data, x0, 1),
                        d0 = data[i - 1],
                        d1 = data[i],
                        d = x0 - d0.time1 > d1.time1 - x0 ? d1 : d0;
//                focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")").attr("r", main_radius(d.actual));
//                focus.select("circle.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")").attr("r", main_radius(d.expected));


                focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");
                focus.select("text.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");//.text(formatOutput0(d));
                focus.select("circle.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");
                focus.select("text.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");//.text(formatOutput1(d));
                focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected > d.actual ? d.expected : d.actual) + ")").attr("y2", main_height - main_y(d.expected > d.actual ? d.expected : d.actual));
                //focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));

                div.transition()
                        .duration(100)
                        .style("opacity", .9);
                div.html("<span class='formatOutput0'>" + formatOutput0(d) + "</span><br><br><span class='formatOutput1'>" + formatOutput1(d) + "</span>")
                        .style("left", (d3.event.pageX + 30) + "px")
                        .style("top", (d3.event.pageY - 120) + "px");

                //focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.actual) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.expected) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(0, " + main_y(d.expected) + ")").attr("x1", main_x(d.time1));
            }

        }
        ;
        var div = d3.select(".contentWrapper").append("div")
                .attr("class", "tooltip")
                .style("opacity", 0);

        var div2 = d3.select(".contentWrapper").append("div")
                .attr("class", "tooltip2")
                .style("opacity", 0);
        /*function formatDates(d) {
            var oldDate = new Date(d);
            var newDate2 = oldDate;
            newDate2.setFullYear(newDate2.getFullYear() - 1);
            newDate2.setDate(newDate2.getDate() + 1);
            var dateString = (d3.time.format("%b %Y")(newDate2).toString() + " - " + d3.time.format("%b %Y")(d).toString())
            return dateString
        }*/

        function formatDates(d) {
            var oldDate=new Date(d);
            var newDate2=oldDate;
            newDate2.setFullYear(newDate2.getFullYear() - 1);
            var leapCondition=false;

            if((newDate2.getFullYear() % 400 == 0) || ((newDate2.getFullYear() % 4 == 0) && (newDate2.getFullYear() % 100 != 0))){
                leapCondition=true;
            }


            if((newDate2.getMonth()+1)==2 && leapCondition){
                leapCondition=true;
            }else{
                leapCondition=false;
            }

            if(leapCondition){
                newDate2.setDate(newDate2.getDate() +2);

            }else{
                newDate2.setDate(newDate2.getDate() +1);
            }

            var dateString= (d3.time.format("%b %Y")(newDate2).toString()+" - "+d3.time.format("%b %Y")(d).toString())
            return dateString;
        }


        function custom_sort(a, b) {
            return a.time1 - b.time1;
        }

        function renderStackedBars(bars) {

            $("#barGraph1").show()
            $("#barGraph1").html("")
            $("#barAxis").html("")

            var claims = bars.chronicMergedWithOtherConditions;
            //var benchMark = 3000;
//            var memberMonths = da.value;
            var newVal;
            var datas = [];
            var dataB = [];
            var parseDate4 = d3.time.format("%Y-%m-%d").parse;
//            var colors = ['#405F83','#416545','#4D7069','#6E9985','#7EBC89','#0283AF','#79BCBF','#99C19E'];
            /*var colorScale ={
             "pa chronic conditions":"#3366cc",
             "affective psychosis":"#dc3912",
             "asthma":"#ff9900",
             "atrial fibrillation":"#109618",
             "blood disorders":"#990099",
             "cad":"#0099c6",
             "copd":"#dd4477",
             "cancer":"#66aa00",
             "chronic pain":"#b82e2e",
             "congestive heart failure":"#316395",
             "demyelinating diseases":"#994499",
             "depression":"#22aa99",
             "diabetes":"#aaaa11",
             "esrd":"#6633cc",
             "eating disorders":"#e67300",
             "hiv/aids":"#8b0707",
             "hyperlipidemia":"#651067",
             "hypertension":"#329262",
             "immune disorders":"#5574a6",
             "inflammatory bowel disease":"#3b3eac",
             "liver diseases":'#1f77b4',
             "morbid obesity":'#ff7f0e',
             "osteoarthritis":'#ff7f0e',
             "peripheral vascular disease":'#2ca02c',
             "rheumatoid arthritis":'#2ca000',
             "only":"blue"
             };*/

//            var colors = ['red','yellow','blue','green','grey','black','#79BCBF','#99C19E'];
            var xPercent = 0, life = 0, yPercent = 0, appendAnd;

            yPercent = parseFloat(bars.metaInfo['percentOfHavingOneOrMoreOtherConditions']).toFixed(2)
            var title = bars.title;


            $.each(claims, function (k, v) {
                newVal = new Object();
                newVal.percent = v['percentOfPopulation'];
                newVal.population = v['population'];
                newVal.oName = v['otherConditionCode'];
                newVal.membersDrill = v['members_drill'];


                if (v['otherConditionDescription'] == v['rootCondition']) {
                    newVal.name = "All "+v['otherConditionDescription'];
                    xPercent = parseFloat(v['percentOfPopulation']).toFixed(2)
                    life = v['population']
                }
                else {
                    appendAnd = v['otherConditionDescription'] == "Only" ? " " : " & "
                    newVal.name = v['rootCondition'] + "" + appendAnd + "" + v['otherConditionDescription'];
                }
                if(newVal.oName=="Only")
                    newVal.oName="0000"

                datas.push(newVal);
            });

            datas.sort(function (a, b) {
                return d3.descending(a.percent, b.percent);
            });

            /*$.each(dataDesc,function(i,d){
             datas.push(d)
             })*/
            $("#barGraph1Info").html("<strong>" + yPercent + "%  </strong> of "+chronicTitle+" have one or more co-morbid conditions" );
            /*", resulting as much as <strong> X % </strong>higher costs per members with only "+chronicTitle*/


            /*var colors = ['#0066AE','#074285','#00187B','#285964','#405F83','#416545','#4D7069','#6E9985','#7EBC89','#0283AF','#79BCBF','#99C19E'];

             var grid = d3.range(10).map(function(i){
             return {'x1':0,'y1':0,'x2':0,'y2':420};
             });

             var tickVals = grid.map(function(d,i){
             if(i>0){ return i*10; }
             else if(i===0){ return "100";}
             });



             var xscale = d3.scale.linear()
             .domain([0,100])
             .range([0,420]);

             */
            /* var xscale = d3.scale.linear()
             .domain([0, d3.max(datas.map(function(d){return d.percent;}))])
             .range([0, 420]);*/
            /*

             var yscale = d3.scale.linear()
             .domain([0,datas.length])
             .range([0,420]);

             var colorScale = d3.scale.quantize()
             .domain([0,datas.length])
             .range(colors);

             var canvas = d3.select('#barGraph1')
             .append('svg')
             .attr({'width':600,'height':500});

             */
            /*var grids = canvas.append('g')
             .attr('id','grid')
             .attr('transform','translate(150,10)')
             .selectAll('line')
             .data(grid)
             .enter()
             .append('line')
             .attr({'x1':function(d,i){ return i*30; },
             'y1':function(d){ return d.y1; },
             'x2':function(d,i){ return i*30; },
             'y2':function(d){ return d.y2; }
             })
             .style({'stroke':'#adadad','stroke-width':'1px'});*/
            /*

             var	xAxis = d3.svg.axis();
             xAxis
             .orient('bottom')
             .scale(xscale)
             .tickValues(tickVals);

             var	yAxis = d3.svg.axis();
             yAxis
             .orient('left')
             .scale(yscale)
             .tickSize(2)
             .tickFormat(function(d,i){ return datas[i]["name"]; })
             .tickValues(d3.range(datas.length));

             var y_xis = canvas.append('g')
             .attr("transform", "translate(100,0)")
             .attr('id','yaxis')
             .call(yAxis);

             var x_xis = canvas.append('g')
             .attr("transform", "translate(100,420)")
             .attr('id','xaxis')
             .call(xAxis);

             var chart = canvas.append('g')
             .attr("transform", "translate(100,0)")
             .attr('id','bars')
             .selectAll('rect')
             //                    .data( datas.map(function(d){return d.percent;}))
             .data(datas.sort(function(a, b) { return (a.percent, b.percent); }))
             .enter()
             .append('rect')
             .attr('height',19)
             .attr({'x':0,'y':function(d,i){ return yscale(i)+19; }})
             .style('fill',function(d,i){ return colorScale(i); })
             .attr("width", function(d) {return xscale(d); });
             */
            /*.attr('width',function(d){ return 0; })
             .transition()
             .duration(1000)
             .attr("width", function(d) {return xscale(d); });*/
            /*
             */
            /* var colorScale = d3.scale.quantize()
             .domain([0,datas.length])
             .range(colors);*/


            var x1 = d3.scale.linear()
                    .domain([0, d3.max(datas.map(function (d) {
                        return d.percent;
                    })) * 1.3])
                    .range([0, 420]);


            var datum = datas;


            var divs = d3.select("#barGraph1")
                    .selectAll("div")
//                    .data(datas)
//                    .data(datas.sort(function(a, b) { return b.percent - a.percent; }))
                    .data(datum)
                    .enter()
                    .append("a")
                    .attr("data-href", function(d,i){
                        if(d.population>0)
                        return createZ5Link(d.membersDrill);
                    })
                    .attr("onclick", "postRequestForGet(this);return false;")
                    .append("div")
                    .style('cursor','pointer')
                    .on('mousemove', function (d, i) {
                        var color=splitColors(colorScale[d.oName])
                        div2.transition()
                                .duration(100)
                                .style("opacity", 1)
                                .style("color", "black")
                                .style("background", "white")
                        div2.html("<span class='formatOutput22'>Population Size: <strong>" + d.population+ "</strong></span>" +
                        "<br><span class='formatOutput22'>Population %: <strong>" + d.percent.toFixed(1).replace(/\.0+$/,'') + "</strong></span>")
                                .style("left", (d3.event.pageX + 10) + "px")
                                .style("top", (d3.event.pageY -100) + "px");
                    })
                    .on("mouseout", function (d,i) {
                        div2.transition()
                                .duration(500)
                                .style("opacity", 0);
                        $(this).css("background-color",colorScale[d.oName])
                    }).on("mouseenter", function (d,i) {
                        var color=splitColors(colorScale[d.oName])
                        $(this).css("background-color",color)
                    })


            divs.style("width", function (d) {
                return 0 + "px";
            })
                    .style('background-color', function (d, i) {
                        return colorScale[d.oName];
                    })
                    .transition()
                    .duration(2000)
                    .style("width", function (d) {
                        return x1(d.percent) + "px";
                    })
                    .text(function (d) {
                        return "  " + d.name.toUpperCase();
                    });


            var xAxis = d3.svg.axis();
            xAxis
                    .orient('bottom')
                    .scale(x1)
                    .tickFormat(function (d, i) {
                        return d;
                    })

            var xAxis2 = d3.svg.axis();
            xAxis2
                    .orient('bottom')
                    .tickSize(0)
                    .tickFormat("% of Population with "+chronicTitle)

            var canvas = d3.select('#barAxis')
                    .append('svg')
                    .attr({'width': 420, 'height': 45});

            var x_xis = canvas.append('g')
                    .attr("class", "x axis")
                    .attr("transform", "translate(5,0)")
                    .attr('id', 'xaxis')
                    .call(xAxis);

            canvas.append("text")
                    .attr("class", "text_y")
                    .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
                    .attr("transform", "translate(" + 200 + "," + (40 ) + ")")  // text is drawn off the screen top left, move down and out and rotate
                    .text("% of Population with "+chronicTitle);


            /* canvas.append('g')
             .attr("class", "x axis")
             .attr("transform", "translate(200,18)")
             .attr('id','xaxis')
             .call(xAxis2);*/


        }

    </script>
    <style>
    #barGraph1 div {
        font: 10px sans-serif;
        font-weight: bold;
        /*background-color: steelblue;*/
        text-align: left;
        height: 40px;
        /*padding: 9px;*/
        line-height: 40px;;
        margin: 6px;
        /*white-space: nowrap;*/
        white-space: pre;
        cursor: default;
        text-shadow: 0px 0px 4px #fff;
        /*text-shadow: #fff 0.2em .2em 0.3em;*/
        color: #000;

    }

    </style>


    <script language="javascript" type="text/javascript"
            src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
    <script>
        $(document).ready(function () {
            var chronicTitle;
            <g:if test="${!isEditable}">
            refreshReport();
            </g:if>
           /* var $table = $('table.z5-fixed-header');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.z5-table');
                }
            });*/
        });

        /*showDropdown = function (element) {
            var event;
            event = document.createEvent('MouseEvents');
//            event.initMouseEvent('mousedown', true, true, window);
//            event.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 1, 1, false, false, false, false, 0, null);
            event.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 1, 1, false, false, false, false, 0, null);
            element.dispatchEvent(event);

            *//*var evObj =document.createEvent('MouseEvents');
            evObj.initMouseEvent('mousedown', true, true, window);
            element.dispatchEvent(evObj);*//*
        };

        window.runThis = function () {
            var dropdown = document.getElementById('chronicCondition');
            showDropdown(dropdown);
        };*/

        /*window.runThis = function () {
            var element = $("#chronicCondition")[0],
                    worked = false;
            if(document.createEvent) { // all browsers
                var e = document.createEvent("MouseEvents");
                e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                worked = element.dispatchEvent(e);
            } else if (element.fireEvent) { // ie
                worked = element.fireEvent("onmousedown");
            }

            *//*if (!worked) { // unknown browser / error
                alert("Please use our application in Chrome");
            }*//*


        }*/




    </script>
    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">
</body>

</html>