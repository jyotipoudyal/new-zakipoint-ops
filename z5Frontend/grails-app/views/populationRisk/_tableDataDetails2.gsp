<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="mb10"><span
        class=" isEditable">Spend for use of ER by members with
    <span class="chronic-title">{{chrc}}</span>
<strong>
    <g:if test="${percent<0}">
        decreased by
        <g:formatNumber number="${-1*percent}" maxFractionDigits="1"/> %

    </g:if><g:else>
        increased by <g:formatNumber number="${percent}" maxFractionDigits="1"/> %

    </g:else>

</strong>
    in the past 3 years.</span>
</div>
<div  class=" z5-table put-border">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id="useOfErByChronicMembers">
    <thead>
    <tr>
        <util:remoteSortableColumn  class="${defaultSort}"  property="year" title="Year" update="barGraph3" action="sortResult" params="[module:'useOfErByChronicMembers',chronicCode:chronicCode]" />
        <util:remoteSortableColumn  class="text-center"  property="member" title="Chronic Condition Member Count" update="barGraph3" action="sortResult" params="[module:'useOfErByChronicMembers',chronicCode:chronicCode]" />
        <util:remoteSortableColumn  class="text-center"  property="erVisitor" title="Er Visited Chronic Condition Member Count" update="barGraph3" action="sortResult" params="[module:'useOfErByChronicMembers',chronicCode:chronicCode]" />
        <util:remoteSortableColumn   class="text-center "  property="erCost" title="Total ER Cost" update="barGraph3" action="sortResult" params="[module:'useOfErByChronicMembers',chronicCode:chronicCode]" />
        <util:remoteSortableColumn  class="text-center "  property="percentOfnonErVisitor" title="No Visit" update="barGraph3" action="sortResult" params="[module:'useOfErByChronicMembers',chronicCode:chronicCode]" />
        <util:remoteSortableColumn   class="text-center"  property="percentOfoneTimeVisitor" title="One Visit" update="barGraph3" action="sortResult" params="[module:'useOfErByChronicMembers',chronicCode:chronicCode]" />
        <util:remoteSortableColumn   class="text-center"  property="percentOftwoOrMoreTimesVisitor" title="Two Or More Visits" update="barGraph3" action="sortResult" params="[module:'useOfErByChronicMembers',chronicCode:chronicCode]" />
    </tr>
    </thead>
    <tbody>
    <g:each in="${useOfErByChronicMembers}" var="val">

        <tr>
            <td><g:formatDate format="yyyy" date="${val.year}"/>
            </td>
            <td  class="text-right">
                    ${val?.member}
               </td>
            <td  class="text-right">

                <g:z5Link disabledLink="${(val?.erVisitor)<=0?'true':'false'}" controller="memberSearch" action="searchResult" params="${val?.members_drill}">
                    ${val?.erVisitor}</g:z5Link>
               </td>
            <td class="text-right">
                <g:formatZ5Currency number="${val?.erCost}" type="number" maxFractionDigits="2" />

            </td>
            <td class="text-right"><g:formatNumber number="${val?.percentOfnonErVisitor}" type="number" maxFractionDigits="0" />%</td>
            <td class="text-right"><g:formatNumber number="${val?.percentOfoneTimeVisitor}" type="number" maxFractionDigits="0" />%</td>
            <td  class="text-right">
                <g:formatNumber number="${val?.percentOftwoOrMoreTimesVisitor}" type="number" maxFractionDigits="0" />%
            </td>

        </tr>
    </g:each>
    </tbody>
</table>
    <script>
        /*$(document).ready(function () {
            var $table = $('table#useOfErByChronicMembers');
            $table.floatThead({
                scrollContainer: function ($table) {
                    return $table.closest('.z5-table');
                }
            });

            chronicTitle=$("#chronicCondition option:selected").text();
            $(".chronic-title").html(chronicTitle);
        });*/
    </script>
</div>







