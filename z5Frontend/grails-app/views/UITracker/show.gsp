
<%@ page import="com.deerwalk.z5.UITracker" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'UITracker.label', default: 'UITracker')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-UITracker" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-UITracker" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list UITracker">
			
				<g:if test="${UITrackerInstance?.content}">
				<li class="fieldcontain">
					<span id="content-label" class="property-label"><g:message code="UITracker.content.label" default="Content" /></span>
					
						<span class="property-value" aria-labelledby="content-label"><g:fieldValue bean="${UITrackerInstance}" field="content"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${UITrackerInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="UITracker.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${UITrackerInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${UITrackerInstance?.lastUpdatedBy}">
				<li class="fieldcontain">
					<span id="lastUpdatedBy-label" class="property-label"><g:message code="UITracker.lastUpdatedBy.label" default="Last Updated By" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdatedBy-label"><g:fieldValue bean="${UITrackerInstance}" field="lastUpdatedBy"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${UITrackerInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="UITracker.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${UITrackerInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${UITrackerInstance?.prevContent}">
				<li class="fieldcontain">
					<span id="prevContent-label" class="property-label"><g:message code="UITracker.prevContent.label" default="Prev Content" /></span>
					
						<span class="property-value" aria-labelledby="prevContent-label"><g:fieldValue bean="${UITrackerInstance}" field="prevContent"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${UITrackerInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="UITracker.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${UITrackerInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${UITrackerInstance?.id}" />
					<g:link class="edit" action="edit" id="${UITrackerInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
