<%@ page import="com.deerwalk.z5.UITracker" %>



<div class="fieldcontain ${hasErrors(bean: UITrackerInstance, field: 'content', 'error')} ">
	<label for="content">
		<g:message code="UITracker.content.label" default="Content" />
		
	</label>
	<g:textField name="content" value="${UITrackerInstance?.content}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: UITrackerInstance, field: 'lastUpdatedBy', 'error')} ">
	<label for="lastUpdatedBy">
		<g:message code="UITracker.lastUpdatedBy.label" default="Last Updated By" />
		
	</label>
	<g:field name="lastUpdatedBy" type="number" value="${UITrackerInstance.lastUpdatedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: UITrackerInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="UITracker.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${UITrackerInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: UITrackerInstance, field: 'prevContent', 'error')} ">
	<label for="prevContent">
		<g:message code="UITracker.prevContent.label" default="Prev Content" />
		
	</label>
	<g:textField name="prevContent" value="${UITrackerInstance?.prevContent}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: UITrackerInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="UITracker.url.label" default="Url" />
		
	</label>
	<g:textField name="url" value="${UITrackerInstance?.url}"/>
</div>

