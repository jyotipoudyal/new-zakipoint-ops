
<%@ page import="com.deerwalk.z5.UITracker" %>
<!DOCTYPE html>
<html>
	<head>
	<g:if test="${isEditable}">
		<meta name="layout" content="mainCms">
	</g:if><g:else>
		<meta name="layout" content="main">
	</g:else>
		<g:set var="entityName" value="${message(code: 'UITracker.label', default: 'UITracker')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
	<div class="contentWrapper">
	<div class="container">
		<div id="chart"></div>
		<g:hiddenField name="showReportingDetails" value="false"/>
		<div class="col-md-12" style="display: none;" id="editContent">
		<sec:ifAllGranted roles="ROLE_Z5ADMIN">
			 <g:if test="${!isEditable}">
				<g:link controller="cms" action="uiTracker" title="Edit Content"><i class="fa fa-edit"></i> Edit Content</g:link>
			</g:if><g:else>
				<g:link controller="UITracker" action="index" title="View Changes" ><i class="fa fa-eye"></i> View Changes</g:link>
			</g:else>

		</sec:ifAllGranted>
	</div>

		<div class="col-md-12">


		<div id="list-UITracker" class="content scaffold-list" role="main">
			<h3><span class=" isEditable" ><g:editMessage code="uiTracking.top.title" /></span></h3>
			%{--<g:link action="refrehRequestMap">Refresh</g:link>--}%
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

    <div class="z5-table" style="min-height: 200px;max-height: 600px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%"  class="z5-fixed-header">
				<thead>
					<tr>
						<th>S.N</th>
						<th><span class=" isEditable" ><g:editMessage code="uiTracking.table1.head1" /></span></th>
						<th><span class=" isEditable" ><g:editMessage code="uiTracking.table1.head2" /></span></th>
						<th><span class=" isEditable" ><g:editMessage code="uiTracking.table1.head3" /></span></th>
						<th><span class=" isEditable" ><g:editMessage code="uiTracking.table1.head4" /></span></th>
						<th><span class=" isEditable" ><g:editMessage code="uiTracking.table1.head5" /></span></th>
						<th><span class=" isEditable" ><g:editMessage code="uiTracking.table1.head6" /></span></th>
						%{--
						<g:sortableColumn property="url" title="${message(code: 'UITracker.url.label', default: 'Path')}" />
						<g:sortableColumn property="name" title="${message(code: 'UITracker.name.label', default: 'Key')}" />
						<g:sortableColumn property="content" title="${message(code: 'UITracker.content.label', default: 'Current Content')}" />
						<g:sortableColumn property="prevContent" title="${message(code: 'UITracker.prevContent.label', default: 'Prev Content')}" />
						<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'UITracker.lastUpdatedBy.label', default: 'Updated By')}" />
						<g:sortableColumn property="dateCreated" title="${message(code: 'UITracker.dateCreated.label', default: 'Updated On')}" />--}%
					</tr>
				</thead>
		<g:if test="${!isEditable}">
				<tbody>
				<g:each in="${uit}" status="i" var="uiObj">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td>${uiObj.id}</td>
						<td>${fieldValue(bean: uiObj, field: "url")}</td>
						<td>${fieldValue(bean: uiObj, field: "name")}</td>
						<td>

							%{--${fieldValue(bean: UITrackerInstance, field: "content")}--}%
							<span id="current_${uiObj.id}">
							${(uiObj.content.toString().length()>30)?(uiObj.content.toString().substring(0,30).encodeAsHTML()+"<a href='javascript:void(0)' title='View more' onclick='showCurr(${uiObj.id})'><i> ..[View More]</i></a>"):uiObj.content}
							<div id="curr_${uiObj.id}" style="display: none">
							${uiObj.content.toString()}
							</div>
							</span>
						</td>
						<td>
							%{--${(UITrackerInstanceList.prevContent.toString().length()>20)?(UITrackerInstanceList.prevContent.toString().substring(0,20)+"..."):UITrackerInstanceList.prevContent}--}%

							%{--${fieldValue(bean: UITrackerInstance, field: "prevContent")}--}%

							<span id="previous_${uiObj.id}">
								${(uiObj.prevContent.toString().length()>30)?(uiObj.prevContent.toString().substring(0,30).encodeAsHTML()+"<a href='javascript:void(0)' title='View more' onclick='showPrev(${uiObj.id})'><i> ..[View More]</i></a>"):uiObj.prevContent}
								<div id="prev_${uiObj.id}" style="display: none">
									${uiObj.prevContent.toString()}
								</div>
							</span>

							%{--${(uiObj.prevContent.toString().length()>20)?(uiObj.prevContent.toString().substring(0,20).encodeAsHTML()+"..."):uiObj.prevContent}
							<div id="prev_${uit.id}"  style="display: none">
								${uiObj.prevContent.toString()}
							</div>--}%
						</td>
						<td>

							${com.deerwalk.security.User.get(uiObj.lastUpdatedBy).username}
						</td>
						<td><g:formatDate format="dd/MM/yyyy HH:mm:ss" date="${uiObj.dateCreated}" /></td>
					</tr>
				</g:each>
		</g:if>
				</tbody>
			</table>
    </div>

		</div>
<g:if test="${!isEditable}">
	<div class="z5-pagination">
                <g:paginate max="10" total="${UITrackerInstanceTotal}" />
            </div>
	</g:if>

	</div>



	</div>
	</div>
	<script>
		$( document ).ready(function() {
			var $table = $('table.z5-fixed-header');
			$table.floatThead({
				scrollContainer: function($table){
					return $table.closest('.z5-table');
				}
			});
		});

		function showCurr(id){
			var text1=$("#curr_"+id).html();
			$("#current_"+id).html(text1);


		}
		function showPrev(id){
			var text1=$("#prev_"+id).html();
			$("#previous_"+id).html(text1);


		}
	</script>
	%{--<g:javascript>
		renderStackedBars()
		function renderStackedBars(){
			var data = [
				["oranges",2312],
				["mangos",674],
				["limes", 994],
				["apples", 3433],
				["strawberries", 127],
				["blueberries",2261]
			];
			var main_margin = {top: 20, right: 60, bottom: 50, left: 70},
					main_width = 600 - main_margin.left - main_margin.right,
					main_height = 400 - main_margin.top - main_margin.bottom;


			var chart = document.getElementById("chart"),
					axisMargin = 20,
					margin = 20,
					valueMargin = 4,
					width = main_width,
					height = main_height,
					barHeight = (height-axisMargin-margin*2)/data.length,
					barPadding = (height-axisMargin-margin*2)*0.1/data.length,
					data, bar, svg, scale, xAxis, labelWidth = 0;

			max = d3.max(data.map(function(i){
				return i[1];
			}));

			svg = d3.select(chart)
					.append("svg")
					.attr("width", width)
					.attr("height", 400);


			bar = svg.selectAll("g")
					.data(data)
					.enter()
					.append("g");

			bar.attr("class", "bar")
					.attr("cx",0)
					.attr("transform", function(d, i) {
						return "translate(" + margin + "," + (i * (barHeight + barPadding) + barPadding) + ")";
					});

			bar.append("text")
					.attr("class", "label")
					.attr("y", barHeight / 2)
					.attr("dy", ".35em") //vertical align middle
					.text(function(d){
						return d[0];
					}).each(function() {
						labelWidth = Math.ceil(Math.max(labelWidth, this.getBBox().width));
					});

			scale = d3.scale.linear()
					.domain([0, max])
					.range([0, width - margin*2 - labelWidth]);
			yscale = d3.scale.linear()
					.domain([0, data.length])
					.range([height,0]);


			xAxis = d3.svg.axis()
					.scale(scale)
					//.tickSize(-height + 2*margin + axisMargin)
					.tickSize(4)
					.orient("bottom");

			var main_yAxisLeft = d3.svg.axis()
					.scale(yscale)
					.ticks(0)
					.outerTickSize(0)
					.orient("left");

			bar.append("rect")
					.attr("transform", "translate("+labelWidth+", 0)")
					.attr("height", barHeight)
					.attr("width", function(d){
						return scale(d[1]);
					});

			bar.append("text")
					.attr("class", "value")
					.attr("y", barHeight / 2)
					.attr("dx", -valueMargin + labelWidth) //margin right
					.attr("dy", ".35em") //vertical align middle
					.attr("text-anchor", "end")
					.text(function(d){
						return d[1];
					})
					.attr("x", function(d){
						var width = this.getBBox().width;
						return Math.max(width + valueMargin, scale(d[1]));
					});

			svg.insert("g",":first-child")
					.attr("class", "axis")
					.attr("transform", "translate(" + (margin + labelWidth) + ","+ (height-barHeight)+")")
					.call(xAxis);

			svg.append("g")
					.attr("class", "y axis yaxisLeft")
					.call(main_yAxisLeft)
					.attr("transform", "translate(" + (margin + labelWidth) + ",0)")
					.selectAll("path");
		}
	</g:javascript>--}%
	%{--<style>






	svg {
		shape-rendering: crispEdges;
	}

	.bar {
		fill: #A18FDB;
	}

	.value {
		font-size: 16px;
		font-weight: 300;
		fill: #333;
	}

	.label {
		font-size: 18px;
		fill: #333;
	}




	</style>--}%
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">
	<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>

	</body>
</html>
