<%@ page import="com.deerwalk.z5.UITracker" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'UITracker.label', default: 'UITracker')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">

</div>
<div id="create-UITracker" class="content scaffold-create" role="main">
</div>
</body>
</html>

<g:javascript>
	d3.selectAll("div").style("color", function() {
		return "hsl(" + Math.random() * 360 + ",100%,50%)";
	});



	var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");

	var width = 960,
			height = 500;

	var svg = d3.select("body").append("svg")
			.attr("width", width)
			.attr("height", height)
			.append("g")
			.attr("transform", "translate(32," + (height / 2) + ")");

	function update(data) {
		console.log(alphabet);
		// DATA JOIN
		// Join new data with old elements, if any.
		var text = svg.selectAll("text")
				.data(data);

		// UPDATE
		// Update old elements as needed.
		text.attr("class", "update");

		// ENTER
		// Create new elements as needed.
		text.enter().append("text")
				.attr("class", "enter")
				.attr("x", function(d, i) { return i * 32; })
				.attr("dy", ".35em")
				.text(function(d) { return d; });
		// ENTER + UPDATE
		// Appending to the enter selection expands the update selection to include
		// entering elements; so, operations on the update selection after appending to
		// the enter selection will apply to both entering and updating nodes.

		// EXIT
		// Remove old elements as needed.
		text.exit().remove();
	}

	// The initial display.
	update(alphabet);

	// Grab a random sample of letters from the alphabet, in alphabetical order.
	/*setInterval(function() {
		update(d3.shuffle(alphabet)
				.slice(0, Math.floor(Math.random() * 26))
				.sort());
	}, 1500);*/

</g:javascript>




%{--
<%@ page import="com.deerwalk.z5.UITracker" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'UITracker.label', default: 'UITracker')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#create-UITracker" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-UITracker" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${UITrackerInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${UITrackerInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
--}%
