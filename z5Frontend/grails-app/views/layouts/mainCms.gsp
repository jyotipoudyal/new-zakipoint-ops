<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>zakipoint Health</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<r:require module="application"/>
	<r:require module="css"/>
	<ckeditor:resources/>
	%{--<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'd3.min.js')}"></script>--}%
	<g:javascript>
		CKEDITOR.config.autoParagraph = false;
		<ckeditor:config var="toolbar_Mytoolbar">
			[
            [ 'Source','Bold', 'Italic', 'Underline', 'Scayt',"NumberedList","BulletedList","Outdent","Indent","Styles","Format", ]
            ]
		</ckeditor:config>

		function displayData(content){


//ev.preventDefault();
		/*var href=window.location.href
		 if(href.toString().indexOf("cms")>-1) return false*/
		setTimeout(function(e){
			if($("#"+content).hasClass("in")){

			}else{
				$("#"+content).modal("show");
			}
		},400);


	}
	</g:javascript>
	<g:layoutHead/>
	<r:layoutResources/>

</head>

<body>
<div class="mainWrapper">
	<g:render template="/include/header"/>
	<div class="container mb25" style="cursor: pointer;">
		<div class="col-md-12">
		</div>
		<div id="reportDetailsEmpty">
		</div>
	</div>
	<g:layoutBody/>
	<g:render template="/include/footer"/>
</div>

<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<g:hiddenField name="currentData" value=""/>
				%{--<g:textArea name="displayContent"/>--}%
				<ckeditor:editor name="displayContent" height="150px" width="100%" toolbar="Mytoolbar">

				</ckeditor:editor>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="saveData()">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
			</div>
		</div>
	</div>
</div>

<div id="sessionBox" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" id="sessionText">
				You have been logged out due to inactivity. Please login to continue.
			</div>
			<div class="modal-footer">
				<g:form controller="dashboard" action="index">
					<button type="submit" class="btn btn-primary">Login</button>
				</g:form>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.sessionTimeout.js')}"></script>
<g:javascript>
	$(function () {
				$.sessionTimeout();
		});

	$('.isEditable').on('click',function(e){
		e.preventDefault();
		CKEDITOR.instances["displayContent"].setData($(this).find('span').html());
		$("#currentData").val($(this).find('input').val());
		$("#myModal").modal("show");
	});

	$('.preventDefault').on('click',function(e){
		e.preventDefault();
	});




	function saveData(){
		var text=CKEDITOR.instances["displayContent"].getData();
		//var val="value="+text+"&code="+$("#currentData").val();
		 $.ajax({
					url: "${createLink(controller: 'cms', action: 'updateData')}",
					async:true,
					type: 'POST',
					data: { value: text, code: $("#currentData").val(),url:window.location.pathname},
					dataType: 'json',
					success: function (req) {
							$("#myModal").modal("hide");
							var ask=confirm("Changes will be reflected once you refresh the page.\nDo you want to refresh the page?");
							if(ask){
							window.location.reload();
							}

					}, error : function(err){
							$("#myModal").modal("hide");
					}
		});
	}
</g:javascript>
<style>
	.isEditable:hover{
		cursor: pointer;
		/*background-color: greenyellow;*/
	}
</style>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-60665302-4', 'auto');
	ga('send', 'pageview');

</script>
<g:javascript library="application"/>
<r:layoutResources/>
</body>
</html>
