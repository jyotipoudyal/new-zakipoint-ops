<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>zakipoint Health</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<r:require module="application"/>
	<r:require module="css"/>
	<ckeditor:resources/>
	%{--<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'd3.min.js')}"></script>--}%
	<g:javascript>
		CKEDITOR.config.autoParagraph = false;
		<ckeditor:config var="toolbar_Mytoolbar">
		[
        [ 'Source','Bold', 'Italic', 'Underline', 'Scayt',"NumberedList","BulletedList","Outdent","Indent","Styles","Format", ]
        ]
	</ckeditor:config>
	</g:javascript>
	<g:layoutHead/>
	<r:layoutResources/>
	<script>

	$( document ).ajaxStart(function() {

		$('#galleryImages').show();

		$("#reportDetail").html("");
		$("#reportDetail").hide();
		if(!$("#showReportingDetails").length) {
			$("#angle-double").show();
		}

	});

	$( document ).ajaxStop(function() {


		var $table =$(".z5-fixed-header");
		$.each($table, function() {
			var $this = $(this);

			var currentSort = $this.find("thead>tr th a.sorted").hasClass("desc");
			if (currentSort) {
				currentSort = "desc";
			} else {
				currentSort = "asc";
			}

			$.each($(this).find("thead>tr th a:not(.sorted)"), function () {
				$(this).addClass(currentSort+"-disabled");
//				$(this).addClass(currentSort);

			})
		});



		$('#galleryImages').hide();
//		$(".angle-double").click();



	});


	function displayData(content){


//ev.preventDefault();
		/*var href=window.location.href
		 if(href.toString().indexOf("cms")>-1) return false*/
		setTimeout(function(e){
			if($("#"+content).hasClass("in")){

			}else{
				$("#"+content).modal("show");
			}
		},400);


	}

	function createZ5Link(attr){
		var href="";
		%{--${createLink(action: "searchResult",controller: "memberSearch")}--}%
//		href+="memberSearch/searchResult?"
		href+='${createLink(action: "searchResult",controller: "memberSearch")}';
		href+= "?"
		var count=0;
		$.each(attr, function (k, v) {
			if(count==0){
				href+= ""
				count=1
			}else{
				href+= '&'
			}

			href+= k
			href+= '='
			href+=v
			href+= ""
		})

		/* href+= " onclick=\""
		 href+= "postRequestForGet(this);return false;"
		 href+= "\""*/
		return href;
	}

	function postRequestForGet(element){
		var p = $(element).attr('data-href').split('?');
		var action = p[0];
		var params = p[1].split('&');
		var form = $(document.createElement('form')).attr('action', action).attr('method','POST');
		$('body').append(form);
		for (var i in params) {
			var tmp= params[i].split('=');
			var urlString=tmp[1];
			var value;
			if(urlString.indexOf("%")>0){
				value = urlString.replace('%','%25');
			}else{
				value = decodeURIComponent(urlString).replace(/\+/g,' ');
			}

			var key = tmp[0];


			$(document.createElement('input')).attr('type', 'hidden').attr('name', key).attr('value', value).appendTo(form);
		}
		var isAll=$(element).attr("data-exp");
		if(!isAll){
			var reportingBasis, range, isExclude;
//		reportingBasis = $("#reportingBasis1").val();
			isExclude = $("#includeOn").val();
			range = ($("#range1").val());
			$(document.createElement('input')).attr('type', 'hidden').attr('name', "isExclude").attr('value', isExclude).appendTo(form);
			$(document.createElement('input')).attr('type', 'hidden').attr('name', "range").attr('value', range).appendTo(form);
//		$(document.createElement('input')).attr('type', 'hidden').attr('name', "reportingBasis").attr('value', reportingBasis).appendTo(form);

		}
		$(form).submit();
		return false;
	}

	/*var colorScale ={
		"pa chronic conditions":"#3366cc",
		"affective psychosis":"#dc3912",
		"asthma":"#ff9900",
		"atrial fibrillation":"#109618",
		"blood disorders":"#990099",
		"cad":"#0099c6",
		"copd":"#dd4477",
		"cancer":"#66aa00",
		"chronic pain":"#b82e2e",
		"chf":"#316395",
		"demyelinating diseases":"#994499",
		"depression":"#22aa99",
		"diabetes":"#aaaa11",
		"ckd":"#6633cc",
		"eating disorders":"#e67300",
		"hiv/aids":"#8b0707",
		"hyperlipidemia":"#651067",
		"hypertension":"#329262",
		"immune disorders":"#5574a6",
		"inflammatory bowel disease":"#3b3eac",
		"liver diseases":'#1f77b4',
		"morbid obesity":'#ff7f0e',
		"osteoarthritis":'#ff7f0e',
		"peripheral vascular disease":'#2ca02c',
		"rheumatoid arthritis":'#2ca000',
		"only":"steelblue"
	};*/

	var colorScale ={
		"1001":"rgba(220, 57, 18, .6)",
		"1002":"rgba(255, 153, 0, .6)",
		"1003":"rgba(16, 150, 24, .6)",
		"1004":"rgba(153, 0, 153, .6)",
		"1005":"rgba(0, 153, 198, .6)",
		"1006":"rgba(102, 170, 0, .6)",
		"1007":"rgba(184, 46, 46, .6)",
		"1008":"rgba(49, 99, 149, .6)",
		"1009":"rgba(221, 68, 119, .6)",
		"1010":"rgba(153, 68, 153, .6)",
		"1011":"rgba(34, 170, 153, .6)",
		"1012":"rgba(170, 170, 17, .6)",
		"1013":"rgba(230, 115, 0, .6)",
		"1014":"rgba(102, 51, 204, .6)",
		"1015":"rgba(139, 7, 7, .6)",
		"1016":"rgba(101, 36, 17, .6)",
		"1017":"rgba(50, 146, 98, .6)",
		"1018":"rgba(85, 116, 166, .6)",
		"1019":"rgba(59, 62, 172, .6)",
		"1020":'rgba(31, 119, 180, .6)',
		"1021":'rgba(255, 167, 113, .6)',
		"1022":'rgba(201, 105, 205, .6)',
		"1023":'rgba(94, 10, 44, .6)',
		"1024":'rgba(180, 1, 200, .6)',
		"0000":"rgba(70, 130, 180, .6)"
	};

	function splitColors(cs,opacity){
		var color=cs.split("(")
		color=color.join().split(")")
		color=color.join().split(")")
		color=color.join().split(",")
//		color="rgba("+color[1]+", "+color[2]+", "+color[3]+", .9)"
		if(!opacity)
		return "rgba("+color[1]+", "+color[2]+", "+color[3]+", .8)"
		else
		return  color
	}

	$(document).ready(function(){
//		$('[data-toggle="tooltip"]').tooltip();

//		$('.custom-tooltip').tooltip({title: "Feature Under Development", placement: "bottom"});

		$('#range1').prop('disabled', 'disabled');
		$('#range1').attr('title', 'Enabled for national benchmark that exclude');
		$('#range1').addClass('disSelect');

		$("#includeOn").on("change",function(){
			if(this.value=="true"){
				$('#range1').prop('disabled', false);
				$('#range1').removeClass('disSelect');
			}else{
				$('#range1').val("0");
				$('#range1').prop('disabled', 'disabled');
				$('#range1').addClass('disSelect');
			}

		})


	});


	</script>


</head>

<body>
<div class="mainWrapper">
	<g:render template="/include/header"/>
	<g:if test="${session.clientname}">
		<div class="container" style="cursor: pointer;">
            <div class="col-md-12">
			<a href="javascript:void(0)" class="pull-right reporting-detail-toggle" style="right:30px; top: 5px;" id="angle-double">Show Reporting Details <i class="fa fa-chevron-down"></i></a>
            </div>
			<div id="reportDetail">
			</div>
		</div>
	</g:if>
	<g:layoutBody/>
	<g:render template="/include/footer"/>
</div>

<div id="spinner" class="spinner" style="display:none;"></div>
<div id="sessionBox" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" id="sessionText">
				You have been logged out due to inactivity. Please login to continue.
			</div>
			<div class="modal-footer">
				<g:form controller="dashboard" action="index">
					<button type="submit" class="btn btn-primary">Login</button>
				</g:form>
			</div>
		</div>
	</div>
</div>

<div id="clientBox" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body" >
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div id="clientLists" >

				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="cohortInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">

				<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">New Cohort Request Form</h4>
			</div>
			<div class="modal-body">
				<div class="mb15">
					Please provide as much detail as possible so that we can build specific cohort. If you are unsure about
					the specific logic your account manager can work with you.
				</div>

				<table class="cohort-table ">
					<tr>
						<td>Cohort Name</td>
						<td><g:textField name="cohortName" class="required"/></td>
					</tr>

					<tr>
						<td>Cohort Logic</td>
						<td><g:textArea name="cohortDesc" style="width:400px; height: 100px;" maxlength="3000" class="required"/></td>
					</tr>

				</table>

			<div class="modal-footer">
				<button type="button" onclick="sendMail()" class="btn btn-primary" >Submit</button>
				<button type="button" class="btn btn-danger btn-ok" data-dismiss="modal">Cancel</button>
			</div>



			</div>

		</div>
	</div>
</div>
%{--<g:hiddenField name="currentControllerName" value="${controllerName}"/>--}%
%{--<g:hiddenField name="currentActionName" value="${actionName}"/>--}%


<div id="galleryImages" style="display: none;;">
	<g:img dir="images" file="ajax-loader.gif" style="position:relative; top:50%;"/>
</div>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.sessionTimeout.js')}"></script>
<g:javascript>

	$(function () {
		$.sessionTimeout();
	});





</g:javascript>
<script>


	function sendMail(){
		$("#cohortInfo").modal("hide");
		var cohortName, cohortDesc, name,file;

		    cohortName = $("#cohortName").val();
			cohortDesc = $("#cohortDesc").val();

			jQuery.ajax({
				type: 'POST',
				data: {cohortName: cohortName, cohortDesc: cohortDesc, name: name,file:file},
				url: '${createLink(controller:"dashboard",action:"sendCohortMail")}',
				success: function (resp) {


					alert("Thank you for submitting new cohort request. Your account manager at ZakiPointHealth may contact you with any additional questions regarding this.")


				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {

					alert("Error!!")

				},
				complete: function (XMLHttpRequest, textStatus) {
				}
			})

	}

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-60665302-4', 'auto');
	ga('send', 'pageview');

	localStorage.clear()



	$(function(){





		if($("#showReportingDetails").length){
			$("#angle-double").hide();
		}


		$("#angle-double").on("click",function(){
			var $this=$(this);
			var reportingBasis, range, isExclude;
			reportingBasis = $("#reportingBasis1").val()||"PaidDate";
			isExclude = $("#includeOn").val()||"false";
			range = $("#range1").val()||"0";

			jQuery.ajax({
				type: 'POST',
				data: {reportingBasis: reportingBasis, range: range, isExclude: isExclude},
				url: '${createLink(controller:"dashboard",action:"reportingDetail")}',
				success: function (resp) {
					var $repDet=$("#reportDetail")
					$("#angle-double").hide();
					$repDet.html(resp);
					$repDet.css("visibility","visible");
					$repDet.fadeIn();
					$repDet.on("click",function(){
//						$(this).css("visibility","hidden");
						$(this).hide();
						$this.show();
						/*$(this).fadeOut(function(){
							$this.slideDown();
						});*/

					});
//					});
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				},
				complete: function (XMLHttpRequest, textStatus) {
				}
			});



		});

		/*$this.find("i").remove();
		 $this.append("");
		 i class='fa fa-angle-double-up'></i>*/

		/*$("#reportDetail").on("click",function(){

			$(this).fadeOut(function(){
				$(".angle-double").slideDown();
			});

		});*/
	})






	/*function sync(){
		$(".clientSelect").on("click",function(){
			var $this=$(this);
			var clientName=$this.text().toString().split(" ").join("")
			if($this.hasClass("actives")){
				$this.removeClass("actives")
				id=$this.text();
				flag=0;
			}else{
				$this.addClass("actives");
				id=$this.text();
				flag=1;
			}
			jQuery.ajax({
				type:'POST',
				async:false,
				data:{id:id,flag:flag},
				url:'${createLink(controller: 'dashboard',action: 'selectGroup')}',
				success:function(resp){
					window.location.reload();
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){
				},
				complete:function(XMLHttpRequest,textStatus){
				}});



			var url="http://"+clientName+"${grailsApplication.config.grails.hostPath}";
			window.location.href=url;
		})

		$(".groupSelect").on("click",function(){
			var $this=$(this)
			var flag,id;

			if($this.hasClass("active")){
				$this.removeClass("active")
				id=$this.text();
				flag=0;
			}else{
				$this.addClass("active");
				id=$this.text();
				flag=1;
			}
			jQuery.ajax({
				type:'POST',
				async:false,
				data:{id:id,flag:flag},
				url:'${createLink(controller: 'dashboard',action: 'selectGroup')}',
				success:function(resp){
					window.location.reload();
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){
				},
				complete:function(XMLHttpRequest,textStatus){
				}});

		})
	}



	jQuery.ajax({
		type:'POST',
		async:false,
		url:'${createLink(controller: 'dashboard',action: 'viewForClientGroup')}',
		success:function(resp){
			$("#addClientView").append(resp);
			sync();

		},
		error:function(XMLHttpRequest,textStatus,errorThrown){
		},
		complete:function(XMLHttpRequest,textStatus){
		}});*/

	$("#addClientView").on("click",function(){
		$("#clientLists").html("");
//		var ctrl=$("#currentControllerName").val()
//		var act=$("#currentActionName").val()
		if(${controllerName=="memberSearch"}){
			alert("You are unable to switch between clients in this page.")
			return false;
		}
		var url=window.location.pathname+""+window.location.search;

		jQuery.ajax({
			type:'POST',
			async:false,
			data:{url:url},
			url:'${createLink(controller: 'dashboard',action: 'viewForClientGroup')}',
			success:function(resp){
				$("#clientLists").html(resp);
				$("#clientBox").modal("show");
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){
			},
			complete:function(XMLHttpRequest,textStatus){
			}});

	})




</script>
%{--<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'layout.css')}">--}%


<g:javascript library="application"/>
<r:layoutResources/>
</body>
</html>
