<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>zakipoint Health</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<r:require module="application"/>
	<r:require module="css"/>
	<ckeditor:resources/>
	%{--<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'd3.min.js')}"></script>--}%
	<g:javascript>
		CKEDITOR.config.autoParagraph = false;
		<ckeditor:config var="toolbar_Mytoolbar">
		[
        [ 'Source','Bold', 'Italic', 'Underline', 'Scayt',"NumberedList","BulletedList","Outdent","Indent","Styles","Format", ]
        ]
	</ckeditor:config>
	</g:javascript>
	<g:layoutHead/>
	<r:layoutResources/>


</head>

<body>
<div class="mainWrapper">
	<div class="headerWrapper">
		<div class="container client-box-wrapper">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<g:link controller="dashboard" title="zakipoint Health" class="logo">
					<g:img dir="images" file="logo-zakipoint-large.png"/>
				%{--<img src="images/logo-zakipoint-large.png" class="img-responsive" alt="zakipoint Health" />--}%
				</g:link></div>
			</div>
			</div>

	<g:layoutBody/>
	<g:render template="/include/footer"/>
</div>

<div id="spinner" class="spinner" style="display:none;"></div>
<div id="sessionBox" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" id="sessionText">
				You have been logged out due to inactivity. Please login to continue.
			</div>
			<div class="modal-footer">
				<g:form controller="dashboard" action="index">
					<button type="submit" class="btn btn-primary">Login</button>
				</g:form>
			</div>
		</div>
	</div>
</div>

<div id="clientBox" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body" >
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div id="clientLists" >

				</div>

			</div>
		</div>
	</div>
</div>
%{--<g:hiddenField name="currentControllerName" value="${controllerName}"/>--}%
%{--<g:hiddenField name="currentActionName" value="${actionName}"/>--}%


<div id="galleryImages" style="display: none;;">
	<g:img dir="images" file="ajax-loader.gif" style="position:relative; top:50%;"/>
</div>


<g:javascript library="application"/>
<r:layoutResources/>
</body>
</html>
