<div class="headerWrapper">
    <div class="container">
        <div class="col-sm-12 col-md-4 col-lg-4">
            <g:link controller="dashboard" title="zakipoint Health" class="logo">
            <g:img dir="images" file="logo-zakipoint-large.png"/>
            %{--<img src="images/logo-zakipoint-large.png" class="img-responsive" alt="zakipoint Health" />--}%
            </g:link></div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <ul class="main-menu pull-left">
                <li><a href="#" class="isEditable"><g:editMessage code="heading.title1"/></a></li>
                <li><a href="#" class="isEditable"><g:editMessage code="heading.title2"/></a></li>
                <li><a href="#" class="isEditable"><g:editMessage code="heading.title3"/></a></li>
            </ul>
        </div>

        <div class="col-sm-12 col-md-2 col-lg-2">
            <ul class="user-stat pull-right">
                <li><a href="#" ><i class="fa fa-user"></i> <g:fullname/></a></li>
                <li><g:link controller="logout" class="isEditable"><i class="fa fa-power-off"></i> <g:editMessage code="heading.logout.message"/></g:link></li>
            </ul>
        </div>
    </div>
</div>
