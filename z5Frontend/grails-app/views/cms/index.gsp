<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="mainCms">
    <r:require module="graph"/>
</head>
<body>
    <div class="contentWrapper">
        <div class="container mt35">
            <div class="col-sm-12 col-md-6 col-lg-6 col-sm-offset-0 col-md-offset-4 col-lg-offset-4 centered">
                <div class="col-sm-12 col-md-4 col-lg-4"><g:img uri="/images/tilde-logo.png" alt="Tilde" class="pull-left m15 bordered" /></div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <h2 class="isEditable"><g:editMessage code="dashboard.title" /></h2>
                    <span class="isEditable"><g:editMessage code="dashboard.title.content1" /></span>
                    <a href="#" class="text-link-upper isEditable"><g:editMessage code="dashboard.title.viewLabel" /></a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="container mt25">
            <div class="col-sm-12 col-md-4 col-lg-4">
                <h1 class="isEditable"><g:editMessage code="dashboard.graph1.title"/> </h1>
                <div id="graph1" class="col-sm-12 col-md-8 col-lg-8 graph"></div>
                <div class="col-sm-12 col-md-4 col-lg-4 graph-label"><span></span></div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 border-lr">
                <h1 class="isEditable"><g:editMessage code="dashboard.graph2.title"/></h1>
                <div  id="graph2"class="col-sm-12 col-md-8 col-lg-8 graph"></div>
                <div class="col-sm-12 col-md-4 col-lg-4 graph-label"><span></span></div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <h1 class="isEditable"><g:editMessage code="dashboard.graph3.title"/></h1>
                <div id="graph3" class="col-sm-12 col-md-8 col-lg-8 graph"><span></span></div>
                <div class="col-sm-12 col-md-4 col-lg-4 graph-label">
                    <span id="graph3-0" class="color-red"> </span><br />
                    <span id="graph3-1" class="color-gray"> </span><br />
                    <span id="graph3-2" class="color-blue"> </span> </div>
            </div>
        </div>
        <div class="container mt25">
            <h1 class="hr-bg text-center isEditable"><span><g:editMessage code="dashboard.container.title"/></span></h1>
            <h2 class="text-center mb35 mt15 isEditable"><g:editMessage code="dashboard.container.subTitle"/></h2>
            <div class="col-sm-12 col-md-4 col-lg-4 mb35">
                <div class="box-blue color-blue">
                    <p class="text-center"><g:img uri="/images/icon-high-pharmacy-cost.png" /></p>
                    <h3 class="mt15 mb15 text-center"><a href="#" class="isEditable"><g:editMessage code="dashboard.container.content1.title"/></a></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content1.desc"/></p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 mb35">
                <div class="box-blue color-blue">
                    <p class="text-center"><g:img uri="/images/icon-overuse-er.png" /></p>
                    <h3 class="mt15 mb15 text-center"><a href="#" class="isEditable"><g:editMessage code="dashboard.container.content2.title"/></a></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content2.desc"/></p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 mb35">
                <div class="box-blue color-blue">
                    <p class="text-center"><g:img uri="/images/icon-low-program-engagement.png" /></p>
                    <h3 class="mt15 mb15 text-center"><a href="#" class="isEditable"><g:editMessage code="dashboard.container.content3.title"/></a></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content3.desc"/></p>
                </div>
            </div>


            <div class="col-sm-12 col-md-4 col-lg-4 mb35">
                <div class="box-blue color-blue">
                    <p class="text-center"><g:img uri="/images/icon-decline-in-health.png" /></p>
                    <h3 class="mt15 mb15 text-center "><a href="#" class="isEditable"><g:editMessage code="dashboard.container.content4.title"/></a></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content4.desc"/></p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 mb35">
                <div class="box-blue color-blue">
                    <p class="text-center"><g:img uri="/images/icon-high-risk-segment.png" /></p>
                    <h3 class="mt15 mb15 text-center"><a href="#" class="isEditable"><g:editMessage code="dashboard.container.content5.title"/></a></h3>
                    <p class="box-content isEditable"><g:editMessage code="dashboard.container.content5.desc"/></p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 mb35">
                <div class="box-blue color-blue">
                    <p class="text-center"><g:img uri="/images/icon-high-cost-claimants.png" /></p>
                    <h3 class="mt15 mb15 text-center"><a href="#" class="isEditable"><g:editMessage code="dashboard.container.content6.title"/></a></h3>
                    <p class="box-content isEditable "><g:editMessage code="dashboard.container.content6.desc"/></p>
                </div>
            </div>
        </div>
        <div class="container mt25">
            <h1 class="hr-bg text-center"><span><g:editMessage code="dashboard.container2.title"/></span></h1>
            <h2 class="text-center mb35 mt15"><g:editMessage code="dashboard.container2.subtitle"/></h2>
            <div class="text-center mb15"><a href="#" class="btn-link-upper"><g:editMessage code="dashboard.container2.button"/></a></div>
            <div class="text-center text-itallic mb35"><g:editMessage code="dashboard.container2.alt"/></div>

            <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
                <h3><g:editMessage code="dashboard.container2.content1.title"/></h3>
                <a href="#"><g:editMessage code="dashboard.container2.content1.li1"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content1.li2"/></a>

                <h3><g:editMessage code="dashboard.container2.content2.title"/></h3>
                <a href="#"><g:editMessage code="dashboard.container2.content2.li1"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content2.li2"/></a><br />
                <a href="#"><g:editMessage code="dashboard.container2.content2.li3"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content2.li4"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content2.li5"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content2.li6"/></a>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
                <h3><g:editMessage code="dashboard.container2.content3.title"/></h3>
                <a href="#"><g:editMessage code="dashboard.container2.content3.li1"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content3.li2"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content3.li3"/></a>

                <h3><g:editMessage code="dashboard.container2.content4.title"/></h3>
                <a href="#"><g:editMessage code="dashboard.container2.content4.li1"/></a><br/>
                <a href="#"><g:editMessage code="dashboard.container2.content4.li2"/></a>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
                <h3>Medical Plan</h3>
                <a href="#">PPO</a><br/>
                <a href="#">HMO</a><br/>
                <a href="#">High-decuctable (HRA)</a><br/>
                <a href="#">High-deductable (HSA)</a>

                <h3>Risk Segment</h3>
                <a href="#">High</a><br/>
                <a href="#">Medium</a><br />
                <a href="#">Low</a>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 mb15 inline-links">
                <h3>Conditions</h3>
                <a href="#">Diabetes</a><br/>
                <a href="#">Heart Disease</a><br/>
                <a href="#">Lung cancer</a><br/>
                <a href="#">Lung Asthma</a><br/>
                <a href="#" class="text-link-upper">See More</a>

                <h3>Employee Type</h3>
                <a href="#">Active</a><br/>
                <a href="#">Retiree Pre-65</a><br />
                <a href="#">Retire Post-65</a><br/>
            </div>

        </div>


    </div>



</body>
</html>