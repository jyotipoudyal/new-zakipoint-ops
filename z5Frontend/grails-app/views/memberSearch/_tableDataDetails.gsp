<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="z5-table put-border" style="max-height: 552px;min-height: 50px; overflow-x: auto;overflow-y: auto;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id="memberTable" >
        <thead>
        <tr>


            %{--<th class="text-left isEditable" >S.N</th>
            <th class="text-left isEditable" >Member ID</th>
            <th class="text-left isEditable" >Name</th>
            <th class="text-left isEditable" >DOB</th>
            <th class="text-left isEditable" >Age</th>
            <th class="text-left isEditable" >Gender</th>
            <th class="text-left isEditable" >Relationship Flag</th>
            <th class="text-left isEditable" >Current Status</th>
            <th class="text-left isEditable" >Risk Score</th>
            <th class="text-left isEditable" >Med Paid Amount</th>
            <th class="text-left isEditable" >RX Paid Amount</th>
            <th class="text-left isEditable" >Total Paid Amount</th>
            <th class="text-left isEditable" ># Of Chronic Conditions</th>
            <th class="text-left isEditable" ># Of Care Gaps</th>--}%

            <th class="text-left isEditable" >S.N</th>
            <util:remoteSortableColumn   class="text-left"   property="unblindMemberId" title="Member ID" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"   property="memberFullName" title="Name" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"   property="memberDOB" title="DOB" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"  property="memberAge" title="Age" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"  property="memberGenderId" title="Gender" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"  property="relationshipFlag" title="Relationship Flag" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"  property="currentStatus" title="Current Status" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"  property="riskScore" title="Prospective Risk Score" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"  property="predictiveCost" title="Predicted Total Cost" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"   property="medTotalPaidAmount" title="Med Paid Amount" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"   property="rxTotalPaidAmount" title="RX Paid Amount" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left ${defaultSort}"   property="totalPaidAmount" title="Total Paid Amount" update="topChronicConditionEr" action="sortResult"/>
            <util:remoteSortableColumn    class="text-left"   property="numberOfChronicConditions" title="# Of Chronic Conditions" update="topChronicConditionEr" action="sortResult"/>
            %{--<util:remoteSortableColumn    class="text-left"  property="gapInCareCount" title="# Of Care Gaps" update="topChronicConditionEr" action="sortResult"/>--}%
            <th class="text-left" style="width: 100px;"># Of Care Gaps</th>
            %{--<util:remoteSortableColumn  class="text-center ${defaultSort}"  property="description" title="${editMessage(code:"erUtilization.container4.table1.head1")}" update="topChronicConditionEr" action="sortResult" defaultOrder="asc" />--}%
        </tr>
        </thead>
        <tbody>
        <g:if test="${data}">

            <g:each in="${data}" var="data1" status="i"> %{--.sort{it.value.totalPaidAmount}.reverse()--}%
                <tr>
                    <td>${sn+i+1}</td>
                    <td >${data1?.value?.memberId}  </td>
                    %{--<td >NA--}%%{--${data1?.value?.unblindMemberId}  --}%%{--</td>--}%
                    <td  style="white-space: nowrap;" >
                       NA %{-- ${(data1?.value.memberFullName)?(data1?.value.memberFullName):"NA"}--}%
                    </td>
                    <td style="white-space: nowrap;"> NA %{--${data1?.value?.memberDOB}--}%</td>
                    <td >${(data1?.value.memberAge)?(data1?.value?.memberAge):"NA"} </td>
                    <td >${(data1?.value.memberGenderId)?(data1?.value?.memberGenderId):"NA"}</td>
                    <td >${(data1?.value.relationshipFlag)?(data1?.value?.relationshipFlag):"NA"}  </td>
                    <td >${(data1?.value.currentStatus)?(data1?.value?.currentStatus):"NA"}  </td>
                    <td >${(data1?.value.riskScore)?(data1?.value?.riskScore):"NA"}  </td>
                    <td >

                        <g:if test="${data1?.value.predictiveCost}">
                            <g:formatZ5Currency number="${data1?.value?.predictiveCost}" type="number" maxFractionDigits="2" />
                        </g:if><g:else>
                            NA
                        </g:else>

                    <td >
                        <g:if test="${data1?.value.medTotalPaidAmount}">
                            <g:formatZ5Currency number="${data1?.value?.medTotalPaidAmount}" type="number" maxFractionDigits="2" />
                        </g:if><g:else>
                            NA
                        </g:else>
                    </td>

                    <td>
                        <g:if test="${data1?.value.rxTotalPaidAmount}">
                                 <g:formatZ5Currency number="${data1?.value?.rxTotalPaidAmount}" type="number" maxFractionDigits="2" />
                        </g:if><g:else>
                            NA
                        </g:else>
                    </td>

                    <td>
                        <g:if test="${data1?.value.totalPaidAmount}">
                            <g:formatZ5Currency number="${data1?.value?.totalPaidAmount}" type="number" maxFractionDigits="2" />
                        </g:if><g:else>
                            NA
                        </g:else>
                    </td>

                    <td >
                        <%
                            def vals="",count=1;
                            for(String str: data1.value.chronicConditionList){
                                vals+= count+". "+str+"</br>"
                                count++;
                            }
                        %>

                        <a href="#" class="info-tooltip" title="Chronic Conditions" data-placement="right" data-toggle="popover"  data-trigger="hover" data-html="true" data-content="${vals}">
                            ${(data1?.value.numberOfChronicConditions)?(data1?.value.numberOfChronicConditions):"NA"} </a>
                    </td>


                    <td >
                        <%
                            def vals2="";
                            count=1
                            for(String str: data1.value.gapInCareList){
                                vals2+= count+". "+str+"</br>"
                                count++;
                            }

                        %>
                        <a href="#" class="info-tooltip" title="Gap In Care" data-placement="left" data-toggle="popover"  data-trigger="hover" data-html="true" data-content="${vals2}">

                            ${(data1?.value.gapInCareCount)?(data1?.value.gapInCareCount):"NA"}
                        </a>
                    </td>



                </tr>


            </g:each>

        </g:if><g:else>
            <tr>
                <td colspan="8">
                    No records to display.
                </td>
            </tr>
        </g:else>


        </tbody>

    </table>




</div>
%{--<g:if test="${total>data.size()}">--}%

<div class="z5-pagination">
    <util:remotePaginate controller="memberSearch" action="remotePagination" total="${total}" update="topChronicConditionEr" max="15" pageSizes="[15: '15 Records/Page', 50:'50 Records/Page',100:'100 Records/Page']" alwaysShowPageSizes="true"  />
</div>
%{--</g:if>--}%
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<script>
    $(document).ready(function() {

//        var $table = $('table.z5-fixed-header');
        var $table =$("#memberTable.z5-fixed-header");
        var currentSort = $table.find("thead>tr th a.sorted").hasClass("desc");
        if (currentSort) {
            currentSort = "desc";
        } else {
            currentSort = "asc";
        }

        $.each($table.find("thead>tr th a:not(.sorted)"), function () {
            $(this).addClass(currentSort+"-disabled");
        })


        $table.floatThead({
            scrollContainer: function($table){
                return $table.closest('.z5-table');
            }
        });







    });

</script>






