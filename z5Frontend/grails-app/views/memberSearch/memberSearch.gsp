<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Top Chronic Details</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>

    <style>
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        height: 31px;
        line-height: 10px;
    }
    .nav>li>a, .nav>li>a {
        height: 31px;
        line-height: 10px;
        text-decoration: none;
        background-color: #eee;
        border: 1px solid #3c3c3c;
    }
    </style>

<link href="${resource(dir:"css",file: "css-switch.css")}" rel="stylesheet"/>
</head>

<body>


<div  id="contentWrapper" class="contentWrapper">

    <div class="container double-arrow-back-top-container">


        <div >
            <div class="col-md-12">
                <a href="javascript:void(0)" onclick="window.history.back()" class=" text-link-upper mb15 double-arrow-left">
                    Previous
                </a>
            </div>


            <div style="clear: both"></div>
        </div>
        <div class="sep-border"></div>








    %{--<div class="col-md-6">
        <g:link controller="erUtilization" action="index" class="text-link-upper mb15">
            --}%%{--<i class="fa fa-angle-double-left iconRmargin"></i>--}%%{--
            --}%%{--<span class="isEditable"><g:editMessage code="erUtilization.costDrivers.top.navigation"/> </span>--}%%{--
        </g:link>
    </div>--}%
       %{-- <div class="col-md-6">
            <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                    <g:link title="Edit Content"  controller="cms" action="topChronicDetails"><i class="fa fa-edit"></i></g:link>
                </g:if><g:else>
                <g:link title="View Changes"  controller="erUtilization" action="topChronicDetails"><i class="fa fa-eye"></i></g:link>
            --}%%{--<a title="View Changes" class="switch-edit-mode"><i class="fa fa-eye"></i></a>--}%%{--
            </g:else>
            </sec:ifAllGranted>

        </div>--}%

        %{--<div class="clearfix"></div>--}%
%{----}%
        %{--<div class="sep-border mb25 pt15"></div>--}%

        <g:hiddenField name="showReportingDetails" value="false"/>
        <g:if test="${expPop}">

            %{--<div class="col-md-12 mb15">
                <div class="switch">
                    <input id="cmn-toggle-9" class="cmn-toggle cmn-toggle-yes-no" type="checkbox">
                    <label for="cmn-toggle-9" data-on="All" data-off="Current"></label>
                </div>
            </div>--}%
            <div class="col-md-12 mb10">
                %{--<g:set var="val" value="${expclass=='overall'?'checked':''}"/>
                <input id="switch-size" name="switchPop" data-label-width="1" type="checkbox"
                       data-size="small"  data-on-text="All" data-off-text="Current" data-off-color="primary" ${val} >--}%
                <ul class="nav nav-pills">
                    <li class="${expclass=='overall'?'active':''}">
                        <g:z5Link data-exp="true"  disabledLink="false" controller="memberSearch" action="searchResult" id="allPop" params="${expPopOverall}">
                        All Members</g:z5Link>
                    </li>
                    <li class="${expclass=='active'?'active':''}"><g:z5Link data-exp="true"  disabledLink="false" id="currentPop" controller="memberSearch" action="searchResult" params="${expPopActive}">
                        Active Members</g:z5Link>
                    </li>
                </ul>
            </div>

        </g:if>



            <div class="cost">
                    <div class="col-md-10">
                        <h4 class="isEditable" ><span class="color-gray">COST</span></h4>
                    </div>
                    <a href="javascript:void(0)">
                        <div class="col-md-2" id="cost-details" onclick="showHide(this,'cost2','2')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
                    </a>

                    <div class="container-fluid">
                                <div class="row-fluid " >
                                        <div class="col-md-12">

                                                <div class="short-div" id="cost2" style="display: none;">

                                                        <div class="col-md-4">
                                                            <strong>Pharmacy PMPM</strong>
                                                            <div id="pharmacyPmpm"></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <strong>Medical PMPM </strong>
                                                            <div id="medicalPmpm"></div>
                                                        </div>
                                                        <div class="col-md-4" >
                                                            <strong  style='text-transform:capitalize'>Total paid PMPM</strong>
                                                            <div id="changeInPmpm"></div>
                                                        </div>

                                                </div>
                                        </div>
                                </div>
                    </div>

                <div class="mb15"></div>
                <div class="sep-border mb15"></div>
                <div style="clear: both"></div>
            </div>


        <div class="claims">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">CLAIMS</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide(this,'claims4','4')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="claims4" style="display: none;">

                            <div class="col-md-4">
                                <strong>Claims Over $10K (per 1000)</strong>
                                <div id="claimsOver10k"></div>
                            </div>
                            <div class="col-md-4">
                                <strong>Claims Under $500 (per 1000)</strong>
                                <div id="claimsBelow500"></div>
                            </div>
                            <div class="col-md-4" >
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>

        <div class="medical">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">MEDICAL UTILIZATION</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide(this,'medical5','5')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="medical5" style="display: none;">

                            <div class="col-md-4" >
                                <strong style='text-transform:capitalize'>ER Visits Per 1000</strong>
                                <div id="erVisitper1000"></div>
                            </div>


                            <div class="col-md-4">
                                <strong style='text-transform:capitalize'>Admissions Per 1000</strong>
                                <div id="totalAdmissionsper1000"></div>
                            </div>
                            <div class="col-md-4" >
                            </div>


                        <div class="mb25"></div>

                            <div class="col-md-4">
                                <strong style='text-transform:capitalize'>Average Length Of Stay</strong>
                                <div id="averageLengthOfStay"></div>
                            </div>


                            <div class="col-md-4">
                                <strong>30 Days Re-Admissions Per 1000 </strong>
                                <div id="thirtyDayReadmitper1000"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>

        <div class="risk">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">RISKS</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide(this,'risk3','3')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="risk3" style="display: none;">

                             <div class="col-md-4">
                             <strong>Concurrent Risk</strong>
                             <div id="concurrentRisk"></div>
                         </div>

                         <div class="col-md-4">
                             <strong style='text-transform:capitalize'>Prospective Risk</strong>
                             <div id="prospectiveRisk"></div>
                         </div>
                            <div class="col-md-4" >
                            </div>

                        </div>




                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>

        %{--mara risk sankey--}%
        %{--<div class="risk">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">RISKS DIAGRAM</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide(this,'risk9','9')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="risk9" style="display: none;">

                             <div class="col-md-6">
                             <strong>Mara Risk</strong>
                             <div id="maraRisk"></div>
                         </div>

                         <div class="col-md-6">
                             <strong style='text-transform:capitalize'>HS Risk</strong>
                             <div id="hsRisk"></div>
                         </div>


                        </div>




                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>--}%

        <div class="pharmacy">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">PHARMACY UTILIZATION</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide(this,'pharmacy6','6')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="pharmacy6" style="display: none;">

                            <div class="col-md-4" >
                                <strong style='text-transform:capitalize'>Prescription Filled By Mail Order</strong>
                                <div id="mailOrder"></div>
                            </div>
                            <div class="col-md-4">
                                <strong>Prescription Filled By Generic Drugs</strong>
                                <div id="genericDrugs"></div>
                            </div>


                            <div class="col-md-4">
                                <strong>Prescription Filled By Speciality Order</strong>
                                <div id="specialityOrder"></div>
                            </div>
                            <div class="mb25"></div>

                            <div class="col-md-4">
                                <strong>Prescription Filled By Formulary</strong>
                                <div id="formulary"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>

        <div class="rel">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">RELATIONSHIPS</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide2(this,'rel7','7')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="rel7" style="display: none;">

                            <div class="col-md-12" >
                                <div class="">
                                    <g:if test="${expclass!='overall'}">
                                        <strong style='text-transform:capitalize'>Relationship</strong>
                                        <div  id="relationship"  class="graph-wrapper" style="border: none;" ></div>
                                    </g:if><g:else>

                                    <strong style='text-transform:capitalize'>Relationship</strong>
                                    <div class="watermark mt10 mb10" style="font-size: 14px;opacity: 0.4;bottom: 0;right: 0;color: red;"><div style="display: inline;text-transform: capitalize">
                                        &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i>
                                        This section is only applicable for Active Population.
                                        &nbsp;&nbsp;</div>
                                    </div>



                                </g:else>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>


        <div class="demography">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">DEMOGRAPHICS</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide2(this,'demography101','101')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="demography101" style="display: none;">
                            <div class=" mb25">
                            <div class="col-md-6" >
                                <div class="bdr-r">
                                    <div id="gender" class="graph-wrapper" style="border: none;"  ></div>
                                </div>
                            </div>

                                <div class="col-md-6" >
                                    <div  id="location"  class="graph-wrapper" style="max-height: 200px;min-height: 200px;overflow-y: auto;"  ></div>
                                    <div id="location_More"></div>
                                </div>
                                <div style="clear: both"></div>
                            </div>


                            <div class=" mb25">
                            <div class="col-md-12" >
                                <div class="">
                                    %{--<strong style='text-transform:capitalize'>Age</strong>--}%
                                    <div  id="age"  class="graph-wrapper" style="border: none;" ></div>
                                </div>
                            </div>


                                <div style="clear: both"></div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>

        <div class="newMetrics">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">CUSTOM METRICS</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide(this,'metrics8','8')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >


                        <div class="short-div" id="metrics8" style="display: none;">

                            <div class="col-md-4">
                                <strong style='text-transform:capitalize'>Gap in care</strong>
                                <div id="GAP_IN_CARE" ></div>
                            </div>

                            <div class="col-md-4">
                                <strong style='text-transform:capitalize'>Engaged in wellness</strong>
                                <div  id="WELLNESS_ENGAGED"   ></div>

                            </div>
                            <div class="col-md-4" >

                                <strong style='text-transform:capitalize'>Engaged in coaching</strong>

                                <div id="COACHING_ENGAGED" ></div>
                            </div>
                            <div style="clear: both"></div>

                            <div class="col-md-4" >

                                <strong style='text-transform:capitalize'>Potential Saving (Predicted-Expected)</strong>

                                <div id="PROJECTED_SAVINGS" ></div>
                            </div>

                            <div class="col-md-4" >

                                <strong style='text-transform:capitalize'>Aggregate Biometric Improvement Score</strong>

                                <div id="BIOMETRIC_SCORE" ></div>
                            </div>

                            <div class="col-md-4" >

                                <strong style='text-transform:capitalize'>Calculated Program Impact</strong>

                                <div id="PROGRAM_IMPACT_IN_AMOUNT" ></div>
                            </div>

                            <div class="col-md-4" >

                                <strong style='text-transform:capitalize'>Members Improving HS Risk Stratification Level</strong>

                                <div id="HS_RISK_STRATIFICATION" ></div>
                            </div>





                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>
        </div>


        <div class="div">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">DIVISION</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide2(this,'div102','102')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div" id="div102" style="display: none;">
                            <div class="col-md-6" >
                                <div class="">
                                    <div  id="divisionWiseErVisitPer1000"  class="graph-wrapper" style="max-height: 475px;min-height: 50px;overflow-y: auto;"  ></div>
                                    <div id="divisionWiseErVisitPer1000_More"></div>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>


        </div>

        <div class="div">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">GAP IN CARE</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide2(this,'gap103','103')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-12">

                        <div class="short-div mb25" id="gap103" style="display: none;">
                            <g:if test="${expclass!='overall'}">
                                <div class="col-md-6" >
                                    <div class="">
                                        %{--<strong style='text-transform:capitalize'>Gap In Care</strong>--}%
                                        <div  id="gapInCare"  class="graph-wrapper" style="max-height: 475px;min-height: 475px;;overflow-y: auto;"  ></div>
                                        <div id="gapInCare_More"></div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </g:if>
                            <g:else>

                                <div class="col-md-12" >
                                    <div class="">
                                        <strong style='text-transform:capitalize'>Gap In Care</strong>
                                        <div class="watermark mt10 mb10" style="font-size: 14px;opacity: 0.4;bottom: 0;right: 0;color: red;"><div style="display: inline;text-transform: capitalize">
                                            &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i>
                                            This section is only applicable for Active Population.
                                            &nbsp;&nbsp;</div>
                                        </div>

                                    </div>
                                </div>



                            </g:else>
                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>


        </div>

        <div class="div">
            <div class="col-md-10">
                <h4 class="isEditable" ><span class="color-gray">TOP CONDITION</span></h4>
            </div>
            <a href="javascript:void(0)">
                <div class="col-md-2" onclick="showHide2(this,'cond104','104')"><i class="fa fa-arrow-circle-o-down "></i> Expand</div>
            </a>

            <div class="container-fluid">
                <div class="row-fluid " >
                    <div class="col-md-6">

                        <div class="short-div mb25" id="cond104" style="display: none;">
                            <g:if test="${expclass!='overall'}">

                                <div class="col-md-12">
                                    <div  id="topConditions"  class="graph-wrapper" style="max-height: 475px;min-height: 475px;;overflow-y: auto;" ></div>
                                    <div id="topConditions_More"></div>
                                </div>
                                <div class="clearfix"></div>
                            </g:if>
                            <g:else>

                                <div class="col-md-12">
                                    <strong style='text-transform:capitalize'>Top conditions</strong>
                                    <div class="watermark mt10 mb10" style="font-size: 14px;opacity: 0.4;bottom: 0;right: 0;color: red;"><div style="display: inline;text-transform: capitalize">
                                        &nbsp;&nbsp;<i class="fa fa-exclamation-triangle fa-4 "></i>
                                        This section is only applicable for Active Population.
                                        &nbsp;&nbsp;</div>
                                    </div>
                                </div>


                            </g:else>
                            <div style="clear: both"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb15"></div>
            <div class="sep-border mb15"></div>
            <div style="clear: both"></div>


        </div>

        <div class="col-md-8">
            <div class="col-md-3" style="padding: 10px 10px 10px 0px;"><h4 class="isEditable" ><span class="color-gray"> MEMBERS</span> LIST</h4></div>
            <div class="clearfix"></div>


            <div style="width:172px;">
                <span class="btn btn-gray icon-provider  iconExplorePopulation" style="background-color:#e9e9e9;padding: 5px 5px 5px 5px;border-radius: 5px;margin: -8px 0 0 0;border:1px solid black;">
                    <g:img uri="/images/icon-explore-population.png" style="margin-top: -5px;" alt="Member Count"/>
                    <span class="data-label">Member Count: &nbsp; &nbsp;<span>${data['summary']['totalCounts']}</span></span>
                </span>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-4">
            <div class="col-md-12 text-right" style="padding-right: 0px;">
                %{--<g:link action="exportPopulation" params="[total:total]"  class=" text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt="Export As CSV"/>
                    <span class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span>
                </g:link>--}%
                <g:form action="exportPopulation" name="exportForm" style="display: none;">
                    <g:hiddenField name="total" value="${total}"/>
                </g:form>

                <a href="javascript:void(0)" onclick="exportCsv()"
                   class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span
                        class="isEditable"><g:editMessage code="hPharmacy.export.csv"/></span><input type="hidden"
                                                                                                     value="hPharmacy.container3.table1.options2">
                </a>
            </div>


            <script>

                function exportCsv() {
                    $('#confirm').modal("show").one('click', '#delete', function(e) {
                                $("#exportForm").submit();
                    });
                }

            </script>
            <g:if test="${grails.util.Environment.current.name.equals('development') ||
                    grails.util.Environment.current.name.equals('qc')}">

                    <div class="col-md-12 no-pad-right">
                        <div style="margin-left: 110px;"><p style="border: 1px solid #044f96;margin-top:-9px;z-index: 999;background: #fff;position: absolute;padding: 0 1px 0 1px;">Start Outreach</p></div>
                    </div>
                    <div class="col-md-12 no-pad-right">
                        <div  style="float: right; margin-top: 2px;border: 1px solid #044f96;padding: 14px 7px 2px 0;">
                            <a href="https://app.moqups.com/ramesh.kumar@zakipoint.com/F5FHxlRs/view/page/a655a74a8?ui=0" class="icon-provider  iconExplorePopulation" target="_blank" >
                                <g:img uri="/images/outreach/Email.jpg" alt="Email" title="Email"/>
                            </a>
                            <a href="https://app.moqups.com/ramesh.kumar@zakipoint.com/F5FHxlRs/view/page/a655a74a8?ui=0" class="icon-provider  iconExplorePopulation" target="_blank">
                                <g:img uri="/images/outreach/Mail-letter.jpg" alt="Mail-letter" title="Mail-letter"/>
                            </a>
                            <a href="https://app.moqups.com/ramesh.kumar@zakipoint.com/F5FHxlRs/view/page/a655a74a8?ui=0" class="icon-provider  iconExplorePopulation" target="_blank">
                                <g:img uri="/images/outreach/mApp.png" alt="Mobile App" title="Mobile App"/>
                            </a>
                            <a href="https://app.moqups.com/ramesh.kumar@zakipoint.com/F5FHxlRs/view/page/a655a74a8?ui=0" class="icon-provider  iconExplorePopulation" target="_blank">
                                <g:img uri="/images/outreach/oneOnOne.png" alt="One-on-one meeting or campaign" title="One-on-one meeting or campaign"/>
                            </a>
                            <a href="https://app.moqups.com/ramesh.kumar@zakipoint.com/F5FHxlRs/view/page/a655a74a8?ui=0" class="icon-provider  iconExplorePopulation" target="_blank">
                            <g:img uri="/images/outreach/SMS.png" alt="SMS" title="SMS"/>
                            </a>
                            <a href="https://app.moqups.com/ramesh.kumar@zakipoint.com/F5FHxlRs/view/page/a655a74a8?ui=0" class="icon-provider  iconExplorePopulation" target="_blank">
                            <g:img uri="/images/outreach/Telephone.png" alt="Telephone" title="Telephone"/>
                            </a>
                        </div>

                    </div>
        </g:if>

        </div>


        <div class="col-md-12 mt15">

                <g:if test="${!isEditable}">
                    <div id="topChronicConditionEr">
                        <g:render template="tableDataDetails" model="[data:data.getAt('result_sets'),total:total]"/>
                    </div>
                </g:if><g:else>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                    <thead>
                    <tr>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head1"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head2"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head3"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head4"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head5"/></span></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </g:else>
                <div class="clearfix"></div>
        </div>
    </div>
    <div id="confirm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h2 style="color:red;">WARNING</h2><br>
                    You are proceeding to <abbr title="Protected Health Information/Personally Identifiable Information">PHI/PII</abbr> information. Please check with your manager or <a href="mailto:support@zakipointhealth.com" target="_top">security@zakipointhealth.com</a> first if you have any questions.
                    <br>
                    <br>
                    As per our security requirement and complying with <abbr title="Health Insurance Portability and Accountability Act">"HIPAA"</abbr> regulations, your access to <abbr title="Protected Health Information">PHI</abbr> is logged for reporting purposes.
                    <br>
                    <br>
                    Use of <abbr title="Protected Health Information">PHI</abbr> information is strictly limited to the use cases mentioned in the statement of work and as defined by your security restrictions.
                    <br>
                    <br>
                    Please make sure your device complies with the <abbr title="Health Insurance Portability and Accountability Act">"HIPAA"</abbr> security requirements, for example, encrypted storage, password policy, automatic screen lock etc. Hit cancel if you need any clarifications.
                    <br>
                    <br>
                    Please <span style="color:red">DO NOT</span> proceed if you do not agree with our terms of use.
            </div>

                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">I agree, Proceed to PHI information</button>
                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                </div>
        </div>
        </div>

    </div>
</div>
<script>


    var areasToTarget=false;
    var demographics=false;

    $("#area1").hide();
    $("#area2").hide();
    //        $("#area1").css("visibility","visible");
    //        $("#area2").css("visibility","visible");

    $(function(){

//        $('').tooltip({title: "Feature Under Development", placement: "bottom"});
        $('.info-tooltip').popover();
        showHide($('#cost-details'),'cost2','2');



        //horizontal bar graph
        %{--renderDemographics(${data3['gender']},'gender',null)--}%
        %{--renderDemographics(${data3['eligibility']},'eligibility',null)--}%
        %{--renderDemographics(${data3['age']},'age',${data3.benchmark['ageDistribution']})--}%
        %{--renderDemographics(${data3['topConditions']},'topConditions',${data3.benchmark['chronicConditionUtilization']})--}%
        %{--renderDemographics(${data3['gapInCare']},'gapInCare',null)--}%
        %{--renderDemographics(${data3['location']},'location',null)--}%
        %{--renderDemographics(${data3['divisionWiseErVisitPer1000']},'divisionWiseErVisitPer1000',null)--}%

        //bar diagram
        %{--renderDemographicsBar(${data3['relationshipClass']},'relationship',${datesMap})--}%

        //vertical bar diagram
        %{--renderData(${data2['mailAndGenericOrderPrescription']},'genericDrugs',${datesMap},${data2.benchmark.overAllBenchmark['Pharmacy Scripts Generic Drugs']})--}%
        %{--renderData(${data2['mailAndGenericOrderPrescription']},'mailOrder',${datesMap},${data2.benchmark.overAllBenchmark['Pharmacy Scripts Mail Order']})--}%
        %{--renderData(${data2['mailAndGenericOrderPrescription']},'specialityOrder',${datesMap},0)--}%
        %{--renderData(${data2['mailAndGenericOrderPrescription']},'formulary',${datesMap},0)--}%
        %{--renderData(${data2['erVisitAndAdmissionUsage']},'erVisitper1000',${datesMap},${data2.benchmark.overAllBenchmark['ER Visits']})--}%
        %{--renderData(${data2['erVisitAndAdmissionUsage']},'totalAdmissionsper1000',${datesMap},${data2.benchmark.overAllBenchmark['Total Admissions']})--}%
        %{--renderData(${data2['erVisitAndAdmissionUsage']},'averageLengthOfStay',${datesMap},${data2.benchmark.overAllBenchmark['Average Length of Stay']})--}%
        %{--renderData(${data2['erVisitAndAdmissionUsage']},'thirtyDayReadmitper1000',${datesMap},${data2.benchmark.overAllBenchmark['30 Day ReAdmissions']})--}%
        %{--renderData(${data2['changeInPmpm']},'changeInPmpm',${datesMap},${data2.benchmark.overAllBenchmark['Total Healthplan Claims PMPM']})--}%
        %{--renderData(${data2['pharmacyPmpm']},'pharmacyPmpm',${datesMap},${data2.benchmark.overAllBenchmark['Pharmacy Claims PMPM']})--}%
        %{--renderData(${data2['medicalPmpm']},'medicalPmpm',${datesMap},${data2.benchmark.overAllBenchmark['Medical Claims PMPM']})--}%
    })


    function showHide(curr,ele,reqId){
        var $this=$(curr);
        var $area=$("#"+ele);
        if ($area.is(":hidden")){

//            if(areasToTarget==false) {
                var promise = $.ajax({
                    type: 'POST',
                    data: {areasToTarget: true,requestIndex:reqId},
                    url: '<g:createLink controller="memberSearch" action="searchResult"/>'
                });


                promise.done(function (data2) {
                    showHideIcon1($this, $area,data2);
                    areasToTarget=true;
                }).fail(function (err) {
                    $area.html("No records to display");
                })
            /*}else{
                showHideIcon1($this, $area);

            }*/




        }else{
            $this.html('<i class="fa fa-arrow-circle-o-down "></i> Expand');
            $area.hide()
        }


    }

    function showHide2(curr,ele,reqId){
        var $this=$(curr);
        var $area=$("#"+ele);
        if ($area.is(":hidden")){

//            if(demographics==false) {
                var promise = $.ajax({
                    type: 'POST',
                    data: {demographics: true,requestIndex:reqId},
                    url: '<g:createLink controller="memberSearch" action="searchResult"/>'
                });


                promise.done(function (data2) {
                    showHideIcon2($this, $area,data2);
                    demographics=true;
                }).fail(function (err) {
                    $area.html("No records to display");
                })
            /*}else{
                showHideIcon2($this, $area);

            }*/




        }else{
            $this.html('<i class="fa fa-arrow-circle-o-down "></i> Expand');
            $area.hide()
        }


    }


    var div2 = d3.select(".contentWrapper").append("div")
            .attr("class", "tooltip2")
            .style("opacity", 0);




    function string_sort(a, b) {
        var aName = a.ind.toLowerCase();
        var bName = b.ind.toLowerCase();
        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }

    function numeric_sort(a, b) {
        var aVal = a.val;
        var bVal = b.val;
        return ((aVal > bVal) ? -1 : ((aVal < bVal) ? 1 : 0));
    }




    function showGapInCare(flag){
//        renderDemographics(data3['gapInCare'],'gapInCare',null,flag)
        renderDemographics(gap, 'gapInCare', null,flag)
    }
    function showLocation(flag){
        renderDemographics(loc, 'location', null,flag)
//        renderDemographics(data3['location'],'location',null,flag)
    }
    function showDivisionWiseErVisitPer1000(flag){
//        renderDemographics(data3['divisionWiseErVisitPer1000'],'divisionWiseErVisitPer1000',null,flag)
        renderDemographics(dErVisit, 'divisionWiseErVisitPer1000',null,flag)
    }

    function showTopCond(flag){
//        renderDemographics(data3['topConditions'],'topConditions',bm,flag)
        renderDemographics(tConditions, 'topConditions',tBenchMark,flag)
    }



    function renderDemographics(d,ids,benchmark,showAll){

        $("#"+ids).html("");
        var bigData=[];
        var add=${total};
        var maximum=0;
        var title;
        var counter=0;
        var mainData=d;
        var bMarkVal=benchmark;

        $.each(d, function(i,v) {
//            add+=v;
            if(ids=="gender"){
                if(i=="F")i= "Female"
                if(i=="M")i= "Male"
                bigData.push({ind:i,val:v,title:i})
            }else if(ids=="relationship"){
                bigData.push({ind:i,val:v,title:i})
            }else if(ids=="age"){
//                if(i=="70-above")i= "70+"
                var bm=benchmark[i]
                bigData.push({ind:i,val:v,title:i,bm:bm})
            }
            else if(ids=="gapInCare"){
                title= v.description.toString()
                i= v.metric
                v= v.count
                bigData.push({ind:i,val:v,title:title})
            }else if(ids=="topConditions"){

                if(i.toString().indexOf("_PopRiskSync_per1000") >= 0){
                    title=i.toString().replace("_PopRiskSync_per1000","")

//                if(bm>add) add=bm;
                    if(v>maximum) maximum=v;
                    add=maximum;

                    var percentage=d[title]
                    var mainValue=d[title+"_PopRiskSync"]

                    if(title=="CKD") title="ESRD"

                    var bm=parseFloat(benchmark[title])

                    bigData.push({ind:i,val:Math.round(v),title:title,bm:bm,percentage:percentage,mainValue:mainValue})
                }

            }else if(ids=="location"){
                title= i.toString().split("__");
                title=title[0].toLowerCase()+", "+title[1].toUpperCase()
                bigData.push({ind:i,val:v,title:title})
            }else if(ids=="divisionWiseErVisitPer1000"){
                if(v.erVisitPer1000>maximum) maximum=Math.round(v.erVisitPer1000);
                add=maximum;
                bigData.push({ind:i,val: Math.round(v.erVisitPer1000),title:i,percentage: v.memberCount})
            }else{
                bigData.push({ind:i,val:v,title:i})
            }

            counter++;

        });
        if(ids=="topConditions" || ids=="gapInCare"){
            bigData.sort(numeric_sort);
            if(!showAll)
            bigData=bigData.slice(0,10)
        }else if(ids=="location"){
            bigData.sort(numeric_sort);
            if(!showAll)
            bigData=bigData.slice(0,10)
        }else if(ids=="divisionWiseErVisitPer1000"){
            bigData.sort(numeric_sort);
            if(!showAll)
            bigData=bigData.slice(0,10)
        } else if(ids=="gender"){

            if($.grep(bigData, function(e){ return e.ind === "Male"; }).length==0)
                bigData.push({ind:"Male",val:0,title:"Male"})

                if($.grep(bigData, function(e){ return e.ind === "Female"; })==0)
                    bigData.push({ind:"Female",val:0,title:"Female"})

                    if($.grep(bigData, function(e){ return e.ind === "Unknown"; })==0)
                        bigData.push({ind:"Unknown",val:0,title:"Unknown"})


            bigData.sort(string_sort);
        }else{
            bigData.sort(string_sort);
        }
        var string="";
        var val=0;
        var bm=0;


        string+="";
//        string+='<table  width="100%" border="0" cellspacing="0" cellpadding="0" id='+ids+'>';
        string+='<div style=" width:500px;" >';
        string+='<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-graph-table z5-fixed-header" id='+ids+' >';
//        string+='<table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" id='+ids+' >';
        string+="<thead>";
        string+="<tr><th><strong style='text-transform:capitalize'>"+ids.replace(/([A-Z1])/g, ' $1').trim()+"</strong></th><th width='10%'></th>" +
        "<th width='40%'>";
        if(ids!="topConditions"&&ids!="divisionWiseErVisitPer1000"){
            string+="<div style='width: 100px;border-top: 1px solid black;font-size: 10px;height: 15px; margin-top: 5px;'>" +
            "<div class='hbar-label' style='width: 50px'><div style='margin-left: -5px;'>0%</div></div>" +
            "<div class='hbar-label' style='width: 25px;'><div style='margin-left: -8px;'>50%</div></div>" +
            "<div class='hbar-label' style='width: 25px;border-right: 1px solid black;border-left: none;text-align: right'><div style='margin-right: -16px;'>100%</div></div>" +
            "</div>";
        }
        string+="</th></tr>";
        string+="</thead>";
        string+="<tbody>";

        $.each(bigData, function(i,v) {
            val = (v['val'] / add) * 100;

            var condVal;
            if(ids=="topConditions"){
                condVal= v['mainValue'];
            }else{
                condVal=v['val'].toFixed(1).replace(/\.0+$/, '');
            }
            string+="<tr><td style='text-transform: capitalize'>"+v['title']/*v['ind']*/+"</td><td width='10%' class='color-blue' style='font-weight: bold;text-align: right;'>"+condVal;/* title="+aTitle+"*/
            string+="</td><td width='40%'>";
            string+="<div class='demographics' id="+ids+"_"+i+" >";

            if(v['bm']>=0) {
                if(ids=="age")
                bm=v['bm']
                else
                bm=(v['bm'] / add) * 100;


                var className="";
                if(bm-val>15){
                    className="dataBelowBm"
                }else if(val-bm>15){
                    className="dataAboveBm"
                }else{
                    className="dataNeutral"
                }
                string+='<div class='+className+'  style="width:'+Math.round(val)+'px;"></div>';
                string+='<div class="benchmark1"   style="width:' + Math.round(bm) + 'px;"></div>';
//            string+='<div class="benchmark2"></div>';
            }else{
                string+='<div class="dataNeutral"  style="width:'+val+'px;"></div>';
            }
            string+='</div>';
            string+="</td></tr>";
        });
        /*if(!showAll){
            if(ids=="gapInCare"){
                string+="<tr><td colspan='2' onclick='showGapInCare(true)'><a href='javascript:void(0)'>Show More...</a><td></tr>"
            }
            if(ids=="topConditions"){
                string+="<tr><td colspan='2' onclick='showTopCond(true)'><a href='javascript:void(0)'>Show More...</a><td></tr>"
            }
        }else{
            if(ids=="gapInCare"){
                string+="<tr><td colspan='2' onclick='showGapInCare(false)'><a href='javascript:void(0)'>Show Less...</a><td></tr>"
            }
            if(ids=="topConditions"){
                string+="<tr><td colspan='2' onclick='showTopCond(false)'><a href='javascript:void(0)'>Show Less...</a><td></tr>"
            }
        }*/

        string+="</tbody>";
        string+="</table>";
        string+="</div>";
        var appendString="";

        if(counter>10){



        if(showAll){

            $("#"+ids).css({border:"1px solid #ccc"});

            if(ids=="gapInCare"){
                appendString+="<div onclick='showGapInCare(false)'><a href='javascript:void(0)' class='see-dtl-utilization'>Less <i class='fa fa-caret-up '></i></a></div>"
            }
            if(ids=="topConditions"){
                appendString+="<div onclick='showTopCond(false)'><a href='javascript:void(0)' class='see-dtl-utilization'>Less <i class='fa fa-caret-up '></i></a></div>"
            }
            if(ids=="location"){
                appendString+="<div onclick='showLocation(false)'><a href='javascript:void(0)' class='see-dtl-utilization'>Less <i class='fa fa-caret-up '></i></a></div>"
            }
            if(ids=="divisionWiseErVisitPer1000"){
                appendString+="<div onclick='showDivisionWiseErVisitPer1000(false)'><a href='javascript:void(0)' class='see-dtl-utilization'>Less <i class='fa fa-caret-up '></i></a></div>"
            }

        }else{
            $("#"+ids).css({border:"1px solid #fff"});

            if(ids=="gapInCare"){
                appendString+="<div onclick='showGapInCare(true)'><a href='javascript:void(0)' class='see-dtl-utilization'>More <i class='fa fa-caret-down '></i></a></div>"
            }
            if(ids=="topConditions"){
                appendString+="<div onclick='showTopCond(true)'><a href='javascript:void(0)' class='see-dtl-utilization'>More <i class='fa fa-caret-down '></i></a></div>"
            }
            if(ids=="location"){
                appendString+="<div onclick='showLocation(true)'><a href='javascript:void(0)' class='see-dtl-utilization'>More <i class='fa fa-caret-down '></i></a></div>"
            }
            if(ids=="divisionWiseErVisitPer1000"){
                appendString+="<div onclick='showDivisionWiseErVisitPer1000(true)'><a href='javascript:void(0)' class='see-dtl-utilization'>More <i class='fa fa-caret-down '></i></a></div>"
            }

        }
        }

        $("#"+ids).html(string);
        $("#"+ids+"_More").html(appendString);

        $.each(bigData, function(i,v) {
            var value= v.val;
            $("#"+ids+"_"+i).on('mousemove', function (d,i) {
                var $this=$(this);
                $this.find(".dataAboveBm").addClass("dataAboveBmH")
                $this.find(".dataBelowBm").addClass("dataBelowBmH")
                $this.find(".dataNeutral").addClass("dataNeutralH")
                var label=""
                if(ids=="topConditions" ){
                    label="Members per 1000 :"
                }else if(ids=="divisionWiseErVisitPer1000"){
                    label="ER visits per 1000 :"
                }else{
                    label="Number of members :"

                }

                var p = $(this);
                var total=${total};
                var members=value.toFixed(1).replace(/\.0+$/, '');
                var percentage;
                if(ids=="topConditions") {
                    percentage = ((v.percentage / total) * 100).toFixed(1).replace(/\.0+$/, '');
                } else if(ids=="divisionWiseErVisitPer1000") {
                    percentage = ((v.percentage / total) * 100).toFixed(1).replace(/\.0+$/, '');
                }else{
                    percentage = ((value / total) * 100).toFixed(1).replace(/\.0+$/, '');

                }
                var position = p.offset();
                var left = position.left;
                var top = position.top;
                div2.transition()
                        .duration(100)
                        .style("opacity", 1)
                        .style("color", "black")
//                        .style("background", "white")
                var tooltipHtml="<span class='formatOutput22'>"+label+"<strong>"+members+"</strong></span>";

                    tooltipHtml+="<br><span class='formatOutput22'>% of members from total population: <strong>"+percentage+"</strong></span>";


                if(v.bm>=0) {
                    var bmValue=v.bm.toFixed(2);

                    if(ids=="age") {
                        bmValue+= "%"
                    } else if(ids=="topConditions") {
                        bmValue+= " Members per 1000"
                    }
                    tooltipHtml+="<br><span class='formatOutput22'>National  Benchmark: <strong>"+bmValue+"</strong></span>";
                }
                div2.html(tooltipHtml)
                        .style("left", (left-60) + "px")
                        .style("top", (top-65) + "px");
            })
                    .on("mouseout", function () {
                        var $this=$(this);

                        $this.find(".dataAboveBm").removeClass("dataAboveBmH")
                        $this.find(".dataBelowBm").removeClass("dataBelowBmH")
                        $this.find(".dataNeutral").removeClass("dataNeutralH")
                        div2.transition()
                                .duration(500)
                                .style("opacity", 0);
                    })

        })

        var $tables = $('#'+ids+'.z5-fixed-header');
        $tables.floatThead({
            scrollContainer: function($table){
                return $tables.closest('.graph-wrapper');
            }
        });

    }

    function wrap(text, width) {
        text.each(function() {
            var text = d3.select(this),
                    words = text.text().split(/\s+/).reverse(),
                    word,
                    line = [],
                    lineNumber = 0,
                    lineHeight = 1.1, // ems
                    y = text.attr("y"),
                    dy = parseFloat(text.attr("dy")),
                    tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan").attr("x", -2).attr("y", 10).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
            }
        });
    }


    function custom_sort(a, b) {
        return new Date(a.year) - new Date(b.year);
    }

    /*function formatDates(d) {
        console.log("===================")
        console.log(d)
        var dDate=new Date(d);
        var oldDate=new Date(d);
        var newDate2=oldDate;
        newDate2.setFullYear(newDate2.getFullYear() - 1);
        newDate2.setDate(newDate2.getDate() +1);
        var dateString= (d3.time.format("%b %Y")(newDate2).toString()+" - "+d3.time.format("%b %Y")(dDate).toString())
        return dateString
    }*/

    function formatDates(d) {
        var dDate=new Date(d);
        var oldDate=new Date(d);
        var newDate2=oldDate;
        newDate2.setFullYear(newDate2.getFullYear() - 1);
        var leapCondition=false;

        if((newDate2.getFullYear() % 400 == 0) || ((newDate2.getFullYear() % 4 == 0) && (newDate2.getFullYear() % 100 != 0))){
            leapCondition=true;
        }


        if((newDate2.getMonth()+1)==2 && leapCondition){
            leapCondition=true;
        }else{
            leapCondition=false;
        }

        if(leapCondition){
            newDate2.setDate(newDate2.getDate() +2);

        }else{
            newDate2.setDate(newDate2.getDate() +1);
        }

        var dateString= (d3.time.format("%b %Y")(newDate2).toString()+" - "+d3.time.format("%b %Y")(dDate).toString())
        return dateString;
    }




    function renderData(data,index,datesmap,benchMark) {
        $("#"+index).html("");

        var parseDate = d3.time.format("%Y-%m-%d");
        var k;
        var direction="-";
        var unit="#";
        if(index=="genericDrugs"){
            direction="+";
            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year:k, percent: val.genericOrderPercent, count: val.genericOrder,totals:(val.totalprescriptionCount-val.genericOrder),memberCount:val.genericOrderMemberCount,totalCount:val.totalprescriptionCount };
            });
        }else if(index=="mailOrder"){
            direction="+";
            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: val.mailOrderPercent, count: val.mailOrder,totals:(val.totalprescriptionCount-val.mailOrder),memberCount:val.mailOrderMemberCount,totalCount:val.totalprescriptionCount };
            });
        }else if(index=="specialityOrder"){
            direction="-";
            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: val.specialityOrderPercent, count: val.specialityOrder,totals:(val.totalprescriptionCount-val.specialityOrder),memberCount:val.specialityOrderMemberCount,totalCount:val.totalprescriptionCount };
            });
        }else if(index=="formulary"){
            direction="+";
            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: val.formularyPercent, count: val.formulary,totals:(val.totalprescriptionCount-val.formulary),memberCount:val.formularyMemberCount,totalCount:val.totalprescriptionCount };
            });
        }else if(index=="erVisitper1000"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count:0,totals:val.erVisitper1000,memberCount:${total},totalCount:val.erVisitper1000  };
            });
        }else if(index=="averageLengthOfStay"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count:0,totals:val.averageLengthOfStay,memberCount:${total},totalCount:val.averageLengthOfStay  };
            });
        }else if(index=="thirtyDayReadmitper1000"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count:0,totals:val.thirtyDayReadmitper1000,memberCount:${total},totalCount:val.thirtyDayReadmitper1000  };
            });
        }else if(index=="totalAdmissionsper1000"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:val.totalAdmissionsper1000,memberCount:${total},totalCount:val.totalAdmissionsper1000   };
            });
        }else if(index=="changeInPmpm"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:val.totalPaidAmountPmpm,memberCount:${total},totalCount:val.totalPaidAmountPmpm  };
            });
        }else if(index=="pharmacyPmpm"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:val.totalPaidAmountPmpm,memberCount:${total},totalCount:val.totalPaidAmountPmpm  };
            });
        }else if(index=="medicalPmpm"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:val.totalPaidAmountPmpm,memberCount:${total},totalCount:val.totalPaidAmountPmpm  };
            });
        }else if(index=="concurrentRisk"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:(val.avgScore),memberCount:${total},totalCount:val.avgScore };
            });
        }else if(index=="prospectiveRisk"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:(val.avgScore),memberCount:${total},totalCount:val.avgScore };
            });
        }else if(index=="claimsBelow500"){
            direction="+";
            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:(val.totalClaimsPer1000),memberCount:${total},totalCount:val.totalClaimsPer1000 };
            });
        }else if(index=="claimsOver10k"){
            direction="-";

            data = $.map(data, function(val, key) {
                k=parseDate(new Date(datesmap[key]));
                return { year: k, percent: 0, count: 0,totals:(val.totalClaimsPer1000),memberCount:${total},totalCount:val.totalClaimsPer1000 };
            });
        }else if(index=="GAP_IN_CARE"){
            var val=data;
            $("#"+index).parent().find("strong").text(val['metric']);

            direction=val.expectedDirection
            unit=val.unit;


            data=[{ year: parseDate(new Date(datesmap['current'])), percent: 0, count: 0,totals:(val.currentmetric),memberCount:${total},totalCount:val.currentmetric },
            { year: parseDate(new Date(datesmap['last'])), percent: 0, count: 0,totals:(val.lastmetric),memberCount:${total},totalCount:val.lastmetric },
            { year: parseDate(new Date(datesmap['prior'])), percent: 0, count: 0,totals:(val.priormetric),memberCount:${total},totalCount:val.priormetric }
                ]

        }else if(index=="WELLNESS_ENGAGED"){


            var val=data;
            $("#"+index).parent().find("strong").text(val['metric']);
            direction=val.expectedDirection
            unit=val.unit;

            data=[{ year: parseDate(new Date(datesmap['current'])), percent: 0, count: 0,totals:(val.currentmetric),memberCount:${total},totalCount:val.currentmetric },
            { year: parseDate(new Date(datesmap['last'])), percent: 0, count: 0,totals:(val.lastmetric),memberCount:${total},totalCount:val.lastmetric },
            { year: parseDate(new Date(datesmap['prior'])), percent: 0, count: 0,totals:(val.priormetric),memberCount:${total},totalCount:val.priormetric }
                ]

        }else if(index=="COACHING_ENGAGED"){

            var val=data;
            $("#"+index).parent().find("strong").text(val['metric']);
            direction=val.expectedDirection

            unit=val.unit;
            data=[{ year: parseDate(new Date(datesmap['current'])), percent: 0, count: 0,totals:(val.currentmetric),memberCount:${total},totalCount:val.currentmetric },
            { year: parseDate(new Date(datesmap['last'])), percent: 0, count: 0,totals:(val.lastmetric),memberCount:${total},totalCount:val.lastmetric },
            { year: parseDate(new Date(datesmap['prior'])), percent: 0, count: 0,totals:(val.priormetric),memberCount:${total},totalCount:val.priormetric }
                ]

        }else if(index=="PROJECTED_SAVINGS"){
            var val=data;
            $("#"+index).parent().find("strong").text(val['metric']);
            direction=val.expectedDirection;
            unit=val.unit;
            data=[{ year: parseDate(new Date(datesmap['current'])), percent: 0, count: 0,totals:(val.currentmetric),memberCount:${total},totalCount:val.currentmetric },
            { year: parseDate(new Date(datesmap['last'])), percent: 0, count: 0,totals:(val.lastmetric),memberCount:${total},totalCount:val.lastmetric },
            { year: parseDate(new Date(datesmap['prior'])), percent: 0, count: 0,totals:(val.priormetric),memberCount:${total},totalCount:val.priormetric }
                ]

        }else if(index=="BIOMETRIC_SCORE" ){
            var val=data;
            $("#"+index).parent().find("strong").text(val['metric']);
            direction=val.expectedDirection;
            unit=val.unit;
            data=[{ year: parseDate(new Date(datesmap['current'])), percent: 0, count: 0,totals:(val.currentmetric),memberCount:${total},totalCount:val.currentmetric },
            { year: parseDate(new Date(datesmap['last'])), percent: 0, count: 0,totals:(val.lastmetric),memberCount:${total},totalCount:val.lastmetric },
            { year: parseDate(new Date(datesmap['prior'])), percent: 0, count: 0,totals:(val.priormetric),memberCount:${total},totalCount:val.priormetric }
                ]

        }else if(index=="PROGRAM_IMPACT_IN_AMOUNT" ){
            var val=data;
            $("#"+index).parent().find("strong").text(val['metric']);
            direction=val.expectedDirection;
            unit=val.unit;
            data=[{ year: parseDate(new Date(datesmap['current'])), percent: 0, count: 0,totals:(val.currentmetric),memberCount:${total},totalCount:val.currentmetric },
            { year: parseDate(new Date(datesmap['last'])), percent: 0, count: 0,totals:(val.lastmetric),memberCount:${total},totalCount:val.lastmetric },
            { year: parseDate(new Date(datesmap['prior'])), percent: 0, count: 0,totals:(val.priormetric),memberCount:${total},totalCount:val.priormetric }
                ]

        }else if(index=="HS_RISK_STRATIFICATION" ){
            var val=data;
            $("#"+index).parent().find("strong").text(val['metric']);
            direction=val.expectedDirection;
            unit=val.unit;
            data=[{ year: parseDate(new Date(datesmap['current'])), percent: 0, count: 0,totals:(val.currentmetric),memberCount:${total},totalCount:val.currentmetric },
            { year: parseDate(new Date(datesmap['last'])), percent: 0, count: 0,totals:(val.lastmetric),memberCount:${total},totalCount:val.lastmetric },
            { year: parseDate(new Date(datesmap['prior'])), percent: 0, count: 0,totals:(val.priormetric),memberCount:${total},totalCount:val.priormetric }
                ]

        }else{
            return true;
        }





        data.sort(custom_sort);



        var a1,a2;

        if(index=="specialityOrder"||index=="mailOrder"||index=="genericDrugs"||index=="formulary") {
            a1=data[1].count
            a2=data[2].count
        }else{
            a1=data[1].totalCount
            a2=data[2].totalCount
        }


//        var redNoti=Math.abs(((a2-a1)/a1)*100);
        var redNoti=((a2-a1)/a1)*100;
        if(isNaN(redNoti)) redNoti=0;
        redNoti=isFinite(redNoti)?redNoti:0;

        /*console.log("=================================")
        console.log(index)
        console.log("=========prev======"+a1)
        console.log("=========nowwwwwwwwww======"+a2)
        console.log("=========result======"+redNoti)*/

        var red=false;
        if(redNoti>10 && (direction=="-"))
            red=true;
        else if(redNoti<-10 && (direction=="+")){
            red=true;
        }



        if(red){
            $("#"+index).closest(".col-md-4").find("strong").css({
            "background-color": "red",
            "color": "white",
            "padding": "3px"
            });
        }


        var margin = {top: 60, right: 120, bottom: 40, left: 100},
                width = 330 - margin.left - margin.right,
                height = 200 - margin.top - margin.bottom;

        var x = d3.scale.ordinal()
                .rangeRoundBands([0, 200], .3);

        var y = d3.scale.linear()
                .rangeRound([height, 0]);

        var color = d3.scale.ordinal()
                .range(["#034f96", "#b53322"]);

        var xAxis = d3.svg.axis()
                .scale(x)
                .tickFormat(formatDates)
                .orient("bottom")
                .outerTickSize(0)

        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left")
                .tickFormat(d3.format(".2s"));

        var svg = d3.select("#"+index).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)






        var g1=svg.append("g").attr("transform", "translate(" + 0 + "," + margin.top + ")");

        color.domain(d3.keys(data[0]).filter(function (key) {
            return (key !== "year" && key!="percent" && key!="memberCount" && key!="totalCount" );
        }));
        var agesData=[];


        data.forEach(function (d,i) {
            var y0 = 0;
            d.ages = color.domain().map(function (name) {
                return {name: name, y0: y0, y1: y0 += +d[name],memberCount:d['memberCount'],year:d['year']};
            });
            d.total = d.ages[d.ages.length - 1].y1;
            if(d.total<benchMark){
                d.max=benchMark
            }else{
                d.max= d.total
            }

            if(index=="genericDrugs" || index=="mailOrder"){
                d.bm=((benchMark/100)* d.total).toFixed(1).replace(/\.0+$/,'');
            }else{
                d.bm=benchMark
            }

        });



        x.domain(data.map(function (d) {
            return d.year;
        }));

        y.domain([0, d3.max(data, function (d) {
            return d.max;
        })]);



        var state = g1.selectAll(".state")
                .data(data)
                .enter().append("g")
                .attr("class", "g")
                .attr("transform", function (d) {
                    return "translate(" + x(d.year) + "," + 0 + ")";
                });

        state.selectAll("rect")
                .data(function (d) {
                    return d.ages;
                })
                .enter().append("rect")
                .attr("width", x.rangeBand())
                .attr("y", function (d) {
                    return y(d.y1);
                })
                .attr("height", function (d) {
                    return y(d.y0) - y(d.y1);
                })
                .attr("class", function (d) {
                    if (color(d.name) == "#034f96")
                        return "dbar"
                    else
                        return "hbar"

                }).on('mousemove', function (d, i) {
                    var $elem=d3.select(this.parentNode);
                    $elem.select(".hbar").attr("class","hbarH")
                    $elem.select(".dbar").attr("class","dbarH")

                    d3.select("#"+index+"_"+d.year).attr("r", 6)

                    var total=${total}
                    var members=d.memberCount
                    var percent=(( d.memberCount/total)*100).toFixed(1).replace(/\.0+$/,'')
                    div2.transition()
                            .duration(100)
                            .style("opacity", 1)
                            .style("color", "black")
//                            .style("background", "white")
                    var bmValue=benchMark;

                    if(index=="erVisitper1000"){
                        bmValue+=" ER Visits per 1000"
                    }else if(index=="totalAdmissionsper1000"){
                        bmValue+=" Admissions per 1000"
                    }else if(index=="changeInPmpm"){
                        bmValue+=" PMPM"
                    }else if(index=="mailOrder"){
                        bmValue+="%"
                    }else if(index=="genericDrugs"){
                        bmValue+="%"
                    }else if(index=="averageLengthOfStay"){
                        bmValue+=" Days"
                    }else if(index=="thirtyDayReadmitper1000"){
                        bmValue+=" Admits Per 1000"
                    }else if(index=="medicalPmpm"){
                        bmValue+=" PMPM"
                    }else if(index=="pharmacyPmpm"){
                        bmValue+=" PMPM"
                    }else if(index=="concurrentRisk"){
                        bmValue+=" Score"
                    }else{
                        bmValue+=" #"
                    }

                    if(index=="specialityOrder" || index=="formulary") {

                        div2.html("<span class='formatOutput22'>Number of members: <strong>" + members + "</strong></span>"+
                        "<br><span class='formatOutput22'>% of members from total population: <strong>" + percent + "</strong></span>"
                        ).style("left", (d3.event.pageX - 80) + "px")
                                .style("top", (d3.event.pageY - 60) + "px");
                    }else if(index=="concurrentRisk" || index=="prospectiveRisk" || index=="claimsBelow500" || index=="claimsOver10k"|| index=="WELLNESS_ENGAGED"|| index=="COACHING_ENGAGED"||index=="BIOMETRIC_SCORE"|| index=="GAP_IN_CARE"|| index=="PROJECTED_SAVINGS"|| index=="PROGRAM_IMPACT_IN_AMOUNT"|| index=="HS_RISK_STRATIFICATION" ) {


                        div2.html("<span class='formatOutput22'>Number of members: <strong>" + members + "</strong></span>"
//                        "<br><span class='formatOutput22'>% of members from total population: <strong>" + percent + "</strong></span>"
                        ).style("left", (d3.event.pageX - 80) + "px")
                                .style("top", (d3.event.pageY - 60) + "px");
                        }
                    else if(index=="pharmacyPmpm" || index=="medicalPmpm" || index=="changeInPmpm"|| index=="erVisitper1000"|| index=="totalAdmissionsper1000" || index=="averageLengthOfStay"|| index=="thirtyDayReadmitper1000"){



                        div2.html("<span class='formatOutput22'>Number of members: <strong>" + members + "</strong></span>" +
                        "<br><span>National Benchmark: <strong>" + bmValue + "</strong></span>")
                                .style("left", (d3.event.pageX - 80) + "px")
                                .style("top", (d3.event.pageY - 60) + "px");
                    }
                    else {


                        div2.html("<span class='formatOutput22'>Number of members: <strong>" + members + "</strong></span>" +
                        "<br><span class='formatOutput22'>% of members from total population: <strong>" + percent + "</strong></span>" +
                        "<br><span>National Benchmark: <strong>" + bmValue + "</strong></span>")
                                .style("left", (d3.event.pageX - 80) + "px")
                                .style("top", (d3.event.pageY - 60) + "px");
                    }


                })
                .on("mouseout", function (d,i) {
                    var $elem=d3.select(this.parentNode);
                    $elem.select(".hbarH").attr("class","hbar")
                    $elem.select(".dbarH").attr("class","dbar")

                    d3.select("#"+index+"_"+d.year).attr("r", 3)
                    div2.transition()
                            .duration(500)
                            .style("opacity", 0);


                })

        g1.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .style("font-size", ".60em")
                .call(xAxis)
                .selectAll(".tick text")
                .call(wrap, x.rangeBand());

        //        concurrentRisk prospectiveRisk claimsBelow500 claimsOver10k


        if(index!="specialityOrder" && index!="formulary" && index!="concurrentRisk" && index!="prospectiveRisk" && index!="claimsBelow500" && index!="claimsOver10k" ){




        // Line function.
        var line = d3.svg.line()
                .x(function(d) { return x(d.year)+x.rangeBand()/2;})
                .y(function(d) { return y(d.bm); });


        var linegraph = g1.append("path")
                .datum(data)
                .attr("d", line)
                .style('stroke', 'gray')
                .attr('fill', 'none')
                .style('shape-rendering', 'optimizeSpeed');


        g1.append('g')
                .selectAll('circle')
                .data(data)
                .enter()
                .append('circle')
                .attr("class", "y1")
                .attr("id",  function (d) {
                    return index+"_"+d.year
                })
                .attr("fill", "black")
                .attr("cx", function (d) {
                    return x(d.year)+x.rangeBand()/2;
                })
                .attr("cy", function (d) {
                    return y(d.bm);
                })
                .attr("r", 3).on('mousemove', function (d, i) {
                    var bmValue=benchMark;

                    if(index=="erVisitper1000"){
                        bmValue+=" ER VIsits per 1000"
                    }else if(index=="totalAdmissionsper1000"){
                        bmValue+=" Admissions per 1000"
                    }else if(index=="changeInPmpm"){
                        bmValue+=" PMPM"
                    }else if(index=="mailOrder"){
                        bmValue+="%"
                    }else if(index=="genericDrugs"){
                        bmValue+="%"
                    }else if(index=="averageLengthOfStay"){
                        bmValue+=" Days"
                    }else if(index=="thirtyDayReadmitper1000"){
                        bmValue+=" Admits Per 1000"
                    }else if(index=="medicalPmpm"){
                        bmValue+=" PMPM"
                    }else if(index=="pharmacyPmpm"){
                        bmValue+=" PMPM"
                    }else if(index=="concurrentRisk"){
                        bmValue+=" Score"
                    }else if(index=="prospectiveRisk"){
                        bmValue+=" Score"
                    }else if(index=="claimsBelow500"){
                        bmValue+=" Claims per 1000"
                    }else if(index=="claimsOver10k"){
                        bmValue+=" Claims per 1000"
                    }



                    d3.select(this).attr("r", 7)
                    var total=${total}
                    var members=d.memberCount
                    var percent=(( d.memberCount/total)*100).toFixed(1).replace(/\.0+$/,'')
                    div2.transition()
                            .duration(100)
                            .style("opacity", 1)
                            .style("color", "black")


//                            .style("background", "white")
                    /*div2.html("<span class='formatOutput22'>Number of members: <strong>"+members+"</strong></span>" +
                    "<br><span class='formatOutput22'>% of members from total population: <strong>"+ percent+"</strong></span>" +
                    "<br><span>National Benchmark: <strong>"+ bmValue+"</strong></span>")
                            .style("left", (d3.event.pageX-80) + "px")
                            .style("top", (d3.event.pageY -60) + "px");*/


                    if(index=="pharmacyPmpm" || index=="medicalPmpm" || index=="changeInPmpm"|| index=="erVisitper1000"|| index=="totalAdmissionsper1000"){



                        div2.html("<span class='formatOutput22'>Number of members: <strong>" + members + "</strong></span>" +
                        "<br><span>National Benchmark: <strong>" + bmValue + "</strong></span>")
                                .style("left", (d3.event.pageX - 80) + "px")
                                .style("top", (d3.event.pageY - 60) + "px");
                    }
                    else {


                        div2.html("<span class='formatOutput22'>Number of members: <strong>" + members + "</strong></span>" +
                        "<br><span class='formatOutput22'>% of members from total population: <strong>" + percent + "</strong></span>" +
                        "<br><span>National Benchmark: <strong>" + bmValue + "</strong></span>")
                                .style("left", (d3.event.pageX - 80) + "px")
                                .style("top", (d3.event.pageY - 60) + "px");
                    }
                })
                .on("mouseout", function (d,i) {
                    d3.select(this).attr("r", 3)
                    div2.transition()
                            .duration(500)
                            .style("opacity", 0);
                })
       /* g1.append('g')
                .selectAll('circle')
                .data(data)
                .enter()
                .append('circle')
                .attr("class", "y2")
                .attr("fill", "none")
                .attr("stroke", "red")
                .attr("cx", function (d) {
                    return x(d.year)+x.rangeBand()/2;
                })
                .attr("cy", function (d) {
                    return y(benchMark);
                })
                .attr("r", 6);*/
        }


        //to remove sub heading i.e percent sign from graph, no filling by dark blue color
        if(index!="totalAdmissionsper1000" && index!="erVisitper1000"&& index!="changeInPmpm"&& index!="averageLengthOfStay"&& index!="thirtyDayReadmitper1000"&& index!="pharmacyPmpm"&& index!="medicalPmpm" && index!="concurrentRisk" && index!="prospectiveRisk" && index!="claimsBelow500" && index!="claimsOver10k"&& index!="COACHING_ENGAGED"&& index!="PROJECTED_SAVINGS"&& index!="GAP_IN_CARE"&& index!="BIOMETRIC_SCORE"&& index!="WELLNESS_ENGAGED"&& index!="PROGRAM_IMPACT_IN_AMOUNT"&& index!="HS_RISK_STRATIFICATION"){
            svg.selectAll(".text").data(data).enter()
                    .append("text")
                    .attr("class", "text")
                    .attr("transform", function (d, i) {
                        return "translate(" + ((i * 60)+20) + "," + 40 + ")";
                    })
                    .text(function (d, i) {
                        return d.percent.toFixed(2).replace(/\.0+$/,'') + "%"
                    }).attr("font-family", "sans-serif")
                    .attr("font-size", "11px")
                    .attr("font-weight", "bold")
                    .attr("fill", "#023563");
        }


        // showing two color graphs, % filled also
        if(index=="specialityOrder"||index=="mailOrder"||index=="genericDrugs"||index=="formulary") {


            var legend = svg.selectAll(".legend")
                    .data(color.domain().slice())
                    .enter().append("g")
                    .attr("class", "legend")
                    .attr("transform", function (d, i) {
                        return "translate(100," + (i*22+5) + ")";
                    });

            legend.append("text")
                    .attr("x", width)
                    .attr("y", 9)
                    .attr("dy", ".30em")
                    .style("text-anchor", "start")
//                    .style("fill",color)
                    .style("fill", function (d) {
                        if (d == "totals")
                            d = "#023563"
                        else
                            d = "#023563"
                        return d;
                    })
                    .attr("font-weight", "bold")
                    .text(function (d) {
                        if (d == "totals")
                            d = "% Filled"
                        else
                            d = "Total Prescriptions"
                        return d;
                    });
        }

        // for main heading title legends for  those having not two set of datas ...i.e. filled

        if(index=="changeInPmpm" ||index=="medicalPmpm" ||index=="pharmacyPmpm" || index=="totalAdmissionsper1000" || index=="erVisitper1000"|| index=="thirtyDayReadmitper1000"|| index=="averageLengthOfStay" || index=="concurrentRisk" || index=="prospectiveRisk" || index=="claimsBelow500" || index=="claimsOver10k" || index=="WELLNESS_ENGAGED"|| index=="COACHING_ENGAGED"||index=="BIOMETRIC_SCORE"|| index=="GAP_IN_CARE"|| index=="PROJECTED_SAVINGS"|| index=="PROGRAM_IMPACT_IN_AMOUNT"|| index=="PROGRAM_IMPACT_IN_AMOUNT") {
            var titleText;


            if(index=="changeInPmpm" ) titleText="PMPM"
            else if(index=="thirtyDayReadmitper1000" ) titleText="per 1000"
            else if(index=="erVisitper1000" ) titleText="per 1000"
            else if(index=="totalAdmissionsper1000" ) titleText="per 1000"
            else if(index=="medicalPmpm" ) titleText="PMPM"
            else if(index=="pharmacyPmpm" ) titleText="PMPM"
             else if(index=="concurrentRisk"){
                titleText=" Score"
            }else if(index=="prospectiveRisk"){
                titleText=" Score"
            }else if(index=="claimsBelow500"){
                titleText=" per 1000"
            }else if(index=="claimsOver10k"){
                titleText=" per 1000"
            }else {
                titleText=""+unit
            }


            var legend = svg.selectAll(".legend")
                    .data(['totals'])
                    .enter().append("g")
                    .attr("class", "legend")
                    .attr("transform", function (d, i) {
                        return "translate(100," + (i*22+7) + ")";
                    });

            legend.append("text")
                    .attr("x", width)
                    .attr("y", 9)
                    .attr("dy", ".30em")
                    .style("text-anchor", "start")
                    .style("font-size", "11px")
//                    .style("fill",color)
                    .style("fill", function (d) {
                            d = "#023563"
                        return d;
                    })
                    .attr("font-weight", "bold")
                    .text(function (d) {
                            d = titleText
                        return d;
                    });
        }

        svg.selectAll(".text1").data(data).enter()
                .append("text")
                .attr("class", "text1")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 60)+20) + "," + 20 + ")";
                })
                .text(function (d, i) {
                    return d3.format(",")(d.totalCount.toFixed(2).replace(/\.0+$/,''));
//                    return (d3.format(".2f")(d.totalCount));

                }).attr("font-family", "sans-serif")
                .attr("font-size", "11px")
                .attr("font-weight", "bold")
                .attr("fill", "#023563");
    };

    var gap,tConditions,tBenchMark,loc,dErVisit;

        function showHideIcon1(ele,tar,data2){

            var $this=$('#galleryImages');
            $this.show();
            tar.show();

            setTimeout(function(){
                ele.html('<i class="fa fa-arrow-circle-o-up"></i> Collapse');

                $this.hide();

                if(data2['areas_2']){
                    renderData(data2['areas_2']['changeInPmpm'], 'changeInPmpm', data2.datesMap, data2['areas_2'].benchmark.overAllBenchmark['Total Healthplan Claims PMPM'])
                    renderData(data2['areas_2']['pharmacyPmpm'], 'pharmacyPmpm', data2.datesMap, data2['areas_2'].benchmark.overAllBenchmark['Pharmacy Claims PMPM'])
                    renderData(data2['areas_2']['medicalPmpm'], 'medicalPmpm', data2.datesMap, data2['areas_2'].benchmark.overAllBenchmark['Medical Claims PMPM'])
                }

                if(data2['areas_3']){
                    renderData(data2['areas_3']['concurrentRisk'], 'concurrentRisk', data2.datesMap, 0)
                    renderData(data2['areas_3']['prospectiveRisk'], 'prospectiveRisk', data2.datesMap, 0)
                }


                if(data2['areas_9']){
//                    renderSankey(data2['areas_9']['MARA_RISK_LEVEL']['reporting'], 'maraRisk',data2['areas_9'].years , data2['areas_9'].levels)
//                    renderSankey(data2['areas_9']['HS_RISK_LEVEL']['reporting'], 'maraRisk',data2['areas_9'].years , data2['areas_9'].levels)
                }



                if(data2['areas_8']){
                    if(data2['areas_8']['newMetrics']){

//                        COACHING_ENGAGED    GAP_IN_CARE   WELLNESS_ENGAGED    PROJECTED_SAVINGS   BIOMETRIC_SCORE
                            if(data2['areas_8']['newMetrics']['GAP_IN_CARE']){
                                renderData(data2['areas_8']['newMetrics']['GAP_IN_CARE'], 'GAP_IN_CARE', data2.datesMap, 0)
                            }else{
                                $("#GAP_IN_CARE").html("No records to display");
                            }

                            if(data2['areas_8']['newMetrics']['WELLNESS_ENGAGED']){
                                renderData(data2['areas_8']['newMetrics']['WELLNESS_ENGAGED'], 'WELLNESS_ENGAGED', data2.datesMap, 0)

                            }else{
                                $("#WELLNESS_ENGAGED").html("No records to display");
                            }

                            if(data2['areas_8']['newMetrics']['COACHING_ENGAGED']){
                                renderData(data2['areas_8']['newMetrics']['COACHING_ENGAGED'], 'COACHING_ENGAGED', data2.datesMap, 0)

                            }else{
                                $("#COACHING_ENGAGED").html("No records to display");
                            }

                            if(data2['areas_8']['newMetrics']['BIOMETRIC_SCORE']){
                                renderData(data2['areas_8']['newMetrics']['BIOMETRIC_SCORE'], 'BIOMETRIC_SCORE', data2.datesMap, 0)

                            }else{
                                $("#BIOMETRIC_SCORE").html("No records to display");
                            }

                            if(data2['areas_8']['newMetrics']['PROJECTED_SAVINGS']){
                                renderData(data2['areas_8']['newMetrics']['PROJECTED_SAVINGS'], 'PROJECTED_SAVINGS', data2.datesMap, 0)

                            }else{
                                $("#PROJECTED_SAVINGS").html("No records to display");
                            }

                        if(data2['areas_8']['newMetrics']['PROGRAM_IMPACT_IN_AMOUNT']){
                                renderData(data2['areas_8']['newMetrics']['PROGRAM_IMPACT_IN_AMOUNT'], 'PROGRAM_IMPACT_IN_AMOUNT', data2.datesMap, 0)

                            }else{
                                $("#PROGRAM_IMPACT_IN_AMOUNT").html("No records to display");
                            }

                        if(data2['areas_8']['newMetrics']['HS_RISK_STRATIFICATION']){
                                renderData(data2['areas_8']['newMetrics']['HS_RISK_STRATIFICATION'], 'HS_RISK_STRATIFICATION', data2.datesMap, 0)

                            }else{
                                $("#HS_RISK_STRATIFICATION").html("No records to display");

                            }

                        }else {

                        $("#GAP_IN_CARE").html("No records to display");
                        $("#WELLNESS_ENGAGED").html("No records to display");
                        $("#COACHING_ENGAGED").html("No records to display");
                        $("#BIOMETRIC_SCORE").html("No records to display");
                        $("#PROJECTED_SAVINGS").html("No records to display");
                        $("#HS_RISK_STRATIFICATION").html("No records to display");
                        $("#PROGRAM_IMPACT_IN_AMOUNT").html("No records to display");



                    }

                    }






                    if(data2['areas_4']) {
                        renderData(data2['areas_4']['claimsBelow500'], 'claimsBelow500', data2.datesMap, 0)
                        renderData(data2['areas_4']['claimsOver10k'], 'claimsOver10k', data2.datesMap, 0)

                    }

                    if(data2['areas_5']) {


                        renderData(data2['areas_5']['erVisitAndAdmissionUsage'], 'erVisitper1000', data2.datesMap, data2['areas_5'].benchmark.overAllBenchmark['ER Visits'])
                        renderData(data2['areas_5']['erVisitAndAdmissionUsage'], 'totalAdmissionsper1000', data2.datesMap, data2['areas_5'].benchmark.overAllBenchmark['Total Admissions'])
                        renderData(data2['areas_5']['erVisitAndAdmissionUsage'], 'averageLengthOfStay', data2.datesMap, data2['areas_5'].benchmark.overAllBenchmark['Average Length of Stay'])
                        renderData(data2['areas_5']['erVisitAndAdmissionUsage'], 'thirtyDayReadmitper1000', data2.datesMap, data2['areas_5'].benchmark.overAllBenchmark['30 Day ReAdmissions'])


                    }

                    if(data2['areas_6']) {


                        renderData(data2['areas_6']['mailAndGenericOrderPrescription'], 'genericDrugs', data2.datesMap, data2['areas_6'].benchmark.overAllBenchmark['Pharmacy Scripts Generic Drugs'])
                        renderData(data2['areas_6']['mailAndGenericOrderPrescription'], 'mailOrder', data2.datesMap, data2['areas_6'].benchmark.overAllBenchmark['Pharmacy Scripts Mail Order'])
                        renderData(data2['areas_6']['mailAndGenericOrderPrescription'], 'specialityOrder', data2.datesMap, 0)
                        renderData(data2['areas_6']['mailAndGenericOrderPrescription'], 'formulary', data2.datesMap, 0)
                    }



            },500)
        }

        function showHideIcon2(ele,tar,data3){
            var $this=$('#galleryImages');
            $this.show();
            setTimeout(function(){




                ele.html('<i class="fa fa-arrow-circle-o-up"></i> Collapse');
                tar.show()
                $this.hide();


                //horizontal bar graph
                if(data3['demographics_101']) {
                    loc = data3['demographics_101']['location'];
                    renderDemographics(data3['demographics_101']['gender'], 'gender', null)
                    renderDemographics(data3['demographics_101']['eligibility'], 'eligibility', null)
                    renderDemographics(data3['demographics_101']['age'], 'age', data3['demographics_101'].benchmark['ageDistribution'])
                    renderDemographics(loc, 'location', null)
                }
                if(data3['demographics_102']){
                    dErVisit=data3['demographics_102']['divisionWiseErVisitPer1000'];
                    renderDemographics(dErVisit, 'divisionWiseErVisitPer1000', null)

                }

                if(data3['demographics_103']){
                    gap = data3['demographics_103']['gapInCare'];
                    renderDemographics(gap, 'gapInCare', null)

                }

                if(data3['demographics_104']){
                    tConditions = data3['demographics_104']['topConditions'];
                    tBenchMark = data3['demographics_104'].benchmark['chronicConditionUtilization'];
                    renderDemographics(tConditions, 'topConditions', tBenchMark)


                }

                //bar diagram
                if(data3['demographics_7']){
                    renderDemographicsBar(data3['demographics_7']['relationshipClass'], 'relationship', data3.datesMap)

                }



            },500)
        }


    $(function(){

        $(".collapse1").on("click",function(e){
            var $this=$(this);
            var area=$("#area1");
            if (area.is(":hidden")){

                if(areasToTarget==false) {
                    var promise = $.ajax({
                        type: 'POST',
                        data: {areasToTarget: true},
                        url: '<g:createLink controller="memberSearch" action="searchResult"/>'
                    });

                    promise.done(function (data2) {
                        showHideIcon1($this, area,data2);
                        areasToTarget=true;
                    }).fail(function (err) {
                        $("#area1").html("No records to display");
                    })
                }else{
                    showHideIcon1($this, area);

                }




            }else{
                $this.html('<i class="fa fa-arrow-circle-o-down "></i> Expand');
                area.hide()
            }


        })


        $(".collapse2").on("click",function(e){
            var $this=$(this);
            var area=$("#area2");
            if (area.is(":hidden")){

                if(demographics==false) {

                    var promise = $.ajax({
                        type: 'POST',
                        data: {demographics: true},
                        url: '<g:createLink controller="memberSearch" action="searchResult"/>'
                    });


                    promise.done(function (data3) {
                            showHideIcon2($this, area,data3);




                            demographics=true;
                    }).fail(function (err) {
                        $("#area2").html("No records to display");
                    })
                }else{
                    showHideIcon2($this, area);
                }


            }else{
                $this.html('<i class="fa fa-arrow-circle-o-down "></i> Expand');
                area.hide()

            }

        })




        $("[name='switchPop']").bootstrapSwitch();
        $('input[name="switchPop"]').on('switchChange.bootstrapSwitch', function(event, state) {
//            console.log(this); // DOM element
//            console.log(event); // jQuery event
//            console.log(state); // true | false
            if(state){
                $("#allPop").click();
                $('#galleryImages').show();

            }else{
                $("#currentPop").click();
                $('#galleryImages').show();

            }
        });


    })

    function renderDemographicsBar(data,ids,datesMap) {
        $("#"+ids).html("");
        var obj;
        var parseDate = d3.time.format("%Y-%m-%d");

        data=$.map(data,function(v,k){
//            obj= v;
            obj=new Object();
            obj.Other= v.Other;
            obj.Spouse= v.Spouse;
            obj.Dependent= v.Dependent;
            obj.Employee= v.Employee;
            obj.year=parseDate(new Date(datesMap[k]))
            return obj;

        });
        data.sort(custom_sort);

//        return;

        var margin = {top: 20, right: 10, bottom: 40, left: 20},
                width = 500 - margin.left - margin.right,
                height = 400 - margin.top - margin.bottom;

        var x = d3.scale.ordinal()
                .rangeRoundBands([0, 300], .3);

        var y = d3.scale.linear()
                .rangeRound([height, 0]);

        var color = d3.scale.ordinal()
                .range(["grey","#01A46D","#5B9BD5","#ED7D31"]);

//        var colors ={"Other":"grey","Employee":"#5B9BD5","Dependent":"#ED7D31"};


        var xAxis = d3.svg.axis()
                .scale(x)
                .tickFormat(formatDates)
                .orient("bottom");

        var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left")
                .tickFormat(d3.format(".2s"));

        var svg = d3.select("#"+ids).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + -25 + "," + margin.top + ")");




        color.domain(d3.keys(data[0]).filter(function (key) {
            return (key !== "year")
//            return (key !== "year" && key !== "inNetwork" && key !== "reportingDates" && key !== "totalPaidAmount"&& key !== "inNetwork" && key !== "unknown"&& key !== "outOfNetwork");
        }));



        data.forEach(function (d) {
            var y0 = 0;
            d.ages = color.domain().map(function (name) {
//                    console.log(name)
                return {name: name, y0: y0, y1: y0 += +d[name]};
            });
            d.total = d.ages[d.ages.length - 1].y1;
        });

        data.sort(function (a, b) {
            return b.order - a.order;
        });

        x.domain(data.map(function (d) {
            return d.year;
        }));
        var yearValues = data.map(function (d) {
            return d.year;
        });
        y.domain([0, d3.max(data, function (d) {
            return d.total * 1.4;
        })]);

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .style("font-size", ".9em")
                .call(xAxis)
                .selectAll(".tick text")
                .call(wrap, x.rangeBand());


        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Population");

        var state = svg.selectAll(".state")
                .data(data)
                .enter().append("g")
                .attr("class", "g")
                .attr("transform", function (d) {
                    return "translate(" + x(d.year) + ",0)";
                });

        state.selectAll("rect")
                .data(function (d) {
                    return d.ages;
                })
                .enter().append("rect")
                .attr("width", x.rangeBand())
                .attr("y", function (d) {
                    return y(d.y1);
                })
                .attr("height", function (d) {
                    return y(d.y0) - y(d.y1);
                })
                .style("fill", function(d) {
                    return color(d.name); })

        var legend = svg.selectAll(".legend")
                .data(color.domain().slice().reverse())
                .enter().append("g")
                .attr("class", "legend")
                .attr("transform", function (d, i) {
                    return "translate(0," + (i * 22 - 15) + ")";
                });

        legend.append("rect")
                .attr("x", width - 18)
                .attr("width", 18)
                .attr("height", 18)
                .style("fill", function(d) {
                    return color(d);
                })

        legend.append("text")
                .attr("x", width - 165)
                .attr("y", 9)
                .attr("dy", ".30em")
                .style("text-anchor", "start")
                .style("fill", function(d) {
                    return color(d); })
                .attr("font-weight", "bold")
                .attr("font-size", "11px")
                .text(function (d) {
                    var text;
                    if(d=="Employee")
                        text=" Employee"
                    if(d=="Dependent")
                        text="Dependent"
                    if(d=="Spouse")
                        text=" Spouse"
                    if(d=="Other")
                        text=" Other"
                    return text;
                });


        var textAppend = svg.selectAll(".text").data(data).enter()
                .append("text")
                .attr("class", "text")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 90) + 30) + "," + 22 + ")";
                })
                .text(function (d, i) {
                    return d3.round(d.Dependent, 1)
                }).attr("font-family", "sans-serif")
                .attr("font-size", "11px")
                .attr("font-weight", "bold")
                .attr("fill",function(d) { return color("Dependent"); });

        var textAppend4 = svg.selectAll(".text5").data(data).enter()
                .append("text")
                .attr("class", "text5")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 90) + 30) + "," + 42 + ")";
                })
                .text(function (d, i) {
                    return d3.round(d.Spouse, 1)
                }).attr("font-family", "sans-serif")
                .attr("font-size", "11px")
                .attr("font-weight", "bold")
                .attr("fill", function(d){return color("Spouse")});

        var textAppend2 = svg.selectAll(".text1").data(data).enter()
                .append("text")
                .attr("class", "text1")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 90) + 30) + "," + 64 + ")";
                })
                .text(function (d, i) {
                    return d3.round(d.Other, 1)
                }).attr("font-family", "sans-serif")
                .attr("font-size", "11px")
                .attr("font-weight", "bold")
                .attr("fill", function(d) { return color("Other"); });



        var textAppend3 = svg.selectAll(".text2").data(data).enter()
                .append("text")
                .attr("class", "text2")
                .attr("transform", function (d, i) {
                    return "translate(" + ((i * 90) + 30) + "," + 0 + ")";
                })
                .text(function (d, i) {
                    return d3.round(d.Employee, 1)
                }).attr("font-family", "sans-serif")
                .attr("font-size", "11px")
                .attr("font-weight", "bold")
                .attr("fill", function(d){return color("Employee")});
    }

    function renderSankey(data,id,yearsMain,levels){
        var colores_g = {"High":"#DC3912","Medium":"#FF7F0E", "Normal":"#E7BA52","Low":"#2CA02C"};

        var data1=data;
        var units = "Member";

        var col=new Object();


        $.each(levels.sort(custom_sort),function(ind,val) {
                    col[val]=colores_g[val];
                }
        );




        // set the dimensions and margins of the graph
        var margin = {top: 10, right: 10, bottom: 10, left: 10},
                width = 400 - margin.left - margin.right,
                height = 300 - margin.top - margin.bottom;

        // format variables
        var formatNumber = d3.format(",.0f"),    // zero decimal places
                format = function(d) { return formatNumber(d) + " " + units; },
//                 color = d3.scaleOrdinal(["#01A46D","#FF9B2B","#EC3E40"]);
                color = d3.scaleOrdinal(["#FF0000","#F48E9F","#A9D08E"]);

        // append the svg object to the body of the page
        var svg = d3.select("#"+id).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        // Set the sankey diagram properties
        var sankey = d3.sankey()
                .nodeWidth(26)
                .nodePadding(40)
                .size([width, height]);

        var path = sankey.link();

        // load the data
        //    d3.json("sankey.json", function(error, graph) {


        graph=data1;
        sankey
                .nodes(graph.nodes)
                .links(graph.links)
                .layout(32);

//         console.log(graph.links);

        // add in the links
        var link = svg.append("g").selectAll(".linkNodes")
                .data(graph.links)
                .enter().append("g").attr("class", "linkNodes")


        link.append("path")
                .attr("class", "link")
                .style("stroke", function(d) {
                    return col[d.source.name]
                })
                .attr("d", path)
                .style("stroke-width", function(d) { return Math.max(0, d.dy); })
//                .sort(function(a, b) { return b.dy - a.dy; });

        // add the link titles
        link.append("title")
                .text(function(d) {
                    return d.source.name + " → " +
                            d.target.name + "\n" + format(d.value); });

        // add in the nodes
        var node = svg.append("g").selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")"; });


        // add the rectangles for the nodes
        node
                .append("rect")
                .attr("height", function(d) {
                    return d.dy; })
                .attr("width", sankey.nodeWidth())
                .style("fill", function(d) {
                    return d.color = col[d.name];
//                     return d.color = color(d.name.replace(/ .*/, ""));
                })
                .style("stroke", function(d) {
                    return d3.rgb(d.color).darker(2); })
                .append("title")
                .append("h6")
                .text(function(d) {

                    return /*d.name*/"Total:\n" + format(d.value)+ "\nNewly Enrolled:\n" + format(d.total-d.sumvalue)+"\n" })


        // add in the title for the nodes
        node.append("text")
                .attr("x", -6)
                .attr("y", function(d) { return d.dy / 2; })
                .attr("dy", ".25em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .attr("class","textClass")
//                .style("text-transform","capitalize")
                .text(function(d) { return d.name; })
                .filter(function(d) { return d.x < width / 2; })
                .attr("x", 6 + sankey.nodeWidth())
                .attr("text-anchor", "start")

        var years=["last"," ","current"];


        var nodess = svg.append("g").selectAll(".nodes")
//                 .data([graph.nodes[0],graph.nodes[3],graph.nodes[6]])
                .data([2,349,696])
                .enter().append("g").attr("class", "nodes").attr("transform", function(d) {
                    return "translate(" + (d) + "," + 0 + ")"; })

        nodess.append("text")
                .attr("x", 26)
                .attr("y", function(d) { return -30 ; })
                .attr("dy", ".85em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .attr("class","textClass1")
//                 .style("text-transform","capitalize")
                .text(function(d,i) {
                    return yearsMain[years[i]];
                })
//                .filter(function(d) { return d.x < width / 2; })
//                .attr("x", 6 + sankey.nodeWidth())
//                .attr("text-anchor", "start")

        // the function for moving the nodes
        function dragmove(d) {
            d3.select(this).attr("transform",
                    "translate(" + (
                            d.x = Math.max(0, Math.min(width - d.dx, d3.event.x))
                    )
                    + "," + (
                            d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))
                    ) + ")");
            sankey.relayout();
            link.attr("d", path);
        }

    }


</script>


<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">

<style type="text/css">

.x.axis text{
    font: 7px sans-serif !important;
}
.textClass{
    text-transform: capitalize;
}
.textClass1{
    font-weight: bold;
    font-size: 14px;
}
.node rect {
    fill-opacity: .9;
    shape-rendering: crispEdges;
}

.node text {
    pointer-events: none;
    text-shadow: 0 1px 0 #fff;
}

.link {
    fill: none;
    stroke: #000;
    stroke-opacity: .5;
}

.link:hover {
    stroke-opacity: .8;
}

.pathA{
    stroke:blue;
}
.pathB{
    stroke:steelblue;
}
.pathC{
    stroke:orange;
}

.node rect {
    /*cursor: move;*/
    fill-opacity: .9;
    shape-rendering: crispEdges;
}

.node text {
    pointer-events: none;
    text-shadow: 0 1px 0 #fff;
}

.popover{
    min-width:200px;
    max-width:800px;
}

.popover-title{
    font-weight: bold;
}

.popover-content{
    font-size: 12px;
}

.dataBelowBmH {
    background-color: greenyellow  !important;
}

.dataNeutralH {
    background-color: #023583  !important;
}


.dataAboveBmH {
    background-color: red !important;
}


    .dbarH{
        fill : #023583 !important;
    }

    .hbarH{
        fill : #D8DFFF !important ;
        /*fill : #BDD7EE !important ;*/

    }

div.tooltip2 {
    position: absolute;
    text-align: center;
    max-width: 300px;
    min-width: 100px;
    text-align: left; padding: 5px 5px;
    line-height: 23px;
    font: 10px sans-serif;
    background:#e7e7e7;
    /*background: lightsteelblue;*/
    /*border: 1px solid white;*/
    border-radius: 5px;
    pointer-events: none;
    white-space: pre;
    z-index: 1000;
}

.hbar {
    fill: #D7DFE5;

}


.x.axis path {
    /*display: none;*/
}

    .icon-provider{
        padding-left:8px;
    }

</style>


<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-switch.js')}"></script>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'bootstrap-switch.css')}">


%{--<script src="https://d3js.org/d3.v4.min.js"></script>--}%
%{--<script language="javascript" type="text/javascript" src="${resource(dir: 'js/dash', file: 'sankey.js')}"></script>--}%

</body>

</html>