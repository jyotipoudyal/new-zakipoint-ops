<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>ER Utilization</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
%{--<link href="${resource(dir:"css",file: "layout.css")}" rel="stylesheet"/>--}%
    <script>
        function exportOptions(){
            $("#exportOptions").modal("show");
            
        }
    </script>
</head>

<body>


<div  id="contentWrapper" class="contentWrapper">

    <div class="container">
        <div class="col-md-6">
            <g:link controller="erUtilization" action="index" class="text-link-upper mb15">
                <i class="fa fa-angle-double-left iconRmargin"></i>
                <span class="isEditable"><g:editMessage code="erUtilization.costDrivers.top.navigation"/> </span>
            </g:link>
        </div>
        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
            <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                    <g:link title="Edit Content" controller="cms" action="costDriversDetails"><i class="fa fa-edit"></i> Edit Content</g:link>
                </g:if><g:else>
                <g:link title="View Changes" controller="erUtilization" action="costDriversDetails"><i class="fa fa-eye"></i> View Changes</g:link>
                %{--<a title="View Changes" class="switch-edit-mode"><i class="fa fa-eye"></i></a>--}%
                </g:else>
            </sec:ifAllGranted>
            </li>

        </div>

        <div class="clearfix"></div>
        <div class="sep-border mb25 pt15"></div>
        <div class="col-md-8 mb15">
            <div class="clearfix"></div>
            <h3 class="mb25" id="graph1Title">
                <span class="isEditable"><g:editMessage code="erUtilization.costDrivers.graph1title"/> </span>

            </h3>



            <g:if test="${!isEditable}">
                <div id="table3Graph">
                    <g:render template="topCondition"/>
                </div>
            </g:if>



        </div>

        <div class="col-md-4 text-right">
            %{--<a href="#" class="btn-link-upper btn-blue mb15 isEditable"><g:editMessage code="erUtilization.container1.graph1.option1"/></a>--}%
            <a href="#" class="custom-tooltip btn-link-upper btn-light-blue inline-block mb15 isEditable"><g:editMessage code="erUtilization.container1.graph1.option2"/></a><br/>

            %{--<a href="#" class="btn-link-upper btn-gray mb15 isEditable"><g:editMessage code="erUtilization.container1.graph1.option3"/></a> --}%

            %{--<a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.explore.population"/></span><input type="hidden" value="erUtilization.container3.table1.options1"></a>--}%
            <g:link action="exportCsv" params="[exportModule:'topConditionByCostAvoidEr']"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span></g:link>
            %{--<a href="javascript:void(0)" onclick="exportOptions()"  class="text-link-upper iconExportCSV" ><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span></a>--}%
            <g:hiddenField name="reportingBasis1" value="${session.er.reportingBasis}"/>
            <g:hiddenField name="includeOn" value="${session.er.isExclude}"/>
            <g:hiddenField name="range1" value="${session.er.range}"/>
        </div>

        <div class="clearfix"></div>
        <div class="sep-border"></div>
        <div class="col-md-6">
            <h3 class="mb25" id="graph2Title">
                <span class="isEditable"><g:editMessage code="erUtilization.costDrivers.graph2title"/> </span>
            </h3>
            <g:if test="${!isEditable}">
                <div id="lineGraph2">
                </div>
                <div class="lineLegends" style="margin-top: -20px;display: none;">
                    <svg width="500" height="20">
                        <g transform="translate(150,6)">
                            <g><rect y="0" fill="red" x="0" height="10" width="33"></rect>
                                <text class="text_y" text-anchor="end" transform="translate(88,10)">UC Visits</text></g>
                        </g>
                        <g transform="translate(300,6)">
                            <g><rect y="0" fill="grey" x="0" height="10" width="33"></rect>
                                <text class="text_y" text-anchor="end" transform="translate(126,10)">Avoidable Visits</text></g>
                        </g>
                    </svg>
                </div>
            </g:if>
        </div>
        <div class="col-md-6 text-right">
            <g:link action="exportCsv" params="[exportModule:'avoidableErVsUrgentVisit']"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span></g:link>

        </div>
        <div class="clearfix"></div>
        %{--last line graph --}%
        <div class="sep-border mb25 pt15"></div>
        <div class="col-md-6">
            <h3 class="mb25">
                <span class="isEditable"><g:editMessage code="erUtilization.costDrivers.graph3title"/></span>

            </h3>
        </div>
        <div class="col-md-6 text-right">
            <g:link action="exportCsv" params="[exportModule:'careAlertErVisit']"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span></g:link>

        </div>
        <div class="clearfix"></div>
        <g:if test="${!isEditable}">
                <div class="col-md-7">

                    <div id="lineGraph4" ></div>
                    <div class="lineLegends" style="margin-top: -20px;display: none;">
                        <svg width="500" height="35">
                            <g transform="translate(140,6)">
                                <g><rect y="0" fill="red" x="0" height="10" width="33"></rect>
                                    <text class="text_y" text-anchor="begin" transform="translate(34,9)">Members not using</text>
                                    <text class="text_y" text-anchor="begin" transform="translate(34,22)">Preventive Care</text>
                                </g>
                            </g>
                            <g transform="translate(300,6)">
                                <g><rect y="0" fill="grey" x="0" height="10" width="33"></rect>
                                    <text class="text_y" text-anchor="begin" transform="translate(34,9)">Members not using</text>
                                    <text class="text_y" text-anchor="begin" transform="translate(34,22)">Routine Care</text>
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
            </g:if>



    </div>

</div>

<div id="exportOptions" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Choose to export
            </div>
            <g:form action="exportCsv">
            <div class="modal-body">
                    <g:hiddenField name="exportModule" value="costDriverDetails"/>
                    <g:checkBox name="module1" checked="false" value="topConditionByCostAvoidEr"/>Top Condition By Cost Avoid Er <br>
                    <g:checkBox name="module2" checked="false" value="avoidableErVsUrgentVisit"/>Avoidable Er Vs Urgent Visit <br>
                    <g:checkBox name="module3" checked="false" value="careAlertErVisit"/>Care Alert Er Visit
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >Export</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            </div>
                </g:form>

        </div>
    </div>
</div>

<style>


.hbar {
    /*mask: url(#mask-stripe)*/
    /*fill: url(#mask-stripe);*/

    fill: url(#pattern-stripe) !important;
}

/*.z5-table table td:nth-child(3) {
    border-right:solid 1px #ccc;
}
.z5-table table th:nth-child(3) {
    border-right:solid 1px #ccc;
}*/

.z5-table table th{
    border-top:1px solid #ccc;
    padding:4px; text-align:center;
    text-transform:uppercase;
    font-weight:normal;
}



.bar {
    fill: steelblue;
}

.lineColor {
    fill: none;
    stroke: #1c94c4;
    stroke-width: 1px;
    stroke-dasharray:6 3
}
.dbar {fill: #034f96;}



.x.axis path {
    /*display: none;*/
}

</style>
<g:javascript>

    <g:if test="${!isEditable}">
        var parseEndDate = d3.time.format("%Y-%m-%d").parse;
        %{--var dataset=["through ${cycleEndDate}"]--}%
         render3Data(${dataPh2});
         render4Data(${dataPh4});

        $(".lineLegends").show();
    </g:if>
 function custom_sort(a, b) {
            return a.date - b.date;
        }

        var div = d3.select(".contentWrapper").append("div")
                .attr("class", "tooltip")
                .style("opacity", 0);


        function render3Data(da){
            $("#lineGraph2").html("");

            /*var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
             main_width = 500 - main_margin.left - main_margin.right,
             main_height =300 - main_margin.top - main_margin.bottom;*/

            var main_margin = {top: 20, right: 60, bottom: 50, left: 60},
                    main_width = 500 - main_margin.left - main_margin.right,
                    main_height = 300 - main_margin.top - main_margin.bottom;
            var pharmacyClaims = da;
            var newVal;
            var data = [];
            var parseDate4 = d3.time.format("%Y-%m-%d").parse;


            $.each(pharmacyClaims,function(key,val) {
                newVal = new Object();
                newVal.time1=parseDate4(val['year']);
                newVal.actual=val['urgentCareVisitsFilter']
                newVal.expected=val['avoidableErVisit']
                data.push(newVal);

            });
            data.sort(custom_sort);
//            console.log(data);


            var bisectDate = d3.bisector(function(d) { return d.time1; }).left,
                    formatOutput1 = function(d) { return Math.round(d.expected) +" Avoidable ER visits / 1000 "; },
                    formatOutput0 = function(d) { return Math.round(d.actual) +" Urgent Care visits / 1000 "; };

            var main_x = d3.time.scale()
                    .range([0, main_width]);

            var main_y = d3.scale.linear()
                    .range([main_height, 0]);

            /*var main_xAxis = d3.svg.axis()
                    .scale(main_x)
                //.tickFormat(d3.time.format("%Y%b"))
                    .outerTickSize(0)
                    .ticks(d3.time.months, 1)
                    .tickFormat(d3.time.format('%b'))
                //.ticks(6)
                    .orient("bottom");*/
            var dateTicks=data.map(function(d){return d.time1});
            var main_xAxis2 =d3.svg.axis()
                    .scale(main_x)
                    //.ticks(d3.time.year)
                //.tickValues( main_x.domain())
                //    .tickFormat(d3.time.format("%Y"))
                .tickValues(dateTicks)
                    .tickFormat(formatDates)
                    .tickSize(5,0)
                    .orient("bottom");

            var main_yAxisLeft = d3.svg.axis()
                    .scale(main_y)
                    .ticks(5)
                    .tickSize(-main_width)
                    .outerTickSize(0)
                    .orient("left");

            var main_line0 = d3.svg.line()
                //.interpolate("basis")
                    .x(function(d) { return main_x(d.time1); })
                    .y(function(d) { return main_y(d.actual); });

            var main_line1 = d3.svg.line()
                //.interpolate("basis")
                    .x(function(d) { return main_x(d.time1); })
                    .y(function(d) { return main_y(d.expected); });

            var svg = d3.select("#lineGraph2").append("svg")
                    .attr("width", main_width + main_margin.left + main_margin.right)
                    .attr("height", main_height + main_margin.top + main_margin.bottom);

            svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("width", main_width)
                    .attr("height", main_height);

            var main = svg.append("g")
                    .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");


%{--d3.csv(paths, function(error, data) {
    data.forEach(function(d) {
        d.time1 = parseDate(d.time1);
        d.expected = +d.expected;
        d.actual = +d.actual;
    });--}%



    data.sort(function(a, b) {
        return a.time1 - b.time1;
    });



    var value1=Math.round(data[data.length-1].actual);
    var value2=Math.round(data[data.length-1].expected);


    main_x.domain([data[0].time1, data[data.length - 1].time1]);
    var array_y0=d3.extent(data, function(d) { return d.actual; });
    var array_y1=d3.extent(data, function(d) { return d.expected; });
    array_y0=array_y0.concat(array_y1);
    main_y.domain([0,d3.max(array_y0)+20]);


    /*main.append("g")
            .attr("class", "x axis xaxisLeft")
            .attr("transform", "translate(0," + main_height + ")")
            .call(main_xAxis);*/

    main.append("g")
            .attr("class", "x axis xaxisLeft")
            .attr("transform", "translate(0," + (main_height) + ")")
            .call(main_xAxis2);

    %{-- var main_xAxis1 =d3.svg.axis()
                    .scale(main_x)
                      .tickValues([data[data.length - 1].time1])
                    .tickSize(0)
//                    .tickFormat(d3.time.format("%b"))
                    .tickFormat(function(d,i) { return dataset[i]; })
                    .orient("bottom");

            main.append("g")
                    .attr("class", "x axis xaxisLeft2")
                    .attr("transform", "translate(0," + (main_height+18) + ")")
                    .call(main_xAxis1);--}%
%{--.append("text")
.attr("class", "text_x")
.attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
.attr("transform", "translate("+ (main_width/2) +",30)");  // centre below axis
//.text("TIME");--}%

    main.append("g")
            .attr("class", "y axis yaxisLeft")
            .call(main_yAxisLeft)
            .selectAll("text")
                    .attr("x", -20);

    main.selectAll("line")
            .attr("stroke", "#ccc");

    main.selectAll(".tick")
            .attr("stroke-dasharray", "3 3")
            .attr("opacity", ".5");

    main.append("text")
            .attr("class", "text_y")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate("+ (-42) +","+(main_height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
            .text(" VISITS / 1000");


    var path1=main.append("path")
//                    .attr("clip-path", "url(#clip)")
            .attr("class", "line line0")
            .attr("d", main_line0(data))
            .style("stroke","red")
            .style("fill", "none")
            .style('stroke-width', '1.2px');


    var path2=main.append("path")
//                    .attr("clip-path", "url(#clip)")
            .attr("class", "line line1")
            .attr("d", main_line1(data))
            .style('stroke', 'gray')
            .style("fill", "none")
            .style('stroke-width', '1.2px');



    var totalLength = path1.node().getTotalLength();

    path1
            //.attr("stroke-dasharray", totalLength + " " + totalLength)
            .attr("stroke-dashoffset", totalLength)
            .transition()
            .duration(600)
            .ease("linear")
            .attr("stroke-dashoffset", 0)
            .each('end', function () {
                //alert(value1);
//                        $("#actualValue").html("<span class='label-value'>$"+value1+"</span><span class='label-unit'>/1000</span><span class='label-title'>actual</span>");
                    });

            path2
                    //.attr("stroke-dasharray", totalLength + " " + totalLength)
                    .attr("stroke-dashoffset", totalLength)
                    .transition()
                    .duration(600)
                    .ease("linear")
                    .attr("stroke-dashoffset", 0)
                    .each('end', function () {
//                        $("#expectedValue").html("<span class='label-value'>$"+value2+"</span><span class='label-unit'>/1000</span><span class='label-title'>benchmark</span>");
//                        $(".graph1-label").fadeIn();
                    });



            var focus = main.append("g")
                    .attr("class", "focus")
                    .style("display", "block");

            focus.append("line")
                    .attr("class", "x")
                    .attr("y1", 0);

            focus.append("circle")
                    .attr("class", "y0")
                    .attr("r", 4);

            focus.append("text")
                    .attr("class", "y0")
                    .attr("dy", "-1em");

            focus.append("circle")
                    .attr("class", "y1")
                    .attr("r", 4);

            focus.append("text")
                    .attr("class", "y1")
                    .attr("dy", "-1em");

                     focus.style("visibility", "hidden");

                     main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y0")
                    .attr("cx", function(d) { return main_x(d.time1) ; })
                    .attr("cy", function(d) { return main_y(d.actual); })
                    .attr("r", 4);

            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y1")
                    .attr("cx", function(d) { return main_x(d.time1) ; })
                    .attr("cy", function(d) { return main_y(d.expected); })
                    .attr("r", 4);


            main.append("rect")
                    .attr("class", "overlay")
                    .attr("fill","none")
                    .attr("pointer-events","all")
                    .attr("width", main_width)
                    .attr("height", main_height)
                    .on("mouseover", function() { focus.style("display", null); })
                    .on("mouseout", function() {
                        div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        focus.style("display", "none");
                    })
                    .on("mousemove", mousemove);

            function mousemove() {
            focus.style("visibility", "visible");
                var x0 = main_x.invert(d3.mouse(this)[0]),
                        i = bisectDate(data, x0, 1),
                        d0 = data[i - 1],
                        d1 = data[i],
                        d = x0 - d0.time1 > d1.time1 - x0 ? d1 : d0;
                focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");
                focus.select("text.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");//.text(formatOutput0(d));
                focus.select("circle.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");
                focus.select("text.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");//.text(formatOutput1(d));
                focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));
                //focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));

                div.transition()
                        .duration(100)
                        .style("opacity", 1);
                div .html("<span class='formatOutput0'>"+formatOutput0(d)+"</span><br><br><span class='formatOutput1'>"+formatOutput1(d)+"</span>")
                        .style("left", (d3.event.pageX +30) + "px")
                        .style("top", (d3.event.pageY - 120) + "px");

                //focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.actual) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.expected) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(0, " + main_y(d.expected) + ")").attr("x1", main_x(d.time1));
            }

        }

         %{--function formatDates(d) {
            var oldDate=new Date(d);
            var newDate2=oldDate;
            newDate2.setFullYear(newDate2.getFullYear() - 1);
            newDate2.setDate(newDate2.getDate() +1);
            var dateString= (d3.time.format("%b %Y")(newDate2).toString()+" - "+d3.time.format("%b %Y")(d).toString())
            return dateString

        }--}%

    function formatDates(d) {
            var oldDate=new Date(d);
            var newDate2=oldDate;
            newDate2.setFullYear(newDate2.getFullYear() - 1);
            var leapCondition=false;

            if((newDate2.getFullYear() % 400 == 0) || ((newDate2.getFullYear() % 4 == 0) && (newDate2.getFullYear() % 100 != 0))){
                leapCondition=true;
            }


            if((newDate2.getMonth()+1)==2 && leapCondition){
                leapCondition=true;
            }else{
                leapCondition=false;
            }

            if(leapCondition){
                newDate2.setDate(newDate2.getDate() +2);

            }else{
                newDate2.setDate(newDate2.getDate() +1);
            }

            var dateString= (d3.time.format("%b %Y")(newDate2).toString()+" - "+d3.time.format("%b %Y")(d).toString())
            return dateString;
        }




    function render4Data(da){
            $("#lineGraph4").html("");

            /*var main_margin = {top: 20, right: 50, bottom: 50, left: 50},
             main_width = 500 - main_margin.left - main_margin.right,
             main_height =300 - main_margin.top - main_margin.bottom;*/

            var main_margin = {top: 20, right: 60, bottom: 50, left: 60},
                    main_width = 500 - main_margin.left - main_margin.right,
                    main_height = 300 - main_margin.top - main_margin.bottom;
            var pharmacyClaims = da;
            var newVal;
            var data = [];
            var parseDate4 = d3.time.format("%Y-%m-%d").parse;


            $.each(pharmacyClaims,function(key,val) {
                newVal = new Object();
                newVal.time1=parseDate4(val['year']);
                newVal.actual=val['noPreventiveVisit']
                newVal.expected=val['noRoutineVisit']
                data.push(newVal);
//                console.log(data)
            });
            data.sort(custom_sort);


            var bisectDate = d3.bisector(function(d) { return d.time1; }).left,
                    formatOutput0 = function(d) { return Math.round(d.actual) +" ER visits / 1000 for members \n not using preventive care"; },
                    formatOutput1 = function(d) { return Math.round(d.expected) +" ER visits / 1000 for members \n not using routine care"; };

            var main_x = d3.time.scale()
                    .range([0, main_width]);

            var main_y = d3.scale.linear()
                    .range([main_height, 0]);

            /*var main_xAxis = d3.svg.axis()
                    .scale(main_x)
                //.tickFormat(d3.time.format("%Y%b"))
                    .outerTickSize(0)
                    .ticks(d3.time.months, 1)
                    .tickFormat(d3.time.format('%b'))
                //.ticks(6)
                    .orient("bottom");*/


            var dateTicks=data.map(function(d){return d.time1});
            var main_xAxis2 =d3.svg.axis()
                    .scale(main_x)
                    //.ticks(d3.time.year)
                //.tickValues( main_x.domain())
                //    .tickFormat(d3.time.format("%Y"))
                .tickValues(dateTicks)
                    .tickFormat(formatDates)
                    .tickSize(5,0)
                    .orient("bottom");

            %{--var main_xAxis2 =d3.svg.axis()
                    .scale(main_x)
                    .ticks(d3.time.year)
                //.tickValues( main_x.domain())
                    .tickFormat(d3.time.format("%Y"))
                    .tickSize(5,0)
                    .orient("bottom");--}%

            var main_yAxisLeft = d3.svg.axis()
                    .scale(main_y)
                    .ticks(5)
                    .tickSize(-main_width)
                    .outerTickSize(0)
                    .orient("left");

            var main_line0 = d3.svg.line()
                //.interpolate("basis")
                    .x(function(d) { return main_x(d.time1); })
                    .y(function(d) { return main_y(d.actual); });

            var main_line1 = d3.svg.line()
                //.interpolate("basis")
                    .x(function(d) { return main_x(d.time1); })
                    .y(function(d) { return main_y(d.expected); });

            var svg = d3.select("#lineGraph4").append("svg")
                    .attr("width", main_width + main_margin.left + main_margin.right)
                    .attr("height", main_height + main_margin.top + main_margin.bottom);

            svg.append("defs").append("clipPath")
                    .attr("id", "clip")
                    .append("rect")
                    .attr("width", main_width)
                    .attr("height", main_height);

            var main = svg.append("g")
                    .attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");


%{--d3.csv(paths, function(error, data) {
    data.forEach(function(d) {
        d.time1 = parseDate(d.time1);
        d.expected = +d.expected;
        d.actual = +d.actual;
    });--}%



    data.sort(function(a, b) {
        return a.time1 - b.time1;
    });





    var value1=Math.round(data[data.length-1].actual);
    var value2=Math.round(data[data.length-1].expected);


    main_x.domain([data[0].time1, data[data.length - 1].time1]);
    var array_y0=d3.extent(data, function(d) { return d.actual; });
    var array_y1=d3.extent(data, function(d) { return d.expected; });
    array_y0=array_y0.concat(array_y1);
    main_y.domain([0,d3.max(array_y0)+20]);


    /*main.append("g")
            .attr("class", "x axis xaxisLeft")
            .attr("transform", "translate(0," + main_height + ")")
            .call(main_xAxis);*/

    main.append("g")
            .attr("class", "x axis xaxisLeft")
            .attr("transform", "translate(0," + (main_height) + ")")
            .call(main_xAxis2);

     %{--var main_xAxis1 =d3.svg.axis()
                    .scale(main_x)
                      .tickValues([data[data.length - 1].time1])
                    .tickSize(0)
//                    .tickFormat(d3.time.format("%b"))
                    .tickFormat(function(d,i) { return dataset[i]; })
                    .orient("bottom");--}%

           %{-- main.append("g")
                    .attr("class", "x axis xaxisLeft2")
                    .attr("transform", "translate(0," + (main_height+18) + ")")
                    .call(main_xAxis1);--}%
%{--.append("text")
.attr("class", "text_x")
.attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
.attr("transform", "translate("+ (main_width/2) +",30)");  // centre below axis
//.text("TIME");--}%

    main.append("g")
            .attr("class", "y axis yaxisLeft")
            .call(main_yAxisLeft)
            .selectAll("text")
                    .attr("x", -20)

    main.selectAll("line")
            .attr("stroke", "#ccc");

    main.selectAll(".tick")
            .attr("stroke-dasharray", "3 3")
            .attr("opacity", ".5");

    main.append("text")
            .attr("class", "text_y")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate("+ (-42) +","+(main_height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
            .text("ER VISITS / 1000");


    var path1=main.append("path")
//                    .attr("clip-path", "url(#clip)")
            .attr("class", "line line0")
            .attr("d", main_line0(data))
            .style("stroke","red")
            .style("fill", "none")
            .style('stroke-width', '1.2px');


    var path2=main.append("path")
//                    .attr("clip-path", "url(#clip)")
            .attr("class", "line line1")
            .attr("d", main_line1(data))
            .style('stroke', 'gray')
            .style("fill", "none")
            .style('stroke-width', '1.2px');



    var totalLength = path1.node().getTotalLength();

    path1
            //.attr("stroke-dasharray", totalLength + " " + totalLength)
            .attr("stroke-dashoffset", totalLength)
            .transition()
            .duration(600)
            .ease("linear")
            .attr("stroke-dashoffset", 0)
            .each('end', function () {
                //alert(value1);
//                        $("#actualValue").html("<span class='label-value'>$"+value1+"</span><span class='label-unit'>/1000</span><span class='label-title'>actual</span>");
                    });

            path2
                    //.attr("stroke-dasharray", totalLength + " " + totalLength)
                    .attr("stroke-dashoffset", totalLength)
                    .transition()
                    .duration(600)
                    .ease("linear")
                    .attr("stroke-dashoffset", 0)
                    .each('end', function () {
//                        $("#expectedValue").html("<span class='label-value'>$"+value2+"</span><span class='label-unit'>/1000</span><span class='label-title'>benchmark</span>");
//                        $(".graph1-label").fadeIn();
                    });



            var focus = main.append("g")
                    .attr("class", "focus")
                    .style("display", "block");

            focus.append("line")
                    .attr("class", "x")
                    .attr("y1", 0);

            focus.append("circle")
                    .attr("class", "y0")
                    .attr("r", 4);

            focus.append("text")
                    .attr("class", "y0")
                    .attr("dy", "-1em");

            focus.append("circle")
                    .attr("class", "y1")
                    .attr("r", 4);

            focus.append("text")
                    .attr("class", "y1")
                    .attr("dy", "-1em");

            focus.style("visibility", "hidden");

            main.append("rect")
                    .attr("class", "overlay")
                    .attr("fill","none")
                    .attr("pointer-events","all")
                    .attr("width", main_width)
                    .attr("height", main_height)
                    .on("mouseover", function() { focus.style("display", null); })
                    .on("mouseout", function() {
                        div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        focus.style("display", "none");
                    })
                    .on("mousemove", mousemove);

            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y0")
                    .attr("cx", function(d) { return main_x(d.time1) ; })
                    .attr("cy", function(d) { return main_y(d.actual); })
                    .attr("r", 4);

            main.append('g')
                    .selectAll('circle')
                    .data(data)
                    .enter()
                    .append('circle')
                    .attr("class", "y1")
                    .attr("cx", function(d) { return main_x(d.time1) ; })
                    .attr("cy", function(d) { return main_y(d.expected); })
                    .attr("r", 4);



            function mousemove() {
                focus.style("visibility", "visible");
                var x0 = main_x.invert(d3.mouse(this)[0]),
                        i = bisectDate(data, x0, 1),
                        d0 = data[i - 1],
                        d1 = data[i],
                        d = x0 - d0.time1 > d1.time1 - x0 ? d1 : d0;
                focus.select("circle.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");
                focus.select("text.y0").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.actual) + ")");//.text(formatOutput0(d));
                focus.select("circle.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");
                focus.select("text.y1").attr("transform", "translate(" + main_x(d.time1) + "," + main_y(d.expected) + ")");//.text(formatOutput1(d));
                focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));
                //focus.select(".x").attr("transform", "translate(" + main_x(d.time1) + ","+main_y(d.expected>d.actual?d.expected:d.actual)+")").attr("y2",main_height-main_y(d.expected>d.actual?d.expected:d.actual));

                div.transition()
                        .duration(100)
                        .style("opacity",.9);
                div .html("<span class='formatOutput0'>"+formatOutput0(d)+"</span><br><br><span class='formatOutput1'>"+formatOutput1(d)+"</span>")
                        .style("left", (d3.event.pageX +30) + "px")
                        .style("top", (d3.event.pageY - 120) + "px");

                //focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.actual) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(" + main_width * -1 + ", " + main_y(d.expected) + ")").attr("x2", main_width + main_x(d.time1));
                //focus.select(".y1").attr("transform", "translate(0, " + main_y(d.expected) + ")").attr("x1", main_x(d.time1));
            }

        };
</g:javascript>

<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'graph2.css')}">


</html>