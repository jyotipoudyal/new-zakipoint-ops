<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="z5-table put-border">
    <g:hiddenField name="reportingBasis1" value="${session.er.reportingBasis}"/>
    <g:hiddenField name="includeOn" value="${session.er.isExclude}"/>
    <g:hiddenField name="range1" value="${session.er.range}"/>
    <table width="100%" id="dataDetails" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
        <thead>
        <tr style="font-size: 15px;">
            <th class="text-left isEditable" ><g:editMessage code="erUtilization.container4.table1.head1"/></th>
            %{--<util:remoteSortableColumn  class="text-center ${defaultSort}"  property="description" title="${editMessage(code:"erUtilization.container4.table1.head1")}" update="topChronicConditionEr" action="sortResult" defaultOrder="asc" />--}%
            <util:remoteSortableColumn  class="text-left"  property="member" title="Chronic Condition Member Count" update="topChronicConditionEr" action="sortResult" params="[show:show]" />
            <util:remoteSortableColumn  class="text-left"  property="erVisitor" title="ER Visited Chronic Condition Member Count" update="topChronicConditionEr" action="sortResult" params="[show:show]" />
            <util:remoteSortableColumn class="text-left" style="width: 90px;"  property="erCost" title="${editMessage(code:"erUtilization.container4.table1.head5")}" update="topChronicConditionEr" action="sortResult" params="[show:show]"/>

            <util:remoteSortableColumn  class="text-left ${defaultSort}" style="width: 60px;"  property="percentOfnonErVisitor" title="${editMessage(code:"erUtilization.container4.table1.head2")}" update="topChronicConditionEr" action="sortResult" params="[show:show]" />

            <util:remoteSortableColumn    class="text-left" style="width: 60px;"  property="percentOfoneTimeVisitor" title="${editMessage(code:"erUtilization.container4.table1.head3")}" update="topChronicConditionEr" action="sortResult" params="[show:show]"/>
            <util:remoteSortableColumn   class="text-left" style="width: 60px;"  property="percentOftwoOrMoreTimesVisitor" title="${editMessage(code:"erUtilization.container4.table1.head4")}" update="topChronicConditionEr" action="sortResult" params="[show:show]"/>
        </tr>
        </thead>
        <tbody>
        <g:if test="${data}">

            <g:each in="${data}" var="data1" status="i">
                <%
                    def rowNum=(i%2);
                %>
                <tr class="${show?'tableHead':''} z5-row-${rowNum} %{--chrc_${data1.total.chronicCode}--}%">
                    <td class="outerData" style="cursor: pointer;" %{--onclick="showHideColumn('${data1.total.description.toString().split(" ").join("")}')"--}%>
                        ${data1.total.description}
                    </td>

                    <td>
                           ${data1.total.member}
                   </td>
                    <td >
                        <g:z5Link disabledLink="${(data1.total.erVisitor)<=0?'true':'false'}" controller="memberSearch" action="searchResult" params="${data1.total?.members_drill}">
                            ${data1.total.erVisitor} </g:z5Link>
                        </td>
                    <td  >
                        %{--$<g:formatNumber number="${data1.total.erCost}" type="number" maxFractionDigits="0" />--}%
                        <g:formatZ5Currency number="${data1.total.erCost}" type="number" maxFractionDigits="2" /></td>

                    <td ><g:formatNumber number="${data1.total.percentOfnonErVisitor}" type="number" maxFractionDigits="0" />%</td>

                    <td  > <g:formatNumber number="${data1.total.percentOfoneTimeVisitor}" type="number" maxFractionDigits="0" />%</td>
                    <td  > <g:formatNumber number="${data1.total.percentOftwoOrMoreTimesVisitor}" type="number" maxFractionDigits="0"/> %</td>

                </tr>

                        <%
                            def desc=data1.total.description;
                            def chronicCode=data1.total.chronicCode;
                             data1.remove('total');
                            if(data1){
                                data1.sort{it.value.description}.each {
                        %>
                <g:if test="${show}">
                    %{--<style>
                        .z5-table{
                            height:600px !important;
                        }
                    </style>--}%
                    <tr class="%{--chrc_${chronicCode}--}% z5-row-${rowNum} boldInnerDetails innerData ${desc.toString().split(" ").join("")}" style="border: 1px solid gray;">
                %{--</g:if>
                <g:else>
                <tr class="z5-row-${rowNum} boldInnerDetails innerData ${desc.toString().split(" ").join("")}" style="display: none;border: 1px solid gray;">
                </g:else>--}%


                        <td>${it.value.description}

                        </td>
                        <td >-</td>
                        <td >${it.value.erVisitor}</td>
                <td>
                    %{--$ <g:formatNumber number="${it.value.erCost}" type="number" maxFractionDigits="0" />--}%
                    <g:formatZ5Currency number="${it.value.erCost}" type="number" maxFractionDigits="2" />

                </td>
                        <td><g:formatNumber number="${it.value.percentOfnonErVisitor}" type="number" maxFractionDigits="0" /> %</td>
                        <td><g:formatNumber number="${it.value.percentOfoneTimeVisitor}" type="number" maxFractionDigits="0" /> %</td>
                        <td><g:formatNumber number="${it.value.percentOftwoOrMoreTimesVisitor}" type="number" maxFractionDigits="0" /> %</td>

                    </tr>
                </g:if>

                    <%

                        }
                        }
                    %>

            </g:each>

        </g:if><g:else>
            <tr>
                <td colspan="8">
                    No records to display.
                </td>
            </tr>
        </g:else>


        </tbody>

    </table>
</div>

<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<script>
   /* $( document ).ready(function() {
        var $table = $('table.z5-fixed-header');
//        var $table = $('#dataDetails');
        $table.floatThead({
            scrollContainer: function ($table) {
                return $table.closest('.z5-table');
            }
        });

        *//*$.each($(".z5-fixed-header>thead>tr th a:not(.sorted)"),function(){


            $(this).addClass(currentSort+"-disabled");

        })*//*
        });*/
   $(document).ready(function() {
       var $table = $('table.z5-fixed-header');
       $table.floatThead({
           scrollContainer: function ($table) {
               return $table.closest('.z5-table');
           }
       });

   });

</script>






