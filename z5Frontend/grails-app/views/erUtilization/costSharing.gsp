<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 11/4/2015
  Time: 3:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Z Solution Armen</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<body>

<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-silder.css')}" type="text/css">
<script type="text/javascript">
    /*var slider = new Slider('#ex1', {
     formatter: function(value) {
     return  value +"%"
     }
     ,tooltip:"always"


     });*/

    /*var slider = new Slider("#ex1", {
     formatter: function(value) {
     return  value +"%"
     }
     ,tooltip:"always"
     ,value: 50
     ,tooltip_position:'bottom'
     });*/


    /*var slider = new Slider(".erSlider", {
     ticks: [0, 50, 100],
     ticks_labels: ['0%', '50%', '100%'],
     ticks_snap_bounds: 1,
     value: 0,
     //        tooltip_position:'bottom',
     tooltip:"always"
     });

     $(".slider-tick").remove();*/


    function bindSlider(id,percent){
        new Slider("#"+id, {
            formatter: function(value) {
                return  value +"%"
            }
            ,tooltip:"always"
            ,value: percent
//            ,tooltip_position:'bottom'
            ,ticks: [0, 50, 100]
            ,ticks_labels: ['0%', '50%', '100%']
            ,ticks_snap_bounds: 1

        });

        $(".slider-tick").remove();






        $("#"+id).on("slide", function(slideEvt) {


            var ids=$(this).attr("id");
            var percent=slideEvt.value;
            var val1=$("#"+ids+"_inp").val();
            var newAmount=Math.round((val1*percent)/100);
            var prefix = d3.formatPrefix(Math.round(newAmount));
            var amount="$"+prefix.scale(Math.round(newAmount)).toFixed(2)+" k";
            $("#"+ids+"_raw").val(newAmount);
            $("#"+ids+"_out").text(amount);


            var totals=0;
            $(".ex_data").each(function(){
                var val=parseInt($(this).val());
                totals+=val;
            });
            prefix = d3.formatPrefix(Math.round(totals));

//            $("#totalAmount").text(d3.format("$,.2s")(Math.round(totals)))
            $("#totalAmount").text("$"+prefix.scale(Math.round(totals)).toFixed(2)+" k")

        });

        $("#"+id).on("change", function(slideEvt) {

            var ids=$(this).attr("id");
            var percent=slideEvt.value.newValue;
            var val2=$("#"+ids+"_inp").val();
            var newAmount=Math.round((val2*percent)/100);
            var prefix = d3.formatPrefix(Math.round(newAmount));
            var amount="$"+prefix.scale(Math.round(newAmount)).toFixed(2)+" k";
            $("#"+ids+"_raw").val(newAmount);
            $("#"+ids+"_out").text(amount);

            var totals=0;
            $(".ex_data").each(function(){
                var val=parseInt($(this).val());
                totals+=val;
            });

            prefix = d3.formatPrefix(Math.round(totals));

            $("#totalAmount").text("$"+prefix.scale(Math.round(totals)).toFixed(2)+" k")


        });






    }

    function submitForm(){

        var conf=confirm("Are you sure you want to save these changes?");
        return false;

    }




</script>
    <div class="contentWrapper container">
        <div class="col-md-6">
            <g:link controller="erUtilization" action="index" class="text-link-upper mb15">
                <i class="fa fa-angle-double-left iconRmargin"></i>
                <span class="isEditable"><g:editMessage code="costSharing.top.navigation"/> </span>
            </g:link>
        </div>
        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
        <sec:ifAllGranted roles="ROLE_Z5ADMIN">
        <g:if test="${!isEditable}">

            <g:link controller="cms" action="costSharing" title="Edit Content" ><i class="fa fa-edit"></i> Edit Content</g:link>
        </g:if><g:else>

            <g:link controller="erUtilization" action="costSharing" title="View Changes"><i class="fa fa-eye"></i> View Changes</g:link>

        </g:else></sec:ifAllGranted>
                </li>
            </div>



        <div class="clearfix"></div>
        <div class="sep-border mb25 pt15"></div>
            <div class="col-md-6 mb15">
                <h3 class="mb15"><span class="isEditable"><g:editMessage code="costSharing.container1.title"/> </span></h3>
                <span class="isEditable"><g:editMessage code="costSharing.container1.subTitle"/>

            </div>


            <div class="clearfix"></div>
            <div class="sep-border mb15"></div>
            <div class="col-md-12 mb15">
                <h3 class="mb15 textGray">
                    <span class="isEditable "><g:editMessage code="costSharing.container2.title"/></span></h3>

                <div>
                    <span class="isEditable "><g:editMessage code="costSharing.container2.content"/></span>
                </div>
                <a href="${pdfMessageCsv(code:'costSharing.container2.footer' )}">
                    <span class="isEditable"><g:editMessage code="costSharing.container2.footer"/>
                </a>


                </span>
            </div>


            <div class="clearfix"></div>

        <div class="sep-border mb15"></div>
        <div class="col-md-12">
            <h3 class="mb15 textGray"><span class="isEditable"><g:editMessage code="costSharing.container4.title"/></span></h3>
            <span class="isEditable"><g:editMessage code="costSharing.container4.content"/> </span>
        </div>
        <div class="clearfix"></div>

            <div class="sep-border mb15"></div>
%{--<g:if test="${!isEditable}">
        <g:form controller="erUtilization" absolute="true" action="saveGoals" onsubmit="return submitForm()">
            <div class="col-md-12 mb25">
                <h3 class="mb15"><span class="isEditable textGray">
                    <g:editMessage code="costSharing.container3.title"/>
                </span>
                    <g:submitButton name="saveG" value="Save Goals" class="btn btn-link-upper btn-primary pull-right" >

                        <span class="isEditable">
                            <g:editMessage code="costSharing.container3.button"/>
                        </span>
                    </g:submitButton>
                </h3>
                <span class="isEditable">
                    <g:editMessage code="costSharing.container3.subTitle"/>
                </span>
            </div>

            <div class="clearfix"></div>
                    <div class="boldHead">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <span class="isEditable">
                                <g:editMessage code="costSharing.container3.tableHead1"/>
                            </span>
                          </div>
                        <div class="col-md-4">
                            <span class="isEditable">
                                <g:editMessage code="costSharing.container3.tableHead2"/>
                            </span>
                            </div>
                        <div class="clearfix"></div>
                    </div>



           --}%%{-- <g:each in="${zSolutionList}" var="solution" status="i">

                    <div class="boldBody">
                        <div class="col-md-4">${solution.title}<br>
                        <span class="boldSpan1">Total potential Savings</span>
                        <span  class="boldSpan1">
                            <g:formatZ5Currency number="${solution.data}" type="number" maxFractionDigits="2" />
                            <g:hiddenField name="ex${i}_inp"  value="${solution.data}"/>
                        </span>

                        </div>
                            <div class="col-md-4">
                                <input id="ex${i}" name="percentage.${solution.code}" data-slider-id='ex${i}Slider'
                                       type="text" --}%%{----}%%{--data-slider-min="0" value="60" data-slider-max="100"
                                       data-slider-step="1"--}%%{----}%%{--/>
                                <script>bindSlider('ex${i}',${solution.percent})</script>
                            </div>
                        <div class="col-md-4 boldHead"><span id="ex${i}_out" >
                            <g:formatZ5Currency number="${solution.data*(solution.percent/100)}" type="number" maxFractionDigits="2" />
                        </span>
                            <g:hiddenField name="ex${i}_raw" class="ex_data" value="${solution.data*(solution.percent/100)}"/>
                        </div>
            <div class="clearfix"></div>
                    </div>
            </g:each>--}%%{--
            </g:form>
                    --}%%{--<div class="boldBody">

                        <div class="col-md-4">Condition Management<br><span  class="boldSpan1">Total potential Savings</span>
                            <span  class="boldSpan1">
                                <g:formatZ5Currency number="${topVal}" type="number" maxFractionDigits="2" />
                                <g:hiddenField name="ex2_inp" value="${topVal}"/>
                            </span>
                        </div>
                        <div class="col-md-4">
                            <input id="ex2"  data-slider-id='ex2Slider'
                                   type="text" data-slider-min="0" data-slider-max="100"
                                   data-slider-step="1"/>
                            <script>bindSlider('ex2')</script>
                        </div>
                        <div class="col-md-4 boldHead"><span id="ex2_out">
                            $<g:formatNumber number="${topVal/2}" type="number" maxFractionDigits="0" />
                        </span>
                            <g:hiddenField name="ex2_raw" value=""/>
                        </div>
                        <div class="clearfix"></div>
                    </div>--}%%{--

            <div class="boldBody">
                <div class="col-md-6"></div>
                <div class="col-md-6 "> <span class="isEditable">
                    <g:editMessage code="costSharing.container3.tableSummary"/>
                </span> <span id="totalAmount">


                </span></div>
                <div class="clearfix"></div>


            </div>
    </g:if>--}%
            <div class="text-itallic col-md-12 mb15">
                <span class="isEditable">
                    <g:editMessage code="costSharing.container3.footer"/>
                </span>

            </div>





            <div class="clearfix"></div>



            <div class="sep-border mb15"></div>
            <div class="col-md-12">
                <h3 class="mb15 textGray"><span class="isEditable"><g:editMessage code="costSharing.container5.title"/></span></h3>
                <h4 class="mb15 text-itallic"><span class="isEditable">
                    <g:editMessage code="costSharing.container5.subTitle"/>
                 </span></h4>
               <span class="isEditable">
                   <g:editMessage code="costSharing.container5.content"/>
               </span>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12 boldBody text-center mt35">
                <span class="isEditable">
                    <g:editMessage code="costSharing.container5.footer"/>
                </span>

            </div>

            <div class="col-md-12 text-center"> <a href="#" class="btn-link-upper btn-blue inline-block mb15"><span class="isEditable">
               <span class="isEditable"><g:editMessage code="costSharing.container5.button"/></span>
                <input type="hidden" value="strategicpage.contact"> </span></a>
            </div>

        <h3 class="mb15 textGray" style="text-align: center">
            <span class="isEditable">
                <g:editMessage code="costSharing.container5.button.message"/>
                </span></h3>



    </div>

    <div id="confirmGoals" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="sessionText">
                    <g:if test="${flash.success}">
                        <script>
                            $("#confirmGoals").modal("show");
                        </script>
                            <div class="alert alert-success">
                                ${flash.success}
                            </div>
                    </g:if>
                    <g:elseif test="${flash.error}">
                        <script>
                            $("#confirmGoals").modal("show");
                        </script>
                            <div class="alert alert-danger">
                                ${flash.error}
                            </div>
                    </g:elseif>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" >OK</button>
                </div>
            </div>
        </div>
    </div>

<script>
    var total=0;
    $(".ex_data").each(function(){
       var val=parseInt($(this).val());
        total+=val;
    });

    var prefixx = d3.formatPrefix(Math.round(total));
    var amountt="$"+prefixx.scale(Math.round(total)).toFixed(2)+" k";
    $("#totalAmount").text(amountt);


</script>

<style>
.slider-selection {
    background: #0480be;
}
.slider-track-high{
    background: #BABABA;
}
</style>
</body>
</html>