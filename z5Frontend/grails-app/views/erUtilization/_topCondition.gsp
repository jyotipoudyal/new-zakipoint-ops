%{--<div class="col-md-6 z5-table top-condition">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th colspan="2" class="text-left"><strong>Top Condition By Cost across all ER visits</strong></th>

        </tr>
        </thead>
        <g:each in="${topCondition.graph1}" var="data">
            <tr>
                <td style="text-transform: capitalize">${data.condition}</td>
                <td><div style="float: left;width: 80px;color: steelblue;font-weight: bold;font-size: 14px;">
                    <g:formatZ5Currency number="${data.totalAmount}" type="number" maxFractionDigits="2" />
                </div>
                    <%
                        def max, min, val;
                        max = topCondition.amount1;
                        min = data.totalAmount;
                        try{
                            val = (min / max) * 100;
                        }catch(ArithmeticException e){
                            val = 0
                        }
                    %>
                    <div style="height:10px;width:100px;background-color: #fff;float: left;margin-top: 5px;">
                        <div style="height:10px;width:${val}px;background-color: steelblue"></div>
                    </div>
                </td>
            </tr>
        </g:each>
    </table>
</div>--}%
%{--<div class="col-md-6 z5-table top-condition border-left">--}%
<div class="graph-table top-condition">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <g:each in="${topCondition.graph2}" var="data">
            <tr>
                <td style="text-transform: capitalize">${data.condition}</td>
                <td><div style="float: left;width: 80px;color: firebrick;font-weight: bold;font-size: 14px;">
                    <g:formatZ5Currency number="${data.totalAmount}" type="number" maxFractionDigits="2" />
                </div>
                    <%
                        max = topCondition.amount2;
                        min = data.totalAmount
                        try {
                            val = (min / max) * 100;
                        } catch (ArithmeticException e) {
                            val = 0
                        }
                    %>
                    <div style="height:10px;width:100px;background-color: #fff;float: left;">
                        <div style="height:10px;width:${val}px;background-color: firebrick;margin-top: 5px;"></div>
                    </div>
                </td>
            </tr>
        </g:each>
    </table>
</div>