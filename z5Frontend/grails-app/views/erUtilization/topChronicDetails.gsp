<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Top Chronic Details</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
%{--<link href="${resource(dir:"css",file: "layout.css")}" rel="stylesheet"/>--}%
</head>

<body>


<div  id="contentWrapper" class="contentWrapper">

    <div class="container">
        <div class="col-md-6">
            <g:link controller="erUtilization" action="index" class="text-link-upper mb15">
                <i class="fa fa-angle-double-left iconRmargin"></i>
                <span class="isEditable"><g:editMessage code="erUtilization.costDrivers.top.navigation"/> </span>
            </g:link>
        </div>
        <div class="col-md-6" style="display: none;" id="editContent">
            <li>
            <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                <g:if test="${!isEditable}">
                    <g:link title="Edit Content"  controller="cms" action="topChronicDetails"><i class="fa fa-edit"></i> Edit Content</g:link>
                </g:if><g:else>
                <g:link title="View Changes"  controller="erUtilization" action="topChronicDetails"><i class="fa fa-eye"></i> View Changes</g:link>
            %{--<a title="View Changes" class="switch-edit-mode"><i class="fa fa-eye"></i></a>--}%
            </g:else>
            </sec:ifAllGranted>
</li>
        </div>

        <div class="clearfix"></div>

        <div class="sep-border mb25 pt15"></div>

        <%
            //        split page
        %>

        <div class="col-md-6">
            <h1 class="isEditable"><g:editMessage code="erUtilization.container3.table1.title1"/></h1>
        </div>
        <div class="col-md-6 text-right">
            <a href="#" class="custom-tooltip text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.explore.population"/></span><input type="hidden" value="erUtilization.container3.table1.options1"></a>
            <g:link action="exportCsv" params="[exportModule:'topChronicConditionEr']"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span><input type="hidden" value="erUtilization.container3.table1.options2"></g:link>

            %{--<a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span><input type="hidden" value="erUtilization.container3.table1.options2"></a>--}%
        </div>


        <div class="col-md-12">

                <g:if test="${!isEditable}">
                    <div id="topChronicConditionEr" class="topChronicConditionEr mrr0">
                        <g:render template="tableDataDetails" model="[data:topChronicConditionEr,show:true]"/>
                    </div>
                </g:if><g:else>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
                    <thead>
                    <tr>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head1"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head2"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head3"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head4"/></span></th>
                        <th><span class="isEditable"><g:editMessage code="erUtilization.container4.table1.head5"/></span></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </g:else>
                <div class="clearfix"></div>

        </div>




    </div>

</div>
<script>
    var $table =$(".z5-fixed-header");
    $.each($table, function() {
        var $this = $(this);

        var currentSort = $this.find("thead>tr th a.sorted").hasClass("desc");
        if (currentSort) {
            currentSort = "desc";
        } else {
            currentSort = "asc";
        }

        $.each($(this).find("thead>tr th a:not(.sorted)"), function () {
				$(this).addClass(currentSort+"-disabled");
//            $(this).addClass(currentSort);

        })
    });
</script>
</body>





</html>