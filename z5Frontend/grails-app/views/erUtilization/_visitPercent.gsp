<h4 xmlns="http://www.w3.org/1999/html">
    <strong>${visitPer.percentOfhadNotVisitedDoctor?(Math.round(visitPer?.percentOfhadNotVisitedDoctor * 100) / 100):0}%</strong> of members over utilizing the ER <strong>in the ${visitPer.time} 12 months</strong> have not had a routine or preventative doctor visit
</h4>
<div class="mb-35 mt35 er-data">
    <div class="er-count">${visitPer?.totalUniquerErVisitor}</div>
    <div class="er-count-label">Total Unique Visitors To ER</div>
</div>
<div class="clearfix"></div>
<div class="er-visit-percent-wrapper">
    <div class="col-md-6"><div class="er-visit-percent">${visitPer.percentOfvisitedMoreThan2Times?(Math.round(visitPer?.percentOfvisitedMoreThan2Times * 100) / 100):0}%</div><div class="er-visit-percent-label">Visited more than 2 times a year</div></div>
    <div class="col-md-6"><div class="er-visit-percent">${visitPer.percentOfvisitedMoreThan3Times?(Math.round(visitPer.percentOfvisitedMoreThan3Times * 100) / 100):0}%</div><div class="er-visit-percent-label">Visited more than 3 times a year</div></div>
    %{--<div class="col-md-4"><div class="er-visit-percent">${visitPer.percentOfhadNotVisitedDoctor?(Math.round(visitPer?.percentOfhadNotVisitedDoctor * 100) / 100):0}%</div><div class="er-visit-percent-label">Had not gone to doctor in past year</div></div>--}%
    %{--${visitPer.percent}--}%
</div>