<div class="col-md-12 z5-table sp-drug-table" style="height:auto !important;">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header" style="float: left">
        <thead>
        <tr>
            <th width="50%">Condition</th>
            <th > <span style="margin-left:13%;">Avoidable ER visits Linked to Gaps In Care</span></th>
            %{--<th>Potential Savings If Gaps In Care Avoided</th>--}%
            %{--<th>Total ER Cost For Condition</th>--}%
            %{--<th>Total ER Visit For Condition</th>--}%
        </tr>
        <tr class="bold-large-th">
            <th >Total</th>
            <th > <span style="margin-left:13%;">${gapTotal?.totalMembersLinkedToGapinCareWithEr}</span></th>
            %{--<th>$6600</th>--}%
        </tr>

        </thead>
        <tbody>
        <%
                        dataGraphGap.each{da->
        %> <tr>
            <td>
                <div style="width: 400px;" >
                    <div style="width: 380px;float: left" ><strong>${da.key}</strong></div>
                    <i  style="cursor: pointer;float: left" class="fa fa-chevron-down" id="condition_${da.key.toString().split(" ").join("")}" onclick="hideRows('${da.key.toString().split(" ").join("")}')"></i>
                </div>
            </td>
            <td> <span style="margin-left:13%;"><strong>${da.value.erVisitCount} total avoidable ER visits</strong></span>%{--${it.value.uniqueMemberCount}--}%</td>
        </tr>
        <tr class="hiddenTable" id="table_${da.key.toString().split(" ").join("")}">
            <td colspan="2">
                <table width="100%" class="gapInCareTable">
        <%
                def vals=da.value;
                vals.remove('uniqueMemberCount');
                vals.remove('erVisitCount');
                vals.sort{it.key}.each {
        %>

        %{--<tr class="hiddenTable table_${da.key}">--}%
        <tr>
                        <td width="10%">
                            <g:include controller="erUtilization" action="mapGapInCare" params="[metric:it.value.metricIdentifier]"/>
                            %{--<g:include controller="erUtilization" action="mapGapInCare" params="[metric:it.value.metricIdentifier]"/>--}%
                            %{--${it.value.metricIdentifier.toString().toLowerCase()}--}%
                        </td>
                        <td width="46%">${it.value.metricDescription}</td>
                        <td style="text-align: left;"><span style="margin-left: 4px;">${Math.round(it.value.percentOfcareAlertPopulation)}%</span></td>
        </tr>

        <%
                }
                    %>
                </table>

            </td>

        </tr>
        <%
            }
        %>

        %{--
        <tr  class="conditionB">
            <td></td>
            <td>Gap5</td>
            <td>$783</td>
            <td><div style="float: left;width: 50px;">$500</div>
                <%
                    max = 10000;
                    min = 500;
                    try {
                        val = (min / max) * 100;
                    } catch (ArithmeticException e) {
                        val = 0
                    }
                %>
                <div style="height:10px;width:100px;background-color: #fff;float: left;margin-top: 5px;">
                    <div style="height:10px;width:${val}px;background-color: steelblue;"></div>
                </div></td>--}%%{--

        </tr>--}%



        </tbody>
    </table>
</div>

<script>
    function hideRows(claz){

        if($("#condition_"+claz).hasClass("fa-chevron-down")){
            $("#table_"+claz).show();
            localStorage.setItem("table_"+claz,1);
            $("#condition_"+claz).removeClass("fa-chevron-down").addClass("fa-chevron-up")
        }else{
            $("#table_"+claz).hide();
            localStorage.setItem("table_"+claz,0);
            $("#condition_"+claz).removeClass("fa-chevron-up").addClass("fa-chevron-down")

        }

    }

    $(function(){
        $(".hiddenTable").each(function(){
            var ids=this.id.split("_")[1]
            var tables=localStorage.getItem('table_'+ids)
            if(tables==1){
                $("#table_"+ids).show();
                $("#condition_"+ids).removeClass("fa-chevron-down").addClass("fa-chevron-up")
            }else{
                $("#table_"+ids).hide();
                $("#condition_"+ids).removeClass("fa-chevron-up").addClass("fa-chevron-down")
            }
//            localStorage.setItem("table_"+ids,false);
        })
    })






//    $(".hiddenTable").hide();
</script>