<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 11/4/2015
  Time: 3:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Z Solution</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>

</head>


<body>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'bootstrap-slider.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-silder.css')}" type="text/css">
<script type="text/javascript">
    /*var slider = new Slider('#ex1', {
     formatter: function(value) {
     return  value +"%"
     }
     ,tooltip:"always"


     });*/

    /*var slider = new Slider("#ex1", {
     formatter: function(value) {
     return  value +"%"
     }
     ,tooltip:"always"
     ,value: 50
     ,tooltip_position:'bottom'
     });*/


    /*var slider = new Slider(".erSlider", {
     ticks: [0, 50, 100],
     ticks_labels: ['0%', '50%', '100%'],
     ticks_snap_bounds: 1,
     value: 0,
     //        tooltip_position:'bottom',
     tooltip:"always"
     });

     $(".slider-tick").remove();*/
    var slider;

    function bindSlider(id){
        var originalVal;
        slider=new Slider("#"+id, {
            formatter: function(value) {
                return  value +"%"
            }
            ,tooltip:"always"
            ,value: 50
            ,tooltip_position:'bottom'
        });

    }




</script>

<div class="mainWrapper">
    <div class="contentWrapper">
        <div class="container">
            <div class="col-md-6">
                <g:link controller="dashboard" action="index" class="text-link-upper mb15">
                    <i class="fa fa-angle-double-left iconRmargin"></i>
                    <span class="isEditable">Population Risk-Diabetes</span>
                </g:link>
            </div>
        <sec:ifAllGranted roles="ROLE_Z5ADMIN">
        <div class="col-md-6"> <g:if test="${!isEditable}">

                <g:link controller="cms" action="strategicPage" title="Edit Content" class="switch-edit-mode"><i class="fa fa-edit"></i></g:link>
        </g:if><g:else>

                <g:link controller="highPharmacyCost" action="strategicPage" title="View Changes" class="switch-edit-mode"><i class="fa fa-eye"></i></g:link>

        </g:else></div>
        </sec:ifAllGranted><br>


            <div class="clearfix"></div>
            <div class="col-md-12">
                <h2 class="mb15"><span class="isEditable">Telemedicine Evaluation </span></h2>
                </div>
            <div class="col-md-12">
                <h3><span class="isEditable">Potential Savings through use of Telemedicine</span><a href="#" class="btn-link-upper btn-blue mb15 inline-block ml25"><span class="isEditable">Find Out How </span></a></h3>
                </div>



            <div class="clearfix"></div>

        <div class="solutionWrapper">
            <div class="boldHead">


            <div class="col-md-4">Cost Driver </div>
            <div class="col-md-4">Expected % Savings</div>
            <div class="col-md-4">Projected Savings</div>
                <div class="clearfix"></div>
            </div>


            <div class="boldBody">
            <div class="col-md-4">Inefficient ER Use</div>
            <div class="col-md-4">
                <input id="ex1" class="erSlider"  data-slider-id='ex1Slider'
                       type="text" data-slider-min="0" data-slider-max="100"
                       data-slider-step="1"/>
                <script>bindSlider('ex1')</script>
            </div>
            <div class="col-md-4">$xxx</div>
                <div class="clearfix"></div>
            </div>
            <div class="boldBody">


            <div class="col-md-4">Diabetes Management</div>
            <div class="col-md-4">
                    <input id="ex2" class="erSlider"  data-slider-id='ex1Slider'
                           type="text" data-slider-min="0" data-slider-max="100"
                           data-slider-step="1"/>
                <script>bindSlider('ex2')</script>
                %{--<input id="ex13" class="erSlider" type="text" data-slider-ticks="[0, 50, 100]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels='["0%", "50%", "100%"]' />--}%
            </div>
            <div class="col-md-4">$xxx</div><div class="clearfix"></div></div>
            <div class="boldBody">


            <div class="col-md-4">Asthama Management</div>
            <div class="col-md-4">
                <input id="ex3" class="erSlider"  data-slider-id='ex1Slider'
                       type="text" data-slider-min="0" data-slider-max="100"
                       data-slider-step="1"/>
                <script>bindSlider('ex3')</script>

            </div>
            <div class="col-md-4">$xxx</div><div class="clearfix"></div></div>
            <div class="boldBody">


            <div class="col-md-4">PrimaryCare Gap</div>
            <div class="col-md-4">
                <input id="ex4" class="erSlider"  data-slider-id='ex1Slider'
                       type="text" data-slider-min="0" data-slider-max="100"
                       data-slider-step="1"/>
                <script>bindSlider('ex4')</script>

            </div>
            <div class="col-md-4">$xxx</div>
                <div class="clearfix"></div>
                </div>



        <div class="boldBody">
            <div class="col-md-8 text-right">Total Projected Savings</div>
            <div class="col-md-4 ">$XXX</div>

            <div class="clearfix"></div>
            </div>
        <div class="boldBody">
            <div class="col-md-4 pull-right"><a href="#" class="btn-link-upper btn-blue btn-inline"><span class="isEditable">Find Out How </span></a></div>

            <div class="clearfix"></div>

        </div>

    </div>
    </div>
    </div>




</div>



<script type="text/javascript">
    /*var slider = new Slider('#ex1', {
        formatter: function(value) {
            return  value +"%"
        }
        ,tooltip:"always"


    });*/

    /*var slider = new Slider("#ex1", {
        formatter: function(value) {
            return  value +"%"
        }
        ,tooltip:"always"
        ,value: 50
        ,tooltip_position:'bottom'
    });*/


    /*var slider = new Slider(".erSlider", {
        ticks: [0, 50, 100],
        ticks_labels: ['0%', '50%', '100%'],
        ticks_snap_bounds: 1,
        value: 0,
//        tooltip_position:'bottom',
        tooltip:"always"
    });

    $(".slider-tick").remove();*/

    /*function bindSlider(id){

        new Slider("#"+id, {
            formatter: function(value) {
                return  value +"%"
            }
            ,tooltip:"always"
            ,value: 50
            ,tooltip_position:'bottom'
        });

    }*/


</script>

<style>
#ex1Slider .slider-selection {
    background: #0480be;
}
#ex1Slider .slider-track-high{
    background: #BABABA;
}
</style>

</body>
</html>