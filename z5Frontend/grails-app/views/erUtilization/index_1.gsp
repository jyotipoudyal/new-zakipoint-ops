<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 12/17/2015
  Time: 11:45 AM
--%>
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>ER Utilization</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
%{--<link href="${resource(dir:"css",file: "layout.css")}" rel="stylesheet"/>--}%
</head>

<body>
<div class="mainWrapper">
    <div style="position: absolute">
        <svg>
            %{--<pattern id="pattern-stripe"
                     width="4" height="4"
                     patternUnits="userSpaceOnUse"
                     patternTransform="rotate(45)">
                <rect width="2" height="4" class="rect" transform="translate(0,0)" fill="white"></rect>
                <g stroke="rgba(225,225,225,.4)"
                   stroke-width="20">
                    <path d="M25,0 25,100"
                    ></path>
                </g>
            </pattern>--}%

            <pattern
                    id="pattern-stripe"
                    width="5"
                    height="50"
                    patternUnits="userSpaceOnUse"
                    patternTransform="rotate(-45)">
                <rect
                        width="4"
                        height="100%"
                        fill="#034f96"></rect>

                <g stroke="#b53322"
                   stroke-width="45">
                    <path d="M25,0 25,100"
                    ></path>
                </g>

            </pattern>


            %{--<defs>
                <pattern id="img1" patternUnits="userSpaceOnUse" width="100" height="100" >
                    <image xlink:href="../images/st.png" width="100" height="100" />
                </pattern>
            </defs>--}%
        </svg>
    </div>
    <div  id="contentWrapper" class="contentWrapper">
        <div class="container">
            <div class="col-md-8">
                <g:link controller="dashboard" action="index" class="text-link-upper mb15">
                    <i class="fa fa-angle-double-left iconRmargin"></i>
                    <span class="isEditable"><g:editMessage code="erUtilization.top.navigation"/> </span>
                </g:link>
            </div>
            <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                    <div class="col-md-4"> <g:if test="${!isEditable}">
                            <g:link controller="cms" action="erUtilization" title="Edit Content" class="switch-edit-mode"><i class="fa fa-edit"></i></g:link>
                    </g:if><g:else>
                            <g:link controller="erUtilization" action="index" title="View Changes" class="switch-edit-mode"><i class="fa fa-eye"></i></g:link>
                    </g:else></div>
        </sec:ifAllGranted>
            <div class="clearfix"></div>
            <div class="col-md-10">
                <h2 class="mb15">
                    <g:img uri="/images/icon-high-pharmacy-cost.png" class="heading-icon" />
                    <span class="isEditable"><g:editMessage code="erUtilization.container1.graph1.title"/> </span></h2>
                <h3 class="isEditable"><g:editMessage code="erUtilization.container1.graph1.subTitle"/></h3>
            </div>
            <div class="col-md-2">
                <g:form action="exportPdf" name="exportForm">
                    <g:hiddenField name="imagePath1"/>
                    <g:hiddenField name="graphTitle1"/>
                    <g:hiddenField name="act"/>
                    <g:hiddenField name="exp"/>
                    <g:hiddenField name="g1Filter"/>
                    <g:hiddenField name="g2Filter"/>
                    <g:hiddenField name="g3Filter"/>
                </g:form>
                <a href="#" class="btn-link-upper btn-blue mb15 isEditable"><g:editMessage code="erUtilization.container1.graph1.option1"/></a>
                <a href="#" class="btn-link-upper btn-light-blue mb15 isEditable"><g:editMessage code="erUtilization.container1.graph1.option2"/></a>

                %{--<a href="#" class="btn-link-upper btn-gray mb15 isEditable"><g:editMessage code="erUtilization.container1.graph1.option3"/></a> --}%
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <div class="hr mb15"></div>
            </div>
            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="erUtilization.container1.graph1.title2"/></h1>
                %{--<h6 class="isEditable"><g:editMessage code="erUtilization.container1.graph1.title3"/></h6>--}%
            </div>
            <div class="col-md-6 text-right">
                <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.explore.population"/></span><input type="hidden" value="erUtilization.container3.table1.options1"></a>
                <a href="#"  class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span><input type="hidden" value="erUtilization.container3.table1.options2"></a>
            </div>
            <div class="col-md-12"><h3 id="graph1Title"></h3>
            </div>
            <div class="col-md-12">
                <h6 class="has-filter">
                    <span id="graph1Filter">
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasisg1" from="${['PaidDate':'paid','ServiceDate':'incurred']}" optionKey="key" optionValue="value" onchange="refreshReport('graph1')" /></span>
                        %{--cost to <span style="color: steelblue;"><g:select name="regionalg1" from="${['1':'NorthEast','2':'NorthCentral','3':'South']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')" noSelection="['':'regional']" /></span>--}%
                        visit to national benchmark</span>
                    <span class="f-right">
                        <span class=" isEditable" ><g:editMessage code="erUtilization.container1.methodology" /></span>
                        <span class="fa fa-chevron-right " onclick="displayData('methodology1')"></span>
                    </span>
                </h6>
                </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <strong style="font-size: 16px;"><span class=" isEditable" ><g:editMessage code="erUtilization.container1.table1.head"/></span></strong>
                <div id="bargraph1" ></div>
                </div>

                <div  class="col-md-6">
                    <strong style="font-size: 16px;"><span class=" isEditable" ><g:editMessage code="erUtilization.container1.table2.head"/></span></strong>
                    <div id="bargraph2">
                    </div>
                </div>

                %{--<g:if test="${!isEditable}">  <div id="graph1" class="graph1"></div></g:if>

                <g:if test="${!isEditable}"> <div class="graph1-label" style="display: none;">
                    <div class="legend legend-red"></div>
                    <div id="actualValue"></div>
                    <div class="legend legend-gray"></div>
                    <div id="expectedValue"></div>
                </div></g:if>--}%
            </div>




        </div>
        <div class="container mt25">
            <div class="sep-border"></div>
            <div class="col-md-6">
                <h3 class="isEditable"><g:editMessage code="erUtilization.container2.title1" /></h3>
                <p class="isEditable"><g:editMessage code="erUtilization.container2.subTitle1" /></p>
                <h3 class="isEditable"><g:editMessage code="erUtilization.container2.title2" /></h3>

                <h4 class="color-blue m150"> <g:img uri="/images/icon-high-cost-claimants.png" class="heading-icon" /> <span class="isEditable">Highcost <strong>Claimants</strong> </span></h4>
                <h4 class="color-blue m150"> <g:img uri="/images/ineffieicnt-network-use.png" class="heading-icon" /> <span class="isEditable"><g:editMessage code="erUtilization.container2.title2.subHeads1"/> </span></h4>
            </div>
            <div class="col-md-6">
                <h3 class="isEditable"><g:editMessage code="erUtilization.container2.title3" /></h3>
                <span class="bulb-lists color-blue isEditable">
                    <g:editMessage code="erUtilization.container2.title3.subHeads1" />
                    %{--<g:pdfMessageRemoveA code="erUtilization.container2.title3.subHeads1" />--}%
                </span>
                %{--<ul class="bulb-lists color-blue isEditable">
                    <li><i class="fa fa-lightbulb-o"></i>Update financial incentives to drive generic and/or mail order use</li>
                    <li><i class="fa fa-lightbulb-o"></i>Launch a targeted campaign around brand drugs where a generic drug is available</li>
                    <li><i class="fa fa-lightbulb-o"></i>Evaluate pharmacy benefit contract for renegotiation or carve-out potential</li>
                    <li><i class="fa fa-lightbulb-o"></i>Implement a speciality pharmacy cost management plan</li>
                </ul>--}%
            </div>

        </div>
        <div class="container">
            <div class="sep-border"></div>
            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="erUtilization.container1.5.title1" /></h1>
                %{--<h6 class="isEditable"><g:editMessage code="erUtilization.container1.5.title2" /></h6>--}%

            </div>

            <div class="col-md-12">
                <h6 class="has-filter">
                    <span>
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasist2" from="${['PaidDate':'paid','ServiceDate':'incurred']}" optionKey="key" optionValue="value" onchange="refreshReport('table2')" /></span>
                        %{--cost to <span style="color: steelblue;"><g:select name="regionalg1" from="${['1':'NorthEast','2':'NorthCentral','3':'South']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')" noSelection="['':'regional']" /></span>--}%
                        cost to national benchmark <span style="color: steelblue;display: none">
                        <g:select name="regionalt2" class="multiSelect" from="${['1':'NorthEast','2':'NorthCentral','3':'South','4':'West']}" optionKey="key" optionValue="value" noSelection="['':'national']"  /></span>
                    over <span style="color: steelblue;"> <g:select name="fromDatet2" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value" onchange="refreshReport('table2')" /></span>
                </span>

                    <span class="f-right">
                        <span class=" isEditable" ><g:editMessage code="erUtilization.container1.methodology" /></span>
                        <span class="fa fa-chevron-right " onclick="displayData('methodology1')"></span>
                    </span>
                </h6>
            </div>
            <div class="clearfix"></div>
    <g:if test="${!isEditable}">
            <div class="col-md-12" id="totalVisits">
                <g:render template="visitPercent"/>
            </div>
    </g:if>
        </div>
        <div class="container mt35">
            <div class="sep-border"></div>
            <div class="col-md-6">
                <h1 class="isEditable"><g:editMessage code="erUtilization.container1.table1.mTitle1"/></h1>
            </div>

            <div class="col-md-6 text-right">
                <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><span class="isEditable"><g:editMessage code="erUtilization.explore.population"/></span><input type="hidden" value="erUtilization.container3.table1.options1"></span> </a>
                <g:link action="exportCsv" params="[module:'table1Data']" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span><input type="hidden" value="erUtilization.container3.table1.options2"></g:link>
            </div>
            <div class="col-md-12">
                <h6 class="has-filter">
                    <span>
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasist3" from="${['PaidDate':'paid','ServiceDate':'incurred']}" optionKey="key" optionValue="value" onchange="refreshReport('table3')" /></span>
                        %{--cost to <span style="color: steelblue;"><g:select name="regionalg1" from="${['1':'NorthEast','2':'NorthCentral','3':'South']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')" noSelection="['':'regional']" /></span>--}%
                        cost to national benchmark <span style="color: steelblue;display: none">
                        <g:select name="regionalg1" class="multiSelect" from="${['1':'NorthEast','2':'NorthCentral','3':'South','4':'West']}" optionKey="key" optionValue="value" noSelection="['':'national']"  /></span>
                    over <span style="color: steelblue;"> <g:select name="fromDatet3" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value" onchange="refreshReport('table3')" /></span>
                </span>
                    <span class="f-right">
                        <span class=" isEditable" ><g:editMessage code="erUtilization.container1.methodology" /></span>
                        <span class="fa fa-chevron-right " onclick="displayData('methodology1')"></span>
                    </span>
                </h6>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <h2 class="mb25 isEditable" id="graph2Title">
                </h2>
                <g:if test="${!isEditable}">
                    <div id="table3Graph">
                        <g:render template="topCondition"/>
                    </div>
                </g:if>

                <h1 class="text-center isEditable"><g:editMessage code="erUtilization.container3.table1.subTitle"/></h1>

            </div>
        </div>
        <div>&nbsp;</div>
        <div class="container">
            <div class="sep-border"></div>
            <div class="col-md-6 mb25">
                <h1 class="isEditable"><g:editMessage code="erUtilization.container1.table2.mTitle1"/></h1>
                %{--<h6 class="isEditable"><g:editMessage code="erUtilization.container1.table2.mTitle2"/></h6>--}%
            </div>
            <div class="col-md-6 text-right">
                <a href="#" class="text-link-upper iconExploreEntirePopulation"><g:img uri="/images/icon-explore-population.png" alt=""/><span class="isEditable"><span class=" isEditable" ><g:editMessage code="erUtilization.explore.population" /></span><input type="hidden" value="erUtilization.container3.table1.options1"></span> </a>
                <g:link action="exportCsv" params="[module:'table2Data']" class="text-link-upper iconExportCSV"><g:img uri="/images/icon-dwonload-data.png" alt=""/><span class="isEditable"><g:editMessage code="erUtilization.export.csv"/></span><input type="hidden" value="erUtilization.container3.table1.options2"></g:link>
            </div>

            <div class="col-md-12 mb25">
                <h6 class="has-filter">
                    <span>
                        Compare <span style="color: steelblue;"><g:select style="width:80px;" name="reportingBasist4" from="${['PaidDate':'paid','ServiceDate':'incurred']}" optionKey="key" optionValue="value" /></span>
                        %{--cost to <span style="color: steelblue;"><g:select name="regionalg1" from="${['1':'NorthEast','2':'NorthCentral','3':'South']}" optionKey="key" optionValue="value"  onchange="refreshReport('graph1')" noSelection="['':'regional']" /></span>--}%
                        cost to national benchmark <span style="color: steelblue;display: none">
                        <g:select name="regionalg1" class="multiSelect" from="${['1':'NorthEast','2':'NorthCentral','3':'South','4':'West']}" optionKey="key" optionValue="value" noSelection="['':'national']"  /></span>
                    over <span style="color: steelblue;"> <g:select name="fromDatet4" from="${['current':'most recent 12 months','past':'past 12 months','prior':'prior 12 months']}" optionKey="key" optionValue="value" onchange="refreshReport('table4')" /></span>
                </span>

                    <span class="f-right">
                        <span class=" isEditable" ><g:editMessage code="erUtilization.container1.methodology" /></span>
                        <span class="fa fa-chevron-right " onclick="displayData('methodology1')"></span>
                    </span>
                </h6>
            </div>
            <g:if test="${!isEditable}">
                <div id="table4Graph">
                        <g:render template="gapInCare"/>
                </div>
            </g:if>

            <div class="clearfix"></div>


        </div>



        <div id="methodology1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="isEditable"> <g:editMessage code="erUtilization.container1.methodology1"/></span>
                    </div>

                </div>
            </div>
        </div>
        <div id="methodology2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="erUtilization.container1.methodology2"/></span>
                    </div>

                </div>
            </div>
        </div>
        <div id="methodology3" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="isEditable"><g:editMessage code="erUtilization.container1.methodology3"/></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <style>


    .hbar {
        /*mask: url(#mask-stripe)*/
    /*fill: url(#mask-stripe);*/

        fill: url(#pattern-stripe) !important;
    }

    /*.z5-table table td:nth-child(3) {
        border-right:solid 1px #ccc;
    }
    .z5-table table th:nth-child(3) {
        border-right:solid 1px #ccc;
    }*/

    .z5-table table th{
        border-top:1px solid #ccc;
        padding:4px; text-align:center;
        text-transform:uppercase;
        font-weight:normal;
    }

    .axis path,
    .axis line {
        fill: none;
        stroke: #000;
        shape-rendering: crispEdges;
    }

    .bar {
        fill: steelblue;
    }

    .lineColor {
        fill: none;
        stroke: #1c94c4;
        stroke-width: 1px;
        stroke-dasharray:6 3
    }
    .dbar {fill: #034f96;}



    .x.axis path {
        display: none;
    }

    </style>
    <body>
    <script>

        $("span.bulb-lists").find("ul").addClass("bulb-lists color-blue");
        $("span.bulb-lists").find("li").prepend("<i class='fa fa-lightbulb-o'></i>");
    </script>
    <script>




        renderData(${dataPh});
        render2Data(${dataPh2},${dataBm2});

        /*var margin = {top: 20, right: 10, bottom: 20, left: 20},
         width = 500 - margin.left - margin.right,
         height = 320 - margin.top - margin.bottom;

         var x = d3.scale.ordinal()
         .rangeRoundBands([0, 300],.4);

         var y = d3.scale.linear()
         .rangeRound([height, 0]);

         var color = d3.scale.ordinal()
         .range([ "#034f96","#b53322"]);

         var xAxis = d3.svg.axis()
         .scale(x)
         .orient("bottom");

         var yAxis = d3.svg.axis()
         .scale(y)
         .orient("left")
         .tickFormat(d3.format(".2s"));

         var svg = d3.select("#graph1").append("svg")
         .attr("width", width + margin.left + margin.right)
         .attr("height", height + margin.top + margin.bottom)
         .append("g")
         .attr("transform", "translate(" + -25 + "," + margin.top + ")");*/


        /*d3.csv("%{--${resource(dir:"dData",file: "multi.csv")}--}%", function(error, data) {
         return false;

         color.domain(d3.keys(data[0]).filter(function(key) {
         return (key !== "year" && key !== "erPercent");
         }))
         //            return false;

         data.forEach(function(d) {
         var y0 = 0;
         d.ages = color.domain().map(function(name) {
         return {name: name, y0: y0, y1: y0 += +d[name]};
         });
         d.total = d.ages[d.ages.length - 1].y1;
         *//*if(typeof d.year == "string"){
         d.order=5000
         }else{
         d.order= d.year
         }*//*



         });

         data.sort(function(a, b) { return b.order - a.order; });

         x.domain(data.map(function(d) { return d.year; }));
         y.domain([0, d3.max(data, function(d) { return d.total*1.2; })]);

         svg.append("g")
         .attr("class", "x axis")
         .attr("transform", "translate(0," + height + ")")
         .call(xAxis);

         *//*svg.append("g")
         .attr("class", "y axis")
         .call(yAxis)
         .append("text")
         .attr("transform", "rotate(-90)")
         .attr("y", 6)
         .attr("dy", ".71em")
         .style("text-anchor", "end")
         .text("Population");*//*

         var state = svg.selectAll(".state")
         .data(data)
         .enter().append("g")
         .attr("class", "g")
         .attr("transform", function(d) { return "translate(" + x(d.year) + ",0)"; });

         state.selectAll("rect")
         .data(function(d) { return d.ages; })
         .enter().append("rect")
         .attr("width", x.rangeBand())
         .attr("y", function(d) { return y(d.y1); })
         .attr("height", function(d) { return y(d.y0) - y(d.y1); })
         //                    .style("fill", function(d) { return color(d.name); })
         .style("fill",function(d) {
         return color(d.name);

         })
         *//*.attr("class", function(d) {
         if(color(d.name)=="#034f96")
         return "dbar"
         else
         return "hbar thing-2"

         })*//*;

         var legend = svg.selectAll(".legend")
         .data(color.domain().slice())
         .enter().append("g")
         .attr("class", "legend")
         .attr("transform", function(d, i) { return "translate(0," + (i * 22-15) + ")"; });

         legend.append("rect")
         .attr("x", width - 18)
         .attr("width", 18)
         .attr("height", 18)
         .style("fill", color);

         legend.append("text")
         .attr("x", width -165)
         .attr("y", 9)
         .attr("dy", ".35em")
         .style("text-anchor", "start")
         .style("fill",color)
         .attr("font-weight", "bold")
         .text(function(d) {
         if(d=="totalPaidAmountForAvoidableEr")
         d="of total was avoidable"
         else
         d="Total ER spend"
         return d;
         });


         var textAppend=svg.selectAll(".text").data(data).enter()
         .append("text")
         .attr("class","text")
         .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 24 + ")"; })
         .text(function(d, i) {
         return d.erPercent+"%"
         }).attr("font-family", "sans-serif")
         .attr("font-size", "15px")
         .attr("font-weight", "bold")
         .attr("fill", "#b53322");

         var textAppend2=svg.selectAll(".text1").data(data).enter()
         .append("text")
         .attr("class","text1")
         .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 0 + ")"; })
         *//*.attr("x", function(d, i) {
         return i * (100)+5;
         })*//*
         .attr("y",0)
         .text(function(d, i) {
         return "$"+d.er
         }).attr("font-family", "sans-serif")
         .attr("font-size", "15px")
         .attr("font-weight", "bold")
         .attr("fill", "#034f96");

         var x2 = d3.scale.ordinal()
         .rangeBands([0, 400],.4);



         x2.domain(data.map(function(d) { return d.year; }));


         var line = d3.svg.line()
         .x(function(d, i) {
         return x2(d.year); })
         .y(function(d, i) { return y(910504); });

         svg.append("path")
         .datum(data)
         .attr("class", "lineColor")
         .attr("d", line);

         svg.append("text")
         .attr("transform", function(d) { return "translate(" + (x(2015)+80) + "," + y(910504) + ")"; })
         .attr("x", 3)
         .attr("dy", ".35em")
         .attr("font-size", "15px")
         .attr("font-weight", "bold")
         .style("fill","#1c94c4")
         .html("$"+910504);

         svg.append("text")
         .attr("transform", function(d) { return "translate(" + (x(2015)+80) + "," + (y(910504)+16) + ")"; })
         .attr("x", 3)
         .attr("dy", ".35em")
         .attr("font-size", "15px")
         .style("fill","#1c94c4")
         .html("BenchMark");



         });*/

        function render2Data(data,bm) {

            var margin = {top: 20, right: 10, bottom: 20, left: 20},
                    width = 500 - margin.left - margin.right,
                    height = 320 - margin.top - margin.bottom;

            var x = d3.scale.ordinal()
                    .rangeRoundBands([0, 300],.4);

            var y = d3.scale.linear()
                    .rangeRound([height, 0]);

            var color = d3.scale.ordinal()
                    .domain(['totalErVisitsperThousand','totalAvoidableErVisitsperThousand'])
                    .range(["#034f96","#b53322"]);

            var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

            var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickFormat(d3.format(".2s"));

            var svg = d3.select("#bargraph2").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + -25 + "," + margin.top + ")");

            color.domain(d3.keys(data[0]).filter(function(key) {
//                return (key !== "year" && key !== "percentOfAvoidableErVisits"&& key !== "memberMonths");
                return (key !== "year" && key !== "percentOfAvoidableErVisits"&& key !== "memberMonths" && key !== "totalAvoidableErVisits" && key !== "totalErVisits");
            }));

            data.forEach(function(d) {
                var y0 = 0;
                d.ages = color.domain().map(function(name) {
//                    console.log(name)
                    return {name: name, y0: y0, y1: y0 += +d[name]};
                });
                d.total = d.ages[d.ages.length - 1].y1;

            });

            data.sort(function(a, b) { return b.order - a.order; });
            x.domain(data.map(function(d) { return d.year; }));
            y.domain([0, d3.max(data, function(d) { return d.total*1.2; })]);


            svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

            svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Population");

            var state = svg.selectAll(".state")
                    .data(data)
                    .enter().append("g")
                    .attr("class", "g")
                    .attr("transform", function(d) { return "translate(" + x(d.year) + ",0)"; });

            state.selectAll("rect")
                    .data(function(d) { return d.ages; })
                    .enter().append("rect")
                    .attr("width", x.rangeBand())
                    .attr("y", function(d) { return y(d.y1); })
                    .attr("height", function(d) { return y(d.y0) - y(d.y1); })
//                    .style("fill", function(d) { return color(d.name); });
                    .attr("class", function(d) {
                        if(color(d.name)=="#034f96")
                            return "dbar"
                        else
                            return "hbar"

                    });

            var legend = svg.selectAll(".legend")
                    .data(color.domain().slice())
                    .enter().append("g")
                    .attr("class", "legend")
                    .attr("transform", function(d, i) { return "translate(0," + (i * 22-15) + ")"; });

            legend.append("rect")
                    .attr("x", width - 18)
                    .attr("width", 18)
                    .attr("height", 18)
                    .style("fill",function(d) {
                        if(d=="totalAvoidableErVisitsperThousand")
                            d="url(#pattern-stripe)"
                        else
                            d="#034f96"
                        return d;
                    });
//                    .style("fill", color);

            legend.append("text")
                    .attr("x", width -195)
                    .attr("y", 9)
                    .attr("dy", ".35em")
                    .style("text-anchor", "start")
//                    .style("fill",color)
                    .style("fill",function(d) {
                        if(d=="totalAvoidableErVisitsperThousand")
                            d="#b53322"
                        else
                            d="#034f96"
                        return d;
                    })
                    .attr("font-weight", "bold")
                    .text(function(d) {
                        if(d=="totalAvoidableErVisitsperThousand")
                            d="of total was avoidable/1000"
                        else
                            d="Total ER visits/1000"
                        return d;
                    });

            var textAppend=svg.selectAll(".text").data(data).enter()
                    .append("text")
                    .attr("class","text")
                    .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 24 + ")"; })
                    .text(function(d, i) {
                        return Math.round(d.percentOfAvoidableErVisits,2)+"%"
                    }).attr("font-family", "sans-serif")
                    .attr("font-size", "15px")
                    .attr("font-weight", "bold")
                    .attr("fill", "#b53322");

            var textAppend2=svg.selectAll(".text1").data(data).enter()
                    .append("text")
                    .attr("class","text1")
                    .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 0 + ")"; })
                    .text(function(d, i) {
                        return Math.round(d.totalErVisitsperThousand,2)
                    }).attr("font-family", "sans-serif")
                    .attr("font-size", "15px")
                    .attr("font-weight", "bold")
                    .attr("fill", "#034f96");

            var x2 = d3.scale.ordinal()
             .rangeBands([-10, 400],.4);



             x2.domain(data.map(function(d) { return d.year; }));


             var line = d3.svg.line()
             .x(function(d, i) {
             return x2(d.year); })
             .y(function(d, i) { return y(bm); });

             svg.append("path")
             .datum(data)
             .attr("class", "lineColor")
             .attr("d", line);

            var bmMark=d3.max(data, function(d) { return d.year })

             svg.append("text")
             .attr("transform", function(d) { return "translate(" + (x(bmMark)+75) + "," + y(bm) + ")"; })
             .attr("x", 2)
             .attr("dy", ".30em")
             .attr("font-size", "13px")
             .attr("class","text1")
             .attr("font-family", "sans-serif")
             .attr("font-weight", "bold")
             .style("fill","#1c94c4")
             .html(bm+" ER visits/1000");

             /*svg.append("text")
             .attr("transform", function(d) { return "translate(" + (x(2014)+80) + "," + (y(bm)+16) + ")"; })
             .attr("x", 3)
             .attr("dy", ".35em")
             .attr("font-size", "15px")
             .style("fill","#1c94c4")
             .html("BenchMark");*/
        };



        function renderData(data) {

            /*var newVal;
             var data = [];
             $.each(datas,function(key,val) {
             newVal = new Object();
             newVal.percentOfAvoidableEr=val.percentOfAvoidableEr
             newVal.totalPaidAmountForEr=val.totalPaidAmountForEr
             newVal.totalPaidAmountForAvoidableEr=val.totalPaidAmountForAvoidableEr
             newVal.year=val.year
             data.push(newVal);
             });*/

            var margin = {top: 20, right: 10, bottom: 20, left: 20},
                    width = 500 - margin.left - margin.right,
                    height = 320 - margin.top - margin.bottom;

            var x = d3.scale.ordinal()
                    .rangeRoundBands([0, 300],.4);

            var y = d3.scale.linear()
                    .rangeRound([height, 0]);

            var color = d3.scale.ordinal()
                    .range([ "#034f96","#b53322"]);

            var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

            var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .tickFormat(d3.format(".2s"));

            var svg = d3.select("#bargraph1").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + -25 + "," + margin.top + ")");

            color.domain(d3.keys(data[0]).filter(function(key) {
                return (key !== "year" && key !== "percentOfAvoidableEr");
            }));

            data.forEach(function(d) {
                var y0 = 0;
                d.ages = color.domain().map(function(name) {
//                    console.log(name)
                    return {name: name, y0: y0, y1: y0 += +d[name]};
                });
                d.total = d.ages[d.ages.length - 1].y1;
            });

            data.sort(function(a, b) { return b.order - a.order; });

            x.domain(data.map(function(d) { return d.year; }));
            y.domain([0, d3.max(data, function(d) { return d.total*1.2; })]);

            svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

            svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Population");

            var state = svg.selectAll(".state")
                    .data(data)
                    .enter().append("g")
                    .attr("class", "g")
                    .attr("transform", function(d) { return "translate(" + x(d.year) + ",0)"; });

            state.selectAll("rect")
                    .data(function(d) { return d.ages; })
                    .enter().append("rect")
                    .attr("width", x.rangeBand())
                    .attr("y", function(d) { return y(d.y1); })
                    .attr("height", function(d) { return y(d.y0) - y(d.y1); })
//                    .style("fill", function(d) { return color(d.name); });
                 /*   .style("fill",function(d) {
                        return color(d.name);

                    })*/
                    .attr("class", function(d) {
                        if(color(d.name)=="#034f96")
                            return "dbar"
                        else
                            return "hbar"

                    });

            var legend = svg.selectAll(".legend")
                    .data(color.domain().slice())
                    .enter().append("g")
                    .attr("class", "legend")
                    .attr("transform", function(d, i) { return "translate(0," + (i * 22-15) + ")"; });

            legend.append("rect")
                    .attr("x", width - 18)
                    .attr("width", 18)
                    .attr("height", 18)
                    .style("fill",function(d) {
                        if(d=="totalPaidAmountForAvoidableEr")
                            d="url(#pattern-stripe)"
                        else
                            d="#034f96"
                        return d;
                    });
//                    .style("fill", color);

            legend.append("text")
                    .attr("x", width -165)
                    .attr("y", 9)
                    .attr("dy", ".35em")
                    .style("text-anchor", "start")
//                    .style("fill",color)
                    .style("fill",function(d) {
                        if(d=="totalPaidAmountForAvoidableEr")
                            d="#b53322"
                        else
                            d="#034f96"
                        return d;
                    })
                    .attr("font-weight", "bold")
                    .text(function(d) {
                        if(d=="totalPaidAmountForAvoidableEr")
                            d="of total was avoidable"
                        else
                            d="Total ER spend"
                        return d;
                    });

            var textAppend=svg.selectAll(".text").data(data).enter()
                    .append("text")
                    .attr("class","text")
                    .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 24 + ")"; })
                    .text(function(d, i) {
                        return Math.round(d.percentOfAvoidableEr,2)+"%"
                    }).attr("font-family", "sans-serif")
                    .attr("font-size", "15px")
                    .attr("font-weight", "bold")
                    .attr("fill", "#b53322");

            var textAppend2=svg.selectAll(".text1").data(data).enter()
                    .append("text")
                    .attr("class","text1")
                    .attr("transform",function(d, i) { return "translate("+((i * 90)+30)+"," + 0 + ")"; })
                    .text(function(d, i){
                        return d3.format("$,")(Math.round(d.totalPaidAmountForEr));/*Math.round(d.totalPaidAmountForEr)*/
                    }).attr("font-family", "sans-serif")
                    .attr("font-size", "15px")
                    .attr("font-weight", "bold")
                    .attr("fill", "#034f96");

             /*var x2 = d3.scale.ordinal()
             .rangeBands([0, 400],.4);



             x2.domain(data.map(function(d) { return d.year; }));


             var line = d3.svg.line()
             .x(function(d, i) {
             return x2(d.year); })
             .y(function(d, i) { return y(2000); });

             svg.append("path")
             .datum(data)
             .attr("class", "lineColor")
             .attr("d", line);

             svg.append("text")
             .attr("transform", function(d) { return "translate(" + (x(2015)+80) + "," + y(2000) + ")"; })
             .attr("x", 3)
             .attr("dy", ".35em")
             .attr("font-size", "15px")
             .attr("font-weight", "bold")
             .style("fill","#1c94c4")
             .html("$"+910504);

             svg.append("text")
             .attr("transform", function(d) { return "translate(" + (x(2015)+80) + "," + (y(2000)+16) + ")"; })
             .attr("x", 3)
             .attr("dy", ".35em")
             .attr("font-size", "15px")
             .style("fill","#1c94c4")
             .html("BenchMark");*/
        };


        function refreshReport(module) {
            var reportingBasis, regional, fromDate;
            if (module == 'graph1') {
                reportingBasis = $("#reportingBasisg1").val();
//                regional = $("#regionalg1").val();
//                fromDate = $("#fromDateg1").val();
                jQuery.ajax({
                    type: 'POST',
                    dataType: "json",
                    data: {reportingBasis: reportingBasis},
                    url: '<g:createLink controller="erUtilization" action="getChartScriptForGraph1"/>',
                    success: function (resp) {
                        $("#bargraph1").html("");
                        $("#bargraph2").html("");
                            renderData(resp.table1);
                            render2Data(resp.table2,resp.bm2);


                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    },
                    complete: function (XMLHttpRequest, textStatus) {
                    }
                });
            }else if(module=='table2'){
                reportingBasis=$("#reportingBasist2").val();
                fromDate=$("#fromDatet2").val();
                jQuery.ajax({
                    type:'POST',
                    data:{reportingBasis:reportingBasis,regional:regional,fromDate:fromDate,module:module},
                    url:'<g:createLink controller="erUtilization" action="getChartScriptTable2"/>',
                    success:function(resp){
                        $('#totalVisits').html(resp);
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                    },
                    complete:function(XMLHttpRequest,textStatus){
                    }});
            }else if(module=='table3'){
                reportingBasis=$("#reportingBasist3").val();
                fromDate=$("#fromDatet3").val();
                jQuery.ajax({
                    type:'POST',
                    data:{reportingBasis:reportingBasis,regional:regional,fromDate:fromDate,module:module},
                    url:'<g:createLink controller="erUtilization" action="getChartScriptTable3"/>',
                    success:function(resp){
                        $('#table3Graph').html(resp);
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                    },
                    complete:function(XMLHttpRequest,textStatus){
                    }});
            }else if(module=='table4'){
//                reportingBasis=$("#reportingBasist4").val();
                fromDate=$("#fromDatet4").val();
                jQuery.ajax({
                    type:'POST',
                    data:{reportingBasis:reportingBasis,regional:regional,fromDate:fromDate,module:module},
                    url:'<g:createLink controller="erUtilization" action="getChartScriptTable4"/>',
                    success:function(resp){
                        $('#table4Graph').html(resp);
                    },
                    error:function(XMLHttpRequest,textStatus,errorThrown){
                    },
                    complete:function(XMLHttpRequest,textStatus){
                    }});
            }
        }





    </script>





    </body>
</html>