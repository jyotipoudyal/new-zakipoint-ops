<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 11/4/2015
  Time: 3:20 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="layout" content="main">
    <title>Solution Overview</title>
    <g:if test="${isEditable}">
        <meta name="layout" content="mainCms">
    </g:if>
    <g:else>
        <meta name="layout" content="main">
    </g:else>
</head>

<body>
<div class="mainWrapper">
    <div class="contentWrapper">
        <div class="col-md-10" style="display: none;" id="editContent">
            <li>
        <sec:ifAllGranted roles="ROLE_Z5ADMIN">

            <g:if test="${!isEditable}">

                <g:link controller="cms" action="strategicPage" title="Edit Content" ><i class="fa fa-edit"></i> Edit Content</g:link>
        </g:if><g:else>

                <g:link controller="highPharmacyCost" action="strategicPage" title="View Changes" ><i class="fa fa-eye"></i> View Changes</g:link>

        </g:else></sec:ifAllGranted></li></div><br>
        <div class="container">
            <div class="col-md-6">
                <g:link controller="dashboard" action="index" class="text-link-upper mb15">
                    <i class="fa fa-angle-double-left iconRmargin"></i>
                    <span class="isEditable">Population Risk-Diabetes</span>
                </g:link>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <h3 class="mb15"><span class="isEditable">Implementing Telemedicine</span>
                    <a href="#" class="btn-link-upper btn-blue pull-right inline-block"><span class="isEditable">Contact Us</span></a>
                    <div class="clearfix"></div>
                </h3>
                <div class="mb15"><span class="isEditable">Understand the process to implement Telemedicine for your population</span></div>
            </div>


            <div class="clearfix"></div>
            <div class="sep-border"></div>
            <div class="col-md-6 mb15">
                <h3 class="mb15"><span class="isEditable">What is it and how does it work?</span></h3>
                <span class="isEditable">TBD</span>
            </div>
            <div class="clearfix"></div>



            <div class="sep-border"></div>
            <div class="col-md-6 mb15">
                <h3 class="mb15"><span class="isEditable">Solution Partners and Case Studies</span></h3>
                <h4><i class="fa fa-eye"></i><span class="isEditable">Employer X saved $NNN working with American Well</span></h4>
                <h4><i class="fa fa-eye"></i><span class="isEditable">Employer Y brought down ER use by Y% working with Teledoc</span></h4>
            </div>
            <div class="clearfix"></div>

            <div class="sep-border"></div>
            <div class="col-md-6">
                <h3 class="mb15"><span class="isEditable">What to expect?</span></h3>
                <h4><span class="isEditable">X-Y Weeks</span></h4>
                <ul>
                    <li>With your permission, we will send your de-identified data with our partner Telemedicine solutions</li>
                    <li>Quotes and contract terms are obtained for employer evaluation</li>
                    <li>Once decided, zakipoint facilitates data transfer and implementation</li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <h3 class="col-md-12 textBlue mb15">
                Once implemented, you can be track impacts through the Zakipoint platform.
            </h3>


            <div class="clearfix"></div>
            <div class="sep-border"></div>
                <div class="col-md-12 text-center"> <a href="#" class="btn-link-upper btn-blue inline-block mb15"><span class="isEditable"><span>CONTACT US</span><input type="hidden" value="strategicpage.contact"> </span></a>
                    <p> <span class="isEditable textGray"><span>Send us a message to start the process! </span><input type="hidden" value="strategicpage.message.info"> </span></p>
                </div>





        </div>



    </div>

</div>

</body>
</html>