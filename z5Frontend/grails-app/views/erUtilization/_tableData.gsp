<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<div class="z5-table put-border">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
        <thead>
        <tr style="font-size: 15px;">
            <th class="text-left isEditable" ><g:editMessage code="erUtilization.container4.table1.head1"/></th>
            %{--<util:remoteSortableColumn  class="text-center ${defaultSort}"  property="description" title="${editMessage(code:"erUtilization.container4.table1.head1")}" update="topChronicConditionEr" action="sortResult" defaultOrder="asc" />--}%
            <util:remoteSortableColumn  class="text-left ${defaultSort}"  property="percentOfnonErvistors" title="${editMessage(code:"erUtilization.container4.table1.head2")}" update="topChronicConditionEr" action="sortResult" params="[show:show]" />
            <util:remoteSortableColumn    class="text-left"  property="percentOfoneTimeVisitor" title="${editMessage(code:"erUtilization.container4.table1.head3")}" update="topChronicConditionEr" action="sortResult" params="[show:show]"/>
            <util:remoteSortableColumn   class="text-left"  property="percentOftwoOrMoreTimesVisitor" title="${editMessage(code:"erUtilization.container4.table1.head4")}" update="topChronicConditionEr" action="sortResult" params="[show:show]"/>
            <util:remoteSortableColumn class="text-left"  property="totalErCost" title="${editMessage(code:"erUtilization.container4.table1.head5")}" update="topChronicConditionEr" action="sortResult" params="[show:show]"/>
        </tr>
        </thead>
        <tbody>
        <g:if test="${data}">

            <g:each in="${data}" var="data1" status="i">
                <tr class="tableHead">
                    <td class="outerData" style="cursor: pointer;" onclick="showHideColumn('${data1.description.toString().split(" ").join("")}')">
                        ${data1.description}

                    </td>
                    <td ><g:formatNumber number="${data1.percentOfnonErvistors}" type="number" maxFractionDigits="0" /> %</td>
                    <td  > <g:formatNumber number="${data1.percentOfoneTimeVisitor}" type="number" maxFractionDigits="0" /> %</td>
                    <td  > <g:formatNumber number="${data1.percentOftwoOrMoreTimesVisitor}" type="number" maxFractionDigits="0"/> %</td>
                    <td  > $ <g:formatNumber number="${data1.totalErCost}" type="number" maxFractionDigits="0" /> </td>
                </tr>

                        <%
                            def vals=data1.diagnosis;
                            if(vals){
                            vals.sort{it.value}.each {
                        %>
                <g:if test="${show}">
                    <style>
                        .z5-table{
                            height:600px !important;
                        }
                    </style>
                    <tr class=" boldInnerDetails innerData ${data1.description.toString().split(" ").join("")}" style="border: 1px solid gray;">
                </g:if><g:else>
                <tr class=" boldInnerDetails innerData ${data1.description.toString().split(" ").join("")}" style="display: none;border: 1px solid gray;">
                </g:else>

                        <td>${it.value}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <%

                        }
                        }
                    %>

            </g:each>

        </g:if><g:else>
            <tr>
                <td colspan="8">
                    No records to display.
                </td>
            </tr>
        </g:else>


        </tbody>

    </table>

    <%
        //        split page
    %>

</div>
<script language="javascript" type="text/javascript" src="${resource(dir: 'js', file: 'jquery.floatThead.min.js')}"></script>
<script>
    $( document ).ready(function() {
        var $table = $('table.z5-fixed-header');
        $table.floatThead({
            scrollContainer: function($table){
                return $table.closest('.z5-table');
            }
        });
    });
</script>






