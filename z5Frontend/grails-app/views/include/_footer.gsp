<div class="footerWrapper">
    <div class="container">
        <div class="col-md-12">
            <div class="privacy-terms">

            <span>%{--<span style="cursor: pointer;"  onclick="showContent('privacyPolicy')" > *</span>--}%
                <a href="#"class="isEditable "><g:editMessage
                    code="footer.privacy"/></a>
            </span>
            <span><a href="#" class="isEditable"><g:editMessage code="footer.terms"/></a></span>

            </div>
            <span class="isEditable">&copy; <g:editMessage code="footer.content"/> v1.6.0</span>
            <div class="clearfix"></div>
        </div>

    </div>
</div>

<div id="privacyPolicy" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="isEditable" style="overflow-y: auto;overflow-x: auto; height:400px;"> <g:editMessage code="privacyPolicy.content1"/></div>
            </div>
        </div>
    </div>
</div>

<script>
    function showContent(id){
        $("#"+id).modal("show");

    }
</script>