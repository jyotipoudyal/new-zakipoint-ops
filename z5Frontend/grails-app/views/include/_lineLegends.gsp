<div class="lineLegends" style="margin-top: -20px;display: none;">
    <svg width="500" height="20">
        <g transform="translate(150,6)">
            <g><rect y="0" fill="red" x="0" height="10" width="33"></rect>
                <text class="text_y" text-anchor="end" transform="translate(83,10)">Actual</text></g>
        </g>
        <g transform="translate(300,6)">
            <g><rect y="0" fill="grey" x="0" height="10" width="33"></rect>
                <text class="text_y" text-anchor="end" transform="translate(112,10)">Benchmark</text></g>
        </g>
    </svg>
</div>