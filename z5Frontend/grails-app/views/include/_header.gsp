<%@ page import="com.deerwalk.z5.UserCobrandConfig; com.deerwalk.z5.MemberDrillService" %>
<div class="headerWrapper">
    <g:if env="production">
    %{--<g:if env="development">--}%
        <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'prod.css')}">
    </g:if>
    <div class="container client-box-wrapper">
        <div class="col-md-8">
            <g:link controller="dashboard" title="zakipoint Health" class="logo">
            <g:if test="${z5Client()}">
                <img class="pull-right brand-logo-adjustment" src="${createLink(controller:'dashboard', action:'clientBrandLogo')}" />
            </g:if>
                <g:elseif test="${z5User()}">
                <img class="pull-right brand-logo-adjustment" src="${createLink(controller:'dashboard', action:'brandLogo')}" />
            </g:elseif><g:else>
                <g:img dir="images" file="logo-zakipoint-large.png"/>
            </g:else>
            %{--<img src="images/logo-zakipoint-large.png" class="img-responsive" alt="zakipoint Health" />--}%
            </g:link></div>
       %{-- <div class="col-md-5">
            --}%%{--<g:if test="${session.clientname}">
            <ul class="main-menu pull-left">
                <li><a href="${createLink(controller: 'populationRisk',action: 'riskChart')}" title="Home Page" class="isEditable"><g:editMessage code="heading.title1"/> </a></li>
                <li><a href="${createLink(controller: 'dashboard',action: 'index')}#costDrivers" title="Home Page" class="isEditable"><g:editMessage code="heading.title2"/></a></li>
                --}%%{----}%%{--<li><a href="${createLink(controller: 'programTracking',action: 'explorer')}" title="Home Page" class="isEditable">Explorer</a></li>--}%%{----}%%{--
                <li><a href="#"   class="custom-tooltip isEditable"><g:editMessage code="heading.title3"/></a></li>
                <%


                    def memberDrillService = grailsApplication.classLoader.loadClass('com.deerwalk.z5.MemberDrillService').newInstance()
                    def helperService = grailsApplication.classLoader.loadClass('com.deerwalk.z5.HelperService').newInstance()
                    def expPop=memberDrillService.getPopulationExplorer(helperService.getReportingDates(session.ceDate))
                %>
                <li class="isEditable">
                <g:z5Link data-exp="true"  disabledLink="false" controller="memberSearch" action="searchResult" params="${expPop}">
                    <span>Population</span>  </g:z5Link>
                </li>

                --}%%{----}%%{--<a href="${createLink(controller: 'programTracking',action: 'index')}" class="isEditable">Population--}%%{----}%%{----}%%{----}%%{--<g:editMessage code="heading.title4"/>--}%%{----}%%{----}%%{----}%%{--</a></li>--}%%{----}%%{--
            </ul>
            </g:if>--}%%{--
        </div>--}%

        <div class="col-md-4 ">
            %{--<ul class="user-stat pull-right">
                <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                    <li><g:link controller="UITracker"><i class="fa fa-eye"></i> UI</g:link></li>
                </sec:ifAllGranted>
                <li><a href="#" title="${fullname(code:"full")}" ><i class="fa fa-user"></i> <g:fullname/> </a></li>
                <li><g:link controller="logout"><i class="fa fa-power-off"></i> <g:editMessage code="heading.logout.message"/></g:link></li>

            </ul>--}%

            <div  style="float:right;">
            <nav id="primary_nav_wrap">


                <ul class="user-stat pull-right">
                    <li class="dropMain"><a href="#" title="${fullname()}" ><i class="fa fa-user"></i> <g:fullname/> &nbsp;</a>
                        <ul id="dropMenu">
                            <g:if test="${actionName!='setClient'}">
                                <li><g:link controller="dashboard" action="changePasswd"><i class="fa fa-key"></i> Change Password</g:link></li>
                                <li id="addClientView"><a href="#"><i class="fa fa-eye"></i> Change Client View</a></li>
                                <sec:ifAllGranted roles="ROLE_Z5ADMIN">
                                    <li><g:link controller="UITracker"><i class="fa fa-table"></i> UI Tracker</g:link></li>
                                    <li><g:link controller="logAnalytics" action="logsAnalytics"><i class="fa fa-list-alt"></i> User Logs</g:link></li>
                                    <li><g:link controller="request" action="fetchData"><i class="fa fa-server"></i> Response</g:link></li>
                                </sec:ifAllGranted>
                            </g:if>
                        %{--<li id="logoutLink"><g:link  controller="logout" action="index"><i class="fa fa-power-off"></i> <g:editMessage code="heading.logout.message"/></g:link></li>--}%
                            <li id="logoutLink"><a href="${grailsApplication.config.grails.protocol}://${grailsApplication.config.grails.navigationUrl}${grailsApplication.config.grails.hostPath}/logout/index"><i class="fa fa-power-off"></i> <g:editMessage code="heading.logout.message"/></a></li>
                        </ul>
                    </li>
                </ul>



            </nav></div>

        <div style="float:right;margin-top: 20px;padding: 0 7px 0 0;">
            <g:link controller="faq" action="index" title="Frequently Asked Questions" class="icon-provider iconExplorePopulation">
                <g:img  uri="/images/faq.png" alt="FAQ"></g:img>
            </g:link>
        </div>

        <div style="float:right;margin-top: 18px;"><a href="javascript:void(0)" data-toggle="modal" data-target="#cohortInfo" title="New Cohort Request" ><g:img uri="/images/cohortReq.png" class="heading-icon"/></a>
        </div>

        </div>

%{--<g:if test="${controllerName!='dashboard' && !isEditable}">
        <sec:ifAllGranted roles="ROLE_Z5ADMIN">
            <div class="client-box has-clientbox">
        </sec:ifAllGranted>
        <sec:ifNotGranted roles="ROLE_Z5ADMIN">
            <div class="client-box">
        </sec:ifNotGranted>

            --}%%{--<span><span style="color: steelblue;">Viewing Client :</span> <span class="color-gray">
                <g:editMessage code="client.${session.clientname}"/>
            </span></span> &nbsp;<img class=" bordered autoImage pull-right" src="${createLink(controller:'dashboard', action:'clientLogo')}" />--}%%{--
    </div>
</g:if>--}%

</div>

<script>
<sec:ifAllGranted roles="ROLE_Z5ADMIN">
    $(function(){
        $("#logoutLink").before($("#editContent").html());
    })
</sec:ifAllGranted>

</script>




        </div>

