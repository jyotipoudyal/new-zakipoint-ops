<!DOCTYPE html>
<html>
	<head>
		<title><g:if env="development">zakipoint Health</g:if><g:else>Error</g:else></title>
		<meta name="layout" content="mainError">
		<g:if env="development"><link rel="stylesheet" href="${resource(dir: 'css', file: 'errors.css')}" type="text/css"></g:if>
	</head>
	<body>
	<div class="contentWrapper">
		<div class="container">
			<g:hiddenField name="showReportingDetails" value="false"/>
			<g:if env="development">
				%{--<div class="col-md-3"></div>
				<div class="col-md-6" style="color: #044f96;padding: 29px 27px 22px 23px;margin-top: 190px;background-color: #ddd;">
					<h4 class="weltitle mb15">
						<p>An error has occurred while processing your request.</p>
						<p>"Please try again and, if the issue persists, contact your Client Service Manager."</p>
					</h4>
					<g:link controller="dashboard" action="index">${session.clientname}.zakipointhealth.com</g:link>
				</div>
				<div class="col-md-3"></div>--}%
			<g:renderException exception="${exception}" />
		</g:if>
		<g:else>
			<div class="col-md-3"></div>
			<div class="col-md-6" style="color: #044f96;padding: 29px 27px 22px 23px;margin-top: 190px;background-color: #ddd;">
				<h4 class="weltitle mb15">
					<p>An error has occurred while processing your request.</p>
					<p>"Please try again and, if the issue persists, contact your Client Service Manager."</p>
				</h4>
				<g:link controller="dashboard" action="index">${session.clientname}.zakipointhealth.com</g:link>
			</div>
			<div class="col-md-3"></div>
		</g:else>
		</div>
		</div>
	</body>
</html>
