<%--
  Created by IntelliJ IDEA.
  User: nisharma
  Date: 3/27/17
  Time: 2:22 PM
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>FAQ</title>
    <meta name="layout" content="main">
</head>
<body>
<div class="contentWrapper">
    <div class="container">
        <g:hiddenField name="showReportingDetails" value="false"/>

        <div class="col-md-12">


            <div id="list-UITracker" class="content scaffold-list" role="main">
                <h3>Frequently Asked Questions!</h3>
                </div>


                </div>
        <div class="clearfix"></div>

        <div class="sep-border mb15"></div>

        <div class="panel-group" id="accordion1">
        <div class="panel-group" id="accordion">
        <g:each in="${data}" var="val" status="i">
            <div class="faqHeader">
                <a class="accordion-toggle ${i==0?'':'collapsed'}" data-toggle="collapse" data-parent="#accordion1" href="#collapseDiv-${i}">
                    Category: ${val.key}
                </a>
            </div>
            <div id="collapseDiv-${i}" class="panel-collapse collapse ${i==0?'in':''}">
            <g:each in="${val.value}" var="values" status="j">
            <div class="panel panel-default">
                <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" id="question${values['gsx$sn']}" data-toggle="collapse" data-parent="#accordion" href="#collapse-${values['gsx$sn']}">

                                <strong>Q</strong>${values['gsx$sn']}. ${values['gsx$question']}
                            </a>
                    </h4>
                </div>
                <div id="collapse-${values['gsx$sn']}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div style="margin-left: 50px">${values['gsx$answer']}</div>
                        <div class="clearfix"></div>
                        <g:if test="${values['gsx$relatedto']}">
                            <div  style="margin-left: 50px;padding: 0;margin-top: 4px;float: left;width: 75px;"><i>Related To : </i></div>
                            <g:each in="${values['gsx$relatedto']}" var="relatedTo" >


                                       <a href="javascript:void(0)" onclick="openQuestion(${relatedTo})">
                                           <div style="float: left;width: 29px;  margin-right: 5px;"><h5><span class="label label-info">${relatedTo}</span></h5></div>
                                       </a>

                            </g:each>
                            <div class="clearfix"></div>
                        </g:if>

                        <g:if test="${values['gsx$referto']}">
                            <div style="padding: 0;margin-top: 4px; float: left;width: 75px;"><i>Refer To : </i></div>

                                <g:each in="${values['gsx$referto']}" var="referTo" >

                                        <div style="float: left;width: 29px;  margin-right: 5px;">
                                            <a href="javascript:void(0)" onclick="openQuestion(${referTo})">
                                            <h5><span class="label label-info"> ${referTo}</span></h5></a></div>
                                </g:each>

                            <div class="clearfix"></div>
                        </g:if>


                    </div>
                </div>
            </div>
                </g:each>
            </div>

            </g:each>
    </div>
    </div>




        %{--<div class="z5-table put-border" style="min-height: 700px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="z5-fixed-header">
        <thead>
        <tr class="heading">
            <g:each in="${columns}" var="column" status="i">

            <th style="text-align:center;" id="th_${column.key}" data-class="${column.key}">${column.value.getAt('displayName')}</th>
            </g:each>
        </tr>
        </thead>
        <tbody>
        <g:if test="${data}">
            <g:each in="${data}" var="val" status="i">
                <tr>
                    <g:each in="${columns}" var="col" status="j">
                        <g:if test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Number.toString()}">
                            <td style="text-align: center">${val.getAt(col.key)}</td>
                        </g:if>
                        <g:elseif test="${col.value.getAt('type').toString() == com.z5.FieldContentType.Amount.toString() || col.value.getAt('type').toString() == com.z5.FieldContentType.LargeAmount.toString()}">

                            <td style="text-align: right">
                                <strong><g:formatZ5Currency number="${val.getAt(col.key)}" type="number" maxFractionDigits="2" />
                                </strong>
                            </td>
                        </g:elseif>

                        <g:else>
                            <td style="text-align: left">${val.getAt(col.key)}</td>
                        </g:else>
                    </g:each>
                <tr>
                <tr>

                </tr>
            </g:each>
        </g:if>
        </tbody>
    </table>
</div>--}%
</div>
</div>
<script>
    function openQuestion(id){
        $("#question"+id).focus();

    }
</script>

<style>
/*#0070C0*/
.panel-default>.panel-heading {
    background-color: #eaf4ff;
    border-color: #ddd;
    color: #333 !important;
}


.faqHeader {
    font-size: 20px;
    margin: 5px 0 5px 0;
    padding: 5px;
    /*color: #fff;*/
    background: steelblue;
}
.faqHeader>a{color: #fff}

.panel-collapse{
    background: #ccc;
}

.faqHeader [data-toggle="collapse"]:after {
    font-family: 'Glyphicons Halflings';
    content: "\e072"; /* "play" icon */
    float: right;
    color: #fff;
    font-size: 18px;
    line-height: 22px;
    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    -o-transform: rotate(-90deg);
    transform: rotate(-90deg);
}

.faqHeader [data-toggle="collapse"].collapsed:after {
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
    color: #454444;
}



.panel-heading [data-toggle="collapse"]:after {
    font-family: FontAwesome;
    content: "\f078";
    float: right;
    color: steelblue;
    font-size: 14px;
    -webkit-transform: rotate(-180deg);
    -moz-transform: rotate(-180deg);
    -ms-transform: rotate(-180deg);
    -o-transform: rotate(-180deg);
    transform: rotate(-180deg);
}

.panel-heading [data-toggle="collapse"].collapsed:after {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
    color: #454444;
}
</style>



</body>
</html>