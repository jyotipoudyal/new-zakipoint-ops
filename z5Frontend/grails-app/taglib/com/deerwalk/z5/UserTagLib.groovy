package com.deerwalk.z5

import com.deerwalk.security.DwClient
import org.codehaus.groovy.grails.plugins.codecs.HTMLCodec
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.codehaus.groovy.grails.web.taglib.GroovyPageAttributes
import org.codehaus.groovy.grails.web.util.WebUtils
import org.springframework.context.MessageSourceResolvable
import org.springframework.context.NoSuchMessageException
import org.springframework.context.support.DefaultMessageSourceResolvable
import org.springframework.util.StringUtils
import org.springframework.web.servlet.support.RequestContextUtils

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat

class UserTagLib {
    def springSecurityService
    def displayService
    def helperService

    def fullname = { attrs ->
        def user=springSecurityService.currentUser
        /*if(name.toString().length()>10)
            name=name.toString().substring(0,9)+"..."

        if(attrs.code=="full")
            name=springSecurityService.currentUser
            */
        if (springSecurityService.isLoggedIn()) {
            out << user.firstname
        }
    }

    def z5User = { attrs ->
        def userBrand=UserCobrandConfig.findByUser(springSecurityService.currentUser)
        /*if(name.toString().length()>10)
            name=name.toString().substring(0,9)+"..."

        if(attrs.code=="full")
            name=springSecurityService.currentUser
            */
        if (springSecurityService.isLoggedIn() && userBrand) {
            out << userBrand
        }
    }

    def z5Client = { attrs ->
        def clientBrand=ClientCobrandConfig.findByClient(DwClient.findByName(session.clientname))

        if (springSecurityService.isLoggedIn() && clientBrand) {
            out << clientBrand
        }
    }



    def editMessage={ attrs, body->
        def vm = displayService.showContent(attrs.code)
        if(attrs.args){
            if(attrs.args[0]){
                vm=vm.toString().replace("{{0}}",attrs.args[0].toString())
            }

        }
        if (vm) {
            out << "<span>"+vm+"</span><input type='hidden' value='${attrs.code}'/>"
        }
    }

    def pdfMessage={ attrs, body->
        def vm = displayService.showContent(attrs.code)
        if(attrs.args){
            if(attrs.args[0]){
                vm=vm.toString().replace("{{0}}",attrs.args[0].toString())
            }
        }
        vm= vm.toString();
//        vm= vm.toString().replaceAll("\\<[^>]*>","")
        vm= vm.replaceAll("<br\\/>","#bp#");
        vm= vm.replaceAll("<br>","#bp#");
//        vm= vm.replaceAll("\\n","#bp@#");
//        vm= vm.replaceAll("#bp#","<br><\\/br>");
        vm= vm.replaceAll("#bp#","<br><\\/br>");
        if (vm) {
            out << vm
        }
    }

    def z5Link = { attrs, body ->
        def writer = getOut()
        def elementId = attrs.remove('elementId')
        def linkAttrs
        if (attrs.params instanceof Map && attrs.params.containsKey('attrs')) {
            linkAttrs = attrs.params.remove('attrs').clone()
        }
        else {
            linkAttrs = [:]
        }
        if (attrs.disabledLink=="false"){
            writer <<  '<a data-href=\"'
            writer << createLink(attrs)//.encodeAsHTML()
            writer << '"'
            if (elementId) {
                writer << " id=\""
                writer << elementId
                writer << "\""
            }
            linkAttrs << attrs
            linkAttrs.each { k, v ->
                writer << ' '
                writer << k
                writer << '='
                writer << '"'
                writer << v?.encodeAsHTML()
                writer << '"'
            }
            writer << " onclick=\""
            writer << "postRequestForGet(this);return false;"
            writer << "\""
            writer << " class=\"drill-enabled\""
            writer << '>'
            writer << body()
            writer << '</a>'


        }else{
            writer << '<span'
            linkAttrs << attrs
            linkAttrs.each { k, v ->
                writer << ' '
                writer << k
                writer << '='
                writer << '"'
                writer << v?.encodeAsHTML()
                writer << '"'
            }
            writer << '>'
            writer << body()
            writer << '</span>'

        }
    }

    def pdfMessageCsv={ attrs, body->
        def vm = displayService.showContent(attrs.code)
        if(attrs.args){
            if(attrs.args[0]){
                vm=vm.toString().replace("{{0}}",attrs.args[0].toString())
            }
        }
        vm= vm.toString();
        vm= vm.toString().replaceAll("\\<[^>]*>","")
        if (vm) {
            out << vm
        }
    }

    def pdfMessageRemoveA={ attrs, body->
        def vm = displayService.showContent(attrs.code)
        if(attrs.args){
            if(attrs.args[0]){
                vm=vm.toString().replace("{{0}}",attrs.args[0].toString())
            }
        }
        vm= vm.toString();
//        vm= vm.toString().replaceAll("\\<[^>]*>","")
        vm= vm.replaceAll("<br\\/>","#bp#");
        vm= vm.replaceAll("<br>","#bp#");
//        vm= vm.replaceAll("\\n","#bp@#");
        vm= vm.replaceAll("#bp#","<br><\\/br>");
        vm = vm.replaceAll("(?s)<a.*?</a>", "");
        if (vm) {
            out << vm
        }
    }

    def displayMessage={ attrs, body->
        def vm = displayService.showContent(attrs.code)
        if (vm) {
            out << vm
        }
    }

    def resolveLocale(localeAttr) {
        def locale = localeAttr
        if (locale != null && !(locale instanceof Locale)) {
            locale = StringUtils.parseLocaleString(locale as String)
        }
        if (locale == null) {
            locale = RequestContextUtils.getLocale(request)
            if (locale == null) {
                locale = Locale.getDefault()
            }
        }
        return locale
    }

    def ifAllGranted = { attrs, body ->
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest();
        def clientName=getClientname(request).toUpperCase()
//        attrs.roles="ROLE_${clientName}_"+attrs.roles
        def roleArr=[]
        attrs.roles.split(",").each {
            roleArr.add("ROLE_${clientName}_"+it)
        }
        attrs.roles=roleArr.join(", ")
        String roles = assertAttribute('roles', attrs, 'ifAllGranted')
        if (SpringSecurityUtils.ifAllGranted(roles)) {
            out << body()
        }
    }

    def assertAttribute(String name, attrs, String tag) {
        if (!attrs.containsKey(name)) {
            throwTagError "Tag [$tag] is missing required attribute [$name]"
        }
        attrs.remove name
    }

    def getClientname(request){
        String requestHost = request.getHeader('Host')
        if (requestHost.indexOf(":") > 0) {
            requestHost = requestHost.split(":")[0]
        }
        def requestedClient = requestHost.substring(0, requestHost.indexOf("."))
        if (requestedClient.indexOf("-") > 0) {
            requestedClient = requestedClient.split("-")[0]
        }
        return requestedClient
    }




    def formatZ5Currency = { attrs ->
        if (!attrs.containsKey('number')) {
            throwTagError("Tag [formatNumber] is missing required attribute [number]")
        }

        def number = attrs.number
        def no=number
        if (number == null) return

        def formatName = attrs.formatName
        def format = attrs.format
        def type = attrs.type
        def locale = resolveLocale(attrs.locale)

        DecimalFormatSymbols dcfs =new DecimalFormatSymbols()

        DecimalFormat decimalFormat = NumberFormat.getNumberInstance(locale)


        if (attrs.nan) {
            dcfs.naN = attrs.nan
            decimalFormat.decimalFormatSymbols = dcfs
        }

        // ensure formatting accuracy
        decimalFormat.setParseBigDecimal(true)

        if (attrs.maxFractionDigits != null) {
            decimalFormat.setMaximumFractionDigits(attrs.maxFractionDigits as Integer)
        }
        if (attrs.minFractionDigits != null) {
            decimalFormat.setMinimumFractionDigits(attrs.minFractionDigits as Integer)
        }
        if(no>=1000) number=number/1000;


        if (!(number instanceof Number)) {
            number = decimalFormat.parse(number as String)
        }


        def formatted
        try {
            formatted = decimalFormat.format(number)
        }
        catch(ArithmeticException e) {
            // if roundingMode is UNNECESSARY and ArithemeticException raises, just return original number formatted with default number formatting
            formatted = NumberFormat.getNumberInstance(locale).format(number)
        }

        if(no>=1000) formatted=("\$"+formatted+" K")
        else{
            formatted=("\$"+(formatted.tokenize(".")[0]))
        }

        out <<  formatted
    }









   /* def remotePaginate = {attrs ->
        def writer = out

        if (attrs.total == null)
            throwTagError("Tag [remotePaginate] is missing required attribute [total]")

        if (attrs.update == null)
            throwTagError("Tag [remotePaginate] is missing required attribute [update]")

        if (!attrs.action)
            throwTagError("Tag [remotePaginate] is missing required attribute [action]")

        def messageSource = grailsAttributes.getApplicationContext().getBean("messageSource")
        def locale = RequestContextUtils.getLocale(request)

        Integer total = attrs.total.toInteger()
        Integer offset// = params.offset?.toInteger()
        Integer max// = params.max?.toInteger()
        Integer maxsteps// = params.maxsteps?.toInteger()
        def tabName = attrs.tabName ? attrs.tabName :null
        def tabId = attrs.tabId ? attrs.tabId :null

        def pageSizes = attrs.pageSizes ?: []
        Boolean alwaysShowPageSizes = new Boolean(attrs.alwaysShowPageSizes?:false)
        Map linkTagAttrs = attrs
        if (!offset) offset = (attrs.offset ? attrs.offset.toInteger() : 0)

        if (!max) max = (attrs.max ? attrs.max.toInteger() : 5)

        if (!maxsteps) maxsteps = (attrs.maxsteps ? attrs.maxsteps.toInteger() : 10)

        Map linkParams = [offset: offset - max, max: max]
        Map selectParams = [:]
        if (params.sort) linkParams.sort = params.sort
        if (params.order) linkParams.order = params.order
        if (attrs.params) {
            linkParams.putAll(attrs.params)
            selectParams.putAll(linkParams)
        }

        if (attrs.id != null) {linkTagAttrs.id = attrs.id}
        linkTagAttrs.params = linkParams

        // determine paging variables
        boolean steps = maxsteps > 0
        Integer currentstep = (offset / max) + 1
        Integer firststep = 1
        Integer laststep = Math.round(Math.ceil(total / max))\

        writer <<"<div id='paginate' class='pagination'>"
        // display previous link when not on firststep
        if (currentstep > firststep) {
            linkTagAttrs.class = 'prevLink'
            linkParams.offset = offset - max
            writer << "<a href='javascript:void(0);' onclick='getMoreData(\"${currentstep-1}\",\"${tabName}\",\"${tabId}\")' class='pag_pre'><div class='ui-icon ui-icon-triangle-1-w' style='float:left;'></div>Previous</a> "
        }

        // display steps when steps are enabled and laststep is not firststep
        if (steps && laststep > firststep) {
            linkTagAttrs.class = 'step'

            // determine begin and endstep paging variables
            Integer beginstep = currentstep - Math.round(maxsteps / 2) + (maxsteps % 2)
            Integer endstep = currentstep + Math.round(maxsteps / 2) - 1

            if (beginstep < firststep) {
                beginstep = firststep
                endstep = maxsteps
            }
            if (endstep > laststep) {
                beginstep = laststep - maxsteps + 1
                if (beginstep < firststep) {
                    beginstep = firststep
                }
                endstep = laststep
            }

            // display firststep link when beginstep is not firststep
            if (beginstep > firststep) {
                linkParams.offset = 0
                writer << "<a href='javascript:void(0);' class='' onclick='getMoreData(\"${firststep}\",\"${tabName}\",\"${tabId}\")'>${helperService.getFormattedData(firststep)}</a>"//remoteLink(linkTagAttrs.clone()){firststep.toString()}
                writer << '<span class="step">..</span>'
            }

            // display paginate steps
            (beginstep..endstep).each {i ->
                if (currentstep == i) {
                    writer << "<span class=\"currentStep ui-state-focus\">${helperService.getFormattedData(i)}</span>"
                } else {
                    linkParams.offset = (i - 1) * max
                    writer << "<a href='javascript:void(0);'  class='' onclick='getMoreData(\"${i}\",\"${tabName}\",\"${tabId}\")'>${helperService.getFormattedData(i)}</a>"
                }
            }

            // display laststep link when endstep is not laststep
            if (endstep < laststep) {
                writer << '<span class="step">..</span>'
                linkParams.offset = (laststep - 1) * max
                writer <<  "<a href='javascript:void(0);' class='' onclick='getMoreData(\"${laststep}\",\"${tabName}\",\"${tabId}\")'>${helperService.getFormattedData(laststep)}</a>"
            }
        }
        // display next link when not on laststep
        if (currentstep < laststep) {
            linkTagAttrs.class = 'nextLink'
            linkParams.offset = offset + max
            writer << "<a href='javascript:void(0);'  class='pag_next' onclick='getMoreData(\"${currentstep+1}\",\"${tabName}\",\"${tabId}\")'>Next<div class='ui-icon ui-icon-triangle-1-e' style='float:right;'></div></a>"
        }
        if(attrs.max==1){
            pageSizes=null;
        }
        if (*//*(alwaysShowPageSizes || total > max) &&*//* pageSizes) {
            selectParams.remove("max")
            selectParams.offset=0
            String paramsStr = selectParams.collect {it.key + "=" + it.value}.join("&")
            paramsStr = '\'' + paramsStr + '&max=\' + this.value'
            linkTagAttrs.params = paramsStr
            Boolean isPageSizesMap = pageSizes instanceof Map

            writer << "<span style='border:none; padding:0 0 3px 0;margin-top:0 !important;'>" + select(from: pageSizes, value: max, name: tabName?"max${tabName}":"max", onchange:"changePageSize(this.value,'${tabName}','${tabId}')" ,class: 'remotepagesizes pag_drop_down font', optionKey: isPageSizesMap?'key':'', optionValue: isPageSizesMap?'value':'') + "</span>"
        }else{
            writer << "<span style='border:none; padding:0 0 3px 0;margin-top:0 !important;'><input type='hidden' name='${tabName?"max"+tabName:"max"}' value='1'/></span>"
        }
        writer << "</div>"
    }*/

}
