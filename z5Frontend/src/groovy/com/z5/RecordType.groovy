package com.z5

/**
 * Created by nisharma on 10/9/2015.
 */
enum RecordType {
    Medical('Medical'),
    Eligibility('Eligibility'),
    Pharmacy('Pharmacy'),
    Lab('Lab'),
    Regimen_Usage('Regimen_Usage'),
    Care_Plan('Care_Plan'),
    Diagnosis('Diagnosis'),
    Lab_Results('Lab_Results'),
    Demographics('Demographics'),
    Biometrics('Biometrics'),
    HRA('HRA'),
    Workers_Comp('Workers_Comp','Workers Comp'),
    Vision('Vision'),
    Dental('Dental'),
    Contracts('Contracts'),
    Case_Audit('Case_Audit'),
    Notes('Notes'),
    Vendor('Vendor'),
    Utilization('Utilization'),
    KYN('KYN'),
    OSHA('OSHA'),
    Episodes('Episodes'),
    ER_Visit('ER_Visit','ER visit'),
    Participation('Participation'),
    EMR_Lab('EMR_Lab', 'Lab EMR'),
    Vitals('Vitals'),
    DiagnosisEMR('DiagnosisEMR','Diagnosis EMR'),
    ProcedureEMR('ProcedureEMR','Procedure EMR'),
    PrescriptionEmr('PrescriptionEmr','Prescription EMR'),
    AppointmentsEmr('AppointmentsEmr','Appointments EMR'),
    VisitEmr('VisitEmr','Visit EMR'),
    Disability('Disability'),
    Chronic_Condition('Chronic_Condition'),
    Immunization('Immunization'),
    Allergy('Allergy'),
    ExamScreening('ExamScreening','Exam Screening')

    final String id;
    final String displayName;

    RecordType(String id) {this(id,id)}
    RecordType(String id,String displayName) { this.id = id;this.displayName=displayName }

    static RecordType getType(String id) {
        RecordType ret = RecordType.No;
        EnumSet.allOf(RecordType).each { RecordType enm ->
            if(enm.id == id) {
                ret = enm
            }
        }

        return ret;
    }

    String getId() {
        return(this.id);
    }

    public static RecordType getById(id){
        return RecordType.values().find{it.id == id};
    }

}
