package com.z5

/**
 * Created by nisharma on 10/9/2015.
 */
enum Module {
    DataSearch('DataSearch'),
    MemberSearch('MemberSearch'),
    DynamicReport('DynamicReport'),
    Top20Report('Top20Report'),
    ClinicalAnalytics('ClinicalAnalytics'),
    CustomReport('CustomReport'),
    QualityOfCare('QualityOfCare'),
    QualityMetricDrillDown('QualityMetricDrillDown'),
    QualityOfCareDrillDown('QualityOfCareDrillDown'),
    DashBoard('DashBoard'),
    MemberProfile('MemberProfile'),
    RiskAnalysis('RiskAnalysis'),
    AutoComplete('AutoComplete'),
    PatientSearch('PatientSearch'),
    PhysicianSearch('PhysicianSearch'),
    PharmacyReport('PharmacyReport'),
    DataView('DataView'),
    FAReportView('FAReportView')
    final String id;

    Module(String id) { this.id = id }

    static Module getType(String id) {
        Module ret = Module.No;
        EnumSet.allOf(Module).each { Module enm ->
            if(enm.id == id) {
                ret = enm
            }
        }

        return ret;
    }

    String getId() {
        return(this.id);
    }
}
