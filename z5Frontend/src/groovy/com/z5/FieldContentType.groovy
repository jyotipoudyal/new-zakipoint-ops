package com.z5

/**
 * Eum containing Content type of the Fields
 * User: Ashish
 * Date: Aug 5, 2011
 * Time: 4:05:04 PM
 */

public enum FieldContentType {
      Text('Text'),
      Date('Date'),
      Custom('Custom'),
      Amount('Amount'),
      LargeAmount('LargeAmount'),
      Number('Number'),
      Code('Code'),
      Percent('Percent'),
	WholeNumber('WholeNumber'),
	BigText('BigText'),
	DateTime('DateTime')
      final String id;

      FieldContentType(String id) { this.id = id }

      static FieldContentType getType(String id) {
            FieldContentType ret = FieldContentType.No;
            EnumSet.allOf(FieldContentType).each { FieldContentType enm ->
                  if(enm.id == id) {
                        ret = enm
                  }
            }

            return ret;
      }

      String getId() {
            return(this.id);
      }
}