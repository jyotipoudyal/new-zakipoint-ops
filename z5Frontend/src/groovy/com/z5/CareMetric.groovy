package com.z5

/**
 * Created by nisharma on 2/17/2016.
 */
enum CareMetric {

    asthma('E'),
    hyperlipidemia('G'),
    rheumatoidarthritis('R'),
    cad('A'),
    hypertension('F'),
    diabetes('D'),
    copd('C'),
    chf('B')

    final String value

    public CareMetric(String value) {
        this.value = value
    }

    @Override
    String toString(){ value }
    String getKey() { name() }

}