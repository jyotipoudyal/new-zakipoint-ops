package report

import org.apache.poi.xslf.usermodel.XMLSlideShow
import org.apache.poi.xslf.usermodel.XSLFPictureData
import org.apache.poi.xslf.usermodel.XSLFPictureShape
import org.apache.poi.xslf.usermodel.XSLFSlide
import sun.misc.BASE64Decoder

import java.awt.Rectangle

/**
 * Created by nisharma on 10/26/16.
 */
class PptBuilder {
    String slideName;
    Integer index;
    String slideType;
    String parameter;



    public String createSlide(){
        def sourceData,parts,imageString;
        byte[] imageByte;
        BASE64Decoder decoder
        def imageMap=[:]

        sourceData = this.parameter;
        parts = sourceData.tokenize(",");
        imageString = parts[1];
        decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);

        XMLSlideShow ppt = new XMLSlideShow();

        // XSLFSlide#createSlide() with no arguments creates a blank slide
//        XSLFSlide blankSlide =  ppt.createSlide();


        /* XSLFSlideMaster master = ppt.getSlideMasters().get(0);

         XSLFSlideLayout layout1 = master.getLayout(SlideLayout.TITLE);
         XSLFSlide slide1 = ppt.createSlide(layout1) ;
         XSLFTextShape[] ph1 = slide1.getPlaceholders();
         XSLFTextShape titlePlaceholder1 = ph1[0];
         titlePlaceholder1.setText("This is a title");
         XSLFTextShape subtitlePlaceholder1 = ph1[1];
         subtitlePlaceholder1.setText("this is a subtitle");

         XSLFSlideLayout layout2 = master.getLayout(SlideLayout.TITLE_AND_CONTENT);
         XSLFSlide slide2 = ppt.createSlide(layout2) ;
         XSLFTextShape[] ph2 = slide2.getPlaceholders();
         XSLFTextShape titlePlaceholder2 = ph2[0];
         titlePlaceholder2.setText("This is a title");
         XSLFTextShape bodyPlaceholder = ph2[1];
         // we are going to add text by paragraphs. Clear the default placehoder text before that
         bodyPlaceholder.clearText();
         XSLFTextParagraph p1 = bodyPlaceholder.addNewTextParagraph();
         p1.setIndentLevel(0);
         p1.addNewTextRun().setText("Level1 text");
         XSLFTextParagraph p2 = bodyPlaceholder.addNewTextParagraph();
         p2.setIndentLevel(1);
         p2.addNewTextRun().setText("Level2 text");
         XSLFTextParagraph p3 = bodyPlaceholder.addNewTextParagraph();
         p3.setIndentLevel(2);
         p3.addNewTextRun().setText("Level3 text");*/
        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
//        XSLFSlideLayout layout3 = master.getLayout(SlideLayout.TITLE);
        XSLFSlide slide3 = ppt.createSlide() ;
//        File image=new File("image.png");
//        byte[] pictureData = IOUtils.toByteArray(new FileInputStream(image));
//        byte[] pictureData = imageByte
        XSLFPictureData pd = ppt.addPicture(imageByte, org.apache.poi.sl.usermodel.PictureData.PictureType.PNG);
        XSLFPictureShape pic = slide3.createPicture(pd);
        pic.setAnchor(new Rectangle(20, 20, 1000, 600));
//        slide3.addShape(pic);





//-----------------------------------------------------
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        ppt.write(output);
        ppt.close();



        response.addHeader("Content-Type", "application/ppt");
        response.addHeader("Content-Disposition", "attachment; filename=Charts.pptx");
        response.getOutputStream().write(output.toByteArray());

        return this.slideType+"slide created"


    }


}
