package report

/**
 * Created by nisharma on 11/22/16.
 */
interface ReportGenerator {

    public boolean generate();

    def public getFinalData();

    public boolean isReady();

    public void reset();

    String getReportId()

    String getReportName()

}