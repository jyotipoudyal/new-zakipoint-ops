package report

/**
 * Created by nisharma on 11/29/16.
 */
public class TopOutOfNetworkProviderReport extends DynamicReportGenerator{

    def topOutOfNetworkProviderColumns = [
                                          providerName:[displayName: "Provider Name",type: FieldContentType.Text],
                                          totalMembers:[displayName: "Total Members",type: FieldContentType.Number],
                                          totalPaidAmount:[displayName: "Total Paid Amount",type: FieldContentType.Amount],
    ]



    protected TopOutOfNetworkProviderReport(ReportBuilder reportBuilder){
        super(reportBuilder)
    }


    public static class ReportGeneratorBuilder extends ReportBuilder<TopOutOfNetworkProviderReport>{
        public ReportGeneratorBuilder(Map dynamicReportParams,Map params) {
            super(dynamicReportParams,params);
        }
        @Override
        public TopOutOfNetworkProviderReport build() {
            return new TopOutOfNetworkProviderReport(this);
        }


    }


    @Override
    protected boolean prepare() throws Exception {
        def incomingJsonArray
        def data
        def formattedData
        def jsonData
        long mapStartTime = System.nanoTime();

//        jsonData = reportingFetchService.getCareAnalyticsData(dynamicReportParams,[:], backendReportId,getExtraParameters(),'highOutNetworkMemberBenchMarks');
        jsonData = reportFetchService.getDataRequest(dynamicReportParams,"topOutOfNetworkProvider")

        if (!jsonData) {
            log.error("No Data Received for "+getClass().getSimpleName());
            throw new Exception("No Data Received")
        }

        data =jsonData

        formattedData = getFormattedData(topOutOfNetworkProviderColumns, data)

        processedData = formattedData;
        isReady = (processedData && processedData.size() >0);
        println("Data MAP Time : ${((System.nanoTime() - mapStartTime) / 1000000000)} seconds.")
        return isReady;
    }

    public getFormattedData(columns, jsonData){
        def formattedData= []
        jsonData = jsonData.sort{it.value.description}
        jsonData.each{key, eachData->
            def rowData =[:]
            if(eachData){
                eachData.each {
                    if (it.value != null && it.value != ""){
                        def value = it.value
                        def formattedValue
                        if (columns[it.key]?.type == FieldContentType.Amount || columns[it.key]?.type == FieldContentType.LargeAmount){
                            formattedValue = helperService.getFormattedCurrency((Double)value)
                        } else if(columns[it.key]?.type == FieldContentType.Number) {
                            formattedValue = helperService.getFormattedDoubleValue((Double)value)
                        } else {
                            formattedValue = value
                        }
                        rowData.put(it.key, formattedValue)
                    }else{
                        rowData.put(it.key, '- -')
                    }
                }
            }
            formattedData.add(rowData)
        }
        return formattedData
    }
}
