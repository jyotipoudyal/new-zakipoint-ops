package report

/**
 * Created by nisharma on 11/29/16.
 */
public class TopOonProviderLocationReport extends DynamicReportGenerator {

    def topOonProviderLocationColumns = [
            state:[displayName: "State",type: FieldContentType.Text],
            city:[displayName: "City",type: FieldContentType.Text],
            memberCount:[displayName: "Member Count",type: FieldContentType.Number],
            oonProviderCount:[displayName: "OON Provider Count",type: FieldContentType.Number],
            totalPaidAmount:[displayName: "Total Paid Amount",type: FieldContentType.Amount],
    ]



    protected TopOonProviderLocationReport(ReportBuilder reportBuilder){
        super(reportBuilder)
    }


    public static class ReportGeneratorBuilder extends ReportBuilder<TopOonProviderLocationReport>{
        public ReportGeneratorBuilder(Map dynamicReportParams,Map params) {
            super(dynamicReportParams,params);
        }
        @Override
        public TopOonProviderLocationReport build() {
            return new TopOonProviderLocationReport(this);
        }
    }


    @Override
    protected boolean prepare() throws Exception {
        def incomingJsonArray
        def data
        def formattedData
        def jsonData
        long mapStartTime = System.nanoTime();

        jsonData = reportFetchService.getDataRequest(dynamicReportParams,"topOutOfNetworkProviderLocation")

        if (!jsonData) {
            log.error("No Data Received for "+getClass().getSimpleName());
            throw new Exception("No Data Received")
        }
        data =jsonData
        formattedData = getFormattedData(topOonProviderLocationColumns, data)
        processedData = formattedData;
        isReady = (processedData && processedData.size() >0);
        println("Data MAP Time : ${((System.nanoTime() - mapStartTime) / 1000000000)} seconds.")
        return isReady;
    }

    public getFormattedData(columns, jsonData){
        def formattedData= []
        jsonData = jsonData.sort{it.value.description}
        jsonData.each{key, eachData->
            def rowData1 =new ArrayList();
            def finalData=[];
            if(eachData){
                def newArr=[];
                eachData.each {k,v->
                    def rowData =[:];
                    v.each{
                        if (it.value != null && it.value != ""){
                            def value = it.value
                            def formattedValue
                            if (columns[it.key]?.type == FieldContentType.Amount || columns[it.key]?.type == FieldContentType.LargeAmount){
                                formattedValue = helperService.getFormattedCurrency((Double)value)
                            } else if(columns[it.key]?.type == FieldContentType.Number) {
                                formattedValue = helperService.getFormattedDoubleValue((Double)value)
                            } else {
                                formattedValue = value
                            }
                            rowData.put(it.key, formattedValue)
                        }else{
                            rowData.put(it.key, '- -')
                        }
                    }
                    newArr.add(rowData)
                }
                def abc=newArr.find { it.city =="total" }
                newArr.remove(abc)
                newArr.sort{it.city}
//                newArr.add(0,abc)
                formattedData.add([total:abc,cat:newArr])
//                rowData1.put(key,newArr)
            }
//                formattedData.add(rowData1);
        }

        return formattedData
    }

    /*public getFormattedData(columns, jsonData){
        def formattedData= []
        jsonData = jsonData.sort{it.value.description}
        jsonData.each{key, eachData->
            def rowData1 =[:]
            def finalData=[];
            if(eachData){
                def newArr=[];
                eachData.each {k,v->
                    def rowData =[:];
                    v.each{
                    if (it.value != null && it.value != ""){
                        def value = it.value
                        def formattedValue
                        if (columns[it.key]?.type == FieldContentType.Amount || columns[it.key]?.type == FieldContentType.LargeAmount){
                            formattedValue = helperService.getFormattedCurrency((Double)value)
                        } else if(columns[it.key]?.type == FieldContentType.Number) {
                            formattedValue = helperService.getFormattedDoubleValue((Double)value)
                        } else {
                            formattedValue = value
                        }
                        rowData.put(it.key, formattedValue)
                    }else{
                        rowData.put(it.key, '- -')
                    }
                    }

                        newArr.add(rowData)

                }
                def abc=newArr.find { it.city =="total" }
                newArr.remove(abc)
                newArr.sort{it.city}
                newArr.add(0,abc)
                rowData1.put(key,newArr)
                finalData=newArr;
            }

            finalData.each {
                formattedData.add(it);
            }

//            formattedData.add(rowData1)
        }
        return formattedData.sort{it.state}
    }*/
}
