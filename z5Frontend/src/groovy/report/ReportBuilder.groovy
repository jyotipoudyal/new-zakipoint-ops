package report



abstract class ReportBuilder<T extends ReportGenerator> {
	protected Map dynamicReportParams=[:];
	protected Map params=[:];
	protected def rawJSON;
	protected def bobJSON;

	public ReportBuilder(Map dynamicReportParams) {
		this.dynamicReportParams = dynamicReportParams
	}

	public ReportBuilder(Map dynamicReportParams,Map params) {
		this.dynamicReportParams = dynamicReportParams
		this.params = params
	}



	public abstract T  build();

	public Map getDynamicReportParams() {
		return dynamicReportParams
	}

	public Map getParams() {
		return params
	}

	public def getRawJSON() {
		return rawJSON
	}

	public def getBobJSON() {
		return bobJSON
	}
}
