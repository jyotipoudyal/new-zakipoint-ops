package report

/**
 * Created by nisharma on 11/22/16.
 */


import grails.util.Holders
import groovy.util.logging.Log4j

@Log4j
public abstract class AbstractReportGenerator implements ReportGenerator{
    protected String reportId;
    protected String backendReportId;
    protected String reportName;
    protected String description;
    protected List processedData;
    protected boolean isReady = false;
    private boolean attempted = false
    protected String root;
    protected def frontEndService
    protected def fetchService;
    protected def populationRiskService;
    protected def reportFetchService;
    protected def helperService;


    public AbstractReportGenerator(){
        this.fetchService = Holders.getGrailsApplication().getMainContext().getBean("fetchService")
        this.frontEndService = Holders.getGrailsApplication().getMainContext().getBean("frontEndService")
        this.populationRiskService = Holders.getGrailsApplication().getMainContext().getBean("populationRiskService")
        this.reportFetchService = Holders.getGrailsApplication().getMainContext().getBean("reportFetchService")
        this.helperService = Holders.getGrailsApplication().getMainContext().getBean("helperService")
    }

    protected AbstractReportGenerator(ReportBuilder builder){
        this.fetchService = Holders.getGrailsApplication().getMainContext().getBean("fetchService")
        this.frontEndService = Holders.getGrailsApplication().getMainContext().getBean("frontEndService")
        this.populationRiskService = Holders.getGrailsApplication().getMainContext().getBean("populationRiskService")
        this.reportFetchService = Holders.getGrailsApplication().getMainContext().getBean("reportFetchService")
        this.helperService = Holders.getGrailsApplication().getMainContext().getBean("helperService")
    }


    //Needs to set all the required report fields and set the isReady flag to true
    protected abstract boolean prepare() throws Exception;

    //Insures that prepare is only executed once
    public boolean generate(){
        if (!isReady && !attempted){
            attempted = true;
            prepare();
        }
        return isReady;
    }

    def public getFinalData(){
        if(generate())
            return processedData;
        else
            return null;
    }

    public boolean isReady(){
        return isReady;
    };

    //Reset attempted flag so that report can be generated again
    public void reset(){
        attempted = false;
        isReady = false;
    }


    String getReportId() {
        return reportId
    }

    String getReportName() {
        return reportName
    }


}

