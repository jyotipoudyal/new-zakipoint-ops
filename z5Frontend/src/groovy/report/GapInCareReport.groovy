package report

/**
 * Created by nisharma on 11/22/16.
 */
public class GapInCareReport extends DynamicReportGenerator {

    def gapInCareReimbursement
    def gapInCareBenchmarksColumns = [description:[displayName: "description",type: FieldContentType.Text],
                                     membersInGroup:[displayName: "membersInGroup",type: FieldContentType.Text],
                                     percentMet:[displayName: "percentMet",type: FieldContentType.Number],
                                     notMeeting:[displayName: "notMeeting",type: FieldContentType.Number],
                                     costPmpy:[displayName: "costPmpy",type: FieldContentType.Amount]
    ]
    def newColumns = [
            'description':[displayName: 'Service Category', type: FieldContentType.Text],
            'membersInGroup':[displayName: 'GapInCareReport Cost', type: FieldContentType.Text],
            'percentMet':[displayName: 'Actual Cost', type: FieldContentType.Number],
            'notMeeting':[displayName: 'Difference', type: FieldContentType.Number],
            'costPmpy':[displayName: 'Factor',type: FieldContentType.Amount]
    ]

    protected GapInCareReport(ReportBuilder reportBuilder) {
        super(reportBuilder)

    }


    public static class ReportGeneratorBuilder extends ReportBuilder<GapInCareReport>{
        public ReportGeneratorBuilder(Map dynamicReportParams,Map params) {
            super(dynamicReportParams,params);
        }
        @Override
        public GapInCareReport build() {
            return new GapInCareReport(this);
        }
    }

    @Override
    protected boolean prepare() throws Exception {
        def incomingJsonArray
        def data
        def formattedData
        def jsonData
        long mapStartTime = System.nanoTime();

//        jsonData = reportingFetchService.getCareAnalyticsData(dynamicReportParams,[:], backendReportId,getExtraParameters(),'gapInCareBenchMarks');
        jsonData = populationRiskService.getPopulationRiskScript(dynamicReportParams,"gapInCare")


        if (!jsonData) {
            log.error("No Data Received for "+getClass().getSimpleName());
            throw new Exception("No Data Received")
        }
//        data = JSON.parse(jsonData)
        data =jsonData

        formattedData = getFormattedData(gapInCareBenchmarksColumns, data)

        processedData = formattedData;
        isReady = (processedData && processedData.size() >0);
        println("Data MAP Time : ${((System.nanoTime() - mapStartTime) / 1000000000)} seconds.")
        return isReady;
    }


    protected String getExtraParameters(){
        gapInCareReimbursement = params.gapInCareReimbursement ? params.gapInCareReimbursement:'gapInCareReimbursementAmount100'

        String parameters="&gapInCareReimbursement=${gapInCareReimbursement}"

        return parameters
    }

    public getFormattedData(columns, jsonData){
        def formattedData= []
        jsonData = jsonData.sort{it.value.description}
        jsonData.each{key, eachData->
            def rowData =[:]
            if(eachData){
                eachData.each {
                    if (it.value != null && it.value != ""){
                        def value = it.value
                        def formattedValue
                        if(it.key =='actualCost'){
                            rowData.putAt("${it.key}_drill",getDrillParams(key))
                        }
                        if (columns[it.key]?.type == FieldContentType.Amount || columns[it.key]?.type == FieldContentType.LargeAmount){
                            formattedValue = helperService.getFormattedCurrency((Double)value)
                        } else if(columns[it.key]?.type == FieldContentType.Number) {
                            formattedValue = helperService.getFormattedDoubleValue((Double)value)
                        } else {
                            formattedValue = value
                        }
                        rowData.put(it.key, formattedValue)
                    }else{
                        rowData.put(it.key, '- -')
                    }
                }
            }
            formattedData.add(rowData)
        }
        return formattedData
    }

    protected def getDrillParams(key){
        def drillMap  = [:]
        def reportingFrom = dynamicReportParams.getAt('reportingFrom').format('MM-dd-yyyy')
        def reportingTo = dynamicReportParams.getAt('reportingTo').format('MM-dd-yyyy')

        if(dynamicReportParams.basis!='PaidDate'){
            drillMap.putAll(['serviceDate':reportingFrom,'serviceDate-to':reportingTo,'comp.serviceDate':'Between'])
        }
        drillMap.putAll(['paidDate':reportingFrom,'paidDate-to':reportingTo,'comp.paidDate':'Between'])
        drillMap.putAll(['gapInCareClaimTypeCode':key,'comp.gapInCareClaimTypeCode':'Equals'])
        /**
         * Add selected LOAs to drill
         */

        return drillMap
    }

}


