package report

/**
 * Created by nisharma on 11/30/16.
 */
public class TopOonProviderSpecialityReport extends DynamicReportGenerator {

    def topOonProviderSpecialityColumns=[
            specialityCode:[displayName: "Speciality Code",type: FieldContentType.Text],
            specialityDesc:[displayName: "Speciality Desc",type: FieldContentType.Text],
            memberCount:[displayName: "Member Count",type: FieldContentType.Number],
            oonProviderCount:[displayName: "Oon Provider Count",type: FieldContentType.Number],
            totalPaidAmount:[displayName: "Total Paid Amount",type: FieldContentType.Amount]

    ]

    protected TopOonProviderSpecialityReport(ReportBuilder reportBuilder){
        super(reportBuilder)
    }


    public static class ReportGeneratorBuilder extends ReportBuilder<TopOonProviderSpecialityReport>{
        public ReportGeneratorBuilder(Map dynamicReportParams,Map params) {
            super(dynamicReportParams,params);
        }
        @Override
        public TopOonProviderSpecialityReport build() {
            return new TopOonProviderSpecialityReport(this);
        }
    }


    @Override
    protected boolean prepare() throws Exception {
        def incomingJsonArray
        def data
        def formattedData
        def jsonData
        long mapStartTime = System.nanoTime();

        jsonData = reportFetchService.getDataRequest(dynamicReportParams,"topOutOfNetworkProviderSpeciality")

        if (!jsonData) {
            log.error("No Data Received for "+getClass().getSimpleName());
            throw new Exception("No Data Received")
        }
        data =jsonData
        formattedData = getFormattedData(topOonProviderSpecialityColumns, data)
        processedData = formattedData;
        isReady = (processedData && processedData.size() >0);
        println("Data MAP Time : ${((System.nanoTime() - mapStartTime) / 1000000000)} seconds.")
        return isReady;
    }

    public getFormattedData(columns, jsonData){
        def formattedData= []
        jsonData = jsonData.sort{it.value.description}
        jsonData.each{key, eachData->
            def rowData1 =[:]
            def finalData=[];
            if(eachData){
                def newArr=[];
                eachData.each {k,v->
                    def rowData =[:];
                    v.each{
                        if (it.value != null && it.value != ""){
                            def value = it.value
                            def formattedValue
                            if (columns[it.key]?.type == FieldContentType.Amount || columns[it.key]?.type == FieldContentType.LargeAmount){
                                formattedValue = helperService.getFormattedCurrency((Double)value)
                            } else if(columns[it.key]?.type == FieldContentType.Number) {
                                formattedValue = helperService.getFormattedDoubleValue((Double)value)
                            } else {
                                formattedValue = value
                            }
                            rowData.put(it.key, formattedValue)
                        }else{
                            rowData.put(it.key, '- -')
                        }
                    }
                    newArr.add(rowData)
                }
                def abc=newArr.find { it.specialityDesc =="total" }
                newArr.remove(abc)
                newArr.sort{it.specialityDesc}
//                newArr.add(0,abc)
//                rowData1.put(key,[total:abc,cat:newArr])
                formattedData.add([total:abc,cat:newArr])
//                rowData1.put(key,newArr)
            }
//            formattedData.add(rowData1);
        }
        return formattedData
    }
}
