package report

/**
 * Created by nisharma on 11/28/16.
 */
public class FaqReport extends DynamicReportGenerator {


    def faqColumns = [                gsx$sn:[displayName: "SN",type: FieldContentType.Text],
                                      gsx$question:[displayName: "Question",type: FieldContentType.Text],
                                      gsx$answer:[displayName: "Answer",type: FieldContentType.Text],
                                      gsx$relatedto:[displayName: "Related T0",type: FieldContentType.Text],
                                      gsx$referTo:[displayName: "Refer To",type: FieldContentType.Text]
    ]



    protected FaqReport(ReportBuilder reportBuilder){
        super(reportBuilder)
    }

    public static class ReportGeneratorBuilder extends ReportBuilder<FaqReport>{
        public ReportGeneratorBuilder(Map dynamicReportParams,Map params) {
            super(dynamicReportParams,params);
        }
        @Override
        public FaqReport build() {
            return new FaqReport(this);
        }
    }

    @Override
    protected boolean prepare() throws Exception {
        def incomingJsonArray
        def data
        def formattedData
        def jsonData
        long mapStartTime = System.nanoTime();

//        jsonData = reportingFetchService.getCareAnalyticsData(dynamicReportParams,[:], backendReportId,getExtraParameters(),'highOutNetworkMemberBenchMarks');
        jsonData = fetchService.fetchFaq(dynamicReportParams)

        if (!jsonData) {
            log.error("No Data Received for "+getClass().getSimpleName());
            throw new Exception("No Data Received")
        }

        data =jsonData

        formattedData = getFormattedData(faqColumns, data)

        processedData = formattedData;

        isReady = (processedData && processedData.size() >0);
        println("Data MAP Time : ${((System.nanoTime() - mapStartTime) / 1000000000)} seconds.")
        return isReady;
    }

    public getFormattedData(columns, jsonData){
        def formattedData= []
        jsonData.each{tr->
            def rowData =[:]
            tr.each{key, eachData->
                def value = eachData['$t']

                if (columns[key]){
                    if(key=='gsx$relatedto'){
                        value=value.toString().tokenize(',')
                    }else if (key=='gsx$referto'){
                        value=value.toString().tokenize(',')
                    }
                    rowData.put(key,value)
                }

            }
            formattedData.add(rowData)
        }
        return formattedData
    }
}
