package report

/**
 * Created by nisharma on 11/22/16.
 */

import java.text.Format
import java.text.SimpleDateFormat

public abstract class DynamicReportGenerator extends AbstractReportGenerator{
    protected def dynamicReportParams;
    protected def params;

    protected DynamicReportGenerator(ReportBuilder reportBuilder){
        super(reportBuilder);
        this.dynamicReportParams=reportBuilder.getDynamicReportParams();
        this.params=reportBuilder.getParams();
    }


}

