package report

/**
 * Created by nisharma on 1/18/17.
 */

public class CohortReport extends DynamicReportGenerator {


        def cohortColumns = [pharmacyPmpm:[displayName: "Pharmacy PMPM",type: FieldContentType.Amount],
                             medicalPmpm:[displayName: "Medical PMPM",type: FieldContentType.Amount],
                             changeInPmpm:[displayName: "Total Paid PMPM",type: FieldContentType.Amount],
                             mailOrder:[displayName: "Prescription Count Filled By Mail Order",type: FieldContentType.Number],
                             genericOrder:[displayName: "Prescription Count Filled By Generic Drugs",type: FieldContentType.Number],
                             specialityOrder:[displayName: "Prescription Count Filled By Speciality Order",type: FieldContentType.Number],
                             erVisitper1000:[displayName: "ER Visits per 1000",type: FieldContentType.Number],
                             thirtyDayReadmitper1000:[displayName: "30 Days Re-Admissions per 1000",type: FieldContentType.Number],
                             totalAdmissionsper1000:[displayName: "Total Admissions per 1000",type: FieldContentType.Number],
                             averageLengthOfStay:[displayName: "Average Length Of Stay",type: FieldContentType.Number],
                             formulary:[displayName: "Prescription Filled By Formulary",type: FieldContentType.Number]
        ]

        def totalCounts;


        protected CohortReport(ReportBuilder reportBuilder){
            super(reportBuilder)
        }

        public static class ReportGeneratorBuilder extends ReportBuilder<CohortReport>{
            public ReportGeneratorBuilder(Map dynamicReportParams,Map params) {
                super(dynamicReportParams,params);
            }
            @Override
            public CohortReport build() {
                return new CohortReport(this);
            }
        }

        @Override
        protected boolean prepare() throws Exception {
            def incomingJsonArray
            def data
            def formattedData
            def jsonData
            long mapStartTime = System.nanoTime();

//        jsonData = reportingFetchService.getCareAnalyticsData(dynamicReportParams,[:], backendReportId,getExtraParameters(),'highOutNetworkMemberBenchMarks');
            jsonData = reportFetchService.getDataRequest(dynamicReportParams,"highOutNetworkMember")
            totalCount=jsonData.getAt('metaInfo').getAt("totalMembers")
            jsonData.remove('metaInfo');


            if (!jsonData) {
                log.error("No Data Received for "+getClass().getSimpleName());
                throw new Exception("No Data Received")
            }

            data =jsonData

            formattedData = getFormattedData(highOutNetworkUsageColumns, data)

            processedData = formattedData;

            isReady = (processedData && processedData.size() >0);
            println("Data MAP Time : ${((System.nanoTime() - mapStartTime) / 1000000000)} seconds.")
            return isReady;
        }

        public getFormattedData(columns, jsonData){
            def formattedData= []
            jsonData = jsonData.sort{it.value.description}
            jsonData.each{key, eachData->
                def rowData =[:]
                if(eachData){
                    eachData.each {
                        if (it.value != null && it.value != ""){
                            def value = it.value
                            def formattedValue
                            if (columns[it.key]?.type == FieldContentType.Amount || columns[it.key]?.type == FieldContentType.LargeAmount){
                                formattedValue = helperService.getFormattedCurrency((Double)value)
                            } else if(columns[it.key]?.type == FieldContentType.Number) {
                                formattedValue = helperService.getFormattedDoubleValue((Double)value)
                            } else {
                                formattedValue = value
                            }
                            rowData.put(it.key, formattedValue)
                        }else{
                            rowData.put(it.key, '- -')
                        }
                    }
                }
                formattedData.add(rowData)
            }
            return formattedData
        }
    }

