package report

/**
 * Created by nisharma on 11/28/16.
 */
public class HighOutNetworkUsageReport extends DynamicReportGenerator {


    def highOutNetworkUsageColumns = [memberId:[displayName: "Member Id",type: FieldContentType.Text],
                                                medicalPaid:[displayName: "Medical Paid",type: FieldContentType.Amount],
                                                medicalOutNetworkPaid:[displayName: "Out of Network Paid",type: FieldContentType.Amount],
    ]

    def totalCount;


    protected HighOutNetworkUsageReport(ReportBuilder reportBuilder){
        super(reportBuilder)
    }

    public static class ReportGeneratorBuilder extends ReportBuilder<HighOutNetworkUsageReport>{
        public ReportGeneratorBuilder(Map dynamicReportParams,Map params) {
            super(dynamicReportParams,params);
        }
        @Override
        public HighOutNetworkUsageReport build() {
            return new HighOutNetworkUsageReport(this);
        }
    }

    @Override
    protected boolean prepare() throws Exception {
        def incomingJsonArray
        def data
        def formattedData
        def jsonData
        long mapStartTime = System.nanoTime();

//        jsonData = reportingFetchService.getCareAnalyticsData(dynamicReportParams,[:], backendReportId,getExtraParameters(),'highOutNetworkMemberBenchMarks');
        jsonData = reportFetchService.getDataRequest(dynamicReportParams,"highOutNetworkMember")
        totalCount=jsonData.getAt('metaInfo').getAt("totalMembers")
        jsonData.remove('metaInfo');


        if (!jsonData) {
            log.error("No Data Received for "+getClass().getSimpleName());
            throw new Exception("No Data Received")
        }

        data =jsonData

        formattedData = getFormattedData(highOutNetworkUsageColumns, data)

        processedData = formattedData;

        isReady = (processedData && processedData.size() >0);
        println("Data MAP Time : ${((System.nanoTime() - mapStartTime) / 1000000000)} seconds.")
        return isReady;
    }

    public getFormattedData(columns, jsonData){
        def formattedData= []
        jsonData = jsonData.sort{it.value.description}
        jsonData.each{key, eachData->
            def rowData =[:]
            if(eachData){
                eachData.each {
                    if (it.value != null && it.value != ""){
                        def value = it.value
                        def formattedValue
                        if (columns[it.key]?.type == FieldContentType.Amount || columns[it.key]?.type == FieldContentType.LargeAmount){
                            formattedValue = helperService.getFormattedCurrency((Double)value)
                        } else if(columns[it.key]?.type == FieldContentType.Number) {
                            formattedValue = helperService.getFormattedDoubleValue((Double)value)
                        } else {
                            formattedValue = value
                        }
                        rowData.put(it.key, formattedValue)
                    }else{
                        rowData.put(it.key, '- -')
                    }
                }
            }
            formattedData.add(rowData)
        }
        return formattedData
    }
}
