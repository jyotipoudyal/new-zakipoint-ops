package com.configurations;

import org.elasticsearch.common.xcontent.XContentBuilder;
import zakipointCore.listener.AbstractSummaryListener;

import java.io.IOException;

/**
 * Created by sktamang on 10/1/15.
 */
public class Wrapper extends AbstractSummaryListener.Record implements XContentWriter {
    public Wrapper(String id) {
        super(id);
    }

    @Override
    public void toXContent(XContentBuilder builder, Params params) throws IOException {

    }
}
