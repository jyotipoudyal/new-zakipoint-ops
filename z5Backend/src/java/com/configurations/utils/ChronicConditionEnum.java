package com.configurations.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sktamang on 5/10/16.
 */
public class ChronicConditionEnum {

    public static enum ChronicConditionMetrics {
//          metricCode; metricName; metricDescription; metricDisplay; chronicPercentFieldName; hasCondition

        c1001(1001, "chrc.1", "AffectivePsychosis","Affective Psychosis","AffectivePsychosis", "hasAffectivePsychosis"),
        c1002(1002, "chrc.2", "Asthma","Asthma","Asthma", "hasAsthma"),
        c1003(1003, "chrc.3", "AtrialFibrillation","Atrial Fibrillation","AtrialFibrillation", "hasAtrialFibrillation"),
        c1004(1004, "chrc.4", "BloodDisorders","Blood Disorders","BloodDisorders", "hasBloodDisorders"),
        c1005(1005, "chrc.5", "CAD","CAD","CAD", "hasCad"),
        c1006(1006, "chrc.6", "Cancer","Cancer","Cancer", "hasCancer"),
        c1007(1007, "chrc.7", "ChronicPain","Chronic Pain","ChronicPain", "hasChronicPain"),
        c1008(1008, "chrc.8", "CHF","Congestive Heart Failure","CongestiveHeartFailure_CHF","hasCongestiveHeartFailure"),
        c1009(1009, "chrc.9", "COPD","COPD","COPD", "hasCopd"),
        c1010(1010, "chrc.10", "DemyelinatingDiseases","Demyelinating Diseases","DemyelinatingDiseases", "hasDemyelinatingDiseases"),
        c1011(1011, "chrc.11", "Depression","Depression","Depression", "hasDepression"),
        c1012(1012, "chrc.12", "Diabetes","Diabetes","Diabetes", "hasDiabetes"),
        c1013(1013, "chrc.13", "EatingDisorders","Eating Disorders","EatingDisorders", "hasEatingDisorders"),
        c1014(1014, "chrc.14", "CKD","CKD","ESRD", "hasEsrd"),
        c1015(1015, "chrc.15", "HIVAIDS","HIV/AIDS","HIV_AIDS", "hasHivAids"),
        c1016(1016, "chrc.16", "Hyperlipidemia","Hyperlipidemia","Hyperlipidemia", "hasHyperlipidemia"),
        c1017(1017, "chrc.17", "Hypertension","Hypertension","Hypertension", "hasHypertension"),
        c1018(1018, "chrc.18", "ImmuneDisorders","Immune Disorders","ImmuneDisorders", "hasImmuneDisorders"),
        c1019(1019, "chrc.19", "InflammatoryBowelDisease","Inflammatory Bowel Disease","InflammatoryBowelDisease", "hasInflammatoryBowelDisease"),
        c1020(1020, "chrc.20", "LiverDiseases","Liver Diseases","LiverDiseases", "hasLiverDisease"),
        c1021(1021, "chrc.21", "MorbidObesity","Morbid Obesity","MorbidObesity", "hasMorbidObesity"),
        c1022(1022, "chrc.22", "Osteoarthritis","Osteoarthritis","Osteoarthritis", "hasOsteoarthritis"),
        c1023(1023, "chrc.23", "PeripheralVascularDisease","Peripheral Vascular Disease","PeripheralVascularDisease", "hasPeripheralVascularDisease"),
        c1024(1024, "chrc.24", "RheumatoidArthritis","Rheumatoid Arthritis","RheumatoidArthritis", "hasRheumatoidArthritis");

        public Integer getMetricCode() {
            return metricCode;
        }

        public String getMetricName() {
            return metricName;
        }

        public String getMetricDescription() {
            return metricDescription;
        }

        public String getMetricDisplay() {return metricDisplay;}

        public String getChronicPercentFieldName() {
            return chronicPercentFieldName;
        }

        public final Integer metricCode;
        public final String metricName;
        public final String metricDescription;
        public final String metricDisplay;
        public final String chronicPercentFieldName;

        public String getHasCondition() {
            return hasCondition;
        }

        public final String hasCondition;

        ChronicConditionMetrics(int metricCode, String metricName, String metricDescription,String metricDisplay,String chronicPercentFieldName,String hasCondition) {
            this.metricCode = metricCode;
            this.metricName = metricName;
            this.metricDescription = metricDescription;
            this.metricDisplay = metricDisplay;
            this.chronicPercentFieldName = chronicPercentFieldName;
            this.hasCondition=hasCondition;
        }


        public static ChronicConditionMetrics getMetricByCode(Integer metricCode) {
            for (ChronicConditionMetrics chronicConditionMetrics : ChronicConditionMetrics.values()) {
                if (chronicConditionMetrics.getMetricCode().equals(metricCode)) {
                    return chronicConditionMetrics;
                }
            }
            throw new IllegalArgumentException("No such metric code exists.");
        }

        public static ChronicConditionMetrics getMetricByMetricDesc(String metricDescription) {
            for (ChronicConditionMetrics chronicConditionMetrics : ChronicConditionMetrics.values()) {
                if (chronicConditionMetrics.getMetricDescription().equals(metricDescription)) {
                    return chronicConditionMetrics;
                }
            }
            throw new IllegalArgumentException("No such metric code exists.");
        }

        public static List<Integer> getAllMetricCode() {
            List<Integer> allMetricCode = new ArrayList<Integer>();

            for (ChronicConditionMetrics chronicConditionMetrics : ChronicConditionMetrics.values()) {

                allMetricCode.add(chronicConditionMetrics.getMetricCode());
            }
            return allMetricCode;
        }

        public static List<String> getAllMetricName() {

            List<String> allMetricName = new ArrayList<String>();

            for (ChronicConditionMetrics chronicConditionMetrics : ChronicConditionMetrics.values()) {
                allMetricName.add(chronicConditionMetrics.getMetricName());
            }
            return allMetricName;
        }

        public static List<String> getAllEnumConstants() {
            List<String> allEnumConstants = new ArrayList<String>();

            String cForChronic = "c";
            for (int count = 1001; count <= 1024; count++) {
                allEnumConstants.add(cForChronic + count);
            }

            return allEnumConstants;
        }


        public static Integer getMetricCodeByMetricName(String metricName) {

            for (ChronicConditionMetrics chronicConditionMetrics : ChronicConditionMetrics.values()) {
                if (chronicConditionMetrics.getMetricCode().equals(metricName)) {
                    return chronicConditionMetrics.getMetricCode();
                }
            }
            throw new IllegalArgumentException("No such metric code exists.");
        }

        public static ChronicConditionMetrics getMetricByHasCondition(String hasCondition) {

            for (ChronicConditionMetrics chronicConditionMetrics : ChronicConditionMetrics.values()) {
                if (chronicConditionMetrics.getHasCondition().equals(hasCondition)) {
                    return chronicConditionMetrics;
                }
            }
            throw new IllegalArgumentException("No such metric code exists.");
        }

        public static Integer getMetricCodeByMetricDesc(String metricDesc) {

            for (ChronicConditionMetrics chronicConditionMetrics : ChronicConditionMetrics.values()) {
                if (chronicConditionMetrics.getMetricDescription().equals(metricDesc)) {
                    return chronicConditionMetrics.getMetricCode();
                }
            }
            throw new IllegalArgumentException("No such metric desc exists.");
        }
    }
}
