package com.configurations.utils;

public class NotQuery extends Query.AbstractBooleanQuery{

    @Override
    public boolean evaluate(Object obj) {
        if (queries.size() > 1) {
            throw new IllegalStateException("A not query can only have single query");
        }
        return ! queries.get(0).evaluate(obj);

    }

    @Override
    public String toString() {
        String query = queries.get(0).toString();
        if(query!=null)return "not (" +query + ")";
        else return "not";
        // return "not";
    }
}
