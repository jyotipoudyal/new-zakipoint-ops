package com.configurations.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public interface Query {
    public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    void add(Query query);
    boolean evaluate(Object obj);

    Query getReducedQuery();

    class AbstractQuery implements Query{

        public void add(Query query) {
            throw new UnsupportedOperationException();
        }

        public boolean evaluate(Object obj) {
            throw new UnsupportedOperationException();
        }

        public Query getReducedQuery() {
            throw new UnsupportedOperationException();
        }

    }
    class AbstractBooleanQuery extends AbstractQuery{
        protected List<Query> queries = new ArrayList<Query>();

        @Override
        public void add(Query query) {
            if(query!=null)
                queries.add(query);
        }

        @Override
        public Query getReducedQuery(){
           /* if(queries.size()==1)
                return queries.get(0);
            else*/ if(queries.size()==0) {
                return null;
            }
            else
                return this;
        }
    }
}
