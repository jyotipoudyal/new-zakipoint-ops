package com.configurations.utils;

import com.configurations.config.*;
import com.configurations.filters.ReportFilters;
import com.configurations.parser.AbstractQueryParser;
import com.configurations.parser.JsonQueryParser;
import com.configurations.search.MemberSearchRequest;
import grails.converters.JSON;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.DateUtils;
import zakipointCore.commonUtils.SpringBeanUtils;
import zakipointCore.drill.filters.ErDrillFilter;

public class MemberSearchQueryBuilder  {

    private MemberSearchRequest searchRequest;
    private AbstractQueryParser queryParser;
    private SearchRequestBuilder builder;
    private HashMap<String, String> requestParameters;
    private String tableCodes;


    public MemberSearchQueryBuilder(MemberSearchRequest request) {
        this.searchRequest = request;
        requestParameters = searchRequest.requestParameters;

        prepareBuilder();
        this.tableCodes = requestParameters.get("table");
        this.queryParser = new JsonQueryParser(tableCodes);
    }

    public MemberSearchQueryBuilder setParser(AbstractQueryParser queryParser) {
        this.queryParser = queryParser;
        return this;
    }

    public static Client getClient(Map<String,String> requestParameters) {
        String indexData = "";
        if(requestParameters.containsKey("indexData")){
            indexData = String.valueOf(requestParameters.get("indexData"));
        }else{
            indexData = String.valueOf(requestParameters.get("clientId"));
        }
        AbstractConfigurationManager configurationManager = SpringBeanUtils.getBean("configurationManager");
        ClusterConfig clusterConfig =configurationManager.getClusterConfig(ConfigFileName.getFileNameClient(indexData));
        return ElasticSearchConnectionManager.getClient(clusterConfig);
    }

    private MemberSearchQueryBuilder prepareBuilder() {
        Client client = getClient(requestParameters);
        String clientId = requestParameters.get("clientId");
        builder = client.prepareSearch(clientId);
        if(requestParameters.containsKey("queryType")) builder.setSearchType(SearchType.COUNT); // for member count in cohorts
        else builder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        builder.setExplain(false);
        return this;
    }

    public MemberSearchQueryBuilder setTypes() {
        final String[] types = FieldConfigurations.fromTableCode(requestParameters.get("table")).tables();
        return setTypes(types);
    }

    public MemberSearchQueryBuilder setTypes(String... types) {
        builder.setTypes(types);
        return this;
    }

    public MemberSearchQueryBuilder setPagination() {
        int pageNumber = Integer.parseInt(requestParameters.get("page"));
        int pageSize = Integer.parseInt(requestParameters.get("pageSize"));
        int from = (pageNumber - 1) * pageSize;
        return setPagination(from,pageSize);
    }
    public MemberSearchQueryBuilder setPagination(int from,int size) {
        builder.setFrom(from).setSize(size);
        return this;
    }

    public MemberSearchQueryBuilder addFields() {
        List<String> fields=new ArrayList<String>();
//           builder.addFields(FieldConfigurations.fromTableCode(tableCodes).defaultColumns(requestParameters.get("defaultColumns")));
            fields.addAll(Arrays.asList(FieldConfigurations.fromTableCode(tableCodes).defaultColumns(requestParameters.get("defaultColumns"))));
            fields.addAll(MaraConfigurationUtils.riskScoreFieldsRawGroup);
        return addFields(fields);
    }


    public MemberSearchQueryBuilder addFields(List<String> fields){
        builder.setFetchSource(fields.toArray(new String[]{}),null);
        return this;
    }


    public MemberSearchQueryBuilder addFilter() {
        if(isZakipoint()){

            AndFilterBuilder filterBuilder;
            if(!requestParameters.containsKey("task")){
                filterBuilder = zakipointSpecificFilters();
            }

            else{
                filterBuilder=    CommonQueries.getFilterForReportFromDatabase(searchRequest.requestParameters);
            }

            builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),filterBuilder));
            return this;
        }
        return addFilter(getFilter());
    }


    private AndFilterBuilder zakipointSpecificFilters() {
        AndFilterBuilder filterBuilder=new AndFilterBuilder();
        filterBuilder.add(getFilter());
        zakiPointSpecificExplorePopFilter(filterBuilder);
        return filterBuilder;
    }


    private void zakiPointSpecificExplorePopFilter(AndFilterBuilder filterBuilder) {

        if(CommonQueries.containsRange(searchRequest.requestParameters)) {
            List<String> memberList = new ArrayList<>();
            int range = Integer.valueOf(searchRequest.requestParameters.get("range"));
            String includeExclude=searchRequest.requestParameters.get("isExclude");
            boolean isExclude=false;
            if(includeExclude.equalsIgnoreCase("true")) isExclude=true;

            if (range > 0 && isExclude) {
                SearchRequestBuilder requestBuilder = new MemberSearchQueryBuilder(searchRequest).build().setSize(0);
                requestBuilder.setTypes(MemberSearch.NAME);

                AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
                andFilterBuilder = ErDrillFilter.caseMemberFilter(searchRequest, andFilterBuilder);
                LevelUtilsMemberSearchRequest.setLevelFilter(searchRequest, andFilterBuilder, searchRequest.requestParameters.get("reportingTo"));
                FilteredQueryBuilder filteredQuery = QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder);

                requestBuilder.setQuery(filteredQuery);
                requestBuilder.addAggregation(AggregationBuilders.terms("members").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0));
                SearchResponse response = requestBuilder.execute().actionGet();

                Terms terms = response.getAggregations().get("members");
                for (Terms.Bucket bucket : terms.getBuckets()) {
                    memberList.add(bucket.getKey());
                }
                filterBuilder.add(FilterBuilders.termsFilter(MemberSearch.FIELD_INT_MEMBER_ID, memberList));
            }
        }

        CommonQueries.cohortFilter(filterBuilder,searchRequest.requestParameters);
    }


    public MemberSearchQueryBuilder addFilter(FilterBuilder filterBuilder) {
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),filterBuilder));
        return this;
    }


    public FilterBuilder getFilter(){
        FilterBuilder filterBuilder=null;
        final FilterBuilder filter = queryParser.getFilter(searchRequest);
        if (searchRequest.requestParameters.containsKey("report")) {   //for ES drill
            filterBuilder=getReportFilter(builder,searchRequest);
        }
        else {
            filterBuilder=filter;
        }
        return filterBuilder;
    }

    private boolean isZakipoint() {
        if(requestParameters.containsKey("isZakipoint")) return true;
        else return false;
    }


    public FilterBuilder getReportFilter(SearchRequestBuilder builder,MemberSearchRequest searchRequest) {
        return ReportFilters.getInstance(searchRequest.requestParameters.get("report")).createFilter(searchRequest);
    }

    private boolean isCohort() {
        return (searchRequest.hasParameter("cohortId"));

    }

    public MemberSearchQueryBuilder setSortOrder() {
        Map<String,String> latestSortFields = FieldMap.simpleFieldMap; /*new HashMap<String, String>(){{
            put("planId","planId") ;
            put("groupId","groupId") ;
            put("divisionId","divisionId") ;
            put("planTypeId","planTypeId") ;
            put("relationshipId","relationshipId") ;
            put("carrierId","carrierId") ;
            put("biometric_mbr_height","mbr_height") ;
            put("biometric_mbr_weight","mbr_weight") ;
            put("mbr_bmi", "mbr_bmi");
            put("mbr_bp_systolic","mbr_bp_systolic");
            put("mbr_bp_diastolic","mbr_bp_diastolic");
        }}  ;

        //Set<String> latestSortFields = new HashSet<String>(Arrays.asList("planId", "groupId", "divisionId", "planTypeId", "relationshipId","carrierId"));
        String sortString = requestParameters.get("order");

        for (String sortParams : SearchUtils.getListFromArray(sortString)) {
            String[] sort = sortParams.split(":");
            String sortField = sort[0];
            if (tableCodes.equals("phr"))
                sortField = sort[0];
            else if (latestSortFields.containsKey(sortField))
                sortField = latestSortFields.get(sortField);
            else{
                FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCodes).fromDisplayName(sort[0]);
                if (fieldConfig == null){
                    continue;
                }else{
                    sortField = fieldConfig.field;
                }
            }
            SortOrder sortOrder = SortOrder.valueOf(sort[1].toUpperCase());
            if (!sortDisabledFieldsForCCEAndPharmacyReport.contains(sortField)) {

                FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCodes).fromDisplayName(sort[0]);
                if (fieldConfig.fieldGroup.equalsIgnoreCase("nested") && requestParameters.containsKey("query")) {
                    builder.addSort(addNestedSortFilter(getFilter(requestParameters.get("query"), sort[0], fieldConfig.path, sortField), sortField, sortOrder, fieldConfig));
                }else{
                    builder.addSort(sortField, sortOrder);
                }

                /*FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCodes).fromDisplayName(sort[0]);
                if (fieldConfig.fieldGroup.equalsIgnoreCase("nested") && requestParameters.containsKey("query")) {
                    Map<String,String> nestedParams =  getParsedJson(requestParameters.get("query"),sort[0]);
                    addNestedSortFilter(nestedParams, sortField, sortOrder, fieldConfig);
                } else*/

                // sorting in the simple fields only.
//            }
//        }
        return this;
    }

    private FilterBuilder getFilter(Object jsonQuery, String sortField, String path, String field) {
        JSONObject jsonObject = new JSONObject(jsonQuery.toString());
        JSONArray innerJson = (JSONArray) JSON.parse(jsonObject.getString("and"));

        for (Object json : innerJson) {
            JSONObject query = (JSONObject) json;

            if (query.containsKey(sortField + ".eq")) {
                return FilterBuilders.termsFilter(field, SearchUtils.getListFromArray(query.get(sortField + ".eq").toString()));
            }
            if (query.containsKey(sortField + ".lte")) {
                return FilterBuilders.rangeFilter(field).lte(getField(query.get(sortField + ".lte").toString()));
            }
            if (query.containsKey(sortField + ".gte")) {
                return FilterBuilders.rangeFilter(field).gte(getField(query.get(sortField + ".gte").toString()));
            }
            if (query.containsKey(sortField + ".between")) {
                List<String> list = SearchUtils.getListFromArray((String) query.get(sortField + ".between"));
                return FilterBuilders.rangeFilter(field).lte(getField(list.get(1))).gte(getField(list.get(0)));
            }
        }
        return null;
    }

    private Object getField(String field){
        try {
            new SimpleDateFormat("yyyy-MM-dd").parse(field);
            return DateUtils.getTimeFromDateWrtTimeZone(field);
        } catch (ParseException e) {
            return field;
        }
    }

    private FieldSortBuilder addNestedSortFilter(FilterBuilder nestedFilter, String sortField, SortOrder sortOrder, FieldConfig fieldConfig) {
        FieldSortBuilder sortBuilder = new FieldSortBuilder(sortField).order(sortOrder).setNestedFilter(nestedFilter).setNestedPath(fieldConfig.path);
        if(fieldConfig.type.equalsIgnoreCase("double") || fieldConfig.type.equalsIgnoreCase("float") || fieldConfig.type.equalsIgnoreCase("int") || fieldConfig.type.equalsIgnoreCase("long")){
            sortBuilder.sortMode("sum");
        }
        return sortBuilder;
    }

    public SearchRequestBuilder build() {
        return builder;
    }

    private Map<String,String> getParsedJson(Object jsonString, String sortField) {

        Map<String, String> nestedParams = new HashMap<String, String>();

        JSONObject jsonObject = new JSONObject(jsonString.toString());
        JSONArray innerJson = (JSONArray)JSON.parse(jsonObject.getString("and"));

        for (int i=0;i< innerJson.size();i++) {
            JSONObject query = (JSONObject)innerJson.get(i);
            if(getNestedParams(sortField, query)!=null) nestedParams.put("id", getNestedParams(sortField, query));
            if(getNestedParams(sortField + "FromDate", query)!=null)  nestedParams.put("fromDate", getNestedParams(sortField + "FromDate", query));
            if(getNestedParams(sortField + "ToDate", query)!=null) nestedParams.put("toDate", getNestedParams(sortField + "ToDate", query));
        }
        return nestedParams;

    }

    private String getNestedParams(String s, JSONObject query) {
        if (query.size() > 0 && query!=null) {
            if(s.contains("program_type") || s.contains("program_name") || s.contains("program_code")) {
                if(s.contains("FromDate")) s = "participationStartDate";
                if(s.contains("ToDate")) s = "participationEndDate";
            }

            if(query.containsKey(s +".eq")) return (String)query.get(s + ".eq");
            if(query.containsKey(s +".lte")) return (String)query.get(s + ".lte");
            if(query.containsKey(s +".gte")) return (String)query.get(s + ".gte");
        }
        return null;
    }
    private FilterBuilder getNestedFilterForSort(Map nestedParams , FieldConfig fieldConfig) {

        AndFilterBuilder andFilter = new AndFilterBuilder();
        RangeFilterBuilder rangeFromFilter = FilterBuilders.rangeFilter(fieldConfig.path + ".fromDate").gte(nestedParams.get("fromDate"));
        RangeFilterBuilder rangeToFilter = FilterBuilders.rangeFilter(fieldConfig.path + ".toDate").lte(nestedParams.get("toDate"));
        return andFilter.add(rangeFromFilter).add(rangeToFilter);
    }

}
