package com.configurations.utils;

import com.configurations.search.MemberSearchRequest;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;

public class MemberSearchQueryExecutor {
    private MemberSearchRequest searchRequest;
    private SearchRequestBuilder builder;
    private ResponseSummary responseSummary = new ResponseSummary();
    private SearchResponse response;

    public MemberSearchQueryExecutor( SearchRequestBuilder builder,MemberSearchRequest searchRequest) {
        this.searchRequest = searchRequest;
        this.builder = builder;
    }

    public JSONObject executeSearch() {
        responseSummary.startQueryTimer();
        response = (SearchResponse) BuilderAction.getResponse(builder, "memberSearch");
        responseSummary.endQueryTimer();
        responseSummary.totalCounts = response.getHits().getTotalHits();
        return renderResults();
    }

    private JSONObject renderResults() {
        if(searchRequest.requestParameters.containsKey("filteredQuery"))
            searchRequest.requestParameters.put("query",searchRequest.requestParameters.get("filteredQuery"));
        responseSummary.startRenderingTimer();
        String tableCode = searchRequest.requestParameters.get("table");
        SearchHit[] queryHits = response.getHits().getHits();
        final JSONObject returnJSON = ResponseRenderers.getInstance(tableCode).renderResults(queryHits, searchRequest);

        JSONObject finalJSON;
        if(searchRequest.hasParameter("report") || searchRequest.hasParameter("reportFormatter")){
            String report = searchRequest.hasParameter("report") ? searchRequest.get("report") :
                    searchRequest.get("reportFormatter");
            finalJSON = ResponseRenderers.getFormatter(report).format(returnJSON, searchRequest.requestParameters);
        }else{
            finalJSON=returnJSON;
        }
        responseSummary.endRenderingTimer();
        return finalJSON;
    }


    public JSONObject getSummaryJson() {
        if (searchRequest.printQuery)
            responseSummary.queryString = builder.toString();
        return responseSummary.getJSON();
    }


}
