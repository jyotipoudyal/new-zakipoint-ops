package com.configurations.utils;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonKeyReplacer {
    public final static List<String> COMPARATOR=new ArrayList<String>(){{
        add("and");
        add("or");
        add("not");
    }} ;
    String oldKey;
    String newKey;

    public JsonKeyReplacer(String oldKey, String newKey) {
        this.oldKey = oldKey;
        this.newKey = newKey;
    }

    public Object replaceQuery(Object obj) {
        JSONObject jsonQuery = (JSONObject) obj;
        return processQuery(jsonQuery);
    }


    public Object processQuery(Object obj) {
        if (obj instanceof JSONObject) {
            return processMap((JSONObject) obj);
        } else if (obj instanceof JSONArray) {
            return processList((JSONArray) obj);
        } else {
            return obj;
        }
    }

    private JSONArray processList(JSONArray list) {
        final JSONArray newList = new JSONArray();
        for (Object o : list) {
            Object listRet = processQuery(o);
            if (listRet != null) {
                newList.add(listRet);
            }
        }
        return newList;
    }
    private JSONObject processMap(JSONObject map) {
        JSONObject newMap = new JSONObject();
        for (Object s : map.keySet()) {
            String key = (String) s;
            Object val;
            val = processQuery(map.get(key));
            String[] field = getField(key);
            if (field[0].equalsIgnoreCase(oldKey)){
                key = newKey +"."+ field[1];
            }
            newMap.put(key, val);
        }
        return newMap.length() > 0 ? newMap : null;
    }

    private static String[] getField(String key) {
        return key.split("\\.");
    }
}
