package com.configurations.utils;

public class VisitAdmission {
    public static final String NAME="VisitAdmission";
    public static final String TOTAL_ADMISSIONS="totalAdmissions";
    public static final String INT_MEMBER_ID ="intMemberId";
    public static final String TOTAL_PLAN_AND_MEMBER_PAID="totalPlanAndMemberPaid";


    public static final String PAID_AMOUNT="paidAmount";
    public static final String TOTAL_AMOUNT="totalAmount";
    public static String THIRD_DAY_READMIT = "thirtyDayReadmit";
    public static String AVOIDABLE_FLAG = "avoidableFlag";
    public static String ER_VISIT = "erVisit";
    public static final String REASON_ERVISIT = "reasonOfErVisit";
    public static final String SUPER_DIAG_GROUPER = "diag1SupergrouperId";
    public static final String SUPER_DIAG_GROUPER_DESC = "diag1Supergrouperdesc";
    public static final String IN_PATIENT_DAYS = "inpatientDays";


    public static class VisitAmounts {
        public static final String NAME = "visitAmounts";
        public static final String DOT = ".";
        public static final String TOTAL_PAID= NAME + DOT +"totalPaid";
    }
}
