package com.configurations.utils;

import com.configurations.config.FieldConfig;
import grails.converters.JSON;
import org.codehaus.groovy.grails.web.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

class DataConvertionUtils {
    public enum ConversionType {
        ES_TO_DISPLAY,
        DISPLAY_TO_ES,
    }

    public static Object getValue(FieldConfig fieldConfig, Object value, ConversionType conversionType) {
        String type = fieldConfig != null ? fieldConfig.type : "";
        if (type.equalsIgnoreCase("Date") || type.equalsIgnoreCase("DateTime")) {
            return getDate(value, type);
        }
        if (type.equalsIgnoreCase("double") || type.equalsIgnoreCase("float")) {
            if (conversionType == ConversionType.ES_TO_DISPLAY) {
                return DataTypeUtils.getFloatForLong(value);
            } else {
                if (SearchUtils.isArray(value.toString())) {
                    List<String> amtList = SearchUtils.getListFromArray(value.toString());
                    List<Object> convertedAmtList = new ArrayList<Object>();
                    for (String amt : amtList) {
                        convertedAmtList.add(DataTypeUtils.getConvertedFloat(Double.valueOf(amt.toString())));
                    }
                    return convertedAmtList;
                } else
                    return DataTypeUtils.getConvertedFloat(Double.valueOf(value.toString()));
            }
        }
        if (type.equalsIgnoreCase("String")) {
            if (value == null) return null;
            if (value instanceof String) {
                if (((String) value).isEmpty()) return null;
            }
            return value;
        }
        if (type.equalsIgnoreCase("MonthYear")) {
            if (conversionType.equals(ConversionType.DISPLAY_TO_ES))
                return DateConversionUtility.getProperMonthYearWrtTimeZone(value.toString(), 15);

            return getMonthYear(value);
        }
        return value;
    }

    private static Object getDate(Object value, String type) {
        Long val;
        if (value instanceof Long)
            val = (Long) value;
        else if (value instanceof Integer)
            val = ((Integer) value).longValue();
        else return convertDate(value.toString());


        Date d = DateConversionUtility.getProperDateWrtTimeZone(val);
        if (type.equalsIgnoreCase("Date"))
            return DateConversionUtility.getDateInSQLFormat(d);
        else
            return DateConversionUtility.getDateTimeInSQLFormat(d); //for datetime type

    }

    public static String convertDate(String value) {
        if (SearchUtils.isArray(value)) {
            List<String> newDateList = new ArrayList<String>();
            List<String> dateList = SearchUtils.getListFromArray(value);
            for (String date : dateList) {
                newDateList.add(DateConversionUtility.convertUsToSqlFormat(date));
            }
            return newDateList.toString();
        } else {
            return DateConversionUtility.convertUsToSqlFormat(value);
        }
    }

    private static Object getMonthYear(Object value) {
        Long val;
        if (value instanceof Long) {
            val = (Long) value;
        } else if(value instanceof Integer){
            val = ((Integer) value).longValue();
        }
        else{
            try{
                SimpleDateFormat stringFormat=new SimpleDateFormat("yyyy-MM-dd"); //required only for test data where we store strings as source
                val=stringFormat.parse(value.toString()).getTime();
            }catch(ParseException ex){val=Long.MIN_VALUE;}
        }
        SimpleDateFormat format = new SimpleDateFormat("MMMMM yyyy");
        return format.format(DateConversionUtility.getProperDateWrtTimeZone(val));
    }

    //converts simple date format to long compatible for ES Query
    private static Object convertToLongFromDate(Object value) {
        if (SearchUtils.isArray(value.toString())) {
            JSONArray array = (JSONArray) JSON.parse(value.toString());
            JSONArray returnArray = new JSONArray();
            for (Object val : array) {
                Long date = DateConversionUtility.getTimeFromDateWrtTimeZone(val.toString());
                returnArray.put(date);
            }
            return returnArray;
        } else return DateConversionUtility.getTimeFromDateWrtTimeZone(value.toString());
    }

    public static Integer stringToInteger(String value,Integer defaultValue){
        Integer result=defaultValue;
        try{
            result=Integer.valueOf(value);
        }catch (NumberFormatException e){
            e.printStackTrace();
        }
        return result;
    }

    public static <K, V extends Comparable<V>> Map<K, V> descendingSortByMapValues(final Map<K, V> map, boolean descending) {
        Comparator<K> valueComparator =  new Comparator<K>() {
            public int compare(K k1, K k2) {
                int compare = map.get(k2).compareTo(map.get(k1));
                if (compare == 0) return 1;
                else return compare;
            }
        };
        Map<K, V> sortedByValues;
        if(descending)
            sortedByValues = new TreeMap<>(valueComparator);
        else
            sortedByValues = new TreeMap<>(valueComparator.reversed());
        sortedByValues.putAll(map);
        return sortedByValues;
    }
}
