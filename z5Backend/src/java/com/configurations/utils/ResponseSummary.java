package com.configurations.utils;

import grails.converters.JSON;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class ResponseSummary {
    public long totalCounts = 0;
    public double responseRenderingTime = 0l;
    public double queryTime = 0;
    private Date startTime;
    private long queryStartTime;
    private Date renderStartTime;

    public String queryString = null;

    public JSONObject getJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("totalCounts", totalCounts);
        jsonObject.put("renderingTime", responseRenderingTime);
        jsonObject.put("queryTime", queryTime);
        if (queryString != null) {
            jsonObject.put("query", JSON.parse(queryString));
        }
        return jsonObject;
    }

    public void startQueryTimer() {
        queryStartTime = System.nanoTime();
    }

    public void endQueryTimer() {
        queryTime = (System.nanoTime() - queryStartTime) / 1000000000.0d;
    }

    public void startTimer() {
        startTime = Calendar.getInstance().getTime();
    }

    public void endTimer() {
        responseRenderingTime = (Calendar.getInstance().getTimeInMillis() - startTime.getTime()) / 1000.0d;
    }

    public void startRenderingTimer() {
        endQueryTimer();//rendering started, so stop queryTimer
        renderStartTime = Calendar.getInstance().getTime();
    }

    public void endRenderingTimer() {
        responseRenderingTime = (Calendar.getInstance().getTimeInMillis() - renderStartTime.getTime()) / 1000.0d;
    }

}
