package com.configurations.utils;

public class OrQuery extends Query.AbstractBooleanQuery{

    @Override
    public boolean evaluate(Object obj){
        for (Query query : queries) {
            if(query.evaluate(obj)==true)
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        String andQuery = "(";
        int count = 0;
        for (Query query : queries) {
            if(count==0)
                andQuery = andQuery + query.toString();
            else
                andQuery = andQuery + " or " + query.toString();
            count++;
        }
        andQuery = andQuery + ")";
        return andQuery;
    }
}
