package com.configurations.utils;

import com.configurations.config.FieldConfig;
import com.configurations.config.FieldConfigurations;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class QueryKeyReplacer {
 List<String> fieldList = new ArrayList<String>();
private String clientId;
    private String tableCode;


    public QueryKeyReplacer(String clientId, String tableCode) {
        this.clientId = clientId;
        this.tableCode = tableCode;
        this.fieldList = MaraConfigurationUtils.riskScoreFieldsDefault;
    }

    public Object replaceQuery(Object obj) {
        JSONObject jsonQuery = (JSONObject) obj;
        return processQuery(jsonQuery);
    }


    public Object processQuery(Object obj) {
        if (obj instanceof JSONObject) {
            return processMap((JSONObject) obj);
        } else if (obj instanceof JSONArray) {
            return processList((JSONArray) obj);
        } else {
            return obj;
        }
    }

    private JSONArray processList(JSONArray list) {
        final JSONArray newList = new JSONArray();
        for (Object o : list) {
            Object listRet = processQuery(o);
            if (listRet != null) {
                newList.add(listRet);
            }
        }
        return newList;
    }

    private JSONObject processMap(JSONObject map) {
        JSONObject newMap = new JSONObject();
        for (Object s : map.keySet()) {
            String key = (String) s;
            Object val;
            val = processQuery(map.get(key));
            String[] field = getField(key);
            String actualField = field[0];
            if(!JsonKeyReplacer.COMPARATOR.contains(field[0]) ) {
                FieldConfig acField = FieldConfigurations.fromTableCode(tableCode).fromDisplayName(field[0]);
                if(acField!=null) actualField = acField.field;
            }
            if (fieldList.contains(actualField)) {
                String maraType = MaraConfigurationUtils.getMaraType(clientId);
                key = (field[0] + (maraType.equals("Bob") ? "" : maraType)) + "." + field[1];
            }
            newMap.put(key, val);
        }
        return newMap.length() > 0 ? newMap : null;
    }

    public static String replaceField(String actualField, String clientId) {
        if (MaraConfigurationUtils.riskScoreFieldsDefault.contains(actualField)) {
            String maraType = MaraConfigurationUtils.getMaraType(clientId);
            maraType = maraType.equals("Bob") ? "" : maraType;
            actualField = MaraConfigurationUtils.maraRiskScoreFields(maraType, actualField);
        }
        return actualField;
    }

    private static String[] getField(String key) {
        return key.split("\\.");
    }
}
