package com.configurations.utils;

import java.util.HashSet;
import java.util.Set;

public class Pharmacy {
    //fields
    public static final String NAME                       = "Pharmacy";
    public static final String PAID_DATE                  = "paidDate";
    public static final String PAID_AMOUNT                  = "paidAmount";
    public static final String PHARMACY_SCRIPT_BRAND_DRUGS= "pharmacyScriptBrandDrugs";
    public static final String PHARMACY_SCRIPTS           = "pharmacyScripts";
    public static final String PHARMACY_SCRIPT_FOR_UM = "pharmacyScriptForUm";
    public static final String PHARMACY_SCRIPTS_MAIL_ORDER= "pharmacyScriptMailOrder";
    public static final String NDC_CODE= "ndcCode";
    public static final String NDC_DESC= "ndcDescription";
    public static final String PARTICIPATION_ARRAY= "participationArray";
    public static final String DRUG_GROUP= "drugGroup";
    public static final String INT_MEMBER_ID= "intMemberId";
    public static final String GENERIC_FLAG= "genericFlag";
    public static final String THERAPY_DESCRIPTION= "therapyDescription";
    public static final String BRAND_NAME= "brandName";

    public static final String DAYS_OF_SUPPLY= "daysOfSupply";
    public static final Set<String> PHARMACY_UM_FIELDS = new HashSet<String>() {{
        add(PHARMACY_SCRIPT_BRAND_DRUGS);
        add(PHARMACY_SCRIPTS);
        add(PHARMACY_SCRIPT_FOR_UM);
        add(PHARMACY_SCRIPTS_MAIL_ORDER);
    }};
}
