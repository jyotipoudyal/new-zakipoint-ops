package com.configurations.utils;

import java.math.BigDecimal;

public class DataTypeUtils {
    public static Double getFloatForLong(Object val) {
        BigDecimal big;
        if (val instanceof Long) big = new BigDecimal((Long) val);
        else if (val instanceof Double) big = new BigDecimal((Double) val);
        else big = new BigDecimal(Integer.valueOf(val.toString()) );
        big.setScale(2, BigDecimal.ROUND_HALF_UP);
        return big.divide(DataConstants.hundred).doubleValue();
    }


    public static Object getConvertedFloat(double fieldValue) {
        BigDecimal bd = new BigDecimal(fieldValue);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        bd = bd.multiply(DataConstants.hundred); //hundred is a BigDecimal constant
        Long value = bd.longValueExact();
        return value;
    }

    public static Integer parseInt(String value) {
        if (value == null) return null;
        char firstChar = value.charAt(0);
        String maxInt = String.valueOf(Integer.MAX_VALUE);
        value=trimZero(value,maxInt);
        if (firstChar == '-') {
            String minInt = String.valueOf(Integer.MIN_VALUE);
            if (value.length()>(minInt.length())) return Integer.MIN_VALUE;
            if((value.length()==minInt.length())&&minInt.compareTo(value) < 0) return Integer.MIN_VALUE;
        } else {
            if (value.length()>maxInt.length()) return Integer.MAX_VALUE;
            if ((value.length()==maxInt.length())&&maxInt.compareTo(value) < 0) return Integer.MAX_VALUE;
        }
        return Integer.parseInt(value);
    }

    private static String trimZero(String value,String minInt){
        int i=value.startsWith("-")?1:0;
        if(value.length()>minInt.length()){
            while(i<value.length()-1){
                if(value.charAt(i)!='0') break;
                i++;
            }
        }
        return (value.startsWith("-")?"-":"") + value.substring(i);
    }
}
