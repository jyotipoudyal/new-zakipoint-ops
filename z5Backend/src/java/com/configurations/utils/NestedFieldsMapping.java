package com.configurations.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NestedFieldsMapping {
    public static final Map<String, List<String>> loaFields = new HashMap<String, List<String>>();

    static {

        loaFields.put("plan", Arrays.asList("planId", "planIdName", "planIdFromDate", "planIdToDate", "planIdEligibilityType"));
        loaFields.put("memberCity", Arrays.asList("memberCityId", "memberCityIdName", "memberCityIdFromDate", "memberCityIdToDate", "memberCityIdEligibilityType"));
        loaFields.put("memberCounty", Arrays.asList("memberCountyId", "memberCountyIdName", "memberCountyIdFromDate", "memberCountyIdToDate", "memberCountyIdEligibilityType"));
        loaFields.put("memberGender", Arrays.asList("memberGenderId", "memberGenderIdName", "memberGenderIdFromDate", "memberGenderIdToDate", "memberGenderIdEligibilityType"));
        loaFields.put("group", Arrays.asList("groupId", "groupIdName", "groupIdFromDate", "groupIdToDate", "groupIdEligibilityType"));
        loaFields.put("carrier", Arrays.asList("carrierId", "carrierIdName", "carrierIdFromDate", "carrierIdToDate", "carrierIdEligibilityType"));
        loaFields.put("planType", Arrays.asList("planTypeId", "planTypeIdName", "planTypeIdFromDate", "planTypeIdToDate", "planTypeIdEligibilityType"));
        loaFields.put("division", Arrays.asList("divisionId", "divisionIdName", "divisionIdFromDate", "divisionIdToDate", "divisionIdEligibilityType"));
        loaFields.put("pcpProvider", Arrays.asList("pcpProviderId", "providerIdName", "pcpProviderIdFromDate", "pcpProviderIdToDate", "pcpProviderIdEligibilityType"));
        loaFields.put("relationship", Arrays.asList("relationshipId", "relationshipIdName", "relationshipIdFromDate", "relationshipIdToDate", "relationshipIdEligibilityType", "memberRelationshipClass"));
        loaFields.put("memberStatus", Arrays.asList("currentStatus", "statusFromDate", "statusToDate", "statusEligibilityType"));
        loaFields.put("coverageTypeDesc", Arrays.asList("coverageTypeDescId", "coverageTypeDescName", "coverageTypeDescIdFromDate", "coverageTypeDescIdToDate"));
        loaFields.put("contractType", Arrays.asList("contractTypeId", "contractTypeName", "contractTypeIdFromDate", "contractTypeIdToDate"));
        loaFields.put("udf16", Arrays.asList("udf16Id", "udf16IdName", "udf16IdFromDate", "udf16IdToDate", "udf16IdEligibilityType"));
        loaFields.put("udf17", Arrays.asList("udf17Id", "udf17IdName", "udf17IdFromDate", "udf17IdToDate", "udf17IdEligibilityType"));
        loaFields.put("udf18", Arrays.asList("udf18Id", "udf18IdName", "udf18IdFromDate", "udf18IdToDate", "udf18IdEligibilityType"));
        loaFields.put("udf19", Arrays.asList("udf19Id", "udf19IdName", "udf19IdFromDate", "udf19IdToDate", "udf19IdEligibilityType"));
        loaFields.put("udf20", Arrays.asList("udf20Id", "udf20IdName", "udf20IdFromDate", "udf20IdToDate", "udf20IdEligibilityType"));
        loaFields.put("udf21", Arrays.asList("udf21Id", "udf21IdName", "udf21IdFromDate", "udf21IdToDate", "udf21IdEligibilityType"));
        loaFields.put("udf22", Arrays.asList("udf22Id", "udf22IdName", "udf22IdFromDate", "udf22IdToDate", "udf22IdEligibilityType"));
        loaFields.put("udf23", Arrays.asList("udf23Id", "udf23IdName", "udf23IdFromDate", "udf23IdToDate", "udf23IdEligibilityType"));
        loaFields.put("udf24", Arrays.asList("udf24Id", "udf24IdName", "udf24IdFromDate", "udf24IdToDate", "udf24IdEligibilityType"));
        loaFields.put("udf25", Arrays.asList("udf25Id", "udf25IdName", "udf25IdFromDate", "udf25IdToDate", "udf25IdEligibilityType"));
        loaFields.put("udf26", Arrays.asList("udf26Id", "udf26IdName", "udf26IdFromDate", "udf26IdToDate", "udf26IdEligibilityType"));
        loaFields.put("udf27", Arrays.asList("udf27Id", "udf27IdName", "udf27IdFromDate", "udf27IdToDate", "udf27IdEligibilityType"));
        loaFields.put("udf28", Arrays.asList("udf28Id", "udf28IdName", "udf28IdFromDate", "udf28IdToDate", "udf28IdEligibilityType"));
        loaFields.put("udf29", Arrays.asList("udf29Id", "udf29IdName", "udf29IdFromDate", "udf29IdToDate", "udf29IdEligibilityType"));
        loaFields.put("udf30", Arrays.asList("udf30Id", "udf30IdName", "udf30IdFromDate", "udf30IdToDate", "udf30IdEligibilityType"));
        loaFields.put("member_amount", Arrays.asList("rxPaidAmount", "medPaidAmount", "serviceMonth", "paidMonth"
                , "totalMemberPaidAmount"));

    }

}
