package com.configurations.utils;

import com.configurations.search.MemberSearchRequest;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.RequestUtils;

import java.util.ArrayList;
import java.util.List;

public class LevelUtilsMemberSearchRequest {
    public static void setLevelFilter(MemberSearchRequest state, AndFilterBuilder andFilter, String month) {
        for (FilterBuilder lvlFilter : getLevelFilters(state, month)) {
            andFilter.add(lvlFilter);
        }
    }

    public static List<FilterBuilder> getLevelFilters(MemberSearchRequest state, String month) {
        List<FilterBuilder> levelFilters = new ArrayList<FilterBuilder>();
        for (String level : getLevels(state)) {
            if (state.requestParameters.containsKey(level)) {
                String[] filterValue = RequestUtils.getArrayRequest(state.requestParameters.get(level));
                levelFilters.add(LevelUtils.getNestedFilter(month, level, filterValue));
            }
        }
        return levelFilters;
    }

    public static List<String> getLevels(MemberSearchRequest request) {
        return LevelUtils.getLevels(request.requestParameters);
    }
}
