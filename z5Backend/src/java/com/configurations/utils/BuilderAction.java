package com.configurations.utils;

import org.apache.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.ElasticsearchTimeoutException;
import org.elasticsearch.action.ActionRequestBuilder;
import org.elasticsearch.action.ActionResponse;

import java.util.concurrent.TimeUnit;

public class BuilderAction {
    private static final Logger logger = Logger.getLogger(BuilderAction.class.getName());

    public static ActionResponse getResponse(ActionRequestBuilder builder, String report) {
        ActionResponse response = null;
        try {
            response = executeBuilder(builder, report);
        } catch (ElasticsearchException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static ActionResponse executeBuilder(ActionRequestBuilder builder, String report) throws ElasticsearchException {
        ActionResponse response;
        if (report.equals("multiSummation") || report.equals("summationByKey"))
            response = exceuteBuilderWithTimeOut(builder, 20, report);
        else {
            response = (ActionResponse) builder.execute().actionGet();
        }
        return response;
    }

    public static ActionResponse exceuteBuilderWithTimeOut(ActionRequestBuilder builder, int timeout, String report) throws ElasticsearchException {
        ActionResponse response;
        try {
            long startTime = System.nanoTime();
            response = (ActionResponse) builder.execute().actionGet(timeout, TimeUnit.SECONDS);
            System.out.println("Time taken for Summation Query Execution: " + ((double) (System.nanoTime() - startTime) / 1000000000) + " Seconds");
        } catch (ElasticsearchTimeoutException e) {
            throw new ElasticsearchTimeoutException("Time Out Exception");
        }
        return response;
    }
}

