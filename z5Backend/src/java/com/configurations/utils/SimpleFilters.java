package com.configurations.utils;


import com.configurations.config.FieldConfig;
import com.configurations.config.FieldConfigurations;
import com.configurations.filters.*;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.index.query.FilterBuilder;
import zakipointCore.commonUtils.RequestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleFilters {static final Map<String, SimpleFilter> filters;

    static {
        filters = new HashMap<String, SimpleFilter>();
        register("eq", TermFilter.INSTANCE);
        register("neq", NotEqualsFilter.INSTANCE);
        register("lt", LessThanFilter.INSTANCE);
        register("lte", LessThanEqualsFilter.INSTANCE);
        register("gt", GreaterThanFilter.INSTANCE);
        register("gte", GreaterThanEqualsFilter.INSTANCE);
        register("between", BetweenFilter.INSTANCE);
        register("contains", ContainsFilter.INSTANCE);
        register("contain", ContainFilter.INSTANCE);
        register("ids", IdsFilter.INSTANCE);
        register("missing", MissingFilter.INSTANCE);
        register("exists", ExistsFilter.INSTANCE);
    }

    public static Filter getFilter(JSONObject jsonObject, String tableCodes, String clientId) {
        if (jsonObject.keySet().size() > 1) {
            throw new IllegalArgumentException("Unexpected key size for jsonObject " + jsonObject);
        }
        String jsonKey = (String) jsonObject.keys().next();
        Object value = jsonObject.get(jsonKey);
        int indexOfDot = jsonKey.lastIndexOf(".");
        String operand = jsonKey.substring(0, indexOfDot);
        String operator = jsonKey.substring(indexOfDot + 1);
        String fieldName;
        FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCodes).fromDisplayName(operand);
        if (fieldConfig != null) {
            fieldName = FieldConfigurations.fromTableCode(tableCodes).fromDisplayName(operand).field;
            if (!operator.equals("missing"))
                value = DataConvertionUtils.getValue(fieldConfig, value, DataConvertionUtils.ConversionType.DISPLAY_TO_ES);
            if (operand.equals("qmMeasure")) {
                value = getQMValue(value);
            }
        } else {
            if (operand.equals("utilization")) {
                fieldName = value.toString();
                operator = "neq";
                value = 0;
            } else
                return null;
        }
        if(fieldConfig != null && fieldConfig.toLowerCase != null)
            value=value.toString().toLowerCase();
        String modifiedFieldname = QueryKeyReplacer.replaceField(fieldName, clientId);

        FilterBuilder filterBuilder = getInstance(operator).getFilter(modifiedFieldname, value, fieldConfig);
        Filter filter = new Filter(filterBuilder);
        if (fieldConfig != null && fieldConfig.path != null) {
            filter.setPath(fieldConfig.path);
        }
        return filter;
    }

    private static Object getQMValue(Object value) {
        String strVal = value.toString();
        if (RequestUtils.isArrayRequest(strVal)) {
            List<Integer> ccMetrics = new ArrayList<Integer>();
            for (String cc : RequestUtils.getArrayRequest(strVal)) {
                cc = cc.trim();
                ccMetrics.add(MetricInfoReader.getMetricInfoReader().getMetricId(cc));
            }
            value = ccMetrics.toArray(new Integer[]{});
        } else {
            value = MetricInfoReader.getMetricInfoReader().getMetricId(value.toString());
        }
        return value;
    }

    public static SimpleFilter getInstance(String operator) {
        if (!filters.containsKey(operator))
            throw new IllegalArgumentException("Operator " + operator + "not registered");

        return filters.get(operator);
    }

    private static void register(String operator, SimpleFilter simpleFilter) {
        filters.put(operator, simpleFilter);
    }

}
