package com.configurations.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 1/7/16.
 */
public class QmUtils {

    public static Map<String, String> QM_METRIC_TO_FULL_FORM = new HashMap<String, String>() {{
        put("Asthma", "Asthma");
        put("COPD", "Chronic Obstructive Pulmonary Disease");
        put("CHF", "Congestive Heart Failure");
        put("CAD", "Coronary Artery Disease");
        put("Diabetes", "Diabetes");
        put("Hyperlipidemia", "Hyperlipidemia");
        put("RA", "Rheumatoid Arthritis");
        put("Hypertension", "Hypertension");

    }};

    public static void init() {
        QM_CONDITION_TO_FULL_INFO_MAP.put("Asthma", ASTHMA_CODE_DESC);
        QM_CONDITION_TO_FULL_INFO_MAP.put("COPD", CHRONIC_OBSTRUCTIVE_PULMONARY);
        QM_CONDITION_TO_FULL_INFO_MAP.put("CHF", CONGESTIVE_HEART_FAILURE);
        QM_CONDITION_TO_FULL_INFO_MAP.put("CAD", CORONARY_ARTERY_DISEASE);
        QM_CONDITION_TO_FULL_INFO_MAP.put("Diabetes", DIABETES);
        QM_CONDITION_TO_FULL_INFO_MAP.put("Hyperlipidemia", HYPERLIPIDEMIA);
        QM_CONDITION_TO_FULL_INFO_MAP.put("RA", RHEUMATOID_ARTHRITIS);
        QM_CONDITION_TO_FULL_INFO_MAP.put("Hypertension", HYPERTENSION);
    }

    public static Map<String, Map<String, String>> QM_CONDITION_TO_FULL_INFO_MAP = new HashMap<String, Map<String, String>>();
    //metricCode __ metric Description __ metric Type (positive or negative __ category __ metricIdentifier
    public static final Map<String, String> QM_CODES_MAP = new HashMap<String, String>() {{
        /* 1=positive metric and it means that numerator= 0 is the care alert population and numerator=[0,1] means group population
        * 0=negative metric and it means that numerator= 1 is the care alert population and numerator=[0,1] means group population
        */
        //Asthma
        put("21", "Return to hospital with same asthma diagnosis within 30 days following inpatient discharge__0__Asthma__asthma.1");
        put("22", "Visit to an ED/Urgent care office for asthma in the past 6 months__0__Asthma__asthma.2");
        put("23", "Asthma and a routine provider visit in the last 12 months__1__Asthma__asthma.3");
        put("24", "Children with asthma-related acute visit in the past two months__0__Asthma__asthma.4");
        put("25", "Members with asthma currently taking a prescription medication for asthma__1__Asthma__asthma.5");
        put("26", "Asthma with pneumococcal vaccination__1__Asthma__asthma.6");
        put("27", "Two or more asthma related ER Visits in the last 6 months__0__Asthma__asthma.7");
        put("28", "Asthma related admit in last 12 months__0__Asthma__asthma.8");
        put("29", "Asthma with influenza vaccination in last 12 months__1__Asthma__asthma.9");
        put("30", "Persistent asthma with annual pulmonary function test__1__Asthma__asthma.10");
        put("31", "Received control inhaler (long acting) in last 12 months__1__Asthma__asthma.11");
        put("32", "Received rescue inhaler (short acting) in last 12 months__1__Asthma__asthma.12");
        put("33", "canister short-acting inhaled beta agonist/months__0__Asthma__asthma.13");

        //Chronic Obstructive Pulmonary Disease
        put("71", "Re-admission to hospital with COPD diagnosis within 30 days following a COPD inpatient stay discharge__0__COPD__COPD.1");
        put("72", "Members with COPD who have had a visit to an ER for COPD related diagnosis in the past 90 days__0__COPD__COPD.2");
        put("73", "Exacerbations in last 12 months__0__COPD__COPD.3");
        put("74", "Individuals over the age of 20 years with diagnosed COPD are on bronchodilator therapy__1__COPD__COPD.4");
        put("75", "Members with COPD who had an annual spirometry test__1__COPD__COPD.5");
        put("76", "Annual flu vaccination__1__COPD__COPD.6");
        put("77", "Exacerbation with inhaled bronchodilator/ corticosteroid last 12 months__1__COPD__COPD.7");


        //Congestive Heart Failure
        put("121", "Heart failure and atrial fibrillation on warfarin therapy__1__CHF__CHF.1");
        put("122", "Heart failure and LVSD on ACE/ARB__1__CHF__CHF.2");
        put("123", "Heart failure and LVSD on beta-blocker__1__CHF__CHF.3");
        put("124", "Re-admission to hospital with Heart Failure diagnosis within 30 days following a HF inpatient stay discharge__0__CHF__CHF.4");
        put("125", "ER Visit for Heart Failure in last 90 days__0__CHF__CHF.5");
        put("126", "Follow-up OV within 4 weeks of discharge from HF admission__1__CHF__CHF.6");

        //Coronary Artery Disease
        put("51", "Annual lipid profile __1__CAD__CAD.1");
        put("52", "On anti-platelet medication __1__CAD__CAD.2");
        put("53", "On lipid lowering medication __1__CAD__CAD.3");
        put("54", "On anti-platelets medication __1__ CAD__CAD.4");

        //Diabetes
        put("91", "Annual dilated eye exam__1__Diabetes__diabetes.1");
        put("92", "Annual foot exam__1__Diabetes__diabetes.2");
        put("93", "Annual HbA1c test done__1__Diabetes__diabetes.3");
        put("94", "Annual lipid profile__1__Diabetes__diabetes.4 ");
        put("95", "Claims for home glucose testing supplies in last 12 months__1__Diabetes__diabetes.5 ");
        put("96", "Annual microalbumin urine screen__1__Diabetes__diabetes.6");
        put("97", "Annual LDL-C screening__1__Diabetes__diabetes.7 ");
        put("98", "Diabetes with CVD or >40 yrs with CVD risks not on statin__0__Diabetes__diabetes.8");

        put("99", "LDL<100mg/dL__1__Diabetes__diabetes.9");
        put("100", "LDL<130mg/dL__1__Diabetes__diabetes.10");
        put("101", "BP<130/80 mmHg__1__Diabetes__diabetes.11");
        put("102", "BP<140/90 mmHg__1__Diabetes__diabetes.12");
        put("103", "HbA1c<7.0%__1__Diabetes__diabetes.13");
        put("104", "HbA1c<8.0%__1__Diabetes__diabetes.14");
        put("105", "HbA1c>9.0%__0__Diabetes__diabetes.15");
        put("106", "Smoking Status/Cessation Advice/Treatment__1__Diabetes__diabetes.16");
        put("107", "Diabetics on an ACE/ARB__1__Diabetes__diabetes.17");
        put("108", "Diabetics on oral anti diabetic agents__1__Diabetes__diabetes.18");

        //Hepatitis
        put("261", "HCV RNA testing at week 12 of antiviral treatment __1__Hepatitis C__hepatitis.1");
        put("262", "HCV RNA testing at week 24 of antiviral treatment __1__Hepatitis C__hepatitis.2");

        //Hyperlipidemia
        put("141", "Hyperlipidemia Annual lipid profile__1__Hyperlipidemia__hyperlipidemia.1");
        put("142", "On lipid-lowering medication__1__Hyperlipidemia__hyperlipidemia.2");
        put("143", "Prescribed statin and gaps in prescription refills__0__Hyperlipidemia__hyperlipidemia.3 ");

        //Hypertension
        put("181", "On antihypertensive medication__1__Hypertension__hypertension.1");
        put("182", "Annual lipid profile__1__Hypertension__hypertension.2");
        put("183", "Hypertension diagnosis and prescribed statin and gaps in prescription refills__0__Hypertension__hypertension.3");
        put("184", "Annual serum creatinine test__1__Hypertension__hypertension.4");
        put("185", "On antihypertensives medication __1__ Hypertension__hypertension.5 ");

        //Rheumatoid Arthritis
        put("281", "On Disease-Modifying Anti-Rheumatic Drugs (DMARD)__1__RA__rheumatoidArthritis.1 ");

        //Wellness
        put("221", "Age 50 to 64 with annual flu vaccination__1__Wellness__wellness.2");
        put("222", "Age 50 to 75 years with colorectal cancer screening__1__Wellness__wellness.3");
        put("223", "Women age 21 years and older with cervical cancer screen last 24 months__1__Wellness__wellness.4 ");
        put("224", "Males age greater than 49 with PSA test in last 24 months__1__Wellness__wellness.5");
        put("225", "Women age 65 and older with screening for osteoporosis__1__Wellness__wellness.6");
        put("226", "Routine exam in last 24 months__1__Wellness__wellness.7 ");
        put("227", "Women age 40 to 69 with a screening mammogram last 24 months__1__Wellness__wellness.8");
        put("228", "Lead Screening in Children__1__Wellness__wellness.9");
        put("229", "Annual well-child exam (age 2 to 6 yrs)__1__Wellness__wellness.10");
        put("230", "Annual well-child exam (age 7 to 12 yrs)__1__Wellness__wellness.11");
        put("231", "Annual well-child exam (age 13 to 21 yrs)__1__Wellness__wellness.12");
        put("232", "Age 2 - 6 yrs with recommended immunizations__1__Wellness__wellness.13");
        put("233", "Age 13 to 21 yrs with recommended immunizations__1__Wellness__wellness.14 ");
        put("234", "Age 13 yrs with recommended immunizations__1__Wellness__wellness.15");
        put("235", "Age 2 yrs with recommended immunizations__1__Wellness__wellness.16 ");
        put("236", "Well Child Visit - 15 months__1__Wellness__wellness.17 ");
        put("237", "Infant - 1 or more Well Child Visit__1__Wellness__wellness.18v");
        put("238", "Infant - Non-Well Child Visit Only__0__Wellness__wellness.19");
        put("239", "Infant - Well & Non-Well Office Visit__1__Wellness__wellness.20 ");
        put("240", "Routine office visit in last 6 months__1__Wellness__wellness.21");
        put("241", "Women age 21 years and older with cervical cancer screen last 36 months__1__Wellness__wellness.22 ");
        put("242", "Glaucoma screening in last 24 months (age 65 yrs. & older)__1__Wellness__wellness.23");
        put("243", "Members aged 19 years to 39 with preventive visit in last 24 months__1__Wellness__wellness.24");
        put("244", "Members aged 40 years to 64 years with preventive visit in last 24 months__1__Wellness__wellness.25");
        put("245", "Women age 21-65 with recommended cervical cancer screening__1__Wellness__wellness.26");
        put("246", "Members age 19 to 39 years with cholesterol screening is done__1__Wellness__wellness.27");
        put("247", "Members age 40 to 64 years with cholesterol screening is done__1__Wellness__wellness.28");
        put("248", "Members aged 65 years and older with an annual preventive visit__1__Wellness__wellness.29");
//
//        //Additional Gaps
        put("1", "Aged 65 years and older on high risk drug__0__Additional Gaps__additionalGaps.1 ");
        put("4", "No PCP visit last 12 months__0__Additional Gaps__additionalGaps.4");


    }};

    public static Map<String, String> ASTHMA_CODE_DESC = new HashMap<String, String>() {{
        put("21", "Return to hospital with same asthma diagnosis within 30 days following inpatient discharge__0__Asthma__asthma.1");
        put("22", "Visit to an ED/Urgent care office for asthma in the past 6 months__0__Asthma__asthma.2");
        put("23", "Asthma and a routine provider visit in the last 12 months__1__Asthma__asthma.3");
        put("24", "Children with asthma-related acute visit in the past two months__0__Asthma__asthma.4");
        put("25", "Members with asthma currently taking a prescription medication for asthma__1__Asthma__asthma.5");
        put("26", "Asthma with pneumococcal vaccination__1__Asthma__asthma.6");
        put("27", "Two or more asthma related ER Visits in the last 6 months__0__Asthma__asthma.7");
        put("28", "Asthma related admit in last 12 months__0__Asthma__asthma.8");
        put("29", "Asthma with influenza vaccination in last 12 months__1__Asthma__asthma.9");
        put("30", "Persistent asthma with annual pulmonary function test__1__Asthma__asthma.10");
        put("31", "Received control inhaler (long acting) in last 12 months __1__Asthma__asthma.11");
        put("32", "Received rescue inhaler (short acting) in last 12 months __1__Asthma__asthma.12");
        put("33", "canister short-acting inhaled beta agonist/months__0__Asthma__asthma.13");
    }};

    public static Map<String, String> CHRONIC_OBSTRUCTIVE_PULMONARY = new HashMap<String, String>() {{
        put("71", "Re-admission to hospital with COPD diagnosis within 30 days following a COPD inpatient stay discharge__0__COPD__COPD.1");
        put("72", "Members with COPD who have had a visit to an ER for COPD related diagnosis in the past 90 days__0__COPD__COPD.2");
        put("73", "Exacerbations in last 12 months__0__COPD__COPD.3");
        put("74", "Individuals over the age of 20 years with diagnosed COPD are on bronchodilator therapy__1__COPD__COPD.4");
        put("75", "Members with COPD who had an annual spirometry test__1__COPD__COPD.5");
        put("76", "Annual flu vaccination__1__COPD__COPD.6");
        put("77", "Exacerbation with inhaled bronchodilator/ corticosteroid last 12 months__1__COPD__COPD.7");
    }};


    public static Map<String, String> CONGESTIVE_HEART_FAILURE = new HashMap<String, String>() {{
        put("121", "Heart failure and atrial fibrillation on warfarin therapy__1__CHF__CHF.1");
        put("122", "Heart failure and LVSD on ACE/ARB__1__CHF__CHF.2");
        put("123", "Heart failure and LVSD on beta-blocker__1__CHF__CHF.3");
        put("124", "Re-admission to hospital with Heart Failure diagnosis within 30 days following a HF inpatient stay discharge__0__CHF__CHF.4");
        put("125", "ER Visit for Heart Failure in last 90 days__0__CHF__CHF.5");
        put("126", "Follow-up OV within 4 weeks of discharge from HF admission__1__CHF__CHF.6");
    }};

    public static Map<String, String> CORONARY_ARTERY_DISEASE = new HashMap<String, String>() {{
        put("51", "Annual lipid profile__1__CAD__CAD.1");
        put("52", "On anti-platelet medication__1__CAD__CAD.2");
        put("53", "On lipid lowering medication__1__CAD__CAD.3");
        put("54", "On anti-platelets medication__1__ CAD__CAD.4");
    }};

    public static Map<String, String> DIABETES = new HashMap<String, String>() {{
        put("91", "Annual dilated eye exam__1__Diabetes__diabetes.1");
        put("92", "Annual foot exam__1__Diabetes__diabetes.2");
        put("93", "Annual HbA1c test done__1__Diabetes__diabetes.3");
        put("94", "Annual lipid profile__1__Diabetes__diabetes.4 ");
        put("95", "Claims for home glucose testing supplies in last 12 months__1__Diabetes__diabetes.5 ");
        put("96", "Annual microalbumin urine screen__1__Diabetes__diabetes.6");
        put("97", "Annual LDL-C screening__1__Diabetes__diabetes.7 ");
        put("98", "Diabetes with CVD or >40 yrs with CVD risks not on statin__0__Diabetes__diabetes.8");
        put("99", "LDL<100mg/dL__1__Diabetes__diabetes.9");
        put("100", "LDL<130mg/dL__1__Diabetes__diabetes.10");
        put("101", "BP<130/80 mmHg__1__Diabetes__diabetes.11");
        put("102", "BP<140/90 mmHg__1__Diabetes__diabetes.12");
        put("103", "HbA1c<7.0%__1__Diabetes__diabetes.13");
        put("104", "HbA1c<8.0%__1__Diabetes__diabetes.14");
        put("105", "HbA1c>9.0%__0__Diabetes__diabetes.15");
        put("106", "Smoking Status/Cessation Advice/Treatment__1__Diabetes__diabetes.16");
//        put("107", "Diabetics on an ACE/ARB__1__Diabetes__diabetes.17");
//        put("108", "Diabetics on oral anti diabetic agents__1__Diabetes__diabetes.18");
    }};

    public static Map<String, String> HYPERLIPIDEMIA = new HashMap<String, String>() {{
        put("141", "Hyperlipidemia Annual lipid profile__1__Hyperlipidemia__hyperlipidemia.1");
        put("142", "On lipid-lowering medication__1__Hyperlipidemia__hyperlipidemia.2");
        put("143", "Prescribed statin and gaps in prescription refills__0__Hyperlipidemia__hyperlipidemia.3 ");
    }};

    public static Map<String, String> HYPERTENSION = new HashMap<String, String>() {{
        put("181", "On antihypertensive medication__1__Hypertension__hypertension.1");
        put("182", "Annual lipid profile__1__Hypertension__hypertension.2");
        put("183", "Hypertension diagnosis and prescribed statin and gaps in prescription refills__0__Hypertension__hypertension.3");
        put("184", "Annual serum creatinine test__1__Hypertension__hypertension.4");
    }};

    public static Map<String, String> RHEUMATOID_ARTHRITIS = new HashMap<String, String>() {{
        put("281", "On Disease-Modifying Anti-Rheumatic Drugs (DMARD)__1__RA__rheumatoidArthritis.1 ");
    }};

    public static Map<String, String> WELLNESS = new HashMap<String, String>() {
        {
            put("221", "Age 50 to 64 with annual flu vaccination__1__Wellness__wellness.2");
            put("222", "Age 50 to 75 years with colorectal cancer screening__1__Wellness__wellness.3");
            put("223", "Women age 21 years and older with cervical cancer screen last 24 months__1__Wellness__wellness.4 ");
            put("224", "Males age greater than 49 with PSA test in last 24 months__1__Wellness__wellness.5");
            put("225", "Women age 65 and older with screening for osteoporosis__1__Wellness__wellness.6");
            put("226", "Routine exam in last 24 months__1__Wellness__wellness.7 ");
            put("227", "Women age 40 to 69 with a screening mammogram last 24 months__1__Wellness__wellness.8");
            put("228", "Lead Screening in Children__1__Wellness__wellness.9");
            put("229", "Annual well-child exam (age 2 to 6 yrs)__1__Wellness__wellness.10");
            put("230", "Annual well-child exam (age 7 to 12 yrs)__1__Wellness__wellness.11");
            put("231", "Annual well-child exam (age 13 to 21 yrs)__1__Wellness__wellness.12");
            put("232", "Age 2 - 6 yrs with recommended immunizations__1__Wellness__wellness.13");
            put("233", "Age 13 to 21 yrs with recommended immunizations__1__Wellness__wellness.14 ");
            put("234", "Age 13 yrs with recommended immunizations__1__Wellness__wellness.15");
            put("235", "Age 2 yrs with recommended immunizations__1__Wellness__wellness.16 ");
            put("236", "Well Child Visit - 15 months__1__Wellness__wellness.17 ");
            put("237", "Infant - 1 or more Well Child Visit__1__Wellness__wellness.18v");
            put("238", "Infant - Non-Well Child Visit Only__0__Wellness__wellness.19");
            put("239", "Infant - Well & Non-Well Office Visit__1__Wellness__wellness.20 ");
            put("240", "Routine office visit in last 6 months__1__Wellness__wellness.21");
            put("241", "Women age 21 years and older with cervical cancer screen last 36 months__1__Wellness__wellness.22 ");
            put("242", "Glaucoma screening in last 24 months (age 65 yrs. & older)__1__Wellness__wellness.23");
            put("243", "Members aged 19 years to 39 with preventive visit in last 24 months__1__Wellness__wellness.24");
            put("244", "Members aged 40 years to 64 years with preventive visit in last 24 months__1__Wellness__wellness.25");
            put("245", "Women age 21-65 with recommended cervical cancer screening__1__Wellness__wellness.26");
            put("246", "Members age 19 to 39 years with cholesterol screening is done__1__Wellness__wellness.27");
            put("247", "Members age 40 to 64 years with cholesterol screening is done__1__Wellness__wellness.28");
            put("248", "Members aged 65 years and older with an annual preventive visit__1__Wellness__wellness.29");
        }
    };


    public static Map<String, String> ADDITIONAL_GAPS = new HashMap<String, String>() {{
        put("1", "Aged 65 years and older on high risk drug__0__Additional Gaps__additionalGaps.1 ");
        put("2", "On statin drug without both an ALT and an AST in the past 12 months__0__Additional Gaps__additionalGaps.2");
        put("3", "No monthly PT/INR for warfarin users__0__Additional Gaps__additionalGaps.3");
        put("4", "No PCP visit last 12 months__0__Additional Gaps__additionalGaps.4");
    }};

    public static Map<String, String> PREGNANCY = new HashMap<String, String>() {
        {
            put("201", "Postpartum visit 21 to 56 days after delivery__1__Pregnancy__pregnancy.1");
            put("202", "Timeliness of Prenatal Care__1__Pregnancy__pregnancy.2");
        }
    };


    public static Map<String, String> UTILIZATION = new HashMap<String, String>() {
        {
            put("161", "Two or more ER Visits in the last 6 months__0__Utilization__utilization.1");
            put("162", "Hospital readmission within 30 days of discharge__0__Utilization__utilization.2");
            put("163", "Office visit within 30 days of inpatient discharge__1__Utilization__utilization.3");
            put("164", "3 or more ER Visits in the last 6 months__0__Utilization__utilization.4");
        }
    };

}
