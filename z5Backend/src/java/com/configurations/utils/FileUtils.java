package com.configurations.utils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FileUtils {
    public static Map<String, String> readFile(String name, Parser parser) {
        Map<String, String> codes = new HashMap<String, String>();

        try {
            FileInputStream stream = new FileInputStream(name);
            DataInputStream in = new DataInputStream(stream);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if ("".equals(line.trim()) || line.startsWith("#")) {//ignore comments and empty lines
                    continue;
                }
                parser.parseLine(codes, line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }

        return codes;
    }

    public static Map<String, String> readFile(String name) {
        return readFile(name, new DefaultParser());
    }

    public static interface Parser {
        public void parseLine(Map<String, String> codes, String line);
    }

    public static class DefaultParser implements Parser {
        @Override
        public void parseLine(Map<String, String> codes, String line) {
            FileUtils.parseLine(codes, line);
        }
    }

    public static class GeneralParser implements Parser {

        @Override
        public void parseLine(Map<String, String> codes, String line) {
            codes.put(line.trim(), "0");
        }
    }

    public static class POSAggregatorParser implements Parser {
        @Override
        public void parseLine(Map<String, String> codes, String line) {
            String[] kv = line.split(":");
            if (kv.length < 2) {
                return;
            }
            codes.put(kv[0].trim(), kv[1]);
        }
    }

    public static class ChronicConditionsParser implements Parser {
        @Override
        public void parseLine(Map<String, String> codes, String line) {
            String[] kv = line.split("=");
            String[] vals = kv[1].split(";");
            codes.put((kv[0]).trim().toLowerCase(), (vals[0]).trim());
        }
    }

    protected static void parseLine(Map<String, String> codes, String line) {
        String code = line;
        if (code.contains("\"")) {
            code = line.substring(1, line.indexOf("\"", 1));
        }
        code = code.replaceAll("\"", "");
        code = code.replaceAll("-", "");
        codes.put(code, "0");
    }
}
