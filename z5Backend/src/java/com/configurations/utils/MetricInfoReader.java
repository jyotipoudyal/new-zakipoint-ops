package com.configurations.utils;

import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.RequestUtils;

import java.net.URL;
import java.util.*;

public class MetricInfoReader {
    private static MetricInfoReader metricInfoReader;
    private Map<Integer, String> metricDescriptionMap;
    public Map<String,Integer> metricNameIdMap;
    Map<Integer,String>metricIdNameMap;
    private Map<Integer, String> metricCategoryMap;
    private Map<Integer, Boolean> metricTypeMap;
    private Map<String, Set<Integer>> categoryMetricMap;
    private Map<String, Set<Integer>> customMetricsMap;
    public static  String[] RESOURCES = {"/Resources/qualityMetricsDescription"};

    public static MetricInfoReader getMetricInfoReader() {
        if (metricInfoReader == null) metricInfoReader = new MetricInfoReader();
        return metricInfoReader;
    }
    public static MetricInfoReader getMetricInfoReader(String file) {
        RESOURCES = new String[]{file};
        metricInfoReader = new MetricInfoReader();
        return metricInfoReader;
    }

    public MetricInfoReader() {
        init();
    }

    private void init() {
        metricDescriptionMap = new HashMap<Integer, String>();
        metricCategoryMap = new HashMap<Integer, String>();
        metricTypeMap = new HashMap<Integer, Boolean>();
        categoryMetricMap = new HashMap<String, Set<Integer>>();
        customMetricsMap = new HashMap<String, Set<Integer>>();
        metricNameIdMap =new HashMap<String, Integer>();
        metricIdNameMap=new HashMap<Integer, String>();
        for (String resource : RESOURCES) {
            URL url = this.getClass().getResource(resource);
            Map<String, String> qualityMetricsMap = FileUtils.readFile(url.getFile(), new FileUtils.ChronicConditionsParser());
            for (Map.Entry<String, String> metric : qualityMetricsMap.entrySet()) {
                String name = metric.getKey().trim();
                String[] tokens = metric.getValue().split(":");
                int id=Integer.parseInt(tokens[3].trim());
                metricNameIdMap.put(name,id);
                metricIdNameMap.put(id,name);
                metricDescriptionMap.put(id, tokens[0].trim());
                metricTypeMap.put(id, "1".equals(tokens[1].trim()));
                String category = tokens[2].trim();
                metricCategoryMap.put(id, category);
                if (tokens.length > 4) //only for custom metrics
                {
                    addMetricMap(tokens[4].trim(), id, customMetricsMap);
                }
                else addMetricMap(category, id, categoryMetricMap);
            }
        }
    }

    public void addMetricMap(String key, int value, Map<String, Set<Integer>> map) {
        if (!map.containsKey(key)) map.put(key, new HashSet<Integer>());
        map.get(key).add(value);
    }
    public List<Integer>getMetricIds(String...names){
        List<Integer>metricIds=new ArrayList<Integer>();
        for(String _name: names){
            String name=_name.toLowerCase();
            if(metricNameIdMap.containsKey(name)){
                metricIds.add(metricNameIdMap.get(name));
            }
            else{
                throw new RuntimeException("metric:"+name+" does not have a defined id");
            }
        }
        return metricIds;
    }
    public int getMetricId(String _name){

        String name=_name.toLowerCase();
        if(metricNameIdMap.containsKey(name)){
            return metricNameIdMap.get(name);
        }
        else{
            for(Integer code : ChronicConditionEnum.ChronicConditionMetrics.getAllMetricCode()){
                ChronicConditionEnum.ChronicConditionMetrics qmValue = ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(code);
                if(qmValue.getMetricDescription().equalsIgnoreCase(name.toString())) {
                    return code;
                }
            }
        }
        throw new RuntimeException("metric:"+name+" does not have a defined id");
    }
    public String getMetricName(int id){

        if(metricIdNameMap.containsKey(id)){
            return metricIdNameMap.get(id);
        }
        else{
            throw new RuntimeException("metric:"+id+" does not have a defined id");
        }

    }
    public String getMetricDescription(int name) {
        return metricDescriptionMap.get(name);
    }

    public String getMetricCategory(int name) {
        return metricCategoryMap.get(name);
    }

    public Boolean isPositiveMetric(int name) {
        return metricTypeMap.get(name);
    }

    public Set<Integer> getMetricsInCategory(String category) {
        return categoryMetricMap.get(category);
    }

    public Collection<Integer> getAllMetricIds() {
        return metricNameIdMap.values();
    }

    public Map<Integer, String> getMetricDescriptionMap() {
        return metricDescriptionMap;
    }

    public Set<Integer> getCustomMetrics(String customClient) {
        return customMetricsMap.get(customClient);
    }
    public Set<Integer>getCustomMetrics(){
        Set<Integer>customMetricsSet=new HashSet<Integer>();
        for(String s:customMetricsMap.keySet()){
            customMetricsSet.addAll(getCustomMetrics(s));
        }
        return customMetricsSet;
    }
    public static Set<Integer> getRequiredMetrics(RequestState state) {
        Set<Integer> requiredMetrics = new HashSet<Integer>();
        if (state.request.hasParameter("categories") || state.request.hasParameter("metrics")) {
            String category = state.request.get("categories");
            getAllMetrics(category, "categories", requiredMetrics);
            if (requiredMetrics.size() == 0) {
                String metrics = state.request.get("metrics");
                getAllMetrics(metrics, "metrics", requiredMetrics);
            }
        } else {

            requiredMetrics.addAll(metricInfoReader.getAllMetricIds());
        }
        return requiredMetrics;
    }
    private static void getAllMetrics(String requestedValue, String requestBasis, Set<Integer> requiredMetrics) {
        if (requestedValue != null) {
            if("metrics".equals(requestBasis)){
                if (RequestUtils.isArrayRequest(requestedValue)) {
                    for (int _item : metricInfoReader.getMetricIds(RequestUtils.getArrayRequest(requestedValue))) {
                        requiredMetrics.add(_item);
                    }
                }
                else requiredMetrics.add(metricInfoReader.getMetricId(requestedValue));
            }else{
                if (RequestUtils.isArrayRequest(requestedValue)) {
                    for (String _item : RequestUtils.getArrayRequest(requestedValue)) {
                        requiredMetrics.addAll(metricInfoReader.getMetricsInCategory(_item));
                    }
                }
                else{
                    requiredMetrics.addAll(metricInfoReader.getMetricsInCategory(requestedValue));
                }
            }
        }
    }

}
