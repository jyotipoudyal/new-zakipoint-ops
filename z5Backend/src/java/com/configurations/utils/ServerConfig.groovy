package com.configurations.utils

class ServerConfig {
    String name
    String hostname
    int port
    int httpPort
}
