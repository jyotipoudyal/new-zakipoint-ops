package com.configurations.utils;

import com.configurations.config.ConfigurationFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaraConfigurationUtils {
    // TODO list maintenance for fields to corresponding fields
    public static final List<String> riskScoreFieldsDefault = Arrays.asList("prospectiveInpatient","prospectiveOutpatient","prospectivePhysician","prospectivePharmacy","prospectiveMedical","prospectiveTotal","prospectiveEstimate","prospectiveERScore","prospectiveOther","prospectiveTotalDouble",
            "concurrentTotalDouble","concurrentInpatient","concurrentOutpatient","concurrentPhysician","concurrentPharmacy","concurrentMedical","concurrentTotal","concurrentEstimate","concurrentERScore","concurrentOther");

    public static final List<String> riskScoreFieldsRawGroup = Arrays.asList("prospectiveTotalNormalizedToGroup","prospectiveTotalDoubleNormalizedToGroup","prospectiveEstimateNormalizedToGroup","prospectiveERScoreNormalizedToGroup","prospectiveOtherNormalizedToGroup","concurrentTotalNormalizedToGroup","concurrentTotalDoubleNormalizedToGroup","concurrentEstimateNormalizedToGroup","concurrentERScoreNormalizedToGroup","concurrentOtherNormalizedToGroup","prospectiveInpatientNormalizedToGroup","prospectiveOutpatientNormalizedToGroup","prospectivePhysicianNormalizedToGroup","prospectivePharmacyNormalizedToGroup","prospectiveMedicalNormalizedToGroup","concurrentInpatientNormalizedToGroup","concurrentOutpatientNormalizedToGroup","concurrentPhysicianNormalizedToGroup","concurrentPharmacyNormalizedToGroup","concurrentMedicalNormalizedToGroup","prospectiveTotalRaw","prospectiveTotalDoubleRaw","prospectiveEstimateRaw","prospectiveERScoreRaw","prospectiveOtherRaw","concurrentTotalRaw","concurrentTotalDoubleRaw","concurrentEstimateRaw","concurrentERScoreRaw","concurrentOtherRaw","prospectiveInpatientRaw","prospectiveOutpatientRaw","prospectivePhysicianRaw","prospectivePharmacyRaw","prospectiveMedicalRaw","concurrentInpatientRaw","concurrentOutpatientRaw","concurrentPhysicianRaw","concurrentPharmacyRaw","concurrentMedicalRaw",
            "prospectiveTotalNormalizedToBob","prospectiveTotalDoubleNormalizedToBob","prospectiveEstimateNormalizedToBob","prospectiveERScoreNormalizedToBob","prospectiveOtherNormalizedToBob","concurrentTotalNormalizedToBob","concurrentTotalDoubleNormalizedToBob","concurrentEstimateNormalizedToBob","concurrentERScoreNormalizedToBob","concurrentOtherNormalizedToBob","prospectiveInpatientNormalizedToBob","prospectiveOutpatientNormalizedToBob","prospectivePhysicianNormalizedToBob","prospectivePharmacyNormalizedToBob","prospectiveMedicalNormalizedToBob","concurrentInpatientNormalizedToBob","concurrentOutpatientNormalizedToBob","concurrentPhysicianNormalizedToBob","concurrentPharmacyNormalizedToBob","concurrentMedicalNormalizedToBob");

    public static String getRootField(String riskField) {
        if (riskField.contains("NormalizedToGroup")) {
            riskField = riskField.replace("NormalizedToGroup", "");
        } else if (riskField.contains("Raw")) {
            riskField = riskField.replace("Raw", "");
        }else if(riskField.contains("NormalizedToBob"))
            riskField = riskField.replace("NormalizedToBob", "");
        return riskField;
    }

    public static String getMaraType(String clientId) {
        String maraType = "Bob";
        if (ConfigurationFactory.clientJsonMap.containsKey(clientId)) {
            Object clientLevelMap = ConfigurationFactory.clientJsonMap.get(clientId);
            Map<String, String> innerMap = new HashMap<String, String>((Map<String, String>) clientLevelMap);

            maraType = innerMap.containsKey("maraType") ? innerMap.get("maraType") : "Bob";
        } else {
            System.out.println("clientId: " + clientId + " not loaded into memory");
        }
        return maraType;
    }

    public static String currentMaraRiskScoreField(String clientId, String fieldName) {
        String maraType = getMaraType(clientId);
        return maraRiskScoreFields(maraType, fieldName);
    }

    public static String maraRiskScoreFields(String maraType, String fieldName) {
        if (riskScoreFieldsDefault.contains(fieldName))
            if (maraType.equalsIgnoreCase("Group")) {
                return fieldName + "NormalizedTo" + maraType;
            }else if (maraType.equalsIgnoreCase("Raw")) {
                return fieldName + maraType;
            } else if (maraType.equalsIgnoreCase("Bob")) {
                return fieldName + "NormalizedTo" + maraType;
            }
        return fieldName;
    }

    public static void replaceScoreFields(String clientId, List<String> fieldList) {
        String maraType = getMaraType(clientId);
        for (String field : fieldList) {
            fieldList.set(fieldList.indexOf(field), maraRiskScoreFields(maraType, field));
        }
    }

    public static Map<String,String> replaceScoreFieldsByClientId(String clientId, List<String> fieldList) {
        String maraType = getMaraType(clientId);
        Map<String, String> map = new HashMap<String, String>();
        for (String field : fieldList) {
            map.put(field,replaceMaraRiskScoreFields(maraType, field));
        }
        return map;
    }

    public static String replaceMaraRiskScoreFields(String maraType, String fieldName) {

        if (maraType.equalsIgnoreCase("Group")) {
            return fieldName + "NormalizedTo" + maraType;
        }else if (maraType.equalsIgnoreCase("Raw")) {
            return fieldName + maraType;
        } else if (maraType.equalsIgnoreCase("Bob")) {
            return fieldName + "NormalizedTo" + maraType;
        }
        return fieldName;
    }
}
