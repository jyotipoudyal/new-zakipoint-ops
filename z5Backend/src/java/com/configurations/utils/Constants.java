package com.configurations.utils;

import java.util.*;

public class Constants {

    public static final String ELIGIBILITY_TYPE_MEDICAL = "medical";
    public static final String ELIGIBILITY_TYPES_FOR_SNAPSHOT = "[medical,vision,dental]";
    public static final String ELIGIBILITY_TYPES_FOR_SELECTED_PROCEDURE = "[medical,vision,dental]";
    public static final List<String> multiSearchReports       = new ArrayList<String>(Arrays.asList("geographicData",
            "pmpmTrending", "chronicCondition","relationshipPMPM","chronicConditions","top20Loa","heatMap",
            "heatMap_drill", "erVisit", "thirtyDayReadmit","snapshot",
            "networkComparison","expenseDistribution","readmission_drill", "coverageByRelationship",
            "newDashboard", "membershipDistribution","selectedProcedure", "GroupRiskScore","grs","gra","grd",
            "GroupAgeRiskDistribution","GroupRelationshipRiskDistribution", "highCostMembers", "programReport",
            "memberCost","cbr","cbrByClass", "summation","programReportDrill","hospitalUtilization","hospitalUtilization_drill","outcomeReport","outreachReport"
            ,"medicalTrend","medicalTrendAbove10K"
            ,"pharmacyTrend"
            ,"pharmacyTrendAbove10K", "qoc", "diagnosisErVisit", "fieldSum","dollarsPerClaim", "membershipGraph", "dowErVisit"
    ));

    public final static Set<String> recordEsTypes = new HashSet<String>(Arrays.asList(Constants.DENTAL, Constants.VISION, Constants.WORKERS_COMPENSATION,Constants.ESTYPE_VENDOR));

    public static final String POS_AGGREGATOR_LOC    = "Resources/pOSAggregator.csv";
    public static final String PARTICIPATION_DEFAULT = "default";
    public static final String TOTAL = "total";

    public static final List<String> BOB_EXCLUDE_FIELDS= Arrays.asList( "comparisonFrom", "comparisonTo", "comparisonPaidThrough","isParticipation","program_type");
    public static final List<String> COHORT_FIELDS = new ArrayList<String>(Arrays.asList("cohortId", "cohortIndex"));
    public static final String FIELD_RELATIONSHIP_CLASS      = "memberRelationshipClass";

    public static final List<String> REQUEST_ITERATOR_NAMES=Arrays.asList(
            "bob" , "benchmark"
    );
    public static final  String[] PERIODS={"reporting","comparison"};
    public static final  String[] PERIODS_AND_BOB={"reporting","comparison","bob"};
    public static final String FIELD_RELATIONSHIP_DESC      = "memberRelationshipDesc";

    private Constants(){}

    public static final Integer ZERO   = 0;
    public static final Integer ONE    = 1;
    public static final Integer TWO    = 2;
    public static final Integer THREE  = 3;
    public static final Integer FOUR   = 4;
    public static final Integer FIVE   = 5;
    public static final Integer TWENTY = 20;
    public static final Integer HUNDRED = 100;
    public static final int THOUSANDS   = 1000;

    public static final String INPATIENT                  = "Inpatient";
    public static final String OUTPATIENT                 = "Outpatient";
    public static final String OFFICE                     = "Office";
    public static final String COLON                 = ":";
    public static final String COLON_HASH            = ":#";
    public static final String TILDE_EXCLAMATION     = "~!";
    public static final String UNDERSCORE            = "_";
    public static final String HASH                  = "#";
    public static final String DASH                  = "-";
    public static final String EQUALS                = "=";
    public static final String AND                   = "&";
    public static final String WHAT                  = "?";
    public static final String DOT                   = ".";

    //types
    public static final String TYPE_ES_GROUP_RISK_ANALYSIS = "ESGroupRiskAnalysis";
    public static final String ESTYPE_MEDICAL              = "Medical";
    public static final String ESTYPE_MEMBERSEARCH         = "MemberSearch";
    public static final String ESTYPE_ELIGIBILITY          = "Eligibility";
    public static final String ESTYPE_PHARMACY             = "Pharmacy";
    public static final String ESTYPE_DENTAL               = "Dental";
    public static final String ESTYPE_VISION               = "Vision";
    public static final String ESTYPE_WORKERS_COMPENSATION = "WorkersCompensation";
    public static final String ESTYPE_VENDOR               = "Vendor";
    public static final String ESTYPE_COHORTS              = "Cohorts";
    public static final String ESTYPE_PARTICIPATION_PTPN   = "ParticipationPTPN";
    public static final String ESTYPE_PROVIDER             = "Provider";
    public static final String ESTYPE_VISIT_ADMISSION      = "VisitAdmission";

    public static final String WORKERS_COMPENSATION       = "Workers_Comp";
    public static final String DENTAL                     = "Dental";
    public static final String VISION                     = "Vision";

    //
    public static final String REPORTING_BASIS_SERVICE_DATE = "ServiceDate";
    public static final String REPORTING_BASIS_PAID_DATE    = "PaidDate";
    //fields
    public static final String FIELD_PAID_DATE            = "PaidDate";
    public static final String FIELD_MEMBER_MONTH         = "memberMonth";
    public static final String FIELD_PROVIDER_ID        = "udf23Id";

    public static final String FIELD_PAID_AMOUNT          = "paidAmount";
    public static final String ES_FIELD_INC_PAID_AMOUNT   = "incPaidAmt";
    public static final String FIELD_RELATIONSHIP         = "relationship";
    public static final String FIELD_RELATIONSHIP_FROM_DATE         = "relationship.fromDate";
    public static final String FIELD_RELATIONSHIP_TO_DATE           = "relationship.toDate";
    public static final String FIELD_RELATIONSHIP_ID                = "relationship.id";
    public static final String FIELD_RELATIONSHIP_ELIGIBILITY       = "relationship.eligibilityType";
    public static final String FIELD_UM_TYPE              = "um.type";
    public static final String FIELD_UM_PAID_AMOUNT       = "um.paidAmount";
    public static final String FIELD_UM_PAID_MONTH        = "um.paidMonth";
    public static final String FIELD_PROGRAM_TYPES           = "participation.type";
    public static final String FIELD_PROGRAM_NAME            = "participation.program";
    public static final String FIELD_PROGRAM_CODE            = "participation.programCode";
    public static final String FIELD_PARTICIPATION_FROM_DATE = "participation.fromDate";
    public static final String FIELD_PARTICIPATION_TO_DATE   = "participation.toDate";
    public static final String FIELD_PARTICIPATION           = "participation";
    public static final String FIELD_RISK                    = "risk";
    public static final String FIELD_RELATIONSHIPID          = "relationshipId";
    public static final String FIELD_RELATIONSHIPI_DESC      = "memberRelationshipDesc";
    public static final String FIELD_ELIGIBILITY_TYPE     = "eligibilityType";
    public static final String FIELD_GROUP                = "group";
    public static final String FIELD_SEARCH_KEY           = "searchKey";
    public static final String FIELD_GROUPID              = "groupId";
    public static final String FIELD_PLAN_TYPE_ID         = "planTypeId";
    public static final String FIELD_VALUE_FIELD          = "valueField";

    public static final String TRENDING_FLAG              = "Trending";
    public static final String PLACE_OF_SERVICE           = "placeOfService";
    public static final String IP_FLAG                    = "IPFlag";
    //
    public static final String TOTAL_MEDICAL_PAID_AMOUNT  = "totalMedicalPaidAmount";
    public static final String TOTAL_PHARMACY_PAID_AMOUNT = "totalPharmacyPaidAmount";
    public static final String PHARMACY_CLAIMS_PAID       = "pharmacyClaimsPaid";
    public static final String SUBSCRIBER_COUNT           = "subscriberCount";
    public static final String SUBSCRIBERS                = "subscribers"; //facetField
    public static final String MEMBER_COUNT               = "memberCount";
    public static final String EMPLOYEE_COUNT             = "employeeCount";
    public static final String MEMBER_MONTHS              = "memberMonths";
    public static final String EMPLOYEE_MONTHS            = "employeeMonths";
    public static final String MEMBERS                    = "members";
    public static final String TOTAL_DENTAL_PAID_AMOUNT           = "totalDentalClaimsPaid";  //   totalDentalPaidAmount
    public static final String TOTAL_VISION_PAID_AMOUNT           = "totalVisionClaimsPaid";  //totalVisionPaidAmount
    public static final String TOTAL_WORKERS_COMP_CLAIMS_PAID     = "totalWorkersCompClaimsPaid";


    //MedicalTrend and PharmacyTrend
    public static final Set<String> MEDICAL_FIELDS = new HashSet<String>(Arrays.asList("totalPaidAmount", "claimCount", "claimLines", "billedAmount", "paidInNetworkFacility", "paidInNetworkProfessional", "medicalPaidFacility", "medicalPaidProfessional", "incurredLag", "receivedLag"));
    public static final Set<String> PHARMACY_FIELDS = new HashSet<String>(Arrays.asList("scripts", "totalPaidAmount", "billedAmount", "allowedAmount", "stax", "deductibleAmount", "coinsuranceAmount", "ingredCost", "genericDrugsAmount", "genericDrugsScripts", "brandDrugsAmount",
            "brandDrugsScripts", "ppoSavings", "90DaysSupplyAmount", "90DaysSupplyScripts", "coPay_0-5", "coPay_5-10", "coPay_10-15", "coPay_15-20", "coPay_20-25", "coPay_25-30", "coPay_30-30+", "totalCopayAmount", "dispAmount"));

    public static final Set<String> MEDICAL_PAID_AMOUNT_FIELDS = new HashSet<String>() {{
        add("billedAmount");
        add("paidInNetworkFacility");
        add("totalPaidAmount");
        add("paidInNetwork");
        add("paidInNetworkProfessional");
        add("medicalPaidFacility");
        add("medicalPaidProfessional");
        add("allowedAmount");
        add("coinsuranceAmount");
        add("totalCopayAmount");
        add("ppoSavings");
        add("90DaysSupplyAmount");
        add("brandDrugsAmount");
        add("genericDrugsAmount");
        add("dispAmount");
        add("deductibleAmount");
        add("ingredCost");

    }};


    // facet pattern
    public static final String FACET_FORMAT                        = "{metric_name}:{month}:{metric_value_name}:#{participation}";
    public static final Integer POSITION_OF_METRIC_IN_FACET        = 0;
    public static final Integer POSITION_OF_DATE_IN_FACET          = 1;
    public static final Integer POSITION_OF_METRIC_FIELD_IN_FACET  = 2;
    public static final String  QUARTER_PREFIX                     = "Q";
    public static Map<Integer, String> qmMap = new HashMap<Integer, String>() {
        {
            put(1001,"affectivePsychosis");
            put(1002,"asthma");
            put(1003,"atrialFibrillation");
            put(1004,"bloodDisorders");
            put(1005,"CAD");
            put(1006,"cancer");
            put(1007,"chronicPain");
            put(1008,"chf");
            put(1009,"COPD");
            put(1010, "demyelinatingDiseases");
            put(1011, "depression");
            put(1012, "diabetes");
            put(1013, "eatingDisorders");
            put(1014, "ESRD");
            put(1015, "HIVAIDS");
            put(1016, "hyperlipidemia");
            put(1017, "hypertension");
            put(1018, "immuneDisorders");
            put(1019, "inflammatoryBowelDisease");
            put(1020, "liverDiseases");
            put(1021, "morbidObesity");
            put(1022, "osteoarthritis");
            put(1023, "peripheralVascularDisease");
            put(1024, "rheumatoidArthritis");
        }
    };
    public static Map<String, String> utilizationMap = new HashMap<String, String>() {
        {
            put("hospitalInpatient", "admits");
            put("erVisit", "erVisit");
        }
    };

    public static Map<String, Boolean> switchable = new HashMap<String, Boolean>() {
        {
            put("udf23Id", false);
        }
    };
}



