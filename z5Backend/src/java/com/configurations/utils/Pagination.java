package com.configurations.utils;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Pagination<T> {
    T[] itemList;
    int itemSize;


    /**
     * The argument should be sorted (and keep the order)
     * @param items
     * */
    public Pagination(T[] items ) {
        this.itemList=items;
        this.itemSize=items.length;
    }

    public T[] paginate(int from,int size){
        if(itemSize>0){
            int listFrom=validate(from*size,true);
            int listTo=validate(from*size+size,false);
            return Arrays.copyOfRange(itemList, listFrom, listTo);
        }else {
            return itemList;
        }
    }

    private int validate(int i,boolean from) {
        int toCompare = from ? (itemSize-1) : itemSize;
        return i>(toCompare) ? (toCompare) : i;
    }

    public static List<Map.Entry<String, Double>> getPaginatedMap(Map<String, Double> sortedMap, int page, int pageSize){
        int from = mapPage(page*pageSize, sortedMap.size(),true);
        int to = mapPage(page*pageSize+pageSize, sortedMap.size(),false);
        List<Map.Entry<String, Double>> indexedList = new LinkedList<>(sortedMap.entrySet());
        if(indexedList.size() > 0)
            return indexedList.subList(from, to);
        else
            return indexedList;
    }

    public static int mapPage(int page, int pageSize, boolean from){
        int i = from ? pageSize : pageSize;
        return page>i ? i : page;
    }
}
