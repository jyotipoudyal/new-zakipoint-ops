package com.configurations.utils;

import com.configurations.ResponseFormatter;
import zakipointCore.commonUtils.SpringBeanUtils;

public class ResponseRenderers {
    private static final String RENDERER = ".renderer";
    private static final String SEPARATOR = "~!";
    private static final String DS_FORMATTER = ".dasformatter";

    public static ResponseRenderer getInstance(String tableCode) {
        tableCode = tableCode.contains(SEPARATOR) ? tableCode.split(SEPARATOR)[0] : tableCode;
        if (SpringBeanUtils.doesBeanExist(tableCode + RENDERER)) {
            return SpringBeanUtils.getBean(tableCode + RENDERER);
        }
        return SpringBeanUtils.getBean("default"+RENDERER);
    }

    /**
     * This method format response from membersearch,
     * If formatter with 'report' (from request) is present then Formatter will be called or
     * default formatter will be called
     *
     * */
    public static ResponseFormatter getFormatter(String request) {
        if (SpringBeanUtils.doesBeanExist(request + DS_FORMATTER)) {
            return SpringBeanUtils.getBean(request + DS_FORMATTER);
        }
        return SpringBeanUtils.getBean("default"+DS_FORMATTER);
    }

    private ResponseRenderers() {
    }
}
