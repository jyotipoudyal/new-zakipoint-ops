package com.configurations.utils;

public class AndQuery extends Query.AbstractBooleanQuery{

    @Override
    public boolean evaluate(Object obj){
        for (Query query : queries) {
            if(query.evaluate(obj)==false)
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String andQuery = "(";
        int count = 0;
        for (Query query : queries) {
            if(count==0)
                andQuery = andQuery + query.toString();
            else
                andQuery = andQuery + " and " + query.toString();
            count++;
        }
        andQuery = andQuery +")";
        return andQuery;
    }
}
