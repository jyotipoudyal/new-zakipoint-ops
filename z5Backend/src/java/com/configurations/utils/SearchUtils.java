package com.configurations.utils;


import java.util.*;

public class SearchUtils {
    //    This method is used to get list from string like [a,b,c]. Also handles simple string like a.

    public static List<String> getListFromArray(String s) {
        if(s==null)
            return Collections.emptyList();

        List<String> list = new ArrayList<String>();
        for (String element : s.replace("]", "").replace("[", "").split(",")) {
            list.add(element.replaceAll("\"","").trim());
        }
        return list;
    }

    public static boolean isArray(String text) {
        String v = text.trim();
        return v.startsWith("[") && v.endsWith("]");
    }

    public static List<String> getSplittedListFromArray(String str){
        if(str==null)
            return Collections.emptyList();

        List<String> list = new ArrayList<String>();
        str = str.replaceAll(" ", "");
        for (String element : str.split(",\\[")) {
            element = element.replace("[","").replace("]","").trim();
            list.add(element);
        }
        return list;
    }
}
