package com.configurations.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateConversionUtility {
    private final static DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private final static DateFormat usDateFormat = new SimpleDateFormat("MM-dd-yyyy");
    private final static TimeZone timeZone = TimeZone.getTimeZone("GMT");

    synchronized public static long getTimeFromDateWrtTimeZone(String dateStr) {
        DateFormat formatter;
        sqlDateFormat.setTimeZone(timeZone);
        usDateFormat.setTimeZone(timeZone);
        dateTimeFormat.setTimeZone(timeZone);
        String[] parts = dateStr.split("-");
        if (parts[0].length() > 2 && (!parts[2].contains(":") || parts[2].contains("T")))
            formatter = sqlDateFormat;
        else if (parts[2].contains(":"))
            formatter = dateTimeFormat;
        else
            formatter = usDateFormat;

        return getTimeFromDateWrtTimeZone(dateStr, formatter);
    }

    public static long getTimeFromDateWrtTimeZone(String dateStr, DateFormat formatter) {
        formatter.setTimeZone(timeZone); // change the time zone of this formatter
        Date date = null;
        try {
            date = formatter.parse(dateStr);
            long time = date.getTime();
            return time;
        } catch (ParseException e) {
            if(dateStr.matches("^[-]?[0-9]+$")) {
                Long longDate = Long.valueOf(dateStr);
                return longDate;
            }
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return Long.MIN_VALUE;
        }
    }

    public static String getDateInSQLFormat(Date d) {
        sqlDateFormat.setTimeZone(timeZone);
        return sqlDateFormat.format(d);

    }

    public static String getDateInSQLFormat( long d ){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(timeZone);
        calendar.setTimeInMillis(d);
        return getDateInSQLFormat(calendar.getTime());
    }

    public static String convertUsToSqlFormat(String date) {
        long d = getTimeFromDateWrtTimeZone(date);
        if (date.contains(":"))
            return getDateTimeInSQLFormat(d);
        else
            return getDateInSQLFormat(d);

    }

    public static Date getProperDateWrtTimeZone( long time ){
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(time);
        return calendar.getTime();
    }

    static {
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        sqlDateFormat.setTimeZone(timeZone);
        usDateFormat.setTimeZone(timeZone);
    }

    public static long getProperMonthYearWrtTimeZone(String value,int dateToSet){
        long dateLong=getTimeFromDateWrtTimeZone(value,new SimpleDateFormat("MMMMM yyyy"));
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(dateLong);
        calendar.set(Calendar.DATE,dateToSet);
        return calendar.getTimeInMillis();
    }


    public static long getLastDateOfMonth(String dateString) {
        long date = getTimeFromDateWrtTimeZone(dateString);
        Calendar cal = getCalendar(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTimeInMillis();
    }

    public static long getFirstDateOfMonth(String dateString) {
        long date = getTimeFromDateWrtTimeZone(dateString);
        Calendar cal = getCalendar(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTimeInMillis();
    }

    public static Calendar getCalendar(long time) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.setTimeInMillis(time);
        return calendar;
    }

    public static String getDateTimeInSQLFormat(Date d) {
        dateTimeFormat.setTimeZone(timeZone);
        return dateTimeFormat.format(d);
    }

    public static String getDateTimeInSQLFormat( long d ){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(timeZone);
        calendar.setTimeInMillis(d);
        return getDateTimeInSQLFormat(calendar.getTime());
    }
}
