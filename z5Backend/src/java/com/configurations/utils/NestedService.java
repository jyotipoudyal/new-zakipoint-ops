package com.configurations.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface NestedService {
    Map<String,Object> process(Object obj, Map<String, String> requestParameters);
    List<HashMap<String,Object>> processPharmacy(Object obj, Map<String, String> requestParameters);
    Map<String,Object> calculate(String fieldName, Object obj, Map<String, String> requestParameters);
}
