package com.configurations.utils;

import java.util.HashMap;
import java.util.Map;

public class FieldMap {
    public static final Map<String,String> simpleFieldMap=new HashMap<String,String>(){{
        put("planId","planId") ;
        put("groupId","groupId") ;
        put("divisionId","divisionId") ;
        put("planTypeId","planTypeId") ;
        put("relationshipId","relationshipId") ;
        put("carrierId","carrierId") ;

    }};
}
