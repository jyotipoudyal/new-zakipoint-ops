package com.configurations.utils;

public class Medical {
    //fields
    public static final String NAME = "Medical";
    public static final String SERVICE_DATE = "serviceDate";
    public static final String PAID_DATE = "paidDate";
    public static final String EM_VISITS = "eAndMVisits";
    public static final String PROCEDURE_COUNTS = "procedureCounts";

    public static final String NDC_CODE ="ndcCode";
    public static final String NDC_DESCRIPTION ="ndcDescription";
    public static final String GROUP_ID = "groupId";
    public static final String PAID_AMOUNT = "paidAmount";
}
