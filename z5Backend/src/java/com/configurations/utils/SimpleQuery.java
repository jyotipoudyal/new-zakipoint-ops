package com.configurations.utils;

import com.configurations.config.FieldConfigurations;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.text.ParseException;
import java.util.*;

public class SimpleQuery extends Query.AbstractQuery {
    String expression = "";
    public static String arrayFieldIndicator = "(\\[).*(\\])"; // the value of array-field is enclosed in square-brackets.
    public static long POSITIVE_INFINITY = Long.MAX_VALUE;
    public static long NEGATIVE_INFINITY = Long.MIN_VALUE;

    public SimpleQuery(String expression) {
        this.expression = expression;
    }

    public boolean evaluate(Object obj) {
        Map<String, Object> objectMap = (Map<String, Object>) obj;
        JSONObject object = (JSONObject) grails.converters.JSON.parse(expression);
        final Object key = object.keys().next();
        final Object value = object.get(key);
        String keyString = key.toString();
        int indexOfOperator = keyString.lastIndexOf(".");
        String fieldName = keyString.substring(0, indexOfOperator);
        String operator = keyString.substring(indexOfOperator+1);

        if (objectMap.get(fieldName) == null && !operator.equals("neq"))
            return false;

        /*Equals*/
        if (operator.equals("eq")) {
            List<String> listValues = SearchUtils.getListFromArray(value.toString());
            for (String listValue : listValues) {
                if(!(objectMap.get(fieldName) instanceof String || objectMap.get(fieldName) instanceof ArrayList)){
                    if(objectMap.get(fieldName) instanceof Double && objectMap.get(fieldName).equals(Double.parseDouble(listValue))){
                        return true;
                    }
                    else if (objectMap.get(fieldName) instanceof Integer && objectMap.get(fieldName).toString().equals(String.valueOf(getDateAsLong(listValue, fieldName)))) {
                        return true;
                    }
                    else if(objectMap.get(fieldName) instanceof Float && objectMap.get(fieldName).equals(Float.parseFloat(listValue))){
                        return true;
                    }
                }
                else{
                    // For now this only applies to pharmacy report drill down
                    if (objectMap.get(fieldName).toString().matches(arrayFieldIndicator)) {
                        if (objectMap.get(fieldName).toString().contains(listValue))
                            return true;
                    }
                    if (listValue.equals(objectMap.get(fieldName).toString())) {
                        return true;
                    }
                }
            }
        }
        /*Contains*/
        if (operator.equals("contains")) {
            List<String> listValues = SearchUtils.getListFromArray(value.toString());
            for (String listValue : listValues) {
                String[] queryValues = (listValue.toLowerCase()).split(" ");
                String objVal = objectMap.get(fieldName).toString().toLowerCase();
                int size = queryValues.length;
                for (String queryVal : queryValues) {
                    if (objVal.contains(queryVal)) {
                        size--;
                    }
                }
                return (size == 0) ? true : false;
            }
        }
        /*Not Equals*/
        if (operator.equals("neq")) {
            List<String> listValues = SearchUtils.getListFromArray(value.toString());
            for (String listValue : listValues) {
                if(!(objectMap.get(fieldName) instanceof String)){
                    if(objectMap.get(fieldName) instanceof Double && !(objectMap.get(fieldName).equals(Double.parseDouble(listValue)))){
                        return true;
                    }
                    else if(objectMap.get(fieldName) instanceof Integer && !(objectMap.get(fieldName).toString().equals(String.valueOf(getDateAsLong(listValue, fieldName))))){
                        return true;
                    }
                    else if(objectMap.get(fieldName) instanceof Float && !(objectMap.get(fieldName).equals(Float.parseFloat(listValue)))){
                        return true;
                    }

                }
                else{
                    if (!listValues.contains(objectMap.get(fieldName))) {
                        return true;
                    }
                }
            }
        }
        /*Between*/
        if (operator.equals("between")) {
            return rangeComparator(value, objectMap, fieldName);
        }
        /*Less Than*/
        if (operator.equals("lt")) {
            long queryDate = getDateAsLong(String.valueOf(value),fieldName);
            String date = objectMap.get(fieldName).toString();
            if (date.matches(arrayFieldIndicator))
                return customRangeComparator(NEGATIVE_INFINITY, queryDate, date, false);
            if ("".equals(date) || date.equals(null) || date.isEmpty()) {
                return false;
            }
            long dateActual = getDateAsLong(objectMap.get(fieldName).toString());
            if (dateActual < queryDate) {
                return true;
            }
        }
        /*Less Than and Equal to*/
        if (operator.equals("lte")) {
            long queryDate = getDateAsLong(String.valueOf(value),fieldName);
            String date = objectMap.get(fieldName).toString();
            if (date.matches(arrayFieldIndicator))
                return customRangeComparator(NEGATIVE_INFINITY, queryDate, date, true);
            if ("".equals(date) || date.equals(null) || date.isEmpty()) {
                return false;
            }
            long dateActual = getDateAsLong(objectMap.get(fieldName).toString());
            if (dateActual <= queryDate) {
                return true;
            }
        }
        /*Greater than*/
        if (operator.equals("gt")) {
            long queryDate = getDateAsLong(String.valueOf(value),fieldName);
            String date = objectMap.get(fieldName).toString();
            if (date.matches(arrayFieldIndicator))
                return customRangeComparator(queryDate, POSITIVE_INFINITY, date, false);
            if ("".equals(date) || date.equals(null) || date.isEmpty()) {
                return false;
            }
            long dateActual = getDateAsLong(objectMap.get(fieldName).toString());
            if (dateActual > queryDate) {
                return true;
            }
        }
        /*Greater than and equal To*/
        if (operator.equals("gte")) {
            long queryDate = getDateAsLong(String.valueOf(value),fieldName);
            String date = objectMap.get(fieldName).toString();
            if (date.matches(arrayFieldIndicator))
                return customRangeComparator(queryDate, POSITIVE_INFINITY, date, true);
            if ("".equals(date) || date.equals(null) || date.isEmpty()) {
                return false;
            }
            long dateActual = getDateAsLong(objectMap.get(fieldName).toString());
            if (dateActual >= queryDate) {
                return true;
            }
        }
        return false;
    }

    private boolean rangeComparator(Object value, Map<String, Object> objectMap, String fieldName) {
        List<String> listDates = SearchUtils.getListFromArray(value.toString());
        long lowerDateRange = getDateAsLong(listDates.get(0),fieldName);
        long upperDateRange = getDateAsLong(listDates.get(1),fieldName);
        String dateValue = objectMap.get(fieldName).toString();
        if (dateValue.matches(arrayFieldIndicator))
            return customRangeComparator(lowerDateRange, upperDateRange, dateValue, true);
        long fieldDateValue = getDateAsLong(dateValue);
        if (lowerDateRange > upperDateRange) {
            throw new UnsupportedOperationException("Lower Date Range greater than upperDateRange");
        } else if (fieldDateValue >= lowerDateRange && fieldDateValue <= upperDateRange) {
            return true;
        }
        return false;
    }

    private boolean customRangeComparator(long lowerDateRange, long upperDateRange, String dateValue, boolean includeUpperLower) {
        String[] dateValues = dateValue.replace("[", "").replace("]", "").split(",");

        for (String value : dateValues) {
            long fieldDateValue = getDateAsLong(value);
            if (lowerDateRange > upperDateRange)
                throw new UnsupportedOperationException("Lower Date Range greater than upperDateRange");
            else {
                if (includeUpperLower && fieldDateValue >= lowerDateRange && fieldDateValue <= upperDateRange)
                    return true;
                else if (!includeUpperLower && fieldDateValue > lowerDateRange && fieldDateValue < upperDateRange)
                    return true;
            }
        }
        return false;
    }

    public static long getDateAsLong(String s, String fieldName) {
        try {
            return formatter.parse(s).getTime();
        } catch (ParseException e) {

            if(isDateField(fieldName)) return (new Double(s)).longValue();

            double d = Double.parseDouble(s)*100;       //Used for fields other than date format
            long l = (new Double(d)).longValue();
            return l;
        }
    }

    public static long getDateAsLong(String s) {
        // todo: find a better solution for "long date format which come as string"
        if (!s.substring(1).contains("-"))
            return Long.parseLong(s);
        try {
            return formatter.parse(s).getTime();
        } catch (ParseException e) {
            double d = Double.parseDouble(s)*100;       //Used for fields other than date format
            long l = (new Double(d)).longValue();
            return l;
        }
    }

    @Override
    public String toString() {
        return expression.replaceAll("[{}]", "");
    }

    private static boolean isDateField(String fieldName) {
        boolean isDate = true;
        if(fieldName == null)
            return isDate;
        String type=null;
        Set<String> tableCode = new HashSet<String>(Arrays.asList("ms","smc","srx","sl"));
        for (String code : tableCode) {
            try {
                type= getType(code, fieldName);
                break;
            } catch (Exception e) {
                continue;
            }

        }
        if(type!=null) {
            if(type.equals("float") || type.equals("double")) {
                isDate =false;
            }
        }

        return isDate;
    }

    private static String getType(String code,String fieldName) {
        return FieldConfigurations.fromTableCode(code).fromFieldName(fieldName).type;
    }
}
