package com.configurations.utils;

import com.configurations.search.MemberSearchRequest;
import org.elasticsearch.index.query.FilterBuilder;

public interface ReportFilter {
    public FilterBuilder createFilter(MemberSearchRequest searchRequest);
}
