package com.configurations.utils;


public class RequestParameters {

    private RequestParameters(){

    }

    public static final String PAGE_SIZE             = "pageSize";
    public static final String PAGE                  = "page";

}
