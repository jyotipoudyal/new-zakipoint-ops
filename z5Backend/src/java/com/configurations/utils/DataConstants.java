package com.configurations.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class DataConstants {
    public static final BigDecimal hundred = new BigDecimal(100);

    {
        hundred.setScale(2);
    }


    public static Map<String, String> mapping = new HashMap<String, String>() {
        {
            put("SearchClaim", "Medical");
            put("SearchElig", "Eligibility");
            put("SearchRx", "Pharmacy");
        }
    };
}
