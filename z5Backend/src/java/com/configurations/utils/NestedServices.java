package com.configurations.utils;

import zakipointCore.commonUtils.SpringBeanUtils;

public class NestedServices {
    public static NestedService getInstance(String displayClass) {
        return SpringBeanUtils.getBean(displayClass + ".nested.renderer");
    }
}
