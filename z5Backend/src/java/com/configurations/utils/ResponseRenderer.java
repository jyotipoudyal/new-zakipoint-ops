package com.configurations.utils;

import com.configurations.search.MemberSearchRequest;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.search.SearchHit;

public interface ResponseRenderer {
    public JSONObject renderResults(SearchHit[] hits, MemberSearchRequest searchRequest);
}
