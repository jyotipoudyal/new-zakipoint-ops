package com.configurations.utils;

import com.configurations.config.FieldConfig;
import org.elasticsearch.index.query.FilterBuilder;

public interface SimpleFilter {
    FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig);
}
