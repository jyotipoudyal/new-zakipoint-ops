package com.configurations.utils;

import com.configurations.config.FieldConfig;
import com.configurations.config.FieldConfigurations;
import com.configurations.parser.JsonQueryParser;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.List;

public class NewQueryParser {
    String tableCode;
    List<String> fieldsToInclude;

    public NewQueryParser(String tableCode, List<String> fieldsToIncludeList) {
        this.tableCode = tableCode;
        this.fieldsToInclude = fieldsToIncludeList;
    }

    public Query getQuery(JSONObject jsonQuery) {
        if (JsonQueryParser.isBooleanExpression(jsonQuery)) {
            String queryType = jsonQuery.keys().next().toString();
            JSONArray array = (JSONArray) jsonQuery.get(queryType);

            Query booleanQuery = getBooleanFilter(queryType);

            for (Object obj : array) {
                booleanQuery.add(getQuery((JSONObject) obj));
            }
            return booleanQuery.getReducedQuery();
        } else {
            return getSimpleFilter(jsonQuery, new CCE_Filter(fieldsToInclude));
        }
    }


    public Query getSimpleFilter(JSONObject jsonObject, FilterQuery filterQuery) {
        final Object key = jsonObject.keys().next();
        String value = jsonObject.get(key).toString();
        JSONObject expression = new JSONObject();
        if (filterQuery.remove(key.toString())) {
            return null;
        }
        String keyString = key.toString();
        int indexOfOperator = keyString.lastIndexOf(".");
        String displayName = keyString.substring(0, indexOfOperator);
        String operator = keyString.substring(indexOfOperator + 1);
        FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCode).fromDisplayName(displayName);
        if (fieldConfig != null) {

            String field = FieldConfigurations.fromTableCode(tableCode).fromDisplayName(displayName).field;

            String type = FieldConfigurations.fromTableCode(tableCode).fromFieldName(field).type;
            if (type.equalsIgnoreCase("date")) {
                value = DataConvertionUtils.convertDate(value);
            }

            expression.accumulate(field + "." + operator, value);
            return new SimpleQuery(expression.toString());
        } else return null;

    }



    public static Query getBooleanFilter(String type) {
        if (type.equalsIgnoreCase("and")) {
            return new AndQuery();
        } else if (type.equalsIgnoreCase("or")) {
            return new OrQuery();
        } else if (type.equalsIgnoreCase("not")) {
            return new NotQuery();
        }
        else
            throw new IllegalArgumentException(type);
    }


    public interface FilterQuery {
        boolean remove(String expression);
    }

    private static class CCE_Filter implements FilterQuery {
        private List<String> fieldsToInclude;

        public CCE_Filter(List<String> fieldsToInclude) {
            this.fieldsToInclude = fieldsToInclude;
        }

        public boolean remove(String expression) {
            String field=getField(expression);
            return !fieldsToInclude.contains(field);
        }
    }

    public static String getField(String key) {
        int indexOfDot = key.lastIndexOf(".");
        return indexOfDot == -1 ? key : key.substring(0, indexOfDot);
    }
}
