package com.configurations.utils;

public class MemberSearch {
    public static final String NAME                       = "MemberSearch";
    public static final String CLIENT_ID                  = "clientId";
    public static final String FIELD_PAID_DATE            = "PaidDate";
    public static final String FIELD_MEMBER_MONTH         = "memberMonth";
    public static final String FIELD_PAID_AMOUNT          = "paidAmount";
    public static final String ES_FIELD_INC_PAID_AMOUNT   = "incPaidAmt";
    public static final String FIELD_RELATIONSHIP         = "relationship";
    public static final String FIELD_RELATIONSHIP_ID      = "relationshipId";
    public static final String FIELD_UM_TYPE              = "um.type";
    public static final String FIELD_UM_PAID_AMOUNT       = "um.paidAmount";
    public static final String FIELD_UM_PAID_MONTH        = "um.paidMonth";
    public static final String FIELD_ELIGIBILITY_TYPE     = "eligibilityType";
    public static final String FIELD_GROUP                = "group";
    public static final String FIELD_GROUP_ID             = "groupId";
    public static final String FIELD_GROUP_NAME           = "groupName";
    public static final String FIELD_MEMBER_AGE           = "memberAge";
    public static final String FIELD_INT_MEMBER_ID        = "intMemberId";
    public static final String MEMBER_ID                  = "memberId";
    public static final String FIELD_MEMBER_GENDER        = "memberGenderId";
    public static final String FIELD_MEMBER_FULL_NAME     = "memberFullName";
    public static final String FIELD_UNBLIND_MEMBER_ID     = "unblindMemberId";

    public static final String FIELD_PROSPECTIVE_TOTAL    = "prospectiveTotal";
    public static final String FIELD_CONCURRENT_TOTAL    = "concurrentTotal";
    public static final String FIELD_SEARCH_KEY           = "searchKey";
    public static final String FIELD_VALUE_FIELD          = "valueField";
    public static final String FIELD_PRIMARY_DIAGNOSIS_DESC = "primaryDiagnosisCodeDesc";

    public static final String MEMBER_STATUS = "memberStatus";
    public static final String FIELD_MEMBER_STATUS_FROM_DATE  = "memberStatus.fromDate";
    public static final String FIELD_MEMBER_STATUS_TO_DATE    = "memberStatus.toDate";
    public static final String FIELD_MEMBER_STATUS_STATUS     = "memberStatus.status";

    public static final String FIELD_CONCURRENT_TOTAL_DOUBLE  = "concurrentTotalDouble";
    public static final String FIELD_PROSPECTIVE_TOTAL_DOUBLE = "prospectiveTotalDouble";

    public static final String FIELD_RA_NESTED                = "RiskAnalysisAmount";
    public static final String FIELD_RA_NESTED_GROUP_ID       = "RiskAnalysisAmount.groupId";
    public static final String FIELD_RA_NESTED_PAID_AMOUNT    = "RiskAnalysisAmount.paidAmount";
    public static final String FIELD_RA_NESTED_ALLOWED_AMOUNT = "RiskAnalysisAmount.allowedAmount";

    public static final String FIELD_PRACTICE_GROUP_ID        = "udf20Id";        //eg. 'GP3278'
    public static final String FIELD_PRACTICE_GROUP_NAME      = "udf21Id";

    public static final String FIELD_LOCATION_ID              = "udf22Id";         //eg '2063 Lowes Alley'
    public static final String FIELD_LOCATION_NAME            = "udf22Id";

    public static final String FIELD_PROVIDER_ID              = "udf23Id";         //eg. '00079821'
    public static final String FIELD_PROVIDER_NAME            = "udf24Id";

    public static final String FIELD_RA_PAID_AMOUNT    = "riskAnalysisTotalPaidAmount";//riskAnalysisTotalPaidAmount
    public static final String FIELD_RA_ALLOWED_AMOUNT = "riskAnalysisTotalAllowedAmount";//riskAnalysisTotalAllowedAmount
    public static final String FIELD_EXPOSURE_MONTHS   = "exposureMonths";


    public static final String FIELD_MEMBER_DOB="memberDOB";
    public static final String FIELD_LCOATION_INFO="locationInfo";
    public static final String FIELD_MSA_TYPE="msa";


    public static final String FIELD_MEDICAL_ICD9="medical_icd9";
    public static final String FIELD_MEDICAL_DIAGNOSIS_GROUPER="medical_diagnosisGrouper";
    public static final String HRA_HISTORY="hra_history";
    public static final String PARTICIPATION_LEVEL="participation_level";
    public  static  final  String KYN_COMPLETION_HISTORY= "kyn_completion_history";

    public static class ParticipationLevel{
        public static final String NAME = "participation_level";
        public static final String NAME_ = NAME + ".";
        public static final String FROM_DATE = NAME_ + "fromDate";
        public static final String TO_DATE = NAME_ + "toDate";
        public static final String PROGRAM = NAME_ + "program";


    }
    public static class Participation{
        public static final String NAME="participation";
        public static final String NAME_=NAME+".";
        public static final String FROM_DATE=NAME_+"fromDate";
        public static final String TO_DATE=NAME_+"toDate";
        public static final String PROGRAM=NAME_+"program";
    }
    public static class  ClinicalSavings{
        public static final String NAME = "clinical_savings";
        public static final String NAME_ = NAME + ".";
        public static final String FROM = NAME_ + "from";
        public static final String TO = NAME_ + "to";
        public static final String TYPE = NAME_ + "type";
        public static final String SAVINGS = NAME_ + "saving";


    }

    public static class Procedure {
        public static final String NAME = "procedure";
        public static final String NAME_ = NAME + ".";
        public static final String PAID_MONTH = NAME_ + "paidMonth";
        public static final String SERVICE_MONTH = NAME_ + "serviceMonth";
        public static final String FACILITY = NAME_ + "facility";
        public static final String TYPE = NAME_+"type";
        public static final String PAID_AMOUNT = NAME_+"paidAmount";
        public static final String VALUE = NAME_+"value";
    }

    public static class ParticipationProgramCount {
        public static final String NAME = "participationProgramCount";
        public static final String NAME_ = NAME + ".";
        public static final String COUNT = NAME_ + "count";
        public static final String PROGRAMS = NAME_ + "programs";
        public static final String FROM_DATE = NAME_ + "fromDate";
        public static final String TO_DATE = NAME_+"toDate";

        public static class Programs{
            public static final String NAME_ = PROGRAMS + ".";
            public static final String PROGRAM = NAME_ + "program";
            public static final String TYPE = NAME_ + "type";
        }
    }

    public static class OutcomeHistory{
        public static final String NAME = "outcomeHistory";
        public static final String NAME_ = NAME + ".";
        public static final String CAREPLAN_START_DATE = NAME_ + "carePlanStartDate";
        public static final String CAREPLAN_END_DATE = NAME_ + "carePlanEndDate";
        public static final String PROGRAM = NAME_ + "program";
        public static final String PARTICIPANT = NAME_ + "participant";
        public static final String STATUS = NAME_ + "status";
        public static final String GOAL_PRESENT = NAME_+"goalPresent";

    }


    public static class NotesHistory{
        public static final String NAME = "notesHistory";
        public static final String NAME_ = NAME + ".";
        public static final String NOTE_TYPE = NAME_ + "noteType";
        public static final String PARTICIPANT = NAME_ + "participant";
        public static final String NOTE_CREATED_DATE = NAME_ + "noteCreatedDate";
        public static final String RELATIONSHIP_TYPE = NAME_+"relationshipType";

    }
    public static class  EducationalMailing{
        public static final String NAME = "educational_mailing";
        public static final String NAME_ = NAME + ".";
        public static final String PARTICIPANT = NAME_ + "participant";
        public static final String SENT_DATE = NAME_ + "sentDate";

    }

    public static class Readmission {
        public static final String NAME = "readmission";
        public static final String NAME_ = NAME + ".";

        public static final String INPATENT_ADMITS = NAME_ + "inpatientAdmit";
        public static final String SEVEN_DAY_READMITS = NAME_ + "sevenDayReadmit";
        public static final String FIFTEEN_DAY_READMITS = NAME_ + "fifteenDayReadmit";
        public static final String THIRTY_DAY_READMITS = NAME_ + "thirtyDayReadmit";
        public static final String PAID_MONTH = NAME_ + "paidMonth";
        public static final String SERVICE_MONTH = NAME_ + "serviceMonth";
        public static final String ADMISSION_DATE = NAME_ + "admitDate";
        public static final String DISCHARGE_DATE = NAME_ + "dischargeDate";
        public static final String CONDITION = NAME_ + "condition";
        public static final String FACILITY = NAME_ + "facility";
        public static final String FACILITY_ID = FACILITY+"Id";
        public static final String PCP = NAME_ + "pcp";
        public static final String PCP_ID = PCP+"Id";
        public static final String DIAGNOSIS_CODES = NAME_ + "diagnosisCodes";
        public static final String PRIMARY_DIAG_CODES = NAME_ + "primaryDiagCodes";
        public static final String PRIOR = NAME_ + "prior";
        public static final String SUBSEQUENT = NAME_ + "subsequent";
        public static final String DAYS_BETWEEN = NAME_ + "DaysBetween";
        public static final String LATEST_PCP = NAME_ + "latestPCP";

    }
    public static class UM {
        public static final String NAME= "um";
        public static final String NAME_= NAME+".";

        public static final String TYPE = NAME_+"type";
        public static final String PAID_AMOUNT = NAME_+"paidAmount";
        public static final String MEMBER_PAID_AMT = NAME_+"memberPaidAmount";
        public static final String PLAN_PAID_AMT = PAID_AMOUNT;
        public static final String ALLOWED_AMOUNT = NAME_+"allowedAmount";
        public static final String VALUE = NAME_+"value";
        public static final String SERVICE_MONTH = NAME_+"serviceMonth";
        public static final String PAID_MONTH = NAME_+"paidMonth";

    }

    public static class QM {
        public static final String NAME= "qm";
        public static final String NAME_= NAME+".";

        public static final String MEASURE = NAME_+"measure";
        public static final String FROM_DATE = NAME_+"fromDate";
        public static final String TO_DATE = NAME_+"toDate";
    }
    public static class RELATIONSHIP {
        public static final String NAME= "relationship";
        public static final String NAME_= NAME+".";

        public static final String DESCRIPTION = NAME_+"description";
        public static final String FROM_DATE = NAME_+"fromDate";
        public static final String TO_DATE = NAME_+"toDate";
        public static final String ELIGIBILITY_TYPE = NAME_+"eligibilityType";
        public static final String ID = NAME_+"id";
        public static final String RELATIONSHIP_CLASS = NAME_+"memberRelationshipClass";
    }

    public static class MEDICAL_AMOUNT {
        public static final String NAME= "medical_amount";
        public static final String NAME_= NAME+".";

        public static final String PAID_AMOUNT = NAME_+"paidAmount";
        public static final String ALLOWED_AMOUNT = NAME_+"allowedAmount";
        public static final String SERVICE_MONTH = NAME_+"serviceMonth";
        public static final String PAID_MONTH = NAME_+"paidMonth";
    }
    public static class PHARMACY_AMOUNT {
        public static final String NAME= "pharmacy_amount";
        public static final String NAME_= NAME+".";

        public static final String PAID_AMOUNT = NAME_+"paidAmount";
        public static final String ALLOWED_AMOUNT = NAME_+"allowedAmount";
        public static final String SERVICE_MONTH = NAME_+"serviceMonth";
        public static final String PAID_MONTH = NAME_+"paidMonth";
    }

    public static class MEMBER_AMOUNT {
        public static final String NAME= "member_amount";
        public static final String NAME_= NAME+".";

        public static final String PAID_AMOUNT = NAME_+"totalPaidAmount";
        public static final String MEDICAL_AMOUNT = NAME_+"medicalPaidAmount";
        public static final String PHARMACY_AMOUNT = NAME_+"pharmacyPaidAmount";
        public static final String ALLOWED_AMOUNT = NAME_+"allowedAmount";
        public static final String SERVICE_MONTH = NAME_+"serviceMonth";
        public static final String PAID_MONTH = NAME_+"paidMonth";
        public static final String RELATIONSHIP_ID = NAME_+"relationshipId";
    }

    public static class MedicalTrends {
        public static final String NAME = "medicalTrends";
        public static final String NAME_ = "medicalTrends" + ".";
        public static final String MEDICAL_TRENDS_CLAIM_COUNT = NAME_+"claimCount";
        public static final String CLAIM_COUNT = "claimCount";
    }

    public static class PharmacyTrends {
        public static final String NAME = "pharmacyTrends";
        public static final String SCRIPTS = "scripts";
    }

    public static class MARA_HISTORICAL_RISK_SCORES{
        public static final String NAME = "MaraHistoricalRiskScores";
        public static final String NAME_ = NAME + ".";
        public static final String PROCESSED_DATE = NAME_ + "processedDate";
        public static final String PROSPECTIVE_TOTAL = NAME_ + "prospectiveTotal";
        public static final String ER_SCORE = NAME_ + "erScore";
    }


    public static class ReimbursementAmount {
        public static final String NAME= "reimbursement_amount";
        public static final String NAME_= NAME+".";
        public static final String PLAN_ID =NAME_ + "planName";
        public static final String RELATIONSHIP_ID =NAME_ + "relationshipCode";
        public static final String GROUP_ID =NAME_ + "groupId";
        public static final String PAID_MONTH =NAME_ + "paidMonth";
        public static final String PAID_START_DATE =NAME_ + "paidStartDate";
        public static final String PAID_END_DATE =NAME_ + "paidEndDate";
        public static final String PAID_AMOUNT =NAME_ + "paidAmount";
    }

    public static class PlanEnrollment {
        public static final String NAME= "planEnrollment";
        public static final String NAME_= NAME+".";
        public static final String PLAN_ID =NAME_ + "planId";
        public static final String PLAN_NAME =NAME_ + "planName";
        public static final String RELATIONSHIP_ID =NAME_ + "relationshipCode";
        public static final String GROUP_ID =NAME_ + "groupId";
        public static final String GROUP_NAME =NAME_ + "groupName";
        public static final String PAID_MONTH =NAME_ + "paidMonth";
        public static final String PAID_START_DATE =NAME_ + "paidStartDate";
        public static final String PAID_END_DATE =NAME_ + "paidEndDate";
        public static final String TIER =NAME_ + "tier";
        public static final String TIER_DESC =NAME_ + "tierDescription";
        public static final String BASIS =NAME_ + "basis";
    }

    public static class PREMIUM_AMOUNT {
        public static final String NAME= "premiumAmount";
        public static final String NAME_= NAME+".";
        public static final String SERVICE_MONTH = NAME_+"serviceMonth";
        public static final String PAID_MONTH = NAME_+"paidMonth";
        public static final String RELATIONSHIP_ID = NAME_+"relationshipId";
        public static final String PREMIUM_AMOUNT = NAME_+"premiumAmount";
        public static final String PLAN_TYPE_ID = NAME_+"planTypeId";
        public static final String PLAN_ID = NAME_+"planId";
        public static final String GROUP_ID = NAME_+"groupId";
        public static final String DIVISION_ID = NAME_+"divisionId";
        public static final String CARRIER_ID = NAME_+"carrierId";
    }

    public static class KYN_HISTORY {
        public static final String NAME = "kyn_history";
        public static final String NAME_ = NAME+".";
        public static final String KYN_TAKEN_DATE = NAME_+"kynTakenDate";
        public static final String ONE_SCORE = NAME_+"oneScore";
    }

    public static class HIGHCOST_MEMBERAMOUNT {
        public static final String NAME = "highcostMemberAmount";
        public static final String NAME_ = NAME+".";

        public static final String PAID_AMOUNT = NAME_+"totalPaidAmount";
        public static final String MEDICAL_AMOUNT = NAME_+"medicalPaidAmount";
        public static final String PHARMACY_AMOUNT = NAME_+"pharmacyPaidAmount";
        public static final String SERVICE_MONTH = NAME_+"serviceMonth";
        public static final String PAID_MONTH = NAME_+"paidMonth";
        public static final String GROUP_ID = NAME_+"groupId";
        public static final String PAID_END_DATE = NAME_+"paidEndDate";
    }
}
