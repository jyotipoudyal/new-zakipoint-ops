package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;

import static org.elasticsearch.index.query.FilterBuilders.termFilter;

public class ContainsFilter implements SimpleFilter {
    public static final ContainsFilter INSTANCE = new ContainsFilter();

    private ContainsFilter() {
    }

    /**
     * @param value Value to clean
     * @return String with no special characters and space greater or equal to two
     */
    public static String replaceAllSpecialCharactersWithSpace(String value) {
        return value.replaceAll("[^a-zA-Z0-9]", " ").replaceAll("[ ]{2,}", " ");
    }

    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        if (value instanceof String)
            value = ((String) value).toLowerCase();

        value = replaceAllSpecialCharactersWithSpace((String) value);
        if (!((String) value).contains(" ")) {
            return termFilter(field + ".contains", value);
        } else {
            AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
            String[] values = ((String) value).split(" ");
            for (String val : values) {
                andFilterBuilder.add(termFilter(field + ".contains", val));
            }
            return andFilterBuilder;
        }
    }
}
