package com.configurations.filters;

import com.configurations.utils.ReportFilter;
import zakipointCore.commonUtils.SpringBeanUtils;

public class ReportFilters {
    private static final String REPORT_FILTER = "reportFilter";


    public static ReportFilter getInstance(String report) {
        if(SpringBeanUtils.doesBeanExist(report + "." + REPORT_FILTER)){
            return SpringBeanUtils.getBean(report + "." + REPORT_FILTER);
        }
        return null;
    }

    private ReportFilters() {
    }
}
