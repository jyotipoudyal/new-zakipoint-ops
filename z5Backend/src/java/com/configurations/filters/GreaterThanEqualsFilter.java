package com.configurations.filters;


import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;

public class GreaterThanEqualsFilter implements SimpleFilter {
    public static final GreaterThanEqualsFilter INSTANCE = new GreaterThanEqualsFilter();

    private GreaterThanEqualsFilter() {
    }

    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        if(value.toString().equalsIgnoreCase("null")) value=null;
        if (isDDHCDrillDown(field)) {
            String queryField = getQueryField(field);
            if (queryField.contains("oneScore"))
                field = queryField;
        }
        return FilterBuilders.rangeFilter(field).gte(value);
    }

    private String getQueryField(String field) {
        String replacePattern = "(DDHC_|ddhc_|_reporting|_comparison)";
        return field.replaceAll(replacePattern, "");
    }

    private boolean isDDHCDrillDown(String field) {
        String pattern = "(DDHC|ddhc).*(reporting|comparison)";
        return field.matches(pattern);
    }
}
