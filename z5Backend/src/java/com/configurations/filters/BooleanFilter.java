package com.configurations.filters;

/**
 * Created with IntelliJ IDEA.
 * User: nirmal
 * Date: 4/17/13
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BooleanFilter {
    public void add(Filter filter);

    public Filter getFilter();
}
