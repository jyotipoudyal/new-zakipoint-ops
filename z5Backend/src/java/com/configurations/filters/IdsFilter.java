package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import com.configurations.config.FieldConfigurations;
import com.configurations.utils.SearchUtils;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.IdsFilterBuilder;

import java.util.List;

import static org.elasticsearch.index.query.FilterBuilders.idsFilter;

public class IdsFilter implements SimpleFilter {
    public static final IdsFilter INSTANCE = new IdsFilter();

    private IdsFilter() {
    }

    /**
     * Table code is supposed to passed to the field as parameter
     * If replaceAll is not done on the value, the last and first entry will be ignored while querying
     */
    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        List<String> ids = SearchUtils.getListFromArray(value.toString().replaceAll("\"", ""));

        String[] tableName = FieldConfigurations.fromTableCode(field).tables();

        IdsFilterBuilder idsFilterBuilder = idsFilter(tableName);
        idsFilterBuilder.addIds(ids.toArray(new String[ids.size()]));

        return idsFilterBuilder;
    }
}
