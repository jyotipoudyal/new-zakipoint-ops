package com.configurations.filters;

import org.elasticsearch.index.query.FilterBuilder;

public class Filter {
    public static final String DEFAULT_PATH = "default";

    private String path;
    FilterBuilder filterBuilder;

    public Filter(FilterBuilder filterBuilder) {
        this.filterBuilder = filterBuilder;
        path = DEFAULT_PATH;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FilterBuilder getFilterBuilder() {
        return filterBuilder;
    }

    public void setFilterBuilder(FilterBuilder filterBuilder) {
        this.filterBuilder = filterBuilder;
    }
}
