package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import org.elasticsearch.index.query.ExistsFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;

public class ExistsFilter implements SimpleFilter {
    public static final ExistsFilter INSTANCE = new ExistsFilter();

    private ExistsFilter() {
    }

    @Override
    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        ExistsFilterBuilder existsFilter = FilterBuilders.existsFilter(field);
        return existsFilter;
    }
}
