package com.configurations.filters;

import org.elasticsearch.index.query.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: nirmal
 * Date: 4/17/13
 * Time: 7:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class BooleanFilters {

    private BooleanFilters() {
    }

    public static BooleanFilter getInstance(String filterType) {
        if (filterType.equalsIgnoreCase("or")) {
            return new BooleanFilters.OrFilter();
        } else if (filterType.equalsIgnoreCase("and")) {
            return new BooleanFilters.AndFilter();
        } else if (filterType.equals("not")) {
            return new NotFilter();
        }
        else
            throw new IllegalArgumentException("No filter of type" + filterType + " defined in BooleanFilters");
    }

    private static class OrFilter implements BooleanFilter {

        Map<String, OrFilterBuilder> pathOrFilterMap = new HashMap<String, OrFilterBuilder>();
        AndFilterBuilder builder = new AndFilterBuilder();

        public void add(Filter filter) {
            if (!pathOrFilterMap.containsKey(filter.getPath())) {
                pathOrFilterMap.put(filter.getPath(), new OrFilterBuilder());
            }
            pathOrFilterMap.get(filter.getPath()).add(filter.getFilterBuilder());
            builder.add(filter.getFilterBuilder());
        }
        public Filter getFilter() {
            return new Filter(getFinalFilter());
        }

        private FilterBuilder getFinalFilter() {
            if (!pathOrFilterMap.containsKey(Filter.DEFAULT_PATH)) {
                pathOrFilterMap.put(Filter.DEFAULT_PATH, new OrFilterBuilder());
            }
            if (pathOrFilterMap.size() == 1) {
                return pathOrFilterMap.values().iterator().next();
            }

            OrFilterBuilder defaultFilter = pathOrFilterMap.get(Filter.DEFAULT_PATH);

            for (String path : pathOrFilterMap.keySet()) {
                if (!path.equals(Filter.DEFAULT_PATH)) { //nested Filter
                    defaultFilter.add(new NestedFilterBuilder(path, pathOrFilterMap.get(path)));
                }
            }
            return defaultFilter;
        }

    }

    private static class NotFilter implements BooleanFilter {

        FilterBuilder filterBuilder;
        private int filterCount = 0;

        @Override
        public void add(Filter filter) {

                filterBuilder = new BoolFilterBuilder().mustNot(filter.getFilterBuilder());
                filterCount++;

                if(filterCount>1)
                     throw new UnsupportedOperationException("Multiple filters inside \"not\" operation is not supported. ");
        }

        public Filter getFilter() {
            return new Filter(filterBuilder);
        }

    }


    private static class AndFilter implements BooleanFilter {

        Map<String, AndFilterBuilder> pathAndFilterMap = new HashMap<String, AndFilterBuilder>();
        AndFilterBuilder builder = new AndFilterBuilder();

        public void add(Filter filter) {
            if (!pathAndFilterMap.containsKey(filter.getPath())) {
                pathAndFilterMap.put(filter.getPath(), new AndFilterBuilder());
            }
            pathAndFilterMap.get(filter.getPath()).add(filter.getFilterBuilder());
            builder.add(filter.getFilterBuilder());
        }

        public Filter getFilter() {
            if (isCCENestedDiagnosisQuery()) {
                return new Filter(getCCENestedDiagnosisQuery());
            } else {
                return new Filter(getFinalFilter());
            }

        }


        public static final String CCE_DIAGNOSIS_PATH = "d";
        public static final String CCE_REGIMENT_PATH = "d.r";
        public static final String CCE_PLAN_PATH = "d.p";
        public static final String CCE_MEDICAL_PATH = "d.m";

        private FilterBuilder getFinalFilter() {
            if (!pathAndFilterMap.containsKey(Filter.DEFAULT_PATH)) {
                pathAndFilterMap.put(Filter.DEFAULT_PATH, new AndFilterBuilder());
            }
            if (pathAndFilterMap.size() == 1) {
                return pathAndFilterMap.values().iterator().next();
            }

            AndFilterBuilder defaultFilter = pathAndFilterMap.get(Filter.DEFAULT_PATH);

            for (String path : pathAndFilterMap.keySet()) {
                if (!path.equals(Filter.DEFAULT_PATH)) { //nested Filter
                    defaultFilter.add(new NestedFilterBuilder(path, pathAndFilterMap.get(path)));
                }
            }
            return defaultFilter;
        }

        private boolean isCCENestedDiagnosisQuery(){
            if (pathAndFilterMap.containsKey(CCE_DIAGNOSIS_PATH) || pathAndFilterMap.containsKey(CCE_REGIMENT_PATH) || pathAndFilterMap.containsKey(CCE_PLAN_PATH) || pathAndFilterMap.containsKey(CCE_MEDICAL_PATH)) {
                return true;
            }
            return false;
        }

        private FilterBuilder getCCENestedDiagnosisQuery() {
            NestedFilterBuilder regFilter, planFilter, medicalFilter = null;
            if (!pathAndFilterMap.containsKey(CCE_DIAGNOSIS_PATH)) {
                pathAndFilterMap.put(CCE_DIAGNOSIS_PATH, new AndFilterBuilder());
            }
            if (pathAndFilterMap.containsKey(CCE_REGIMENT_PATH)) {
                regFilter = new NestedFilterBuilder(CCE_REGIMENT_PATH, pathAndFilterMap.get(CCE_REGIMENT_PATH));
                pathAndFilterMap.get(CCE_DIAGNOSIS_PATH).add(regFilter);
            }
            if (pathAndFilterMap.containsKey(CCE_PLAN_PATH)) {
                planFilter = new NestedFilterBuilder(CCE_PLAN_PATH, pathAndFilterMap.get(CCE_PLAN_PATH));
                pathAndFilterMap.get(CCE_DIAGNOSIS_PATH).add(planFilter);
            }
            if (pathAndFilterMap.containsKey(CCE_MEDICAL_PATH)) {
                medicalFilter = new NestedFilterBuilder(CCE_MEDICAL_PATH, pathAndFilterMap.get(CCE_MEDICAL_PATH));
                pathAndFilterMap.get(CCE_DIAGNOSIS_PATH).add(medicalFilter);
            }
            return new NestedFilterBuilder(CCE_DIAGNOSIS_PATH, pathAndFilterMap.get(CCE_DIAGNOSIS_PATH));
        }
    }
}
