package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import org.elasticsearch.index.query.FilterBuilder;

import static org.elasticsearch.index.query.FilterBuilders.termFilter;

public class ContainFilter implements SimpleFilter {
    public static final ContainFilter INSTANCE = new ContainFilter();

    private ContainFilter() {
    }

    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        if (value instanceof String)
            value = ((String) value).toLowerCase();
        return termFilter(field + ".contains", value);
    }
}
