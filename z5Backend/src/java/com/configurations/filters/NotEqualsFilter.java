package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import com.configurations.utils.SearchUtils;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import zakipointCore.commonUtils.QueryUtils;

import java.util.List;

public class NotEqualsFilter  implements SimpleFilter {
    public static final NotEqualsFilter INSTANCE = new NotEqualsFilter();

    private NotEqualsFilter() {
    }

    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        FilterBuilder filterBuilder;
        String queryField = field;
        if (fieldConfig != null && fieldConfig.sortable) {
            queryField = field + ".sort";
        }
        if (QueryUtils.isArray(value.toString())) {
            List<String> andListFromArray = SearchUtils.getSplittedListFromArray(value.toString());
            if (andListFromArray.get(0).equalsIgnoreCase("and")) {
                AndFilterBuilder andFilter = new AndFilterBuilder();
                for (int index = 1; index < andListFromArray.size(); index++) {
                    FilterBuilder termsFilter = FilterBuilders.termsFilter(queryField, andListFromArray.get(index).split(","));
                    if (termsFilter != null) {
                        andFilter.add(termsFilter);
                    }
                }
                filterBuilder = andFilter;
            } else {
                List<String> listFromArray = SearchUtils.getListFromArray(value.toString());
                filterBuilder = FilterBuilders.termsFilter(queryField, listFromArray);
            }
        } else {
            filterBuilder = FilterBuilders.termFilter(queryField, value);
        }

        return FilterBuilders.notFilter(filterBuilder);
    }
}
