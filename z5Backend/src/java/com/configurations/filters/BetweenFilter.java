package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import com.configurations.utils.VisitAdmission;
import com.configurations.utils.SearchUtils;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.NestedFilterBuilder;

import java.util.List;

public class BetweenFilter implements SimpleFilter {
    public static final BetweenFilter INSTANCE = new BetweenFilter();

    private BetweenFilter() {
    }

    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        List<String> list = SearchUtils.getListFromArray(value.toString());

        for(int i=0;i<list.size();i++){
            if(list.get(i).equalsIgnoreCase("null")) list.set(i,null);
        }
        if (list.size() != 2) {
            throw new IllegalArgumentException("Expecting an array in between filter . " + value);
        }

        if (field.equals("eligibleDates") || field.equals("eligibleDatesDental") || field.equals("eligibleDatesVision")) {
            return getEligibleDatesFilter(field, list);
        }

        if (field.equals(VisitAdmission.TOTAL_PLAN_AND_MEMBER_PAID)) {
            return getTotalPlanAndMemberPaidFilter(field, list);
        }

        // This case is only applicable for DDHC outcome report drill down.
        if (isDDHCDrillDown(field)) {
            String queryField = getQueryField(field);
            if (queryField.equals("eligibleDates"))
                return getEligibleDatesFilter(queryField, list);

            return new NestedFilterBuilder("hra_history", FilterBuilders.rangeFilter(queryField).from(list.get(0)).to(list.get(1)));
        }

        FilterBuilder filterBuilder = FilterBuilders.rangeFilter(field).from(list.get(0)).to(list.get(1));
        return filterBuilder;
    }

    private FilterBuilder getTotalPlanAndMemberPaidFilter(String field, List<String> list) {
        FilterBuilder filterBuilder = FilterBuilders.rangeFilter(field).from(list.get(0)).to(list.get(1)).includeLower(false);
        return filterBuilder;
    }

    private FilterBuilder getEligibleDatesFilter(String field, List<String> list) {
        FilterBuilder fromFilterBuilder = FilterBuilders.rangeFilter(field + ".fromDate").lte(list.get(1));
        FilterBuilder toFilterBuilder = FilterBuilders.rangeFilter(field + ".toDate").gte(list.get(0));

        return new NestedFilterBuilder(field, new AndFilterBuilder(fromFilterBuilder, toFilterBuilder));
    }

    private String getQueryField(String field) {
        String replacePattern = "(DDHC_|ddhc_|_reporting|_comparison)";
        String queryField = field.replaceAll(replacePattern, "");
        return queryField;
    }

    private boolean isDDHCDrillDown(String field) {
        String pattern = "(DDHC|ddhc).*(reporting|comparison)";
        if (field.matches(pattern))
            return true;
        return false;
    }
}
