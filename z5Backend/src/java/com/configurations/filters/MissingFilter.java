package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.MissingFilterBuilder;

public class MissingFilter implements SimpleFilter {
    public static final MissingFilter INSTANCE = new MissingFilter();

    private MissingFilter() {
    }

    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {

        MissingFilterBuilder missingFilter = FilterBuilders.missingFilter(field);
        missingFilter.existence(true);
        missingFilter.nullValue(true);
        return missingFilter;
    }
}
