package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import com.configurations.utils.SearchUtils;
import org.elasticsearch.index.query.*;
import zakipointCore.commonUtils.QueryUtils;

import java.util.List;

public class TermFilter implements SimpleFilter {
    public static final TermFilter INSTANCE = new TermFilter();

    private TermFilter() {
    }


    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        FilterBuilder filterBuilder;
        String queryField = field;
        if (fieldConfig != null && fieldConfig.sortable) {
            queryField = field + ".sort";
        }
        // This is special case to check the exists/missing diagnosis
        if (field.equals("cceDiagnosis")) {
            if (value.equals("TRUE"))
                return new ExistsFilterBuilder("d");
            return new MissingFilterBuilder("d");
        }

        if (QueryUtils.isArray(value.toString())) {
            List<String> andListFromArray = SearchUtils.getSplittedListFromArray(value.toString());
            if (andListFromArray.get(0).equalsIgnoreCase("and")) {
                AndFilterBuilder andFilter = new AndFilterBuilder();
                for (int index = 1; index < andListFromArray.size(); index++) {
                    FilterBuilder termsFilter = FilterBuilders.termsFilter(queryField, andListFromArray.get(index).split(","));
                    if (termsFilter != null) {
                        andFilter.add(termsFilter);
                    }
                }
                filterBuilder = andFilter;
            } else {
                List<String> listFromArray = SearchUtils.getListFromArray(value.toString());
                filterBuilder = FilterBuilders.termsFilter(queryField, listFromArray);
            }
        } else if (value.equals("null")) {
            MissingFilterBuilder missingFilter = FilterBuilders.missingFilter(field);
            return missingFilter;
        } else {
            filterBuilder = FilterBuilders.termFilter(queryField, value);
        }
        return filterBuilder;
    }

}
