package com.configurations.filters;

import com.configurations.config.FieldConfig;
import com.configurations.utils.SimpleFilter;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;

public class LessThanFilter implements SimpleFilter {
    public static final LessThanFilter INSTANCE = new LessThanFilter();


    private LessThanFilter() {
    }

    public FilterBuilder getFilter(String field, Object value, FieldConfig fieldConfig) {
        if(value.toString().equalsIgnoreCase("null")) value=null;
        FilterBuilder filterBuilder = FilterBuilders.rangeFilter(field).lt(value);
        return filterBuilder;
    }

}
