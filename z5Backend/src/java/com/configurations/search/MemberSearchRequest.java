package com.configurations.search;

import java.util.HashMap;

public class MemberSearchRequest {
    public HashMap<String, String> requestParameters;       //to hold the request params from the front end request
    public Boolean printQuery = false;

    public void initialize() {
        if (requestParameters.containsKey("printQuery") && requestParameters.get("printQuery").equals("true")) {
            printQuery = true;
        }
        requestParameters.put("msRequest" , "true");
    }

    public boolean hasParameter(String param) {
        return requestParameters.containsKey(param) && !requestParameters.get(param).isEmpty();
    }

    public String  get(String param) {
        return requestParameters.get(param);
    }

    public MemberSearchRequest() {
    }}
