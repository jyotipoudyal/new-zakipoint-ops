package com.configurations.config

class FieldConfig {
    public String field
    public String displayName
    public String type
    public String fieldGroup
    public String displayClass
    public String path
    public String replace
    public String phi
    public boolean customDate
    public boolean sortable 
    public String toLowerCase
}
