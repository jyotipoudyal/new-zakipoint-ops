package com.configurations.config;

import java.util.HashMap;
import java.util.Map;

public class FieldConfigurations {
    private static final Map<String, FieldConfiguration> cache = new HashMap<String, FieldConfiguration>();


    public static FieldConfiguration fromTableCode(String tableCode) {
        if (!cache.containsKey(tableCode))
            cache.put(tableCode, new FieldConfiguration(tableCode));

        return cache.get(tableCode);
    }
}
