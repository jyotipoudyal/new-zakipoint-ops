package com.configurations.config;

import com.configurations.utils.ServerConfig;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ElasticSearchConnectionManager {
    public static boolean isForTest=false;
    static final SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    static Map<String,Client> clusterClients=new HashMap<String, Client>();


    public static Client getClient(ClusterConfig clusterConfig) {
        return getClientWithDynamicCluster(clusterConfig);
    }

    private static Client getInstance(ClusterConfig clusterConfig) {
        if(clusterConfig.getNodes().size()==0){
            throw new RuntimeException("No servers specified. Refer to ServersConfig.");
        }
        String esClusterName = !clusterConfig.getClusterName().isEmpty()?clusterConfig.getClusterName():ClusterConfig.ES_DEFAULT_CLUSTER_NAME;


        System.out.println(dt.format(new Date()) + " ElasticSearchConnectionManager.getInstance: Connection attempt to cluster: "+ esClusterName);

        Settings settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", esClusterName).put("client.transport.ping_timeout","30s").build();
        TransportClient client = new TransportClient(settings);
        for(ServerConfig serverConfig:clusterConfig.getNodes().values()){
            System.out.println(dt.format(new Date()) + " ElasticSearchConnectionManager.getInstance: Adding cluster node: "+serverConfig.getName()+" HostName: "+serverConfig.getHostname()+" Port: "+serverConfig.getPort());
            client.addTransportAddress(new InetSocketTransportAddress(serverConfig.getHostname(),serverConfig.getPort()));
        }
        return client;
    }

    public static synchronized Client getClientWithDynamicCluster(ClusterConfig clusterConfig) {
        String clusterName=clusterConfig.getClusterName();
        if (!clusterClients.containsKey(clusterName)||clusterClients.get(clusterName)==null) {

            clusterClients.put(clusterName, getInstance(clusterConfig));
        }
        return clusterClients.get(clusterName);
    }
}
