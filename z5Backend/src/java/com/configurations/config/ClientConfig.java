package com.configurations.config;

import java.util.ArrayList;
import java.util.List;

public class ClientConfig {
    List<String> customCalendarGroup;
    boolean hasCustomCalendarGroup = false;
    List<String> clientWiseLOA ;
    String clusterConfigFile ;

    public ClientConfig() {
        this.customCalendarGroup = new ArrayList<String>();
        this.clientWiseLOA = new ArrayList<String>();
    }

    public List<String> getCustomCalendarGroup() {
        return customCalendarGroup;
    }

    public boolean hasCustomCalendarGroup() {
        return hasCustomCalendarGroup;
    }
    public boolean hasClusterConfig(){
        return clusterConfigFile!=null && !clusterConfigFile.isEmpty();
    }

    public String getClusterConfigFile() {
        return clusterConfigFile;
    }

    public List<String> getClientWiseLOA() {
        return clientWiseLOA;
    }

    public static class Builder {
        ClientConfig clientConfig ;

        public Builder() {
            this.clientConfig = new ClientConfig();
        }
        public Builder(ClientConfig clientConfig1) {
            this.clientConfig = clientConfig1;
        }

        public Builder addCustomCalanderGroup (String groupId){
            clientConfig.customCalendarGroup.add(groupId);
            return this;
        }

        public Builder customCalendarGroup(boolean yes) {
            clientConfig.hasCustomCalendarGroup = yes;
            return this;
        }
        public Builder addClusterConfig(String yes) {
            clientConfig.clusterConfigFile = yes;
            return this;
        }
        public Builder addClientWiseLOA (List<String> loa){
            clientConfig.clientWiseLOA = loa;
            return this;
        }


        public ClientConfig build() {
            return clientConfig;
        }
    }

    @Override
    public String toString() {
        String cc = "";
        cc+=  "customCalendarGroup = " + customCalendarGroup ;
        cc+=  "clientWiseLOA = " + clientWiseLOA ;
        cc+=  "hasCustomCalendarGroup = " + hasCustomCalendarGroup ;
        return cc;
    }
}
