package com.configurations.config

import com.configurations.utils.ServerConfig
import grails.util.Holders
import org.codehaus.groovy.grails.commons.GrailsApplication
import zakipointCore.commonUtils.RequestUtils
import zakipointCore.commonUtils.SpringBeanUtils

class AbstractConfigurationManager {
    def qualifier="dev_"
    static Map<String,ClusterConfig> clusterConfigs = [:]

    public ClusterConfig getClusterConfig(String server) {
        if(clusterConfigs.containsKey(server)) return clusterConfigs.get(server)
        else{
            def context = SpringBeanUtils.getApplicationContext()
            def config = context.getBean(GrailsApplication).config[server]
            ClusterConfig clusterConfig = new ClusterConfig(clusterName: config.clusterName)
            clusterConfig.nodes = [:]
            config.each { k1, v1 ->
                if (k1.contains("node")) {
                    ServerConfig serverConfig=new ServerConfig()
                    v1.each { k, v ->
                        serverConfig."$k" = v
                    }
                    println serverConfig.name
                    clusterConfig.nodes.put(serverConfig.name, serverConfig)
                }
            }
            clusterConfigs.put(server , clusterConfig)
            return clusterConfig
        }
    }

    //returns qualifier
    public static getConfigQualifier(){
        def configurationManager=(AbstractConfigurationManager)Holders.applicationContext.getBean('configurationManager')
        return configurationManager.qualifier
    }

    public Map<String,NewTableConfig> getNewTableConfig(String[] tableScriptFiles) {

        Map<String, NewTableConfig> map = [:]
        def NewTablesBuilder builder;

        for (String scriptFiles : tableScriptFiles) {
            builder = new NewTablesBuilder()
            def script = getScript(scriptFiles)
            script.run()
            def tables = script.tables
            tables.delegate = builder
            tables()
            map.putAll(builder.outputTables)
        }
        map
    }

    def setIndex(requestMap){
        def clientId=getClientId(requestMap)

        if (clientId instanceof String)clientId = getCorrectedId(clientId)

        def indexName
        if(clientId){
            def table=getTable(requestMap)
            indexName=clientId

            if(qualifier!=null) indexName = qualifier+indexName
            requestMap.put("index_name",indexName)
        }
        else{
            throw new RuntimeException("clientId not defined")
        }
    }

    private Object getScript(String scriptFiles) {
        def script

        try {
            script = ConfigurationManager.classLoader.loadClass(scriptFiles).newInstance()

        } catch (ClassNotFoundException e) {
            script = ConfigurationManager.classLoader.loadClass("dastables." + scriptFiles).newInstance()

        }
        script
    }


    def getClientId(requestMap){
        requestMap!=null?requestMap.get("clientId"):""
    }

    def getTable(requestMap){
        requestMap!=null?requestMap.get("table"):""
    }

    private String getCorrectedId(String clientId) {
        if (clientId.contains("[")) {
            String[] temp = RequestUtils.getArrayRequest(clientId)
            clientId = temp[0]
        }
        RequestUtils.getClientId(clientId)
    }
}
