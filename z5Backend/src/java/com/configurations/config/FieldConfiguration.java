package com.configurations.config;

import com.configurations.utils.SearchUtils;

import java.util.*;

public class FieldConfiguration {
    private final Map<String, FieldConfig> displayNameToFieldConfig = new HashMap<String, FieldConfig>();
    private final Map<String, FieldConfig> fieldNameToFieldConfig = new HashMap<String, FieldConfig>();
    private final Map<String, List<FieldConfig>> pathToFieldConfigList=new HashMap<String,List<FieldConfig>>();
    private final List<String> defaultFields = new ArrayList<String>();
    private final List<String> tables = new ArrayList<String>();
    private final List<String> fields = new ArrayList<String>();
    private final List<String> nonRequiredTables= Arrays.asList("visitadm");

    private static final Map<String, String> nestedFieldMapping = new HashMap<String, String>();
    static {
        nestedFieldMapping.put("currentStatus", "memberStatus");
        nestedFieldMapping.put("medPaidAmount", "member_amount");
        nestedFieldMapping.put("rxPaidAmount", "member_amount");
        nestedFieldMapping.put("metricCompliance", "qm");
        nestedFieldMapping.put("totalProfessionalPlanPaid", "visitAmounts");
        nestedFieldMapping.put("totalFacilityPlanPaid", "visitAmounts");
        nestedFieldMapping.put("premiumAmount", "premiumAmount");

    }

    public FieldConfiguration(String tableCodes) {
        if(!nonRequiredTables.contains(tableCodes)){
        for (String tableCode : SearchUtils.getListFromArray(tableCodes)) {
            final NewTableConfig tableConfig = ConfigurationFactory.newTableConfig.get(tableCode);
            tables.add(tableConfig.tableName);
            List<String> defaultColumns = ConfigurationFactory.newTableConfig.get(tableCode).defaultColumns;

            for (FieldConfig fieldConfig : tableConfig.fields.values()) {
                displayNameToFieldConfig.put(fieldConfig.displayName, fieldConfig);
                fieldNameToFieldConfig.put(fieldConfig.field, fieldConfig);
                fields.add(fieldConfig.field);
                if (defaultColumns.contains(fieldConfig.fieldGroup)) {
                    defaultFields.add(fieldConfig.field);
                }
                if (fieldConfig.path != null && !fieldConfig.path.isEmpty()) {
                    manageGroupingWithPath(fieldConfig.path);
                    pathToFieldConfigList.get(fieldConfig.path).add(fieldConfig);
                }
            }
            }
        }
    }

    public FieldConfig fromDisplayName(String displayName) {
        return displayNameToFieldConfig.get(displayName);
    }

    public FieldConfig fromFieldName(String fieldName) {
        return fieldNameToFieldConfig.get(fieldName);
    }

    public List<FieldConfig> fromPathToFieldConfigList(String path) {
        return pathToFieldConfigList.get(path);
    }

    public String[] defaultColumns(String defaultColumns) {
        if (defaultColumns == null || defaultColumns.isEmpty()) {
            return defaultFields.toArray(new String[]{});
        }
        Set<String> fields = new HashSet<String>();

        List<String> defaultColumnsList = SearchUtils.getListFromArray(defaultColumns);

        for (FieldConfig fieldConfig : fieldNameToFieldConfig.values()) {
            if (defaultColumnsList.contains(fieldConfig.fieldGroup)) {
                fields.add(fieldConfig.field);
            }
        }
        return fields.toArray(new String[]{});
    }
    public List<String> uniqueKey(String tableCode) {
        return ConfigurationFactory.newTableConfig.get(tableCode).uniqueKey;
    }

    public String getDocIdFromTableCode(String tableCode) {
        return ConfigurationFactory.newTableConfig.get(tableCode).docId;
    }

    public String[] tables() {
        return tables.toArray(new String[tables.size()]);
    }

    private void manageGroupingWithPath(String path){
        if(pathToFieldConfigList.get(path)==null) {
            pathToFieldConfigList.put(path, new ArrayList<FieldConfig>());
        }
    }

    public List<String> getFields(){

        return fields;
    }
    public String[] getFields(String fields) {
        List<String> defaultFields = SearchUtils.getListFromArray(fields);
        return getFields(defaultFields);
    }
    public String[] getFields(List<String> defaultFields) {

        Set<String> fieldsSet = new HashSet<String>();
        for (String field : defaultFields) {
            if (displayNameToFieldConfig.containsKey(field)) {
                //fieldsSet.add(displayNameToFieldConfig.get(field).field);
                String value=null;
                if(displayNameToFieldConfig.get(field).path!=null && nestedFieldMapping.containsKey(field)) {
                    value = nestedFieldMapping.get(field);
                }else{
                    value=displayNameToFieldConfig.get(field).field;
                }
                fieldsSet.add(value);

            }
//            else if(MaraConfigurationUtils.riskScoreFieldsRawGroup.contains(field)){
//                fieldsSet.add(field);
//            }
        }
        return fieldsSet.toArray(new String[]{});
    }
}
