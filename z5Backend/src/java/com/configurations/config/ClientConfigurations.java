package com.configurations.config;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ClientConfigurations {
    static Map<String,ClientConfig> clientConfigMap = new HashMap<String,ClientConfig>() ;
    public static Collection<String> getClients (){
        return clientConfigMap.keySet();
    }


    private ClientConfigurations() {
    }
    public static void print(){
        System.out.println("clientConfigMap = " + clientConfigMap);
    }

    public static ClientConfig getInstance(String clientId){
        return clientConfigMap.get(clientId);
    }
    public static ClientConfig addInstance(String clientId, ClientConfig clientConfig){
        clientConfigMap.put(clientId,clientConfig);
        return getInstance(clientId);
    }
    public static ClientConfig addInstance(String clientId, ClientConfig.Builder ccBuilder){
        return addInstance(clientId, ccBuilder.build());
    }
}
