package com.configurations.config;

import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.common.hppc.cursors.ObjectCursor;
import org.elasticsearch.search.SearchHit;

import java.util.*;

public class ConfigurationFactory {
    private static ClusterConfig clusterConfig = null;

    public static Map<String, NewTableConfig> newTableConfig = null;     //added for member search
    public static List<String> clientList = new ArrayList<>();
//    public static Map<String, BenchmarkType> benchmark = Collections.synchronizedMap(new HashMap<>());
    public static Map<String, String> clusterConfigFiles = new HashMap<>();
    public static Map<String, String> clientCycleDateMap = new HashMap<>();
    public static Map<String, Object> clientJsonMap = new HashMap<>();


    public static List<String> commonLoaFields = new ArrayList<>(Arrays.asList("coverageTypeDesc", "contractType", "memberCity", "memberCounty", "plan", "group", "division", "planType", "relationship", "carrier",
            "businessUnit", "udf16", "udf17", "udf18", "udf19", "udf20", "udf21", "udf22", "udf23", "udf24", "udf25", "udf26", "udf27", "udf28", "udf29", "udf30", "memberGender"));

    public static Map<String, List<String>> clientWiseLOA = new HashMap<String, List<String>>() {{
    }};


    @SuppressWarnings("unchecked")
    public static void init() {
    }

    public static void init(AbstractConfigurationManager configurationManager, String[] tableScriptFiles) {
        System.out.println("Calling Configuration init");
        clientList.clear();
        initClusterConfig(configurationManager);
        getClientLOA(ConfigFileName.getFileName(), configurationManager);

        newTableConfig = configurationManager.getNewTableConfig(tableScriptFiles);  //added for member search
        printTables();

    }

    public static void initClusterConfig(AbstractConfigurationManager configurationManager) {
        clusterConfig = configurationManager.getClusterConfig(ConfigFileName.getFileName());
        System.out.println("Cluster configuration file name => " + ConfigFileName.getFileName());
    }


    private static void printTables() {
        System.out.println("Total tables loaded= " + newTableConfig.size());
        String memberSearchTables = "Client Wise Tables--> ";
        String otherTables = "Common Tables--> ";
        for (String tableName : newTableConfig.keySet()) {
            if (tableName.contains("ms"))
                memberSearchTables += tableName + " ,";
            else
                otherTables += tableName + " ,";

        }

        System.out.println("--------------------MemberSearch Tables for Clients----------------------------------------------");
        System.out.print(memberSearchTables.substring(0, memberSearchTables.length() - 1));
        System.out.println("--------------------Other tables------------------------------------------------------------------");
        System.out.print(otherTables.substring(0, otherTables.length() - 1));
        System.out.println("--------------------------------------------------------------------------------------------------");
        System.out.println("--------------------------------------------------------------------------------------------------");

    }

    public static void getClientLOA(String clusterConfigName, AbstractConfigurationManager configurationManager) {
        ClusterConfig clusterConfig = configurationManager.getClusterConfig(clusterConfigName);
        Client client = ElasticSearchConnectionManager.getClient(clusterConfig);
        List<String> currentAlias = new ArrayList<>();
        String qualifier = (String) configurationManager.getQualifier();
        if (!ElasticSearchConnectionManager.isForTest) {

            GetAliasesResponse response = client.admin().indices().getAliases(new GetAliasesRequest("*")).actionGet();//.cluster().state(new ClusterStateRequest()).actionGet().getState().getMetaData().aliases().keySet();
            Map<String, String> aliasMap = new SummableMap<>();
            Iterator<ObjectCursor<String>> iter = response.getAliases().keys().iterator();
            while (iter.hasNext()) {
                String key = iter.next().value;
                List<AliasMetaData> aliases = response.getAliases().get(key);
                for (AliasMetaData alias : aliases) {
                    if (alias.getAlias().contains(qualifier)) {
                        boolean qualify = false;
                        String clientId = alias.getAlias().replace(qualifier, "");
                        if (qualifier.equals("")) {
                            String[] qualifiers = clientId.split("_");
                            if (qualifiers.length > 1)
                                qualify = true;
                        }
                        if (qualify) {
                            continue;
                        }
                        String configName = clusterConfigFiles.get(clientId) != null ? clusterConfigFiles.get(clientId) : clusterConfigName;
                        if (configName.equals(clusterConfigName)) {
                            aliasMap.put(key, alias.getAlias());
                        }
                    }
                }
            }
            String[] array = aliasMap.keySet().toArray(new String[aliasMap.keySet().size()]);
            SearchResponse searchResponse = ElasticSearchConnectionManager.getClient(clusterConfig).prepareSearch(array)
                    .setTypes("ClientConfig")
                    .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setFrom(0).setSize(10000).setExplain(false)
                    .execute()
                    .actionGet();
            Iterator<SearchHit> itr = searchResponse.getHits().iterator();
            while (itr.hasNext()) {
                SearchHit hit = itr.next();
                String[] dataList = aliasMap.get(hit.getIndex()).split(";");
                Map<String, Object> sourceMap = hit.sourceAsMap();
                List<String> dummy = new ArrayList<>(Arrays.asList("coverageTypeDesc", "contractType", "memberCounty", "plan", "group", "division", "planType", "relationship", "carrier",
                        "businessUnit"));

                if (sourceMap.containsKey("switchable_fields")) {
                    String[] temp = hit.getSource().get("switchable_fields").toString().split(";");
                    for (String s : temp) {
                        dummy.add(s.trim().split(":")[2]);
                    }
                }

                if (dataList != null) {
                    for (String alias : dataList) {
                        if (!clientList.contains(alias)) {
                            currentAlias.add(alias);
                            ClientConfig.Builder clientConfigBuilder = new ClientConfig.Builder();
                            for (String sourceKey : sourceMap.keySet()) {
                                ClientConfigParser.parse(sourceKey, sourceMap.get(sourceKey), clientConfigBuilder);
                                ClientConfigurations.addInstance(alias, clientConfigBuilder);
                            }

                            clientList.add(alias);
                            clientWiseLOA.put(alias, Collections.unmodifiableList(dummy));
                            clientJsonMap.put(alias, sourceMap);
                            clientCycleDateMap.put(alias, (String) sourceMap.get("cycleEndDate"));
                            if(!qualifier.equals("")) {
                                String pseudoAlias= alias.replace(qualifier,"");
                                clientList.add(pseudoAlias);
                                clientWiseLOA.put(pseudoAlias, Collections.unmodifiableList(dummy));
                                clientJsonMap.put(pseudoAlias, sourceMap);
                                clientCycleDateMap.put(pseudoAlias, (String) sourceMap.get("cycleEndDate"));
                            }
                        }
                    }
                }
            }
        }
    }



//    public static Boolean getBenchmarkValue(String clientId, String benchmarkType) {
//        if (Environment.getCurrentEnvironment().equals(Environment.DEVELOPMENT)) {
//            return true;
//        }
//        clientId = clientId.replace("QA_","");
//        if (benchmark.containsKey(clientId)) {
//            BenchmarkType requestBenchmarkType = BenchmarkType.valueById(benchmarkType);
//            BenchmarkType clientBenchmarkType = benchmark.get(clientId);
//            if (requestBenchmarkType == null || clientBenchmarkType == null) {
//                return false;
//            }
//            return requestBenchmarkType.get_order() <= clientBenchmarkType.get_order();
//        }
//        return false;
//    }
}
