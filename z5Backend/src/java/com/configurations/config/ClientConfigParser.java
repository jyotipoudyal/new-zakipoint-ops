package com.configurations.config;

import org.elasticsearch.common.collect.MapBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ClientConfigParser {
    static Map<String,Parser> parserMap = new MapBuilder<String,Parser>()
            .put("switchable_fields",new SwitchableFiledParser())
            .put("custom_calendar_group",new CalendarGroupParser())
            .map();
    public static interface Parser {
        public void parse(Object value , ClientConfig.Builder clientConfig);
    }

    public static class CalendarGroupParser implements Parser{
        @Override
        public void parse(Object value, ClientConfig.Builder clientConfig) {
            System.out.println("calendar__++++" + value);
            clientConfig.customCalendarGroup(Boolean.parseBoolean(value.toString()));
        }
    }
    public static class SwitchableFiledParser implements  Parser {
        @Override
        public void parse(Object value, ClientConfig.Builder clientConfig) {
            List<String> dummy = new ArrayList<String>(Arrays.asList("division", "planType", "relationship", "carrier", "group", "plan", "businessUnit"));
            String[] temp = value.toString().split(";");
            for (String s : temp) {
                dummy.add(s.trim().split(":")[2]);
            }
            clientConfig.addClientWiseLOA(dummy);
        }
    }

    public static void parse (String field , Object value , ClientConfig.Builder clientConfig){
        if(parserMap.containsKey(field)){
            parserMap.get(field).parse(value,clientConfig);
        }
    }
}
