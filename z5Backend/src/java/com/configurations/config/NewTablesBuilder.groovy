package com.configurations.config

class NewTablesBuilder extends BuilderSupport {
    def Map<String, NewTableConfig> outputTables = [:]
    private NewTableConfig _currentTable
    private String _currentFieldFamily
    private static final SEPARATOR = "~!"
    private static final Map<String, String> tables = ['ms': 'MemberSearch']

    /**
     * {@inheritDoc}
     * @see groovy.util.BuilderSupport#createNode(java.lang.Object)
     */
    @Override
    protected Object createNode(Object name) {
        switch (name) {
            case "table":
                _currentTable = new NewTableConfig()
                _currentTable.fields = new HashMap<String, FieldConfig>()
                return (name)
            default:
                throw new IllegalArgumentException("Cannot create empty node with name '$name'")
        }
    }

    @Override
    protected Object createNode(Object name, Object value) {
        switch (name) {
            case "tableCode":
            case "tableName":
            case "defaultColumns":
            case "uniqueKey":
                _currentTable."$name" = value
                return name
        }

        throw new IllegalArgumentException("Cannot create node with name '$name' and value '$value'")
    }


    @Override
    protected Object createNode(Object name, Map attributes) {
        switch (name) {
            case "field":
                attributes.put('fieldGroup', _currentFieldFamily)
                _currentTable.fields.put(attributes.get("field"), new FieldConfig(attributes))
                return name
            case "fields":
                _currentFieldFamily = attributes.get("family")
                return name
        }
        throw new UnsupportedOperationException()
    }

    @Override
    protected Object createNode(Object name, Map attributes, Object value) {
        throw new UnsupportedOperationException()
    }

    @Override
    protected void setParent(Object parent, Object child) {
        // do nothing
    }

    @Override
    protected void nodeCompleted(Object parent, Object node) {
        if (node == "table") {
            if (tables.keySet().contains(_currentTable.tableCode)) {
                ConfigurationFactory.clientList.each { client ->
                    Map<String, FieldConfig> fields = getFields()
                    NewTableConfig tableConfig = getTableConfig(_currentTable.tableCode, client, fields)
                    populateOutputTables(tableConfig, client)
                }
            }
            outputTables(_currentTable.tableCode, _currentTable)
        }
    }

    private void populateOutputTables(NewTableConfig tableConfig, String client) {
        String tableCode = tableConfig.tableCode + SEPARATOR + client
        tableConfig.tableCode = tableCode
        outputTables(tableCode, tableConfig)
    }

    private void outputTables(String tableCode, NewTableConfig tableConfig) {
        if (outputTables.containsKey(tableCode)) println "===============[" + tableConfig.tableName + "] already exist===============!!!"
        else outputTables.put(tableCode, tableConfig)
    }

    private Map<String, FieldConfig> getFields() {
        Map<String, FieldConfig> fields = new HashMap<String, FieldConfig>();
        for (String key : _currentTable.fields.keySet()) {
            fields.put(key, _currentTable.fields.get(key));
        }
        return fields
    }

    private NewTableConfig getTableConfig(String tableCode, String client, Map<String, FieldConfig> fields) {

        switch (tables.keySet().contains(tableCode)) {
            case true:
                NewTableConfig tableConfig = new NewTableConfig(fields: fields, tableCode: tableCode, tableName: tables.get(tableCode), defaultColumns: new ArrayList<String>(Arrays.asList("basic", "derived")));
                List<String> loaList = ConfigurationFactory.commonLoaFields
                loaList.each { loaField ->
                    if (ConfigurationFactory.clientWiseLOA.get(client).contains(loaField)) {
//                        insertAsLOA(loaField, tableConfig)
                    } else {
                        insertAsSimpleField(loaField, tableConfig)
                    }
                }
                return tableConfig
                break;
        }
    }


    protected void insertAsLOA(String key, NewTableConfig msTableConfig) {
        FieldConfig LOAField = new FieldConfig(field: key, displayName: key, type: "nested", displayClass: "matchedLOA", fieldGroup: "basic")
        msTableConfig.fields.put(key, LOAField)

        FieldConfig LOAId = new FieldConfig(field: key + ".id", displayName: key + "Id", type: "string", fieldGroup: "nested", path: key)
        msTableConfig.fields.put(key + ".id", LOAId)

        FieldConfig LOAName = new FieldConfig(field: key + ".description", displayName: key + "IdName", type: "string", fieldGroup: "nested", path: key)
        msTableConfig.fields.put(key + ".description", LOAName)

        FieldConfig LOAFromDate = new FieldConfig(field: key + ".fromDate", displayName: key + "IdFromDate", type: "Date", fieldGroup: "nested", path: key)
        msTableConfig.fields.put(key + ".fromDate", LOAFromDate)

        FieldConfig LOAToDate = new FieldConfig(field: key + ".toDate", displayName: key + "IdToDate", type: "Date", fieldGroup: "nested", path: key);
        msTableConfig.fields.put(key + ".toDate", LOAToDate)

        FieldConfig LOAEligibilityType = new FieldConfig(field: key + ".eligibilityType", displayName: key + "IdEligibilityType", type: "String", fieldGroup: "nested", path: key);
        msTableConfig.fields.put(key + ".eligibilityType", LOAEligibilityType)
        if (key.equals("relationship")) {
            FieldConfig LOARelationshipClass = new FieldConfig(field: key + ".memberRelationshipClass", displayName: "memberRelationshipClass", type: "String", fieldGroup: "nested", path: key);
            msTableConfig.fields.put(key + ".memberRelationshipClass", LOARelationshipClass)
        }

    }

    protected void insertAsSimpleField(String key, NewTableConfig msTableConfig) {
        FieldConfig LOAId = new FieldConfig(field: key + "Id", displayName: key + "Id", type: "string", fieldGroup: "basic");
        msTableConfig.fields.put(key + "Id", LOAId)

        FieldConfig LOAName = new FieldConfig(field: key + "IdName", displayName: key + "IdName", type: "string", fieldGroup: "basic");
        msTableConfig.fields.put(key + "IdName", LOAName)
    }

    protected void clear(String key, NewTableConfig msTableConfig) {
        msTableConfig.fields.remove(key + "Id")
        msTableConfig.fields.remove(key + "IdName")
        msTableConfig.fields.remove(key + ".id")
        msTableConfig.fields.remove(key + ".description")
        msTableConfig.fields.remove(key + ".fromDate")
        msTableConfig.fields.remove(key + ".toDate")
        msTableConfig.fields.remove(key)
    }
}