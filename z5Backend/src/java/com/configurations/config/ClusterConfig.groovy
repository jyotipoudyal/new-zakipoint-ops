package com.configurations.config

import com.configurations.utils.ServerConfig

class ClusterConfig {
    public static String ES_DEFAULT_CLUSTER_NAME="elasticsearch";
    Map<String,ServerConfig> nodes
    String clusterName
}
