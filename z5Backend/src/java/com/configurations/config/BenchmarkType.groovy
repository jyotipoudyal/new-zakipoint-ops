package com.configurations.config

enum BenchmarkType {
    OFF("Off",1),
    BENCHMARK("Benchmark", 2),
    TRUVEN_BENCHMARK("TruvenBenchmark",3),
    CUSTOM_BENCHMARK("CustomBenchmark", 4);

    String _id;
    int _order;

    BenchmarkType(String id,int order){
        this._id=id;
        this._order=order
    }

    private static Map<String, BenchmarkType> map = new HashMap<String, BenchmarkType>();

    static {
        for (BenchmarkType benchmarkType : values()) {
            map.put(benchmarkType._id, benchmarkType);
        }
    }

    public static BenchmarkType valueById(String id) {
        if(id==null){
            return null;
        }
        return map.get(id);
    }
}
