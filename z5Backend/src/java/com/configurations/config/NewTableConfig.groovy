package com.configurations.config

class NewTableConfig {
        public String tableCode
        public String tableName
        public String docId
        public Map<String, FieldConfig> fields
        public List<String> defaultColumns
        public List<String> uniqueKey
}
