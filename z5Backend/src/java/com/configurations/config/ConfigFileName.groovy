package com.configurations.config


import org.codehaus.groovy.grails.commons.GrailsApplication
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import zakipointCore.commonUtils.SpringBeanUtils

class ConfigFileName {
    public static
    final String SERVER_CONFIG = SpringBeanUtils.getApplicationContext().getBean(GrailsApplication).config.serverConfig
    public static
    final String SERVER_CONFIG_BACKUP = SpringBeanUtils.getApplicationContext().getBean(GrailsApplication).config.serverConfigBackup

    public static void setFileName(String file) {
        def context = SpringBeanUtils.getApplicationContext()
        context.getBean(GrailsApplication).config.serverConfig = file
    }

    public static String getFileName() {
        String clientId;
        try{
            def clientIdMap =
                    ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                            .getRequest().getParameterMap();
            clientId = clientIdMap.containsKey("indexData") ? clientIdMap.get("indexData")[0] :
                    clientIdMap.get("clientId")[0];
            clientId = clientId.replace("QA_", "")
        }
        catch (IllegalStateException | NullPointerException ex){
            clientId = ""
        }
        return getFileNameClient(clientId)
    }

    public static String getFileNameClient(String clientId) {
        clientId = clientId.replace("QA_", "")
        if(!clientId.isEmpty() && ConfigurationFactory.clusterConfigFiles.containsKey(clientId)){
            return ConfigurationFactory.clusterConfigFiles.get(clientId)
        }else {
            def context = SpringBeanUtils.getApplicationContext()
            return context.getBean(GrailsApplication).config.serverConfig
        }
    }



    public static String swapCluster(String currentCluster) {
        String targetCluster;
        if (currentCluster.equals(SERVER_CONFIG)) targetCluster = SERVER_CONFIG_BACKUP
        else targetCluster = SERVER_CONFIG;
        return targetCluster;
    }

}
