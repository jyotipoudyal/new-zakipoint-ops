package com.configurations.parser;

import com.configurations.filters.BooleanFilter;
import com.configurations.filters.Filter;
import com.configurations.search.MemberSearchRequest;
import com.configurations.config.FieldConfigurations;
import com.configurations.filters.BooleanFilters;
import com.configurations.utils.SimpleFilters;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.MatchAllFilterBuilder;
import zakipointCore.commonUtils.LevelUtils;

import java.util.HashMap;

public class JsonQueryParser implements AbstractQueryParser{
    String tableCodes;

    public JsonQueryParser(String tableCodes) {
        this.tableCodes = tableCodes;
    }

    public FilterBuilder getFilter(MemberSearchRequest request) {
        FilterBuilder queryFilter=getFilterForQuery(request,"query") !=null? getFilterForQuery(request,"query"): new MatchAllFilterBuilder();

        FilterBuilder finalFilter=null;
        if(hasParentTable(request)){
            finalFilter = getParentChildFilter(queryFilter, request, "parentTable", "parentQuery");
        }else if (hasChildTable(request)) {
            finalFilter = getParentChildFilter(queryFilter, request, "childTable", "childQuery");
        } else {
            finalFilter = FilterBuilders.andFilter(queryFilter);
        }
        return finalFilter;
    }

    private boolean hasParentTable(MemberSearchRequest request) {
        return request.hasParameter("parentTable");
    }
    private boolean hasChildTable(MemberSearchRequest request) {
        return request.hasParameter("childTable");
    }


    private FilterBuilder getFilterForQuery(MemberSearchRequest request,String queryName) {
        String query = request.requestParameters.get(queryName);

        if (query == null) return null;

        JSONObject jsonQuery = (JSONObject) grails.converters.JSON.parse(query);

        if(isEmptyQuery(jsonQuery))    {
            return null;
        }
        return getFilter(jsonQuery, LevelUtils.getClientId(request.requestParameters)).getFilterBuilder();
    }

    public static boolean isEmptyQuery(JSONObject query) {
        JSONArray jsonArray= (JSONArray) query.get("and");
        if (jsonArray.size()==0)
            return true;

        return false;
    }

    public Filter getFilter(JSONObject jsonObject, String clientId) {
        if (isSimpleExpression(jsonObject)) {
            return SimpleFilters.getFilter(jsonObject, tableCodes, clientId);
        }
        else {
            String filterType = getKey(jsonObject);
            BooleanFilter booleanFilter = BooleanFilters.getInstance(filterType);
            JSONArray innerJson = (JSONArray) jsonObject.get(filterType);

            for (Object object : innerJson) {
                Filter filter = getFilter((JSONObject) object, clientId);
                if (filter != null)
                    booleanFilter.add(filter);
            }
            return booleanFilter.getFilter();
        }
    }

    private static boolean isSimpleExpression(JSONObject jsonObject) {
        return !isBooleanExpression(jsonObject);
    }

    public static boolean isBooleanExpression(JSONObject jsonObject) {

        String key = getKey(jsonObject);
        if (key.equals("or") || key.equals("and") || key.equals("nested") || key.equals("not")) {
            return true;
        }
        return false;
    }

    public static String getKey(JSONObject jsonObject) {
        if (jsonObject.keySet().size() > 1) {
            throw new RuntimeException("Unexpected size of jsonObject in ctor of BooleanFilter . JsonObject" + jsonObject);
        }
        return jsonObject.keys().next().toString();
    }

    public static boolean isValidField(Object o) {

        if (isBooleanExpression((JSONObject) o)) {
            return true;
        }

        return true;
    }

    private FilterBuilder getParentChildFilter( FilterBuilder queryFilter, MemberSearchRequest request, String pcTable, String pcQuery){
        HashMap<String,String> requestMap= (HashMap<String, String>) request.requestParameters.clone();
        String parentTable=request.requestParameters.get(pcTable);
        String query=request.requestParameters.get(pcQuery);
        request.requestParameters.remove(pcTable);
        request.requestParameters.remove(pcQuery);

        request.requestParameters.put("query",query);
        FilterBuilder parentFilter=new JsonQueryParser(parentTable).getFilter(request);
        AndFilterBuilder finalFilter = FilterBuilders.andFilter().add(queryFilter);
        if(parentFilter!=null){
            //  pcFilter= FilterBuilders.andFilter();
            String[] tables= FieldConfigurations.fromTableCode(parentTable).tables();
            finalFilter= pcTable.equals("childTable") ? finalFilter.add(FilterBuilders.hasChildFilter(tables[0], parentFilter)):finalFilter.add(FilterBuilders.hasParentFilter(tables[0], parentFilter));
            // pcFilter.add(queryFilter);
            //finalFilter=pcFilter;
        }
        request.requestParameters=requestMap;
        return finalFilter;
    }
}
