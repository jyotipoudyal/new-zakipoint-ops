package com.configurations.parser;

import com.configurations.search.MemberSearchRequest;
import org.elasticsearch.index.query.FilterBuilder;

public interface AbstractQueryParser {
    public FilterBuilder getFilter(MemberSearchRequest request);
}