package com.configurations;

import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.Map;

public interface ResponseFormatter {
        public JSONObject format(JSONObject dasResponse, Map<String,String> requestMap);
    }
