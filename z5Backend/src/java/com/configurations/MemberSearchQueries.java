package com.configurations;

import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;

public class MemberSearchQueries {


    public static FilterBuilder eligibleRelationshipBuilder(String month){
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.rangeFilter("relationship.fromDate").lte(month));
        andFilterBuilder.add(FilterBuilders.rangeFilter("relationship.toDate").gte(month));

        andFilterBuilder.add(FilterBuilders.termsFilter("relationship.eligibilityType","medical"));
        andFilterBuilder.add(FilterBuilders.termsFilter("relationship.memberRelationshipClass","Employee"));

        return FilterBuilders.nestedFilter("relationship",andFilterBuilder);
    }
}
