package com.configurations;

import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public interface XContentWriter {

    public static class Params {
        public Map<String,Object> map;
        public Params() {
            map=new HashMap<String, Object>();
        }

        public Params(Map<String,Object> n) {
            map=n;
        }
        public boolean contains(String key){
            return map.containsKey(key);
        }
        public <T> T get(String key){
            return contains(key) ?  (T)map.get(key) : null;
        }
        public Object getRaw(String key){
            return map.get(key);
        }
        public Double getDouble(String key){
            return (Double) map.get(key);
        }
        public Object put(String key,Object val){
            return map.put(key, val);
        }
        public Object remove(String key){
            return map.remove(key);
        }

        @Override
        protected Params clone() {
            return new Params(new HashMap<String, Object>(map));
        }

        @Override
        public String toString() {
            return map.toString();
        }
    }
    public static final Params EMPTY_PARAMS=new Params();

    /**
     * This method is called to write json to builder,
     * we provide params and json is written
     *
     * @param builder where json is appended
     * @param params parameters for building json
     *
     * */
    void toXContent(XContentBuilder builder, Params params) throws IOException;

    /**
     * This method should be implemented to specify if class is writeable or not
     * */
    boolean isWriteable();
}
