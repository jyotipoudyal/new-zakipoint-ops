package zakipointCore.commonUtils;

import com.configurations.config.AbstractConfigurationManager;
import com.configurations.config.ClusterConfig;
import com.configurations.config.ConfigFileName;
import com.configurations.config.ElasticSearchConnectionManager;
import org.elasticsearch.client.Client;

import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 9/24/15.
 */
public class RequestUtils {
    public static final String REPORTING="reporting";
    public static final String COMPARISON="comparison";
    public static final String IS_BOB = "isBOB";

    public static String[] getPeriod(SearchRequest request){
        return getPeriod(request.requestParameters);
    }
    public static String[] getPeriod(Map<String, String> request){
        StringBuilder builder=new StringBuilder();
        if(request.containsKey("reportingFrom"))      {
            builder.append(request.get("reportingFrom")).append(":").append(request.get("reportingTo"));
        }
        if(request.containsKey("comparisonFrom"))  builder.append(",").append(request.get("comparisonFrom")).append(":").append(request.get("comparisonTo"));

        return builder.toString().split(",");
    }
    public static boolean isArrayRequest(String request){
        return (request.startsWith("[")&& request.endsWith("]"));
    }
    public static String getValue(SearchRequest request,String key,String d){
        if(!request.hasParam(key)){
            return d;
        }else{
            return request.get(key);
        }
    }
    public static Integer from (SearchRequest request,Integer def){
        return formatToInt(request,"from",String.valueOf(def));
    }
    public static Integer pageSize (SearchRequest request,Integer def){
        return formatToInt(request, "size",String.valueOf(def));
    }
    public static boolean isPaginationRequest(SearchRequest request){
        return request.hasParameter("from") && request.hasParameter("size");
    }

    public static Integer formatToInt(SearchRequest request, String key, String def) {
        return Integer.valueOf(RequestUtils.getValue(request, key, def));
    }
    public static String[] getArrayRequest(String request) {
//        String requestParams=isArrayRequest(request)?request.replace("[","").replace("]","").split(","):request;
        String[] tempValues = isArrayRequest(request) ? request.replace("[", "").replace("]", "").split(",") : request.split("#!:!#");
        //String[] tempValues = requestParams.split(",");
        String[] values = new String[tempValues.length];
        int i = 0;
        for (String value : tempValues) {
            values[i] = value.trim();
            i++;
        }
        return values;
        //return requestParams.split(",");
    }

    public static String getArrayRequestValue(String request) {
        String tempValues = isArrayRequest(request) ? request.replace("[", "").replace("]", ""): request;
        return tempValues;
    }

    public static String makeArrayRequest(List<String> req){
        StringBuilder stringBuilder = new StringBuilder("[");
        boolean isFirst = true;
        for (String s : req) {
            if(!isFirst) stringBuilder.append(",");
            else isFirst = false;
            stringBuilder.append(s);
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    public static Client getClient(Map<String,String> requestParameters) {
        String indexData = "";
        if(requestParameters.containsKey("indexData")){
            indexData = String.valueOf(requestParameters.get("indexData"));
        }else{
            indexData = String.valueOf(requestParameters.get("clientId"));
        }

        AbstractConfigurationManager configurationManager = SpringBeanUtils.getBean("configurationManager");
        ClusterConfig clusterConfig =configurationManager.getClusterConfig(ConfigFileName.getFileNameClient(indexData));
        return ElasticSearchConnectionManager.getClient(clusterConfig);
    }

    public static String getClientId(String clientId){
        AbstractConfigurationManager configurationManager = SpringBeanUtils.getBean("configurationManager");
        return clientId.replace(configurationManager.getQualifier().toString(), "");
    }
}
