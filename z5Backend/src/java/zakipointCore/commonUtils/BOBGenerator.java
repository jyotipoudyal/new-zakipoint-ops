package zakipointCore.commonUtils;

import com.configurations.utils.Constants;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 5/10/16.
 */
public class BOBGenerator {
    public static Object generate(Map<String, String> requestMap, ReportGenerator generator) {
        Map<String,String> bobRequest=bobRequest(requestMap);
        JSONObject dasResponse=generator.generate(bobRequest);
        return new BoBFormatter().format(dasResponse,requestMap);
    }


    public static Map<String,String> bobRequest(Map<String,String> request){
        Map<String,String> newRequest=new HashMap<String, String>(request);
        List<String> toRemoveFields=new ArrayList<String>();

        toRemoveFields.addAll(Constants.BOB_EXCLUDE_FIELDS);
        SearchRequest searchRequest=new SearchRequest();
        searchRequest.requestParameters=newRequest;
        toRemoveFields.addAll(LevelUtils.getAllLevels(searchRequest));
        searchRequest.requestParameters.put(RequestUtils.IS_BOB,"yes");
        toRemoveFields.addAll(Constants.COHORT_FIELDS);

        for (String field : toRemoveFields) {
            if(searchRequest.hasParam(field)){
                searchRequest.requestParameters.remove(field);
            }
        }
        return searchRequest.requestParameters;
    }
    public static final class BoBFormatter {
        public Object format(JSONObject dasResponse, Map<String, String> requestMap) {
            return dasResponse.get("reporting");
        }
    }
}
