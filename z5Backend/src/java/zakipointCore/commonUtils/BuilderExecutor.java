package zakipointCore.commonUtils;

import com.configurations.utils.BuilderAction;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionRequestBuilder;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.concurrent.*;

/**
 * Created by sktamang on 9/25/15.
 */
public class BuilderExecutor {
    XContentBuilder contentBuilder;
    JSONObject response;
    ActionRequestBuilder builder;
    AbstractSummaryListener listener;
    public boolean isSuccess = false;

    public XContentBuilder getContentBuilder() {
        return contentBuilder;
    }
    public JSONObject getResponse() {
        return response;
    }

    public BuilderExecutor(ActionRequestBuilder builder,AbstractSummaryListener listener) {
        this.builder=builder;
        this.listener=listener;
    }
    public BuilderExecutor execute(){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        FutureTask<JSONObject> futureTask = new FutureTask<JSONObject>(new Executor(builder,listener));
        futureTask.run();
        executorService.execute(futureTask);
        try {
            response = futureTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        isSuccess = futureTask.isDone();
        return this;
    }

    public  class Executor implements Callable<JSONObject> {
        ActionRequestBuilder builder;
        AbstractSummaryListener summaryListener;

        public Executor(ActionRequestBuilder builder, AbstractSummaryListener summaryListener) {
            this.builder = builder;
            this.summaryListener = summaryListener;
        }

        @Override
        public JSONObject call() throws Exception {
            ActionResponse response = null;
            try {
                response = BuilderAction.executeBuilder(builder, summaryListener.getReport());
                summaryListener.onResponse(response);
            }catch (ElasticsearchException e) {
//                BuilderAction.print(summaryListener.getReport(), builder);
                summaryListener.onFailure(e);
            }catch (Exception e){
                summaryListener.onFailure(e);
            }
            while (!summaryListener.processComplete.get());
            return summaryListener.state.response;
        }

    }
}
