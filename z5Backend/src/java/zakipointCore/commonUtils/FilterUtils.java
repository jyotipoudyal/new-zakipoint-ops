package zakipointCore.commonUtils;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.Pharmacy;
import com.configurations.utils.VisitAdmission;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.*;

import java.util.*;

/**
 * Created by sktamang on 9/24/15.
 */
public class FilterUtils {
    public static Map<String, String> ELIGIBLE_FIELDS = new HashMap<String, String>() {{
        put("medical", "eligibleDates");
        put("dental", "eligibleDatesDental");
        put("vision", "eligibleDatesVision");
    }};

    public static BoolFilterBuilder must(BoolFilterBuilder boolFilter, FilterBuilder... filters) {
        if (boolFilter == null) boolFilter = new BoolFilterBuilder();
        for (FilterBuilder filter : filters) {
            if (filter != null) boolFilter.must(filter);
        }
        return boolFilter;
    }

    public static BoolFilterBuilder must(FilterBuilder... filters) {
        return must(null, filters);
    }

    public static RangeFilterBuilder getDateRangeFilter(String field, Object fromDate, Object toDate, boolean lower, boolean upper) {
        RangeFilterBuilder range = FilterBuilders.rangeFilter(field);
        range.from(fromDate).to(toDate).includeLower(lower).includeUpper(upper);
        return range;
    }


    public static FilterBuilder andMultipleFilters(AndFilterBuilder andFilterBuilder, FilterBuilder... filter) {
        if (andFilterBuilder == null) andFilterBuilder = FilterBuilders.andFilter();
        for (FilterBuilder filterBuilder : filter) {
            if (filterBuilder != null) {
                andFilterBuilder.add(filterBuilder);
            }
        }
        return andFilterBuilder;
    }


    public static TermFilterBuilder getTermFilter(String name, Object value) {
        String val = String.valueOf(value);
        if (val != null && !val.isEmpty())
            return FilterBuilders.termFilter(name, value);
        return null;
    }
    public static FilterBuilder getTermsFilter(String name, Object... value) {
        return FilterBuilders.termsFilter(name, value);
    }
    public static FilterBuilder getAndFilter(FilterBuilder... filters) {
        return FilterBuilders.andFilter(filters);
    }

    public static RangeFilterBuilder getRangeFilter(String field, Integer lower, Integer upper, Boolean includeLower, Boolean includeUpper) {
        RangeFilterBuilder range = FilterBuilders.rangeFilter(field);
        range.from(lower).to(upper).includeLower(includeLower).includeUpper(includeUpper);
        return range;
    }

    public static RangeFilterBuilder getParticipatingFilter(RequestState state) {

        String reportingTo = state.request.hasParameter(state.period() + "To") ? state.request.get(state.period() + "To") : QueryUtils.DEFAULT_DATE_UPPER_END;
        String[] date = reportingTo.split("-");
        String monthFrom = date[0] + "-" + date[1] + "-01";

        RangeFilterBuilder participatingFilter = FilterBuilders.rangeFilter("participatingMonths");
        participatingFilter.from(monthFrom);
        participatingFilter.to(reportingTo);
        return participatingFilter;
    }

    public static AndFilterBuilder getEligibleMonthsFilter(RequestState state, Client client) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        final String reportingTo = state.request.hasParameter(state.period() + "To") ? state.request.get(state.period() + "To") : "2012-12-31";
        long eligibleFrom = DateUtils.getFirstDateOfMonth(reportingTo);
        long eligibleTo = DateUtils.getLastDateOfMonth(reportingTo);
        RangeFilterBuilder rangeEligible = FilterUtils.getDateRangeFilter("eligibleMonths", eligibleFrom, eligibleTo, true, true);
        andFilter.add(rangeEligible);
        if (QueryUtils.getGroupFilter(state.request) != null) {
            andFilter.add(QueryUtils.getGroupFilter(state.request));
        }
        return andFilter;
    }

    public static FilterBuilder getRelationShipFilter(RequestState state,String relationShipId) {

        AndFilterBuilder andFilter = new AndFilterBuilder();
        final String reportingTo = state.request.hasParameter(state.period() + "To") ? state.request.get(state.period() + "To") : "2012-12-31";
        andFilter.add(FilterBuilders.rangeFilter("relationship.fromDate").lte(reportingTo));
        andFilter.add(FilterBuilders.rangeFilter("relationship.toDate").gte(reportingTo));

        andFilter.add(FilterBuilders.termFilter(MemberSearch.RELATIONSHIP.ID,relationShipId))
                .add(FilterBuilders.termsFilter(MemberSearch.RELATIONSHIP.ELIGIBILITY_TYPE,"medical","dental","vision"));
        return FilterBuilders.nestedFilter("relationship",andFilter);
    }

    public static FilterBuilder eligibleDateFilter(RequestState state) {

        AndFilterBuilder andFilter = new AndFilterBuilder();
        final String reportingTo = state.request.hasParameter(state.period() + "To") ? state.request.get(state.period() + "To") : "2012-12-31";
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.fromDate").lte(reportingTo));
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.toDate").gte(reportingTo));

        return FilterBuilders.nestedFilter("eligibleDates",andFilter);
    }

    /*EligibleDate Filter for SpecificRange*/
    public static FilterBuilder eligibleDateFilter(Object reportingFrom, Object reportingTo) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.fromDate").lte(reportingTo));
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.toDate").gte(reportingFrom));
        return FilterBuilders.nestedFilter("eligibleDates",andFilter);
    }
    public static FilterBuilder eligibleDateFilter(String reportingTo) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        Long longDate = DateUtils.getTimeFromDateWrtTimeZone(reportingTo);
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.fromDate").lte(longDate));
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.toDate").gte(longDate));

        return FilterBuilders.nestedFilter("eligibleDates", andFilter);
    }
    public static FilterBuilder eligibleDateFilter(Long reportingTo) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.fromDate").lte(reportingTo));
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.toDate").gte(reportingTo));

        return FilterBuilders.nestedFilter("eligibleDates", andFilter);
    }
    public static FilterBuilder eligibleDateFilter(Object reportingTo) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.fromDate").lte(reportingTo));
        andFilter.add(FilterBuilders.rangeFilter("eligibleDates.toDate").gte(reportingTo));

        return FilterBuilders.nestedFilter("eligibleDates", andFilter);
    }

    public static AndFilterBuilder groupFilter(RequestState state) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        final String reportingTo = state.request.hasParameter(state.period() + "To") ? state.request.get(state.period() + "To") : "2012-12-31";
//        final String reportingFrom = state.request.param(state.period() + "From");
        andFilter.add(FilterBuilders.rangeFilter("groups.fromDate").from(null).to(reportingTo).includeLower(true).includeUpper(true));
        andFilter.add(FilterBuilders.rangeFilter("groups.toDate").from(reportingTo).to(null).includeLower(true).includeUpper(true));
        String groupId = state.request.get("groupId");
        andFilter.add(FilterBuilders.termFilter("groups.groupId", groupId));
        return andFilter;
    }

    public static FilterBuilder getMultiEligibleFilter(RequestState state) {
        OrFilterBuilder orFilter = new OrFilterBuilder();
        Set<String> eligibleFields = getEligibleFields(state);
        for (String eligibleField : eligibleFields) {

            AndFilterBuilder andFilter = new AndFilterBuilder();
            String reportingTo = state.request.hasParam(state.period() + "To") ? state.request.param(state.period() + "To") : "2012-12-31";
            andFilter.add(FilterBuilders.rangeFilter(eligibleField + ".fromDate").lte(reportingTo));
            andFilter.add(FilterBuilders.rangeFilter(eligibleField + ".toDate").gte(reportingTo));
            orFilter.add(FilterBuilders.nestedFilter(eligibleField, andFilter));
        }

        return orFilter;
    }

    private static Set<String> getEligibleFields(RequestState state) {
        Set<String> eligibleFields = new HashSet<String>();
        String[] eligibleTypes = RequestUtils.getArrayRequest(state.request.param("eligibilityType"));
        for (String eligibleType : eligibleTypes) {
            if (ELIGIBLE_FIELDS.containsKey(eligibleType)) eligibleFields.add(ELIGIBLE_FIELDS.get(eligibleType));
        }
        if (eligibleFields.size() == 0) eligibleFields.add("eligibleDates");
        return eligibleFields;
    }

    public static QueryBuilder defaultFilteredQuery(FilterBuilder filter){
        return QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),filter);
    }
    public static FilterBuilder rangeFilter(String field,Object from,Object to){
        return FilterBuilders.rangeFilter(field).from(from).to(to);
    }
    public static FilterBuilder nestedFilter(String nestedOn,FilterBuilder filter){
        return FilterBuilders.nestedFilter(nestedOn,filter);
    }

    public static FilterBuilder buildPaidDateFilter(RequestState state,boolean isPaidStart,String field){
        AndFilterBuilder filterBuilder = new AndFilterBuilder();
        FilterBuilder paidDateFilter = FilterUtils.nestedPaidDateFilter(state, state.periodFrom(), state.periodTo(), field);
        filterBuilder.add(paidDateFilter);
        if(isPaidStart){
            filterBuilder.add(FilterBuilders.termFilter(field+"." + "isPaidStart", 1));
        }
        return FilterBuilders.nestedFilter(field,filterBuilder);
    }


    public static void addRangeFilter(String periodTo, String periodFrom, String field, AndFilterBuilder filterBuilder) {
        filterBuilder.add(FilterBuilders.rangeFilter(field).to(DateUtils.getTimeFromDateWrtTimeZone(periodTo)).
                from(DateUtils.getTimeFromDateWrtTimeZone(periodFrom)));
    }

    public static void paidServiceDateFilter(RequestState state, AndFilterBuilder filterBuilder) {
        String basis = QueryUtils.parseBasis(state.request);
        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.param(state.period() + "PaidThrough");
        } else {
            periodToPaid = state.periodTo();
        }
        addRangeFilter(periodToPaid, state.periodFrom(), "paidDate", filterBuilder);
        if (basis.equals("serviceDate")) {
            addRangeFilter(state.periodTo(), state.periodFrom(), "serviceDate", filterBuilder);
        }
    }

    public static void serviceDateFilter(RequestState state, AndFilterBuilder filterBuilder) {
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            addRangeFilter(state.periodTo(), state.periodFrom(), "serviceDate", filterBuilder);
        }
    }

    public static void monthWiseServiceDateFilter(RequestState state, AndFilterBuilder filterBuilder,String periodFrom,String periodTo) {
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            addRangeFilter(periodTo, periodFrom, "serviceDate", filterBuilder);
        }
    }

    public static void nestedPaidDateFilter(RequestState state, String field, AndFilterBuilder andFilterBuilder) {
        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.get(state.period() + "PaidThrough");
        } else {
            periodToPaid = state.periodTo();
        }
        andFilterBuilder.add(buildRangeFilter(field + "." + "paidDate", state.periodFrom(), periodToPaid));
    }

    public static RangeFilterBuilder buildRangeFilter(String fieldName, String from, String to) {
        return FilterBuilders.rangeFilter(fieldName).gte(from).lte(to);
    }

    public static FilterBuilder visitTypeFilter(String visitType){
        return FilterBuilders.notFilter(
                FilterBuilders.termFilter(
                        visitType ,0
                )
        );
    }

    public static Set<String> getEligibieFields(RequestState state) {
        Set<String> eligibleFields = new HashSet<String>();
        String[] eligibleTypes = RequestUtils.getArrayRequest(state.request.param("eligibilityType"));
        for (String eligibleType : eligibleTypes) {
            if(ELIGIBLE_FIELDS.containsKey(eligibleType))eligibleFields.add(ELIGIBLE_FIELDS.get(eligibleType));
        }
        if(eligibleFields.size()==0) eligibleFields.add("eligibleDates");
        return eligibleFields;
    }

    public static void serviceDateFilter(RequestState state, AndFilterBuilder filterBuilder,String type) {
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            addRangeFilter(state.periodTo(), state.periodFrom(), type + "serviceDate", filterBuilder);
        }
    }

    public static void paidDateFilter(RequestState state, AndFilterBuilder filterBuilder,String type) {
            addRangeFilter(state.periodTo(), state.periodFrom(), type + "paidDate", filterBuilder);
    }

    public static void serviceDateFilter(RequestState state, AndFilterBuilder filterBuilder, String from, String to) {
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            addRangeFilter(to, from, "serviceDate", filterBuilder);
        }
    }

    public static FilterBuilder nestedPaidDateFilter(RequestState state, String periodFrom, String periodTo, String field) {

        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.param(state.period() + "PaidThrough");
        } else {
            periodToPaid = periodTo;
        }
        return buildRangeFilter(field + "." + "paidDate", periodFrom, periodToPaid);
    }

    public static FilterBuilder nestedEarliestPaidDateFilter(RequestState state, String field) {
        AndFilterBuilder filterBuilder = new AndFilterBuilder();
        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.get(state.period() + "PaidThrough");
        } else {
            periodToPaid = state.periodTo();
        }
        filterBuilder.add(FilterBuilders.termFilter(field+"." + "isPaidStart", 1));
        filterBuilder.add(buildRangeFilter(field + "." + "paidDate", state.periodFrom(), periodToPaid));
        return FilterBuilders.nestedFilter(field, filterBuilder);
    }


    public static FilterBuilder nestedEarliestPaidDateFilter(String field,String periodFrom,String periodTo) {
        AndFilterBuilder filterBuilder = new AndFilterBuilder();
        filterBuilder.add(FilterBuilders.termFilter(field+"." + "isPaidStart", 1));
        filterBuilder.add(buildRangeFilter(field + "." + "paidDate", periodFrom, periodTo));
        return FilterBuilders.nestedFilter(field, filterBuilder);
    }

    public static FilterBuilder totalPaidAmountFilter(String field, double value){
        return FilterBuilders.notFilter(FilterBuilders.termFilter(field, value));
    }

    public static FilterBuilder paidDateFilter(RequestState state) {
        AndFilterBuilder filterBuilder = new AndFilterBuilder();
        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.param(state.period() + "PaidThrough");
        } else {
            periodToPaid = state.periodTo();
        }
        addRangeFilter(periodToPaid, state.periodFrom(), "paidDate", filterBuilder);
        return filterBuilder;
    }

    public static FilterBuilder paidMonthFilter(RequestState state,String periodFrom,String periodTo) {
        AndFilterBuilder filterBuilder = new AndFilterBuilder();
        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.param(state.period() + "PaidThrough");
        } else {
            periodToPaid = state.periodTo();
        }
        addRangeFilter(periodTo, periodFrom, "paidDate", filterBuilder);
        return filterBuilder;
    }


    public static FilterBuilder getPaidServiceMonthFilter(RequestState state, String field) {
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        FilterBuilder filterBuilder;
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            andFilterBuilder.add(FilterUtils.buildRangeFilter(field + "." + "serviceMonth", state.periodFrom(), state.periodTo()));
        }
            andFilterBuilder.add(FilterUtils.buildRangeFilter(field + "." + "paidMonth", state.periodFrom(), state.periodTo()));
        return FilterBuilders.nestedFilter(field,andFilterBuilder);
    }

    public static FilterBuilder getPaidServiceMonthFilterNonNested(RequestState state, String field) {
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        FilterBuilder filterBuilder;
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            andFilterBuilder.add(FilterUtils.buildRangeFilter(field + "." + "serviceMonth", state.periodFrom(), state.periodTo()));
        }
        andFilterBuilder.add(FilterUtils.buildRangeFilter(field + "." + "paidMonth", state.periodFrom(), state.periodTo()));
        return andFilterBuilder;
    }


    public static FilterBuilder implementNumerator(RequestState state,String numOrDen,boolean isNumerator,Object metricCode) {
        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermsFilter("qm.measure", metricCode));
        if(isNumerator) qmFilter.add(FilterBuilders.termFilter("qm.numerator", numOrDen));
        else qmFilter.add(FilterBuilders.termsFilter("qm.numerator",Arrays.asList("1","0")));
        qmFilter.add(getRangeFilter(state));
        return FilterBuilders.nestedFilter("qm",qmFilter);
    }

    public static FilterBuilder implementNumerator(Map<String,String> requestParams,String numOrDen,boolean isNumerator,Object metricCode) {
        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermsFilter("qm.measure", metricCode));
        if(isNumerator) qmFilter.add(FilterBuilders.termFilter("qm.numerator", numOrDen));
        else qmFilter.add(FilterBuilders.termsFilter("qm.numerator",Arrays.asList("1","0")));
        qmFilter.add(getRangeFilter(requestParams));
        return FilterBuilders.nestedFilter("qm",qmFilter);
    }

    public static AndFilterBuilder getRangeFilter(RequestState state) {
        String period = state.request.hasParam(state.period() + "To") ? state.request.param(state.period() + "To") : "2012-12-31";
        RangeFilterBuilder rangeFilter = FilterBuilders.rangeFilter("qm.toDate").from(period).to(null);
        RangeFilterBuilder rangeFilter1 = FilterBuilders.rangeFilter("qm.fromDate").from(null).to(period);
        return new AndFilterBuilder().add(rangeFilter).add(rangeFilter1);
    }

    public static AndFilterBuilder getRangeFilter(Map<String,String> requestParams) {
        String period = requestParams.get("reportingTo");
        RangeFilterBuilder rangeFilter = FilterBuilders.rangeFilter("qm.toDate").from(period).to(null);
        RangeFilterBuilder rangeFilter1 = FilterBuilders.rangeFilter("qm.fromDate").from(null).to(period);
        return new AndFilterBuilder().add(rangeFilter).add(rangeFilter1);
    }

    public static AndFilterBuilder getParentEligibilityFilter(String periodTo){
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.hasParentFilter(MemberSearch.NAME,eligibleDateFilter(periodTo)));
        return andFilterBuilder;
    }

    public static AndFilterBuilder getUmVisitFilter(AndFilterBuilder andFilterBuilder, String aggField){
        andFilterBuilder.add(FilterBuilders.notFilter(FilterBuilders.termFilter(aggField,0)))
                .add(FilterBuilders.notFilter(FilterBuilders.termFilter(VisitAdmission.TOTAL_AMOUNT,0)));
        return andFilterBuilder;
    }


    public static void serviceDateFilterForChild(RequestState state, AndFilterBuilder filterBuilder) {
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            addRangeFilter(state.periodTo(), state.periodFrom(), VisitAdmission.NAME+".serviceDate", filterBuilder);
        }
    }

    public static FilterBuilder nestedEarliestPaidDateFilterForChild(RequestState state, String field) {
        AndFilterBuilder filterBuilder = new AndFilterBuilder();
        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.get(state.period() + "PaidThrough");
        } else {
            periodToPaid = state.periodTo();
        }
        filterBuilder.add(FilterBuilders.termFilter(VisitAdmission.NAME+"."+field+"." + "isPaidStart", 1));
        filterBuilder.add(buildRangeFilter(VisitAdmission.NAME + "." + field + "." + "paidDate", state.periodFrom(), periodToPaid));
        return FilterBuilders.nestedFilter(VisitAdmission.NAME+"."+field, filterBuilder);
    }

    public static FilterBuilder paidDateFilterForChild(RequestState state) {
        AndFilterBuilder filterBuilder = new AndFilterBuilder();
        String periodToPaid;
        if (state.isPaidThrough()) {
            periodToPaid = state.request.param(state.period() + "PaidThrough");
        } else {
            periodToPaid = state.periodTo();
        }
        addRangeFilter(periodToPaid, state.periodFrom(), "VisitAdmission.paidDate", filterBuilder);
        return filterBuilder;
    }

    public static AndFilterBuilder getErVisitFilter(AndFilterBuilder andFilterBuilder){
        andFilterBuilder.add(FilterBuilders.termFilter(VisitAdmission.NAME+"."+VisitAdmission.ER_VISIT,1))
                .add(FilterBuilders.notFilter(FilterBuilders.termFilter(VisitAdmission.NAME+"."+VisitAdmission.TOTAL_AMOUNT, 0)));
        return andFilterBuilder;
    }

    public static AndFilterBuilder applyServiceAndPaidDateFilter(RequestState state,AndFilterBuilder andFilterBuilder){
        FilterUtils.serviceDateFilter(state, andFilterBuilder);
        FilterUtils.addRangeFilter(state.periodTo(), state.periodFrom(), Pharmacy.PAID_DATE, andFilterBuilder);
        return andFilterBuilder;
    }

    public static AndFilterBuilder newQmRangeFilter(String period) {
        RangeFilterBuilder rangeFilter = FilterBuilders.rangeFilter("qm.toDate").from(period).to(null);
        RangeFilterBuilder rangeFilter1 = FilterBuilders.rangeFilter("qm.fromDate").from(null).to(period);

        return new AndFilterBuilder().add(rangeFilter).add(rangeFilter1);
    }

    public static void paidServiceDateFilter(Map<String,String> requestParams, AndFilterBuilder filterBuilder) {
//        String basis = QueryUtils.parseBasis(requestParams);
        String periodToPaid;
//        if (state.isPaidThrough()) {
//            periodToPaid = state.request.param(state.period() + "PaidThrough");
//        } else {
//            periodToPaid = state.periodTo();
//        }
        addRangeFilter(requestParams.get("reportingTo"), requestParams.get("reportingFrom"), "paidDate", filterBuilder);
//        if (basis.equals("serviceDate")) {
//            addRangeFilter(state.periodTo(), state.periodFrom(), "serviceDate", filterBuilder);
//        }
    }

}
