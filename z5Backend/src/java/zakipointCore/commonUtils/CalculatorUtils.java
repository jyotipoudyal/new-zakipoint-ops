package zakipointCore.commonUtils;

/**
 * Created by sktamang on 3/14/16.
 */
public class CalculatorUtils {
    public static Double  divide(Double a,Double b){
        if(b <= 0){
            return 0D;
        }else{
            return (a/b);
        }
    }
    public static Double  newPmpy(Double amount,Double memberMonths){
        if(memberMonths.intValue()==0){
            return 0D;
        }else{
            return (amount / memberMonths)*12;
        }
    }
    public static Double dividePositive(Double a,Double b){
        Double div=divide(a,b);
        return formatPositive(div);
    }

    public static Double formatPositive(Double a){
        if(a>0D){
            return a;
        }
        return 0D;
    }
    public static Double pepm(Double paidAmount,Double memberMonths){
        return divide(paidAmount,memberMonths);
    }

    public static Double calculatePercentage(Double count,Double total){
        if(total>0) return divide(count, total)*100;
        else return 0D;
    }

    public static Double perThousand(Double value,Double memberMonths,Double months){
        Double mmPerMonth = divide(memberMonths, months);
        return divide(value * 1000, mmPerMonth) * (12/months);
    }

    public static Double perThousand(Double metric, Double memberMonths){
        return divide(metric * 12 *1000, memberMonths);
    }
    public static Double perThousandMembers(Double metric, Double memberCount){
        return divide(metric * 1000, memberCount);
    }

    public static Double calculatePercentageForLong(Long count,Long total){
        return divide(count.doubleValue(),total.doubleValue())*100;
    }

    public static Double calculatePercentage(Number a,Number b){
        if(b.doubleValue() <= 0){
            return 0D;
        }else{
            return (a.doubleValue()/b.doubleValue())*100;
        }
    }
}
