package zakipointCore.commonUtils;

import java.util.Map;

/**
 * Created by sktamang on 9/24/15.
 */
public class SearchRequest {
    public Map<String, String> requestParameters;       //to hold the request params from the front end request

    public SearchRequest(){
    }

    public boolean hasParameter(String param) {
        return requestParameters.containsKey(param) && !requestParameters.get(param).isEmpty();
    }

    public String  get(String param) {
        return requestParameters.get(param);
    }
    public boolean hasParam(String param) {
        return requestParameters.containsKey(param) && !requestParameters.get(param).isEmpty();
    }

    public String  param(String param) {
        return requestParameters.get(param);
    }

    public void print(){
        for(Map.Entry entry : requestParameters.entrySet()){
            System.out.print(entry.getKey() + "=>" + entry.getValue());
        }
    }

    public int paramAsInt(String key, int defaultValue){
        if (requestParameters.containsKey(key)){
            return Integer.parseInt(requestParameters.get(key));
        }else
            return defaultValue;
    }
}
