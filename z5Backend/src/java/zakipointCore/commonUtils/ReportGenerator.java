package zakipointCore.commonUtils;

import org.codehaus.groovy.grails.web.converters.exceptions.ConverterException;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.Map;

/**
 * Created by sktamang on 9/25/15.
 */
public interface ReportGenerator {
    public JSONObject generate(Map<String, String> map) throws ConverterException;
}
