package zakipointCore.commonUtils;

import com.configurations.config.AbstractConfigurationManager;
import com.configurations.config.ConfigFileName;
import com.configurations.config.ElasticSearchConnectionManager;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 5/10/16.
 */
public class BenchmarkGenerator {
    public static final String BENCHMARK_TYPE="benchmarkType";
    public static final Map<String, String> AVAILABLE_BENCHMARKS = new HashMap<String, String>(){{
        put("benchmark", "Benchmark");
        put("truvenBenchmark", "TruvenBenchmark");
    }};

    public static final Map<String, BenchmarkListener> BENCHMARKS_LISTENER = new HashMap<String, BenchmarkListener>(){{
        put("Benchmark",new BenchmarkListener() );
        put("TruvenBenchmark", new TruvenBenchmarkListener());
    }};

    public static JSONObject generate(Map<String, String> requestMap, ReportGenerator generator) {

        Client client = getClient();
        SearchRequestBuilder searchRequestBuilder=client.prepareSearch("benchmark").setTypes("BenchmarkAug2016");
        searchRequestBuilder.setQuery(QueryBuilders.termQuery("_id","default"));
        TruvenBenchmarkListener benchmarkListener =new TruvenBenchmarkListener();
        String benchmarkKey=requestMap.containsKey("benchmarkKey") ? requestMap.get("benchmarkKey") : "healthPlan";
        benchmarkListener.onResponse(
                searchRequestBuilder.execute().actionGet(), benchmarkKey
        );
        return benchmarkListener.benchmark;
    }

    public  static Client getClient() {
        AbstractConfigurationManager configurationManager = SpringBeanUtils.getBean("configurationManager");
        return ElasticSearchConnectionManager.getClient(configurationManager.getClusterConfig(ConfigFileName.getFileName()));
    }

    public static class BenchmarkListener {
        public JSONObject benchmark;

        public void onResponse(SearchResponse response,String reportName) {
            benchmark = new JSONObject();
            for (SearchHit hitFields : response.getHits().hits()) {
                for (String key : hitFields.getSource().keySet()) {
                    benchmark.put(key, hitFields.getSource().get(key));
                }
            }
        }
    }

    public static class TruvenBenchmarkListener extends BenchmarkListener{

        public void onResponse(SearchResponse response,String benchmarkKey) {
            benchmark = new JSONObject();
            for (SearchHit hitFields : response.getHits().hits()) {
                Map<String,Object> values= (Map<String, Object>) hitFields.getSource().get(benchmarkKey);
                if(values!=null){
                    for (String key : values.keySet()) {
                        benchmark.put(key, values.get(key));
                    }
                    if(benchmarkKey.equalsIgnoreCase("healthPlan")){
                        values=(Map<String, Object>)hitFields.getSource().get("UMReport");
                        for (String key : values.keySet()) {
                            benchmark.put(key, values.get(key));
                        }
                    }
                }
            }
        }
    }
}
