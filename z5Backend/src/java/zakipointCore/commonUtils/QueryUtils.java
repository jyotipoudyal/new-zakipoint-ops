package zakipointCore.commonUtils;

import com.configurations.search.MemberSearchRequest;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.query.*;

import java.util.*;

/**
 * Created by sktamang on 9/24/15.
 */
public class QueryUtils {
    public static final String PARAM_PAID_THROUGH="PaidThrough";
    public static int size = 0;
    public static final String DEFAULT_DATE_UPPER_END = "2012-12-31";
    public static final int FIXED_DAY_OF_MONTH=15;

    public static boolean isArray(String text) {
        String v = text.trim();
        return v.startsWith("[") && v.endsWith("]");
    }

    //    public static ESLogger logger;
    public static SearchRequestBuilder buildEndSearch(Client client, final SearchRequest request) {
        String index = LevelUtils.getClientId(request.requestParameters);
        SearchRequestBuilder builder = client.prepareSearch(index);
        builder.setSearchType(SearchType.QUERY_THEN_FETCH);
        return builder;
    }
    public static SearchRequestBuilder buildEndSearch(final MemberSearchRequest request) { Settings settings = ImmutableSettings.settingsBuilder()
            .put("cluster.name", "zakipoint_es").build();
        Client client =    new TransportClient(settings);
        String index = request.hasParameter("clientId") ? request.get("clientId") : "0005";
        SearchRequestBuilder builder = client.prepareSearch(index);
        builder.setSearchType(SearchType.QUERY_THEN_FETCH);
        return builder;
    }
    public static Client getClient(final MemberSearchRequest request) { Settings settings = ImmutableSettings.settingsBuilder()
            .put("cluster.name", "zakipoint_es").build();
        Client client =    new TransportClient(settings);
        return client;
    }

    public  static MultiSearchRequestBuilder buildMultiSearch(Client client) {
        MultiSearchRequestBuilder multiSearchRequestBuilder = client.prepareMultiSearch();
        return multiSearchRequestBuilder;
    }

    public static String parseBasis(SearchRequest request) {
        String reportingBasis = request.hasParameter("reportingBasis") ? request.get("reportingBasis") : "paidDate";
        return reportingBasis.equalsIgnoreCase("serviceDate") ? "serviceDate" : "paidDate";
    }

    public static void setSize(SearchRequestBuilder builder) {
        builder.setFrom(0).setSize(1).setExplain(false);
    }

    public static String getKeyField(SearchRequest request) {
        return request.hasParameter("keyField") ? request.get("keyField") : "memberId";
    }


    public static String getGroupId(SearchRequest request){
        return request.get("groupId");
    }

    public static FilterBuilder getGroupFilter(SearchRequest request) {
        String groupId = getGroupId(request);
        if (groupId != null) {
            if (!groupId.trim().isEmpty()) return FilterUtils.getTermFilter("groupId", groupId);
        }
        return null;
    }

    public static String getReportName(SearchRequest request) {
        return getReportName(request.get("report"),request);
    }

    public static String getReportName(String reportWithFlag, SearchRequest request) {
        String report = reportWithFlag.contains(":") ? reportWithFlag.split(":")[0] : reportWithFlag;
        return request.hasParameter("trending") ? "Trending"+report : report;
    }

    public static String getMetricName(String reportWithFlag) {
        return reportWithFlag.contains(":") ? reportWithFlag.split(":")[1] : reportWithFlag;
    }

//    public static String getIntervalMonthFormat(String from, String interval) {
//        if ("monthly".equals(interval)) {
//            return DateUtils.getMonthYearForDate(DateUtils.getTimeFromDateWrtTimeZone(from));
//        } else if ("quarterly".equals(interval)) {
//            return Constants.QUARTER_PREFIX+ DateUtils.getQuarter(DateUtils.getDateTime(from))+ Constants.DASH+ DateUtils.getYear(from);
//        } else if ("yearly".equals(interval)) {
//            return DateUtils.getYear(from);
//        }
//
//        return "";
//    }

//    public static FieldConfig getColumnConfig(String table, String key, boolean isESResponse) {
//        FieldConfig entryColumnConfig = null;
//        NewTableConfig webConfigEntry = ConfigurationFactory.newTableConfig.get(table);
//        FieldConfig fieldConfig = null;
//        if(isESResponse)fieldConfig = FieldConfigurations.fromTableCode(webConfigEntry.tableCode).fromFieldName(key);
//        else fieldConfig = FieldConfigurations.fromTableCode(webConfigEntry.tableCode).fromDisplayName(key);
//        if(fieldConfig==null)return null;
//        key = fieldConfig.field;
//
//        if (webConfigEntry.fields.containsKey(key)) {
//            entryColumnConfig = webConfigEntry.fields.get(key);
//        }
//
//        return entryColumnConfig;
//    }

    public static SearchRequestBuilder buildEndSearch(Client client, Map<String, String> request) {
        String indexName = LevelUtils.getClientId(request);
        SearchRequestBuilder requestBuilder = client.prepareSearch(indexName);
        return requestBuilder;
    }

    public static SearchRequestBuilder buildEndSearch(Client client, String index) {
        SearchRequestBuilder builder = client.prepareSearch(index);
        builder.setSearchType(SearchType.QUERY_THEN_FETCH);
        return builder;
    }

    public static SearchRequestBuilder buildEndSearchWihType(Client client, final SearchRequest request,String type) {
        String index = LevelUtils.getClientId(request.requestParameters);
        SearchRequestBuilder builder = client.prepareSearch(index).setTypes(type).setSize(0);
        builder.setSearchType(SearchType.QUERY_THEN_FETCH);
        return builder;
    }

    public static SearchRequestBuilder buildEndSearchWihType(Client client, Map<String ,String > requestParams,String type) {
        String index = LevelUtils.getClientId(requestParams);
        SearchRequestBuilder builder = client.prepareSearch(index).setTypes(type).setSize(0);
        builder.setSearchType(SearchType.QUERY_THEN_FETCH);
        return builder;
    }
}
