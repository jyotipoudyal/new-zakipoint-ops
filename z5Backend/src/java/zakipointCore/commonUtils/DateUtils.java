package zakipointCore.commonUtils;


import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.tz.FixedDateTimeZone;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by sktamang on 9/24/15.
 */
public class DateUtils {
    private final static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
    private final static DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final static DateFormat usDateFormat = new SimpleDateFormat("MM-dd-yyyy");
    private final static TimeZone timeZone = TimeZone.getTimeZone("GMT");
    private final static DateTimeZone dateTimeZone = DateTimeZone.forID("GMT");
    private final static SimpleDateFormat monthYearformatter = new SimpleDateFormat("MMM-yyyy");
    private final static SimpleDateFormat monthFormatter = new SimpleDateFormat("MMM");
    private final static SimpleDateFormat esMonthYearFormatter = new SimpleDateFormat("yyyy-MM");
    private final static SimpleDateFormat monthYearValueformatter = new SimpleDateFormat("MM-yyyy");
    static Map<String, Map<String, String>> quarterMap = null;

    static {
        quarterMap = new HashMap<String, Map<String, String>>();
        quarterMap.put("1", new HashMap<String, String>(){{
            put("-01-01","-03-31");
        }});
        quarterMap.put("2", new HashMap<String, String>(){{
            put("-04-01","-06-30");
        }});
        quarterMap.put("3", new HashMap<String, String>(){{
            put("-07-01","-09-30");
        }});
        quarterMap.put("4", new HashMap<String, String>(){{
            put("-10-01","-12-31");
        }});
    }
    public static String getMonthAndYearForDate (long time) {
        DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        sqlDateFormat.setTimeZone(timeZone);
        String date = sqlDateFormat.format(time);
        String[] split = date.split("-");
        return split[1] +"-"+ split[0];
    }

    public static String sqlToFormatted(String date,String toFormat) throws ParseException {
        DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        DateFormat formatter = new SimpleDateFormat(toFormat);
        sqlDateFormat.setTimeZone(timeZone);
        Date d = sqlDateFormat.parse(date);
        return formatter.format(d);
    }

    public static void setDefaultTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        DateTimeZone.setDefault(DateTimeZone.forID("GMT"));
    }

    public static long getTimeFromDateWrtTimeZone(String dateStr) {
        DateFormat usDateFormat = new SimpleDateFormat("MM-dd-yyyy");

        DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        DateFormat formatter;
        sqlDateFormat.setTimeZone(timeZone);
        usDateFormat.setTimeZone(timeZone);
        String[] parts = dateStr.split("-");
        if (parts[0].length() > 2)
            formatter = sqlDateFormat;
        else
            formatter = usDateFormat;
        if (parts.length == 2) { //contains only MM-YYYY
            dateStr = parts[1] + "-" + parts[0] + "-" + "01";
            formatter = sqlDateFormat;
        }
        formatter.setTimeZone(timeZone); // change the time zone of this formatter
        Date date = null;
        try {
            date = formatter.parse(dateStr);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return Long.MIN_VALUE;
        }
    }
    public static final String DATE_SEPR=",";

    public static Map<String,String> getDOBFromAge(Map<String,String> ageMap,  String reportingTo,boolean isForAggregation) {
        Map<String, String> toSend=new HashMap<String, String>();
        for (String age : ageMap.keySet()) {
            toSend.put(age,getDOBFromAge(ageMap.get(age),reportingTo,isForAggregation));
        }
        return toSend;
    }
    public static String getDOBFromAge(String range , String periodTo ){
        return getDOBFromAge(range, periodTo,false);
    }
    public static String getDOBFromAge(String range , String periodTo , boolean isForAggregation){
        String temp , from_ , to_ , from , to;
        boolean incLower=range.substring(0,1).equals("[");
        boolean incUpper=range.substring(range.length()-1,range.length()).equals("]");

        temp=range.substring(1,range.length()-1);

        from_=temp.split(",")[0];
        to_=temp.split(",")[1];


        if(from_.equals("null")){
            to="null";
        }else {
            Calendar toCalendar= DateUtils.getCalendar(periodTo);
            toCalendar.add(Calendar.YEAR,-(Integer.valueOf(from_)));
            if(!incLower){
                toCalendar.add(Calendar.DAY_OF_MONTH,-1);
            }
            if(isForAggregation){
                toCalendar.add(Calendar.DAY_OF_MONTH,1);
            }
            to= DateUtils.getStringDate(toCalendar.getTimeInMillis());
        }
        if(to_.equals("null")){
            from="null";
        }else{
            Calendar fromCalendar= DateUtils.getCalendar(periodTo);
            fromCalendar.add(Calendar.YEAR,-(Integer.valueOf(to_)));

            if(!incUpper){
                fromCalendar.add(Calendar.DAY_OF_MONTH,1);
            }
            from=DateUtils.getStringDate(fromCalendar.getTimeInMillis());
        }
        return from + "," + to;
    }

    public static Calendar getCalendar(String dateStr) {
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(getTimeFromDateWrtTimeZone(dateStr));
        return calendar;
    }

    public static String getMonthForDate(String dateStr){
        Calendar cal=getCalendar(dateStr);
        return cal.get(Calendar.YEAR)+"-"+String.format("%02d",cal.get(Calendar.MONTH)+1)+"-15";
    }

    public static String getMonthOnlyForDate(String dateStr){
        Calendar cal=getCalendar(dateStr);
        return cal.get(Calendar.YEAR)+"-"+String.format("%02d",cal.get(Calendar.MONTH)+1);
    }

    public static Calendar getCalendar(long time) {
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(time);
        return calendar;
    }

    public static long getLastDateOfMonth(String dateString) {
        long date = getTimeFromDateWrtTimeZone(dateString);
        Calendar cal = getCalendar(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTimeInMillis();
    }

    public static long getFirstDateOfMonth(String dateString) {
        long date = getTimeFromDateWrtTimeZone(dateString);
        Calendar cal = getCalendar(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTimeInMillis();
    }

    public static DateTime getDateTime(String dateString) {
        long date = getTimeFromDateWrtTimeZone(dateString);
        return getDateTime(date);
    }

    public static DateTime getDateTime(long date) {
        DateTimeZone dateTimeZone = DateTimeZone.forID("GMT");
        return new DateTime(date, dateTimeZone);
    }

    public static Map<Long, Long> getTrendInterval(String from, String to, String interval) {
        Map<Long, Long> ranges = null;
        if ("monthly".equals(interval)) {
            ranges = getAllMonthsNew(from, to);
        } else if ("quarterly".equals(interval)) {
            ranges = getAllQuartersNew(from, to);
        } else if ("yearly".equals(interval)) {
            ranges = getAllYearsNew(from, to);
        } else {
            ranges = new TreeMap<Long, Long>();
        }
        return ranges;
    }

    public static Map<Long, Long> getAllMonths(String from, String to) {
        Map<Long, Long> ranges = new TreeMap<Long, Long>();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));

        DateTime startMonth = formatter.parseDateTime(from);
        DateTime endMonth   = formatter.parseDateTime(to);

        ranges.put(startMonth.getMillis(), getMaxDate(startMonth));
        startMonth = startMonth.plusMonths(1);

        DateTime currentMonth = null;
        while ((startMonth.isBefore(endMonth) || startMonth.isEqual(endMonth))){
            currentMonth = startMonth;
            startMonth = startMonth.plusMonths(1);
            ranges.put(getMinDate(currentMonth), getMaxDate(currentMonth));
        }
        if (currentMonth != null) {
            ranges.put(getMinDate(currentMonth), endMonth.getMillis());
        }
        return ranges;
    }

    public static Map<String, String> getAllMonthsString(String from, String to) {
        Map<String, String> ranges = new TreeMap<String, String>();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));

        DateTime startMonth = formatter.parseDateTime(from);
        DateTime endMonth   = formatter.parseDateTime(to);

        ranges.put(startMonth.toString(formatter), startMonth.dayOfMonth().withMaximumValue().toString(formatter));
        startMonth = startMonth.plusMonths(1);

        DateTime currentMonth = null;
        while ((startMonth.isBefore(endMonth) || startMonth.isEqual(endMonth))){
            currentMonth = startMonth;
            startMonth = startMonth.plusMonths(1);
            ranges.put(currentMonth.dayOfMonth().withMinimumValue().toString(formatter),currentMonth.dayOfMonth().withMaximumValue().toString(formatter));
        }
        if (currentMonth != null) {
                ranges.put(currentMonth.dayOfMonth().withMinimumValue().toString(formatter), endMonth.toString(formatter));
        }
        return ranges;
    }

    public static String getPreceedingMonthOnly(String from) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        DateTime startMonth = formatter.parseDateTime(from);
        startMonth = startMonth.minusMonths(1);
        return DateUtils.getStringDate(getMinDate(startMonth));
    }

    public static Map<Long, Long> getPreceedingMonth(String from) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        Map<Long, Long> ranges = new TreeMap<Long, Long>();
        DateTime startMonth = formatter.parseDateTime(from);
        startMonth = startMonth.minusMonths(1);
        ranges.put(getMinDate(startMonth), getMaxDate(startMonth));
        return ranges;
    }

    public static Map<Long, Long> getAllMonthsIncludingPrecedingMonth(String from, String to) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        Map<Long, Long> ranges = new TreeMap<Long, Long>();

        DateTime startMonth = formatter.parseDateTime(from);
        DateTime endMonth   = formatter.parseDateTime(to);

        ranges.put(startMonth.getMillis(), getMaxDate(startMonth));
        startMonth = startMonth.plusMonths(1);

        DateTime currentMonth = null;
        while ((startMonth.isBefore(endMonth) || startMonth.isEqual(endMonth))){
            currentMonth = startMonth;
            startMonth = startMonth.plusMonths(1);
            ranges.put(getMinDate(currentMonth), getMaxDate(currentMonth));
        }
        if (currentMonth != null) {
            ranges.put(getMinDate(currentMonth), endMonth.getMillis());
        }

        //for preceding month
        startMonth = formatter.parseDateTime(from);
        startMonth = startMonth.minusMonths(1);
        ranges.put(getMinDate(startMonth), getMaxDate(startMonth));


        return ranges;
    }

    public static String getPrecedingMonthString(String from) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        DateTime startMonth = formatter.parseDateTime(from);
        startMonth = startMonth.minusMonths(1);
        Long date = DateUtils.getMinDate(startMonth);
        String formattedDate = DateUtils.getMonthYearForDate(DateUtils.getTimeFromDateWrtTimeZone(DateUtils.getStringDate(date)));

        return formattedDate;
    }

    public static Map<Long, Long> getAllMonthsNew(String from, String to) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        Map<Long, Long> ranges = new TreeMap<Long, Long>();

        DateTime startMonth = formatter.parseDateTime(from);
        DateTime endMonth = formatter.parseDateTime(to);

        while ((startMonth.isBefore(endMonth) || startMonth.isEqual(endMonth))) {

            Long currentStartMonth = getMinDate(startMonth) < startMonth.getMillis() ? startMonth.getMillis() : getMinDate(startMonth);
            Long currentEndMonth = getMaxDate(startMonth) > endMonth.getMillis() ? endMonth.getMillis() : getMaxDate(startMonth);

            ranges.put(currentStartMonth, currentEndMonth);
            startMonth = getDateTime(currentEndMonth).plusDays(1);
        }
        return ranges;
    }

    public static Map<Long, Long> getAllQuarters(String from, String to) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        Map<Long, Long> trendingMap = new TreeMap<Long, Long>();

        DateTime startDate = formatter.parseDateTime(from);
        DateTime endDate   = formatter.parseDateTime(to);
        String startYear   = from.split("-")[0];

        int quarter = getQuarter(startDate);
        Map<String,String> quartersMap = quarterMap.get(String.valueOf(quarter));

        DateTime quarterStartDate = formatter.parseDateTime(startYear + quartersMap.keySet().iterator().next());
        DateTime quarterEndDate   = formatter.parseDateTime(startYear + quartersMap.values().iterator().next());

        boolean sameQuarterFlag;
        DateTime tempQuarterEndDate;
        if (quarterEndDate.isAfter(endDate) || quarterEndDate.isEqual(endDate)) {
            tempQuarterEndDate = endDate;
            sameQuarterFlag = true;
        } else {
            tempQuarterEndDate = quarterEndDate;
            sameQuarterFlag = false;
        }
        trendingMap.put(startDate.getMillis(), tempQuarterEndDate.getMillis());

        while ((startDate.isBefore(endDate) || startDate.isEqual(endDate)) && !sameQuarterFlag) {
            quarter     =  getQuarter(quarterStartDate.plusMonths(3));
            quartersMap = quarterMap.get(String.valueOf(quarter));

            String currentYear    = String.valueOf(quarterStartDate.plusMonths(3).getYear());
            String currentDate    = currentYear + quartersMap.keySet().iterator().next();

            startDate          = formatter.parseDateTime(currentDate).plusMonths(3);
            quarterStartDate   = formatter.parseDateTime(currentDate);
            quarterEndDate     = formatter.parseDateTime(currentYear + quartersMap.values().iterator().next());
            trendingMap.put(quarterStartDate.getMillis(), quarterEndDate.getMillis());

        }
        if (!sameQuarterFlag) {
            trendingMap.put(quarterStartDate.getMillis(), endDate.getMillis());
        }
        return trendingMap;
    }

    public static Map<Long, Long> getAllQuartersNew(String from, String to) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        Map<Long, Long> trendingMap = new TreeMap<Long, Long>();

        DateTime startDate = formatter.parseDateTime(from);
        DateTime endDate = formatter.parseDateTime(to);

        while ((startDate.isBefore(endDate) || startDate.isEqual(endDate))) {
            int quarter = getQuarter(startDate);
            String startYear = String.valueOf(startDate.getYear());
            Map<String, String> quartersMap = quarterMap.get(String.valueOf(quarter));

            DateTime currentStartDate = formatter.parseDateTime(startYear + quartersMap.keySet().iterator().next());
            DateTime currentEndDate = formatter.parseDateTime(startYear + quartersMap.values().iterator().next());

            DateTime quarterStartDate = startDate.isAfter(currentStartDate) ? startDate : currentStartDate;
            DateTime quarterEndDate = endDate.isBefore(currentEndDate) ? endDate : currentEndDate;

            trendingMap.put(quarterStartDate.getMillis(), quarterEndDate.getMillis());
            startDate = quarterEndDate.plusDays(1);
        }

        return trendingMap;
    }

    public static int getQuarter(DateTime startDate) {
        int quarter;
        int monthOfYear = startDate.getMonthOfYear();
        if (monthOfYear % 3 == 0)
            quarter = (monthOfYear / 3);
        else
            quarter = (monthOfYear / 3) + 1;
        return quarter;
    }

    public static Map<Long, Long> getAllYears(String from, String to) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        Map<Long, Long> trendingMap = new TreeMap<Long, Long>();

        DateTime startDate = formatter.parseDateTime(from);
        DateTime endDate   = formatter.parseDateTime(to);

        String startYear      = from.split("-")[0];
        String endDateForYear = startYear + "-12-31";
        boolean sameYearFlag;
        DateTime tempQuarterEndDate;
        if (formatter.parseDateTime(endDateForYear).isAfter(endDate) || formatter.parseDateTime(endDateForYear).isEqual(endDate)) {
            tempQuarterEndDate = endDate;
            sameYearFlag = true;
        } else {
            tempQuarterEndDate = formatter.parseDateTime(endDateForYear);
            sameYearFlag = false;
        }
        trendingMap.put(startDate.getMillis(), tempQuarterEndDate.getMillis());

        DateTime currentStartDate = null;
        while ((startDate.isBefore(endDate) || startDate.isEqual(endDate)) && !sameYearFlag) {
            startDate          = startDate.plusYears(1);
            String currentYear = String.valueOf(startDate.getYear());

            currentStartDate         = formatter.parseDateTime(currentYear + "-01-01");
            DateTime currentEndDate  = formatter.parseDateTime(currentYear + "-12-31");

            if (startDate.getYear() == endDate.getYear()) {
                break;
            }
            trendingMap.put(currentStartDate.getMillis(), currentEndDate.getMillis());
        }
        if (currentStartDate != null && !sameYearFlag) {
            trendingMap.put(currentStartDate.getMillis(),endDate.getMillis());
        }

        return trendingMap;
    }

    public static Map<Long, Long> getAllYearsNew(String from, String to) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withZone(new FixedDateTimeZone("GMT", "GMT", 0, 0));
        Map<Long, Long> trendingMap = new TreeMap<Long, Long>();

        DateTime startDate = formatter.parseDateTime(from);
        DateTime endDate = formatter.parseDateTime(to);

        while ((startDate.isBefore(endDate) || startDate.isEqual(endDate))) {
            String startYear = String.valueOf(startDate.getYear());

            DateTime currentStartDate = formatter.parseDateTime(startYear + "-01-01");
            DateTime currentEndDate = formatter.parseDateTime(startYear + "-12-31");

            DateTime yearStartDate = startDate.isAfter(currentStartDate) ? startDate : currentStartDate;
            DateTime yearEndDate = endDate.isBefore(currentEndDate) ? endDate : currentEndDate;

            trendingMap.put(yearStartDate.getMillis(), yearEndDate.getMillis());
            startDate = yearEndDate.plusDays(1);
        }

        return trendingMap;
    }

    public static long getMaxDate(DateTime date) {
        return date.dayOfMonth().withMaximumValue().getMillis();
    }

    public static long getMinDate(DateTime date) {
        return date.dayOfMonth().withMinimumValue().getMillis();
    }

    public static String getStringForDate(Date time) {
        DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        sqlDateFormat.setTimeZone(timeZone);
        return sqlDateFormat.format(time);
    }

    public static String getMonthYearForDate(long date) {
        SimpleDateFormat monthYearformatter = new SimpleDateFormat("MMM-yyyy");

        monthYearformatter.setTimeZone(timeZone);
        return monthYearformatter.format(date);
    }
    public static String getMonthYearForDate(String date) {
        SimpleDateFormat monthYearformatter = new SimpleDateFormat("MMM-yyyy");
        Date date1 =getDateTime(date).toDate();

        monthYearformatter.setTimeZone(timeZone);
        return monthYearformatter.format(date1);
    }
    public static String getMonthForDate(long date) {
        SimpleDateFormat monthFormatter = new SimpleDateFormat("MMM");
        monthFormatter.setTimeZone(timeZone);
        return monthFormatter.format(date);
    }

    public static String getMonthInitialForDate(String date) {
        return getMonthForDate(getTimeFromDateWrtTimeZone(date));
    }

    public static String getESMonthYearForDate(long date) {
        SimpleDateFormat esMonthYearFormatter = new SimpleDateFormat("yyyy-MM");

        esMonthYearFormatter.setTimeZone(timeZone);
        return esMonthYearFormatter.format(date);
    }

    public static String getYear(String from) {
        return from.split(Constants.DASH)[0];
    }

    public static class MonthRange {
        public String start;
        public String end;
        public String quarterYear;

        public MonthRange(int start, int year, MonthRangeType range) {
            Calendar calendar = Calendar.getInstance(timeZone);
            calendar.set(year, start * range.getRange(), 1);
            this.start = getStringForDate(calendar.getTime());
            calendar.set(year, start * range.getRange() + range.getRange() - 1, 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            this.end = getStringForDate(calendar.getTime());
            this.quarterYear = range == MonthRangeType.EACH_MONTH ? getMonthYearForDate(calendar.getTimeInMillis()) : "Q" + (start + 1) + "-" + year;
        }

        public enum MonthRangeType {
            EACH_MONTH(1), QUARTER(3), HALF_YEARLY(6), YEARLY(12);

            int range;

            MonthRangeType(int range) {
                this.range = range;
            }

            public int getRange() {
                return this.range;
            }
        }
    }

    public static class MonthRangeDate implements Iterator<MonthRange> {

        Calendar dateFrom;
        Calendar dateTo;
        int startYear;
        int endYear;
        int startQuarter;
        int endQuarter;
        MonthRange.MonthRangeType rangeType;

        public MonthRangeDate(String dateFrom, String dateTo, MonthRange.MonthRangeType rangeType) {
            this.dateFrom = getCalendar(getTimeFromDateWrtTimeZone(dateFrom));
            this.dateTo = getCalendar(getTimeFromDateWrtTimeZone(dateTo));
            startQuarter = this.dateFrom.get(Calendar.MONTH) / rangeType.getRange();
            endQuarter = this.dateTo.get(Calendar.MONTH) / rangeType.getRange();
            startYear = this.dateFrom.get(Calendar.YEAR);
            endYear = this.dateTo.get(Calendar.YEAR);
            this.rangeType = rangeType;
        }

        @Override
        public boolean hasNext() {
            if (startYear > endYear) return false;
            if (startQuarter > endQuarter && startYear == endYear) return false;
            return true;
        }

        @Override
        public MonthRange next() {
            MonthRange range = new MonthRange(startQuarter, startYear, rangeType);
            int val = startQuarter + 1;
            startYear = startYear + val / (12 / rangeType.getRange());
            startQuarter = val % (12 / rangeType.getRange());
            return range;
        }

        @Override
        public void remove() {

        }
    }

    public static String getAdjustedFromDate(String fromDate,String  toDate) {
        fromDate = getPeriodMonth(fromDate);
        toDate = getPeriodMonth(toDate);

        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            DateTime dateTime_fromDate = new DateTime(df.parse(fromDate).getTime());
            DateTime dateTime_toDate = new DateTime(df.parse(toDate).getTime());
            if (getYears(dateTime_fromDate, dateTime_toDate) >= 1) {
                dateTime_fromDate = dateTime_toDate.minusYears(1);
            }
            return df.format(new Date(dateTime_fromDate.getMillis()));
        } catch (Exception ex) {
            throw new RuntimeException("Dates could not be parsed fromDate: " + fromDate + " toDate: " + toDate);
        }
    }

    public static int getYears(DateTime date1,DateTime date2) {
        return Years.yearsBetween(date1, date2).getYears();
    }
    public static String getPeriodMonth(String period) {
        String[] tokens = period.split("-");
        return tokens[0]+"-"+tokens[1]+"-15";
    }

    public static String getStartingDateOfMonth(String period) {
        String[] tokens = period.split("-");
        return tokens[0]+"-"+tokens[1]+"-01";
    }

    /**
     * This method returns the set of all months between fromDate to toDate,date format: yyyy-MM-dd format
     * @param fromDate
     * @param toDate
     * @return set of months
     */
    public static Set<String> getMonthsBetween(String fromDate, String toDate) {
        Set<String> monthsSet = new HashSet<String>();

        Map<Long, Long> monthMap = DateUtils.getAllMonths(fromDate, toDate);
        for (Long month : monthMap.values()) {
            Date dt = new Date(month);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            monthsSet.add(dateFormat.format(dt));

        }

        return monthsSet;
    }

    public static String getStringDate(Long date){
        Date from = new Date(date);
        return DateUtils.getStringForDate(from);
    }

    public static String getMonthYearValueForDate(long date) {
        SimpleDateFormat monthYearValueformatter = new SimpleDateFormat("MM-yyyy");
        monthYearValueformatter.setTimeZone(timeZone);
        return monthYearValueformatter.format(date);
    }

    public static Integer getAge(String latestDate,String oldestDate){
        Calendar reportingToDate= DateUtils.getCalendar(latestDate);
        Calendar ageYearDate= DateUtils.getCalendar(oldestDate);

        int diff=reportingToDate.get(Calendar.YEAR) - ageYearDate.get(Calendar.YEAR);
        if ((reportingToDate.get(Calendar.MONTH) > ageYearDate.get(Calendar.YEAR)) ||
                (reportingToDate.get(Calendar.MONTH) == ageYearDate.get(Calendar.MONTH) && reportingToDate.get(Calendar.DATE) >
                        ageYearDate.get(Calendar.DATE))){
            diff--;
        }
        return diff;
    }
    public static Integer getAge(Long latestDate,Long oldestDate){
        return getAge(getStringDate(latestDate),getStringDate(oldestDate));
    }

    /**
     *
     * @param state
     * @return Map of all the months between fromDate to toDate
     */
    public static Map<Long, Long> getMonths(final RequestState state) {
        return DateUtils.getAllMonths(state.periodFrom(), state.periodTo());
    }

    /*
    * This function substract minusYear (provided) with refDate,
    * and give date in form of YYyy-mm-dd
    * */
    public static String minusDate(String refDate,Integer minusYear){
        DateTime reportingToDate= DateUtils.getDateTime(refDate);
        return DateUtils.getStringForDate(reportingToDate.minusYears(minusYear).toDate());
    }

    public static int getMonthsDifference(String date1, String date2) {
        Calendar calendar1=getCalendar(date1);
        Calendar calendar2=getCalendar(date2);

        int diffYear = calendar2.get(Calendar.YEAR) - calendar1.get(Calendar.YEAR);

        return diffYear * 12 + calendar2.get(Calendar.MONTH) - calendar1.get(Calendar.MONTH);
    }
    public static List<String> getYearMonthsBetween(String fromDate , String toDate){
        List<String> yearMonthStr = new ArrayList<String>();
        DateTime startMonth = DateUtils.getDateTime(fromDate);
        DateTime endMonth = DateUtils.getDateTime(toDate);
        while ((startMonth.isBefore(endMonth) || startMonth.isEqual(endMonth))) {
            Long currentStartMonth = DateUtils.getMinDate(startMonth) < startMonth.getMillis() ? startMonth.getMillis() : DateUtils.getMinDate(startMonth);
            yearMonthStr.add(DateUtils.getESMonthYearForDate(currentStartMonth));
            Long currentEndMonth = getMaxDate(startMonth) > endMonth.getMillis() ? endMonth.getMillis() : getMaxDate(startMonth);
            startMonth = getDateTime(currentEndMonth).plusDays(1);
        }
        return yearMonthStr;
    }
    public static Long longDateMY(String monthYear){
        DateFormat formatter;
        SimpleDateFormat esMonthYearFormatter = new SimpleDateFormat("yyyy-MM");

        esMonthYearFormatter.setTimeZone(timeZone);
        Date date = null;
        try {
            date = esMonthYearFormatter.parse(monthYear);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return Long.MIN_VALUE;
        }
    }

    public static Long getMaxDateMY(String monthYear){
        Long startDate = longDateMY(monthYear);
        return getMaxDate(getDateTime(startDate));
    }

    public static Long getMinDateMY(String monthYear){
        Long startDate = longDateMY(monthYear);
        return getMinDate(getDateTime(startDate));
    }
    public static boolean isBefore(String date,String compareTo){
        return getDateTime(date).isBefore(getDateTime(compareTo));
    }

    public static String convertUStoSqlDateFormat(String USdateFormat){
        DateFormat usDateFormat = new SimpleDateFormat("MM-dd-yyyy");

        DateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String targetDate = "";
        Date date;
        try {
            if (USdateFormat.matches("([0-9]{2})-([0-9]{2})-([0-9]{4})")){
                date = usDateFormat.parse(USdateFormat);
                targetDate = sqlDateFormat.format(date);
            }else {
                targetDate = USdateFormat;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetDate;
    }

    public static String getMonthFieldName(long time) {
        Calendar cal = DateUtils.getCalendar(time);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Integer.toString(cal.get(Calendar.MONTH) + 1)).append("-").append(Integer.toString(cal.get(Calendar.YEAR)));
        return stringBuilder.toString();
    }

    public static boolean isLeapYear(Integer year){
        if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) return true;
        else return false;
    }
}
