package zakipointCore.commonUtils

import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes
import org.springframework.context.ApplicationContext

/**
 * Created by sktamang on 9/24/15.
 */
class SpringBeanUtils {
    public static ApplicationContext getApplicationContext() {
        return (ApplicationContext) ServletContextHolder.getServletContext().getAttribute(GrailsApplicationAttributes.APPLICATION_CONTEXT);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String beanName) {
        if (!doesBeanExist(beanName)) {
            throw new IllegalStateException("No bean found for: $beanName");
        } else return (T) applicationContext.getBean(beanName);
    }

    public static boolean doesBeanExist(String beanName) {
        return applicationContext.containsBean(beanName);
    }
}
