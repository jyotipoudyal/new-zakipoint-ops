package zakipointCore.commonUtils;

import com.configurations.config.ConfigurationFactory;
import org.elasticsearch.index.query.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 9/24/15.
 */
public class LevelUtils {
    public static List<String> getAllLevels(SearchRequest request) {
        List<String> levels=new ArrayList<String>();
        levels.addAll(getLevels(request));
        return levels;
    }

    private static List<String> getLevels(SearchRequest request) {
        return getLevels(request.requestParameters);
    }
    public static List<String> getLevels(Map<String,String> request) {
        return getLevels(getClientId(request));
    }
    public static String getClientId(Map<String,String> request){
        return request.containsKey("msRequest") ? request.get("clientId") : request.get("clientId");
    }

    public static List<String> getLevels(RequestState request) {
        return getLevels(request.request);
    }

    public static List<String> getLevels(String clientId) {
            return ConfigurationFactory.clientWiseLOA.get(clientId);
    }
    public static void setLevelFilter(RequestState state, AndFilterBuilder andFilter, Long month) {
        for (FilterBuilder lvlFilter : getLevelFilters(state, month)) {
            if(andFilter == null)
                andFilter = new AndFilterBuilder();
            andFilter.add(lvlFilter);
        }
    }


    /*LOA withOut Switching*/
    public static void setLOAFilter(RequestState state, AndFilterBuilder andFilter) {
        List<String> level = /*isWorkersCompensationReport(state) ? workersCompensationLevels : */getLevels(state);
        for (String loa : level) {
            if (state.request.hasParameter(loa) && !state.request.get(loa).isEmpty()) {
                String[] values = RequestUtils.getArrayRequest(state.request.get(loa));
                TermsFilterBuilder loaFilter = FilterBuilders.termsFilter(loa + "Id", values);
                andFilter.add(loaFilter);
            }
        }

    }

    public static void setLOAFilter(RequestState state, BoolFilterBuilder boolFilterBuilder) {
        List<String> level = /*isWorkersCompensationReport(state) ? workersCompensationLevels : */getLevels(state);
        for (String loa : level) {
            if (state.request.hasParameter(loa) && !state.request.get(loa).isEmpty()) {
                String[] values = RequestUtils.getArrayRequest(state.request.get(loa));
                TermsFilterBuilder loaFilter = FilterBuilders.termsFilter(loa + "Id", values);
                boolFilterBuilder.must(loaFilter);
            }
        }
    }


    public static List<FilterBuilder> getLevelFilters(RequestState state, Long month) {
        List<FilterBuilder> levelFilters = new ArrayList<FilterBuilder>();
        for (String level : getLevels(state)) {
            if (state.request.hasParameter(level)) {
                String[] filterValue = RequestUtils.getArrayRequest(state.request.get(level));
                levelFilters.add(getNestedFilter(month, level, filterValue));
            }
        }
        return levelFilters;
    }


    public static boolean hasLOA(RequestState state) {
        //Check if level filter exists
        for (String level : getLevels(state)) {
            if (state.request.hasParameter(level) && !state.request.get(level).isEmpty() || state.request.hasParameter("cohortId")) {
                return true;
            }
        }
        return false;
    }


    public static FilterBuilder getNestedFilter(Long month, String field, String... filterValue ) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.rangeFilter(field + ".fromDate").lte(month));
        andFilter.add(FilterBuilders.rangeFilter(field + ".toDate").gte(month));
        andFilter.add(FilterBuilders.termsFilter(field + ".id", filterValue));

        return FilterBuilders.nestedFilter(field, andFilter);
    }
    public static FilterBuilder getNestedFilter(Object month, String field, String... filterValue ) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.rangeFilter(field + ".fromDate").lte(month));
        andFilter.add(FilterBuilders.rangeFilter(field + ".toDate").gte(month));
        andFilter.add(FilterBuilders.termsFilter(field + ".id", filterValue));

        return FilterBuilders.nestedFilter(field, andFilter);
    }

    public static FilterBuilder getNestedFilter(RequestState state, String field, String... filterValue ) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.rangeFilter(field + ".fromDate").lte(state.periodTo()));
        andFilter.add(FilterBuilders.rangeFilter(field + ".toDate").gte(state.periodFrom()));
        andFilter.add(FilterBuilders.termsFilter(field + ".id", filterValue));

        return FilterBuilders.nestedFilter(field, andFilter);
    }

    public static void setLevelFilter(RequestState state, AndFilterBuilder andFilter, String month) {
        for (FilterBuilder lvlFilter : getLevelFilters(state, month)) {
            andFilter.add(lvlFilter);
        }
    }

    public static void setLevelFilterOnlyId(RequestState state, AndFilterBuilder andFilter) {
        for (FilterBuilder lvlFilter : getLevelFiltersOnlyId(state)) {
            andFilter.add(lvlFilter);
        }
    }
    public static void setLevelOnlyFilter(RequestState state, AndFilterBuilder andFilter, String month) {
        for (FilterBuilder lvlFilter : getLevelFilters(state, month)) {
            andFilter.add(lvlFilter);
        }

    }

    private static List<FilterBuilder> getLevelFilters(RequestState state) {
        List<FilterBuilder> levelFilters = new ArrayList<FilterBuilder>();
        for (String level : getLevels(state)) {
            if (state.request.hasParameter(level)) {
                String[] filterValue = RequestUtils.getArrayRequest(state.request.get(level));
                levelFilters.add(getNestedFilter(state, level, filterValue));
            }
        }
        return levelFilters;
    }

    private static List<FilterBuilder> getLevelFilters(RequestState state, String month) {
        List<FilterBuilder> levelFilters = new ArrayList<FilterBuilder>();
        for (String level : getLevels(state)) {
            if (state.request.hasParameter(level)) {
                String[] filterValue = RequestUtils.getArrayRequest(state.request.get(level));
                levelFilters.add(getNestedFilter(month, level, filterValue));
            }
        }
        return levelFilters;
    }

    private static List<FilterBuilder> getLevelFiltersOnlyId(RequestState state) {
        List<FilterBuilder> levelFilters = new ArrayList<FilterBuilder>();
        for (String level : getLevels(state)) {
            if (state.request.hasParameter(level)) {
                String[] filterValue = RequestUtils.getArrayRequest(state.request.get(level));
                levelFilters.add(getNestedFilterOnlyId(level, filterValue));
            }
        }
        return levelFilters;
    }


    public static FilterBuilder getNestedFilter(String month, String field, String... filterValue ) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        Long longDate = null;
        if (month != null)
            longDate = DateUtils.getTimeFromDateWrtTimeZone(month);
        andFilter.add(FilterBuilders.rangeFilter(field + ".fromDate").lte(longDate));
        andFilter.add(FilterBuilders.rangeFilter(field + ".toDate").gte(longDate));
        andFilter.add(FilterBuilders.termsFilter(field + ".id", filterValue));

        return FilterBuilders.nestedFilter(field, andFilter);
    }

    public static FilterBuilder getNestedFilterOnlyId(String field, String... filterValue ) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.termsFilter(field + ".id", filterValue));
        return FilterBuilders.nestedFilter(field, andFilter);
    }


    public static void setUMLoa(RequestState state, AndFilterBuilder andFilter) {
        for (String loa : LevelUtils.getLevels(state)) {
            if (state.request.hasParam(loa) && !state.request.param(loa).isEmpty()) {
                String[] values = RequestUtils.getArrayRequest(state.request.param(loa));
                TermsFilterBuilder loaFilter = FilterBuilders.termsFilter("um." + loa + "Id", values);
                andFilter.add(loaFilter);
            }
        }
    }

    public static void setHighCostLevelFilter(RequestState state, AndFilterBuilder andFilter) {
        for (FilterBuilder lvlFilter : getLevelFilters(state)) {
            andFilter.add(lvlFilter);
        }
    }


    public static void setNestedFieldLoa(RequestState state, AndFilterBuilder andFilter, String type) {
        for (String loa : LevelUtils.getLevels(state)) {
            if (state.request.hasParam(loa) && !state.request.param(loa).isEmpty()) {
                String[] values = RequestUtils.getArrayRequest(state.request.param(loa));
                TermsFilterBuilder loaFilter = FilterBuilders.termsFilter(type + "." + loa + "Id", values);
                andFilter.add(loaFilter);
            }
        }
    }

    public static void setLevelAndUdfFilter(RequestState state, AndFilterBuilder andFilter,String month) {
        for (FilterBuilder lvlFilter : getLevelFilters(state, month)) {
            andFilter.add(lvlFilter);
        }
    }
}
