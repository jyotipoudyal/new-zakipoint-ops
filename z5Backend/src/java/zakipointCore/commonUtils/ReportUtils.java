package zakipointCore.commonUtils;

import com.configurations.BenchmarkFormatter;
import com.configurations.ResponseFormatter;
import org.codehaus.groovy.grails.web.converters.exceptions.ConverterException;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 9/25/15.
 */
public class ReportUtils {

    public static JSONObject generateReport(Map<String,String> request, ReportGenerator makaluService) throws ConverterException{
        Map<String,String> req1=new HashMap<String, String>(request);
        JSONObject otherStr=makaluService.generate(req1);

        if(hasIncludes(request,"bob")){
            otherStr.put("bob", BOBGenerator.generate(request, makaluService));
        }
        JSONObject finalObj = otherStr;
        JSONObject benchmark = null;
        Boolean includesBenchmark=containsBenchmark(request);
        if(includesBenchmark
//                && ConfigurationFactory.getBenchmarkValue(request.get("clientId"), request.get(BenchmarkGenerator.BENCHMARK_TYPE)).equals(true)
                ){
            benchmark = BenchmarkGenerator.generate(request,makaluService);
            finalObj.put("benchmark",benchmark);
        }
        if(isFormatterAvailable(request)){
            finalObj=formatResponse(otherStr,request);
        }
        if(benchmark!=null){
            finalObj.remove("benchmark");
            finalObj.put(request.get("_"+BenchmarkGenerator.BENCHMARK_TYPE),benchmark);
        }
        return finalObj;
    }

    private static boolean hasIncludes(Map<String, String> request,String name) {
        if(request.containsKey("includes")){
            String[] includes= RequestUtils.getArrayRequest(request.get("includes"));
            for (String include : includes) {
                if(include.equalsIgnoreCase(name)){
                    return true;
                }
            }
        }
        return false;
    }

    public  static boolean containsBenchmark(Map<String, String> request){
        if (request.containsKey("includes")){
            String[] includes= RequestUtils.getArrayRequest(request.get("includes"));
            for (String include : includes) {
                if (BenchmarkGenerator.AVAILABLE_BENCHMARKS.containsKey(include)){
                    request.put(BenchmarkGenerator.BENCHMARK_TYPE, BenchmarkGenerator.AVAILABLE_BENCHMARKS.get(include));
                    request.put("_"+BenchmarkGenerator.BENCHMARK_TYPE, include);
                    return true;
                }
            }
        }
        return false;
    }


    private static JSONObject formatResponse(JSONObject dasResponse,Map<String,String> requestMap) {
        ResponseFormatter responseFormatter= SpringBeanUtils.getBean(getBeanName(requestMap));
        if(!(responseFormatter instanceof BenchmarkFormatter) && dasResponse.containsKey("benchmark")){
            dasResponse.remove("benchmark");
        }
        return responseFormatter.format(dasResponse,requestMap);
    }

    private static String getBeanName(Map<String, String> requestMap) {
        String reportName=requestMap.containsKey("report_name") ? requestMap.get("report_name") : requestMap.get("report");
        return reportName+".formatters";
    }

    private static boolean isFormatterAvailable(Map<String, String> request) {
        return SpringBeanUtils.doesBeanExist(getBeanName(request));
    }


}

