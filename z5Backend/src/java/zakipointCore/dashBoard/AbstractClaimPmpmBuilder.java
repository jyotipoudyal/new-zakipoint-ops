package zakipointCore.dashBoard;

import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.*;

import java.util.Map;

/**
 * Created by sktamang on 9/24/15.
 */
public abstract class AbstractClaimPmpmBuilder implements SummaryQueryBuilder {


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {

        MultiSearchRequestBuilder multiSearchRequestBuilder = QueryUtils.buildMultiSearch(client);
        multiSearchRequestBuilder.add(memberMonthsBuilder(state, client));
        multiSearchRequestBuilder.add(medicalOrPharmacy(state, client));
        return multiSearchRequestBuilder;
    }


    protected SearchRequestBuilder memberMonthsBuilder(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        FiltersAggregationBuilder filters = (FiltersAggregationBuilder) CommonQueries.memberMonthsAggs(state, "memberMonths");
        builder.addAggregation(filters);
        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }


    protected SearchRequestBuilder medicalOrPharmacy(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearch(client, state.request);
        builder.setSize(0);
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        LevelUtils.setLOAFilter(state,andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));

        FiltersAggregationBuilder filters = AggregationBuilders.filters("medicalTrends");
        Map<String, String> range = DateUtils.getAllMonthsString(state.periodFrom(), state.periodTo());

        for (Map.Entry<String, String> month : range.entrySet()) {
            AndFilterBuilder filter = new AndFilterBuilder();
            FilterUtils.monthWiseServiceDateFilter(state, filter, month.getKey(), month.getValue());
            filter.add(FilterUtils.paidMonthFilter(state, month.getKey(), month.getValue()));
            filters.filter(DateUtils.getMonthOnlyForDate(month.getValue()), filter);
        }

        getMedicalrPharmacyClaimsRequestBuilder(builder, filters);
        return builder;
    }

    public abstract SearchRequestBuilder getMedicalrPharmacyClaimsRequestBuilder(SearchRequestBuilder builder, FiltersAggregationBuilder filters);

}
