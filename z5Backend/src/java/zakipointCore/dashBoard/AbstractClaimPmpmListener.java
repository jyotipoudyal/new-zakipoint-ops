package zakipointCore.dashBoard;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 9/28/15.
 */
public abstract class AbstractClaimPmpmListener extends AbstractSummaryListener {

    protected void handleResponse(SearchResponse response) {
        for (Aggregation aggregation : response.getAggregations()) {
            if (aggregation.getName().contains("memberMonths")) {
                handleMemberMonths(aggregation);
            } else hanleMetrics(aggregation);
        }
    }

    protected abstract void hanleMetrics(Aggregation aggregation);

    private void handleMemberMonths(Aggregation aggregation) {
        Record wrapper = getRecord(state, "memberMonths");
        Filters filters = (Filters) aggregation;
        for (Filters.Bucket bucket : filters.getBuckets()) {
            wrapper.recordMap.put(bucket.getKey(), bucket.getDocCount());
        }
    }

    @Override
    protected Record getRecord(RequestState state, String id) {
        return super.getRecord(state, id);
    }
}
