package zakipointCore.dashBoard.populationRiskByTopChronicConditions;

import com.configurations.utils.ChronicConditionEnum;
import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.NestedBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.*;
import zakipointCore.drill.ConsecutiveDateGenerator;

import java.util.HashMap;

/**
 * Created by sktamang on 5/17/16.
 */
public class PopulationRiskByTopChronicConditionBuilder implements SummaryQueryBuilder {


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {

        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(query(state, client,false,true,"",state));

        double memberCount = 0.0d;
        double memberMonth = 0.0d;

        state.hits.put("memberCount", CommonQueries.getMemberCount(state, client, memberCount));
        state.hits.put("memberMonth", CommonQueries.getTotalMemberMonth(state, client, memberMonth));

        threeAndOneMonthsAddedMemberBuilder(builder,state,client);
        return builder;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }

    private void threeAndOneMonthsAddedMemberBuilder(MultiSearchRequestBuilder builder, RequestState state, Client client) {

        for(int i=0;i<2;i++) {
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.requestParameters = new HashMap<>(state.request.requestParameters);

            String aggName=(i==0)?"_three" :"_one";
            String reporintToDate=(i==0)? ConsecutiveDateGenerator.getThreeOrOneMonthsBackDate(state.request.get("reportingTo"), true) : ConsecutiveDateGenerator.getThreeOrOneMonthsBackDate(state.request.get("reportingTo"), false);
            state.hits.put(aggName,reporintToDate);
            searchRequest.requestParameters.put("reportingTo", reporintToDate);
            searchRequest.requestParameters.put("reportingToOriginal", state.periodTo());
            RequestState stateThreeMonths = new RequestState("populationRiskByTopChronicCondition", "", searchRequest, null);
            builder.add(query(stateThreeMonths, client,true,false,aggName,state));
        }
    }


    public SearchRequestBuilder query(RequestState state1, Client client, boolean isThreeMonthOrOne, boolean isNormal, String aggName,RequestState stateOriginal) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state1.request, MemberSearch.NAME);

        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterUtils.eligibleDateFilter(state1.periodTo()));
        if(isThreeMonthOrOne)andFilter.add(FilterUtils.eligibleDateFilter(state1.request.get("reportingToOriginal")));
        LevelUtils.setLevelFilter(state1,andFilter, state1.periodTo());

        CommonQueries.caseMemberFilter(state1, client, andFilter);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilter));

        totalPaidAmountAggregation(state1, builder,isThreeMonthOrOne,isNormal,aggName,stateOriginal);

        if(isNormal) {
            projectScoreAggregation(state1, builder);
            overAllTotalPaidAmountAggregation(state1, builder);
        }
        return builder;
    }

    private void overAllTotalPaidAmountAggregation(RequestState state, SearchRequestBuilder builder) {
        builder.addAggregation(memberAmountAggregator(state));
    }


    private void totalPaidAmountAggregation(RequestState state, SearchRequestBuilder builder,boolean isThreeMonthOrOne,boolean isNormal,String aggName,RequestState stateOriginal) {

        FiltersAggregationBuilder qmFilters = AggregationBuilders.filters("qmFilters"+aggName);

        for (Integer metricCode : ChronicConditionEnum.ChronicConditionMetrics.getAllMetricCode()) {
            AndFilterBuilder andFilterBuilder = qmELigFilter(state, metricCode);
            if(isThreeMonthOrOne){
                AndFilterBuilder andFilterBuilder1=qmELigFilter(stateOriginal, metricCode);
                andFilterBuilder.add(andFilterBuilder1);
            }
            qmFilters.filter(metricCode.toString(), andFilterBuilder);
        }

        builder.addAggregation(qmFilters);
        if(isNormal)qmFilters.subAggregation(memberAmountAggregator(state));
    }


    private void projectScoreAggregation(RequestState state, SearchRequestBuilder builder) {
        String riskScoreFieldProspective = state.request.hasParam("group") ? "prospectiveTotalDoubleNormalizedToGroup" : "prospectiveTotalDoubleNormalizedToBob";
        String riskScoreFieldConcurrent = state.request.hasParam("group") ? "concurrentTotalDoubleNormalizedToGroup" : "concurrentTotalDoubleNormalizedToBob";
        for (Integer metricCode : ChronicConditionEnum.ChronicConditionMetrics.getAllMetricCode()) {
            String ProspectiveRawField = "ProspectiveRaw";  //this is the percentContribution * prospectiveTotal
            String concurrentRawField = "ConcurrentRaw";

            AndFilterBuilder andFilterBuilder = qmELigFilter(state, metricCode);
            builder.addAggregation(AggregationBuilders.filter(metricCode.toString()).filter(andFilterBuilder)
                    .subAggregation(AggregationBuilders.sum("projectedRiskScore")
                            .field(ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(metricCode).getChronicPercentFieldName()+ProspectiveRawField))
                    .subAggregation(AggregationBuilders.sum("ConcurrentRawScore")
                            .field(ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(metricCode).getChronicPercentFieldName() + concurrentRawField))
                    .subAggregation(AggregationBuilders.avg("actualProspective").field(riskScoreFieldProspective))
                    .subAggregation(AggregationBuilders.avg("actualConcurrent").field(riskScoreFieldConcurrent))
            );

        }
    }

    private AndFilterBuilder qmELigFilter(RequestState state, Integer metricCode) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(implementQmCodeFilter(state, metricCode));
        LevelUtils.setLevelFilter(state, andFilterBuilder, state.periodTo());
        return andFilterBuilder;
    }


    private NestedBuilder memberAmountAggregator(RequestState state) {

        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        CommonQueries.paidMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        CommonQueries.serviceMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);

        return AggregationBuilders.nested("memberAmountNested").path(MemberSearch.MEMBER_AMOUNT.NAME)
                .subAggregation(AggregationBuilders.filter("memberAmountFilter").filter(memberAmountFilter)
                        .subAggregation(AggregationBuilders.sum("totalMemberPaidAmount").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT)));

    }


    public AndFilterBuilder implementQmCodeFilter(RequestState state, Object... chronicCode) {

        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermsFilter("qm.measure", chronicCode));
        qmFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));
        qmFilter.add(FilterUtils.getRangeFilter(state));
        return FilterBuilders.andFilter(FilterBuilders.nestedFilter("qm", qmFilter));
    }
}
