package zakipointCore.dashBoard.populationRiskByTopChronicConditions;

import com.configurations.utils.ChronicConditionEnum;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Map;

/**
 * Created by sktamang on 5/17/16.
 */
public class PopulationRiskByTopChronicConditionListener extends AbstractSummaryListener {


    @Override
    protected void handleResponse(SearchResponse response) {

        if(response.getAggregations().asMap().containsKey("qmFilters")) {
            Filters filters = response.getAggregations().get("qmFilters");
            metricWiseInfoCalculation(filters);

            extractProjectedRiskScore(response);

            Record record = getRecord(state, "metaInfo");
            for (Map.Entry<String, Object> pair : state.hits.entrySet()) {
                record.recordMap.put(pair.getKey(), pair.getValue());
            }

            Nested memberAmountNestedOverAll = response.getAggregations().get("memberAmountNested");
            Sum totalMemberPaidAmountOverAll = memberAmountExtractor(memberAmountNestedOverAll);
            record.recordMap.put("totalMemberPaidAmountOverAll", totalMemberPaidAmountOverAll.getValue());
        }
        else{
            for (Aggregation aggregation : response.getAggregations()) {
                String[] aggName=aggregation.getName().split("_");
                Filters filters=(Filters) aggregation;

                for (Filters.Bucket bucket : filters.getBuckets()) {
                    Record record=getRecord(state,bucket.getKey());
                    record.recordMap.put("population_"+aggName[1],bucket.getDocCount());
                }
            }
        }
    }


    private void metricWiseInfoCalculation(Filters filters) {

        for (Filters.Bucket bucket : filters.getBuckets()) {
            String metric=bucket.getKey();
            Record record=getRecord(state,bucket.getKey());
            record.recordMap.put("population",bucket.getDocCount());

            Nested memberAmountNested=bucket.getAggregations().get("memberAmountNested");
            Sum totalMemberPaidAmount = memberAmountExtractor(memberAmountNested);
            record.recordMap.put("cost",totalMemberPaidAmount.getValue());

            record.recordMap.put("metricName", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(Integer.parseInt(metric)).getMetricName());
            record.recordMap.put("metricDisplay",ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(Integer.parseInt(metric)).getMetricDisplay());
            record.recordMap.put("metricDescription", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(Integer.parseInt(metric)).getMetricDescription());
            record.recordMap.put("metricCode",metric);
        }
    }


    private Sum memberAmountExtractor(Nested memberAmountNested) {
        Filter memberAmountFilter=memberAmountNested.getAggregations().get("memberAmountFilter");
        return memberAmountFilter.getAggregations().get("totalMemberPaidAmount");
    }


    private void extractProjectedRiskScore(SearchResponse response) {

        for (Aggregation aggregation : response.getAggregations()) {
            if(aggregation instanceof Filter){
                Record record=getRecord(state,aggregation.getName());
                Filter filter=(Filter) aggregation;
                Sum projectedRiskScore=filter.getAggregations().get("projectedRiskScore");
                Sum ConcurrentRawScore=filter.getAggregations().get("ConcurrentRawScore");
                Avg actualProspective=filter.getAggregations().get("actualProspective");
                Avg actualConcurrent=filter.getAggregations().get("actualConcurrent");

                record.recordMap.put("projectedRiskScore",projectedRiskScore.getValue());
                record.recordMap.put("ConcurrentRawScore",ConcurrentRawScore.getValue());
                record.recordMap.put("normalizedScoreAvg",Double.isNaN(actualProspective.getValue()) ? 0 : actualProspective.getValue());
                record.recordMap.put("normalizedConcurrentScoreAvg",Double.isNaN(actualConcurrent.getValue()) ? 0 : actualConcurrent.getValue());
            }
        }
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        JSONObject metaInfo = jsonObject.getJSONObject("metaInfo");
        double totalMemberPaidAmountOverAll=metaInfo.getDouble("totalMemberPaidAmountOverAll");
        double memberCountOverAll=metaInfo.getDouble("memberCount");
        double avgCost= CalculatorUtils.divide(totalMemberPaidAmountOverAll,memberCountOverAll);
        double pmpm=CalculatorUtils.divide(totalMemberPaidAmountOverAll,metaInfo.getDouble("memberMonth"));
        metaInfo.put("pmpm",pmpm);

        metaInfo.put("avgCost",avgCost);

        for (Object o : jsonObject.entrySet()) {

            Map.Entry<String, Object> innerMap = (Map.Entry<String, Object>) o;
            if (!innerMap.getKey().equalsIgnoreCase("metaInfo")) {
            JSONObject object = (JSONObject) innerMap.getValue();
            object.put("percentOfPopulation", CalculatorUtils.calculatePercentage((Number) object.get("population"), (Number) state.hits.get("memberCount")));
            object.put("projectedCostConcurrent",((double)object.get("ConcurrentRawScore") * avgCost));
            object.put("projectedCostProspective",((double)object.get("projectedRiskScore") * avgCost));
            object.put("projectedCostActual",((double)object.get("normalizedScoreAvg") * pmpm * 12 * ((int)((Number) object.get("population")).doubleValue())));
            object.put("projectedCostConcurrentActual",((double)object.get("normalizedConcurrentScoreAvg") * pmpm * 12 * (Double.valueOf((int)object.get("population")))));
            object.put("newPopInLastThreeMonths",((int)object.get("population")-(int)object.get("population_three")));
            object.put("newPopInLastOneMonth",((int)object.get("population")-(int)object.get("population_one")));
            object.put("populationPerThousand",CalculatorUtils.perThousand(Double.valueOf((int)object.get("population")),metaInfo.getDouble("memberMonth")));
        }
    }
        return jsonObject;
    }
}
