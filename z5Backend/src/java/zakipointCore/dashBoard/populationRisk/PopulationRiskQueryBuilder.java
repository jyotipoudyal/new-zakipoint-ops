package zakipointCore.dashBoard.populationRisk;

import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.RequestState;


/**
 * Created by sktamang on 10/8/15.
 */
public class PopulationRiskQueryBuilder implements SummaryQueryBuilder {
    public static final String AGG_N_STATS = "stats";

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        return null;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }

//    @Override
//    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
//        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
//        Map<String, Double[]> riskDeviations = prepareRiskDeviations(state, client);
//        builder.add(riskCalulator(state, client, riskDeviations));
//        return builder;
//    }
//
//    private SearchRequestBuilder riskCalulator(RequestState state, Client clinet, Map<String, Double[]> riskDeviations) {
//        SearchRequestBuilder builder = QueryUtils.buildEndSearch(clinet, state.request);
//        builder.setTypes("MemberSearch").setSize(0);
//        for (String month : DateUtils.getMonthsBetween(state.periodFrom(), state.periodTo())) {
//            AndFilterBuilder andFilter = new AndFilterBuilder();
//            andFilter.add(FilterUtils.eligibleDateFilter(month));
//            LevelUtils.setLevelFilter(state,andFilter,month);
//            builder.addAggregation(AggregationBuilders.filter(DateUtils.getMonthOnlyForDate(month)).filter(andFilter)
//                    .subAggregation(addProspectiveTotalStatsFacets(riskDeviations.get(DateUtils.getMonthOnlyForDate(month))
//                    ))
//            );
//        }
//        return builder;
//    }
//
//    private FiltersAggregationBuilder addProspectiveTotalStatsFacets(Double[] sds) {
//        FiltersAggregationBuilder filters=AggregationBuilders.filters("riskCalculator");
//        addHighRiskPopulationStatAggs(sds[0],filters);
//        addAverageRiskPopulationStatAggs(filters);
//        addNormalRiskPopulationStatAggs(sds[1],filters);
//        addBelowRiskPopulationStatAggs(sds[1],filters);
//        return  addStats(filters);
//    }
//
//    protected void addHighRiskPopulationStatAggs(Double totalSD,FiltersAggregationBuilder filters) {
//        String aggsName = DashboardConstants.PopulationMetric.HIGH_RISK.value();
//        FilterBuilder prospectiveTotalRangeFilter = FilterBuilders.rangeFilter(MemberSearch.FIELD_PROSPECTIVE_TOTAL)
//                .gt(getLong(getBaseRiskScore() + totalSD));
//        addStatsFilter(filters, aggsName, prospectiveTotalRangeFilter);
//    }
//
//    protected void addAverageRiskPopulationStatAggs(FiltersAggregationBuilder filters) {
//        String aggsName = DashboardConstants.PopulationMetric.AVERAGE_RISK.value();
//        FilterBuilder prospectiveTotalRangeFilter = FilterBuilders.rangeFilter(MemberSearch.FIELD_PROSPECTIVE_TOTAL)
//                .gt(getBaseRiskScore());
//        addStatsFilter(filters, aggsName, prospectiveTotalRangeFilter);
//    }
//
//    protected void addNormalRiskPopulationStatAggs(Double bottomSD,FiltersAggregationBuilder filters) {
//        String aggsName = DashboardConstants.PopulationMetric.NORMAL_RISK.value();
//        FilterBuilder prospectiveTotalRangeFilter = FilterBuilders.rangeFilter(MemberSearch.FIELD_PROSPECTIVE_TOTAL)
//                .gte(getLong(bottomSD))
//                .lte(getBaseRiskScore());
//        addStatsFilter(filters, aggsName, prospectiveTotalRangeFilter);
//
//    }
//
//    private Long getLong(Double bottomSD) {
//        return Math.round(bottomSD);
//    }
//
//    protected void addBelowRiskPopulationStatAggs(Double bottomSD,FiltersAggregationBuilder filters) {
//        String aggsName = DashboardConstants.PopulationMetric.LOW_RISK.value();
//        FilterBuilder prospectiveTotalRangeFilter = FilterBuilders.rangeFilter(MemberSearch.FIELD_PROSPECTIVE_TOTAL)
//                .lt(getLong(bottomSD));
//        addStatsFilter(filters,aggsName, prospectiveTotalRangeFilter);
//    }
//
//    private Map<String, Double[]> prepareRiskDeviations(RequestState state, Client clinet) {
//        SearchRequestBuilder builder = QueryUtils.buildEndSearch(clinet, state.request);
//        builder.setSize(0).setTypes(MemberSearch.NAME);
//        FiltersAggregationBuilder filters = (FiltersAggregationBuilder) CommonQueries.memberMonthsAggs(state, "statsFilter");
//        builder.addAggregation(filters);
//        statAggs(filters);
//        return processriskDeviation(state,builder);
//    }
//
//    private Map<String, Double[]> processriskDeviation(RequestState state,SearchRequestBuilder builder) {
//        SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "populationRisk");
//        Map<String, Double[]> deviationsMap = new HashMap<String, Double[]>();
//        PopulationRiskListener.initRecordIds(state);
//        for (Aggregation aggregation : response.getAggregations()) {
//            for (Filters.Bucket bucket : ((Filters) aggregation).getBuckets()) {
//                Double[] deviations = new Double[3];
//                for (Aggregation aggregation1 : bucket.getAggregations()) {
//                    String aggsName = aggregation1.getName();
//                    ExtendedStats statAggs = ((Filter) aggregation1).getAggregations().get(AGG_N_STATS);
//                    if (aggsName.contains(DashboardConstants.FACET_NAME_TOTAL_SD)) {
//                        deviations[0] = handleIsNan(statAggs.getStdDeviation());
//                        deviations[2] = (double) statAggs.getCount();
//                    } else if (aggsName.contains(DashboardConstants.FACET_NAME_BOTTOM_SD)) {
//                        deviations[1] = handleIsNan(statAggs.getStdDeviation());
//                    }
//                }
//                deviationsMap.put(bucket.getKey(), deviations);
//                writeSDs(state, deviations, bucket.getKey());
//            }
//        }
//        return deviationsMap;
//    }
//
//    protected void writeSDs(RequestState state, Double[] sds,String recordId) {
//        if(!state.recordIds.containsKey(recordId)){
//            AbstractSummaryListener.Record wrapper1=new AbstractSummaryListener.Record(recordId);
//            wrapper1.id=recordId;
//            state.recordIds.put(recordId,wrapper1);
//        }
//        AbstractSummaryListener.Record wrapper=state.recordIds.get(recordId);
//        wrapper.recordMap.put(DashboardConstants.PopulationMetric.TOTAL_SD.value(), AmountUtils.getAmount(sds[0]));
//        wrapper.recordMap.put(DashboardConstants.PopulationMetric.BOTTOM_SD.value(), AmountUtils.getAmount(sds[1]));
//        wrapper.recordMap.put(DashboardConstants.PopulationMetric.MEMBER_COUNT.value(), (sds[2]));
//    }
//
//    private Double handleIsNan(double stdDeviation) {
//        return !Double.isNaN(stdDeviation) ? stdDeviation : DashboardConstants.DEFAULT_STD_DEVIATION;
//    }
//
//    @Override
//    public SearchRequestBuilder query(RequestState state, Client client) {
//        return null;
//    }
//
//    private Long getBaseRiskScore() {
//        return Long.valueOf(100);
//    }
//
//    protected void statAggs(FiltersAggregationBuilder filters) {
//        addTotalSDStats(filters);
//        addBottomSDStats(filters);
//    }
//
//    protected void addTotalSDStats(FiltersAggregationBuilder filters) {
//        String aggsName = DashboardConstants.FACET_NAME_TOTAL_SD;
//        addStatsAggs(filters, aggsName, FilterBuilders.matchAllFilter());
//    }
//
//    protected void addBottomSDStats(FiltersAggregationBuilder filters) {
//        String aggsName = DashboardConstants.FACET_NAME_BOTTOM_SD;
//        FilterBuilder prospectiveTotalRangeFilter = FilterBuilders.rangeFilter(MemberSearch.FIELD_PROSPECTIVE_TOTAL).lte(getBaseRiskScore());
//        addStatsAggs(filters, aggsName, prospectiveTotalRangeFilter);
//    }
//
//    protected void addStatsAggs(FiltersAggregationBuilder filters, String aggsName, FilterBuilder filterBuilder) {
//        filters.subAggregation(
//                filter(filterBuilder, aggsName).subAggregation(
//                        extendedStats(MemberSearch.FIELD_PROSPECTIVE_TOTAL, AGG_N_STATS)
//                )
//        );
//    }
//
//    protected void addStatsFilter(FiltersAggregationBuilder filters, String aggsName, FilterBuilder filterBuilder) {
//        filters.
//                filter(aggsName, filterBuilder);
//    }
//
//    protected  FiltersAggregationBuilder addStats(FiltersAggregationBuilder filters){
//        return  filters.subAggregation( extendedStats(MemberSearch.FIELD_PROSPECTIVE_TOTAL, AGG_N_STATS));
//    }
}
