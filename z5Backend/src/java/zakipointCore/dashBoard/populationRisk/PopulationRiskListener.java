package zakipointCore.dashBoard.populationRisk;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 10/8/15.
 */
public class PopulationRiskListener extends AbstractSummaryListener {

    @Override
    protected void initState() {
        if (state.recordIds == null) {
            try {
                state.recordIds = createRecordMap();
                state.contentBuilder = XContentFactory.jsonBuilder();
            } catch (Exception e) {
                onFailure(e);
            }
        }
    }

    protected void handleResponse(SearchResponse response) {
        for (Aggregation aggregation : response.getAggregations()) {
            Record wrapper = getRecord(state,aggregation.getName());
            Filters filters = ((Filter) aggregation).getAggregations().get("riskCalculator");
            for (Filters.Bucket bucket : filters.getBuckets()) {
                wrapper.recordMap.put(bucket.getKey(), bucket.getDocCount());
            }
        }
    }

    public static void initRecordIds(RequestState state1) {
        try {
            state1.recordIds = createRecordMap();
            state1.contentBuilder = XContentFactory.jsonBuilder();
        } catch (Exception e) {
        }
    }

}
