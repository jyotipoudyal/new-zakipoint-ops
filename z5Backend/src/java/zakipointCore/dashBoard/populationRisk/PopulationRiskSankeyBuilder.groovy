package zakipointCore.dashBoard.populationRisk

import com.configurations.utils.BuilderAction
import org.elasticsearch.action.search.MultiSearchRequestBuilder
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.Client
import org.elasticsearch.index.query.AndFilterBuilder
import org.elasticsearch.index.query.FilterBuilders
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.aggregations.Aggregation
import org.elasticsearch.search.aggregations.AggregationBuilders
import org.elasticsearch.search.aggregations.bucket.filter.Filter
import org.elasticsearch.search.aggregations.bucket.nested.Nested
import org.elasticsearch.search.aggregations.bucket.nested.ReverseNestedBuilder
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStats
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStatsBuilder
import zakipointCore.builder.CommonQueries
import zakipointCore.builder.SummaryQueryBuilder
import zakipointCore.commonUtils.FilterUtils
import zakipointCore.commonUtils.LevelUtils
import zakipointCore.commonUtils.QueryUtils
import zakipointCore.commonUtils.RequestState

/**
 * Created by sktamang on 6/6/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class PopulationRiskSankeyBuilder implements SummaryQueryBuilder {

    private static String PROSPECTIVE_TOTAL = "prospectiveTotal";

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(getSDForProspectiveTotal(state, client))
        dateWiseBuilder(state, client, builder);
        return builder;
    }

    @Override
    SearchRequestBuilder query(RequestState state, Client client) {
        return null
    }

    private void dateWiseBuilder(RequestState state, Client client, MultiSearchRequestBuilder builder) {

        List<RequestState> requestStateList = ConsecutiveDateGenerator.getConsecutiveRequestStates(state.request.requestParameters);
        builder.add(lastYearRiskBuilder(requestStateList.get(1), client))
    }


    SearchRequestBuilder lastYearRiskBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        eligibilityAndLevelFilter(state, client, builder)

        builder.addAggregation(AggregationBuilders.nested("nestedMaraHis").path("MaraHistoricalRiskScores")
                .subAggregation(AggregationBuilders.filter("processedDateFilter")
                .filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores.processedDate").gte(state.periodTo()).lte(state.periodTo()))
                .subAggregation(AggregationBuilders.filter("allPop").filter(FilterBuilders.matchAllFilter())
                .subAggregation(AggregationBuilders.extendedStats("stats").field("MaraHistoricalRiskScores.prospectiveTotal")))
                .subAggregation(AggregationBuilders.filter("bottomPop").filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores.prospectiveTotal").from(null).to(1))
                .subAggregation(AggregationBuilders.extendedStats("stats").field("MaraHistoricalRiskScores.prospectiveTotal")))))

        SearchResponse response = BuilderAction.getResponse(builder, "populationRiskSankey")
        Nested nestedMaraHis = response.getAggregations().get("nestedMaraHis")
        Filter processedDateFilter = nestedMaraHis.getAggregations().get("processedDateFilter")

        Map<String, Double> SDMapLast = new HashMap<>()
        for (Aggregation aggregation : processedDateFilter.getAggregations()) {
            Filter filter = (Filter) aggregation;
            ExtendedStats stats = filter.getAggregations().get("stats")
            double SDVal = Double.isNaN(stats.stdDeviation) ? 0.0d : stats.stdDeviation
            SDMapLast.put(filter.getName(), SDVal);
        }

        return prepareRiskQueryBuilderForLastYear(state, client, SDMapLast)
    }


    SearchRequestBuilder prepareRiskQueryBuilderForLastYear(RequestState state, Client client, HashMap<String, Double> stringDoubleHashMap) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        eligibilityAndLevelFilter(state, client, builder)

        double highVal = stringDoubleHashMap.get("allPop")
        double bottomVal = stringDoubleHashMap.get("bottomPop")

        builder.addAggregation(AggregationBuilders.nested("nestedMaraHis").path("MaraHistoricalRiskScores")
                .subAggregation(AggregationBuilders.filter("processedDateFilter")
                .filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores.processedDate").gte(state.periodTo()).lte(state.periodTo()))
                .subAggregation(AggregationBuilders.filter("highRisk").filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores." + PROSPECTIVE_TOTAL).gt(1 + highVal).lte(null))
                .subAggregation(termsAggregatorWithReverseNested())
                .subAggregation(extendedStatsAggregatorForNested()))
                .subAggregation((AggregationBuilders.filter("medRisk").filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores." + PROSPECTIVE_TOTAL).gt(1).lte(highVal))
                .subAggregation(termsAggregatorWithReverseNested())
                .subAggregation(extendedStatsAggregatorForNested())))
                .subAggregation(AggregationBuilders.filter("norRisk").filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores." + PROSPECTIVE_TOTAL).gte(bottomVal).lte(1))
                .subAggregation(termsAggregatorWithReverseNested())
                .subAggregation(extendedStatsAggregatorForNested()))
                .subAggregation(AggregationBuilders.filter("lowRisk").filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores." + PROSPECTIVE_TOTAL).gt(null).lt(bottomVal))
                .subAggregation(termsAggregatorWithReverseNested())
                .subAggregation(extendedStatsAggregatorForNested()))))

        return builder;

    }

    ReverseNestedBuilder termsAggregatorWithReverseNested() {
        return AggregationBuilders.reverseNested("reverse").subAggregation(AggregationBuilders.terms("members").field(MemberSearch.FIELD_UNBLIND_MEMBER_ID).size(0));

    }

    public SearchRequestBuilder getSDForProspectiveTotal(RequestState state, Client client) {

        SearchRequestBuilder searchRequestBuilder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        eligibilityAndLevelFilter(state, client, searchRequestBuilder)

        searchRequestBuilder.addAggregation(AggregationBuilders.filter("allPop").filter(FilterBuilders.matchAllFilter())
                .subAggregation(AggregationBuilders.extendedStats("stats").field("prospectiveTotal")))
                .addAggregation(AggregationBuilders.filter("bottomPop").filter(FilterBuilders.rangeFilter("prospectiveTotal").from(null).to(100))
                .subAggregation(AggregationBuilders.extendedStats("stats").field("prospectiveTotal")))

        SearchResponse response = BuilderAction.getResponse(searchRequestBuilder, "populationRiskSankey");


        Map<String, Double> SDMapCurrent = new HashMap<>()
        for (Aggregation aggregation : response.getAggregations()) {
            Filter filter = (Filter) aggregation;
            ExtendedStats stats = filter.getAggregations().get("stats")
            double SDVal = Double.isNaN(stats.stdDeviation) ? 0.0d : stats.stdDeviation
            SDMapCurrent.put(filter.getName(), SDVal);
        }

        return prepareRiskQueryBuilder(state, client, SDMapCurrent);
    }

    public void eligibilityAndLevelFilter(RequestState state, Client client, SearchRequestBuilder searchRequestBuilder) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterUtils.eligibleDateFilter(state.periodTo()))
        LevelUtils.setLevelFilter(state, andFilterBuilder, state.periodTo())
        CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        searchRequestBuilder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
    }

    public SearchRequestBuilder prepareRiskQueryBuilder(RequestState state, Client client, HashMap<String, Double> SDMapCurrent) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        eligibilityAndLevelFilter(state, client, builder)

        double highVal = SDMapCurrent.get("allPop")
        double bottomVal = SDMapCurrent.get("bottomPop")

        builder.addAggregation(AggregationBuilders.filter("highRisk").filter(FilterBuilders.rangeFilter(PROSPECTIVE_TOTAL).gt(100 + highVal).lte(null))
                .subAggregation(termsAggregator())
                .subAggregation(extendedStatsAggregator()))
                .addAggregation(AggregationBuilders.filter("medRisk").filter(FilterBuilders.rangeFilter(PROSPECTIVE_TOTAL).gt(100).lte(highVal))
                .subAggregation(termsAggregator())
                .subAggregation(extendedStatsAggregator()))
                .addAggregation(AggregationBuilders.filter("norRisk").filter(FilterBuilders.rangeFilter(PROSPECTIVE_TOTAL).gte(bottomVal).lte(100))
                .subAggregation(termsAggregator())
                .subAggregation(extendedStatsAggregator()))
                .addAggregation(AggregationBuilders.filter("lowRisk").filter(FilterBuilders.rangeFilter(PROSPECTIVE_TOTAL).gt(null).lt(bottomVal))
                .subAggregation(termsAggregator())
                .subAggregation(extendedStatsAggregator()))

        return builder;

    }

    private TermsBuilder termsAggregator() {
        return AggregationBuilders.terms("members").field(MemberSearch.FIELD_UNBLIND_MEMBER_ID).size(0)
    }

    private ExtendedStatsBuilder extendedStatsAggregator() {
        return AggregationBuilders.extendedStats("stats").field(PROSPECTIVE_TOTAL)
    }

    private ExtendedStatsBuilder extendedStatsAggregatorForNested() {
        return AggregationBuilders.extendedStats("stats").field("MaraHistoricalRiskScores." + PROSPECTIVE_TOTAL)
    }


}
