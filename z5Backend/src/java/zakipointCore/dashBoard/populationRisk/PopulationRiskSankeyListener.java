package zakipointCore.dashBoard.populationRisk;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.ReverseNested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.*;

/**
 * Created by sktamang on 6/9/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class PopulationRiskSankeyListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        for (Aggregation aggregation : response.getAggregations()) {
            if (!aggregation.getName().equals("nestedMaraHis")) handldeCurrentYearRiskDistribution(aggregation);
            else hanldeLastYearRiskDistribution(aggregation);
        }
    }

    public void hanldeLastYearRiskDistribution(Aggregation aggregation) {

        Record record = getRecord(state, "last");
        Map<String, List<String>> riskCategoryToMemberListMap = getHashMap();
        Nested nested = (Nested) aggregation;
        Filter processedDateFilter = nested.getAggregations().get("processedDateFilter");

        for (Aggregation aggregation1 : processedDateFilter.getAggregations()) {
            Filter filter = (Filter) aggregation1;
            ReverseNested reverse = filter.getAggregations().get("reverse");
            Terms members = reverse.getAggregations().get("members");

            List<String> memberList = new ArrayList<>();

            for (Terms.Bucket bucket : members.getBuckets()) {
                memberList.add(bucket.getKey());
            }

            record.recordMap.put(filter.getName(), memberList);
            riskCategoryToMemberListMap.put("last_" + filter.getName(), memberList);
        }
    }


    void handldeCurrentYearRiskDistribution(Aggregation aggregation) {
        Map<String, List<String>> riskCategoryToMemberListMap = getHashMap();

        Record recordClass = getRecord(state, "current");
        Filter filter = (Filter) aggregation;
        Terms members = filter.getAggregations().get("members");

        List<String> memberList = new ArrayList<>();
        for (Terms.Bucket memberId : members.getBuckets()) {
            memberList.add(memberId.getKey());
        }
        recordClass.recordMap.put(filter.getName(), memberList);
        riskCategoryToMemberListMap.put("current_" + filter.getName(), memberList);
    }

    Map<String, List<String>> getHashMap() {
        if (state.hits.containsKey("riskCategoryToMemberListMap"))
            return (Map<String, List<String>>) state.hits.get("riskCategoryToMemberListMap");
        else {
            Map<String, List<String>> map = new HashMap<>();
            state.hits.put("riskCategoryToMemberListMap", map);
            return (Map<String, List<String>>) state.hits.get("riskCategoryToMemberListMap");
        }
    }

    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        JSONObject current = jsonObject.has("current") == true ? (JSONObject) jsonObject.get("current") : new JSONObject();
        JSONObject last = jsonObject.has("last") == true ? (JSONObject) jsonObject.get("last") : new JSONObject();

        JSONArray linkArray = new JSONArray();
        retainMembers(last, current, "last", "current", linkArray);

        finalize(jsonObject);
        jsonObject.put("links", linkArray);
        return jsonObject;
    }


    private Map<String, Object> retainMembers(JSONObject initial, JSONObject terminal, String initKey, String termKey, JSONArray linkObject) {

        Map<String, Object> retainInfoMap = new HashMap<>();

        for (Object intialKeys : initial.keySet()) {

            String initialKey = intialKeys.toString();
            List memberListInitial = new ArrayList((List) initial.get(initialKey));

            for (Object finalKeys : terminal.keySet()) {
                String finalKey = finalKeys.toString();
                List memberListTerminal = new ArrayList((List) terminal.get(finalKey));

                memberListTerminal.retainAll(memberListInitial);
                JSONObject tempMap = new JSONObject();

                tempMap.put("value", memberListTerminal.size());
                tempMap.put("source", initKey + "_" + initialKey);
                tempMap.put("target", termKey + "_" + finalKey);
                linkObject.put(tempMap);
            }
        }
        return retainInfoMap;
    }


    protected void finalize(JSONObject jsonObject) {

        List<String> metricList = Arrays.asList("lowRisk", "medRisk","norRisk", "highRisk");
        List<String> keyList1 = Arrays.asList("current", "last");
        JSONArray finalTemp = new JSONArray();

        int i = 0;
        for (String key : keyList1) {

            for (String s : metricList) {
                JSONObject tempJson = new JSONObject();

                tempJson.put("year", key);
                tempJson.put("node", key + "_" + s);
                tempJson.put("total", ((List) ((JSONObject) jsonObject.get(key)).get(s)).size());
                tempJson.put("id", i);
                tempJson.put("name", s);
                finalTemp.put(tempJson);
                i++;
            }
            jsonObject.remove(key);
        }
        jsonObject.put("nodes", finalTemp);
        jsonObject.put("levels",metricList);
    }
}

