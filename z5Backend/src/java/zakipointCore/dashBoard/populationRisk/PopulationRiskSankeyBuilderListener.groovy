package zakipointCore.dashBoard.populationRisk

import com.MemberByLevels
import com.zakipoint.service.ReportFromDomainService
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import zakipointCore.builder.ReportFromDomainBuilders

/**
 * Created by sktamang on 6/15/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class PopulationRiskSankeyBuilderListener implements ReportFromDomainBuilders {

    private static List<String> LOA_LIST = Arrays.asList("groupId", "category")
    List<String> PERIOD_LIST = ["p1", "p2"]
    List<String> RISK_LEVEL = ["High", "Medium", "Normal", "Low"];

    private List<String> MEMBER_LIST = new ArrayList<>();

    List<String> getMEMBER_LIST() {
        return MEMBER_LIST
    }

    void setMEMBER_LIST(List<String> MEMBER_LIST) {
        this.MEMBER_LIST = MEMBER_LIST
    }


    @Override
    JSONObject getResults(Map<String, String> requestParams) {
        JSONObject finalJSON = new JSONObject();

        Map<String, List<String>> YEAR_TO_MEMBERLIST_MAP = new HashMap<>()
        def loaMap = populateLoaMap(requestParams)
        JSONArray jsonArray = new JSONArray();
        int i = 0;
        for (String period : PERIOD_LIST) {
            JSONObject memberStorer = new JSONObject()

            for (String level : RISK_LEVEL) {
                JSONObject nodes = new JSONObject();
                Object memberList = getMemberListForPeriodAndLevel(loaMap, period, level, MEMBER_LIST)
                YEAR_TO_MEMBERLIST_MAP.put(period + "_" + level, memberList)
                nodes.put("year", period)
                nodes.put("name", level)
                nodes.put("total", memberList.size())
                nodes.put("id", i)
                nodes.put("node", period + "_" + level)
                jsonArray.put(nodes)

                memberStorer.put(level, memberList)
                i++
            }
            finalJSON.put(period, memberStorer)
        }
        JSONObject jsonObject = new JSONObject()
        return jsonObject.put("reporting", finalResponseGenerator(finalJSON, jsonArray))
    }

    protected JSONObject finalResponseGenerator(JSONObject jsonObject, JSONArray jsonArray) {

        JSONObject current = jsonObject.has("p1") == true ? (JSONObject) jsonObject.get("p1") : new JSONObject();
        JSONObject last = jsonObject.has("p2") == true ? (JSONObject) jsonObject.get("p2") : new JSONObject();

        JSONArray linkArray = new JSONArray();
        retainMembers(last, current, "p2", "p1", linkArray);

        jsonObject.put("links", linkArray);

        PERIOD_LIST.each { jsonObject.remove(it) }

        jsonObject.put("nodes", jsonArray)
        jsonObject.put("levels",RISK_LEVEL)
        return jsonObject;
    }


    private Map<String, Object> retainMembers(JSONObject initial, JSONObject terminal, String initKey, String termKey, JSONArray linkObject) {

        Map<String, Object> retainInfoMap = new HashMap<>();
        for (Object intialKeys : initial.keySet()) {

            String initialKey = intialKeys.toString();
            List memberListInitial = new ArrayList((List) initial.get(initialKey));

            for (Object finalKeys : terminal.keySet()) {
                String finalKey = finalKeys.toString();
                List memberListTerminal = new ArrayList((List) terminal.get(finalKey));

                memberListTerminal.retainAll(memberListInitial);
                JSONObject tempMap = new JSONObject();

                tempMap.put("source", initKey + "_" + initialKey);
                tempMap.put("target", termKey + "_" + finalKey);
                tempMap.put("value", memberListTerminal.size());
                linkObject.put(tempMap);
            }
        }
        return retainInfoMap;
    }

    Object getMemberListForPeriodAndLevel(loaMap, String period, String level, List<String> memberListGet) {
        def riskWiseMember = MemberByLevels.createCriteria()
        def memberList = riskWiseMember.list {
            loaMap.each { k, v ->
                eq k, v
            }
            and {
                eq(period, level)
                if (memberListGet.size() > 0) 'in'("mbrId", memberListGet)
            }
            projections {
                property('mbrId')
            }
        }
        memberList
    }


    Map<String, String> populateLoaMap(Map<String, String> requestMap) {
        def loaMap = [:]
        for (String key : requestMap.keySet()) {
            if (LOA_LIST.contains(key)) {
                Object value = requestMap.get(key);
                String key1 = key
                if (key.equalsIgnoreCase("group")) {
                    key1 = key + "Id"
                    value = RequestUtils.getArrayRequestValue(requestMap.get(key))
                }
                loaMap.put(key1, value)
            }
        }
        return loaMap
    }

    @Override
    Object getDrillResults(Map<String, String> requestParams) {
        String drillType = requestParams.get("drillType")
        ReportFromDomainService.loaParamsManagement(requestParams)
        def loaMap = populateLoaMap(requestParams)
        if (drillType.equalsIgnoreCase("sediment")) {
            def period = requestParams.get("period")
            def level = requestParams.get("level")
            Object memberList = getMemberListForPeriodAndLevel(loaMap, period, level, MEMBER_LIST)
            return memberList

        } else if (drillType.equalsIgnoreCase("flow")) {
            String[] source = requestParams.get("source").split("_")
            String[] target = requestParams.get("target").split("_")

            List<String> memberListSource = getMemberListForPeriodAndLevel(loaMap, source[0], source[1], MEMBER_LIST)
            List<String> memberListTarget = getMemberListForPeriodAndLevel(loaMap, target[0], target[1], MEMBER_LIST)

            memberListTarget.retainAll(memberListSource)
            return memberListTarget;
        }
        return null
    }
}
