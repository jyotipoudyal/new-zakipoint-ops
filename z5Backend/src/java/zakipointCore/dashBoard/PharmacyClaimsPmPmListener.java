package zakipointCore.dashBoard;

import com.configurations.utils.AmountUtils;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;

/**
 * Created by sktamang on 9/24/15.
 */
public class PharmacyClaimsPmPmListener extends AbstractClaimPmpmListener {

    @Override
    protected void hanleMetrics(Aggregation aggregation) {
        Record wrapper=getRecord(state,"pharmacyClaims");
        for (Filters.Bucket dateRanges : ((Filters) aggregation).getBuckets()) {
                Sum paidAmount=dateRanges.getAggregations().get("paidAmount");
            wrapper.recordMap.put(dateRanges.getKey(), AmountUtils.getAmount(paidAmount.getValue()));
        }
    }
}
