package zakipointCore.dashBoard;

import com.configurations.utils.AmountUtils;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;

/**
 * Created by sktamang on 9/24/15.
 */
public class MedicalClaimsPmPmListener extends AbstractClaimPmpmListener {

    @Override
    protected void hanleMetrics(Aggregation aggregation) {

        Record wrapper=getRecord(state,"medicalClaims");

        for (Filters.Bucket dateRanges : ((Filters) aggregation).getBuckets()) {
            Terms placeOfServices=dateRanges.getAggregations().get("placeOfService");

            double paidAmountValue=0.0d;

            for (Terms.Bucket placeOfServiceBuckets : placeOfServices.getBuckets()) {
                Sum paidAmount=placeOfServiceBuckets.getAggregations().get("paidAmounts");
                paidAmountValue+= AmountUtils.getAmount(paidAmount.getValue());
            }
            wrapper.recordMap.put(dateRanges.getKey(),paidAmountValue);
        }
    }
}
