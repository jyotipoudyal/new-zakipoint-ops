package zakipointCore.dashBoard;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;

/**
 * Created by sktamang on 9/24/15.
 */
public class MedicalClaimsPmPmBuilder extends AbstractClaimPmpmBuilder{
    @Override
    public SearchRequestBuilder getMedicalrPharmacyClaimsRequestBuilder(SearchRequestBuilder builder,FiltersAggregationBuilder filters) {
        builder.setTypes("Medical");
        builder.addAggregation(filters.subAggregation(AggregationBuilders.terms("placeOfService").field("placeOfService").size(0)
                .subAggregation(AggregationBuilders.sum("paidAmounts").field("paidAmount"))));
        return builder;
    }
}
