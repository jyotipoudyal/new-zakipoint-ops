package zakipointCore.dashBoard;

import com.configurations.utils.Pharmacy;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;

/**
 * Created by sktamang on 9/24/15.
 */
public class PharmacyClaimsPmPmBuilder extends AbstractClaimPmpmBuilder{

    @Override
    public SearchRequestBuilder getMedicalrPharmacyClaimsRequestBuilder(SearchRequestBuilder builder, FiltersAggregationBuilder filters) {
        builder.setTypes(Pharmacy.NAME);
        builder.addAggregation(filters.subAggregation(AggregationBuilders.sum("paidAmount").field("paidAmount")));
        return  builder;
    }
}
