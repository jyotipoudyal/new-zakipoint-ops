package zakipointCore.features.featureInfo;

import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 5/5/16.
 */
public class FeatureInfoBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        builder.add(query(state,client));
        return builder;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        SearchRequestBuilder builder=QueryUtils.buildEndSearchWihType(client,state.request, MemberSearch.NAME);
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder= CommonQueries.caseMemberFilter(state,client,andFilterBuilder);
        LevelUtils.setLevelFilterOnlyId(state, andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        implementAggregations(builder, state);
        return builder;
    }

    private void implementAggregations(SearchRequestBuilder builder, RequestState state) {
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(FilterUtils.eligibleDateFilter(state.periodTo()));
        LevelUtils.setLevelFilter(state, andFilterBuilder, state.periodTo());
        builder.addAggregation(AggregationBuilders.filter("eligibleFilter").filter(andFilterBuilder));
    }

}
