package zakipointCore.features.featureInfo;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Map;

/**
 * Created by sktamang on 5/5/16.
 */
public class FeatureInfoListener extends AbstractSummaryListener {
    @Override
    protected void handleResponse(SearchResponse response) {
        Record record=getRecord(state,"featureInfo");
        for (Map.Entry<String,String> requestParameter : state.request.requestParameters.entrySet()) {
            record.recordMap.put(requestParameter.getKey(),requestParameter.getValue());
        }

        Filter eligibleMemberCount=response.getAggregations().get("eligibleFilter");
        record.recordMap.put("eligibleMemberCount",eligibleMemberCount.getDocCount());
        record.recordMap.put("overAllMemberCount",response.getHits().totalHits());
    }
}
