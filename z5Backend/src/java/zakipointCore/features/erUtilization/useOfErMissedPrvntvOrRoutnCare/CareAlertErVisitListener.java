package zakipointCore.features.erUtilization.useOfErMissedPrvntvOrRoutnCare;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.children.Children;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 3/16/16.
 */
public class CareAlertErVisitListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {
        for (Aggregation aggregation : response.getAggregations()) {
            Filters filters=(Filters)aggregation;
            double memberMonths= (double) state.hits.get("memberMonths");
            for (Filters.Bucket bucket : filters.getBuckets()) {
                Record record=getRecord(state,bucket.getKey());
                record.recordMap.put("uniqueMembers",bucket.getDocCount());

                Filter visitAdmissionAsChildFilter=bucket.getAggregations().get("visitAdmissionAsChild");
                Children visitAdmissionAsChild=visitAdmissionAsChildFilter.getAggregations().get("visitAdmissionChildren");
                Sum erVisitSum=visitAdmissionAsChild.getAggregations().get("erVisitSum");

                record.recordMap.put("erVisit",erVisitSum.getValue());
                record.recordMap.put("erVisitper1000", CalculatorUtils.perThousand(erVisitSum.getValue(),memberMonths));
            }
        }
    }
}
