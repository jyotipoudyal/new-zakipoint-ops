package zakipointCore.features.erUtilization.useOfErMissedPrvntvOrRoutnCare;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.features.erUtilization.avoidableToTotalErRatio.AvoidableToTotalErRatioBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 3/16/16.
 */
public class CareAlertErVisitBuilder implements SummaryQueryBuilder {

    public Map<String, Map<String, String>> QM_CONDITION_TO_FULL_INFO_MAP = new HashMap<String, Map<String, String>>();

    public static Map<String, String> NO_PREVENTIVE_VISIT = new HashMap<String, String>() {{
        put("229", "Annual well-child exam (age 2 to 6 yrs)__1__Wellness__wellness.10");
        put("230", "Annual well-child exam (age 7 to 12 yrs)__1__Wellness__wellness.11");
        put("231", "Annual well-child exam (age 13 to 21 yrs)__1__Wellness__wellness.12");
        put("236", "Well Child Visit - 15 months__1__Wellness__wellness.17 ");
        put("237", "Infant - 1 or more Well Child Visit__1__Wellness__wellness.18");
        put("243", "Members aged 19 years to 39 with preventive visit in last 24 months__1__Wellness__wellness.24");
        put("244", "Members aged 40 years to 64 years with preventive visit in last 24 months__1__Wellness__wellness.25");
        put("248", "Members aged 65 years and older with an annual preventive visit__1__Wellness__wellness.29");
    }};

    public static Map<String, String> NO_ROUTINE_VISIT = new HashMap<String, String>() {{
        put("240", "Routine office visit in last 6 months__1__Wellness__wellness.21");
        put("4", "No PCP visit last 12 months__0__Additional Gaps__additionalGaps.4");
    }};


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {

        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        init();

        builder.add(query(state, client));
        double memberMonth = 0.0d;
        state.hits.put("memberMonths", CommonQueries.getTotalMemberMonth(state, client, memberMonth));
        return builder;
    }

    private void init() {
        QM_CONDITION_TO_FULL_INFO_MAP.put("noPreventiveVisit", NO_PREVENTIVE_VISIT);
        QM_CONDITION_TO_FULL_INFO_MAP.put("noRoutineVisit", NO_ROUTINE_VISIT);
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        AndFilterBuilder andFilter = new AndFilterBuilder();
        CommonQueries.caseMemberFilter(state, client, andFilter);
        LevelUtils.setLevelFilter(state, andFilter, state.periodTo());
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilter));

        FiltersAggregationBuilder qmFilters = AggregationBuilders.filters("qmFilters");

        for (Map.Entry<String, Map<String, String>> pair : QM_CONDITION_TO_FULL_INFO_MAP.entrySet()) {
            AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
            Map<String, String> metricFullInfoMap = pair.getValue();
            List<String> postiveMetricList = new ArrayList<>();
            List<String> negativeMetricList = new ArrayList<>();

            for (Map.Entry<String, String> qmInfo : metricFullInfoMap.entrySet()) {
                String postiveOrNegative = qmInfo.getValue().split("__")[1];
                if (postiveOrNegative.equals("1")) postiveMetricList.add(qmInfo.getKey());
                else negativeMetricList.add(qmInfo.getKey());
            }

            AndFilterBuilder positiveMetricFilter = implementNumerator(state, "0", postiveMetricList.toArray());
            AndFilterBuilder negativeMetricFilter = implementNumerator(state, "1", negativeMetricList.toArray());

            OrFilterBuilder orFilterBuilder = new OrFilterBuilder();
            orFilterBuilder.add(positiveMetricFilter).add(negativeMetricFilter);
            andFilterBuilder.add(orFilterBuilder);
            qmFilters.filter(pair.getKey(), andFilterBuilder);
        }

        builder.addAggregation(qmFilters);
        applyVisitAdmissionAggregation(state, qmFilters);
        return builder;
    }


    private void applyVisitAdmissionAggregation(RequestState state, FiltersAggregationBuilder qmFilters) {

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        FilterUtils.serviceDateFilter(state, andFilterBuilder);
        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        FilterUtils.getUmVisitFilter(andFilterBuilder, VisitAdmission.ER_VISIT);

        if (state.request.hasParam("exclude")) andFilterBuilder.add(excludeAvoidableErVisitor());
        HasChildFilterBuilder hasChildFilterBuilder = FilterBuilders.hasChildFilter(VisitAdmission.NAME, andFilterBuilder);
        qmFilters.subAggregation(AggregationBuilders.filter("visitAdmissionAsChild").filter(hasChildFilterBuilder)
                .subAggregation(AggregationBuilders.children("visitAdmissionChildren").childType(VisitAdmission.NAME)
                        .subAggregation(AggregationBuilders.sum("erVisitSum").field(VisitAdmission.NAME + "." + VisitAdmission.ER_VISIT))));
    }


    private AndFilterBuilder excludeAvoidableErVisitor() {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.termFilter(VisitAdmission.AVOIDABLE_FLAG, 1))
                .add(FilterBuilders.termsFilter(VisitAdmission.REASON_ERVISIT, AvoidableToTotalErRatioBuilder.AVOIDABLE_ER_VISITS_IDS));
        return andFilterBuilder;
    }


    public AndFilterBuilder implementNumerator(RequestState state, String numOrDen, Object[] metricCode) {
        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermsFilter("qm.measure", metricCode));
        qmFilter.add(FilterBuilders.termFilter("qm.numerator", numOrDen));
        qmFilter.add(FilterUtils.getRangeFilter(state));
        return FilterBuilders.andFilter(FilterBuilders.nestedFilter("qm", qmFilter));
    }
}
