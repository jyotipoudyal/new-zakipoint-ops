package zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions;

import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.*;

import java.util.Map;
import java.util.Set;

/**
 * Created by sktamang on 3/17/16.
 */
public class ErVisitsLikelyToIncreaseBuilder extends ErUtilzationMonthlyTrendBuilder {



    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
//        builder.add(query(state, client));
        builder.add(visitAdmissionQuery(state,client,""));
        calculatePmpm(state,client);
        return  builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }

    private void calculatePmpm(RequestState state, Client client) {
        SearchRequestBuilder builder= QueryUtils.buildEndSearchWihType(client,state.request, MemberSearch.NAME);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        LevelUtils.setLevelFilter(state,andFilterBuilder,state.periodTo());
        andFilterBuilder.add(FilterUtils.eligibleDateFilter(state.periodTo()));

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        CommonQueries.paidMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        CommonQueries.serviceMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);

        builder.addAggregation(AggregationBuilders.nested("memberAmountNested").path(MemberSearch.MEMBER_AMOUNT.NAME)
                .subAggregation(AggregationBuilders.filter("memberAmountFilter").filter(memberAmountFilter)
                        .subAggregation(AggregationBuilders.sum("totalMemberPaidAmount").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT))));

        SearchResponse response= (SearchResponse) BuilderAction.getResponse(builder,"erVisitLikelyToIncrease");
        Nested memberAmountNested = response.getAggregations().get("memberAmountNested");
        Filter memberAmount = memberAmountNested.getAggregations().get("memberAmountFilter");
        Sum totalMemberPaidAmount = memberAmount.getAggregations().get("totalMemberPaidAmount");

        double memberMonths = 0;
        memberMonths=CommonQueries.getTotalMemberMonth(state,client,memberMonths);

        state.hits.put("groupAverage", CalculatorUtils.divide(totalMemberPaidAmount.getValue(),memberMonths)*12);

    }


    protected void implementAgg(SearchRequestBuilder builder,RequestState state) {

    }


    @Override
    protected void applyPreviousYearFilter(RequestState state, SearchRequestBuilder builder) {
        super.applyPreviousYearFilter(state,builder);
    }


    @Override
    protected void applyAggregations(RequestState state, SearchRequestBuilder builder,String trend) {
        super.applyAggregations(state, builder,null);
    }

    @Override
    protected Set<Map.Entry<String, String>> getDateMap(RequestState state,String trend) {
        return super.getDateMap(state,"");
    }

}
