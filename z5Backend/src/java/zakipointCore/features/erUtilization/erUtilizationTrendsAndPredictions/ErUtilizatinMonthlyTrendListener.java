package zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions;

import com.configurations.utils.AmountUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.global.InternalGlobal;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Map;

/**
 * Created by sktamang on 3/14/16.
 */
public class ErUtilizatinMonthlyTrendListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {
        String trend=state.request.hasParam("trend") ? state.request.get("trend"):"monthly";
        Map<String, Double> memberMonthMap = null;
        double memberMonth = 0;
        if(trend.equalsIgnoreCase("monthly")) {memberMonthMap = (Map<String, Double>) state.hits.get("memberMonthMap");}
        else {memberMonth= (double) state.hits.get("memberMonth");}

        for (Aggregation aggregation : response.getAggregations()) {
            if (!aggregation.getName().equalsIgnoreCase("overAll")) {
                Filters months = (Filters) aggregation;
                for (Filters.Bucket bucket : months.getBuckets()) {
                    Record record = getRecord(state, bucket.getKey());
                    Sum sum = bucket.getAggregations().get("totalErVisits");
                    record.recordMap.put(sum.getName(), sum.getValue());
                    erPer1000Calculation(memberMonthMap, bucket, record, sum,memberMonth,trend);

                    Nested visitAmounts=bucket.getAggregations().get("nestedVisitAmounts");
                    extractVisitAmount(record, visitAmounts);

                    Terms members=bucket.getAggregations().get("intMemberId");
                }
            }else
            {
                Record record=getRecord(state,"overAllTotalAmount");
                InternalGlobal globalFOrPreviousYear=(InternalGlobal) aggregation;
                Filter previousYearFilter=globalFOrPreviousYear.getAggregations().get("overAllAmount");
                Nested visitAmounts=previousYearFilter.getAggregations().get("nestedVisitAmounts");
                extractVisitAmount(record,visitAmounts);
            }
        }
    }

    private void extractVisitAmount(Record record, Nested visitAmounts) {
        Filter visitAmountFilter=visitAmounts.getAggregations().get("visitAmountFilter");
        Sum totalPaidAmount=visitAmountFilter.getAggregations().get("totalPaid");
        record.recordMap.put(totalPaidAmount.getName(), AmountUtils.getAmount(totalPaidAmount.getValue()));
    }

    private void erPer1000Calculation(Map<String, Double> memberMonthMap, Filters.Bucket bucket, Record record, Sum sum,double memberMonth,String trend) {
        double per1000=0;
        if(trend.equalsIgnoreCase("monthly")){
            memberMonth=memberMonthMap.get(bucket.getKey());
            per1000= CalculatorUtils.perThousandMembers(sum.getValue(), memberMonth);
        }
        else per1000=CalculatorUtils.perThousand(sum.getValue(), memberMonth);
        record.recordMap.put("memberCount",memberMonth );
        record.recordMap.put("erVisitPer1000PerMonth",per1000);
    }
}
