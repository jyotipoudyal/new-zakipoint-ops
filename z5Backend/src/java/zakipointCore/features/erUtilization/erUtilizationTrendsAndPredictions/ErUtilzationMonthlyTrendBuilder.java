package zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions;

import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.*;

import java.util.*;

/**
 * Created by sktamang on 3/14/16.
 */
public class ErUtilzationMonthlyTrendBuilder implements SummaryQueryBuilder {


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        String trend = state.request.hasParam("trend") ? state.request.get("trend") : "monthly";
        builder.add(visitAdmissionQuery(state, client, trend));
        //calculate memberMonths
        trendWiseMemberMonthsCalculation(state, client, trend);
        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }


    protected SearchRequestBuilder visitAdmissionQuery(RequestState state, Client client, String trend) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, VisitAdmission.NAME);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder = FilterUtils.getUmVisitFilter(andFilterBuilder, VisitAdmission.ER_VISIT);

        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        LevelUtils.setLOAFilter(state, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        applyAggregations(state, builder, trend);
        return builder;
    }


    protected void applyAggregations(RequestState state, SearchRequestBuilder builder, String trend) {

        FiltersAggregationBuilder monthWiseFilters = AggregationBuilders.filters("monthWiseFilters");

        for (Map.Entry<String, String> periods : getDateMap(state, trend)) {
            AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
            FilterUtils.serviceDateFilter(state, andFilterBuilder);
            andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(VisitAdmission.VisitAmounts.NAME, periods.getKey(), periods.getValue()));
            monthWiseFilters.filter(DateUtils.getMonthOnlyForDate(periods.getKey()), andFilterBuilder);
        }

        monthWiseFilters.subAggregation(AggregationBuilders.sum("totalErVisits").field(VisitAdmission.ER_VISIT));
        monthWiseFilters.subAggregation(AggregationBuilders.terms("intMemberId").field(VisitAdmission.INT_MEMBER_ID).size(0));
        builder.addAggregation(monthWiseFilters.subAggregation(CommonQueries.nestedVisitAmountSum(state)));
        applyPreviousYearFilter(state, builder);
    }


    protected Set<Map.Entry<String, String>> getDateMap(final RequestState state, String trend) {

        final String periodFrom = state.periodFrom();
        final String periodTo = state.periodTo();

        if (trend.equalsIgnoreCase("monthly"))
            return DateUtils.getAllMonthsString(state.periodFrom(), state.periodTo()).entrySet();

        else {
            return new HashMap<String, String>() {{
                put(periodFrom, periodTo);
            }}.entrySet();
        }
    }


    protected void applyPreviousYearFilter(RequestState state, SearchRequestBuilder builder) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter("visitAmounts", state.periodFrom(), state.periodTo()));
        FilterUtils.serviceDateFilter(state, andFilterBuilder, state.periodFrom(), state.periodTo());
        builder.addAggregation(AggregationBuilders.global("overAll")
                .subAggregation(AggregationBuilders.filter("overAllAmount").filter(andFilterBuilder).subAggregation(
                        CommonQueries.nestedVisitAmountSum(state))));
    }


    private void trendWiseMemberMonthsCalculation(RequestState state, Client client, String trend) {

        if (trend.equalsIgnoreCase("monthly")) {
            Map<String, Double> memberMonthMap = new HashMap<>();
            state.hits.put("memberMonthMap", CommonQueries.getMemberMonthMap(state, client, memberMonthMap));
        }

        else {
            double memberMonth = 0.0d;
            memberMonth = CommonQueries.getTotalMemberMonth(state, client, memberMonth);
            state.hits.put("memberMonth", memberMonth);
        }
    }
}
