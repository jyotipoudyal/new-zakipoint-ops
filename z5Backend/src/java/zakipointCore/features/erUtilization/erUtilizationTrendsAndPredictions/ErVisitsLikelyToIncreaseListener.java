package zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions;

import com.configurations.utils.AmountUtils;
import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearch;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.global.InternalGlobal;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sktamang on 3/17/16.
 */
public class ErVisitsLikelyToIncreaseListener extends AbstractSummaryListener {


    @Override
    protected void handleResponse(SearchResponse response) {


        double memberMonths = 0;
        memberMonths = CommonQueries.getTotalMemberMonth(state, client, memberMonths);
        state.hits.put("memberMonths",memberMonths);

        for (Aggregation aggregation : response.getAggregations()) {
            if (aggregation.getName().contains("monthWiseFilters")) {
                Filters filters = (Filters) aggregation;
                extractMonthWiseRecords(memberMonths, filters);
            }
            else {
                Record record = getRecord(state, "overAllTotalAmount");
                InternalGlobal globalFOrPreviousYear = (InternalGlobal) aggregation;
                Filter previousYearFilter = globalFOrPreviousYear.getAggregations().get("overAllAmount");
                Nested visitAmounts = previousYearFilter.getAggregations().get("nestedVisitAmounts");
                extractVisitAmount(record, visitAmounts);
            }
        }
        extractRiskScore();
    }


    private void extractMonthWiseRecords(double memberMonths, Filters filters) {

        for (Filters.Bucket bucket : filters.getBuckets()) {
            double visitAmount;

            Sum erVisit = bucket.getAggregations().get("totalErVisits");
            Record record = getRecord(state, erVisit.getName());
            Nested visitAmounts = bucket.getAggregations().get("nestedVisitAmounts");
            Terms members=bucket.getAggregations().get("intMemberId");
            List<String> erMemberList=new ArrayList<>();

            for (Terms.Bucket bucket1 : members.getBuckets()) {
                erMemberList.add(bucket1.getKey());
            }

            state.hits.put("memberList",erMemberList);
            visitAmount = extractVisitAmount(record, visitAmounts);
            visitRecord(memberMonths, visitAmount, erVisit, record);
            record.recordMap.put("totalErMembers",members.getBuckets().size());
        }
    }


    private double extractVisitAmount(Record record, Nested visitAmounts) {
        Filter visitAmountFilter = visitAmounts.getAggregations().get("visitAmountFilter");
        Sum totalPaidAmount = visitAmountFilter.getAggregations().get("totalPaid");
        record.recordMap.put(totalPaidAmount.getName(), AmountUtils.getAmount(totalPaidAmount.getValue()));
        return totalPaidAmount.getValue();
    }


    private void visitRecord(double memberMonths, double visitAmount, Sum erVisit, Record record) {
        double erVisitPer1000;

        record.recordMap.put("erVisitCount", erVisit.getValue());
        erVisitPer1000 = CalculatorUtils.perThousand(erVisit.getValue(), memberMonths);
        record.recordMap.put("erVisitPer1000", erVisitPer1000);
        record.recordMap.put("memberCount", memberMonths);
        record.recordMap.put("avgCostPerErVisit", CalculatorUtils.divide(AmountUtils.getAmount(visitAmount), erVisit.getValue()));
    }


    public void extractRiskScore() {
        List<String> memberList= (List<String>) state.hits.get("memberList");
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.termsFilter(MemberSearch.FIELD_INT_MEMBER_ID,memberList.toArray()));

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        builder.addAggregation(AggregationBuilders.sum("riskScoreEr").field("erScoreNormalizedToGroup"))
                .addAggregation(AggregationBuilders.sum("riskScoreErConcurrent").field("concurrentErScoreNormalizedToGroup"));
//        String normlizedField = state.request.hasParam("group") ? "prospectiveTotalNormalizedToGroup" : "concurrentErScoreNormalizedToGroup";
//        builder.addAggregation(AggregationBuilders.sum("riskScoreAll").field(normlizedField));

        SearchResponse response= (SearchResponse) BuilderAction.executeBuilder(builder,"erVisitLikelyToIncrease");
        for (Aggregation aggregation : response.getAggregations()) {
            extractErScore(aggregation);
        }
    }


    private void extractErScore(Aggregation aggregation) {
        double erRisk;

        Sum avgErScore = (Sum) aggregation;
        erRisk = Double.isNaN(avgErScore.getValue()) ? 0 : avgErScore.getValue();
        Record record = getRecord(state, "riskScoreEr");

        if (!avgErScore.getName().contains("Er")) erRisk = erRisk ;
        record.recordMap.put(aggregation.getName(), erRisk);
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject response) {


        double riskScoreErProspective = (double) response.getJSONObject("riskScoreEr").get("riskScoreEr");
        double riskScoreErConcurrent = (double) response.getJSONObject("riskScoreEr").get("riskScoreErConcurrent");
        double groupAvgCost=(Double) state.hits.get("groupAverage");

        double erVisitCount = (double) response.getJSONObject("totalErVisits").get("erVisitCount");

        response.put("groupAverageCost",groupAvgCost);

        double predictedCost=riskScoreErProspective*groupAvgCost;
        double expectedCost=riskScoreErConcurrent*groupAvgCost;
        double avgErCost = CalculatorUtils.divide(expectedCost,erVisitCount);

        response.put("totalErProjectedInNext12Months", predictedCost);
        response.put("totalErExpectedInNext12Months", expectedCost);

        double percentLikelyToIncreaseOrDecrease = CalculatorUtils.calculatePercentage(predictedCost - expectedCost, expectedCost);
        response.put("percentLikelyToIncreaseOrDecrease", percentLikelyToIncreaseOrDecrease);

        double predictedErVisit=CalculatorUtils.divide(predictedCost, avgErCost);

        double predictedERVisitsPer1000InNext12Months = CalculatorUtils.perThousand(predictedErVisit, (Double) state.hits.get("memberMonths"));
        response.put("predictedERVisitsPer1000InNext12Months", predictedERVisitsPer1000InNext12Months);
        return  response;
    }
}

