package zakipointCore.features.erUtilization.avoidableErVsUrgentCareVisit;

import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.features.erUtilization.avoidableToTotalErRatio.AvoidableToTotalErRatioBuilder;

/**
 * Created by sktamang on 3/30/16.ableToTotalErRatio&eli
 */
public class AvoidableErVsUrgentVisitBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {

        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        double memberMonths=0;
        state.hits.put("memberMonths", CommonQueries.getTotalMemberMonth(state,client,memberMonths));

        builder.add(visitAdmissionBuilder(state, client));
        return builder;
    }


    private SearchRequestBuilder visitAdmissionBuilder(RequestState state, Client client) {

        SearchRequestBuilder builder=QueryUtils.buildEndSearchWihType(client,state.request, VisitAdmission.NAME);
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();

        andFilterBuilder=CommonQueries.caseMemberFilter(state,client,andFilterBuilder);
        LevelUtils.setLOAFilter(state,andFilterBuilder);

        FilterUtils.serviceDateFilter(state,andFilterBuilder);
        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        andFilterBuilder.add(FilterBuilders.notFilter(FilterBuilders.termFilter(VisitAdmission.TOTAL_AMOUNT, 0)));
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        applyAggregationsForTheVisits(builder);
        return builder;

    }

    private void applyAggregationsForTheVisits( SearchRequestBuilder builder) {

        builder.addAggregation(AggregationBuilders.filter("urgentCareVisitsFilter").filter(FilterBuilders.notFilter(FilterBuilders.termFilter("urgentCareVisits", 0)))
                .subAggregation(AggregationBuilders.sum("urgentCareVisits").field("urgentCareVisits")));

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.notFilter(FilterBuilders.termFilter(VisitAdmission.ER_VISIT,0)));
        andFilterBuilder.add(FilterBuilders.termFilter(VisitAdmission.AVOIDABLE_FLAG, 1));
        andFilterBuilder.add(FilterBuilders.termsFilter(VisitAdmission.REASON_ERVISIT, AvoidableToTotalErRatioBuilder.AVOIDABLE_ER_VISITS_IDS));

        builder.addAggregation(AggregationBuilders.filter("avoidableErVisit").filter(andFilterBuilder)
                .subAggregation(AggregationBuilders.sum("totalVisit").field(VisitAdmission.ER_VISIT)));
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
