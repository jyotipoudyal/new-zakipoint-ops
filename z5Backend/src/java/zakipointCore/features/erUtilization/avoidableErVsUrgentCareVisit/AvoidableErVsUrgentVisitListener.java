package zakipointCore.features.erUtilization.avoidableErVsUrgentCareVisit;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 3/30/16.
 */
public class AvoidableErVsUrgentVisitListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        Record otherRecord=getRecord(state,"metaData");
        otherRecord.recordMap.put("memberMonth",state.hits.get("memberMonths"));

        for (Aggregation aggregation : response.getAggregations()) {
            Filter filter=(Filter)aggregation;
            Record record=getRecord(state,filter.getName());

            for (Aggregation aggregation1 : filter.getAggregations()) {
                Sum visitSum=(Sum)aggregation1;
                record.recordMap.put("visitCount",visitSum.getValue());
                record.recordMap.put("visitCountPer1000", CalculatorUtils.perThousand(visitSum.getValue(),(double)state.hits.get("memberMonths")));
            }
        }
    }
}
