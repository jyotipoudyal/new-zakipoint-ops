package zakipointCore.features.erUtilization.topConditionByCost;

import com.configurations.utils.VisitAdmission;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;

/**
 * Created by sktamang on 12/24/15.
 */
public class TopConditionByCostAvoidErBuilder extends TopConditionByCostAllErBuilder {

    @Override
    protected TermsBuilder getDescription() {
        return AggregationBuilders.terms("description");
    }

    @Override
    protected AndFilterBuilder addAvoidableVisitFilter(AndFilterBuilder filter) {
        return filter.add(FilterBuilders.termFilter(VisitAdmission.AVOIDABLE_FLAG,1));
    }

    @Override
    protected String getTermsField() {
        return VisitAdmission.REASON_ERVISIT;
    }
}
