package zakipointCore.features.erUtilization.topConditionByCost;

import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 12/24/15.
 */
public class TopConditionByCostAllErBuilder implements SummaryQueryBuilder {
    public static final String CATEGORY_TERMS = "CategoryTerms";
    public static final String TOTAL_AMOUNT = "TotalAmount";

    public static final String DESCRIPTION = "description";
    public static final String TERM_FIELD = VisitAdmission.SUPER_DIAG_GROUPER;

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        double memberMonths= 0.0d;
        state.hits.put("memberMonths", CommonQueries.getTotalMemberMonth(state, client, memberMonths));
        return builder.add(query(state, client));
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, VisitAdmission.NAME);
        AndFilterBuilder andFilter = new AndFilterBuilder();

        FilterUtils.serviceDateFilter(state, andFilter, state.periodFrom(), state.periodTo());
        FilterUtils.getUmVisitFilter(andFilter, VisitAdmission.ER_VISIT);
        andFilter.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        addAvoidableVisitFilter(andFilter);
        andFilter=CommonQueries.caseMemberFilter(state,client,andFilter);

        LevelUtils.setLOAFilter(state, andFilter);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilter));

        builder.addAggregation(AggregationBuilders.terms(CATEGORY_TERMS).field(getTermsField()).size(0)
                .order(Terms.Order.aggregation("nestedVisitAmounts>visitAmountFilter>totalPaid", false))
                .subAggregation(CommonQueries.nestedVisitAmountSum(state))
                .subAggregation(getDescription())
                .subAggregation(AggregationBuilders.sum("erVisitCount").field(VisitAdmission.ER_VISIT)))
                .addAggregation(CommonQueries.nestedVisitAmountSum(state))
                .addAggregation(AggregationBuilders.sum("erVisitCount").field(VisitAdmission.ER_VISIT));
        return builder;
    }

    protected TermsBuilder getDescription() {
        return AggregationBuilders.terms(DESCRIPTION).field(VisitAdmission.SUPER_DIAG_GROUPER_DESC).size(1);
    }

    protected AndFilterBuilder addAvoidableVisitFilter(AndFilterBuilder filter) {
        return filter;
    }

    protected String getTermsField() {
        return TERM_FIELD;
    }
}
