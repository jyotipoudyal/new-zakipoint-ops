package zakipointCore.features.erUtilization.topConditionByCost;

import com.configurations.XContentWriter;
import org.elasticsearch.common.xcontent.XContentBuilder;
import zakipointCore.commonUtils.RequestState;

import java.io.IOException;

/**
 * Created by sktamang on 12/24/15.
 */
public class TopConditionByCostAvoidErListener extends TopConditionByCostAllErListener {

    @Override
    protected Wrapper getRecord(RequestState state, String id) {
        if (!state.recordIds.containsKey(id)) {
            Wrapper wrapper = new Wrapper(id);
            state.recordIds.put(id, wrapper);
        }
        return (Wrapper) state.recordIds.get(id);
    }

    public class Wrapper extends TopConditionByCostAllErListener.Wrapper implements XContentWriter {

        public Wrapper(String id) {
            super(id);
        }

        @Override
        protected void addFurtherFields(XContentBuilder builder) throws IOException {
            String condition = DIAGNOSIS_LIST.keySet().contains(id) ? DIAGNOSIS_LIST.get(id) : "other";
            builder.field("condition", condition);
            builder.endObject();
        }
    }
}
