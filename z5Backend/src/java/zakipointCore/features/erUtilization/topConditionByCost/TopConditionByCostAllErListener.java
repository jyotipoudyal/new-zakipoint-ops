package zakipointCore.features.erUtilization.topConditionByCost;

import com.configurations.XContentWriter;
import com.configurations.utils.AmountUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 12/24/15.
 */
public class TopConditionByCostAllErListener extends AbstractSummaryListener {

    public static final Map<String, String> DIAGNOSIS_LIST = new HashMap<String, String>() {{
        put("1", "Acute Bronchitis");
        put("2", "Acute Upper Respiratory Infection");
        put("3", "Acute Throat Infection");
        put("4", "Back Pain");
        put("5", "Headache");
        put("6", "Otitis Media");
        put("7", "Rash/Dermatitis/Insect Bites");
        put("8", "Constipation");
        put("9", "Conjunctivitis");
        put("10", "Prescription Refill");
        put("11", "Urinary Tract Infection");
        put("12", "Allergies");
        put("13", "Strains/Sprains");
        put("14", "General Exam/Preventive Care");
        put("", "");
    }};

    @Override
    protected void handleResponse(SearchResponse response) {
        List<Terms.Bucket> top10DiagnosisList;
        List<Terms.Bucket> otherDiagnosisList = new ArrayList<>();
        double erVisitCost = 0.0d;

        Nested nestedTotalAmount = response.getAggregations().get("nestedVisitAmounts");
        erVisitCost = AmountUtils.getAmount(extractVisitAmount(erVisitCost, nestedTotalAmount));
        state.hits.put("totalErCost", erVisitCost);

        Sum erVisit=response.getAggregations().get("erVisitCount");
        extractMetaInfo(erVisitCost, erVisit);

        Terms diagnosis = response.getAggregations().get("CategoryTerms");
        if (diagnosis.getBuckets().size() > 10) {
            top10DiagnosisList = diagnosis.getBuckets().subList(0, 10);
            otherDiagnosisList = diagnosis.getBuckets().subList(10, diagnosis.getBuckets().size());
        }
        else top10DiagnosisList = diagnosis.getBuckets();

        top10DiagnosisExtractor(top10DiagnosisList);
        otherDiagnosisExtractor(otherDiagnosisList);
    }



    private void otherDiagnosisExtractor(List<Terms.Bucket> otherDiagnosisList) {
        Wrapper wrapper = getRecord(state, "other");
        double totalPaidAmount = 0.0d;
        double erVisitCount = 0.0d;
        List<String> diagList=new ArrayList<>();

        if (otherDiagnosisList.size() != 0) {
            for (Terms.Bucket bucket : otherDiagnosisList) {
                Nested nestedTotalAmount = bucket.getAggregations().get("nestedVisitAmounts");
                totalPaidAmount = extractVisitAmount(totalPaidAmount, nestedTotalAmount);
                Sum erVisit = bucket.getAggregations().get("erVisitCount");
                erVisitCount = erVisitCount + erVisit.getValue();
                diagList.add(bucket.getKey());
            }
        }
        wrapper.recordMap.put("condition", "other");
        wrapper.recordMap.put("diagCode", diagList);
        fillWrapper(wrapper, totalPaidAmount, erVisitCount);
    }


    private double extractVisitAmount(double totalPaidAmount, Nested nestedTotalAmount) {
        Filter filter = nestedTotalAmount.getAggregations().get("visitAmountFilter");
        Sum totalPaidSum = filter.getAggregations().get("totalPaid");
        totalPaidAmount = totalPaidAmount + totalPaidSum.getValue();
        return totalPaidAmount;
    }

    private void top10DiagnosisExtractor(List<Terms.Bucket> top10DiagnosisList) {
        for (Terms.Bucket bucket : top10DiagnosisList) {
            double totalPaidAmount = 0.0d;
            Nested nestedTotalAmount = bucket.getAggregations().get("nestedVisitAmounts");
            Wrapper wrapper = getRecord(state, bucket.getKey());
            Terms diagnosisDesc = bucket.getAggregations().get("description");
            totalPaidAmount = extractVisitAmount(totalPaidAmount, nestedTotalAmount);
            String diagnosisDes = diagnosisDesc.getBuckets().size() > 0 ? diagnosisDesc.getBuckets().get(0).getKey() : "";

            Sum erVisit = bucket.getAggregations().get("erVisitCount");
            wrapper.recordMap.put("condition", diagnosisDes);
            wrapper.recordMap.put("diagCode", bucket.getKey());
            fillWrapper(wrapper, totalPaidAmount, erVisit.getValue());
        }
    }

    private void fillWrapper(Wrapper wrapper, double totalPaidAmount, double erVisitCount) {
        double per1000;
        per1000= CalculatorUtils.perThousand(erVisitCount, (double) state.hits.get("memberMonths"));
        wrapper.recordMap.put("erVisit", erVisitCount);
        wrapper.recordMap.put("erVisitPer1000", per1000);
        wrapper.recordMap.put("totalAmount", AmountUtils.getAmount(totalPaidAmount));
        wrapper.recordMap.put("percentOfTotalAmount", CalculatorUtils.calculatePercentage(AmountUtils.getAmount(totalPaidAmount), (double) state.hits.get("totalErCost")));
        wrapper.recordMap.put("percentOfErVisitPer1000", CalculatorUtils.calculatePercentage(per1000, (double) state.hits.get("totalErVisitPer1000")));
    }

    private void extractMetaInfo(double erVisitCost, Sum erVisit) {
        double per1000;
        Wrapper wrapper = getRecord(state, "metaInfo");
        wrapper.recordMap.put("memberMonths", state.hits.get("memberMonths"));
        wrapper.recordMap.put("totalErCost", erVisitCost);
        wrapper.recordMap.put("totalErVisit",erVisit.getValue());

        per1000=CalculatorUtils.perThousand(erVisit.getValue(), (double) state.hits.get("memberMonths"));
        wrapper.recordMap.put("totalErVisitPer1000",per1000);
        state.hits.put("totalErVisitPer1000",per1000);

    }

    @Override
    protected Wrapper getRecord(RequestState state, String id) {
        if (!state.recordIds.containsKey(id)) {
            Wrapper wrapper = new Wrapper(id);
            state.recordIds.put(id, wrapper);
        }
        return (Wrapper) state.recordIds.get(id);
    }

    public class Wrapper extends Record implements XContentWriter {
        public Wrapper(String id) {
            super(id);
        }
    }
}
