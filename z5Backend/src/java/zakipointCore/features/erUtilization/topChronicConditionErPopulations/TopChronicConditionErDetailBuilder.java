package zakipointCore.features.erUtilization.topChronicConditionErPopulations;

import com.configurations.utils.VisitAdmission;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 4/6/16.
 */
public class TopChronicConditionErDetailBuilder extends TopChronicConditionErBuilder {

    @Override
    protected void erVisitMetricBuilder(FiltersAggregationBuilder chronicConditionsFilters, AndFilterBuilder erVisitFilter, RequestState state) {
        chronicConditionsFilters.subAggregation(AggregationBuilders.filter("erVisitors").filter(erVisitFilter)
                .subAggregation(AggregationBuilders.terms("diagGrouperWise").field("diag1SupergrouperId").size(0)
                        .subAggregation(AggregationBuilders.terms("membersCount").field(VisitAdmission.INT_MEMBER_ID).size(0)
                                .subAggregation(AggregationBuilders.sum("erVisitCount").field(VisitAdmission.ER_VISIT)))
                        .subAggregation(CommonQueries.nestedVisitAmountSum(state))
                        .subAggregation(AggregationBuilders.terms("diagnosisDesc").field("diag1Supergrouperdesc").size(1)))
                .subAggregation(AggregationBuilders.terms("conditionWise").field("intMemberId").size(0)
                        .subAggregation(AggregationBuilders.sum("conditionWiseErVisitors").field(VisitAdmission.ER_VISIT)))
                .subAggregation(CommonQueries.nestedVisitAmountSum(state)))
                .subAggregation(AggregationBuilders.terms("allMembers").field(VisitAdmission.INT_MEMBER_ID).size(0));
    }
}
