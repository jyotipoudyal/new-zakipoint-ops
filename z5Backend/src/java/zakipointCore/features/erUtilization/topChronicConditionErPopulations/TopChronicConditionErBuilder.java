package zakipointCore.features.erUtilization.topChronicConditionErPopulations;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 3/14/16.
 */
public class TopChronicConditionErBuilder implements SummaryQueryBuilder {

    public static final Map<String, String> QM_ESFIELD_HASMETRIC_INFO = new HashMap<String, String>() {
        {
            put("Asthma__Asthma", "hasAsthma");
            put("COPD__Chronic Obstructive Pulmonary Disease", "hasCopd");
            put("CHF__Congestive Heart Failure", "hasCongestiveHeartFailure");
            put("CAD__Coronary Artery Disease", "hasCad");
            put("Diabetes__Diabetes", "hasDiabetes");
            put("Hyperlipidemia__Hyperlipidemia", "hasHyperlipidemia");
            put("RheumatoidArthritis__Rheumatoid Arthritis", "hasRheumatoidArthritis");
            put("Hypertension__Hypertension", "hasHypertension");
            put("Depression__Depression", "hasDepression");
            put("Cancer__Cancer", "hasCancer");
            put("ChronicPain__ChronicPain", "hasChronicPain");
            put("AffectivePsychosis__Affective Psychosis", "hasAffectivePsychosis");
            put("AtrialFibrillation__Atrial Fibrillation", "hasAtrialFibrillation");
            put("BloodDisorders__Blood Disorders", "hasBloodDisorders");
            put("DemyelinatingDiseases__Demyelinating Diseases", "hasDemyelinatingDiseases");
            put("CKD__CKD", "hasEsrd");
            put("EatingDisorders__Eating Disorders", "hasEatingDisorders");
            put("HIVAIDS__HIV/AIDS", "hasHivAids");
            put("ImmuneDisorders__Immune Disorders", "hasImmuneDisorders");
            put("InflammatoryBowelDisease__Inflammatory Bowel Disease", "hasInflammatoryBowelDisease");
            put("LiverDiseases__Liver Diseases", "hasLiverDisease");
            put("MorbidObesity__Morbid Obesity", "hasMorbidObesity");
            put("Osteoarthritis__Osteoarthritis", "hasOsteoarthritis");
            put("PeripheralVascularDisease__Peripheral Vascular Disease", "hasPeripheralVascularDisease");
        }
    };


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(visitAdmission(state, client));
        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }


    public SearchRequestBuilder visitAdmission(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, VisitAdmission.NAME);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        FilterUtils.serviceDateFilter(state, andFilterBuilder);
        CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        LevelUtils.setLOAFilter(state, andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        applyAggregations(builder, state);
        return builder;
    }


    private void applyAggregations(SearchRequestBuilder builder, RequestState state) {

        FiltersAggregationBuilder chronicConditionsFilters = AggregationBuilders.filters("chrocnicConditionsFilters");

        for (Map.Entry<String, String> qmPair : QM_ESFIELD_HASMETRIC_INFO.entrySet()) {
            AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
            andFilterBuilder.add(FilterBuilders.hasParentFilter(MemberSearch.NAME, FilterBuilders.termFilter(qmPair.getValue(), "Yes")));
            chronicConditionsFilters.filter(qmPair.getKey(), andFilterBuilder);
        }

        AndFilterBuilder erVisitFilter = new AndFilterBuilder();
        FilterUtils.getUmVisitFilter(erVisitFilter, VisitAdmission.ER_VISIT);
        erVisitMetricBuilder(chronicConditionsFilters, erVisitFilter, state);
        builder.addAggregation(chronicConditionsFilters);
    }


    protected void erVisitMetricBuilder(FiltersAggregationBuilder chronicConditionsFilters, AndFilterBuilder erVisitFilter, RequestState state) {

        chronicConditionsFilters.subAggregation(AggregationBuilders.filter("erVisitors").filter(erVisitFilter)
                .subAggregation(AggregationBuilders.terms("membersCount").field(VisitAdmission.INT_MEMBER_ID).size(0)
                        .subAggregation(AggregationBuilders.sum("erVisitCount").field(VisitAdmission.ER_VISIT)))

                .subAggregation(CommonQueries.nestedVisitAmountSum(state))
                .subAggregation(AggregationBuilders.terms("diagnosisCode").field("diag1SupergrouperId").size(0)
                        .subAggregation(AggregationBuilders.terms("diagnosisDesc").field("diag1Supergrouperdesc").size(1))))

                .subAggregation(AggregationBuilders.terms("allMembers").field(VisitAdmission.INT_MEMBER_ID).size(0));
    }
}
