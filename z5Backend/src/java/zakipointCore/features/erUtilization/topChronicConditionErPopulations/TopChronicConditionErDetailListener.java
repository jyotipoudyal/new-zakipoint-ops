package zakipointCore.features.erUtilization.topChronicConditionErPopulations;

import com.configurations.XContentWriter;
import com.configurations.utils.AmountUtils;
import com.configurations.utils.ChronicConditionEnum;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.listener.AbstractSummaryListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 4/6/16.
 */
public class TopChronicConditionErDetailListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {
        for (Aggregation aggregation : response.getAggregations()) {
            Filters filters = (Filters) aggregation;
            for (Filters.Bucket chronicConditionsBucket : filters.getBuckets()) {
                Wrapper record = getRecord(state, chronicConditionsBucket.getKey().split("__")[0]);

                Map<String, Object> conditionWiseTotalMap = new HashMap<>();
                conditionWiseTotalMap.put("description", chronicConditionsBucket.getKey().split("__")[1]);
                conditionWiseTotalMap.put("chronicCode", ChronicConditionEnum.ChronicConditionMetrics.getMetricByMetricDesc(chronicConditionsBucket.getKey().split("__")[0]).getMetricCode());

                Terms allMembers = chronicConditionsBucket.getAggregations().get("allMembers");
                conditionWiseTotalMap.put("member", (double) allMembers.getBuckets().size());

                extractErVisitors(chronicConditionsBucket, record, conditionWiseTotalMap, allMembers);
                record.recordMap.put("total", conditionWiseTotalMap);
            }
        }
    }


    private void extractErVisitors(Filters.Bucket chronicConditionsBucket, Wrapper record, Map<String, Object> conditionWiseTotalMap, Terms allMembers) {

        Filter erVisitors = chronicConditionsBucket.getAggregations().get("erVisitors");
        Terms conditionWiseErVisitor = erVisitors.getAggregations().get("conditionWise");

        Nested nestedVisitAmounts = erVisitors.getAggregations().get("nestedVisitAmounts");
        conditionWiseTotalMap.put("erCost", calculateTotalVisitAmounts(nestedVisitAmounts));
        extractDiagWiseErVisitCount(record, erVisitors, (double) conditionWiseErVisitor.getBuckets().size());

        calculateErVisitCount(conditionWiseErVisitor, conditionWiseTotalMap, "conditionWiseErVisitors");
        conditionWiseTotalMap.put("erVisitor", (double) conditionWiseErVisitor.getBuckets().size());
        conditionWiseTotalMap.put("nonErVisitor", (double) (allMembers.getBuckets().size() - conditionWiseErVisitor.getBuckets().size()));
    }


    private double calculateTotalVisitAmounts(Nested nestedVisitAmounts) {
        Filter visitAmountFilter = nestedVisitAmounts.getAggregations().get("visitAmountFilter");
        Sum totalPaid = visitAmountFilter.getAggregations().get("totalPaid");
        return AmountUtils.getAmount(totalPaid.getValue());
    }


    private void extractDiagWiseErVisitCount(Wrapper record, Filter totalErVisitor, double totalErVisitors) {

        Terms diagnosisGroups = totalErVisitor.getAggregations().get("diagGrouperWise");

        for (Terms.Bucket bucket : diagnosisGroups.getBuckets()) {
            Map<String, Object> diagGroupWiseRecordMap = new HashMap<>();
            Terms erVisitMemberCount = bucket.getAggregations().get("membersCount");
            double daigGroupWiseErVisitor = (double) erVisitMemberCount.getBuckets().size();
            diagGroupWiseRecordMap.put("erVisitor", daigGroupWiseErVisitor);
            calculateErVisitCount(erVisitMemberCount, diagGroupWiseRecordMap, "erVisitCount");

            Nested visitAmount = bucket.getAggregations().get("nestedVisitAmounts");
            diagGroupWiseRecordMap.put("erCost", calculateTotalVisitAmounts(visitAmount));
            diagGroupWiseRecordMap.put("nonErVisitor", totalErVisitors - daigGroupWiseErVisitor);

            Terms diagnosisDesc = bucket.getAggregations().get("diagnosisDesc");
            for (Terms.Bucket diagDesc : diagnosisDesc.getBuckets()) {
                diagGroupWiseRecordMap.put("description", diagDesc.getKey());
            }
            record.recordMap.put(bucket.getKey(), diagGroupWiseRecordMap);
        }
    }


    private void calculateErVisitCount(Terms erVisitMemberCount, Map<String, Object> diagGroupWiseRecordMap, String aggName) {

        double oneVisit = 0.0d;
        double twoOrMoreVisits = 0.0d;

        for (Terms.Bucket bucket : erVisitMemberCount.getBuckets()) {
            Sum erVisitCount = bucket.getAggregations().get(aggName);
            if (erVisitCount.getValue() == 1) oneVisit++;
            else twoOrMoreVisits++;
        }


        diagGroupWiseRecordMap.put("oneTimeVisitor", oneVisit);
        diagGroupWiseRecordMap.put("twoOrMoreTimesVisitor", twoOrMoreVisits);
    }

    protected Wrapper getRecord(RequestState state, String id) {
        if (!state.recordIds.containsKey(id)) {
            Wrapper wrapper = new Wrapper(id);
            state.recordIds.put(id, wrapper);
        }
        return (Wrapper) state.recordIds.get(id);
    }


    public class Wrapper extends Record implements XContentWriter {
        public Wrapper(String id) {
            super(id);
        }

        @Override
        public void toXContent(XContentBuilder builder, Params params) throws IOException {
            builder.startObject(id);

            for (Map.Entry<String, Object> pair : recordMap.entrySet()) {

                if (pair.getValue() instanceof Map) {
                    Map<String, Object> innerMap = (Map<String, Object>) pair.getValue();
                    builder.startObject(pair.getKey());

                    for (Map.Entry<String, Object> innerPair : innerMap.entrySet()) {
                        builder.field(innerPair.getKey(), innerPair.getValue());
                    }
                    builder.endObject();
                }

                else builder.field(pair.getKey(), pair.getValue());
            }
            addFurtherFields(builder);
        }
    }
}
