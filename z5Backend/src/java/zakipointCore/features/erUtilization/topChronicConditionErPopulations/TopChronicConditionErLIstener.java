package zakipointCore.features.erUtilization.topChronicConditionErPopulations;

import com.configurations.XContentWriter;
import com.configurations.utils.AmountUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.listener.AbstractSummaryListener;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 3/14/16.
 */
public class TopChronicConditionErLIstener extends AbstractSummaryListener {
    private final List<String> FIELDS_TOBE_PERCENTAGE_CALCULATED = Arrays.asList("oneTimeVisitor", "twoOrMoreTimesVisitor");

    @Override
    protected void handleResponse(SearchResponse response) {
        Wrapper recordForTotalPaidAmt = getRecord(state, "total");
        for (Aggregation aggregation : response.getAggregations()) {
            Filters filters = (Filters) aggregation;
            for (Filters.Bucket bucket : filters.getBuckets()) {
                Wrapper record = getRecord(state, bucket.getKey().split("__")[0]);
                record.recordMap.put("description", bucket.getKey().split("__")[1]);

                Filter erVisitFilter = bucket.getAggregations().get("erVisitors");
                Terms allMembers = bucket.getAggregations().get("allMembers");

                extractErVisitCount(erVisitFilter, record, recordForTotalPaidAmt);
                extractAllMember(allMembers, record);
            }
        }
    }

    private void extractAllMember(Terms allMembers, Record record) {
        record.recordMap.put("totalMember", (double) allMembers.getBuckets().size());
    }

    protected void extractErVisitCount(Filter erVisitFilter, Wrapper record, Wrapper recordForTotalPaidAmt) {

        for (Aggregation aggregation : erVisitFilter.getAggregations()) {

            if (aggregation.getName().equalsIgnoreCase("membersCount")) {

                Terms uniqueMember = (Terms) aggregation;
                record.recordMap.put("erVisitors", (double) uniqueMember.getBuckets().size());
                double oneVisit = 0.0d;
                double twoOrMoreVisits = 0.0d;

                for (Terms.Bucket bucket : uniqueMember.getBuckets()) {
                    Sum erVisitCount = bucket.getAggregations().get("erVisitCount");
                    if (erVisitCount.getValue() == 1) oneVisit++;
                    else twoOrMoreVisits++;
                }
                record.recordMap.put("oneTimeVisitor", oneVisit);
                record.recordMap.put("twoOrMoreTimesVisitor", twoOrMoreVisits);

            }

            else if (aggregation.getName().contains("nestedVisitAmounts")) {

                Nested nestedVisitAmounts = (Nested) aggregation;
                Sum totalErSpent = nestedVisitAmounts.getAggregations().get("totalErCost");
                System.out.println("totalErSpent = " + totalErSpent.getValue());
                sumUpChronicAmounts(AmountUtils.getAmount(totalErSpent.getValue()), recordForTotalPaidAmt);
                record.recordMap.put("totalErCost", AmountUtils.getAmount(totalErSpent.getValue()));
            }

            else if (aggregation.getName().contains("diagnosisCode")) {

                Terms primaryDiagCode = ((StringTerms) aggregation);
                Map<String, String> codeDescMap = new HashMap<>();

                for (Terms.Bucket codes : primaryDiagCode.getBuckets()) {
                    Terms codeDesc = codes.getAggregations().get("diagnosisDesc");
                    for (Terms.Bucket bucket : codeDesc.getBuckets()) {
                        codeDescMap.put(codes.getKey(), bucket.getKey());
                    }
                }
                record.recordMap.put("diagnosis", codeDescMap);
            }
        }
    }

    private void sumUpChronicAmounts(double amount, Wrapper recordForTotalPaidAmt) {
        if (!recordForTotalPaidAmt.recordMap.containsKey("totalPaidAmountForAllCondition")) {
            recordForTotalPaidAmt.recordMap.put("totalPaidAmountForAllCondition", 0.0d);
        }
        recordForTotalPaidAmt.recordMap.put("totalPaidAmountForAllCondition", (double) recordForTotalPaidAmt.recordMap.get("totalPaidAmountForAllCondition") + amount);
    }

    protected Wrapper getRecord(RequestState state, String id) {
        if (!state.recordIds.containsKey(id)) {
            Wrapper wrapper = new Wrapper(id);
            state.recordIds.put(id, wrapper);
        }
        return (Wrapper) state.recordIds.get(id);
    }


    public class Wrapper extends Record implements XContentWriter {
        public Wrapper(String id) {
            super(id);
        }

        @Override
        public void toXContent(XContentBuilder builder, Params params) throws IOException {
            super.toXContent(builder, params);
        }

        @Override
        protected void addFurtherFields(XContentBuilder builder) throws IOException {

            if (!id.equalsIgnoreCase("total")) {
                double nonErVisitors = (double) recordMap.get("totalMember") - (double) recordMap.get("erVisitors");
                builder.field("nonErvistors", nonErVisitors);
                builder.field("percentOfnonErvistors", CalculatorUtils.calculatePercentage(nonErVisitors, ((double) recordMap.get("totalMember"))));

                for (String field : FIELDS_TOBE_PERCENTAGE_CALCULATED) {
                    builder.field("percentOf" + field, CalculatorUtils.calculatePercentage((double) recordMap.get(field), ((double) recordMap.get("totalMember"))));
                }
            }
            builder.endObject();
        }
    }
}
