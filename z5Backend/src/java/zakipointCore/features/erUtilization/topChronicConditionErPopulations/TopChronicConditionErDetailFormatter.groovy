package zakipointCore.features.erUtilization.topChronicConditionErPopulations

import com.configurations.ResponseFormatter
import org.codehaus.groovy.grails.web.json.JSONObject

/**
 * Created by sktamang on 4/7/16.
 */
class TopChronicConditionErDetailFormatter implements ResponseFormatter {
    List<String> METRIC_PERCENTAGE_CALCULATION = Arrays.asList("nonErVisitor", "oneTimeVisitor", "twoOrMoreTimesVisitor")

    @Override
    JSONObject format(JSONObject dasResponse, Map<String, String> requestMap) {
        dasResponse.each { String period, JSONObject values ->
            double totalErCostForAllConditions = 0.0d
            values.each {String chronicConditionKeys, Object innervalues ->
                Map<String, Object> innerMap = innervalues;
                innerMap.each { String diagnosisKeys, Object diagMetrics ->
                    for (String metric : METRIC_PERCENTAGE_CALCULATION) {
                        diagMetrics.put("percentOf" + metric,
                                CalculatorUtils.calculatePercentage(diagMetrics.get(metric), innerMap.total.member))
                    }
                }
                totalErCostForAllConditions = totalErCostForAllConditions + innerMap.total.erCost
            }
            values.put("totalPaidAmountForAllCondition", totalErCostForAllConditions)
        }
        return dasResponse
    }
}
