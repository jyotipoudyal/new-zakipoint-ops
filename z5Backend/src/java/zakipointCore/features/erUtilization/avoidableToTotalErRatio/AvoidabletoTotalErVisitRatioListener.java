package zakipointCore.features.erUtilization.avoidableToTotalErRatio;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.DateUtils;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 1/5/16.
 */
public class AvoidabletoTotalErVisitRatioListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {
        Record record = getRecord(state, DateUtils.getYear(state.periodFrom()));
        Filters filters = response.getAggregations().get("filters");
        for (Filters.Bucket bucket : filters.getBuckets()) {
            Sum sum = bucket.getAggregations().get("totalErVisits");
            record.recordMap.put(bucket.getKey(), sum.getValue());
            record.recordMap.put(bucket.getKey() + "perThousand", CalculatorUtils.perThousand(sum.getValue(), (double) state.hits.get("memberMonths")));
        }
        record.recordMap.put("memberMonths", state.hits.get("memberMonths"));
        record.recordMap.put("percentOfAvoidableErVisits"
                , CalculatorUtils.calculatePercentage((double) record.recordMap.get(AvoidabletoTotalErVisitRatioBuilder.TOTAL_AVOIDABLE_VISIST),
                        (double) record.recordMap.get(AvoidabletoTotalErVisitRatioBuilder.TOTAL_VISIST)));
    }
}
