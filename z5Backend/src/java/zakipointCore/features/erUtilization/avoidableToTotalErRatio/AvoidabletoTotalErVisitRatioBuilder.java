package zakipointCore.features.erUtilization.avoidableToTotalErRatio;

import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 1/5/16.
 */
public class AvoidabletoTotalErVisitRatioBuilder extends AvoidableToTotalErRatioBuilder {
    public static String TOTAL_VISIST="totalErVisits";
    public static String TOTAL_AVOIDABLE_VISIST="totalAvoidableErVisits";

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        builder.add(query(state,client));

        //calculate memberMonths
        double memberMonths=0.0d;
        state.hits.put("memberMonths", CommonQueries.getTotalMemberMonth(state,client,memberMonths));
        return builder;
    }

    @Override
    protected void applyAggregations(SearchRequestBuilder builder,RequestState state) {
        FiltersAggregationBuilder filters= AggregationBuilders.filters("filters");
        filters.filter(TOTAL_VISIST,FilterBuilders.matchAllFilter());
        filters.filter(TOTAL_AVOIDABLE_VISIST,getAvoidableErVisitFilter());

        filters.subAggregation(AggregationBuilders.sum("totalErVisits").field(VisitAdmission.ER_VISIT));
        builder.addAggregation(filters);
    }
}
