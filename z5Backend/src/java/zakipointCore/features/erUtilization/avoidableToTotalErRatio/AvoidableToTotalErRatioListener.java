package zakipointCore.features.erUtilization.avoidableToTotalErRatio;

import com.configurations.utils.AmountUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.DateUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 12/24/15.
 */
public class AvoidableToTotalErRatioListener extends AbstractSummaryListener {

    private Map<String,String> AGG_NAME_MAP=new HashMap<String,String>(){
            {
                put("nestedVisitAmounts","totalPaidAmountForEr");
                put("totalPaidAmountForAvoidableEr","totalPaidAmountForAvoidableEr");
            }
    };

    @Override
    protected void handleResponse(SearchResponse response) {
        Record wrapper = getRecord(state, DateUtils.getYear(state.periodFrom()));
            for (Aggregation aggregation:response.getAggregations()) {
                double paidAmount=0.0d;
                if(aggregation instanceof Filter) paidAmount=getSumOfTotalPaid(((Filter) aggregation).getAggregations().get(AvoidableToTotalErRatioBuilder.NESTED_VISITAMOUNTS));
                else paidAmount=getSumOfTotalPaid(aggregation);
                wrapper.recordMap.put(AGG_NAME_MAP.get(aggregation.getName()), AmountUtils.getAmount(paidAmount));
            }
        wrapper.recordMap.put("percentOfAvoidableEr", CalculatorUtils.calculatePercentage((double) wrapper.recordMap.get(AvoidableToTotalErRatioBuilder.AVOIDABLE_ER), (double) wrapper.recordMap.get(AvoidableToTotalErRatioBuilder.ALL_ER)));
    }

    private double getSumOfTotalPaid(Aggregation aggregation) {
        Filter visitAmountFilter=((Nested)aggregation).getAggregations().get("visitAmountFilter");
        return ((Sum)(visitAmountFilter.getAggregations().get(AvoidableToTotalErRatioBuilder.TOTAL_ER_PAID))).getValue();
    }
}

