package zakipointCore.features.erUtilization.avoidableToTotalErRatio;

import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 12/24/15.
 */
public class AvoidableToTotalErRatioBuilder implements SummaryQueryBuilder {
    public static final String ALL_ER = "totalPaidAmountForEr";
    public static final String AVOIDABLE_ER = "totalPaidAmountForAvoidableEr";
    public static final String NESTED_VISITAMOUNTS = "nestedVisitAmounts";
    public static final String TOTAL_ER_PAID = "totalPaid";

    public static final int AVOIDABLE_ER_VISITS_IDS[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(query(state, client));
        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearch(client, state.request);
        builder.setSize(0).setTypes(VisitAdmission.NAME);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        FilterUtils.getUmVisitFilter(andFilterBuilder, VisitAdmission.ER_VISIT);
        FilterUtils.serviceDateFilter(state, andFilterBuilder);
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));

        LevelUtils.setLOAFilter(state, andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        applyAggregations(builder, state);
        return builder;
    }

    protected void applyAggregations(SearchRequestBuilder builder, RequestState state) {
        builder.addAggregation(avoidableErPaidAggregations(state));
        builder.addAggregation(CommonQueries.nestedVisitAmountSum(state));
    }

    private AbstractAggregationBuilder avoidableErPaidAggregations(RequestState state) {
        AndFilterBuilder andFilterBuilder = getAvoidableErVisitFilter();
        return AggregationBuilders.filter(AVOIDABLE_ER).filter(andFilterBuilder).subAggregation(CommonQueries.nestedVisitAmountSum(state));
    }

    protected AndFilterBuilder getAvoidableErVisitFilter() {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.termFilter(VisitAdmission.AVOIDABLE_FLAG, 1))
                .add(FilterBuilders.termsFilter(VisitAdmission.REASON_ERVISIT, AVOIDABLE_ER_VISITS_IDS));
        return andFilterBuilder;
    }
}
