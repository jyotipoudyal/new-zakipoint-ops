package zakipointCore.features.biometric

import com.z5.BiometricLevelInfo

/**
 * Created by sktamang on 4/3/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class BiometricDomainOperation {
    static List<String> LOA_LIST_BIOMETIRC = Arrays.asList("biometricName");

    public static Map<String, String> getBiometrics(Map<String, String> params) {
        Map<String, String> biometricMap = new HashMap<>();

        Map<String, Object> PARAM_MAP = new HashMap<>();

        for (String s : LOA_LIST_BIOMETIRC) {
            if (params.containsKey(s)) {
                PARAM_MAP.put("biometricId", params.get(s));
            }
        }


        List<BiometricLevelInfo> list = BiometricLevelInfo.createCriteria().list() {
            PARAM_MAP.each { k, v ->
                eq k, v
            }
        }

        for (BiometricLevelInfo bmi : list) {
            biometricMap.put(bmi.biometricId + "__" + bmi.levelRangeName, bmi.rangeBegin + "__" + bmi.rangeEnd)
        }
        return biometricMap;
    }
}
