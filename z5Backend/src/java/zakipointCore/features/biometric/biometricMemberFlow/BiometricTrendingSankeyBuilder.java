package zakipointCore.features.biometric.biometricMemberFlow;

import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.drill.ConsecutiveDateGenerator;
import zakipointCore.features.biometric.BiometricDomainOperation;
import zakipointCore.features.biometric.dashboard.BiometricSnapshotBuilder;

import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 12/21/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class BiometricTrendingSankeyBuilder extends BiometricSnapshotBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        networkUsageBuilder(state,client,builder);
        return builder;
    }


    private void networkUsageBuilder(RequestState state,Client client,MultiSearchRequestBuilder builder){

        state.request.requestParameters.put("reportingFrom","2015-04-01");
        state.request.requestParameters.put("reportingTo","2016-03-31");

        List<RequestState> requestStateList= ConsecutiveDateGenerator.getConsecutiveRequestStates(state.request.requestParameters);
        Map<String,String> biometricMap= BiometricDomainOperation.getBiometrics(state.request.requestParameters);

        state.hits.put("biometricInfoMap",biometricMap);

        for (RequestState requestState : requestStateList) {
            SearchRequestBuilder searchRequestBuilder=biometricBuilder(requestState,client,biometricMap);
            searchRequestBuilder.setSize(1);

            String scriptField="\""+requestState.request.get("reportingPeriod")+"\"";
            searchRequestBuilder.addScriptField("period",scriptField);
            builder.add(searchRequestBuilder);
        }
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
