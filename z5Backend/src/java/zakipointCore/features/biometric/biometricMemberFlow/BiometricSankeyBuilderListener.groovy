package zakipointCore.features.biometric.biometricMemberFlow
import com.z5.Biometric
import com.zakipoint.service.ReportFromDomainService
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import zakipointCore.builder.ReportFromDomainBuilders
import zakipointCore.features.biometric.BiometricDomainOperation

/**
 * Created by sktamang on 6/21/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class BiometricSankeyBuilderListener implements ReportFromDomainBuilders{
    private  List<String> LOA_LIST = Arrays.asList("groupId","biometricName")
    List<String> PERIOD_LIST = ["current", "last","prior"]
    private List<String> MEMBER_LIST=new ArrayList<>();

    List<String> getMEMBER_LIST() {
        return MEMBER_LIST
    }

    void setMEMBER_LIST(List<String> MEMBER_LIST) {
        this.MEMBER_LIST = MEMBER_LIST
    }

    @Override
    JSONObject getResults(Map<String, String> requestParams) {
        Map<String,String> biometricMap= BiometricDomainOperation.getBiometrics(requestParams);
        JSONObject finalJSON = new JSONObject();

        Map<String, List<String>> YEAR_TO_MEMBERLIST_MAP = new HashMap<>()
        def loaMap = populateLoaMap(requestParams)

        JSONArray jsonArray = new JSONArray();
        int i = 0;
        List<String> levels=new ArrayList<>();
        for (String period : PERIOD_LIST) {
            JSONObject memberStorer = new JSONObject()

            for (String level : biometricMap.keySet()) {
                String level1=level.split("__")[1]
                JSONObject nodes = new JSONObject();
                Object memberList = getMemberListForPeriodAndLevel(loaMap, period, level1, MEMBER_LIST)

                YEAR_TO_MEMBERLIST_MAP.put(period + "_" + level, memberList)
                nodes.put("year", period)
                nodes.put("name", level1)
                nodes.put("total", memberList.size())
                nodes.put("id", i)
                nodes.put("node",period + "_" + level1)
                jsonArray.put(nodes)

                memberStorer.put(level1, memberList)
                if(!levels.contains(level1))levels.add(level1)
                i++
            }
            finalJSON.put(period, memberStorer)
        }
        finalJSON.put("levels",levels)
        finalResponseGenerator(finalJSON)
        return finalJSON.put("nodes", jsonArray)
    }

    Object getMemberListForPeriodAndLevel(loaMap, String period, String level, List<String> memberListGet) {
        def levelWiseMember = Biometric.createCriteria()
        def memberList = levelWiseMember.list {
            loaMap.each { k, v ->
                eq k, v
            }
            and {
                eq("period", period)
                eq("biometricLevel", level)
                if (memberListGet.size() > 0) 'in'("unblindMemberId", memberListGet)
            }
            projections {
                property('unblindMemberId')
            }
        }
        memberList
    }

    Map<String, String> populateLoaMap(Map<String, String> requestMap) {
        def loaMap = [:]
        for (String key : requestMap.keySet()) {
            if (LOA_LIST.contains(key)) {
                Object value = requestMap.get(key);
                String key1=key
                if(key.equalsIgnoreCase("biometricName")) key1="biometricId"
                if(key.equalsIgnoreCase("group")){
                    key1=key+"Id"
                    value=RequestUtils.getArrayRequestValue(requestMap.get(key))
                }
                loaMap.put(key1, value)
            }
        }
        return loaMap
    }

    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        JSONObject current = jsonObject.has("current") == true ? (JSONObject) jsonObject.get("current") : new JSONObject();
        JSONObject last = jsonObject.has("last") == true ? (JSONObject) jsonObject.get("last") : new JSONObject();
        JSONObject prior = jsonObject.has("prior") == true ? (JSONObject) jsonObject.get("prior") : new JSONObject();

        JSONArray linkArray = new JSONArray();
        retainMembers(last, current, "last", "current", linkArray);
        retainMembers(prior, last, "prior", "last", linkArray);

        jsonObject.put("links", linkArray);

        PERIOD_LIST.each { jsonObject.remove(it) }

        return jsonObject;
    }

    private Map<String, Object> retainMembers(JSONObject initial, JSONObject terminal, String initKey, String termKey, JSONArray linkObject) {

        Map<String, Object> retainInfoMap = new HashMap<>();
        for (Object intialKeys : initial.keySet()) {

            String initialKey = intialKeys.toString();
            List memberListInitial = new ArrayList((List) initial.get(initialKey));

            for (Object finalKeys : terminal.keySet()) {
                String finalKey = finalKeys.toString();
                List memberListTerminal = new ArrayList((List) terminal.get(finalKey));

                memberListTerminal.retainAll(memberListInitial);
                JSONObject tempMap = new JSONObject();

                tempMap.put("source", initKey + "_" + initialKey);
                tempMap.put("target", termKey + "_" + finalKey);
                tempMap.put("value", memberListTerminal.size());
                linkObject.put(tempMap);
            }
        }
        return retainInfoMap;
    }

    @Override
    Object getDrillResults(Map<String, String> requestParams) {
        String drillType=requestParams.get("drillType")
        ReportFromDomainService.loaParamsManagement(requestParams)
        def loaMap = populateLoaMap(requestParams)

        if(drillType.equalsIgnoreCase("sediment")){
            def period=requestParams.get("period")
            def level=requestParams.get("level")
            Object memberList = getMemberListForPeriodAndLevel(loaMap, period, level, MEMBER_LIST)
            return memberList
        }
        else if(drillType.equalsIgnoreCase("flow")){
            String[] source=requestParams.get("source").split("_")
            String[] target=requestParams.get("target").split("_")

            List<String> memberListSource=getMemberListForPeriodAndLevel(loaMap,source[0],source[1],MEMBER_LIST)
            List<String> memberListTarget=getMemberListForPeriodAndLevel(loaMap,target[0],target[1],MEMBER_LIST)

            memberListTarget.retainAll(memberListSource)
            return memberListTarget;
        }
        return null
    }
}
