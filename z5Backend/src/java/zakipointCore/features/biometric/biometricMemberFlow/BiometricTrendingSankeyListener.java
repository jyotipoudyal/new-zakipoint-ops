package zakipointCore.features.biometric.biometricMemberFlow;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.features.network.networkTrendingSankey.NetworkTrendingSankeyListener;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.*;

/**
 * Created by sktamang on 12/21/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class BiometricTrendingSankeyListener extends NetworkTrendingSankeyListener {

    protected void handleResponse(SearchResponse response, int counter) {

        Map<String,String> biometricMap= (Map<String, String>) state.hits.get("biometricInfoMap");
        Map<String,List<String>> listMap= new HashMap<>();
        List<String> levelList=new ArrayList<>();

        for (String s : biometricMap.keySet()) {
            String levelName=s.split("__")[1];
            levelList.add(levelName);
            listMap.put(levelName,new ArrayList<>());
        }

        state.hits.put("levelList",levelList);

        String period = YEAR_COUNT_MAP.get(counter);
        AbstractSummaryListener.Record record = getRecord(state, period);

        if (response.getHits().getHits().length > 0) {
            Filters filters = response.getAggregations().get("filters");
            for (Filters.Bucket bucket : filters.getBuckets()) {

                Terms memberId=bucket.getAggregations().get("memberId");
                String levelName=bucket.getKey().split("__")[1];
                extractTermsKeySet(listMap.get(levelName),memberId);
            }

        }

        for (String s : listMap.keySet()) {
            record.recordMap.put(s,listMap.get(s));
        }
    }


    private void extractTermsKeySet(List<String> destination,Terms source){
        for (Terms.Bucket bucket : source.getBuckets()) {
            destination.add(bucket.getKey());
        }
    }

    protected void finalize(JSONObject jsonObject) {

        List<String> levelList= (List<String>) state.hits.get("levelList");

        List<String> keyList = Arrays.asList("current", "last", "prior");
        JSONArray finalTemp = new JSONArray();

        int i = 0;
        for (String key : keyList) {

            for (String s : levelList) {
                JSONObject tempJson = new JSONObject();

                tempJson.put("year", key);
                tempJson.put("node", key + "_" + s);
                tempJson.put("total", ((List) ((JSONObject) jsonObject.get(key)).get(s)).size());
                tempJson.put("name", s);
                tempJson.put("id", i);
                finalTemp.put(tempJson);
                i++;
            }
            jsonObject.remove(key);
        }
        jsonObject.put("nodes", finalTemp);
        jsonObject.put("levels",levelList);
    }
}
