package zakipointCore.features.biometric.dashboard

import com.z5.Biometric
import com.zakipoint.service.ReportFromDomainService
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import zakipointCore.builder.ReportFromDomainBuilders
import zakipointCore.commonUtils.RequestUtils

/**
 * Created by sktamang on 7/4/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class BiometricSnapshotBuilderListener implements ReportFromDomainBuilders{

    private  List<String> LOA_LIST = Arrays.asList("groupId","biometricId")
    List<String> PERIOD_LIST = ["current", "last","prior"]
    private List<String> MEMBER_LIST=new ArrayList<>();

    List<String> getMEMBER_LIST() {
        return MEMBER_LIST
    }

    void setMEMBER_LIST(List<String> MEMBER_LIST) {
        this.MEMBER_LIST = MEMBER_LIST
    }


    @Override
    JSONObject getResults(Map<String, String> requestParams) {
        Map<String,String> biometricMap= BiometricDomainOperation.getBiometrics(requestParams);
        JSONObject finalJSON = new JSONObject();

        def loaMap = populateLoaMap(requestParams)
        for (String period : PERIOD_LIST) {
            JSONObject periodWiseObject = new JSONObject()
            JSONObject metaInfoObject=new JSONObject()
            for (String level : biometricMap.keySet()) {
                String biometricId=level.split("__")[0]
                String level1=level.split("__")[1]

                String rangeBegin=biometricMap.get(level).split("__")[0]
                String rangeEnd=biometricMap.get(level).split("__")[1]

                JSONObject biometricIdWiseObject=getBiometricIdWiseObjct(biometricId,periodWiseObject);

                Object metricObject= getMemberListForPeriodAndLevel(loaMap, period, level1,biometricId, MEMBER_LIST)

                int memberListSize=Arrays.asList(metricObject[0]).get(0)
                String metricName=Arrays.asList(metricObject[0]).get(1) == null? biometricId:Arrays.asList(metricObject[0]).get(1)

                biometricIdWiseObject.put("biometricId", biometricId)
                biometricIdWiseObject.put(level1,memberListSize )
                biometricIdWiseObject.put("biometricName", metricName)
                periodWiseObject.put(biometricId,biometricIdWiseObject)


                JSONObject biometricIdWiseMetaInfoObject=getBiometricIdWiseObjct(biometricId,metaInfoObject)
                List<String> levelList=getLevelList(biometricIdWiseMetaInfoObject);
                levelList.add(level1)
                biometricIdWiseMetaInfoObject.put("biometricName",metricName)
                biometricIdWiseMetaInfoObject.put("rangeBegin_"+level1,rangeBegin)
                biometricIdWiseMetaInfoObject.put("rangeEnd_"+level1,rangeEnd)
            }
            finalJSON.put("metaInfo",metaInfoObject)
            finalJSON.put(period, periodWiseObject)
        }

        return finalJSON
    }

    List<String> getLevelList(JSONObject jsonObject) {
        if(jsonObject.containsKey("levels")) return (List<String>)jsonObject.get("levels")
        else{
            jsonObject.put("levels",new ArrayList<String>())
            return (List<String>)jsonObject.get("levels")
        }

    }

    JSONObject getBiometricIdWiseObjct(String biometricId, JSONObject periodWiseObject) {
        if(periodWiseObject.keySet().contains(biometricId)) return periodWiseObject.get(biometricId)
        else {
            periodWiseObject.put(biometricId,new JSONObject());
            return periodWiseObject.get(biometricId)
        }
    }


    Object getMemberListForPeriodAndLevel(loaMap, String period, String level,String biometricId, List<String> memberListGet) {
        def levelWiseMember = Biometric.createCriteria()
        def memberList = levelWiseMember.list {
            loaMap.each { k, v ->
                eq k, v
            }
            and {
                eq("period", period)
                eq("biometricLevel", level)
                eq("biometricId", biometricId)
                if (memberListGet.size() > 0) 'in'("unblindMemberId", memberListGet)
            }
            projections {
                count("unblindMemberId")
                property("biometricName")
            }
        }
        memberList
    }

    Map<String, String> populateLoaMap(Map<String, String> requestMap) {
        def loaMap = [:]
        for (String key : requestMap.keySet()) {
            if (LOA_LIST.contains(key)) {
                Object value = requestMap.get(key);
                String key1=key
                if(key.equalsIgnoreCase("group")){
                    key1=key+"Id"
                    value=RequestUtils.getArrayRequestValue(requestMap.get(key))
                }
                loaMap.put(key1, value)
            }
        }
        return loaMap
    }

    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        JSONObject current = jsonObject.has("current") == true ? (JSONObject) jsonObject.get("current") : new JSONObject();
        JSONObject last = jsonObject.has("last") == true ? (JSONObject) jsonObject.get("last") : new JSONObject();
        JSONObject prior = jsonObject.has("prior") == true ? (JSONObject) jsonObject.get("prior") : new JSONObject();

        JSONArray linkArray = new JSONArray();
        retainMembers(last, current, "last", "current", linkArray);
        retainMembers(prior, last, "prior", "last", linkArray);

        jsonObject.put("links", linkArray);

        PERIOD_LIST.each { jsonObject.remove(it) }

        return jsonObject;
    }

    private Map<String, Object> retainMembers(JSONObject initial, JSONObject terminal, String initKey, String termKey, JSONArray linkObject) {

        Map<String, Object> retainInfoMap = new HashMap<>();
        for (Object intialKeys : initial.keySet()) {

            String initialKey = intialKeys.toString();
            List memberListInitial = new ArrayList((List) initial.get(initialKey));

            for (Object finalKeys : terminal.keySet()) {
                String finalKey = finalKeys.toString();
                List memberListTerminal = new ArrayList((List) terminal.get(finalKey));

                memberListTerminal.retainAll(memberListInitial);
                JSONObject tempMap = new JSONObject();

                tempMap.put("source", initKey + "_" + initialKey);
                tempMap.put("target", termKey + "_" + finalKey);
                tempMap.put("value", memberListTerminal.size());
                linkObject.put(tempMap);
            }
        }
        return retainInfoMap;
    }

    @Override
    Object getDrillResults(Map<String, String> requestParams) {
        String drillType=requestParams.get("drillType")
        ReportFromDomainService.loaParamsManagement(requestParams)
        def loaMap = populateLoaMap(requestParams)

        if(drillType.equalsIgnoreCase("sediment")){
            def period=requestParams.get("period")
            def level=requestParams.get("level")
            Object memberList = getMemberListForPeriodAndLevel(loaMap, period, level, MEMBER_LIST)
            return memberList
        }
        else if(drillType.equalsIgnoreCase("flow")){
            String[] source=requestParams.get("source").split("_")
            String[] target=requestParams.get("target").split("_")

            List<String> memberListSource=getMemberListForPeriodAndLevel(loaMap,source[0],source[1],MEMBER_LIST)
            List<String> memberListTarget=getMemberListForPeriodAndLevel(loaMap,target[0],target[1],MEMBER_LIST)

            memberListTarget.retainAll(memberListSource)
            return memberListTarget;
        }
        return null
    }
}
