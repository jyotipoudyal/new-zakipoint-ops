package zakipointCore.features.biometric.dashboard;

import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.features.biometric.BiometricDomainOperation;

import java.util.*;

/**
 * Created by sktamang on 12/7/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class BiometricSnapshotBuilder implements SummaryQueryBuilder {

    
    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        Map<String,String> biometricMap= BiometricDomainOperation.getBiometrics(state.request.requestParameters);
        state.hits.put("biometricInfoMap",biometricMap);

        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        builder.add(biometricBuilder(state,client,biometricMap));
        return  builder;
    }

    protected SearchRequestBuilder biometricBuilder(RequestState state, Client client,Map<String,String> biometricMap) {
        SearchRequestBuilder builder=QueryUtils.buildEndSearchWihType(client,state.request,"Z5Biometric");

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.rangeFilter("measuredDate").gte(state.periodFrom()).lt(state.periodTo()));
        LevelUtils.setLOAFilter(state,andFilterBuilder);
        if(state.request.hasParam("metricCode")){
            String metricCode=state.request.get("metricCode");
            andFilterBuilder.add(FilterBuilders.termFilter("biometricId",metricCode));
        }
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));
        builder.addAggregation(AggregationBuilders.terms("biometricId").field("biometricId").size(0)
                .subAggregation(AggregationBuilders.terms("biometricName").field("biometricName").size(1)));

        implementAggregation(builder,state,biometricMap);
        return builder;
    }

    protected void implementAggregation(SearchRequestBuilder builder, RequestState state,Map<String,String> biometricMap) {

        FiltersAggregationBuilder filters= AggregationBuilders.filters("filters");

        for (String keys : biometricMap.keySet()) {
            String biometricId=keys.split("__")[0];
            String levelRangeName=keys.split("__")[1];
            String normalRangeBegin=biometricMap.get(keys).split("__")[0];
            String normalRangeEnd=biometricMap.get(keys).split("__")[1];

            filters.filter(biometricId+"__"+levelRangeName,getRangeFilter("biometricValue",true,true,normalRangeBegin,normalRangeEnd,biometricId));
        }

        filters.subAggregation(AggregationBuilders.terms("memberId").field("unblindMemberId").size(0));
        builder.addAggregation(filters);
    }

    protected AndFilterBuilder getRangeFilter(String field,boolean includeLower,boolean includeUpper,String lowerLimit, String upperLimit,String biometricName){
        if(lowerLimit.equalsIgnoreCase("null") )lowerLimit=null;
        else if( upperLimit.equalsIgnoreCase("null"))upperLimit=null;

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.termFilter("biometricId",biometricName));
        return    andFilterBuilder.add(FilterBuilders.rangeFilter(field).gte(lowerLimit).lte(upperLimit));
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
