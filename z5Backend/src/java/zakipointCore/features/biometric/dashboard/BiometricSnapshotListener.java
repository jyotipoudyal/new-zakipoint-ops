package zakipointCore.features.biometric.dashboard;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 12/7/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class BiometricSnapshotListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        Map<String,String> biometricIdToNameMap=new HashMap<>();

        Terms biometricIdTerms=response.getAggregations().get("biometricId");
        for (Terms.Bucket bucket : biometricIdTerms.getBuckets()) {
            String id=bucket.getKey();
            Terms biometricName=bucket.getAggregations().get("biometricName");
            for (Terms.Bucket names : biometricName.getBuckets()) {
                String name=names.getKey();
                biometricIdToNameMap.put(id,name);
            }
        }

        Filters filters=response.getAggregations().get("filters");
        Map<String,String> biometricInfoMap= (Map<String, String>) state.hits.get("biometricInfoMap");

        for (Filters.Bucket bucket : filters.getBuckets()) {
              String[] range=biometricInfoMap.get(bucket.getKey()).split("__");
              String rangeBegin=range[0];
              String rangeEnd=range[1];
              String biometricId=bucket.getKey().split("__")[0];
              String rangeName=bucket.getKey().split("__")[1];

            Record record=getRecord(state,biometricId);
            List levelList=getList(record.recordMap);
            levelList.add(rangeName);
            Terms memberId=bucket.getAggregations().get("memberId");

            record.recordMap.put(rangeName,memberId.getBuckets().size());
            record.recordMap.put("beginRange_"+rangeName, rangeBegin);
            record.recordMap.put("endRange_"+rangeName, rangeEnd);
            record.recordMap.put("description", biometricIdToNameMap.get(biometricId));
            record.recordMap.put("levels", levelList);
        }
    }

    private List<String> getList(Map<String, Object> recordMap) {
        if(recordMap.containsKey("levels")) return (List<String>) recordMap.get("levels");
        else return new ArrayList<>();

    }
}
