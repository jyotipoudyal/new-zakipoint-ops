package zakipointCore.features.biometric;

/**
 * Created by sktamang on 12/7/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public enum BiometricsEnum {

//    WHEN 'Biometric Score' THEN 'Biometric Score'
//    WHEN 'Wellness Score' THEN 'Wellness Score'
//    WHEN 'BP Diastolic (mmHg)' THEN 'BP Diastolic'
//    WHEN 'BP Systolic (mmHg)' THEN 'BP Systolic'
//    WHEN 'Total Cholesterol (mg/dL)' THEN 'Total Cholesterol'
//    WHEN 'Fasting Blood Sugar (mg/dL)' THEN 'Fasting Blood Sugar'
//    WHEN 'HDL Cholesterol(mg/dL)' THEN 'HDL Cholesterol'
//    WHEN 'Cholesterol Ratio' THEN 'Cholesterol Ratio'
//    WHEN 'Triglycerides (mg/dL)' THEN 'Triglycerides'
//    WHEN 'LDL Cholesterol(mg/dL)' THEN 'LDL Cholesterol'
//    WHEN 'Waist (in)' THEN 'Waist'
//    WHEN 'PSA (ng/dL)' THEN 'PSA'
//    WHEN 'Nonfasting Blood Sugar' THEN 'Nonfasting Blood Sugar'
    //code,name,beginRange, endRange
    bm1("A1C", "A1C (%)", "4", "5.6"),
//    bm2("BMI100", "BMI", "18.5", "24"),
    bm3("CHOLESTEROL_RATIO", "Cholesterol Ratio", "1", "3.4"),
    bm4("WAIST", "Waist", "25", "36"),
    bm5("BP_DIASTOLIC", "BP Diastolic", "25", "80"),
    bm6("BP_SYSTOLIC", "BP Systolic", "75", "120"),
    bm7("BIOMETRIC_SCORE", "Biometric Score", "75", "100"),
    bm8("FASTING_BLOOD_SUGAR", "Fasting Blood Sugar", "50", "100"),
    bm9("HDL_CHOLESTEROL", "HDL Cholesterol", "60", "100"),
    bm10("LDL_CHOLESTEROL", "LDL Cholesterol", "50", "100"),
    bm11("NON_FASTING_BLOOD_SUGAR", "Nonfasting Blood Sugar", "50", "140"),
    bm12("PSA", "PSA", "0", "4"),
    bm13("TOTAL_CHOLESTEROL", "Total Cholesterol", "25", "200"),
    bm14("TRIGLYCERIDE", "Triglycerides", "25", "150"),
    bm15("WELLNESS_SCORE", "Wellness Score", "75", "100"),
    bm16("WEIGHT", "Weight", "5", "180"),
    bm17("BODY_FAT", "Body Fat Percent", "0", "18");


    public String getBiometricCode() {
        return biometricCode;
    }

    public String getBiometricName() {
        return biometricName;
    }

    public String getBeginRange() {
        return beginRange;
    }

    public String getEndRange() {
        return endRange;
    }

    public String biometricCode;
    public String biometricName;
    public String beginRange;
    public String endRange;

    BiometricsEnum(String biometricCode, String biometricName, String beginRange, String endRange) {
        this.biometricCode = biometricCode;
        this.biometricName = biometricName;
        this.beginRange = beginRange;
        this.endRange = endRange;
    }


    public static BiometricsEnum getBiometricsByCode(String code) {

        for (BiometricsEnum biometrics : BiometricsEnum.values()) {
            if (biometrics.biometricCode.equalsIgnoreCase(code)) {
                return biometrics;
            }
        }
        throw new IllegalArgumentException("No such metric code exists.");
    }
}
