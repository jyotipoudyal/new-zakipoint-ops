package zakipointCore.features.familySpouse;

import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 12/23/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class HighFamilySpouseCostBuilder implements SummaryQueryBuilder {
    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        return null;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
