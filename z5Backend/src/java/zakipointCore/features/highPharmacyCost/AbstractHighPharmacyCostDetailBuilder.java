package zakipointCore.features.highPharmacyCost;

import com.configurations.utils.*;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.sum.SumBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

import java.util.*;

/**
 * Created by sktamang on 5/4/16.
 */
public abstract class AbstractHighPharmacyCostDetailBuilder implements SummaryQueryBuilder {
    public static String GENERIC_BRAND_SAVINGS = "genericBrandSavings";
    public static String MAIL_RETAIL_SAVINGS = "mailRetailSavings";
    public static String UDF_FIELD = "udf30Id";
    public static int DEFAULT_PAGE = 1;
    public static int DEFAULT_PAGE_SIZE = 5;
    public static List<String> LOA_MAP = new ArrayList<String>() {{add("brandName");}};
    

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder multiSearchRequestBuilder = QueryUtils.buildMultiSearch(client);
        double memberMonths=0.0d;
        state.hits.put("memberMonths", CommonQueries.getTotalMemberMonth(state, client, memberMonths));

        multiSearchRequestBuilder.add(savingsGenerator(state, client));
        return multiSearchRequestBuilder;
    }


    private SearchRequestBuilder savingsGenerator(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, Pharmacy.NAME);
        String[] drugList = getDrugList(state, client);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder=applyServiceAndPaidDateFilter(state,andFilterBuilder);
        LevelUtils.setLOAFilter(state,andFilterBuilder);
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        reportSpecificFilterImplementation(state,andFilterBuilder);
        if(state.request.hasParam("brandName") && state.request.get("brandName")!=null) andFilterBuilder.add(FilterBuilders.termsFilter("brandName", state.request.get("brandName")));
        else if(drugList.length != 0) andFilterBuilder.add(FilterBuilders.termsFilter("brandName", drugList));
//        brandNameFilter(andFilterBuilder, state);
        return aggregationImplementations(builder,andFilterBuilder);
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }

    protected abstract SearchRequestBuilder aggregationImplementations(SearchRequestBuilder builder,AndFilterBuilder andFilterBuilder);

    protected String[] getDrugList(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, Pharmacy.NAME);
        String orderField = state.request.requestParameters.get("order").split(":")[0];
        String orderString = state.request.requestParameters.get("order").split(":")[1];
        orderField = switchOrder(orderField);
        boolean order = true;

        if (orderString.equalsIgnoreCase("Desc")) order = false;

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder=applyServiceAndPaidDateFilter(state,andFilterBuilder);
        LevelUtils.setLOAFilter(state, andFilterBuilder);
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        reportSpecificFilterImplementation(state,andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders
                .matchAllQuery(), andFilterBuilder));
        drugListAggregationImplementation(builder, orderField, order);
        return executeDrugBuilder(builder, state);
    }

    protected abstract void reportSpecificFilterImplementation(RequestState state, AndFilterBuilder andFilterBuilder);


    protected abstract void drugListAggregationImplementation(SearchRequestBuilder builder, String orderField, boolean order);

    public AndFilterBuilder applyServiceAndPaidDateFilter(RequestState state,AndFilterBuilder andFilterBuilder){
        FilterUtils.serviceDateFilter(state, andFilterBuilder);
        FilterUtils.addRangeFilter(state.periodTo(), state.periodFrom(), Pharmacy.PAID_DATE, andFilterBuilder);
        return andFilterBuilder;
    }

    public SumBuilder sumBuilder(String aggName, String fieldName) {
        return AggregationBuilders.sum(aggName).field(fieldName);
    }

    public FilterAggregationBuilder filterAggregationForEachDrug(){
        return AggregationBuilders.filter("rangeForNonReversal").filter(FilterBuilders.rangeFilter(Pharmacy.PHARMACY_SCRIPT_FOR_UM).gte(null).lte(null))
                .subAggregation(sumBuilder("annualCost", Pharmacy.PAID_AMOUNT))
                .subAggregation(sumBuilder("prescriptions", Pharmacy.PHARMACY_SCRIPT_FOR_UM))
                .subAggregation(sumBuilder("paidAmountPmpm", Pharmacy.PAID_AMOUNT))
                .subAggregation(AggregationBuilders.terms("memberVolume").field("memberId").size(0));
    }


    private String[] executeDrugBuilder(SearchRequestBuilder builder, RequestState state) {
        Double memberMonths = (Double) state.hits.get("memberMonths");
        SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, state.report);
        int page = (state.request.hasParam(RequestParameters.PAGE)) ? Integer.parseInt(state.request.get(RequestParameters.PAGE)) : DEFAULT_PAGE;
        int pageSize = (state.request.hasParam(RequestParameters.PAGE_SIZE)) ? Integer.parseInt(state.request.get(RequestParameters.PAGE_SIZE)) : DEFAULT_PAGE_SIZE;
        Terms drugs = null;
        Map<String, Double> totalMap = new HashMap<String, Double>();

        double totalAnualCost = 0.0d;
        double totalPopulationSize = 0.0d;
        double totalPrescriptions = 0.0d;
        double totalPaidAmountPmpm = 0.0d;
        for (Aggregation agg : response.getAggregations()) {
            if (agg.getName().contains("drugs")) {
                drugs = response.getAggregations().get("drugs");
                for (Terms.Bucket drugKeys : drugs.getBuckets()) {
                    for (Aggregation innerAgg : drugKeys.getAggregations()) {
                        if (innerAgg instanceof Filter) {

                            Filter filter = (Filter) innerAgg;
                            totalAnualCost = totalAnualCost + AmountUtils.getAmount(((Sum) filter.getAggregations().get("annualCost")).getValue());
                            totalPaidAmountPmpm = totalPaidAmountPmpm + AmountUtils.getAmount(((Sum) filter.getAggregations().get("paidAmountPmpm")).getValue());
                            totalPrescriptions = totalPrescriptions + ((Sum) filter.getAggregations().get("prescriptions")).getValue();
                            Terms memberBuckets = filter.getAggregations().get("memberVolume");
                            totalPopulationSize = totalPopulationSize + memberBuckets.getBuckets().size();
                        }
                    }
                }
            }
            else totalMap.put(agg.getName(),(((Sum) agg).getValue()));
        }

        totalMap.put("totalAnnualCost", totalAnualCost);
        totalMap.put("totalPopulationSize", totalPopulationSize);
        totalMap.put("totalPrescriptions", totalPrescriptions);
        totalMap.put("totalPaidAmountPmpm", totalPaidAmountPmpm / memberMonths);

        String[] stringTerms = new String[drugs.getBuckets().size()];
        Iterator<Terms.Bucket> iterator = drugs.getBuckets().iterator();
        int index = 0;
        while (iterator.hasNext()) {
            stringTerms[index] = iterator.next().getKey();
            iterator.remove();
            index++;
            if (index > stringTerms.length) {
                break;
            }
        }
        state.hits.put("totalMap", totalMap);
        state.hits.put("totalRecords", stringTerms.length);

        Pagination<String> pagination = new Pagination<String>(stringTerms);
        String[] drugList = pagination.paginate(page - 1, pageSize);
        state.hits.put("drugList", drugList);
        return drugList;
    }

    public String switchOrder(String orderField) {
        switch (orderField) {
            case "annualCost":
                orderField = "rangeForNonReversal>annualCost";
                break;
            case "populationSize":
                orderField = "rangeForNonReversal";
                break;
            case "prescriptions":
                orderField = "rangeForNonReversal>prescriptions";
                break;
            case "paidAmountPmpm":
                orderField = "rangeForNonReversal>paidAmountPmpm";
                break;
            default:
                break;
        }
        return orderField;
    }
}
