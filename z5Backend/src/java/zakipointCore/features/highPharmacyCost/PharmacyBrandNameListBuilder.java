package zakipointCore.features.highPharmacyCost;

import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 11/10/15.
 */
public class PharmacyBrandNameListBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        SearchRequestBuilder searchRequestBuilder = QueryUtils.buildEndSearch(client, state.request);
        searchRequestBuilder.setSize(0).setTypes("PharmacySaving");
        searchRequestBuilder.addAggregation(AggregationBuilders.terms("brands").field("brandName").size(0));
        builder.add(searchRequestBuilder);
        return builder;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
