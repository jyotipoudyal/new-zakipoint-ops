package zakipointCore.features.highPharmacyCost.rxRetailMailCostDrivers;

import org.elasticsearch.action.search.SearchResponse;
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostListener;

/**
 * Created by sktamang on 4/20/16.
 */
public class RxRetailMailCostListener extends RxBrandGenericCostListener {
    @Override
    protected void handleResponse(SearchResponse response) {
        super.handleResponse(response);
    }
}
