package zakipointCore.features.highPharmacyCost.rxRetailMailCostDrivers;

import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostBuilder;

/**
 * Created by sktamang on 4/20/16.
 */
public class RxRetailMailCostBuilder extends RxBrandGenericCostBuilder {

    @Override
    protected void implementFilterForSavingsScope(AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.rangeFilter("mailRetailSavings").gt(0).lte(null));
    }
}
