package zakipointCore.features.highPharmacyCost;

import com.configurations.XContentWriter;
import com.configurations.utils.AmountUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Map;

/**
 * Created by sktamang on 5/4/16.
 */
public abstract class AbstractHighPharmacyCostDetailListener extends AbstractSummaryListener {
    protected void handleResponse(SearchResponse response) {
        for (Aggregation aggregation : response.getAggregations()) {
            handleOtherMetrics(aggregation);
        }
    }

    private void handleOtherMetrics(Aggregation aggregation) {
        Double memberMonths = (Double) state.hits.get("memberMonths");
        Terms metrics = (Terms) aggregation;
        for (Terms.Bucket drugKeys : metrics.getBuckets()) {
            Record wrapper = getRecord(state, drugKeys.getKey());
            wrapper.recordMap.put("drugName", drugKeys.getKey());
            for (Aggregation innerAgg : drugKeys.getAggregations()) {
                if (innerAgg instanceof Filter) handleFilter(memberMonths, wrapper, (Filter) innerAgg);
                else if (innerAgg instanceof Sum) { //this part generates the keys genericBrandSavings, mailRetailSavings
                    wrapper.recordMap.put(innerAgg.getName(),(((Sum) innerAgg).getValue()));
                }
                else if(innerAgg.getName().equalsIgnoreCase("genericOrBrand"))equivaletGenericNameFinder(wrapper, (Terms) innerAgg);
                else reportSpecificAggregationExtractor(wrapper,(Terms)innerAgg);
            }
        }
    }

    //this function generates the key genericOrBrand
    private void equivaletGenericNameFinder(Record wrapper, Terms innerAgg) {
        Terms equivalentGenericTerms = innerAgg;
        if (equivalentGenericTerms.getBuckets().size() != 0) {
            String genericOrBrand = equivalentGenericTerms.getBuckets().get(0).getKey();
            if (genericOrBrand.equalsIgnoreCase("Y")) genericOrBrand = "generic";
            else genericOrBrand = "brand";
            wrapper.recordMap.put("genericOrBrand", genericOrBrand);
        } else wrapper.recordMap.put("genericOrBrand", "");
    }


    /* this function generates the following keys
     equvalentGenericDrug ,populationSize, annualCost, prescriptions, paidAmountPmpm
     */
    private void handleFilter(Double memberMonths, Record wrapper, Filter innerAgg) {
        for (Aggregation aggregation1 : innerAgg.getAggregations()) {
            if (aggregation1 instanceof Sum) {
                Sum sum = (Sum) aggregation1;
                if (sum.getName().equalsIgnoreCase("annualCost"))
                    wrapper.recordMap.put(aggregation1.getName(), AmountUtils.getAmount(sum.getValue()));
                else if (sum.getName().contains("Pmpm"))
                    wrapper.recordMap.put(aggregation1.getName(), ((AmountUtils.getAmount(sum.getValue()))) / memberMonths);
                else wrapper.recordMap.put(aggregation1.getName(), sum.getValue());
            }
            else if (aggregation1.getName().equalsIgnoreCase("memberVolume"))
                wrapper.recordMap.put("populationSize", ((Terms) aggregation1).getBuckets().size());
            else if (aggregation1.getName().equalsIgnoreCase("equvalentGeneric")) {
                reportSpecificAggregationExtractor(wrapper, (Terms) aggregation1);
            }
        }
    }

    protected void reportSpecificAggregationExtractor(Record wrapper, Terms aggregation1) {
            Terms equivalentGenericTerms = aggregation1;
            if (equivalentGenericTerms.getBuckets().size() != 0) wrapper.recordMap.put("equvalentGenericDrug", equivalentGenericTerms.getBuckets().get(0).getKey());
            else wrapper.recordMap.put("equvalentGenericDrug", "");
    }

    @Override
    protected void writeContent(Map<String, Record> memberIds, XContentBuilder contentBuilder, String period) throws Exception {
        contentBuilder.startObject(period);
        XContentWriter.Params param = new XContentWriter.Params();
        Map<String, Double> totalMap = (Map<String, Double>) state.hits.get("totalMap");
        Record totalWrapper = getRecord(state, "total");
        for (Map.Entry<String, Double> pair : totalMap.entrySet()) {
            totalWrapper.recordMap.put(pair.getKey(), pair.getValue());
        }
        contentBuilder.field("totalHits", state.hits.get("totalRecords"));
        for (Record record : memberIds.values()) {
            Record wrapper =record;
            wrapper.toXContent(contentBuilder, param);
        }
        contentBuilder.endObject();
    }
}
