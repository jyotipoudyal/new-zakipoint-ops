package zakipointCore.features.highPharmacyCost.rxHistoricAndPredictedPharmacyCost;

import com.configurations.XContentWriter;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.dashBoard.PharmacyClaimsPmPmListener;

import java.io.IOException;
import java.util.Map;

/**
 * Created by sktamang on 4/20/16.
 */
public class RxHistoricPredictiveListener extends PharmacyClaimsPmPmListener {


    @Override
    protected void handleResponse(SearchResponse response) {
        if (!response.getAggregations().asMap().containsKey("prospectivePharmacyRaw")) {
            super.handleResponse(response);
        } else extractRiskScore(response);
    }


    private void extractRiskScore(SearchResponse response) {
        Avg riskScorePharmacy = response.getAggregations().get("prospectivePharmacyNormalized");
        Avg prospectivePharmacyRaw = response.getAggregations().get("prospectivePharmacyRaw");

        state.hits.put("prospectivePharmacyRaw", Double.isNaN(prospectivePharmacyRaw.getValue()) ? 0 : prospectivePharmacyRaw.getValue());
        state.hits.put("prospectivePharmacyNormalized", Double.isNaN(riskScorePharmacy.getValue()) ? 0 : riskScorePharmacy.getValue());
    }


    @Override
    protected Wrapper getRecord(RequestState state, String id) {
        if (!state.recordIds.containsKey(id)) {
            Wrapper wrapper = new Wrapper(id);
            state.recordIds.put(id, wrapper);
            return wrapper;
        }
        return (Wrapper) state.recordIds.get(id);
    }


    public class Wrapper extends Record implements XContentWriter {

        public Wrapper(String id) {
            super(id);
        }

        @Override
        public void toXContent(XContentBuilder builder, Params params) throws IOException {
            builder.startObject(id);

            double totalPharmacyPaidAmount = 0.0d;
            double totalMemberMonths = 0.0d;

            for (Map.Entry<String, Object> pair : recordMap.entrySet()) {
                builder.field(pair.getKey(), pair.getValue());

                if (id.equalsIgnoreCase("pharmacyClaims"))
                    totalPharmacyPaidAmount = totalPharmacyPaidAmount + (double) pair.getValue();

                else if (!id.equalsIgnoreCase("riskScore"))
                    totalMemberMonths = totalMemberMonths + ((Long) (pair.getValue())).doubleValue();
            }

            if (id.equalsIgnoreCase("pharmacyClaims")) state.hits.put("totalPaidAmount", totalPharmacyPaidAmount);
            else if (!id.equalsIgnoreCase("riskScore")) state.hits.put("totalMemberMonths", totalMemberMonths);
            builder.endObject();
        }
    }


    @Override
    protected void writeContent(Map<String, Record> memberIds, XContentBuilder contentBuilder, String period) throws Exception {

        contentBuilder.startObject(period);

        for (Record record : memberIds.values()) {
            Wrapper wrapper = (Wrapper) record;
            wrapper.toXContent(contentBuilder, XContentWriter.EMPTY_PARAMS);
        }

        for (Map.Entry<String, Object> pair : state.hits.entrySet()) {
            contentBuilder.field(pair.getKey(), pair.getValue());
        }

        double riskFactor = CalculatorUtils.divide((double) state.hits.get("prospectivePharmacyRaw"), (double) state.hits.get("prospectivePharmacyNormalized"));
        double PharmacyPaidAmountPmpm = CalculatorUtils.divide((double) state.hits.get("totalPaidAmount"), (double) state.hits.get("totalMemberMonths"));
        double pharmacyPaidAmountPredicted = PharmacyPaidAmountPmpm * riskFactor;

        contentBuilder.field("pharmacyPaidAmountPmpm", PharmacyPaidAmountPmpm);
        contentBuilder.field("pharmacyPaidAmountPmpmPredicted", pharmacyPaidAmountPredicted);
        contentBuilder.field("percentOfPharmacyPaidAmountPmpmLikelyToIncrease",
                CalculatorUtils.calculatePercentage(pharmacyPaidAmountPredicted - PharmacyPaidAmountPmpm, PharmacyPaidAmountPmpm));
        contentBuilder.endObject();
    }
}
