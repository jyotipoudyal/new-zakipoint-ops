package zakipointCore.features.highPharmacyCost.rxHistoricAndPredictedPharmacyCost;

import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 5/2/16.
 */
public class RxClaimsLikelyToIncreaseBuilder extends RxHistoricPredictiveBuilder{
    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder multiSearchRequestBuilder = QueryUtils.buildMultiSearch(client);
        multiSearchRequestBuilder.add(memberMonthsBuilder(state, client));
        multiSearchRequestBuilder.add(medicalOrPharmacy(state, client));
        return multiSearchRequestBuilder.add(riskQueryBuilder(state,client));
    }


    public SearchRequestBuilder riskQueryBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder=QueryUtils.buildEndSearchWihType(client,state.request, MemberSearch.NAME);
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        groupFilter(state, andFilterBuilder);
        LevelUtils.setLevelFilter(state, andFilterBuilder, state.periodTo());

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        builder.addAggregation(AggregationBuilders.avg("riskScoreEr").field("erScore"));
        String normlizedField=state.request.hasParam("group") ? "prospectiveTotalNormalizedToGroup" :"prospectiveTotalNormalizedToBob";
        builder.addAggregation(AggregationBuilders.avg("riskScoreAll").field(normlizedField));
        return builder;
    }

    public static AndFilterBuilder groupFilter(RequestState state,AndFilterBuilder andFilterBuilder) {
        AndFilterBuilder andFilter=new AndFilterBuilder();
        String reportingTo = state.request.hasParameter(state.period() + "To") ? state.request.get(state.period() + "To") : "2012-12-31";
        andFilter.add(FilterBuilders.rangeFilter("group.fromDate").from(null).to(reportingTo).includeLower(true).includeUpper(true));
        andFilter.add(FilterBuilders.rangeFilter("group.toDate").from(reportingTo).to(null).includeLower(true).includeUpper(true));
        andFilter.add(FilterBuilders.termsFilter("group.eligibilityType","medical"));
        return andFilterBuilder.add(FilterBuilders.nestedFilter("group",andFilter));
    }
}
