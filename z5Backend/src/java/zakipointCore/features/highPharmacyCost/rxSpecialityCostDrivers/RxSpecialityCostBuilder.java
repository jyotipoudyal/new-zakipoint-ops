package zakipointCore.features.highPharmacyCost.rxSpecialityCostDrivers;

import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostBuilder;

/**
 * Created by sktamang on 4/22/16.
 */
public class RxSpecialityCostBuilder extends RxBrandGenericCostBuilder {

    @Override
    protected void implementFilterForSavingsScope(AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.termFilter("udf5_pharmacy", "S"));
    }
}
