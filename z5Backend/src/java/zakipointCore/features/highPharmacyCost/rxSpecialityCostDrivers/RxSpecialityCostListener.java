package zakipointCore.features.highPharmacyCost.rxSpecialityCostDrivers;

import org.elasticsearch.action.search.SearchResponse;
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostListener;

/**
 * Created by sktamang on 4/22/16.
 */
public class RxSpecialityCostListener extends RxBrandGenericCostListener {
    @Override
    protected void handleResponse(SearchResponse response) {
        super.handleResponse(response);
    }
}
