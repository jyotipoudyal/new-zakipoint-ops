package zakipointCore.features.highPharmacyCost;

import com.configurations.Wrapper;
import com.configurations.XContentWriter;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Map;

/**
 * Created by sktamang on 11/10/15.
 */
public class PharmacyBrandNameListListener extends AbstractSummaryListener {

    @Override
    public void handleResponse(SearchResponse response) {
     Wrapper wrapper=getWrapper("drugs");
        int i=0;
        for (Aggregation aggregation : response.getAggregations()) {
            Terms drugBuckets=(Terms)aggregation;
            for (Terms.Bucket drug : drugBuckets.getBuckets()) {
                wrapper.recordMap.put(String.valueOf(i),drug.getKey());
                i++;
            }
        }
    }

    public Wrapper getWrapper(String key) {
        if (!state.recordIds.containsKey(key)) {
            Wrapper wrapper = new Wrapper(key);
            state.recordIds.put(key, wrapper);
        }
        return (Wrapper) state.recordIds.get(key);
    }

    @Override
    protected void writeContent(Map<String, Record> memberIds, XContentBuilder contentBuilder, String period) throws Exception {
        contentBuilder.startObject(period);
        for (Record record : memberIds.values()) {
            Wrapper wrapper = (Wrapper) record;
            wrapper.toXContent(contentBuilder, XContentWriter.EMPTY_PARAMS);
        }
        contentBuilder.endObject();
    }
}
