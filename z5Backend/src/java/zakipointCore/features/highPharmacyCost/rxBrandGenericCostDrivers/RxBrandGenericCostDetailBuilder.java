package zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers;

import com.configurations.utils.Pharmacy;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.features.highPharmacyCost.AbstractHighPharmacyCostDetailBuilder;

/**
 * Created by sktamang on 5/4/16.
 */
public class RxBrandGenericCostDetailBuilder extends AbstractHighPharmacyCostDetailBuilder {

    @Override
    protected void reportSpecificFilterImplementation(RequestState state, AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.termFilter("genericFlag", "N"));
    }

    @Override
    protected void drugListAggregationImplementation(SearchRequestBuilder builder, String orderField, boolean order) {
        builder.addAggregation(AggregationBuilders.terms("drugs").field(Pharmacy.BRAND_NAME).size(0).order(Terms.Order.aggregation(orderField, order))
                .subAggregation(sumBuilder("savings", GENERIC_BRAND_SAVINGS))
                .subAggregation(filterAggregationForEachDrug()))
                .addAggregation(sumBuilder("totalSavings", GENERIC_BRAND_SAVINGS));
    }

    @Override
    protected SearchRequestBuilder aggregationImplementations(SearchRequestBuilder builder, AndFilterBuilder andFilterBuilder) {
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        builder.addAggregation(AggregationBuilders.terms("drugs").field(Pharmacy.BRAND_NAME).size(0)
                        .subAggregation(sumBuilder("savings", GENERIC_BRAND_SAVINGS))
                        .subAggregation(filterAggregationForEachDrug())
                        .subAggregation(AggregationBuilders.terms("equvalentGeneric").field(UDF_FIELD).size(1))
                        .subAggregation(AggregationBuilders.terms("genericOrBrand").field(Pharmacy.GENERIC_FLAG).size(1))
        );
        return builder;
    }
}
