package zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers;

import com.configurations.utils.AmountUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 4/20/16.
 */
public class RxBrandGenericCostListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        double totalPaidAmount;
        double expensePaidAmount;

        Filter paidAmountForBrand = response.getAggregations().get("savingsScope");
        Sum paidAmount = response.getAggregations().get("paidAmount");
        expensePaidAmount = AmountUtils.getAmount(((Sum) paidAmountForBrand.getAggregations().get("paidAmount")).getValue());
        totalPaidAmount = AmountUtils.getAmount(paidAmount.getValue());

        Record record=getRecord(state,state.periodFrom()+"_to_"+state.periodTo());
        record.recordMap.put("paidAmtForSavingsScope",expensePaidAmount);
        record.recordMap.put("paidAmtForTotal",totalPaidAmount);
        record.recordMap.put("percentOfPaidAmtForSavingsScope", CalculatorUtils.calculatePercentage(expensePaidAmount,totalPaidAmount));
    }
}
