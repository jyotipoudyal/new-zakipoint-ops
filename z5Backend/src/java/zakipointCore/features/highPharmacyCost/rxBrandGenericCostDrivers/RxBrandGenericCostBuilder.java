package zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers;

import com.configurations.utils.Pharmacy;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.sum.SumBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 4/20/16.
 */
public class RxBrandGenericCostBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder searchRequestBuilder = QueryUtils.buildMultiSearch(client);
        searchRequestBuilder.add(query(state, client));
        return searchRequestBuilder;
    }
    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, Pharmacy.NAME);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder = FilterUtils.applyServiceAndPaidDateFilter(state, andFilterBuilder);

        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        LevelUtils.setLOAFilter(state, andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        applyAggregations(builder);
        return builder;
    }

    protected void applyAggregations(SearchRequestBuilder builder) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        implementFilterForSavingsScope(andFilterBuilder);

        builder.addAggregation(getSum())
                .addAggregation(AggregationBuilders.filter("savingsScope").filter(andFilterBuilder)
                        .subAggregation(getSum()));
    }

    protected void implementFilterForSavingsScope(AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.termFilter(Pharmacy.GENERIC_FLAG, "N"))
                .add(FilterBuilders.existsFilter("udf30Id"));
    }

    protected SumBuilder getSum() {
        return AggregationBuilders.sum("paidAmount").field(Pharmacy.PAID_AMOUNT);
    }
}
