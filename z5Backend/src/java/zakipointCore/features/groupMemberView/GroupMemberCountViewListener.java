package zakipointCore.features.groupMemberView;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Map;

/**
 * Created by sktamang on 3/2/16.
 */
public class GroupMemberCountViewListener extends AbstractSummaryListener {
    @Override
    protected void handleResponse(SearchResponse response) {
        Record record = getRecord(state, "groupDetail");
        if (state.request.hasParam("group")) {
            for (SearchHit searchHitFields : response.getHits().getHits()) {
                for (Map.Entry<String, SearchHitField> stringMap : searchHitFields.fields().entrySet()) {
                    record.recordMap.put(stringMap.getKey(), stringMap.getValue().getValue());
                }
            }
        } else {
            record.recordMap.put("groupId", "zph");
            record.recordMap.put("groupName", "Zph");
        }

        for (Aggregation aggregation : response.getAggregations()) {
            Filters filters = (Filters) aggregation;
            for (Filters.Bucket bucket : filters.getBuckets()) {
                record.recordMap.put(bucket.getKey(), bucket.getDocCount());
            }
        }
    }
}
