package zakipointCore.features.groupMemberView;

import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.NestedFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.*;

/**
 * Created by sktamang on 3/2/16.
 */
public class GroupMemberCountViewBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        builder.add(query(state,client));
        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        SearchRequestBuilder builder=QueryUtils.buildEndSearchWihType(client,state.request, MemberSearch.NAME);
        builder.setSize(1).addFields(MemberSearch.FIELD_GROUP_NAME,MemberSearch.FIELD_GROUP_ID);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        LevelUtils.setLevelFilter(state,andFilterBuilder,state.periodTo());

        if(state.request.hasParam("group")) {
            String[] groupId = RequestUtils.getArrayRequest(state.request.get("group"));
            NestedFilterBuilder nestedFilterForGroup = FilterBuilders.nestedFilter("group", FilterBuilders.termsFilter("group.id", groupId));
            andFilterBuilder.add(nestedFilterForGroup);
        }
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        applyAggregations(state, builder);
        return builder;
    }


    public void applyAggregations(RequestState state,SearchRequestBuilder builder){
        FiltersAggregationBuilder filters=AggregationBuilders.filters("filters");
        filters.filter("employeeCount", FilterUtils.getRelationShipFilter(state,"SB"));

        AndFilterBuilder eligibilityFilter=new AndFilterBuilder();
        eligibilityFilter.add(FilterUtils.eligibleDateFilter(state.periodTo()));
        filters.filter("memberCount",eligibilityFilter);

        builder.addAggregation(filters);
    }
}
