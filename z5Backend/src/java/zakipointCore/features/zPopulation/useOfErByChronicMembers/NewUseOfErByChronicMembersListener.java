package zakipointCore.features.zPopulation.useOfErByChronicMembers;

import com.configurations.utils.AmountUtils;
import com.configurations.utils.ChronicConditionEnum;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 5/23/16.
 */
public class NewUseOfErByChronicMembersListener extends AbstractSummaryListener {

    List<String> PERCENTAGE_CALCULATION_KEYS = Arrays.asList("oneTimeVisitor", "nonErVisitor", "twoOrMoreTimesVisitor");


    @Override
    protected void handleResponse(SearchResponse response) {

        int metricCode = Integer.parseInt(state.request.get("chronicCode"));
        String display = ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(metricCode).metricDisplay;

        for (Aggregation aggregation : response.getAggregations()) {
            Filters filters = (Filters) aggregation;
            for (Filters.Bucket chronicConditionsBucket : filters.getBuckets()) {
                Record record = getRecord(state, "chronicConditions");

                Map<String, Object> conditionWiseTotalMap = new HashMap<>();
                Terms allMembers = chronicConditionsBucket.getAggregations().get("allMembers");
                extractErVisitors(chronicConditionsBucket, record, conditionWiseTotalMap, allMembers);

                conditionWiseTotalMap.put("chronicCode", metricCode);
                conditionWiseTotalMap.put("chronicDesc", display);
                conditionWiseTotalMap.put("member", (double) allMembers.getBuckets().size());

                state.hits.put("allMembers", allMembers.getBuckets().size());
                record.recordMap.put("total", conditionWiseTotalMap);
            }
        }
    }


    private void extractErVisitors(Filters.Bucket chronicConditionsBucket, Record record, Map<String, Object> conditionWiseTotalMap, Terms allMembers) {

        Filter erVisitors = chronicConditionsBucket.getAggregations().get("erVisitors");
        Terms conditionWiseErVisitor = erVisitors.getAggregations().get("conditionWise");

        Nested nestedVisitAmounts = erVisitors.getAggregations().get("nestedVisitAmounts");
        extractDiagWiseErVisitCount(record, erVisitors, (double) conditionWiseErVisitor.getBuckets().size());
        conditionWiseTotalMap.put("erCost", calculateTotalVisitAmounts(nestedVisitAmounts));

        calculateErVisitCount(conditionWiseErVisitor, conditionWiseTotalMap, "conditionWiseErVisitors");
        conditionWiseTotalMap.put("erVisitor", (double) conditionWiseErVisitor.getBuckets().size());
        conditionWiseTotalMap.put("nonErVisitor", (double) (allMembers.getBuckets().size() - conditionWiseErVisitor.getBuckets().size()));
    }


    private void extractDiagWiseErVisitCount(Record record, Filter totalErVisitor, double totalErVisitors) {

        Terms diagnosisGroups = totalErVisitor.getAggregations().get("diagGrouperWise");

        for (Terms.Bucket bucket : diagnosisGroups.getBuckets()) {
            Map<String, Object> diagGroupWiseRecordMap = new HashMap<>();
            Terms erVisitMemberCount = bucket.getAggregations().get("membersCount");

            double daigGroupWiseErVisitor = (double) erVisitMemberCount.getBuckets().size();
            calculateErVisitCount(erVisitMemberCount, diagGroupWiseRecordMap, "erVisitCount");
            diagGroupWiseRecordMap.put("erVisitor", daigGroupWiseErVisitor);

            Nested visitAmount = bucket.getAggregations().get("nestedVisitAmounts");
            diagGroupWiseRecordMap.put("erCost", calculateTotalVisitAmounts(visitAmount));
            diagGroupWiseRecordMap.put("nonErVisitor", totalErVisitors - daigGroupWiseErVisitor);

            Terms diagnosisDesc = bucket.getAggregations().get("diagnosisDesc");

            for (Terms.Bucket diagDesc : diagnosisDesc.getBuckets()) {
                diagGroupWiseRecordMap.put("description", diagDesc.getKey());
            }
            record.recordMap.put(bucket.getKey(), diagGroupWiseRecordMap);
        }
    }


    private void calculateErVisitCount(Terms erVisitMemberCount, Map<String, Object> diagGroupWiseRecordMap, String aggName) {

        double twoOrMoreVisits = 0.0d;
        double oneVisit = 0.0d;

        for (Terms.Bucket bucket : erVisitMemberCount.getBuckets()) {
            Sum erVisitCount = bucket.getAggregations().get(aggName);
            if (erVisitCount.getValue() == 1) oneVisit++;
            else twoOrMoreVisits++;
        }

        diagGroupWiseRecordMap.put("twoOrMoreTimesVisitor", twoOrMoreVisits);
        diagGroupWiseRecordMap.put("oneTimeVisitor", oneVisit);
    }


    private double calculateTotalVisitAmounts(Nested nestedVisitAmounts) {

        Filter visitAmountFilter = nestedVisitAmounts.getAggregations().get("visitAmountFilter");
        Sum totalPaid = visitAmountFilter.getAggregations().get("totalPaid");
        return AmountUtils.getAmount(totalPaid.getValue());
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        for (Object object : jsonObject.values()) {
            JSONObject chronicConditions = (JSONObject) object;
            for (Object o : chronicConditions.values()) {
                JSONObject diagGroups = (JSONObject) o;
                for (String s : PERCENTAGE_CALCULATION_KEYS) {
                    diagGroups.put("percentOf" + s, CalculatorUtils.calculatePercentage((Number) diagGroups.get(s), (Number) state.hits.get("allMembers")));
                }
            }
        }

        return jsonObject;
    }
}
