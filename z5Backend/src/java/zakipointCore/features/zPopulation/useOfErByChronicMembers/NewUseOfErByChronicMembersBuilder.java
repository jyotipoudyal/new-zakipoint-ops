package zakipointCore.features.zPopulation.useOfErByChronicMembers;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 5/23/16.
 */
public class NewUseOfErByChronicMembersBuilder implements SummaryQueryBuilder {

    public static final Map<String, String> QM_ESFIELD_HASMETRIC_INFO = new HashMap<String, String>() {
        {
            put("1002", "hasAsthma");
            put("1009", "hasCopd");
            put("1008", "hasCongestiveHeartFailure");
            put("1005", "hasCad");
            put("1012", "hasDiabetes");
            put("1016", "hasHyperlipidemia");
            put("1024", "hasRheumatoidArthritis");
            put("1017", "hasHypertension");
            put("1011", "hasDepression");
            put("1006", "hasCancer");
            put("1007", "hasChronicPain");
            put("1001", "hasAffectivePsychosis");
            put("1003", "hasAtrialFibrillation");
            put("1010", "hasDemyelinatingDiseases");
            put("1014", "hasEsrd");
            put("1013", "hasEatingDisorders");
            put("1015", "hasHivAids");
            put("1018", "hasImmuneDisorders");
            put("1019", "hasInflammatoryBowelDisease");
            put("1020", "hasLiverDisease");
            put("1021", "hasMorbidObesity");
            put("1022", "hasOsteoarthritis");
            put("1023", "hasPeripheralVascularDisease");
            put("1004", "hasBloodDisorders");
        }
    };


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {

        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(visitAdmission(state, client));

        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }


    public SearchRequestBuilder visitAdmission(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, VisitAdmission.NAME);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        FilterUtils.serviceDateFilter(state, andFilterBuilder);
        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        LevelUtils.setLOAFilter(state, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        applyAggregations(builder, state);
        return builder;
    }


    private void applyAggregations(SearchRequestBuilder builder, RequestState state) {

        FiltersAggregationBuilder chronicConditionsFilters = AggregationBuilders.filters("chrocnicConditionsFilters");
        String chronicCode = state.request.get("chronicCode");
        String chronicMetric = QM_ESFIELD_HASMETRIC_INFO.get(chronicCode);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.hasParentFilter(MemberSearch.NAME, FilterBuilders.termFilter(chronicMetric, "Yes")));
        chronicConditionsFilters.filter("chronicFilter_" + chronicMetric, andFilterBuilder);

        AndFilterBuilder erVisitFilter = new AndFilterBuilder();
        FilterUtils.getUmVisitFilter(erVisitFilter, VisitAdmission.ER_VISIT);
        erVisitMetricBuilder(chronicConditionsFilters, erVisitFilter, state);
        builder.addAggregation(chronicConditionsFilters);
    }


    protected void erVisitMetricBuilder(FiltersAggregationBuilder chronicConditionsFilters, AndFilterBuilder erVisitFilter, RequestState state) {

        chronicConditionsFilters.subAggregation(AggregationBuilders.filter("erVisitors").filter(erVisitFilter)
                .subAggregation(AggregationBuilders.terms("diagGrouperWise").field("diag1SupergrouperId").size(0)
                        .subAggregation(AggregationBuilders.terms("membersCount").field(VisitAdmission.INT_MEMBER_ID).size(0)
                                .subAggregation(AggregationBuilders.sum("erVisitCount").field(VisitAdmission.ER_VISIT)))

                        .subAggregation(CommonQueries.nestedVisitAmountSum(state))
                        .subAggregation(AggregationBuilders.terms("diagnosisDesc").field("diag1Supergrouperdesc").size(1)))

                .subAggregation(AggregationBuilders.terms("conditionWise").field("intMemberId").size(0)
                        .subAggregation(AggregationBuilders.sum("conditionWiseErVisitors").field(VisitAdmission.ER_VISIT)))

                .subAggregation(CommonQueries.nestedVisitAmountSum(state)))
                .subAggregation(AggregationBuilders.terms("allMembers").field(VisitAdmission.INT_MEMBER_ID).size(0));
    }

}
