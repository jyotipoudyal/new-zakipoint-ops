package zakipointCore.features.zPopulation.useOfErByChronicMembers;

import com.configurations.utils.AmountUtils;
import com.configurations.utils.ChronicConditionEnum;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 5/19/16.
 */
public class UseOfErByChronicMembersListener extends AbstractSummaryListener {

    List<String> percentCaluclation= Arrays.asList("nonErVisitor","oneTimeVisitor","twoOrMoreTimesVisitor");


    @Override
    protected void handleResponse(SearchResponse response) {

        Integer chronicCode=(Integer.parseInt(state.request.get("chronicCode")));
        Integer chronicMembers=(Integer)state.hits.get("chronicMemberCount");
        Record record=getRecord(state, "erVisitInfoByChronicMembers");
        Nested visitAmounts=response.getAggregations().get("nestedVisitAmounts");

        record.recordMap.put("erCost", calculateTotalVisitAmounts(visitAmounts));
        record.recordMap.put("chronicMemberCount", chronicMembers);
        record.recordMap.put("chronicCode", chronicCode);
        record.recordMap.put("chronicDesc", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(chronicCode).getMetricDisplay());

        Terms erMemberCount=response.getAggregations().get("erMemberCount");
        record.recordMap.put("erVisitor", erMemberCount.getBuckets().size());
        record.recordMap.put("nonErVisitor",chronicMembers-erMemberCount.getBuckets().size());
        calculateErVisitCount(erMemberCount, record.recordMap, "erVisitCount");
    }


    private double calculateTotalVisitAmounts(Nested nestedVisitAmounts) {
        Filter visitAmountFilter = nestedVisitAmounts.getAggregations().get("visitAmountFilter");
        Sum totalPaid = visitAmountFilter.getAggregations().get("totalPaid");
        return AmountUtils.getAmount(totalPaid.getValue());
    }


    private void calculateErVisitCount(Terms erVisitMemberCount, Map<String, Object> diagGroupWiseRecordMap, String aggName) {
        double oneVisit = 0.0d;
        double twoOrMoreVisits = 0.0d;

        for (Terms.Bucket bucketEr : erVisitMemberCount.getBuckets()) {

            double erVisitCount = ((Sum)bucketEr.getAggregations().get(aggName)).getValue();
            if (erVisitCount == 1) oneVisit++;
            else twoOrMoreVisits++;
        }
        diagGroupWiseRecordMap.put("twoOrMoreTimesVisitor", twoOrMoreVisits);
        diagGroupWiseRecordMap.put("oneTimeVisitor", oneVisit);
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {
        for (String s : percentCaluclation) {
                double percent = CalculatorUtils.calculatePercentage((Number) jsonObject.getJSONObject("erVisitInfoByChronicMembers").get(s), (Number)state.hits.get("chronicMemberCount"));
                jsonObject.getJSONObject("erVisitInfoByChronicMembers").put("percentOf"+s, percent);
            }
        return  jsonObject;
    }
}
