package zakipointCore.features.zPopulation.useOfErByChronicMembers;

import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearch;
import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sktamang on 5/19/16.
 */
public class UseOfErByChronicMembersBuilder implements SummaryQueryBuilder {


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(visitAdmission(state, client));
        return builder;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }


    private List<Integer> memberList(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterUtils.eligibleDateFilter(state.periodTo()));
        LevelUtils.setLevelFilter(state, andFilterBuilder, state.periodTo());
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        builder.addAggregation(AggregationBuilders.filter("chronicFilter").filter(CommonQueries.newQmFilter(state.periodTo(), state.request.get("chronicCode")))
                .subAggregation(AggregationBuilders.terms("members").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0)));
        SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "useOfErByChronicMembers");
        Filter chronicFilter = response.getAggregations().get("chronicFilter");
        Terms members = chronicFilter.getAggregations().get("members");

        List<Integer> memberList = new ArrayList<>();
        for (Terms.Bucket member : members.getBuckets()) {
            memberList.add(Integer.parseInt(member.getKey()));
        }

        state.hits.put("chronicMemberCount",memberList.size());
        return memberList;
    }


    public SearchRequestBuilder visitAdmission(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, VisitAdmission.NAME);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        LevelUtils.setLOAFilter(state, andFilterBuilder);
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        andFilterBuilder.add(FilterBuilders.termsFilter("intMemberId", memberList(state, client)));

        FilterUtils.getUmVisitFilter(andFilterBuilder, VisitAdmission.ER_VISIT);
        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        applyAggregations(builder, state);
        return builder;
    }


    private void applyAggregations(SearchRequestBuilder builder, RequestState state) {
        AndFilterBuilder paidDateFilter = new AndFilterBuilder();
        FilterUtils.nestedPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME, paidDateFilter);
        builder.addAggregation(CommonQueries.nestedVisitAmountSum(state))
                .addAggregation(AggregationBuilders.terms("erMemberCount").field("intMemberId").size(0)
                        .subAggregation(AggregationBuilders.sum("erVisitCount").field(VisitAdmission.ER_VISIT)));
    }
}
