package zakipointCore.features.zPopulation.rootConditionAlongOtherChronicConditions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 5/27/16.
 */
public class ChronicAndCormobiditiesPair {

    public static Map<Integer,List<Integer>> CHRONIC_CORMOBIDITIES=new HashMap<Integer,List<Integer>>(){{
        put(1012, Arrays.asList(1017,1016,1005,1014));   //obesity to be implemented
        put(1008,Arrays.asList(1002,1012,1014));
        put(1005,Arrays.asList(1002,1017,1012,1016));  //obesity to be implemented
        put(1002,Arrays.asList(1009));    //obesity to be implemented
        put(1009,Arrays.asList(1005,1008));  //Osteoporosis and Pneumonia to be implemented
    }};
}
