package zakipointCore.features.zPopulation.rootConditionAlongOtherChronicConditions;

import com.configurations.utils.ChronicConditionEnum;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 5/10/16.
 */
public class RootCondnAlongOtherChronicListener extends AbstractSummaryListener {


    @Override
    protected void handleResponse(SearchResponse response) {

        Filters rootConditionAlongOtherConditionsAggregation = response.getAggregations().get("rootConditionAlongOtherConditionsAggregation");
        Record record = getRecord(state, "chronicMergedWithOtherConditions");

        Integer chronicCode = Integer.parseInt(state.request.get("chronicCode"));
        String rootCondition = ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(chronicCode).getMetricDescription();

        int i = 0;

        for (Filters.Bucket conditions : rootConditionAlongOtherConditionsAggregation.getBuckets()) {

            Map<String, Object> chronicInfoMap = new HashMap<>();

            if (conditions.getKey().contains("only")) {
                state.hits.put("onlyRootConditionPopulation", ((Long) conditions.getDocCount()).doubleValue());
                chronicInfoMap.put("population", ((Long) conditions.getDocCount()).doubleValue());
                chronicInfoMap.put("otherConditionCode", "Only");
                chronicInfoMap.put("rootCondition", rootCondition);
                chronicInfoMap.put("otherConditionDescription", "Only");
                chronicInfoMap.put("rootConditionCode",chronicCode);
            }

            else {

                int otherCondition = Integer.valueOf(conditions.getKey());

                if (otherCondition==chronicCode) {
                    state.hits.put("rootConditionCode", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(otherCondition).getMetricCode());
                    state.hits.put("rootConditionPopulation", ((Long) conditions.getDocCount()).doubleValue());
                }
                chronicInfoMap.put("population", ((Long) conditions.getDocCount()).doubleValue());
                chronicInfoMap.put("otherConditionCode", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(otherCondition).getMetricCode());
                chronicInfoMap.put("rootCondition",rootCondition);
                chronicInfoMap.put("rootConditionCode",chronicCode);
                chronicInfoMap.put("otherConditionDescription", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(otherCondition).getMetricDescription());

            }
            record.recordMap.put(String.valueOf(i), chronicInfoMap);
            i++;
        }

        Filter rootCondPopWithOtherConditions = response.getAggregations().get("havingOneOrMoreOtherConditions");
        Record record1 = getRecord(state, "metaInfo");
        record1.recordMap.put("havingOneOrMoreOtherConditions", ((Long) rootCondPopWithOtherConditions.getDocCount()).doubleValue());
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        double memberCount = (double) state.hits.get("memberCount");
        jsonObject.getJSONObject("metaInfo").put("memberCount", memberCount);

        for (Object object : jsonObject.getJSONObject("chronicMergedWithOtherConditions").entrySet()) {
            Map<String, Object> innerObject = (Map<String, Object>) ((Map.Entry<String, Object>) object).getValue();
            double percent = CalculatorUtils.calculatePercentage((Double) innerObject.get("population"), memberCount);
            innerObject.put("percentOfPopulation", percent);
        }

        double rootConditon = (double) state.hits.get("rootConditionPopulation");
        double havingOneOrMoreOtherConditions = (double) jsonObject.getJSONObject("metaInfo").get("havingOneOrMoreOtherConditions");

        jsonObject.getJSONObject("metaInfo").put("percentOfHavingOneOrMoreOtherConditions", CalculatorUtils.calculatePercentage(havingOneOrMoreOtherConditions, rootConditon));
        jsonObject.getJSONObject("metaInfo").put("rootConditionPopulation", rootConditon);
        jsonObject.getJSONObject("metaInfo").put("rootConditionDescription",
                ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(Integer.parseInt(state.request.get("chronicCode"))).getMetricDisplay());

        return jsonObject;
    }
}
