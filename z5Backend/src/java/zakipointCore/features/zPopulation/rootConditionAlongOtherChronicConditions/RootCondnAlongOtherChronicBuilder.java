package zakipointCore.features.zPopulation.rootConditionAlongOtherChronicConditions;

import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

import java.util.*;

/**
 * Created by sktamang on 5/10/16.
 */
public class RootCondnAlongOtherChronicBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(query(state, client));
        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {

        int chronicCode=Integer.parseInt(state.request.get("chronicCode"));
        double memberCount=0.0d;
        state.hits.put("memberCount", CommonQueries.getMemberCount(state, client, memberCount));

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        AndFilterBuilder andFilter = new AndFilterBuilder();
        CommonQueries.caseMemberFilter(state, client, andFilter);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilter));

        rootConditionAlongOtherConditionsAggregation(state, builder, "rootConditionAlongOtherConditionsAggregation", chronicCode);
        rootCondPopWithOtherConditions(state, builder, chronicCode);
        return builder;
    }


    private void rootCondPopWithOtherConditions(RequestState state, SearchRequestBuilder builder,int chronicCode) {
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(CommonQueries.newQmFilter(state.periodTo(),Arrays.asList(chronicCode)));
        builder.addAggregation(AggregationBuilders.filter("havingOneOrMoreOtherConditions")
                .filter(eligibilityAndQmFilter(state,ChronicAndCormobiditiesPair.CHRONIC_CORMOBIDITIES.get(chronicCode),andFilterBuilder)));
    }


    private void rootConditionAlongOtherConditionsAggregation(RequestState state
            , SearchRequestBuilder builder,String aggName,int chronicCode) {

        FiltersAggregationBuilder filtersAggs = AggregationBuilders.filters(aggName);

        for (int pair : ChronicAndCormobiditiesPair.CHRONIC_CORMOBIDITIES.get(chronicCode)) {
            AndFilterBuilder andFilter=new AndFilterBuilder();
            andFilter = eligAndQmFilter(state, chronicCode, pair, andFilter);
            filtersAggs.filter(String.valueOf(pair), andFilter);
        }

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder = eligAndQmFilter(state, chronicCode, chronicCode, andFilterBuilder);
        filtersAggs.filter(String.valueOf(chronicCode), andFilterBuilder);

        implementChronicConditionOnlyAggregation(state,filtersAggs,chronicCode);
        builder.addAggregation(filtersAggs);
    }

    private AndFilterBuilder eligAndQmFilter(RequestState state, int chronicCode, int pair, AndFilterBuilder andFilter) {
        andFilter = eligibilityAndQmFilter(state, Arrays.asList(pair),andFilter);
        andFilter.add(CommonQueries.newQmFilter(state.periodTo(), Arrays.asList(chronicCode)));
        return andFilter;
    }


    private void implementChronicConditionOnlyAggregation(RequestState state, FiltersAggregationBuilder filtersAggs,int chronicCode) {

        AndFilterBuilder andFilter=new AndFilterBuilder();
        andFilter = eligibilityAndQmFilter(state, Arrays.asList(chronicCode),andFilter);
        andFilter.add(FilterBuilders.notFilter(CommonQueries.newQmFilter(state.periodTo(), ChronicAndCormobiditiesPair.CHRONIC_CORMOBIDITIES.get(chronicCode))));
        filtersAggs.filter("onlyRootCondition", andFilter);
    }


    private AndFilterBuilder eligibilityAndQmFilter(RequestState state, List<Integer> pair,AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(levelAndEligibilityFilter(state));
        andFilterBuilder.add(CommonQueries.newQmFilter(state.periodTo(), pair));
        return andFilterBuilder;
    }


    private AndFilterBuilder levelAndEligibilityFilter(RequestState state) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        LevelUtils.setLevelFilter(state, andFilter, state.periodTo());
        andFilter.add(FilterUtils.eligibleDateFilter(state.periodTo()));
        return andFilter;
    }
}
