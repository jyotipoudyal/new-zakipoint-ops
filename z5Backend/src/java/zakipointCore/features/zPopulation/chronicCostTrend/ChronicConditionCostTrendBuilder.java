package zakipointCore.features.zPopulation.chronicCostTrend;

import com.configurations.utils.ChronicConditionEnum;
import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.NestedBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.*;

/**
 * Created by sktamang on 5/10/16.
 */
public class ChronicConditionCostTrendBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {

        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        builder.add(query(state, client));

        double memberMonths = 0;
        memberMonths = CommonQueries.getTotalMemberMonth(state, client, memberMonths);
        state.hits.put("memberMonths", memberMonths);
        return builder;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        String chronicCode = state.request.get("chronicCode");

        AndFilterBuilder andFilter = new AndFilterBuilder();
        CommonQueries.caseMemberFilter(state, client, andFilter);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilter));

        totalPaidAmountAggregation(state, builder, chronicCode);
        conditionWiseMemberMonthAggregation(state, builder, chronicCode);
        return builder;
    }


    private void conditionWiseMemberMonthAggregation(RequestState state, SearchRequestBuilder builder, String chronicCode) {
        builder.addAggregation(newMemberMonth(state, "eligibilityAndQmMetricFilter", chronicCode));
    }


    private void totalPaidAmountAggregation(RequestState state, SearchRequestBuilder builder, String chronicCode) {
        AndFilterBuilder overAllMemberFilter=new AndFilterBuilder();
        overAllMemberFilter.add(FilterUtils.eligibleDateFilter(state.periodTo()));
        LevelUtils.setLevelFilter(state, overAllMemberFilter, state.periodTo());

        FiltersAggregationBuilder qmFilters = AggregationBuilders.filters("qmFilters");
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(implementQmCodeFilter(state, chronicCode));
        qmFilters.filter("diabetes", andFilterBuilder);

        implementConditionWiseAggregation(state, qmFilters);

        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        CommonQueries.paidMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        CommonQueries.serviceMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        builder.addAggregation(AggregationBuilders.filter("overAllMember").filter(overAllMemberFilter)
                .subAggregation(qmFilters).subAggregation(memberAmountSumAggregator(memberAmountFilter)));
    }


    private void implementConditionWiseAggregation(RequestState state, FiltersAggregationBuilder qmFilters) {

        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        CommonQueries.paidMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        CommonQueries.serviceMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);

        qmFilters.subAggregation(memberAmountSumAggregator(memberAmountFilter));

        actualProspectivePmpyCalculation(qmFilters, state);
        concurrentAndProspectivePmpyCalculation(qmFilters, state);
    }


    private void concurrentAndProspectivePmpyCalculation(FiltersAggregationBuilder qmFilters, RequestState state) {

        int chronicCode = Integer.valueOf(state.request.get("chronicCode"));
        String concurrentRawField = "ConcurrentRaw";  //this is the percentContribution * concurrentTotal
        String ProspectiveRawField = "ProspectiveRaw";  //this is the percentContribution * prospectiveTotal

        qmFilters.subAggregation(AggregationBuilders.sum("concurrent")
                .field(ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(chronicCode).getChronicPercentFieldName()+concurrentRawField))
                .subAggregation(AggregationBuilders.sum("prospective")
                        .field(ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(chronicCode).getChronicPercentFieldName()+ProspectiveRawField));
    }

    private void actualProspectivePmpyCalculation(FiltersAggregationBuilder qmFilters, RequestState state) {
        String riskScoreField = state.request.hasParam("group") ? "prospectiveTotalDoubleNormalizedToGroup" : "prospectiveTotalDoubleNormalizedToBob";
        qmFilters.subAggregation(AggregationBuilders.avg("actualPmpyProspective").field(riskScoreField));
    }


    private NestedBuilder memberAmountSumAggregator(AndFilterBuilder memberAmountFilter) {
        return AggregationBuilders.nested("memberAmountNested").path(MemberSearch.MEMBER_AMOUNT.NAME)
                .subAggregation(AggregationBuilders.filter("memberAmountFilter").filter(memberAmountFilter)
                        .subAggregation(AggregationBuilders.sum("totalMemberPaidAmount").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT)));
    }


    public AndFilterBuilder implementQmCodeFilter(RequestState state, Object... chronicCode) {

        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermsFilter("qm.measure", chronicCode));
        qmFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));
        qmFilter.add(FilterUtils.getRangeFilter(state));

        return FilterBuilders.andFilter(FilterBuilders.nestedFilter("qm", qmFilter));
    }


    public AbstractAggregationBuilder newMemberMonth(RequestState state, String aggName, Object... chronicCode) {

        String paramFrom = state.periodFrom();
        String paramTo = state.periodTo();
        FiltersAggregationBuilder filtersAggs = AggregationBuilders.filters(aggName);
        getMemberMonthsFilter(state, paramFrom, paramTo, filtersAggs, chronicCode);

        return filtersAggs;
    }

    public static void getMemberMonthsFilter(RequestState state, String paramFrom, String paramTo, FiltersAggregationBuilder filtersAggs, Object[] chronicCode) {
        for (String month : DateUtils.getMonthsBetween(paramFrom, paramTo)) {
            AndFilterBuilder andFilter = new AndFilterBuilder();
            LevelUtils.setLevelFilter(state, andFilter, month);
            andFilter.add(FilterUtils.eligibleDateFilter(month));
            andFilter.add(newQmFilter(state, chronicCode));
            filtersAggs.filter(month, andFilter);
        }
    }

    public static FilterBuilder newQmFilter(RequestState state, Object... metric) {

        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.termsFilter("qm.measure", metric));
        andFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));
        andFilter.add(newQmRangeFilter(state));

        return FilterBuilders.nestedFilter("qm", andFilter);
    }

    public static AndFilterBuilder newQmRangeFilter(RequestState state) {
        RangeFilterBuilder rangeFilter = FilterBuilders.rangeFilter("qm.toDate").from(state.periodFrom()).to(null);
        RangeFilterBuilder rangeFilter1 = FilterBuilders.rangeFilter("qm.fromDate").from(null).to(state.periodTo());

        return new AndFilterBuilder().add(rangeFilter).add(rangeFilter1);
    }
}
