package zakipointCore.features.zPopulation.chronicCostTrend;

import com.configurations.utils.ChronicConditionEnum;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 5/10/16.
 */
public class ChronicConditionCostTrendListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        int metricCode = Integer.parseInt(state.request.get("chronicCode"));
        Filters newMemberMonthsFilters = response.getAggregations().get("eligibilityAndQmMetricFilter");

        Filter overAllMember=response.getAggregations().get("overAllMember");
        Filters qmFilters = overAllMember.getAggregations().get("qmFilters");
        Nested memberAmountNested = overAllMember.getAggregations().get("memberAmountNested");

        Record record = getRecord(state, "chronicInfo");
        record.recordMap.put("memberMonths",state.hits.get("memberMonths"));
        record.recordMap.put("overAllMember", Double.valueOf(overAllMember.getDocCount()));
        metaInfoGenerator(metricCode, record);

        totalPaidExtract(record, memberAmountNested, "overAllTotalPaidAmount");
        extractPaidAmount(qmFilters, record);
        extractNewMemberMonths(newMemberMonthsFilters, record);
    }

    private void metaInfoGenerator(int metricCode, Record record) {
        record.recordMap.put("description", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(metricCode).getMetricDescription());
        record.recordMap.put("display", ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(metricCode).getMetricDisplay());
        record.recordMap.put("metricCode", metricCode);
    }


    private void extractPaidAmount(Filters qmFilters, Record record) {

        for (Filters.Bucket bucket : qmFilters.getBuckets()) {

            Nested memberAmountNested = bucket.getAggregations().get("memberAmountNested");

            Sum chronicPercentConcurrent=bucket.getAggregations().get("concurrent");
            Sum ChronicPercentProspective=bucket.getAggregations().get("prospective");

            Avg normalizedScoreAvg=bucket.getAggregations().get("actualPmpyProspective");
            totalPaidExtract(record, memberAmountNested, "totalPaidAmount");

            record.recordMap.put("chronicPercentConcurrent",chronicPercentConcurrent.getValue());
            record.recordMap.put("population",((Long)bucket.getDocCount()).doubleValue());
            record.recordMap.put("chronicPercentProspective",ChronicPercentProspective.getValue());
            record.recordMap.put("normalizedScoreAvg",Double.isNaN(normalizedScoreAvg.getValue()) ? 0 : normalizedScoreAvg.getValue());
        }
    }


    private void totalPaidExtract(Record record, Nested memberAmountNested,String aggName) {

        Filter memberAmountFilter = memberAmountNested.getAggregations().get("memberAmountFilter");
        Sum totalMemberPaidAmount = memberAmountFilter.getAggregations().get("totalMemberPaidAmount");
        record.recordMap.put(aggName, totalMemberPaidAmount.getValue());
    }


    private void extractNewMemberMonths(Filters newMemberMonthsFilters, Record record) {

        double conditionWiseMemberMonth = 0.0d;
        for (Filters.Bucket bucket : newMemberMonthsFilters.getBuckets()) {
            conditionWiseMemberMonth = conditionWiseMemberMonth + (double) bucket.getDocCount();
        }
        record.recordMap.put("conditionWiseMemberMonth", conditionWiseMemberMonth);
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject response) {

        JSONObject chronicInfo = response.getJSONObject("chronicInfo");

        double totalPaidAmount = (double) chronicInfo.get("totalPaidAmount");
        double overAllTotalPaidAmount = (double) chronicInfo.get("overAllTotalPaidAmount");
        double overAllMember = (double) chronicInfo.get("overAllMember");
        double conditionWiseMemberMonth = (double) chronicInfo.get("conditionWiseMemberMonth");

        double pmpy = CalculatorUtils.newPmpy(totalPaidAmount, conditionWiseMemberMonth);

        double overAllPmpm = CalculatorUtils.divide(overAllTotalPaidAmount,(double) state.hits.get("memberMonths"));
        chronicInfo.put("overAllPmpm",overAllPmpm);

        double avgCostForOverAllMember=CalculatorUtils.divide(overAllTotalPaidAmount,overAllMember);
        chronicInfo.put("overAllAvg",avgCostForOverAllMember);

        acutualPmpyProjected(chronicInfo, overAllPmpm);
        chronicInfo.put("pmpy", pmpy);
        projectedPmpyCalculation(chronicInfo,avgCostForOverAllMember,totalPaidAmount);
        return response;
    }


    private double acutualPmpyProjected(JSONObject chronicInfo, double overAllPmpm) {

        double normalizedScoreAvgForActualpmpy=(double)chronicInfo.get("normalizedScoreAvg");
        double actualProspectiveCost=overAllPmpm*normalizedScoreAvgForActualpmpy;

        chronicInfo.put("actualProspectiveCostPmpy",actualProspectiveCost*12);
        chronicInfo.put("actualProspectiveCost",actualProspectiveCost);
        return  actualProspectiveCost;
    }


    private void projectedPmpyCalculation(JSONObject chronicInfo,double avgCostForOverAll,double totalPaidAmountForChronicMembers) {
        double population = (double) chronicInfo.get("population");

        double chronicPercentProspective = (double) chronicInfo.get("chronicPercentProspective");
        double chronicPercentConcurrent = (double) chronicInfo.get("chronicPercentConcurrent");

        double concurrentChronicCostPredicted =avgCostForOverAll * chronicPercentConcurrent;
        double prospectiveChronicCostPredicted =avgCostForOverAll * chronicPercentProspective;

        chronicInfo.put("projectCostTobeChangeByPercent", CalculatorUtils.calculatePercentage(prospectiveChronicCostPredicted-concurrentChronicCostPredicted,concurrentChronicCostPredicted));
        chronicInfo.put("concurrentPmpy",CalculatorUtils.divide(concurrentChronicCostPredicted, population));
        chronicInfo.put("prospectivePmpy",CalculatorUtils.divide(prospectiveChronicCostPredicted, population));
        chronicInfo.put("populationRiskContribution",CalculatorUtils.calculatePercentage(concurrentChronicCostPredicted, totalPaidAmountForChronicMembers));
        chronicInfo.put("concurrentChronicCostPredicted",concurrentChronicCostPredicted);
        chronicInfo.put("prospectiveChronicCostPredicted",prospectiveChronicCostPredicted);
    }
}
