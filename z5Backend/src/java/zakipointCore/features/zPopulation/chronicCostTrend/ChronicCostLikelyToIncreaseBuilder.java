package zakipointCore.features.zPopulation.chronicCostTrend;

import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 6/17/16.
 */
public class ChronicCostLikelyToIncreaseBuilder implements SummaryQueryBuilder {
    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        return null;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
