package zakipointCore.features.zPopulation.recommendedCareForChronicPatients;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.QmUtils;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.*;

import java.util.*;

/**
 * Created by sktamang on 5/11/16.
 */
public class RecommendedCareForChronicBuilder implements SummaryQueryBuilder {

    public  Map<Integer, Map<String,String>> CHRONIC_INFO = new HashMap<>();

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {

        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        initChronicInfo();
        state.hits.put("chronicInfoMap",CHRONIC_INFO);
        builder.add(memberSearchBuilder(state, client,0));
        return builder;
    }

    protected void initChronicInfo(){
        CHRONIC_INFO.put(1002, QmUtils.ASTHMA_CODE_DESC);
        CHRONIC_INFO.put(1009, QmUtils.CHRONIC_OBSTRUCTIVE_PULMONARY);
        CHRONIC_INFO.put(1008, QmUtils.CONGESTIVE_HEART_FAILURE);
        CHRONIC_INFO.put(1005, QmUtils.CORONARY_ARTERY_DISEASE);
        CHRONIC_INFO.put(1012, QmUtils.DIABETES);
        CHRONIC_INFO.put(1016, QmUtils.HYPERLIPIDEMIA);
        CHRONIC_INFO.put(1024, QmUtils.RHEUMATOID_ARTHRITIS);
        CHRONIC_INFO.put(1017, QmUtils.HYPERTENSION);
    }

    public SearchRequestBuilder memberSearchBuilder(RequestState state,Client client, int metricCode){
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);

        initBuilder(state, client, builder);

        metricCode=(metricCode == 0)?Integer.parseInt(state.request.get("chronicCode")):metricCode;

        numeratorDenominatorAgg(state, builder,metricCode);
        metaInfoBuilder(state, metricCode, builder,true);
        costCalulationForEachMetric(state, builder,metricCode);
        conditionWiseMemberMonth(state, builder,metricCode);
        return builder;
    }

    protected void metaInfoBuilder(RequestState state, int metricCode, SearchRequestBuilder builder,boolean addMetaInfoAggregator) {
        if(addMetaInfoAggregator) {
            compliantNonCompliantMemberAgg(state, builder, metricCode, "overAllCompliantMember", true);
            compliantNonCompliantMemberAgg(state, builder, metricCode, "overAllNonCompliantMember", false);
            overAllUniqueMemberAgg(state, builder, metricCode);
        }
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {return null;}

    protected void initBuilder(RequestState state, Client client, SearchRequestBuilder builder) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterUtils.eligibleDateFilter(state.periodTo()));
        LevelUtils.setLevelFilter(state, andFilterBuilder, state.periodTo());
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
    }


    protected void numeratorDenominatorAgg(RequestState state, SearchRequestBuilder builder, int metricCode) {

        FiltersAggregationBuilder qmDiabetesfilters = AggregationBuilders.filters("filters_"+metricCode);

        for (Map.Entry<String, String> pair : CHRONIC_INFO.get(metricCode).entrySet()) {
            qmFilter(state, qmDiabetesfilters, pair);
        }

        builder.addAggregation(qmDiabetesfilters);
    }

    protected void qmFilter(RequestState state, FiltersAggregationBuilder qmDiabetesfilters, Map.Entry<String, String> pair) {
        AndFilterBuilder andFilterForEligiblePop = new AndFilterBuilder();
        AndFilterBuilder andFilterForNonMeetingPop = new AndFilterBuilder();
        String postiveOrNegative = pair.getValue().split("__")[1];
        String flag = postiveOrNegative.equalsIgnoreCase("1") ? "0" : "1";

        andFilterForNonMeetingPop.add(FilterUtils.implementNumerator(state, flag, true, pair.getKey()));
        andFilterForEligiblePop.add(FilterUtils.implementNumerator(state, "", false, pair.getKey()));

        qmDiabetesfilters.filter(pair.getKey() + "_" + "num", andFilterForNonMeetingPop);
        qmDiabetesfilters.filter(pair.getKey() + "_" + "deno", andFilterForEligiblePop);
    }


    protected void conditionWiseMemberMonth(RequestState state, SearchRequestBuilder builder,int metricCode) {

        String paramFrom = state.periodFrom();
        String paramTo = state.periodTo();
        for (String key : CHRONIC_INFO.get(metricCode).keySet()) {
            FiltersAggregationBuilder filtersAggs = AggregationBuilders.filters(key + "_memberMonths_"+metricCode);

            for (String month : DateUtils.getMonthsBetween(paramFrom, paramTo)) {
                AndFilterBuilder andFilter = new AndFilterBuilder();
                LevelUtils.setLevelFilter(state, andFilter, month);
                andFilter.add(FilterUtils.eligibleDateFilter(month));
                andFilter.add(CommonQueries.newQmFilter(month, key));
                filtersAggs.filter(month, andFilter);
            }

            builder.addAggregation(filtersAggs);
        }
    }


    protected void costCalulationForEachMetric(RequestState state, SearchRequestBuilder builder,int metricCode) {

        FiltersAggregationBuilder qmDiabetesfilters = AggregationBuilders.filters("memberAmountCostFilters_"+metricCode);

        for (Map.Entry<String, String> pair : CHRONIC_INFO.get(metricCode).entrySet()) {
            AndFilterBuilder andFilterForEligiblePop = new AndFilterBuilder();
            andFilterForEligiblePop.add(FilterUtils.implementNumerator(state, "", false, pair.getKey()));
            qmDiabetesfilters.filter(pair.getKey() + "_population", andFilterForEligiblePop);
        }

        qmDiabetesfilters.subAggregation(implementMemberAmount(state));
        builder.addAggregation(qmDiabetesfilters);
    }


    protected AggregationBuilder implementMemberAmount(RequestState state) {

        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        CommonQueries.paidMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        CommonQueries.serviceMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);

        return AggregationBuilders.nested("memberAmountNested").path(MemberSearch.MEMBER_AMOUNT.NAME)
                .subAggregation(AggregationBuilders.filter("memberAmountFilter").filter(memberAmountFilter)
                        .subAggregation(AggregationBuilders.sum("totalMemberPaidAmount").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT)));

    }


    protected void compliantNonCompliantMemberAgg(RequestState state, SearchRequestBuilder builder, int metricCode, String aggName, boolean compliantOrNon) {

        int positiveBit=1;
        int negativeBit=0;

        if(!compliantOrNon){
            int temp=positiveBit;
            positiveBit=negativeBit;
            negativeBit=temp;
        }

        List<String> positiveMetricList = new ArrayList<>();
        List<String> negativeMetricList = new ArrayList<>();
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();

        for (Map.Entry<String, String> pair : CHRONIC_INFO.get(metricCode).entrySet()) {
            String postiveOrNegative = pair.getValue().split("__")[1];
            if (postiveOrNegative.equals("1")) positiveMetricList.add(pair.getKey());
            else negativeMetricList.add(pair.getKey());
        }

        AndFilterBuilder positiveMetricFilter = new AndFilterBuilder();
        AndFilterBuilder negativeMetricFilter = new AndFilterBuilder();
        positiveMetricFilter.add(implementNumerator(state, positiveBit, true, positiveMetricList.toArray()));
        negativeMetricFilter.add(implementNumerator(state, negativeBit, true, negativeMetricList.toArray()));

        OrFilterBuilder orFilterBuilder = new OrFilterBuilder();
        orFilterBuilder.add(positiveMetricFilter).add(negativeMetricFilter);

        andFilterBuilder.add(orFilterBuilder);
        builder.addAggregation(AggregationBuilders.filter(aggName+"_"+metricCode).filter(andFilterBuilder)
                .subAggregation(implementMemberAmount(state)));
    }


    protected void overAllUniqueMemberAgg(RequestState state, SearchRequestBuilder builder,int metricCode) {

        List<String> metricList = new ArrayList<>();

        for (Map.Entry<String, String> pair : CHRONIC_INFO.get(metricCode).entrySet()) {
            metricList.add(pair.getKey());
        }

        AndFilterBuilder filter = new AndFilterBuilder();
        filter.add(implementNumerator(state, 0, false, metricList.toArray()));
        builder.addAggregation(AggregationBuilders.filter("overAllUniqueMember_"+metricCode).filter(filter));
    }


    public static AndFilterBuilder implementNumerator(RequestState state, int numOrDen, boolean isNumerator, Object[] metricCode) {

        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermsFilter("qm.measure", metricCode));
        if (isNumerator) qmFilter.add(FilterBuilders.termFilter("qm.numerator", numOrDen));
        else qmFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));

        qmFilter.add(FilterUtils.getRangeFilter(state));
        return FilterBuilders.andFilter(FilterBuilders.nestedFilter("qm", qmFilter));
    }
}
