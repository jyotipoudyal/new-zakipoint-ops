package zakipointCore.features.zPopulation.recommendedCareForChronicPatients;

import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.Map;

/**
 * Created by sktamang on 5/11/16.
 */
public class RecommendedCareForChronicListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        String metricCode="_"+state.request.requestParameters.get("chronicCode");

        for (Aggregation aggregation : response.getAggregations()) {
            if (aggregation.getName().contains("memberMonths")) handleMemberMonths(aggregation);
        }

        Filters filters = response.getAggregations().get("filters"+metricCode);
        Filters memberAmountCostFilters = response.getAggregations().get("memberAmountCostFilters"+metricCode);

        extractMetricWisePopulation(filters,metricCode);
        extractMemberAmount(memberAmountCostFilters);
        metaInfoGenerator(response,metricCode);
    }


    protected void metaInfoGenerator(SearchResponse response,String metricCode) {

        Filter overAllUniqueMember = response.getAggregations().get("overAllUniqueMember"+metricCode);
        Filter overAllNonCompliantMember = response.getAggregations().get("overAllNonCompliantMember"+metricCode);
        Filter overAllCompliantMember = response.getAggregations().get("overAllCompliantMember"+metricCode);

        long nonCompliantMember = overAllNonCompliantMember.getDocCount();
        long compliantMember = overAllCompliantMember.getDocCount();
        long overAllMember = overAllUniqueMember.getDocCount();

        Nested overAllNonCompliantMemberNested = overAllNonCompliantMember.getAggregations().get("memberAmountNested");
        Nested overAllCompliantMemberNested = overAllCompliantMember.getAggregations().get("memberAmountNested");

        Record record = getRecord(state, "metaInfo");
        record.recordMap.put("overAllNonCompliantMember", nonCompliantMember);
        record.recordMap.put("overAllUniqueMember", overAllMember);
        record.recordMap.put("compliantMember", compliantMember);

        record.recordMap.put("percentOfNonCompliant", CalculatorUtils.calculatePercentageForLong(nonCompliantMember, overAllMember));
        record.recordMap.put("percentOfCompliant", CalculatorUtils.calculatePercentageForLong(compliantMember, overAllMember));
        record.recordMap.put("overAllCompliantMemberCost", calculateTotalVisitAmounts(overAllCompliantMemberNested));
        record.recordMap.put("overAllNonCompliantMemberCost", calculateTotalVisitAmounts(overAllNonCompliantMemberNested));
    }


    protected void handleMemberMonths(Aggregation aggregation) {

        Record record = getRecord(state, aggregation.getName().split("_")[0]);
        Filters filters = (Filters) aggregation;
        double conditionWiseMemberMonth = 0.0d;
        for (Filters.Bucket bucket : filters.getBuckets()) {
            conditionWiseMemberMonth = conditionWiseMemberMonth + bucket.getDocCount();
        }
        record.recordMap.put("conditionWiseMemberMonth", conditionWiseMemberMonth);
    }


    protected void extractMetricWisePopulation(Filters filters,String metric) {

        int metricCode=0;
        if(state.request.hasParam("chronicCode")) metricCode=Integer.parseInt(state.request.get("chronicCode"));
        else metricCode=Integer.parseInt(metric);

        Map<Integer, Map<String,String>> chronicInfo= (Map<Integer, Map<String, String>>) state.hits.get("chronicInfoMap");
        for (Filters.Bucket bucket : filters.getBuckets()) {
            String[] aggName = bucket.getKey().split("_");
            Record record = getRecord(state, aggName[0]);
            String[] metricInfo = chronicInfo.get(metricCode).get(aggName[0]).split("__");

            String pop = "membersInGroup";
            if (aggName[1].equalsIgnoreCase("num")) pop = "notMeeting";

            record.recordMap.put(pop, bucket.getDocCount());
            record.recordMap.put("description", metricInfo[0]);
            record.recordMap.put("metricType", metricInfo[1].equalsIgnoreCase("1") ? "positive" : "negative");
            record.recordMap.put("category", metricInfo[2]);
            record.recordMap.put("name", metricInfo[3]);
            record.recordMap.put("code", aggName[0]);
        }
    }


    protected void extractMemberAmount(Filters memberAmountCostFilters) {

        for (Filters.Bucket bucket : memberAmountCostFilters.getBuckets()) {
            Record record = getRecord(state, bucket.getKey().split("_")[0]);
            Nested memberAmountNested = bucket.getAggregations().get("memberAmountNested");
            Filter memberAmountFilter = memberAmountNested.getAggregations().get("memberAmountFilter");
            Sum totalMemberPaidAmount = memberAmountFilter.getAggregations().get("totalMemberPaidAmount");
            record.recordMap.put("totalPaidAmount", totalMemberPaidAmount.getValue());
        }
    }


    private double calculateTotalVisitAmounts(Nested nestedVisitAmounts) {
        Filter visitAmountFilter = nestedVisitAmounts.getAggregations().get("memberAmountFilter");
        Sum totalPaid = visitAmountFilter.getAggregations().get("totalMemberPaidAmount");
        return totalPaid.getValue();
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        for (Object o : jsonObject.entrySet()) {
            Map.Entry<String, Object> innerMap = (Map.Entry<String, Object>) o;

            if (!innerMap.getKey().contains("metaInfo")) {
                JSONObject object = (JSONObject) innerMap.getValue();
                double metPopulation = (Integer) object.get("membersInGroup")-(Integer) object.get("notMeeting");
                object.put("percentNotMeeting", CalculatorUtils.calculatePercentage((Number) object.get("notMeeting"), (Number) object.get("membersInGroup")));
                object.put("met", metPopulation);
                object.put("percentMet", CalculatorUtils.calculatePercentage(metPopulation, (Number) object.get("membersInGroup")));
                object.put("costPmpy", CalculatorUtils.newPmpy((double) object.get("totalPaidAmount"), (double) object.get("conditionWiseMemberMonth")));
            }
        }

        return jsonObject;
    }
}
