package zakipointCore.features.network.highClaimantsOON;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sktamang on 11/17/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class HighOutNetworkMemberListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        if((boolean)state.hits.get("testResult")==true) {

            List<Terms.Bucket> sortedMembers = paginate(response.getAggregations().get("members"), state);

            for (Terms.Bucket memberBucket : sortedMembers) {

                Sum totalPaidAmount = memberBucket.getAggregations().get("totalPaidAmount");
                Filter outNetworkFilter = memberBucket.getAggregations().get("outNetworkFilter");
                Sum outNetworkTotalPaidAmount = outNetworkFilter.getAggregations().get("totalPaidAmount");

                Record record = getRecord(state, memberBucket.getKey());
                record.recordMap.put("memberId", memberBucket.getKey());
                record.recordMap.put("medicalPaid", totalPaidAmount.getValue() / 100);
                record.recordMap.put("medicalOutNetworkPaid", outNetworkTotalPaidAmount.getValue() / 100);
            }
        }
    }

    private  List<Terms.Bucket> paginate(Terms members, RequestState state) {

        int page=state.request.hasParam("page") ?Integer.parseInt(state.request.get("page")) : 1;
        int pageSize=state.request.hasParam("pageSize") ?Integer.parseInt(state.request.get("pageSize")) : 5;

        int lastIndex=page*pageSize;
        int firstIndex =lastIndex-pageSize;

        int i=0;
        List<Terms.Bucket> sortedMemberList=new LinkedList<>();

        for (Terms.Bucket bucket : members.getBuckets()) {
            Filter outNetworkFilter=bucket.getAggregations().get("outNetworkFilter");
            Sum outNetworkTotalPaidAmount=outNetworkFilter.getAggregations().get("totalPaidAmount");
            if(outNetworkTotalPaidAmount.getValue() > 500000) {
                i++;
                sortedMemberList.add(bucket);
            }
        }

        if(sortedMemberList.size() <= lastIndex) lastIndex = sortedMemberList.size();

        Record record=getRecord(state,"metaInfo");
        record.recordMap.put("totalMembers",i);

        return sortedMemberList.subList(firstIndex,lastIndex);
    }
}
