package zakipointCore.features.network.highClaimantsOON;

import com.configurations.utils.Medical;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 11/17/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class HighOutNetworkMemberBuilder implements SummaryQueryBuilder {


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        builder.add(highClaimantsBuilder(state,client));
        return builder;
    }

    private SearchRequestBuilder highClaimantsBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder = setBuilder(state, client);
        imlementAggregation(builder,state,client);
        return builder;
    }

    private SearchRequestBuilder setBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder= QueryUtils.buildEndSearchWihType(client,state.request, Medical.NAME);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        LevelUtils.setLOAFilter(state,andFilterBuilder);
        FilterUtils.paidServiceDateFilter(state,andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));
        return builder;
    }

    private void imlementAggregation(SearchRequestBuilder builder,RequestState state,Client client) {
        String orderField = state.request.requestParameters.get("order").split(":")[0];
        String orderString = state.request.requestParameters.get("order").split(":")[1];
        String order="totalPaidAmount";
        boolean orderBool=false;

        boolean testResult=testBuilder(state,client);
        state.hits.put("testResult",testResult);

        if(orderField.equalsIgnoreCase("medicalPaid")) order = "totalPaidAmount";
        else {
            if(testResult)order = "outNetworkFilter>totalPaidAmount";
        }

        if(orderString.equalsIgnoreCase("asc")) orderBool=true;

        if(testResult) {
            builder.addAggregation(AggregationBuilders.terms("members").field("intMemberId").size(0).order(Terms.Order.aggregation(order, orderBool))
                    .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT))
                    .subAggregation(AggregationBuilders.filter("outNetworkFilter").filter(FilterBuilders.termFilter("inOutNetworkFlag", "N"))
                            .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT))));
        }
    }

    private boolean testBuilder(RequestState state,Client client) {
        SearchRequestBuilder builder = setBuilder(state, client);
        builder.addAggregation(AggregationBuilders.filter("outNetworkFilter").filter(FilterBuilders.termFilter("inOutNetworkFlag","N"))
                        .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT)));

        SearchResponse response=builder.get();
        Filter outNetworkFilter=response.getAggregations().get("outNetworkFilter");
        Sum outNetworkTotalPaidAmount=outNetworkFilter.getAggregations().get("totalPaidAmount");
        if(outNetworkTotalPaidAmount.getValue()>0) return true;
        else return false;
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
