package zakipointCore.features.network.usage;

import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 11/16/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class NetworkUsageListener extends AbstractSummaryListener {

    private Map<String,String> NETOWRK_INFO=new HashMap<String, String>(){{
        put("Y","inNetwork");
        put("N","outOfNetwork");
        put("U","unknown");
    }};

    @Override
    protected void handleResponse(SearchResponse response) {
        Terms status=response.getAggregations().get("inOutNetworkFlag");

        Record record=getRecord(state,"networkUsage");
        double totalPaidAmount=0.0d;

        for (String s : NETOWRK_INFO.keySet()) {
            Terms.Bucket networkFlags= status.getBucketByKey(s);
            if(networkFlags !=null){
                Sum networkPaidAmount=networkFlags.getAggregations().get("paidAmount");
                totalPaidAmount+=networkPaidAmount.getValue()/100;
                record.recordMap.put(NETOWRK_INFO.get(s),networkPaidAmount.getValue()/100);
            }
            else record.recordMap.put(NETOWRK_INFO.get(s),0);

        }
        record.recordMap.put("totalPaidAmount",totalPaidAmount);
    }

    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {
        JSONObject networkUsage= (JSONObject) jsonObject.get("networkUsage");
        double totalPaidAmount=networkUsage.getDouble("totalPaidAmount");

        for (Object key : NETOWRK_INFO.values()) {
          networkUsage.put("percentOf"+key, CalculatorUtils.calculatePercentage(networkUsage.getDouble(key.toString()),totalPaidAmount));
        }
        return networkUsage;
    }
}
