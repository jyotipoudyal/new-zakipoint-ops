package zakipointCore.features.network.usage;

import com.configurations.utils.Medical;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 11/16/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class NetworkUsageBuilder implements SummaryQueryBuilder {


    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        builder.add(networkBuilder(state,client));
        return builder;
    }


    private SearchRequestBuilder networkBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder=QueryUtils.buildEndSearchWihType(client,state.request, Medical.NAME);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        FilterUtils.paidServiceDateFilter(state,andFilterBuilder);
        LevelUtils.setLOAFilter(state,andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));
        implementAggregation(builder);
        return builder;
    }


    private void implementAggregation(SearchRequestBuilder builder) {
        builder.addAggregation(AggregationBuilders.terms("inOutNetworkFlag").field("inOutNetworkFlag").size(0)
                        .subAggregation(AggregationBuilders.sum("paidAmount").field("paidAmount")));
    }


    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {return null;}
}
