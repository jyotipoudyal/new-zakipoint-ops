package zakipointCore.features.network.networkTrendingSankey;

import com.configurations.utils.Medical;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.drill.ConsecutiveDateGenerator;

import java.util.List;

/**
 * Created by sktamang on 11/28/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class NetworkTrendingSankeyBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
        networkUsageBuilder(state,client,builder);
        return builder;
    }


    private void networkUsageBuilder(RequestState state,Client client,MultiSearchRequestBuilder builder){
        List<RequestState> requestStateList= ConsecutiveDateGenerator.getConsecutiveRequestStates(state.request.requestParameters);

        for (RequestState requestState : requestStateList) {
            SearchRequestBuilder searchRequestBuilder=QueryUtils.buildEndSearchWihType(client,requestState.request, Medical.NAME);
            searchRequestBuilder.setSize(1);

            AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
            FilterUtils.paidServiceDateFilter(requestState,andFilterBuilder);
            LevelUtils.setLOAFilter(requestState,andFilterBuilder);

            searchRequestBuilder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));

            String scriptField="\""+requestState.request.get("reportingPeriod")+"\"";
            searchRequestBuilder.addScriptField("period",scriptField);
            imlementAggregation(searchRequestBuilder);

            builder.add(searchRequestBuilder);
        }
    }

    private void imlementAggregation(SearchRequestBuilder builder) {
        builder.addAggregation(AggregationBuilders.filter("oonFilter").filter(FilterBuilders.termFilter("inOutNetworkFlag","N"))
                .subAggregation(
        AggregationBuilders.terms("members").field("intMemberId").size(0)
                        .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT))));
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
