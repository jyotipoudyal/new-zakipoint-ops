package zakipointCore.features.network.networkTrendingSankey;

import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.*;

/**
 * Created by sktamang on 11/28/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class NetworkTrendingSankeyListener extends AbstractSummaryListener {

    protected Map<Integer, String> YEAR_COUNT_MAP = new HashMap<Integer, String>() {{
        put(1, "current");
        put(2, "last");
        put(3, "prior");
    }};

    protected void processResponse(ActionResponse response) {
        MultiSearchResponse response1 = (MultiSearchResponse) response;
        int i = 1;
        for (MultiSearchResponse.Item item : response1.getResponses()) {
            if (item.isFailure()) {
                System.out.println("the error is = " + item.getFailureMessage());
            } else {
                handleResponse(item.getResponse(), i);
                i++;
            }
        }
    }


    protected void handleResponse(SearchResponse response, int counter) {

        List<String> highCosteMember = new ArrayList<>();
        List<String> mediumCosteMember = new ArrayList<>();
        List<String> lowCosteMember = new ArrayList<>();

        String period = YEAR_COUNT_MAP.get(counter);
        Record record = getRecord(state, period);

        if (response.getHits().getHits().length > 0) {
            Filter oonFilter = response.getAggregations().get("oonFilter");
            Terms members = oonFilter.getAggregations().get("members");


            for (Terms.Bucket bucket : members.getBuckets()) {
                double totalPaidAmount = ((Sum) bucket.getAggregations().get("totalPaidAmount")).getValue() / 100;

                if (totalPaidAmount > 5000) highCosteMember.add(bucket.getKey());

                else if (totalPaidAmount > 1000 && totalPaidAmount <= 5000) mediumCosteMember.add(bucket.getKey());

                else lowCosteMember.add(bucket.getKey());
            }
        }
        record.recordMap.put("high", highCosteMember);
        record.recordMap.put("medium", mediumCosteMember);
        record.recordMap.put("low", lowCosteMember);
    }


    @Override
    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {

        JSONObject current = jsonObject.has("current") == true ? (JSONObject) jsonObject.get("current") : new JSONObject();
        JSONObject last = jsonObject.has("last") == true ? (JSONObject) jsonObject.get("last") : new JSONObject();
        JSONObject prior = jsonObject.has("prior") == true ? (JSONObject) jsonObject.get("prior") : new JSONObject();

        JSONArray linkArray = new JSONArray();
        retainMembers(prior, last, "prior", "last", linkArray);
        retainMembers(last, current, "last", "current", linkArray);

        finalize(jsonObject);
        jsonObject.put("links", linkArray);
        return jsonObject;
    }


    private Map<String, Object> retainMembers(JSONObject initial, JSONObject terminal, String initKey, String termKey, JSONArray linkObject) {

        Map<String, Object> retainInfoMap = new HashMap<>();
        for (Object intialKeys : initial.keySet()) {

            String initialKey = intialKeys.toString();
            List memberListInitial = new ArrayList((List) initial.get(initialKey));

            for (Object finalKeys : terminal.keySet()) {
                String finalKey = finalKeys.toString();
                List memberListTerminal = new ArrayList((List) terminal.get(finalKey));

                memberListTerminal.retainAll(memberListInitial);
                JSONObject tempMap = new JSONObject();

                tempMap.put("source", initKey + "_" + initialKey);
                tempMap.put("target", termKey + "_" + finalKey);
                tempMap.put("value", memberListTerminal.size());
                linkObject.put(tempMap);
            }
        }
        return retainInfoMap;
    }


    protected void finalize(JSONObject jsonObject) {

        List<String> metricList = Arrays.asList("low", "medium", "high");
        List<String> keyList = Arrays.asList("current", "last", "prior");
        JSONArray finalTemp = new JSONArray();

        int i = 0;
        for (String key : keyList) {

            for (String s : metricList) {
                JSONObject tempJson = new JSONObject();

                tempJson.put("node", key + "_" + s);
                tempJson.put("year", key);
                tempJson.put("total", ((List) ((JSONObject) jsonObject.get(key)).get(s)).size());
                tempJson.put("name", s);
                tempJson.put("id", i);
                finalTemp.put(tempJson);
                i++;
            }
            jsonObject.remove(key);
        }
        jsonObject.put("nodes", finalTemp);
    }
}


