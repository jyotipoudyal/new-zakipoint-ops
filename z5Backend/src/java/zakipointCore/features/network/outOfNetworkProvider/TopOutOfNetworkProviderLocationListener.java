package zakipointCore.features.network.outOfNetworkProvider;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 11/21/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class TopOutOfNetworkProviderLocationListener extends TopOutOfNetworkProviderListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        Terms providerState = response.getAggregations().get("providerState");

        for (Terms.Bucket stateBucket : providerState.getBuckets()) {

            Filter inOutNetworkFlagState = stateBucket.getAggregations().get("inOutNetworkFlag");
            double totalPaidAmountState = ((Sum) inOutNetworkFlagState.getAggregations().get("totalPaidAmount")).getValue() / 100;
            if (totalPaidAmountState <= 0) break;

            Record record = getRecord(state, stateBucket.getKey());
            Terms providerCity = stateBucket.getAggregations().get("providerCity");

            String cityName;
            int memberCount;
            int oonProviderCount;
            double totalPaidAmount;
            int totalMemberCount = 0;
            int totalOonProviderCount = 0;
            double totalPaidAmountTotal = 0.0d;

            for (Terms.Bucket cityBucket : providerCity.getBuckets()) {
                Map<String, Object> cityMap = new HashMap<>();
                cityName = cityBucket.getKey();

                Filter inOutNetworkFlag = cityBucket.getAggregations().get("inOutNetworkFlag");
                totalPaidAmount = ((Sum) inOutNetworkFlag.getAggregations().get("totalPaidAmount")).getValue() / 100;
                oonProviderCount = ((Terms) inOutNetworkFlag.getAggregations().get("providerId")).getBuckets().size();

                if (oonProviderCount > 0 && totalPaidAmount > 0) {
                    memberCount = ((Terms) inOutNetworkFlag.getAggregations().get("intMemberId")).getBuckets().size();
                    totalMemberCount += memberCount;
                    totalOonProviderCount += oonProviderCount;
                    totalPaidAmountTotal += totalPaidAmount;

                    cityMap.put("city", cityName);
                    cityMap.put("memberCount", memberCount);
                    cityMap.put("oonProviderCount", oonProviderCount);
                    cityMap.put("totalPaidAmount", totalPaidAmount);
                    cityMap.put("state", stateBucket.getKey());
                    record.recordMap.put(cityName, cityMap);
                }
            }

            if (totalOonProviderCount > 0 && totalPaidAmountTotal > 0) {
                Map<String, Object> totalMap = new HashMap<>();
                totalMap.put("city", "total");
                totalMap.put("memberCount", totalMemberCount);
                totalMap.put("oonProviderCount", totalOonProviderCount);
                totalMap.put("totalPaidAmount", totalPaidAmountTotal);
                totalMap.put("state", stateBucket.getKey());
                record.recordMap.put("total", totalMap);
            } else state.recordIds.remove(stateBucket.getKey());
        }
    }
}
