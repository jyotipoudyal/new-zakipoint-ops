package zakipointCore.features.network.outOfNetworkProvider;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;

/**
 * Created by sktamang on 11/25/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class TopOutOfNetworkProviderSpecialityBuilder extends TopOutOfNetworkProviderBuilder{

    @Override
    protected void imlementAggregation(SearchRequestBuilder builder) {
        builder.addAggregation(termAggregator("providerSpeciality1")
                .subAggregation(termAggregator("providerSpeciality1Desc")
                        .subAggregation(AggregationBuilders.sum("totalPaidAmount").field("paidAmount"))
                        .subAggregation(termAggregator("intMemberId"))
                        .subAggregation(termAggregator("providerId"))));
    }
}
