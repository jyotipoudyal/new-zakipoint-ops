package zakipointCore.features.network.outOfNetworkProvider;

import com.configurations.utils.Medical;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 11/21/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class TopOutOfNetworkProviderBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
            MultiSearchRequestBuilder builder= QueryUtils.buildMultiSearch(client);
            builder.add(topOONProviderBuilder(state,client));
            return builder;
        }

    private SearchRequestBuilder topOONProviderBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder=QueryUtils.buildEndSearchWihType(client,state.request, Medical.NAME);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        FilterUtils.paidServiceDateFilter(state,andFilterBuilder);
        LevelUtils.setLOAFilter(state,andFilterBuilder);
        customFilter(andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));
        imlementAggregation(builder);
        return builder;
    }

    protected void customFilter(AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.termFilter("inOutNetworkFlag","N"));
    }

    protected void imlementAggregation(SearchRequestBuilder builder) {
        builder.addAggregation(AggregationBuilders.terms("providerNetworkName").field("providerFirstName").size(0)
                .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT))
                .subAggregation(AggregationBuilders.terms("members").field("intMemberId").size(0)));
    }

    protected TermsBuilder termAggregator(String aggName) {return AggregationBuilders.terms(aggName).field(aggName).size(0);}

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }
}
