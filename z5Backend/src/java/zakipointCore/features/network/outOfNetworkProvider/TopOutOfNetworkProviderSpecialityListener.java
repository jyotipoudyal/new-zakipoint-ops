package zakipointCore.features.network.outOfNetworkProvider;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.listener.AbstractSummaryListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sktamang on 11/25/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class TopOutOfNetworkProviderSpecialityListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        Terms providerSpeciality1=response.getAggregations().get("providerSpeciality1");

        for (Terms.Bucket specialityCode : providerSpeciality1.getBuckets()) {
            Terms providerSpeciality1Desc= specialityCode.getAggregations().get("providerSpeciality1Desc");

            Record record=getRecord(state,specialityCode.getKey());

            String specialityDescString;
            int memberCount;
            int oonProviderCount;

            int totalMemberCount=0;
            int totalOonProviderCount=0;
            double totalPaidAmountTotal=0.0d;

            for (Terms.Bucket specialityDesc : providerSpeciality1Desc.getBuckets()) {

                double paidAmount;
                specialityDescString=specialityDesc.getKey();

                paidAmount=((Sum)specialityDesc.getAggregations().get("totalPaidAmount")).getValue()/100;
                Map<String,Object> innerMap=new HashMap<>();

                if(paidAmount>0) {
                    memberCount=((Terms) specialityDesc.getAggregations().get("intMemberId")).getBuckets().size();
                    oonProviderCount=((Terms)specialityDesc.getAggregations().get("providerId")).getBuckets().size();

                    innerMap.put("totalPaidAmount", paidAmount);
                    innerMap.put("memberCount", memberCount);
                    innerMap.put("oonProviderCount", oonProviderCount);
                    innerMap.put("specialityCode", specialityCode.getKey());
                    innerMap.put("specialityDesc", specialityDescString);

                    totalMemberCount += memberCount;
                    totalOonProviderCount += oonProviderCount;
                    totalPaidAmountTotal += paidAmount;
                    record.recordMap.put(specialityDescString,innerMap);
                }
            }

            if(totalPaidAmountTotal >0 ) {
                Map<String, Object> totalMap = new HashMap<>();
                totalMap.put("specialityDesc", "total");
                totalMap.put("memberCount", totalMemberCount);
                totalMap.put("oonProviderCount", totalOonProviderCount);
                totalMap.put("totalPaidAmount", totalPaidAmountTotal);
                totalMap.put("specialityCode", specialityCode.getKey());
                record.recordMap.put("total", totalMap);
            }else state.recordIds.remove(specialityCode.getKey());
        }
    }
}
