package zakipointCore.features.network.outOfNetworkProvider;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 11/21/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class TopOutOfNetworkProviderListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {

        Terms providerNetworkName=response.getAggregations().get("providerNetworkName");

        for (Terms.Bucket providers : providerNetworkName.getBuckets()) {
            Record record=getRecord(state,providers.getKey());

            Terms members=providers.getAggregations().get("members");
            Sum totalPaidAmount=providers.getAggregations().get("totalPaidAmount");

            record.recordMap.put("totalMembers",members.getBuckets().size());
            record.recordMap.put("totalPaidAmount",totalPaidAmount.getValue()/100);
            record.recordMap.put("providerName",providers.getKey());
        }
    }
}
