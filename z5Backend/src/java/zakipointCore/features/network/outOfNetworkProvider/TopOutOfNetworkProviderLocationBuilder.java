package zakipointCore.features.network.outOfNetworkProvider;

import com.configurations.utils.Medical;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

/**
 * Created by sktamang on 11/21/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class TopOutOfNetworkProviderLocationBuilder extends TopOutOfNetworkProviderBuilder {

    @Override
    protected void customFilter(AndFilterBuilder andFilterBuilder) {}

    @Override
    protected void imlementAggregation(SearchRequestBuilder builder) {
        builder.addAggregation(termAggregator("providerState").order(Terms.Order.aggregation("inOutNetworkFlag>totalPaidAmount",false))
                .subAggregation(termAggregator("providerCity")
                        .subAggregation(AggregationBuilders.filter("inOutNetworkFlag").filter(FilterBuilders.termFilter("inOutNetworkFlag", "N"))
                                .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT))
                                .subAggregation(termAggregator("intMemberId"))
                                .subAggregation(termAggregator("providerId"))))
                .subAggregation(AggregationBuilders.filter("inOutNetworkFlag").filter(FilterBuilders.termFilter("inOutNetworkFlag", "N"))
                        .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT))));
        System.out.println(builder);
    }

}
