package zakipointCore.features.programTracking;

import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearch;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.SummaryQueryBuilder;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 11/11/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class ProgramTrackingBuilder implements SummaryQueryBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder searchRequestBuilder = QueryUtils.buildMultiSearch(client);
        searchRequestBuilder.add(memberSearchBuilder(state, client));
        return searchRequestBuilder;
    }

    private SearchRequestBuilder memberSearchBuilder(RequestState state, Client client) {
        Map<String, Object> memberProgramList = programTrackingBuilder(state, client);
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);

        FiltersAggregationBuilder filters = AggregationBuilders.filters("memberStatusProgram");
        for (Map.Entry<String, Object> programStatus : memberProgramList.entrySet()) {
            Map<String, List<String>> statusMember = (Map<String, List<String>>) programStatus.getValue();

            for (Map.Entry<String, List<String>> memberListToStatus : statusMember.entrySet()) {
                AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
                andFilterBuilder.add(FilterBuilders.termsFilter("memberId", memberListToStatus.getValue()));
                filters.filter(programStatus.getKey() + "__" + memberListToStatus.getKey(), andFilterBuilder);
            }
        }
        builder.addAggregation(filters);

        FiltersAggregationBuilder memberMonthsFilters= (FiltersAggregationBuilder) CommonQueries.memberMonthsAggs(state,"memberMonths");
        filters.subAggregation(memberMonthsFilters);
        return builder;
    }

    @Override
    public SearchRequestBuilder query(RequestState state, Client client) {
        return null;
    }

    public Map<String, Object> programTrackingBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, "Z5MemberByProgram");

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.rangeFilter("programEnrollmentDate").gte(state.periodFrom()).lte(state.periodTo()));

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        builder.addAggregation(AggregationBuilders.terms("programType").field("programType").size(0)
                .subAggregation(AggregationBuilders.terms("mostRecentStatus").field("mostRecentStatus").size(0)
                        .subAggregation(AggregationBuilders.terms("memberId").field("memberId").size(0))));

        return extractProgramsDetail(builder);

    }

    private Map<String, Object> extractProgramsDetail(SearchRequestBuilder builder) {

        SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "programTracking");

        Terms programType = response.getAggregations().get("programType");
        Map<String, Object> programToStatus = new HashMap<>();
        for (Terms.Bucket programs : programType.getBuckets()) {

            Map<String, List<String>> statusToMember = new HashMap<>();
            Terms mostRecentStatus = programs.getAggregations().get("mostRecentStatus");
            for (Terms.Bucket status : mostRecentStatus.getBuckets()) {
                Terms members = status.getAggregations().get("memberId");
                List<String> memberList = new ArrayList<>();
                for (Terms.Bucket memberId : members.getBuckets()) {
                    memberList.add(memberId.getKey());
                }
                statusToMember.put(status.getKey(), memberList);
            }
            programToStatus.put(programs.getKey(), statusToMember);
        }
        return programToStatus;
    }
}
