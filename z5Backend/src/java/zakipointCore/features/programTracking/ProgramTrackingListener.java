package zakipointCore.features.programTracking;

import org.elasticsearch.action.search.SearchResponse;
import zakipointCore.listener.AbstractSummaryListener;

/**
 * Created by sktamang on 11/11/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class ProgramTrackingListener extends AbstractSummaryListener {

    @Override
    protected void handleResponse(SearchResponse response) {
        System.out.println("inside the listener");
    }
}
