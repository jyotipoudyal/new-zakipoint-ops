package zakipointCore.features.gapInCare;

import com.configurations.utils.QmUtils;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.features.zPopulation.recommendedCareForChronicPatients.RecommendedCareForChronicBuilder;

import java.util.Map;

/**
 * Created by sktamang on 11/15/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class GapInCareBuilder extends RecommendedCareForChronicBuilder {

    @Override
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client) {
        MultiSearchRequestBuilder builder = QueryUtils.buildMultiSearch(client);
        initChronicInfo();
        state.hits.put("chronicInfoMap",CHRONIC_INFO);

        for (Map.Entry<Integer, Map<String, String>> integerMapEntry : CHRONIC_INFO.entrySet()) {
            builder.add(memberSearchBuilder(state,client,integerMapEntry.getKey()));
        }
        return builder;
    }


    @Override
    protected void metaInfoBuilder(RequestState state, int metricCode, SearchRequestBuilder builder, boolean addMetaInfoAggregator) {}


    @Override
    protected void initChronicInfo() {
        super.initChronicInfo();
        CHRONIC_INFO.put(1026, QmUtils.ADDITIONAL_GAPS);
        CHRONIC_INFO.put(1027,QmUtils.PREGNANCY);
        CHRONIC_INFO.put(1028,QmUtils.UTILIZATION);
        CHRONIC_INFO.put(1029,QmUtils.WELLNESS);
        CHRONIC_INFO.put(1030,QmUtils.RHEUMATOID_ARTHRITIS);
    }

}
