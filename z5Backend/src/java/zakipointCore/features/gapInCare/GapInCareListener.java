package zakipointCore.features.gapInCare;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import zakipointCore.features.zPopulation.recommendedCareForChronicPatients.RecommendedCareForChronicListener;

/**
 * Created by sktamang on 11/15/16.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class GapInCareListener extends RecommendedCareForChronicListener {

    @Override
    protected void handleResponse(SearchResponse response) {


        String metricCodeArray[]=response.getAggregations().asList().get(0).getName().split("_");
        String metricCode =metricCodeArray.length >2 ? metricCodeArray[2] : metricCodeArray [1];
        for (Aggregation aggregation : response.getAggregations()) {
            if (aggregation.getName().contains("memberMonths")) handleMemberMonths(aggregation);
        }

        Filters filters = response.getAggregations().get("filters"+"_"+metricCode);
        Filters memberAmountCostFilters = response.getAggregations().get("memberAmountCostFilters"+"_"+metricCode);

        extractMetricWisePopulation(filters,metricCode);
        extractMemberAmount(memberAmountCostFilters);
    }
}
