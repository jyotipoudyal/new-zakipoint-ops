package zakipointCore.features.gapInCare

import com.qm.MembersByQualityMetrics
import com.qm.QualityMetrics
import org.codehaus.groovy.grails.web.json.JSONObject
import zakipointCore.builder.ReportFromDomainBuilders
import zakipointCore.commonUtils.CalculatorUtils
import zakipointCore.commonUtils.RequestUtils

/**
 * Created by sktamang on 3/2/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class GapInCareRealtimeBuilder implements ReportFromDomainBuilders{

    static List<String> LOA_LIST_COHORT_COMPARATOR=Arrays.asList("groupId","divisionId");
    static List<String> LOA_LIST_DRILL=Arrays.asList("qmName","groupId","criteriaMetFlag","qmId");
    static List<String> LOA_LIST_DRILL_INTEGER_VALUES=Arrays.asList("criteriaMetFlag");

    Map<String,Object> keyMap=new HashMap<String,Object>(){{
        put("notMeeting","criteriaNotMetCount");
        put("totalPaidAmount","");
        put("conditionWiseMemberMonth","");
        put("met","criteriaMetCount");
        put("category","category");
        put("metricType","");
        put("membersInGroup","memberCount");
        put("description","qmName");
        put("costPmpy","");
        put("qmId","qmId");
    }}

    Map<String,String> qmMap=new HashMap<String,String>(){{
        put("LDL<100mg/dL","diabetes.9");
        put("LDL<130mg/dL","diabetes.10");
        put("BP<130/80 mmHg","diabetes.11");
        put("BP<140/90 mmHg","diabetes.12");
        put("HbA1c<7.0%","diabetes.13");
        put("HbA1c<8.0%","diabetes.14");
        put("HbA1c<9.0%","diabetes.507");
    }}

    Map<String,String> qmToCode=new HashMap<String,String>(){{
        put("diabetes.9","99");
        put("diabetes.10","100");
        put("diabetes.11","101");
        put("diabetes.12","102");
        put("diabetes.13","103");
        put("diabetes.14","104");
        put("diabetes.507","507");
    }}


    @Override
    JSONObject getResults(Map<String,String> requestMap) {

        def Map<String,String> loaMap=[:]

        populateLoaMap(requestMap, loaMap,LOA_LIST_COHORT_COMPARATOR)
        def c = QualityMetrics.createCriteria()
        def results = c.list {
            loaMap.each {k,v->
                eq k, v
            }
        }

        JSONObject jsonObject=new JSONObject()

        for (QualityMetrics qm : results) {
            Map<String,Object> data= qm.toMap()

            JSONObject obj=new JSONObject();

            for (Map.Entry < String, Object > pair: keyMap.entrySet()) {
                if(data.containsKey(pair.getValue())) obj.put(pair.getKey(),data.get(pair.getValue()))
                else obj.put(pair.getKey(),"");
            }

            obj.put("percentMet",CalculatorUtils.calculatePercentage(Double.valueOf(obj.get("met")),Double.valueOf(obj.get("membersInGroup"))))
            obj.put("percentNotMeeting",CalculatorUtils.calculatePercentage(Double.valueOf(obj.get("notMeeting")),Double.valueOf(obj.get("membersInGroup"))))
            String name=qmMap.get(data.qmName)
            obj.put("name",name)
            obj.put("code",qmToCode.get(name))

            jsonObject.put(qmToCode.get(name),obj)
        }

        return jsonObject;
    }

    public void populateLoaMap(Map<String, String> requestMap, LinkedHashMap<String, Object> loaMap, List<String> cohortList) {
        for (String s : cohortList) {
            if (requestMap.containsKey(s)) {
                Object value=requestMap.get(s);
                if(LOA_LIST_DRILL_INTEGER_VALUES.contains(s)) value=Integer.parseInt(requestMap.get(s))
                String key=s;
                if(s.contains("group")) {
//                        key=key+"Id"
                        value=RequestUtils.getArrayRequestValue(requestMap.get(key))
                }
                loaMap.put(key, value)
            }
        }
    }

    @Override
    Object getDrillResults(Map<String, String> requestParams) {
        def Map<String,Object> loaMap=[:]

        populateLoaMap(requestParams, loaMap,LOA_LIST_DRILL)
        def c = MembersByQualityMetrics.createCriteria()
        def results = c.list {
            loaMap.each {k,v->
                eq k, v
            }

            projections {
                property('memberId')
            }
        }

        return results;
    }
}
