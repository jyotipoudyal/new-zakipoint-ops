package zakipointCore.renderers;

import com.configurations.utils.NestedFieldsMapping;
import com.configurations.utils.NestedService;
import com.configurations.utils.NewQueryParser;
import com.configurations.utils.Query;
import grails.converters.JSON;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONElement;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.*;

/**
 * Created by sktamang on 7/19/16.
 */
public class MemberStatusZphRenderer implements NestedService {

    String memberStatusQuery="{\n" +
            "      \"and\": [\n" +
            "        {\n" +
            "          \"statusEligibilityType.eq\": \"[medical]\"\n" +
            "        }\n" +
            "      ]\n" +
            "    }";

    @Override
    public Map<String, Object> process(Object obj, Map<String, String> requestParameters) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    public List<HashMap<String, Object>> processPharmacy(Object obj, Map<String, String> requestParameters) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<String, Object> calculate(String fieldName, Object obj, Map<String, String> requestParameters) {

        JSONObject memberStausQueryObject;
        memberStausQueryObject=new JSONObject(memberStatusQuery);
        String tableCode = requestParameters.get("table");
        Map<String, Object> globalLoaMap = Collections.EMPTY_MAP;
        Set<String> status = Collections.EMPTY_SET;
        JSONElement jsonElement= JSON.parse(obj.toString());
        List<Map<String, Object>> loaObj = (List<Map<String, Object>>) jsonElement;

        Map<String, Object> matchedLoaMap = new HashMap<>();
        JSONObject query=populateDates(memberStausQueryObject,requestParameters);
        Query loaQuery = new NewQueryParser(tableCode, NestedFieldsMapping.loaFields.get(fieldName)).getQuery(query);

        for (Map<String, Object> loaMap : loaObj) {
            matchedLoaMap.clear();
            populateMap(fieldName, matchedLoaMap, loaMap);
            if (loaQuery == null ) {
                return matchedLoaMap;
            } else {
                if (loaQuery.evaluate(matchedLoaMap)) {
                    String statusFieldName = fieldName + ".status";
                    if (query.toString().contains("currentStatus")) {
                        return matchedLoaMap;
                    }
                    globalLoaMap = matchedLoaMap;
                    if (!matchedLoaMap.get(statusFieldName).equals("Active")) {
                        continue;
                    } else
                        return matchedLoaMap;

                } else {
                    matchedLoaMap.put("memberStatus.status", "Termed");
                    globalLoaMap = matchedLoaMap;
                }
            }


        }
        return globalLoaMap;
    }

    private JSONObject populateDates(JSONObject memberStausQueryObject,Map<String, String> requestParameters) {
        String reportingToDate=requestParameters.get("reportingTo");

        JSONObject statusToDate=new JSONObject().put("statusToDate.gte",reportingToDate);
        JSONObject statusFromDate=new JSONObject().put("statusFromDate.lte",reportingToDate);

        JSONArray and=memberStausQueryObject.getJSONArray("and");

        and.add(statusFromDate);
        and.add(statusToDate);

        return memberStausQueryObject.put("and",and);
    }

    private Map<String, Object> getFirstEntry(String fieldName, List<Map<String, Object>> loaObj) {
        if(loaObj.size()==0)
            return Collections.EMPTY_MAP;

        Map<String, Object> map = new HashMap<String, Object>();
        populateMap(fieldName, map, loaObj.get(0));
        return map;
    }
    private void populateMap(String nestedPath, Map<String, Object> matchedLoaMap, Map<String, Object> loaMap) {
        for (String key : loaMap.keySet()) {
            matchedLoaMap.put(nestedPath + "." + key, loaMap.get(key));
        }
    }
}
