package zakipointCore.renderers;

import com.configurations.config.FieldConfig;
import com.configurations.config.FieldConfigurations;
import com.configurations.search.MemberSearchRequest;
import com.configurations.utils.*;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.*;

import java.util.*;

/**
 * Created by sktamang on 6/20/16.
 */
public class MemberSearchZphRenderer implements ResponseRenderer {
    public static MemberSearchZphRenderer INSTANCE = new MemberSearchZphRenderer();

    public JSONObject renderResults(SearchHit[] hits, MemberSearchRequest searchRequest) {
        double pmpm=customFieldAddition(searchRequest);
        JSONObject returnJSON = new JSONObject();
        if(!searchRequest.requestParameters.get("requestIndex").equalsIgnoreCase("1") || searchRequest.requestParameters.containsKey("drillReport")) return returnJSON;
             {
            int counter = 0;
            // TODO: Remove this hard-coded QM flag which is only used for metric compliance. Rather make a generic method which would parse the query and make the decision for further calculation.
            Boolean qmFlag = true;
            String tableCode = searchRequest.requestParameters.get("table");
            for (SearchHit hit : hits) {
                final Map<String, Object> fields = hit.getSource();


                JSONObject innerJSON = handleAHit(searchRequest, qmFlag, tableCode, hit,pmpm);
                returnJSON.accumulate(Integer.toString(counter), innerJSON);
                counter += 1;
            }
        }
        return returnJSON;
    }

    private double customFieldAddition(MemberSearchRequest searchRequest1) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.requestParameters = new HashMap<>(searchRequest1.requestParameters);
        Client client = MemberSearchQueryBuilder.getClient(searchRequest.requestParameters);

        RequestState state = new RequestState("predictiveCost", "", searchRequest, null);
        double memberMonths=0.0d;
        memberMonths= CommonQueries.getTotalMemberMonth(state,client,memberMonths);

        SearchRequestBuilder builder1= QueryUtils.buildEndSearchWihType(client,state.request, MemberSearch.NAME);
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        LevelUtils.setLevelFilter(state,andFilterBuilder,searchRequest.requestParameters.get("periodTo"));
        andFilterBuilder.add(FilterUtils.eligibleDateFilter(state.periodTo()));
        builder1.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        CommonQueries.paidMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        CommonQueries.serviceMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);

        builder1.addAggregation(AggregationBuilders.nested("memberAmountNested").path(MemberSearch.MEMBER_AMOUNT.NAME)
                .subAggregation(AggregationBuilders.filter("memberAmountFilter").filter(memberAmountFilter)
                        .subAggregation(AggregationBuilders.sum("totalMemberPaidAmount").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT))));

        SearchResponse response= (SearchResponse) BuilderAction.getResponse(builder1,"predictiveCost");
        Nested memberAmountNested = response.getAggregations().get("memberAmountNested");
        Filter memberAmountFilterBuc = memberAmountNested.getAggregations().get("memberAmountFilter");
        Sum totalMemberPaidAmount = memberAmountFilterBuc.getAggregations().get("totalMemberPaidAmount");
        double pmpm= CalculatorUtils.divide(totalMemberPaidAmount.getValue(),memberMonths);
        return pmpm*12;
    }

    public JSONObject handleAHit(MemberSearchRequest searchRequest, Boolean qmFlag, String tableCode, SearchHit hit,double pmpm) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        JSONObject innerJSON = new JSONObject();
        if(searchRequest.requestParameters.containsKey("showId")&&searchRequest.requestParameters.get("showId").equals("true"))innerJSON.accumulate("_docId", hit.getId()); // only used for IM... (YALA)
        final Map<String, Object> fields = hit.getSource();
        List<String> chronicConditionList=new ArrayList<>();
        for (String fieldName : fields.keySet()) {
            Object value1=fields.get(fieldName);
            if(fieldName.equalsIgnoreCase("prospectiveTotal")){
                double a=((Integer)value1).doubleValue();
                innerJSON.accumulate("predictiveCost",a/100 * pmpm);
            }

            if(fieldName.contains("has") && value1.equals("Yes")) {
                String metric= ChronicConditionEnum.ChronicConditionMetrics.getMetricByHasCondition(fieldName).getMetricDisplay();
                chronicConditionList.add(metric);
            }
            if(value1==null){
                continue;
            }
            FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCode).fromFieldName(fieldName);
            if (fieldConfig == null){
                continue;
            }
            if (fieldConfig.type.equalsIgnoreCase("nested")) {
                Map<String, Object> map = new HashMap<String, Object>();
                if (fieldName.equals("qm") && qmFlag) // this is for QM drill down only. If true, instantiates the "MetricCompliance" class otherwise not.
                    map = NestedServices.getInstance(fieldConfig.displayClass).calculate(fieldName, value1, searchRequest.requestParameters);
                else if (!fieldName.equals("qm")){
                    if (fieldConfig.displayClass != null) {
                        map = NestedServices.getInstance(fieldConfig.displayClass).calculate(fieldName, value1, searchRequest.requestParameters);
                    }
                }
                for (String field : map.keySet()) {
                    resultMap.put(field, map.get(field));
                }

            }else {
                Object value = DataConvertionUtils.getValue(fieldConfig, value1, DataConvertionUtils.ConversionType.ES_TO_DISPLAY);
                innerJSON.accumulate(fieldConfig.displayName, value);
                }
        }

        innerJSON.accumulate("chronicConditionList",chronicConditionList);

        if (!resultMap.isEmpty()) {
            for (String field : resultMap.keySet()) {
                FieldConfig config = FieldConfigurations.fromTableCode(tableCode).fromFieldName(field);
                if(config==null)continue;
                if (innerJSON.containsKey(config.displayName))
                    innerJSON.remove(config.displayName);
                Object value = DataConvertionUtils.getValue(config, resultMap.get(field), DataConvertionUtils.ConversionType.ES_TO_DISPLAY);
                innerJSON.accumulate(config.displayName, value);
            }
        }
        return innerJSON;
    }
}
