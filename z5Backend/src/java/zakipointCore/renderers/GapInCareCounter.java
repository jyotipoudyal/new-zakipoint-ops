package zakipointCore.renderers;

import com.configurations.utils.DateConversionUtility;
import com.configurations.utils.NestedService;
import com.configurations.utils.QmUtils;

import java.util.*;

/**
 * Created by sktamang on 6/6/16.
 */
public class GapInCareCounter implements NestedService {

    public static GapInCareCounter INSTANCE = new GapInCareCounter() {
    };

    @Override
    public Map<String, Object> calculate(String fieldName, Object obj, Map<String, String> requestParameters) {
        return getMetricCompliance(obj, requestParameters, fieldName);
    }


    private static Map<String, Object> getMetricCompliance(Object obj, Map<String, String> requestParameters, String fieldName) {
        List<HashMap<String, Object>> qmArray = (List<HashMap<String, Object>>) obj;
        Map<String, Object> compliance = new HashMap<String, Object>();

        String reportingFrom = requestParameters.get("reportingFrom");
        String reportingTo = requestParameters.get("reportingTo");

        long fromDate = DateConversionUtility.getTimeFromDateWrtTimeZone(reportingFrom);
        long toDate = DateConversionUtility.getTimeFromDateWrtTimeZone(reportingTo);

        int careGapCount = 0;
        List<String> qmList=new ArrayList<>();

        for (HashMap entry : qmArray) {
            long qmFromDate = Long.valueOf(entry.get("fromDate").toString());
            long qmToDate = Long.valueOf(entry.get("toDate").toString());
            int numerator = Integer.valueOf(entry.get("numerator").toString());
            String measure = String.valueOf(entry.get("measure"));

            if (qmFromDate > fromDate && qmFromDate < toDate || qmToDate > fromDate && qmToDate < toDate ) {
                if (QmUtils.QM_CODES_MAP.keySet().contains(measure)) {
                    String[] metricInfo = QmUtils.QM_CODES_MAP.get(measure).split("__");
                    int posOrNeg = Integer.valueOf(metricInfo[1]);

                    if (numerator == 0 && posOrNeg == 1 && !qmList.contains(QmUtils.QM_CODES_MAP.get(measure).split("__")[0])) {
                            careGapCount++;
                            qmList.add(QmUtils.QM_CODES_MAP.get(measure).split("__")[0]);
                    }
                    else if (numerator == 1 && posOrNeg == 0 && !qmList.contains(QmUtils.QM_CODES_MAP.get(measure).split("__")[0])) {
                        careGapCount++;
                        qmList.add(QmUtils.QM_CODES_MAP.get(measure).split("__")[0]);

                    }
                }
            }
        }

        compliance.put("gapInCareCount", careGapCount);
        compliance.put("gapInCareList", qmList);
        return compliance;
    }


    @Override
    public Map<String, Object> process(Object obj, Map<String, String> requestParameters) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<HashMap<String, Object>> processPharmacy(Object obj, Map<String, String> requestParameters) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
