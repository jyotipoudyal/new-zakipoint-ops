package zakipointCore.listener;

import com.configurations.XContentWriter;
import grails.converters.JSON;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.RequestUtils;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 9/24/15.
 */
public class AbstractSummaryListener implements ActionListener<ActionResponse> {

    public AtomicBoolean processComplete;

    /**
     * Logger propagated down so as to write correctly to configured ES log
     */
    protected ESLogger logger;
    /**
     * Current state of request in this listener
     */
    public RequestState state;
    /**
     * report requested. binds to requestparam: report
     */
    protected String report;
    /**
     * client for ES command execution
     */
    protected Client client;

    public String Id;

    /**
     * Default constructor  to be used in factory
     */
    public AbstractSummaryListener() {
    }

    public AbstractSummaryListener(ESLogger logger, RequestState state, Client client, AtomicBoolean processComplete) {
        this.logger = logger;
        this.state = state;
        this.client = client;
        this.processComplete = processComplete;
    }


    public void onResponse(ActionResponse response) {
        long first = new Date().getTime();
        logger.debug("querying completed at " + (first - state.time));
        // logger.info("Response-->"+response);
        logger.debug("response parsing started after " + first);
        try {
            // logger.debug("total hits " + response.getHits().getTotalHits());
            if (state.reportCount == 0 && state.repeat) {
                initState();
            }
            processResponse(response);
            logger.debug("response parsing completed after " + ((new Date()).getTime() - first));
            preProcessContentBuilder();
        } catch (Exception e) {
            onFailure(e);
            processComplete.set(true);
            logger.error("errr " + e.getMessage());
        }
    }



    protected void processResponse(ActionResponse response) {
        MultiSearchResponse response1 = (MultiSearchResponse) response;
        for (MultiSearchResponse.Item item : response1.getResponses()) {
            if (item.isFailure()) {
                System.out.println("the error is = " + item.getFailureMessage());
            } else handleResponse(item.getResponse());
        }
    }

    protected void handleResponse(SearchResponse response) {
    }

    protected void initState() {
        try {
            state.recordIds = createRecordMap();
            state.contentBuilder = XContentFactory.jsonBuilder();
        } catch (Exception e) {
            onFailure(e);
        }
    }

    protected void preProcessContentBuilder() {
        try {
            //defaults to writing content
            XContentBuilder contentBuilder = state.contentBuilder.startObject();
            writeContent(state.recordIds, contentBuilder, RequestUtils.REPORTING);
            contentBuilder.endObject();
            postProcessContentBuilder(contentBuilder);
            processComplete.set(true);
            //state.channel.sendResponse(new XContentRestResponse(state.request, OK, state.contentBuilder));
        } catch (Exception e) {
            processComplete.set(true);
            logger.error("error in writing " + e.getMessage());
            onFailure(e);
        }
    }

    protected void postProcessContentBuilder(XContentBuilder builder){
        JSONObject jsonObject=(JSONObject) JSON.parse(builder.bytes().toUtf8());
        finalResponseGenerator((JSONObject)jsonObject.get("reporting"));
        state.response= jsonObject;
    }

    protected JSONObject finalResponseGenerator(JSONObject jsonObject) {
        return jsonObject;
    }

    protected void writeContent(Map<String, Record> memberIds, XContentBuilder contentBuilder, String period) throws Exception {
        contentBuilder.startObject(period);
        for (Record record : memberIds.values()) {
            Record wrapper = record;
            wrapper.toXContent(contentBuilder, XContentWriter.EMPTY_PARAMS);
        }
        contentBuilder.endObject();
    }

    public static Map<String, Record> createRecordMap() {
        return new LinkedHashMap<String, Record>();

    }

    @Override
    public void onFailure(Throwable throwable) {
        logger.error(throwable.getMessage());
        processComplete.set(true);
    }

    public static class Record {
        public String id;
        public Map<String, Object> recordMap = new HashMap<String, Object>();

        public Record(String id) {
            this.id = id;
        }

        public boolean isWriteable() {
            return false;
        }

        public void toXContent(XContentBuilder builder, XContentWriter.Params params) throws IOException {
            builder.startObject(id);
            for (Map.Entry<String, Object> pair : recordMap.entrySet()) {
                builder.field(pair.getKey(), pair.getValue());
            }
            addFurtherFields(builder);
        }

        protected void addFurtherFields(XContentBuilder builder) throws IOException {
                builder.endObject();
            }
        }


    protected Record getRecord(RequestState state, String id) {
        if (!state.recordIds.containsKey(id)) {
            Record wrapper = new Record(id);
            state.recordIds.put(id, wrapper);
        }
        return state.recordIds.get(id);
    }
    public void setState(RequestState state) {
        this.state = state;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setLogger(ESLogger logger) {
        this.logger = logger;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public ESLogger getLogger() {
        return logger;
    }

    public RequestState getState() {
        return state;
    }

    public String getReport() {
        return report;
    }

    public Client getClient() {
        return client;
    }
}
