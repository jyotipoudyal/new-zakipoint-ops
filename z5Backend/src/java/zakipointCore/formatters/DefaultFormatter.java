package zakipointCore.formatters;

import com.configurations.ResponseFormatter;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.util.Map;

public class DefaultFormatter implements ResponseFormatter {
    @Override
    public JSONObject format(JSONObject dasResponse, Map<String, String> requestMap) {
        return dasResponse;
    }
}
