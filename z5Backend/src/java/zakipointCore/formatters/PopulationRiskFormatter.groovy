package zakipointCore.formatters

import com.configurations.ResponseFormatter
import org.codehaus.groovy.grails.web.json.JSONObject
import zakipointCore.commonUtils.CalculatorUtils

/**
 * Created by sktamang on 10/9/15.
 */
class PopulationRiskFormatter implements ResponseFormatter {

    List<String> metrics = Arrays.asList("lowRisk", "highRisk", "normalRisk", "averageRisk")
    List<String> nonFormattedMetric = Arrays.asList("memberCount", "bottomSD", "totalSD")
    JSONObject arrayFormatted = new JSONObject();
    JSONObject finalJSON = new JSONObject();

    @Override
    JSONObject format(JSONObject dasResponse, Map<String, String> requestMap) {
        dasResponse.each { String period, JSONObject values ->
            values.each { String key, JSONObject it ->
                JSONObject formattedObject = calculate(it)
                arrayFormatted.put(key, formattedObject)
            }
            finalJSON.put(period, arrayFormatted)
        }
        return finalJSON
    }

    def calculate(JSONObject jsonObject) {
        JSONObject jsonObject1 = new JSONObject()
        def memberCount = jsonObject.memberCount
        for (String s : metrics) {
            jsonObject1.put(s, (CalculatorUtils.divide(jsonObject.get(s),memberCount)) * 100)
        }
        for (String s : nonFormattedMetric) {
            jsonObject1.put(s, jsonObject.get(s))
        }
        return jsonObject1
    }
}
