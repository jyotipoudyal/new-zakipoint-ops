package zakipointCore.drill;


import zakipointCore.commonUtils.DateUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by sktamang on 6/23/16.
 */
public class ConsecutiveDateGenerator {

//    public static void main(String[] args){
////        getDate("03-28-2017");
//        getThreeOrOneMonthsBackDate("01-28-2017",true);
//    }

    public static String getThreeOrOneMonthsBackDate(String phicEndDate,boolean threeOrOne){

        int value=threeOrOne?-3:-1;
        Calendar reportingToCurrent = DateUtils.getCalendar(phicEndDate);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        reportingToCurrent.add(Calendar.MONTH,value);
        int lastDayOfThisMonth=reportingToCurrent.getActualMaximum(Calendar.DAY_OF_MONTH);
        reportingToCurrent.set(Calendar.DAY_OF_MONTH,lastDayOfThisMonth);
        String toDate = formatter.format(reportingToCurrent.getTime());
        return toDate;
    }

    public static Map<String,String> getDate(String phicEndDate){

        Calendar reportingToCurrent = DateUtils.getCalendar(phicEndDate);
        Map<String,String> dateGroups=new HashMap<>();

        int priorYear = reportingToCurrent.get(Calendar.YEAR) -3 ;
        int counter=1;

        getDateGroups(dateGroups,phicEndDate,priorYear,counter);
        return dateGroups;
    }


    private static Map<String,String> getDateGroups(Map<String, String> dateGroups, String phicEndDate, int priorYear,int counter) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Calendar reportingTo = DateUtils.getCalendar(phicEndDate);


        if(DateUtils.isLeapYear(reportingTo.get(Calendar.YEAR)) && reportingTo.get(Calendar.MONTH) ==1 ){
            reportingTo.set(Calendar.DAY_OF_MONTH,29);
        }
        int previousYear=reportingTo.get(Calendar.YEAR)-1;
        String toDate = formatter.format(reportingTo.getTime());

        Calendar reportingFromDate = getReportingFromDate(phicEndDate);
        String fromDate = formatter.format(reportingFromDate.getTime());

        dateGroups.put(ZphMemberDrillGenerator.reportingPeriodMap.get(String.valueOf(counter)),fromDate+"__"+toDate);
        counter++;

        if(previousYear == priorYear){
            return  dateGroups;
        }
        else {
            reportingTo.add(reportingTo.YEAR,-1);
            String toDate1=formatter.format(reportingTo.getTime());
            getDateGroups(dateGroups, toDate1, priorYear,counter);
        }
        return dateGroups;
    }


    private static Calendar getReportingFromDate(String phicEndDate) {

        Calendar reportingFromDate= DateUtils.getCalendar(phicEndDate);
        int dayOfThisMonth=reportingFromDate.get(Calendar.DAY_OF_MONTH);
        int lastDayOfThisMonth=reportingFromDate.getActualMaximum(Calendar.DAY_OF_MONTH);

        if(dayOfThisMonth != lastDayOfThisMonth) {
            if(DateUtils.isLeapYear(reportingFromDate.get(Calendar.YEAR)) )reportingFromDate.add(reportingFromDate.DAY_OF_MONTH, +2);
            else reportingFromDate.add(reportingFromDate.DAY_OF_MONTH, +1);
        }

        else {
            reportingFromDate.set(Calendar.DAY_OF_MONTH, +1);
            reportingFromDate.add(reportingFromDate.MONTH, +1);
        }

        reportingFromDate.add(reportingFromDate.YEAR, -1);
        return reportingFromDate;
    }


    public static List<RequestState> getConsecutiveRequestStates(Map<String, String> requestParams){
        List<RequestState> consecutiveReqeustStates=new ArrayList<>();
        Map<String,String> datePair=getDate(requestParams.get("reportingTo"));

        for (Map.Entry<String, String> dates : datePair.entrySet()) {
                SearchRequest request = new SearchRequest();
                request.requestParameters = new HashMap<>(requestParams);

                String fromDate = dates.getValue().split("__")[0];
                String toDate = dates.getValue().split("__")[1];

                request.requestParameters.put("reportingFrom", fromDate);
                request.requestParameters.put("reportingTo", toDate);
                request.requestParameters.put("reportingPeriod", dates.getKey());

                RequestState state = new RequestState("networkTrendingSanky", "", request, null);
                consecutiveReqeustStates.add(state);
        }

        return  consecutiveReqeustStates;

    }

}
