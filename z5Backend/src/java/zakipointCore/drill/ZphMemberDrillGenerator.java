package zakipointCore.drill;

import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearchQueryBuilder;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.ReportFromDomainBuilders;
import zakipointCore.builder.ZphSpecificMemberDrillExecutor;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;
import zakipointCore.commonUtils.SpringBeanUtils;
import zakipointCore.drill.drillReport.DrillBuilder;
import zakipointCore.drill.filters.zphMemberDrillBuilderListener.DomainBasedMetrics;
import zakipointCore.drill.filters.zphMemberDrillBuilderListener.LoaWiseMemberMonthsBuilderListener;
import zakipointCore.drill.filters.zphMemberDrillBuilderListener.MemberMonthsBuilderListener;
import zakipointCore.drill.filters.zphMemberDrillBuilderListener.PopulationRiskLevelFromDomainBuilderListener;

/**
 * Created by sktamang on 6/22/16.
 */
public class ZphMemberDrillGenerator {

    public static final Map<String, String> reportingPeriodMap = new HashMap<String, String>() {{
        put("1", "current");
        put("2", "last");
        put("3", "prior");
    }};


    public static JSONObject checkIsZphClient(Map<String, String> requestParams, SearchRequestBuilder builder, JSONObject finalJSON) {


        Map<String, Object> CLASS_MAP = new HashMap<>();
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.requestParameters = new HashMap<>(requestParams);
        if (requestParams.containsKey("isZakipoint")) {

            String resultIndex = requestParams.get("requestIndex");
            if (resultIndex.equalsIgnoreCase("1")) return finalJSON;
            extractMembers(builder, finalJSON,requestParams);
            List<String> memberList = (List<String>) finalJSON.get("intMembers");

            if (!searchRequest.requestParameters.containsKey("drillReport")) {
                Map<String, String> dateGroup = ConsecutiveDateGenerator.getDate(searchRequest.requestParameters.get("phiCEDate"));
                defaultRendering(requestParams, builder, finalJSON, CLASS_MAP, searchRequest, memberList, dateGroup, resultIndex);
            }
            else {
                finalJSON.remove("result_sets");
                String report = searchRequest.requestParameters.get("drillReport");
                RequestState state = new RequestState(report, "", searchRequest, null);
                DrillBuilder drillBuilder = (DrillBuilder) DrillBuilder.getBuilderClass(report);
                finalJSON.put(report, drillBuilder.execute(state, memberList));
            }

            finalJSON.remove("intMembers");
        }
        return finalJSON;
    }


    private static void defaultRendering(Map<String, String> requestParams, SearchRequestBuilder builder, JSONObject finalJSON, Map<String, Object> CLASS_MAP, SearchRequest searchRequest, List<String> memberList, Map<String, String> dateGroup, String requestIndex) {

        init(CLASS_MAP, searchRequest, memberList, finalJSON, dateGroup, requestIndex);
        featureSpecificDrill(CLASS_MAP);
        finalJSON.put("date", dateGroup);

        List<String> demographicList=Arrays.asList("101","103","104");

        if (demographicList.contains(requestIndex)) {
            ZphMemberDrillDemographicBuilderListener zphSpecificMemberDrill = new ZphMemberDrillDemographicBuilderListener(requestIndex);
            zphSpecificMemberDrill.constructDrill(requestParams, builder, finalJSON);
        }
        else if (requestIndex.equalsIgnoreCase("102")) {
            LoaWiseMemberMonthsBuilderListener.createLoaBuilder(builder,finalJSON,requestParams);
        }

        else if(requestIndex.equalsIgnoreCase("8")){
            DomainBasedMetrics.getResults(builder,finalJSON,requestParams);
        }

        else if(requestIndex.equalsIgnoreCase("9")){
            PopulationRiskLevelFromDomainBuilderListener populationRiskLevelFromDomainBuilderListener=new PopulationRiskLevelFromDomainBuilderListener();
            populationRiskLevelFromDomainBuilderListener.getResult(requestParams,memberList,finalJSON);
        }

        benchMarkGenerator(finalJSON,requestIndex);
        finalJSON.remove("result_sets");
    }


    private static void extractMembers(SearchRequestBuilder builder, JSONObject finalJSON,Map<String, String> requestParams) {
        List<String> memberList = new ArrayList<>();
        if(!requestParams.containsKey("task")) {

            builder.setSize(0);
            builder.addAggregation(AggregationBuilders.terms("intMembers").field(CommonQueries.getFieldIntMemberId(requestParams)).size(0));
            SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "memberDrill");
            Terms intMembers = response.getAggregations().get("intMembers");

            for (Terms.Bucket bucket : intMembers.getBuckets()) {
                memberList.add(bucket.getKey());
            }
        }
        else{
            String feature=requestParams.get("domainTask");
            ReportFromDomainBuilders reportFromDomainBuilders= SpringBeanUtils.getBean(feature);
            memberList= (List<String>) reportFromDomainBuilders.getDrillResults(requestParams);
        }
        finalJSON.put("intMembers", memberList);
    }


    private static void init(Map<String, Object> CLASS_MAP, SearchRequest searchRequest, List<String> memberList, JSONObject finalJSON, Map<String, String> dateGroup, String requestIndex) {

        Client client = MemberSearchQueryBuilder.getClient(searchRequest.requestParameters);
        MemberMonthsBuilderListener memberDrillExecutor = new MemberMonthsBuilderListener(searchRequest, memberList, finalJSON, client, new AtomicBoolean(false), dateGroup);
        memberDrillExecutor.execute();

        ClassLoader.loadClasses(requestIndex,CLASS_MAP);
        for (String className: CLASS_MAP.keySet()) {
            ZphSpecificMemberDrillExecutor zphSpecificMemberDrillExecutor= (ZphSpecificMemberDrillExecutor) CLASS_MAP.get(className);
            initializeListener(zphSpecificMemberDrillExecutor,searchRequest, memberList, finalJSON, client, new AtomicBoolean(false), dateGroup);
        }
    }

    private static void initializeListener(ZphSpecificMemberDrillExecutor zphSpecificMemberDrillExecutor,SearchRequest searchRequest, List<String> memberList, JSONObject finalResponse,
                                    Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        zphSpecificMemberDrillExecutor.setClient(client);
        zphSpecificMemberDrillExecutor.setSearchRequest(searchRequest);
        zphSpecificMemberDrillExecutor.setMemberList(memberList);
        zphSpecificMemberDrillExecutor.setFinalResponse(finalResponse);
        zphSpecificMemberDrillExecutor.setDateGroup(dateGroup);
        zphSpecificMemberDrillExecutor.processComplete = isComplete;
    }

    private static void featureSpecificDrill(Map<String, Object> class_map) {

        for (Map.Entry<String, Object> objectPair : class_map.entrySet()) {
            ZphSpecificMemberDrillExecutor zphSpecificMemberDrillExecutor = (ZphSpecificMemberDrillExecutor) objectPair.getValue();
            zphSpecificMemberDrillExecutor.execute();
        }
    }


    private static void benchMarkGenerator(JSONObject finalJSON,String requestIndex) {
        BenchmarkGeneratorForZph benchMarkGeneratorZph=new BenchmarkGeneratorForZph();
        JSONObject benchmark = benchMarkGeneratorZph.generate(requestIndex);
        finalJSON.put("benchmark", benchmark);
    }
}
