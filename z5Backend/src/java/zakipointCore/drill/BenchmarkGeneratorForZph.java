package zakipointCore.drill;

import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import zakipointCore.commonUtils.BenchmarkGenerator;

import java.util.*;

/**
 * Created by sktamang on 5/3/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class BenchmarkGeneratorForZph {

    private  String TRUVEN = "BenchmarkAug2016";
    private  String BENCH_MARK = "benchmark";
    private List<String> keys = new ArrayList<>();
    private  List<String>healthPlanRequests = Arrays.asList("2", "5","6");
    private  List<String> chronicConditionUtilizationRequests = Arrays.asList("101","104");


    public JSONObject generate(String requestIndex) {

        if(healthPlanRequests.contains(requestIndex) || chronicConditionUtilizationRequests.contains(requestIndex)) {
            if(healthPlanRequests.contains(requestIndex)) keys.add("healthPlan");
            else {
                if(requestIndex.equalsIgnoreCase("104"))keys.add("chronicConditionUtilization");
                else keys.add("membershipDistribution");
            }

            Client client = BenchmarkGenerator.getClient();
            JSONObject jsonObject = new JSONObject();

            SearchRequestBuilder searchRequestBuilderTruven = client.prepareSearch(BENCH_MARK).setTypes(TRUVEN);
            searchRequestBuilderTruven.setQuery(QueryBuilders.termQuery("_id", "default"));
            onResponse(searchRequestBuilderTruven.execute().actionGet(), keys, jsonObject, true, requestIndex);
            return jsonObject;
        }
        else return null;
    }


    public  JSONObject onResponse(SearchResponse response, List<String> benchmarkKeys, JSONObject jsonObject, boolean isTruvenBenchMark, String requestIndex) {
        for (SearchHit hitFields : response.getHits().hits()) {
            if (isTruvenBenchMark) {
                for (String benchmarkKey : benchmarkKeys) {
                    if (benchmarkKey.equalsIgnoreCase("healthPlan") && healthPlanRequests.contains(requestIndex)) {
                        jsonObject.put("overAllBenchmark", new JSONObject((Map) hitFields.getSource().get(benchmarkKey)));
                        jsonObject.getJSONObject("overAllBenchmark").putAll((Map<String, Object>) hitFields.getSource().get("UMReport"));
                    } else {
                        Map<String, Object> values = (Map<String, Object>) hitFields.getSource().get(benchmarkKey);
                        if (values != null) {
                            jsonObject.put(benchmarkKey, values);
                        }
                        if (benchmarkKey.equalsIgnoreCase("membershipDistribution"))
                            reformatAgeRange(values, jsonObject);
                    }
                }
            }
        }
        return jsonObject;
    }

    private static void reformatAgeRange(Map<String, Object> values, JSONObject jsonObject) {

        Map<String, List<String>> bandList = new HashMap<String, List<String>>() {{
            put("0-19", Arrays.asList("0-4", "5-9", "10-14", "15-19"));
            put("20-39", Arrays.asList("20-24", "25-29", "30-34", "35-39"));
            put("40-59", Arrays.asList("40-44", "45-49", "50-54", "55-59"));
            put("60-69", Arrays.asList("60-64", "65-69"));
            put("70+", Arrays.asList("70+"));
        }};

        values.remove("total");
        JSONObject jsonObject1 = new JSONObject();
        for (Map.Entry<String, List<String>> bands : bandList.entrySet()) {

            double value = 0;
            for (String s : bands.getValue()) {
                value += Double.valueOf(values.get(s).toString().replace("%", ""));
            }

            jsonObject1.put(bands.getKey(), value);
        }
        jsonObject1.put("70+", Double.valueOf(values.get("70+").toString().replace("%", "")));
        jsonObject.put("ageDistribution", jsonObject1);
    }
}

