package zakipointCore.drill;

import com.configurations.utils.BuilderAction;
import com.configurations.utils.ChronicConditionEnum;
import com.configurations.utils.QmUtils;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.RangeFilterBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.features.erUtilization.topChronicConditionErPopulations.TopChronicConditionErBuilder;

import java.util.*;

/**
 * Created by sktamang on 6/21/16.
 */
public class ZphMemberDrillDemographicBuilderListener {

    private String index;
    Map<String, Object> hits;

    public ZphMemberDrillDemographicBuilderListener(String index){
        this.index=index;
        hits=new HashMap<String,Object>();
    }

    Map<String,String> TERMS_AGGREGATOR_MAP=new HashMap<String,String>(){{
        put("eligibility", "memberRelationshipDesc");
        put("gender", "memberGenderId");
        put("location", "memberCityId");
    }};


    public JSONObject constructDrill(Map<String,String> searchRequest, SearchRequestBuilder builder, JSONObject finalResponse) {


        if(index.equals("101")) {
            ageWiseAgg(builder);
            termAggregator(builder);
        }

        else if(index.equals("103"))gapInCareMemberCount(builder, searchRequest);
        else if(index.equals("104")){
            topChronicConditionWiseAgg(builder,searchRequest);
        }
        executeBuilder(builder,finalResponse);
        if(index.equals("104")){
            topConditionsPer1000Extract(finalResponse);
        }
        return finalResponse;
    }

    private void termAggregator(SearchRequestBuilder builder){

        for (Map.Entry<String, String> termPair : TERMS_AGGREGATOR_MAP.entrySet()) {
            TermsBuilder termsBuilder=AggregationBuilders.terms(termPair.getKey()).field(termPair.getValue()).size(0);

            if(termPair.getKey().equalsIgnoreCase("location"))
                termsBuilder.subAggregation(AggregationBuilders.terms("state").field("memberState").size(1));
            builder.addAggregation(termsBuilder);
        }
    }


    private void gapInCareMemberCount(SearchRequestBuilder builder,Map<String,String> requestParams) {
        FiltersAggregationBuilder qmGapInCareFilters = AggregationBuilders.filters("qmGapInCareFilters");

        for (Map.Entry<String, String> pair : QmUtils.QM_CODES_MAP.entrySet()) {
            String[] metricInfo=pair.getValue().split("__");
            String description=metricInfo[0];
            String category=metricInfo[2];
            String metric=metricInfo[3];

            AndFilterBuilder andFilterForNonMeetingPop = new AndFilterBuilder();
            String positiveOrNegative = pair.getValue().split("__")[1];
            String flag = positiveOrNegative.equalsIgnoreCase("1") ? "0" : "1";

            andFilterForNonMeetingPop.add(FilterUtils.implementNumerator(requestParams, flag, true, pair.getKey()));
            qmGapInCareFilters.filter(pair.getKey() + "_" + description+"_"+category+"_"+metric, andFilterForNonMeetingPop);
        }
        builder.addAggregation(qmGapInCareFilters);
    }



    private void topChronicConditionWiseAgg(SearchRequestBuilder builder,Map<String,String> requestMap) {
        FiltersAggregationBuilder topConditionFilters=AggregationBuilders.filters("topConditionFilters");

        for (Map.Entry<String,String> conditionPaid : TopChronicConditionErBuilder.QM_ESFIELD_HASMETRIC_INFO.entrySet()) {
            topConditionFilters.filter(conditionPaid.getKey(),FilterBuilders.termFilter(conditionPaid.getValue(),"Yes"));
        }
        builder.addAggregation(topConditionFilters);



        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterUtils.eligibleDateFilter(requestMap.get("reportingTo")));

        FiltersAggregationBuilder qmFilters = AggregationBuilders.filters("qmFilters");

        for (Integer metricCode : ChronicConditionEnum.ChronicConditionMetrics.getAllMetricCode()) {
            AndFilterBuilder andFilterBuilder = qmELigFilter(requestMap.get("reportingTo"), metricCode);
            qmFilters.filter(metricCode.toString(), andFilterBuilder);
        }

        builder.addAggregation(AggregationBuilders.filter("eligibilityFilter").filter(andFilter).subAggregation(qmFilters));
    }

    private AndFilterBuilder qmELigFilter(String periodTo, Integer metricCode) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(implementQmCodeFilter(periodTo, metricCode));
        return andFilterBuilder;
    }

    public AndFilterBuilder implementQmCodeFilter(String periodTo, Integer chronicCode) {

        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermFilter("qm.measure", chronicCode));
        qmFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));

        RangeFilterBuilder rangeFilter = FilterBuilders.rangeFilter("qm.toDate").from(periodTo).to(null);
        RangeFilterBuilder rangeFilter1 = FilterBuilders.rangeFilter("qm.fromDate").from(null).to(periodTo);

        qmFilter.add(rangeFilter).add(rangeFilter1);
        return FilterBuilders.andFilter(FilterBuilders.nestedFilter("qm", qmFilter));
    }


    private void ageWiseAgg(SearchRequestBuilder builder) {
        FiltersAggregationBuilder ageFilters=AggregationBuilders.filters("age");
        List<String> ageBandList= Arrays.asList("0-19","20-39","40-59","60-69","70-above");

        for (String s : ageBandList) {
            String lowerLimit=s.split("-")[0];
            String upperLimit=s.split("-")[1].equalsIgnoreCase("above") ? null :s.split("-")[1];
            ageFilters.filter(s, FilterBuilders.rangeFilter("memberAge").gte(lowerLimit).lte(upperLimit));
        }
        builder.addAggregation(ageFilters);
    }


    private void executeBuilder(SearchRequestBuilder builder,JSONObject finalResponse) {
        SearchResponse response= (SearchResponse) BuilderAction.getResponse(builder,"memberDrill");

        if(index.equals("101")){
            Filters age=response.getAggregations().get("age");
            extractTermsAggregation(response,finalResponse);
            extractAgeBand(age, finalResponse);
        }
        else if(index.equals("103")){
            Filters qmGapInCareFilters=response.getAggregations().get("qmGapInCareFilters");
            extractGapInCare(qmGapInCareFilters,finalResponse);
        }
        else if(index.equals("104")){
            extractTopConditions(response, finalResponse);
        }
    }


    private void extractTermsAggregation(SearchResponse response,JSONObject finalResponse) {

        for (String aggName: TERMS_AGGREGATOR_MAP.keySet()) {
            JSONObject object=new JSONObject();
            Terms terms=response.getAggregations().get(aggName);

            for (Terms.Bucket bucket : terms.getBuckets()) {

                if(aggName.equalsIgnoreCase("location")){
                    Terms mbrState = bucket.getAggregations().get("state");

                    for (Terms.Bucket bucket1 : mbrState.getBuckets()) {
                        object.put(bucket.getKey()+"__"+bucket1.getKey(),bucket.getDocCount());
                    }
                }
                else object.put(bucket.getKey(), bucket.getDocCount());
            }
            finalResponse.put(aggName,object);
        }
    }


    private void extractTopConditions(SearchResponse response,JSONObject finalResponse) {

        Filters topConditions=response.getAggregations().get("topConditionFilters");
        JSONObject topConditionObject=new JSONObject();

        for (Filters.Bucket bucket : topConditions.getBuckets()) {
            topConditionObject.put(ChronicConditionEnum.ChronicConditionMetrics.getMetricByMetricDesc
                    (bucket.getKey().split("__")[0]).getMetricDisplay(),Double.valueOf(bucket.getDocCount()));
        }


        Filter newTopConditionsFilter=response.getAggregations().get("eligibilityFilter");
        Filters qmFilters=newTopConditionsFilter.getAggregations().get("qmFilters");

        for (Filters.Bucket bucket : qmFilters.getBuckets()) {
            topConditionObject.put(ChronicConditionEnum.ChronicConditionMetrics.getMetricByCode(Integer.parseInt(bucket.getKey())).getMetricDisplay()+"_PopRiskSync",Double.valueOf(bucket.getDocCount()));
        }
        finalResponse.put("topConditions",topConditionObject);
    }


    private void extractAgeBand(Filters ageBands,JSONObject finalResponse) {
        JSONObject ageObject=new JSONObject();

        for (Filters.Bucket bucket : ageBands.getBuckets()) {
            String key=bucket.getKey();
            if(bucket.getKey().split("-")[1].equalsIgnoreCase("above")) key="70+";
            ageObject.put(key,bucket.getDocCount());
        }
        finalResponse.put("age",ageObject);
    }


    private void extractGapInCare(Filters qmGapInCareFilters,JSONObject finalResponse) {
        JSONObject gapInCareCounterObject=new JSONObject();

        for (Filters.Bucket bucket : qmGapInCareFilters.getBuckets()) {
            JSONObject jsonObject=new JSONObject();

            String[] metricInfo=bucket.getKey().split("_");
            String description=metricInfo[1];
            String category=metricInfo[2];
            String metric=metricInfo[3];

            jsonObject.put("count",bucket.getDocCount());
            jsonObject.put("description",description);
            jsonObject.put("category",category);
            jsonObject.put("metric",metric);

            gapInCareCounterObject.put(metric, jsonObject);
        }
        finalResponse.put("gapInCare",gapInCareCounterObject);
    }

    private  JSONObject topConditionsPer1000Extract(JSONObject finalJSON) {
        JSONObject memberMonthsObj = (JSONObject) finalJSON.get("memberMonths");
        Map<String, Double> topConditionsObj = (Map<String, Double>) finalJSON.get("topConditions");
        Map<String, Double> tempMap = new HashMap<>(topConditionsObj);

        double memberMonth = (double) memberMonthsObj.get("current");

        for (Map.Entry<String, Double> pair : topConditionsObj.entrySet()) {
            double per1000 = CalculatorUtils.perThousand(pair.getValue(), memberMonth);
            tempMap.put(pair.getKey() + "_per1000", per1000);
        }

        return finalJSON.put("topConditions", tempMap);
    }
}
