package zakipointCore.drill.drillReport.erVisit;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.MemberSearchQueryBuilder;
import com.configurations.utils.VisitAdmission;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.*;
import zakipointCore.drill.drillReport.DrillBuilder;
import zakipointCore.drill.filters.zphMemberDrillBuilderListener.MemberMonthsBuilderListener;

import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 8/1/16.
 */
public class ErVisitPerThousandTrendBuilderListener extends DrillBuilder {

    private final String ERVISIT="erVisit";
    private final String MEMBER_MONTHS ="memberMonths";

    @Override
    public MultiSearchRequestBuilder createBuilder(RequestState state, List<String> memberList) {
        Client client = MemberSearchQueryBuilder.getClient(state.request.requestParameters);

        MultiSearchRequestBuilder builder1= QueryUtils.buildMultiSearch(client);
        MemberMonthsBuilderListener memberMonthsBuilderListener =new MemberMonthsBuilderListener();
        memberMonthsBuilderListener.constructDrillBuilder(builder1,state,memberList,client);

        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request.requestParameters, VisitAdmission.NAME);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.termsFilter(MemberSearch.FIELD_INT_MEMBER_ID, memberList));
        FilterUtils.getErVisitFilter(andFilterBuilder);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));

        Map<String,String> dateMap= DateUtils.getAllMonthsString(state.periodFrom(),state.periodTo());

        FiltersAggregationBuilder filters= AggregationBuilders.filters("filters");
        for (Map.Entry<String, String> dateEntry : dateMap.entrySet()) {
            AndFilterBuilder andFilterBuilder1=new AndFilterBuilder();
            andFilterBuilder1.add(FilterUtils.nestedEarliestPaidDateFilter(VisitAdmission.VisitAmounts.NAME,dateEntry.getKey(),dateEntry.getValue()));
            filters.filter(DateUtils.getMonthOnlyForDate(dateEntry.getValue()),andFilterBuilder1);
        }

        builder.addAggregation(filters
                .subAggregation(AggregationBuilders.sum("erVisit").field(VisitAdmission.ER_VISIT)));

        return builder1.add(builder);
    }


    protected  JSONObject createListener(SearchResponse response, RequestState state,JSONObject jsonObject) {

        for (Aggregation aggregation : response.getAggregations()) {
            JSONObject jsonObject1=new JSONObject();
            Filters filters= (Filters) aggregation;
            if(aggregation.getName().contains("filters")){

                for (Filters.Bucket bucket : filters.getBuckets()) {
                    Sum erVisit= bucket.getAggregations().get("erVisit");
                    jsonObject1.put(bucket.getKey(),erVisit.getValue());
                }
                jsonObject.put("erVisit",jsonObject1);
            }else {
                for (Filters.Bucket bucket : filters.getBuckets()) {
                    jsonObject1.put(bucket.getKey(),Double.valueOf(bucket.getDocCount()));
                }
                jsonObject.put("memberMonths",jsonObject1);
            }
        }

        return  jsonObject;
    }


    @Override
    protected void finalResponseRenderer(JSONObject jsonObject) {
        Map<String,Double> erVisit= (Map<String, Double>) jsonObject.get(ERVISIT);
        Map<String,Double> memberMonths= (Map<String, Double>) jsonObject.get(MEMBER_MONTHS);

        JSONObject finalObject=new JSONObject();

        for (Map.Entry<String, Double> pair : erVisit.entrySet()) {
            finalObject.put(pair.getKey()+"__ErPer1000", CalculatorUtils.perThousand(pair.getValue(),memberMonths.get(pair.getKey())));
        }
        jsonObject.put("erPer1000",finalObject);
    }
}
