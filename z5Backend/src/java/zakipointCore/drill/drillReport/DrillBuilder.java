package zakipointCore.drill.drillReport;

import com.configurations.utils.BuilderAction;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchResponse;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.drill.drillReport.erVisit.ErVisitPerThousandTrendBuilderListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 8/1/16.
 */
public abstract class DrillBuilder {
    static Map<String, Object> CLASS_MAP = new HashMap<>();

    public JSONObject execute(RequestState state, List<String> memberList){
        MultiSearchResponse response= (MultiSearchResponse) BuilderAction.getResponse(createBuilder(state,memberList),"trendingDrill");
        return processResponse(response,state);
    }

    public static Object getBuilderClass(String reportName){
        init();
        DrillReportBuilder<Object,Object,Object> drillReportBuilder = (report,temp) ->  CLASS_MAP.get(report);
        return drillReportBuilder.getResult(reportName,null);
    }


    public static void init() {
        {{
            CLASS_MAP.put("erVisitAndAdmissionUsage", new ErVisitPerThousandTrendBuilderListener());
        }};
    }

    private   JSONObject processResponse(ActionResponse response, RequestState state) {
        MultiSearchResponse response1 = (MultiSearchResponse) response;
        JSONObject jsonObject=new JSONObject();
        for (MultiSearchResponse.Item item : response1.getResponses()) {
            if (item.isFailure()) {
                System.out.println("the error is = " + item.getFailureMessage());
            } else createListener(item.getResponse(),state,jsonObject);
        }
        finalResponseRenderer(jsonObject);
        return jsonObject;
    }


    protected  abstract JSONObject createListener(SearchResponse response, RequestState state,JSONObject jsonObject);
    public abstract MultiSearchRequestBuilder createBuilder(RequestState state, List<String> memberList);
    protected abstract void finalResponseRenderer(JSONObject jsonObject);

}
