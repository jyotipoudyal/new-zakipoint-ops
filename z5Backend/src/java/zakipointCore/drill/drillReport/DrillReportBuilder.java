package zakipointCore.drill.drillReport;

/**
 * Created by sktamang on 8/1/16.
 */
public interface DrillReportBuilder<T,V,Z> {

    public Z getResult(V v, T t);
}
