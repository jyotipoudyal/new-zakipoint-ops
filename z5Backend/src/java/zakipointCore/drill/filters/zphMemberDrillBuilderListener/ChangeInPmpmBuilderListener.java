package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.MemberSearch;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.ZphSpecificMemberDrillExecutor;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 6/22/16.
 */
public class ChangeInPmpmBuilderListener extends ZphSpecificMemberDrillExecutor {


    public ChangeInPmpmBuilderListener(SearchRequest searchRequest, List<String> memberList,
                                       JSONObject finalResponse, Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse,client,isComplete,dateGroup);
        this.reportName="changeInPmpm";
    }

    public ChangeInPmpmBuilderListener() {
        this.reportName="changeInPmpm";
    }


    @Override
    public MultiSearchRequestBuilder constructDrillBuilder(MultiSearchRequestBuilder multiSearchRequestBuilder, RequestState state, List<String> memberList, Client client) {

        SearchRequestBuilder builder= QueryUtils.buildEndSearchWihType(client,state.request, MemberSearch.NAME);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        memberListFilter(andFilterBuilder, memberList,state.request.requestParameters);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        implementAgg(builder, state);
        return  multiSearchRequestBuilder.add(builder);
    }


    protected void implementAgg(SearchRequestBuilder builder,RequestState state) {
        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        CommonQueries.paidMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);
        CommonQueries.serviceMonth(state, memberAmountFilter, MemberSearch.MEMBER_AMOUNT.NAME);

        builder.addAggregation(AggregationBuilders.nested("memberAmountNested").path(MemberSearch.MEMBER_AMOUNT.NAME)
                .subAggregation(AggregationBuilders.filter("memberAmountFilter").filter(memberAmountFilter)
                        .subAggregation(AggregationBuilders.sum("totalMemberPaidAmount").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT))));
    }


    @Override
    protected JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse,RequestState state) {

        JSONObject jsonObject1=new JSONObject();
        String reportingPeriod = insertMetaInfo(response, jsonObject1, state);

        double memberMonths= (double)((JSONObject) finalResponse.get("memberMonths")).get(reportingPeriod);

        Nested memberAmountNested = response.getAggregations().get("memberAmountNested");
        Filter memberAmountFilter = memberAmountNested.getAggregations().get("memberAmountFilter");
        Sum totalMemberPaidAmount = memberAmountFilter.getAggregations().get("totalMemberPaidAmount");

        jsonObject1.put("totalPaidAmount",totalMemberPaidAmount.getValue());
        jsonObject1.put("totalPaidAmountPmpm", CalculatorUtils.divide(totalMemberPaidAmount.getValue(),memberMonths));
        jsonObject1.put("memberMonths", memberMonths);

        jsonObject.put(reportingPeriod,jsonObject1);
        return  jsonObject;
    }
}
