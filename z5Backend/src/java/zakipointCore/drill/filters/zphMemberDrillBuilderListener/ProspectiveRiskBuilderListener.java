package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.avg.AvgBuilder;
import zakipointCore.commonUtils.LevelUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 1/17/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class ProspectiveRiskBuilderListener extends ChangeInPmpmBuilderListener {

    public ProspectiveRiskBuilderListener(SearchRequest searchRequest, List<String> memberList,
                                          JSONObject finalResponse, Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse, client, isComplete, dateGroup);
        this.reportName = "prospectiveRisk";
    }

    public ProspectiveRiskBuilderListener() {
        this.reportName = "prospectiveRisk";
    }

    protected void implementAgg(SearchRequestBuilder builder, RequestState state) {
        AndFilterBuilder groupFilter = new AndFilterBuilder();
        groupFilter.add(FilterBuilders.matchAllFilter());
        LevelUtils.setLevelFilter(state, groupFilter, state.periodTo());
        builder.addAggregation(AggregationBuilders.filter("groupFilter").filter(groupFilter)
                .subAggregation( AggregationBuilders.nested("maraHistoricalRiskScores").path("MaraHistoricalRiskScores")
                        .subAggregation(AggregationBuilders.filter("processedDateFilter").filter(FilterBuilders.rangeFilter("MaraHistoricalRiskScores.processedDate")
                                .gte(state.periodTo()).lte(state.periodTo()))
                                .subAggregation(getAvgFilter()))));
    }

    protected AvgBuilder getAvgFilter() {
        return AggregationBuilders.avg("avgScore").field("MaraHistoricalRiskScores.prospectiveTotalNormalizedToBob");
    }


    @Override
    protected JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse, RequestState state) {
        JSONObject jsonObject1 = new JSONObject();
        String reportingPeriod = insertMetaInfo(response, jsonObject1, state);

        Filter groupFilter = response.getAggregations().get("groupFilter");
        Nested maraHistoricalRiskScores=groupFilter.getAggregations().get("maraHistoricalRiskScores");
        Filter filter=maraHistoricalRiskScores.getAggregations().get("processedDateFilter");
        Avg avg = filter.getAggregations().get("avgScore");

        jsonObject1.put("avgScore", (!Double.isNaN(avg.getValue())) ? avg.getValue() : 0);
        jsonObject.put(reportingPeriod, jsonObject1);
        return jsonObject;
    }
}
