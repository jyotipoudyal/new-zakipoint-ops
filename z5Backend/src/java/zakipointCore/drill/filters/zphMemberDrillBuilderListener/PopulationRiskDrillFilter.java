package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.search.MemberSearchRequest;
import com.configurations.utils.MemberSearch;
import com.configurations.utils.MemberSearchQueryBuilder;
import com.configurations.utils.ReportFilter;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.commonUtils.FilterUtils;
import zakipointCore.drill.ConsecutiveDateGenerator;
import zakipointCore.drill.filters.ChronicComorbiditiesDrillFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sktamang on 5/17/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class PopulationRiskDrillFilter implements ReportFilter {
    private final String AGG_MEMBERS = "members";
    String tableName;


    @Override
    public FilterBuilder createFilter(MemberSearchRequest searchRequest) {
        List<String> stringList = createTermList(searchRequest);
        return FilterBuilders.andFilter(FilterBuilders.termsFilter(MemberSearch.FIELD_INT_MEMBER_ID, stringList));
    }

    public List<String> createTermList(MemberSearchRequest searchRequest) {

        SearchRequestBuilder builderForPriorMonths = getBuilder(searchRequest);
        SearchResponse response = builderForPriorMonths.execute().actionGet();
        searchRequest.requestParameters.put("table", tableName);
        List<String> memberListCurrent = new ArrayList<String>();
        List<String> memberListPrior= new ArrayList<String>();

        Terms terms = response.getAggregations().get(AGG_MEMBERS);
            for (Terms.Bucket bucket : terms.getBuckets()) {
                memberListCurrent.add(bucket.getKey());
            }

        Filter filter=response.getAggregations().get("priorMonths");
        Terms termsPrior = filter.getAggregations().get(AGG_MEMBERS);
        for (Terms.Bucket bucket : termsPrior.getBuckets()) {
            memberListPrior.add(bucket.getKey());
        }

        memberListCurrent.removeAll(memberListPrior);
        return memberListCurrent;
    }


    private SearchRequestBuilder getBuilder(MemberSearchRequest searchRequest) {
        String threeOrOne=searchRequest.requestParameters.get("threeOrOne");
        boolean threeOrOneBool=threeOrOne.equalsIgnoreCase("three")?true:false;
        String metricCode=searchRequest.requestParameters.get("chronicCode");

        String reporintToDate= ConsecutiveDateGenerator.getThreeOrOneMonthsBackDate(searchRequest.requestParameters.get("reportingTo"), threeOrOneBool);

        tableName = searchRequest.requestParameters.get("table");
        if (searchRequest.requestParameters.containsKey("recordTypes"))
            searchRequest.requestParameters.put("table", searchRequest.requestParameters.get("recordTypes"));

        SearchRequestBuilder requestBuilder = new MemberSearchQueryBuilder(searchRequest).setTypes().build().setSize(0);
        searchRequest.requestParameters.put("query", searchRequest.requestParameters.get("dataSearchQuery"));

        FilterBuilder filter = ChronicComorbiditiesDrillFilter.getNewFilter(searchRequest);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(filter);


        AndFilterBuilder andFilterBuilderForPriorMonths=new AndFilterBuilder();
        andFilterBuilderForPriorMonths.add(FilterUtils.eligibleDateFilter(reporintToDate));
        andFilterBuilderForPriorMonths.add(implementQmCodeFilter(reporintToDate,metricCode));

        FilteredQueryBuilder filteredQuery = QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder);
        requestBuilder.setQuery(filteredQuery);

        AggregationBuilder termsForMemberId;
        termsForMemberId=AggregationBuilders.terms(AGG_MEMBERS).field(MemberSearch.FIELD_INT_MEMBER_ID).size(0);
        requestBuilder.addAggregation(AggregationBuilders.filter("priorMonths").filter(andFilterBuilderForPriorMonths).subAggregation(termsForMemberId))
                .addAggregation(termsForMemberId);
        return requestBuilder;
    }

    public AndFilterBuilder implementQmCodeFilter(String periodTo, String chronicCode) {

        AndFilterBuilder qmFilter = new AndFilterBuilder();
        qmFilter.add(FilterUtils.getTermFilter("qm.measure", chronicCode));
        qmFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));

        RangeFilterBuilder rangeFilter = FilterBuilders.rangeFilter("qm.toDate").from(periodTo).to(null);
        RangeFilterBuilder rangeFilter1 = FilterBuilders.rangeFilter("qm.fromDate").from(null).to(periodTo);

        qmFilter.add(rangeFilter).add(rangeFilter1);
        return FilterBuilders.andFilter(FilterBuilders.nestedFilter("qm", qmFilter));
    }
}
