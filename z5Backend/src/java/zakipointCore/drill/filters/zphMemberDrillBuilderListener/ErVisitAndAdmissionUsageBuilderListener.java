package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.VisitAdmission;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.ZphSpecificMemberDrillExecutor;
import zakipointCore.commonUtils.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 6/22/16.
 */
public class ErVisitAndAdmissionUsageBuilderListener extends ZphSpecificMemberDrillExecutor {

    List<String> Um_Metrics= Arrays.asList(VisitAdmission.ER_VISIT,VisitAdmission.TOTAL_ADMISSIONS,
                                            VisitAdmission.THIRD_DAY_READMIT,VisitAdmission.IN_PATIENT_DAYS);

    public ErVisitAndAdmissionUsageBuilderListener(SearchRequest searchRequest, List<String> memberList,
                                                   JSONObject finalResponse, Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse,client,isComplete,dateGroup);
        this.reportName="erVisitAndAdmissionUsage";
    }

    public ErVisitAndAdmissionUsageBuilderListener(){
        this.reportName="erVisitAndAdmissionUsage";
    }


    @Override
    public MultiSearchRequestBuilder constructDrillBuilder(MultiSearchRequestBuilder multiSearchRequestBuilder, RequestState state, List<String> memberList, Client client) {

        SearchRequestBuilder builder= QueryUtils.buildEndSearchWihType(client, state.request, VisitAdmission.NAME);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        LevelUtils.setLOAFilter(state,andFilterBuilder);
        memberListFilter(andFilterBuilder, memberList,state.request.requestParameters);

        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        implementAgg(builder);
        return multiSearchRequestBuilder.add(builder);
    }


    private void implementAgg(SearchRequestBuilder builder) {

        FilterBuilder totalAmountNonZeroFilter=FilterBuilders.notFilter(FilterBuilders.termFilter(VisitAdmission.TOTAL_AMOUNT, 0));
        builder.addAggregation(AggregationBuilders.filter("totalAmountNonZeroFilter").filter(totalAmountNonZeroFilter)
                .subAggregation(umSpecificFilter()));
    }

    private FiltersAggregationBuilder umSpecificFilter() {
        FiltersAggregationBuilder umMetricsFilters=AggregationBuilders.filters("umMetricsFilters");

        for (String um_metric : Um_Metrics) {
            umMetricsFilters.filter(um_metric, FilterBuilders.notFilter(FilterBuilders.termFilter(um_metric,0)));
        }

        for (String um_metric : Um_Metrics) {
            umMetricsFilters.subAggregation(AggregationBuilders.sum(um_metric).field(um_metric))
                    .subAggregation(AggregationBuilders.terms(um_metric+"_members").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0));
        }

        return  umMetricsFilters;
    }


    @Override
    public JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse,RequestState state) {
        JSONObject jsonObject1=new JSONObject();
        String reportingPeriod = super.insertMetaInfo(response, jsonObject1,state);

        Filter totalAmountNonZeroFilter=response.getAggregations().get("totalAmountNonZeroFilter");
        Filters umMetricsFilters=totalAmountNonZeroFilter.getAggregations().get("umMetricsFilters");

        double memberMonths= (double)((JSONObject) finalResponse.get("memberMonths")).get(reportingPeriod);

        for (Filters.Bucket bucket : umMetricsFilters.getBuckets()) {
            for (Aggregation aggregation : bucket.getAggregations()) {

                if(aggregation instanceof Sum) {
                    Sum sum = (Sum) aggregation;
                    if (sum.getName().equalsIgnoreCase(bucket.getKey())) {
                        jsonObject1.put(sum.getName(), sum.getValue());
                        jsonObject1.put(sum.getName() + "per1000", CalculatorUtils.perThousand(sum.getValue(), memberMonths));
                    }
                }
                else {
                    Terms members=(Terms)aggregation;
                    if(members.getName().split("_")[0].equalsIgnoreCase(bucket.getKey())){
                        jsonObject1.put(members.getName().split("_")[0]+"MemberCount",members.getBuckets().size());
                    }
                }
            }
        }

        specificMetricCalculation(jsonObject1,memberMonths);
        jsonObject.put(reportingPeriod,jsonObject1);
        return jsonObject;
    }

    private void specificMetricCalculation(JSONObject jsonObject1,double memberMonths) {

        double inpatientDays=(double)jsonObject1.get("inpatientDays");
        double totalAdmissions=(double)jsonObject1.get("totalAdmissions");

        double averageLengthOfStay=CalculatorUtils.divide(inpatientDays,totalAdmissions);
        jsonObject1.put("averageLengthOfStay",averageLengthOfStay);
        jsonObject1.put("averageLengthOfStay" +"per1000", CalculatorUtils.perThousand(averageLengthOfStay, memberMonths));
    }
}
