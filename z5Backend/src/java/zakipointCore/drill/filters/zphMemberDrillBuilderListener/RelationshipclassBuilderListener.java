package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.util.*;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 1/4/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class RelationshipclassBuilderListener extends ChangeInPmpmBuilderListener {

    List<String> RELATIONSHIPCLASS_VALUES= Arrays.asList("Employee","Dependent","Spouse","Other");

    public RelationshipclassBuilderListener(){this.reportName = "relationshipClass";}

    public RelationshipclassBuilderListener(SearchRequest searchRequest, List<String> memberList, JSONObject finalResponse,
                                            Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {

        super(searchRequest, memberList, finalResponse, client, isComplete, dateGroup);
        this.reportName = "relationshipClass";
    }

    @Override
    protected void implementAgg(SearchRequestBuilder builder, RequestState state) {
        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        andFilterBuilder.add(getRelationShipFilter(state));

        builder.addAggregation(AggregationBuilders.filter("relationsipFilter").filter(andFilterBuilder)
                .subAggregation(AggregationBuilders.terms("memberRelationshipClass").field("memberRelationshipClass").size(0)));
    }

    public static FilterBuilder getRelationShipFilter(RequestState state) {

        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.rangeFilter("relationship.fromDate").lte(state.periodTo()));
        andFilter.add(FilterBuilders.rangeFilter("relationship.toDate").gte(state.periodTo()));
        return FilterBuilders.nestedFilter("relationship",andFilter);
    }

    @Override
    protected JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse, RequestState state) {

        JSONObject jsonObject1=new JSONObject();
        String reportingPeriod = insertMetaInfo(response, jsonObject1, state);

        Filter relationsipFilter=response.getAggregations().get("relationsipFilter");
        Terms  memberRelationshipClass=relationsipFilter.getAggregations().get("memberRelationshipClass");

        for (String s : RELATIONSHIPCLASS_VALUES) {
            double temp=0;
            for (Terms.Bucket bucket : memberRelationshipClass.getBuckets()) {
                if(bucket.getKey().equalsIgnoreCase(s)) {
                    temp=bucket.getDocCount();
                    break;
                }
            }
            jsonObject1.put(s,temp);
        }

        jsonObject.put(reportingPeriod,jsonObject1);
        return  jsonObject;
    }
}
