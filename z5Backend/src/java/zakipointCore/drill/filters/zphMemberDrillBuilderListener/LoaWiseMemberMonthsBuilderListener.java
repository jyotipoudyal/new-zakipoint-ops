package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearch;
import com.configurations.utils.MemberSearchQueryBuilder;
import com.configurations.utils.VisitAdmission;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.*;

import java.util.List;
import java.util.Map;


/**
 * Created by sktamang on 7/27/16.
 */
public class LoaWiseMemberMonthsBuilderListener {

    public static void createLoaBuilder(SearchRequestBuilder builder, JSONObject finalResponse, Map<String, String> requestParams) {
        String aggName = "division";
        TermsBuilder termsBuilder = AggregationBuilders.terms(aggName).field("divisionId").size(0);
        builder.addAggregation(termsBuilder);

        SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "divisionWiseAggregatorExplorePop");

        JSONObject object = new JSONObject();
        Terms terms = response.getAggregations().get(aggName);

        for (Terms.Bucket bucket : terms.getBuckets()) {
            object.put(bucket.getKey(), bucket.getDocCount());
        }
        finalResponse.put(aggName, object);
        constructDrillBuilder(requestParams, finalResponse);
    }

    private static void constructDrillBuilder(Map<String, String> requestParams, JSONObject finalResponse) {

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.requestParameters = requestParams;

        Client client = MemberSearchQueryBuilder.getClient(searchRequest.requestParameters);
        Map<String, String> divisionMap = (Map<String, String>) finalResponse.getJSONObject("division");
        List<String> memberList = (List<String>) finalResponse.get("intMembers");

        JSONObject jsonObject = new JSONObject();
        for (String loa : divisionMap.keySet()) {
            requestParams.put("division", loa);
            RequestState state = new RequestState("", "", searchRequest, null);

            MultiSearchRequestBuilder multiSearchRequestBuilder = QueryUtils.buildMultiSearch(client);
            MemberMonthsBuilderListener memberMonthsBuilderListener = new MemberMonthsBuilderListener();

            memberMonthsBuilderListener.constructDrillBuilder(multiSearchRequestBuilder, state, memberList, client);
            multiSearchRequestBuilder.add(erVisitBuilder(state, memberList, client));
            constructDrillListener(multiSearchRequestBuilder, loa, jsonObject);
        }
        finalResponse.put("divisionWiseErVisitPer1000", jsonObject);

    }

    private static SearchRequestBuilder erVisitBuilder(RequestState state, List<String> memberList, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, VisitAdmission.NAME);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        LevelUtils.setLOAFilter(state, andFilterBuilder);
//        memberListFilter(andFilterBuilder, memberList, state.request.requestParameters);

        andFilterBuilder.add(FilterUtils.nestedEarliestPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME));
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        FilterBuilder totalAmountNonZeroFilter = FilterBuilders.notFilter(FilterBuilders.termFilter(VisitAdmission.TOTAL_AMOUNT, 0));
        builder.addAggregation(AggregationBuilders.filter("totalAmountNonZeroFilter").filter(totalAmountNonZeroFilter)
                .subAggregation(AggregationBuilders.filter("erVisitFilter").filter(FilterBuilders.termFilter(VisitAdmission.ER_VISIT, 1))
                        .subAggregation(AggregationBuilders.sum("erVisitSum").field(VisitAdmission.ER_VISIT))
                        .subAggregation(AggregationBuilders.terms("memberCount").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0))));
        return builder;
    }


    private static JSONObject constructDrillListener(MultiSearchRequestBuilder builder, String loa, JSONObject jsonObject) {
        ActionResponse response = BuilderAction.executeBuilder(builder, "erVisitPer1000DivisionWise");
        MultiSearchResponse response1 = (MultiSearchResponse) response;

        JSONObject jsonObject1 = new JSONObject();
        for (MultiSearchResponse.Item item : response1.getResponses()) {
            if (item.isFailure()) {
                System.out.println("the error is = " + item.getFailureMessage());
            } else constructListener(item.getResponse(), jsonObject1);
        }

        jsonObject1.put("erVisitPer1000", CalculatorUtils.perThousand((double) jsonObject1.get("erVisit"), (double) jsonObject1.get("memberMonths")));
        return jsonObject.put(loa, jsonObject1);
    }


    private static void constructListener(SearchResponse response, JSONObject jsonObject) {
        double memberMonths = 0;

        for (Aggregation aggregation : response.getAggregations()) {

            if (aggregation.getName().equalsIgnoreCase("totalAmountNonZeroFilter")) {

                Filter totalAmountNonZeroFilter = (Filter) aggregation;
                Filter erVisitFilter = totalAmountNonZeroFilter.getAggregations().get("erVisitFilter");
                Sum erVisitSum = erVisitFilter.getAggregations().get("erVisitSum");
                Terms memberCount = erVisitFilter.getAggregations().get("memberCount");

                jsonObject.put("erVisit", erVisitSum.getValue());
                jsonObject.put("memberCount", memberCount.getBuckets().size());
            } else {
                Filters memberMonthFilters = (Filters) aggregation;

                for (Filters.Bucket bucket : memberMonthFilters.getBuckets()) {
                    memberMonths += bucket.getDocCount();
                }
                jsonObject.put("memberMonths", memberMonths);
            }
        }
    }
}
