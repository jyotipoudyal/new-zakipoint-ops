package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.MemberSearch;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import zakipointCore.builder.CommonQueries;
import zakipointCore.builder.ZphSpecificMemberDrillExecutor;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 7/4/16.
 */
public class MemberMonthsBuilderListener extends ZphSpecificMemberDrillExecutor {

    public MemberMonthsBuilderListener(SearchRequest searchRequest, List<String> memberList, JSONObject finalResponse, Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse, client, isComplete, dateGroup);
        this.reportName="memberMonths";
    }

    public MemberMonthsBuilderListener(){
        this.reportName="memberMonths";
    }

    @Override
    public MultiSearchRequestBuilder constructDrillBuilder(MultiSearchRequestBuilder multiSearchRequestBuilder
                                                    , RequestState state, List<String> memberList, Client client) {
        SearchRequestBuilder builder= QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);

        AndFilterBuilder andFilterBuilder=new AndFilterBuilder();
        memberListFilter(andFilterBuilder, memberList,state.request.requestParameters);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(),andFilterBuilder));

        FiltersAggregationBuilder memberMonthFilters = (FiltersAggregationBuilder) CommonQueries.memberMonthsAggs(state, "memberMonthsFilter");
        builder.addAggregation(memberMonthFilters);
        return multiSearchRequestBuilder.add(builder);
    }


    @Override
    protected JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse, RequestState state) {
        Filters memberMonthsFilters=response.getAggregations().get("memberMonthsFilter");
        double memberMonths=0;

        for (Filters.Bucket bucket : memberMonthsFilters.getBuckets()) {
           memberMonths=memberMonths+bucket.getDocCount();
        }

        String reportingPeriod = super.insertMetaInfo(response, null,state);
        jsonObject.put(reportingPeriod,memberMonths);
        return jsonObject;
    }


}
