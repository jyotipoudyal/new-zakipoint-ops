package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.CalculatorUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 1/2/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class PharmacyPmpmBuilderListener  extends MailGenericOrderPrescriptionBuilderListener {

    public PharmacyPmpmBuilderListener(SearchRequest searchRequest, List<String> memberList, JSONObject finalResponse,
                                       Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {

        super(searchRequest, memberList, finalResponse, client, isComplete, dateGroup);
        this.reportName = "pharmacyPmpm";
    }

    public PharmacyPmpmBuilderListener() {
        this.reportName = "pharmacyPmpm";
    }


    protected void implementAggs(SearchRequestBuilder builder) {
        builder.addAggregation(AggregationBuilders.sum("paidAmount").field("paidAmount"));
    }


    @Override
    public JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse, RequestState state) {

        JSONObject jsonObject1 = new JSONObject();
        String reportingPeriod = insertMetaInfo(response, jsonObject1, state);
        double memberMonths= (double)((JSONObject) finalResponse.get("memberMonths")).get(reportingPeriod);

        Sum paidAmount = response.getAggregations().get("paidAmount");
        jsonObject1.put("totalPaidAmountPmpm", CalculatorUtils.divide(paidAmount.getValue()/100,memberMonths));
        jsonObject1.put("totalPaidAmount", paidAmount.getValue()/100);
        jsonObject.put(reportingPeriod, jsonObject1);
        return jsonObject;
    }
}
