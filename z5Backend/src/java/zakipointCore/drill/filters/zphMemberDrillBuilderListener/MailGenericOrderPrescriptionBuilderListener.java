package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.MemberSearch;
import com.configurations.utils.Pharmacy;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.aggregations.metrics.sum.SumBuilder;
import zakipointCore.builder.ZphSpecificMemberDrillExecutor;
import zakipointCore.commonUtils.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 6/22/16.
 */
public class MailGenericOrderPrescriptionBuilderListener extends ZphSpecificMemberDrillExecutor {

    public MailGenericOrderPrescriptionBuilderListener(SearchRequest searchRequest, List<String> memberList, JSONObject finalResponse,
                                                       Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {

        super(searchRequest, memberList, finalResponse, client, isComplete, dateGroup);
        this.reportName = "mailAndGenericOrderPrescription";
    }

    public MailGenericOrderPrescriptionBuilderListener() {
        this.reportName = "mailAndGenericOrderPrescription";
    }

    @Override
    public MultiSearchRequestBuilder constructDrillBuilder(MultiSearchRequestBuilder multiSearchRequestBuilder, RequestState state, List<String> memberList, Client client) {

        SearchRequestBuilder builder = QueryUtils.buildEndSearch(client, state.request);
        setTypes(builder);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        memberListFilter(andFilterBuilder, memberList,state.request.requestParameters);

        FilterUtils.paidServiceDateFilter(state, andFilterBuilder);
        LevelUtils.setLOAFilter(state, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));

        implementAggs(builder);
        return multiSearchRequestBuilder.add(builder);
    }

    protected void setTypes(SearchRequestBuilder builder) {
        builder.setTypes(Pharmacy.NAME).setSize(0);
    }


    protected void implementAggs(SearchRequestBuilder builder) {

        FiltersAggregationBuilder filters = AggregationBuilders.filters("orderFilters");
        filters.filter("mailOrder", FilterBuilders.termFilter("mailOrderFlag", "M"));
        filters.filter("genericOrder", FilterBuilders.termFilter("genericFlag", "Y"));
        filters.filter("specialityOrder", FilterBuilders.termFilter("udf5_pharmacy", "S"));
        filters.filter("formulary", FilterBuilders.termFilter("formularyInd", "Y"));

        builder.addAggregation(filters.subAggregation(pharmacyScriptSum())
                        .subAggregation(AggregationBuilders.terms("members").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0)))
                .addAggregation(pharmacyScriptSum());

    }


    private SumBuilder pharmacyScriptSum() {
        return AggregationBuilders.sum("totalPrescriptions").field("pharmacyScriptForUm");
    }


    @Override
    public JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse, RequestState state) {

        JSONObject jsonObject1 = new JSONObject();
        String reportingPeriod = insertMetaInfo(response, jsonObject1, state);

        Sum totalPrescriptionCount = response.getAggregations().get("totalPrescriptions");

        Filters orderFilters=response.getAggregations().get("orderFilters");

        for (Filters.Bucket filter: orderFilters.getBuckets()) {
                Sum prescriptions = filter.getAggregations().get("totalPrescriptions");
                Terms members=filter.getAggregations().get("members");
                jsonObject1.put(filter.getKey(), prescriptions.getValue());
                jsonObject1.put(filter.getKey() + "Percent", CalculatorUtils.calculatePercentage(prescriptions.getValue(), totalPrescriptionCount.getValue()));
                jsonObject1.put(filter.getKey()+"MemberCount",members.getBuckets().size());
        }

        jsonObject1.put("totalprescriptionCount", totalPrescriptionCount.getValue());

        jsonObject.put(reportingPeriod, jsonObject1);
        return jsonObject;
    }
}
