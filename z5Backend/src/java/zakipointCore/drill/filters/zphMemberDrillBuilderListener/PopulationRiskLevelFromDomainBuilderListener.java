package zakipointCore.drill.filters.zphMemberDrillBuilderListener;


import org.codehaus.groovy.grails.web.json.JSONObject;
import zakipointCore.dashBoard.populationRisk.PopulationRiskSankeyBuilderListener;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 6/20/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class PopulationRiskLevelFromDomainBuilderListener {

    private List<String> LEVELS= Arrays.asList("MARA_RISK_LEVEL","HS_RISK_LEVEL");

    public void getResult(Map<String,String> requestMap,List<String> memberList,JSONObject finalJSON){
        for (String level : LEVELS) {
            requestMap.put("category",level);
            PopulationRiskSankeyBuilderListener builder= new PopulationRiskSankeyBuilderListener();
            builder.setMEMBER_LIST(memberList);
            finalJSON.put(level,builder.getResults(requestMap));
        }
    }

}
