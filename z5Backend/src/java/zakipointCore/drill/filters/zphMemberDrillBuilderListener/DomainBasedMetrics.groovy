package zakipointCore.drill.filters.zphMemberDrillBuilderListener

import com.cohort.CustomMetric
import com.cohort.MemberByCustomMetric
import com.configurations.utils.BuilderAction
import org.codehaus.groovy.grails.web.json.JSONObject
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.search.aggregations.AggregationBuilders
import org.elasticsearch.search.aggregations.bucket.terms.Terms
import zakipointCore.commonUtils.CalculatorUtils
import zakipointCore.commonUtils.RequestUtils

/**
 * Created by sktamang on 5/1/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class DomainBasedMetrics {
    public static Map<String,String> YEAR_AMP=new HashMap<String,String>(){{
        put("1", "current");
        put("2", "last");
        put("3", "prior");
    }};


    private static String SUM_A="sum(A)";
    private static String SUM_B="sum(B)";
    private static String SUM_B_BY_SUM_A="sum(B)/Sum(A)";
    private static String SUM_B_BY_N="sum(B)/N";
    private static String SUM_A_BY_N="sum(A)/N";


    public DomainBasedMetrics(){

    }

    public static getResults(SearchRequestBuilder builder,JSONObject finalResponse,Map<String,String> params){
        List<String> unblindMemberIdList=new ArrayList<>();

        builder.addAggregation(AggregationBuilders.terms("members").field("unblindMemberId").size(0));
        SearchResponse response=(SearchResponse)BuilderAction.getResponse(builder,"metricsFromDomain");

        Terms terms=response.getAggregations().get("members");
        for (Terms.Bucket bucket  : terms.buckets) {
            unblindMemberIdList.add(bucket.getKey());
        }
        JSONObject jsonObject1=new JSONObject();
        String groupId=params.containsKey("group")?RequestUtils.getArrayRequestValue(params.get("group")):""
        List<CustomMetric> customMetricList=CustomMetric.findAll{groupId==groupId}
        for (CustomMetric customMetric : customMetricList) {
            JSONObject jsonObject=new JSONObject();

            String metricId=customMetric.metricId
            String metricName=customMetric.metricName

            jsonObject.put("formula",customMetric.formula)
            jsonObject.put("metricId",customMetric.metricId)
            jsonObject.put("metric",metricName)
            jsonObject.put("expectedDirection",customMetric.expectedDirection)
            jsonObject.put("unit",customMetric.metricUnit)
            jsonObject.put("groupId",customMetric.groupId)

            String formulaOverall=customMetric.formula

            for (String year : YEAR_AMP.keySet()) {
                def memberForCustomAndPeriod = MemberByCustomMetric.createCriteria()

                List<String> results = new ArrayList<>();
                results = memberForCustomAndPeriod.list {
                    and {
                        eq("metricId", metricId)
                        eq("period", year)
                        eq("groupId", groupId)
                    }
                    projections {
                        property('mbrId')
                    }
                }

                results.retainAll(unblindMemberIdList)
                def retainedMember = MemberByCustomMetric.createCriteria()

                String a="a"
                String b="b"
                String c="c"
                String d="d"

                Double aa=0
                Double bb=0
                Double cc=0
                Double dd=0

                int N=unblindMemberIdList.size()
                if (results.size() >0) {
                     def sumList = retainedMember.list {
                        and {
                            eq("metricId", metricId)
                            eq("period", year)
                        }
                        "in"("mbrId", results)

                        projections {
                            if(formulaOverall.equalsIgnoreCase(SUM_A) || formulaOverall.equalsIgnoreCase(SUM_A_BY_N)) sum(a)
                            else if(formulaOverall.equalsIgnoreCase(SUM_B_BY_SUM_A)) {
                                sum(b)
                                sum(a)
                            }
                        }
                    }

                    if(formulaOverall.equalsIgnoreCase(SUM_A) || formulaOverall.equalsIgnoreCase(SUM_A_BY_N)) aa=Double.valueOf(sumList[0])
                    else if(formulaOverall.equalsIgnoreCase(SUM_B_BY_SUM_A)) {
                        bb = Double.valueOf(Arrays.asList(sumList[0]).get(0))
                        aa = Double.valueOf(Arrays.asList(sumList[0]).get(1))
                    }
                }

                jsonObject.put("metricId", metricId)
                jsonObject.put("metricName", customMetric.metricName)
                jsonObject.put("year", YEAR_AMP.get(year))
                if(formulaOverall.equalsIgnoreCase(SUM_A) || formulaOverall.equalsIgnoreCase(SUM_A_BY_N)) jsonObject.put(YEAR_AMP.get(year)+"YearValue",aa)
                else if(formulaOverall.equalsIgnoreCase(SUM_B_BY_SUM_A)) jsonObject.put(YEAR_AMP.get(year)+"YearValue",CalculatorUtils.divide(bb,aa))

                if(formulaOverall.equalsIgnoreCase(SUM_A_BY_N))jsonObject.put(YEAR_AMP.get(year)+"metric",CalculatorUtils.calculatePercentage(aa,N))
                else if(formulaOverall.equalsIgnoreCase(SUM_B_BY_SUM_A)) jsonObject.put(YEAR_AMP.get(year) + "metric", CalculatorUtils.calculatePercentage(bb, aa))
                else if(formulaOverall.equalsIgnoreCase(SUM_A)) jsonObject.put(YEAR_AMP.get(year) + "metric",aa)

            }
            jsonObject.put("percentDiffInCurrent",CalculatorUtils.calculatePercentage(jsonObject.get("currentmetric")-jsonObject.get("lastmetric"),jsonObject.get("lastmetric")))
            jsonObject.put("percentDiffInLast",CalculatorUtils.calculatePercentage(jsonObject.get("lastmetric")-jsonObject.get("priormetric"),jsonObject.get("priormetric")))
            jsonObject1.put(metricId,jsonObject)

        }

        return finalResponse.put("newMetrics",jsonObject1)
    }
}
