package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.avg.AvgBuilder;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 1/17/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class ConcurrentRiskBuilderListener extends ProspectiveRiskBuilderListener{

    public ConcurrentRiskBuilderListener(SearchRequest searchRequest, List<String> memberList,
                                         JSONObject finalResponse, Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse,client,isComplete,dateGroup);
        this.reportName="concurrentRisk";
    }

    public ConcurrentRiskBuilderListener() {
        this.reportName="concurrentRisk";
    }

    @Override
    protected AvgBuilder getAvgFilter() {
        return AggregationBuilders.avg("avgScore").field("MaraHistoricalRiskScores.concurrentTotalNormalizedToBob");
    }
}
