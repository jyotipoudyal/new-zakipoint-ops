package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.Medical;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 1/2/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class MedicalPmpmBuilderListener extends PharmacyPmpmBuilderListener {

    public MedicalPmpmBuilderListener(SearchRequest searchRequest, List<String> memberList, JSONObject finalResponse,
                                      Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse, client, isComplete, dateGroup);
        this.reportName = "medicalPmpm";
    }

    public MedicalPmpmBuilderListener() {
        this.reportName = "medicalPmpm";
    }

    @Override
    protected void setTypes(SearchRequestBuilder builder) {
        builder.setTypes(Medical.NAME).setSize(0);
    }

}
