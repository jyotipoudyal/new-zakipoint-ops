package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.Medical;
import com.configurations.utils.Pharmacy;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.ZphSpecificMemberDrillExecutor;
import zakipointCore.commonUtils.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 1/17/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class ClaimsRangeBuilderListener extends ZphSpecificMemberDrillExecutor {

    public ClaimsRangeBuilderListener(SearchRequest searchRequest, List<String> memberList,
                                      JSONObject finalResponse, Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse, client, isComplete, dateGroup);
        this.reportName = "claimsOver10k";
    }

    public ClaimsRangeBuilderListener() {
        this.reportName = "claimsOver10k";
    }

    @Override
    public MultiSearchRequestBuilder constructDrillBuilder(MultiSearchRequestBuilder multiSearchRequestBuilder, RequestState state, List<String> memberList, Client client) {

        multiSearchRequestBuilder.add(createBuilder(state, memberList, client, Medical.NAME,"medicalClaims"));
        multiSearchRequestBuilder.add(createBuilder(state, memberList, client, Pharmacy.NAME,"pharmacyClaims"));
        return multiSearchRequestBuilder;
    }

    private SearchRequestBuilder createBuilder(RequestState state, List<String> memberList, Client client,String type,String aggName) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request,type);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        memberListFilter(andFilterBuilder, memberList,state.request.requestParameters);

        FilterUtils.paidServiceDateFilter(state, andFilterBuilder);
        LevelUtils.setLOAFilter(state, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        implementAgg(builder,aggName);
        return builder;
    }


    protected void implementAgg(SearchRequestBuilder builder,String aggName) {
            builder.addAggregation(AggregationBuilders.terms(aggName).field("claimNumber").size(0).order(Terms.Order.aggregation("totalPaidAmount",false))
                    .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT)));
    }


    @Override
    protected JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse, RequestState state) {

        String reportingPeriod = insertMetaInfo(response, null, state);
        JSONObject jsonObject1 = getInnerJsonObject(reportingPeriod);
        double memberMonths = (double) ((JSONObject) finalResponse.get("memberMonths")).get(reportingPeriod);

        int totalClaims = 0;
        for (Aggregation aggregation : response.getAggregations()) {

            Terms claims=(Terms) aggregation;
            for (Terms.Bucket bucket : claims.getBuckets()) {
                Sum claimAmount=bucket.getAggregations().get("totalPaidAmount");
                if(checkCondition(claimAmount)) totalClaims++;
                else break;
            }
            jsonObject1.put(claims.getName(), totalClaims);
            jsonObject1.put("memberMonths", memberMonths);
        }
        jsonObject.put(reportingPeriod, jsonObject1);
        return jsonObject;
    }

    protected boolean checkCondition(Sum claimAmount) {
        return claimAmount.getValue() > 1000000;
    }

    @Override
    protected void finalResponseGenerator(JSONObject thisResponse) {

        for (Object key : thisResponse.keySet()) {
            JSONObject innerObj=thisResponse.getJSONObject(key.toString());
            double totalClaim=0.0d;
            double memberMonths=innerObj.getDouble("memberMonths");
            for (Object innerKey : innerObj.keySet()) {
                if(innerKey.toString().contains("Claims")) totalClaim+=innerObj.getDouble(innerKey.toString());
            }
            innerObj.put("totalClaims",totalClaim);
            innerObj.put("totalClaimsPer1000",CalculatorUtils.perThousand(totalClaim,memberMonths));
        }
    }
}
