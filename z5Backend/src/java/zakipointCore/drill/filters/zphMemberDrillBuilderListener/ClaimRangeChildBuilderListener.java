package zakipointCore.drill.filters.zphMemberDrillBuilderListener;

import com.configurations.utils.Medical;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 1/17/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class ClaimRangeChildBuilderListener extends ClaimsRangeBuilderListener{


    public ClaimRangeChildBuilderListener(SearchRequest searchRequest, List<String> memberList,
                                          JSONObject finalResponse, Client client, AtomicBoolean isComplete, Map<String, String> dateGroup) {
        super(searchRequest, memberList, finalResponse,client,isComplete,dateGroup);
        this.reportName="claimsBelow500";
    }

    public ClaimRangeChildBuilderListener() {
        this.reportName="claimsBelow500";
    }


    @Override
    protected void implementAgg(SearchRequestBuilder builder, String aggName) {
        builder.addAggregation(AggregationBuilders.terms(aggName).field("claimNumber").size(0).order(Terms.Order.aggregation("totalPaidAmount",true))
                .subAggregation(AggregationBuilders.sum("totalPaidAmount").field(Medical.PAID_AMOUNT)));
    }

    @Override
    protected JSONObject constructDrillListener(SearchResponse response, JSONObject finalResponse, RequestState state) {
        return super.constructDrillListener(response,finalResponse,state);
    }

    @Override
    protected boolean checkCondition(Sum claimAmount) {
        return claimAmount.getValue() < 50000;
    }
}
