package zakipointCore.drill.filters;

import com.configurations.search.MemberSearchRequest;
import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearch;
import com.configurations.utils.MemberSearchQueryBuilder;
import com.configurations.utils.ReportFilter;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.DateUtils;
import zakipointCore.features.zPopulation.rootConditionAlongOtherChronicConditions.ChronicAndCormobiditiesPair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sktamang on 6/1/16.
 */
public class ChronicComorbiditiesDrillFilter implements ReportFilter {

    final String AGG_MEMBERS = "members";

    @Override
    public FilterBuilder createFilter(MemberSearchRequest searchRequest) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        FilterBuilder loaFilters = getNewFilter(searchRequest);
        List<String> stringList = createTermList(searchRequest);
        andFilterBuilder.add(FilterBuilders.termsFilter(MemberSearch.FIELD_INT_MEMBER_ID, stringList)).add(loaFilters);
        return andFilterBuilder;
    }


    public List<String> createTermList(MemberSearchRequest searchRequest) {

        SearchRequestBuilder builder = getBuilder(searchRequest);
        SearchResponse response = builder.execute().actionGet();
        List<String> memberList = new ArrayList<String>();

        Terms terms = response.getAggregations().get(AGG_MEMBERS);
        for (Terms.Bucket bucket : terms.getBuckets()) {
            memberList.add(bucket.getKey());
        }
        return memberList;
    }

    private SearchRequestBuilder getBuilder(MemberSearchRequest searchRequest) {

        SearchRequestBuilder requestBuilder = new MemberSearchQueryBuilder(searchRequest).setTypes().build().setSize(0);

        String comorbidValue = searchRequest.requestParameters.get("comorbidCode");
        int comorbidCode = 0;

        if (!comorbidValue.equalsIgnoreCase("only")) comorbidCode = Integer.valueOf(comorbidValue);

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        rootConditionAlongOtherConditionsAggregation(searchRequest, comorbidCode, andFilterBuilder);
        FilteredQueryBuilder filteredQuery = QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder);

        requestBuilder.setQuery(filteredQuery);
        requestBuilder.addAggregation(AggregationBuilders.terms(AGG_MEMBERS).field(MemberSearch.FIELD_INT_MEMBER_ID).size(0));
        return requestBuilder;
    }


    public static FilterBuilder getNewFilter(MemberSearchRequest memberSearchRequest) {

        MemberSearchRequest searchRequest = new MemberSearchRequest();
        searchRequest.requestParameters = (HashMap<String, String>) memberSearchRequest.requestParameters.clone();

        if (memberSearchRequest.hasParameter("report")) {
            searchRequest.requestParameters.remove("report");
        }

        MemberSearchQueryBuilder memberSearchQueryBuilder = new MemberSearchQueryBuilder(searchRequest);
        return memberSearchQueryBuilder.getFilter();
    }


    private void rootConditionAlongOtherConditionsAggregation(MemberSearchRequest searchRequest
            , int comorbidCode, AndFilterBuilder andFilter) {

        int chronicCode = Integer.valueOf(searchRequest.requestParameters.get("chronicCode"));
        if (comorbidCode == 0) {
            andFilter.add(FilterBuilders.notFilter(CommonQueries.newQmFilter(searchRequest.requestParameters.get("reportingTo"), ChronicAndCormobiditiesPair.CHRONIC_CORMOBIDITIES.get(chronicCode))));
            andFilter.add(CommonQueries.newQmFilter(searchRequest.requestParameters.get("reportingTo"), Arrays.asList(chronicCode)));
        }
        else andFilter.add(CommonQueries.newQmFilter(searchRequest.requestParameters.get("reportingTo"), Arrays.asList(comorbidCode)));
    }

    public static final String MEMBER_FIELD = "intMemberId";

    public static List<Integer> createMemberRangeBuilder(MemberSearchRequest memberSearchRequest) {
        SearchRequestBuilder builder = new MemberSearchQueryBuilder(memberSearchRequest).build();
        int range = Integer.parseInt(memberSearchRequest.requestParameters.get("range"));
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(FilterBuilders.rangeFilter("totalPaidAmount").gt(range).lte(null));


        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        paidMonth(memberSearchRequest, memberAmountFilter, "member_amount");
//        serviceMonth(state, memberAmountFilter, "member_amount");

        NestedFilterBuilder memberAmountNestedFilter = FilterBuilders.nestedFilter(MemberSearch.MEMBER_AMOUNT.NAME, memberAmountFilter);
        andFilterBuilder.add(memberAmountNestedFilter);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        builder.addAggregation(AggregationBuilders.terms("members").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0)
                .subAggregation(AggregationBuilders.nested("totalPaidAmount").path(MemberSearch.MEMBER_AMOUNT.NAME)
                        .subAggregation(AggregationBuilders.filter("loaFilters").filter(memberAmountFilter)
                                .subAggregation(AggregationBuilders.sum("paidAmountTotal").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT)))));
        return extractCaseMembers(builder, range);
    }

    private static List<Integer> extractCaseMembers(SearchRequestBuilder builder, int range) {
        SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "caseMembersGenerator");
        List<Integer> memberList = new ArrayList<>();
        for (Aggregation aggregation : response.getAggregations()) {
            Terms intMembers = (Terms) aggregation;
            for (Terms.Bucket bucket : intMembers.getBuckets()) {
                Nested totalPaidAmountNested = bucket.getAggregations().get("totalPaidAmount");
                Filter loaFilters = totalPaidAmountNested.getAggregations().get("loaFilters");
                Sum paidAmountTotal = loaFilters.getAggregations().get("paidAmountTotal");
                if (paidAmountTotal.getValue() > range) memberList.add(Integer.parseInt(bucket.getKey()));
            }
        }
        return memberList;
    }

    public static void paidMonth(MemberSearchRequest state, AndFilterBuilder filterBuilder, String type) {
        addRangeFilter(state.requestParameters.get("reportingTo"), state.requestParameters.get("reportingFrom"), type + "." + "paidMonth", filterBuilder);
    }

    public static void addRangeFilter(String periodTo, String periodFrom, String field, AndFilterBuilder filterBuilder) {
        filterBuilder.add(FilterBuilders.rangeFilter(field).to(DateUtils.getTimeFromDateWrtTimeZone(periodTo)).
                from(DateUtils.getTimeFromDateWrtTimeZone(periodFrom)));
    }

    public static AndFilterBuilder caseMemberFilter(MemberSearchRequest state, AndFilterBuilder andFilterBuilder) {
        if (state.requestParameters.containsKey("range")) {
            List<Integer> caseMembers;
            caseMembers = createMemberRangeBuilder(state);
            andFilterBuilder = implementCaseMembersFilters(caseMembers, andFilterBuilder);
        }
        return andFilterBuilder;
    }

    public static AndFilterBuilder implementCaseMembersFilters(List<Integer> caseMembers, AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.termsFilter(MEMBER_FIELD, caseMembers));
        return andFilterBuilder;
    }
}
