package zakipointCore.drill.filters;

import com.configurations.search.MemberSearchRequest;
import com.configurations.utils.*;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.builder.CommonQueries;
import zakipointCore.commonUtils.DateUtils;
import zakipointCore.features.zPopulation.useOfErByChronicMembers.NewUseOfErByChronicMembersBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sktamang on 5/31/16.
 */
public class ErDrillFilter implements ReportFilter {


    private final String AGG_MEMBERS = "members";
    private final String ER_VISIT = "erVisit";
    String tableName;
    public static final String MEMBER_FIELD = "intMemberId";


    @Override
    public FilterBuilder createFilter(MemberSearchRequest searchRequest) {
        FilterBuilder loaFilters = ChronicComorbiditiesDrillFilter.getNewFilter(searchRequest);
        List<String> stringList = createTermList(searchRequest);
        return FilterBuilders.andFilter(FilterBuilders.termsFilter(CommonQueries.getFieldIntMemberId(searchRequest.requestParameters), stringList), loaFilters);
    }



    public List<String> createTermList(MemberSearchRequest searchRequest) {

        SearchRequestBuilder builder = getBuilder(searchRequest);
        SearchResponse response = builder.execute().actionGet();
        searchRequest.requestParameters.put("table", tableName);
        List<String> memberList = new ArrayList<String>();

        Terms terms = response.getAggregations().get(AGG_MEMBERS);
        if (!containsErVisit(searchRequest)) {
            for (Terms.Bucket bucket : terms.getBuckets()) {
                memberList.add(bucket.getKey().trim());
            }
        } else {
            String erVisitCount = searchRequest.requestParameters.get("erVisit");
            for (Terms.Bucket bucket : terms.getBuckets()) {
                Sum erVisit = bucket.getAggregations().get(ER_VISIT);
            }
        }
        return memberList;
    }


    private SearchRequestBuilder getBuilder(MemberSearchRequest searchRequest) {

        tableName = searchRequest.requestParameters.get("table");
        if (searchRequest.requestParameters.containsKey("recordTypes"))
            searchRequest.requestParameters.put("table", searchRequest.requestParameters.get("recordTypes"));

        SearchRequestBuilder requestBuilder = new MemberSearchQueryBuilder(searchRequest).setTypes().build().setSize(0);
        searchRequest.requestParameters.put("query", searchRequest.requestParameters.get("dataSearchQuery"));

        FilterBuilder filter = ChronicComorbiditiesDrillFilter.getNewFilter(searchRequest);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder.add(filter);

        featureSpecificFilters(andFilterBuilder, searchRequest);

        FilteredQueryBuilder filteredQuery = QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder);
        requestBuilder.setQuery(filteredQuery);

        AggregationBuilder termsForMemberId;
                if(CommonQueries.useMemberIdInAggInExpPop(searchRequest.requestParameters)) termsForMemberId=AggregationBuilders.terms(AGG_MEMBERS).field(CommonQueries.getFieldIntMemberId(searchRequest.requestParameters)).size(0);
                else termsForMemberId=AggregationBuilders.terms(AGG_MEMBERS).field(MemberSearch.FIELD_INT_MEMBER_ID).size(0);
        if (containsErVisit(searchRequest))
            termsForMemberId.subAggregation(AggregationBuilders.sum(ER_VISIT).field(VisitAdmission.ER_VISIT));
        requestBuilder.addAggregation(termsForMemberId);
        return requestBuilder;
    }

    private boolean containsErVisit(MemberSearchRequest searchRequest) {
        return searchRequest.requestParameters.containsKey("erVisit");
    }


    private void featureSpecificFilters(AndFilterBuilder andFilterBuilder, MemberSearchRequest searchRequest) {

        if (searchRequest.requestParameters.containsKey("feature")) {
            if (searchRequest.requestParameters.get("feature").equalsIgnoreCase("useOfErByChronicMembers")) {
                String chroniCode = searchRequest.requestParameters.get("chronicCode");
                String filterTerm = NewUseOfErByChronicMembersBuilder.QM_ESFIELD_HASMETRIC_INFO.get(chroniCode);
                andFilterBuilder.add(FilterBuilders.hasParentFilter(MemberSearch.NAME, FilterBuilders.termFilter(filterTerm, "Yes")));
            }
        }
    }


    public static List<Integer> createMemberRangeBuilder(MemberSearchRequest memberSearchRequest) {

        SearchRequestBuilder builder = new MemberSearchQueryBuilder(memberSearchRequest).build();
        int range = Integer.parseInt(memberSearchRequest.requestParameters.get("range"));
        String includeExclude = memberSearchRequest.requestParameters.get("isExclude");

        boolean isExclude = false;
        if (includeExclude.equalsIgnoreCase("true")) isExclude = true;

        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();

        AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
        paidMonth(memberSearchRequest, memberAmountFilter, "member_amount");
        serviceMonth(memberSearchRequest, memberAmountFilter, "member_amount");

        NestedFilterBuilder memberAmountNestedFilter = FilterBuilders.nestedFilter(MemberSearch.MEMBER_AMOUNT.NAME, memberAmountFilter);
        andFilterBuilder.add(memberAmountNestedFilter);

        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder)).setSize(0);
        builder.addAggregation(AggregationBuilders.terms("members").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0)
                .subAggregation(AggregationBuilders.nested("totalPaidAmount").path(MemberSearch.MEMBER_AMOUNT.NAME)
                        .subAggregation(AggregationBuilders.filter("loaFilters").filter(memberAmountFilter)
                                .subAggregation(AggregationBuilders.sum("paidAmountTotal").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT)))));
        return extractCaseMembers(builder, range, isExclude);
    }


    private static List<Integer> extractCaseMembers(SearchRequestBuilder builder, int range, boolean isExclude) {
        SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "caseMembersGenerator");
        List<Integer> memberList = new ArrayList<>();
        CommonQueries.CostWiseMemberFilter.memberExtract(range, isExclude, response, memberList);
        return memberList;
    }

    public static void serviceMonth(MemberSearchRequest state, AndFilterBuilder filterBuilder, String type) {
        String basis = parseBasis(state);

        if (basis.equals("serviceDate")) {
            addRangeFilter(state.requestParameters.get("reportingTo"), state.requestParameters.get("reportingFrom"), type + "." + "serviceMonth", filterBuilder);
        }
    }

    public static String parseBasis(MemberSearchRequest request) {
        String reportingBasis = request.hasParameter("reportingBasis") ? request.get("reportingBasis") : "paidDate";
        return reportingBasis.equalsIgnoreCase("serviceDate") ? "serviceDate" : "paidDate";
    }

    public static void paidMonth(MemberSearchRequest state, AndFilterBuilder filterBuilder, String type) {
        addRangeFilter(state.requestParameters.get("reportingTo"), state.requestParameters.get("reportingFrom"), type + "." + "paidMonth", filterBuilder);
    }


    public static void addRangeFilter(String periodTo, String periodFrom, String field, AndFilterBuilder filterBuilder) {
        filterBuilder.add(FilterBuilders.rangeFilter(field).to(DateUtils.getTimeFromDateWrtTimeZone(periodTo)).
                from(DateUtils.getTimeFromDateWrtTimeZone(periodFrom)));
    }


    public static AndFilterBuilder caseMemberFilter(MemberSearchRequest state, AndFilterBuilder andFilterBuilder) {
        if (state.requestParameters.containsKey("range")) {
            List<Integer> caseMembers;
            caseMembers = createMemberRangeBuilder(state);
            andFilterBuilder = implementCaseMembersFilters(caseMembers, andFilterBuilder);
        }
        return andFilterBuilder;
    }


    public static AndFilterBuilder implementCaseMembersFilters(List<Integer> caseMembers, AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.termsFilter(MEMBER_FIELD, caseMembers));
        return andFilterBuilder;
    }
}
