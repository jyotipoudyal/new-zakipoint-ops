package zakipointCore.drill;


import zakipointCore.drill.filters.zphMemberDrillBuilderListener.*;

import java.util.Map;

/**
 * Created by sktamang on 4/10/17.
 * Sanjeev Kumar Tamang
 * email : tamang88sanjeev@gmail.com
 */
public class ClassLoader {

    public static void loadClasses(String requestIndex, Map<String, Object> CLASSES) {

        switch (requestIndex) {
            case "2":
                CLASSES.put("changeInPmpm", new ChangeInPmpmBuilderListener());
                CLASSES.put("pharmacyPmpm", new PharmacyPmpmBuilderListener());
                CLASSES.put("medicalPmpm", new MedicalPmpmBuilderListener());
                break;
            case "3":
                CLASSES.put("prospectiveRisk", new ProspectiveRiskBuilderListener());
                CLASSES.put("concurrentRisk", new ConcurrentRiskBuilderListener());
                break;
            case "4":
                CLASSES.put("claimsOver10k", new ClaimsRangeBuilderListener());
                CLASSES.put("claimsBelow10k", new ClaimRangeChildBuilderListener());
                break;
            case "5":
                CLASSES.put("erVisitAndAdmissionUsage", new ErVisitAndAdmissionUsageBuilderListener());
                break;
            case "6":
                CLASSES.put("mailGenericOrderPrescription", new MailGenericOrderPrescriptionBuilderListener());
                break;
            case "7":
                CLASSES.put("relationshipClass", new RelationshipclassBuilderListener());
                break;
            default:
                break;
        }
    }
}
