package zakipointCore.builder;

import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import zakipointCore.commonUtils.RequestState;

/**
 * Created by sktamang on 9/24/15.
 */
public interface SummaryQueryBuilder {
    public MultiSearchRequestBuilder multiSearchQuery(RequestState state, Client client);
    public SearchRequestBuilder query(RequestState state, Client client);
}
