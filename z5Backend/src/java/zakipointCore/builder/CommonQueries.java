package zakipointCore.builder;

import com.cohort.MembersByCohort;
import com.configurations.MemberSearchQueries;
import com.configurations.utils.BuilderAction;
import com.configurations.utils.MemberSearch;
import com.configurations.utils.VisitAdmission;
import org.elasticsearch.action.ActionRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.Filter;
import org.elasticsearch.search.aggregations.bucket.filters.Filters;
import org.elasticsearch.search.aggregations.bucket.filters.FiltersAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import zakipointCore.commonUtils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by sktamang on 9/24/15.
 */
public class CommonQueries {

    public static AbstractAggregationBuilder memberMonthsAggs(RequestState state, String aggName) {
        String paramFrom = state.periodFrom();
        String paramTo = state.periodTo();
        FiltersAggregationBuilder filtersAggs = AggregationBuilders.filters(aggName);

        for (String month : DateUtils.getMonthsBetween(paramFrom, paramTo)) {
            AndFilterBuilder andFilter = new AndFilterBuilder();
            LevelUtils.setLevelFilter(state, andFilter, month);
            andFilter.add(FilterUtils.eligibleDateFilter(month));
            filtersAggs.filter(DateUtils.getMonthOnlyForDate(month), andFilter);
        }

        return filtersAggs;
    }


    public static AbstractAggregationBuilder employeeMonthsAggs(RequestState state, String aggName) {
        String paramFrom = state.periodFrom();
        String paramTo = state.periodTo();
        FiltersAggregationBuilder filtersAggs = AggregationBuilders.filters(aggName);

        for (String month : DateUtils.getMonthsBetween(paramFrom, paramTo)) {
            AndFilterBuilder andFilter = new AndFilterBuilder();
            andFilter.add(FilterUtils.eligibleDateFilter(month));
            andFilter.add(MemberSearchQueries.eligibleRelationshipBuilder(month));
            filtersAggs.filter(month, andFilter);
        }
        return filtersAggs;
    }


    public static SearchRequestBuilder memberMonthsBuilder(RequestState state, Client client) {
        SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        andFilterBuilder = CommonQueries.caseMemberFilter(state, client, andFilterBuilder);
        builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
        FiltersAggregationBuilder memberMonthFilters = (FiltersAggregationBuilder) memberMonthsAggs(state, "memberMonthsFilter");
        builder.addAggregation(memberMonthFilters);
        return builder;
    }


    public static double getTotalMemberMonth(RequestState state, Client client, double memberMonths) {
        SearchRequestBuilder builder = memberMonthsBuilder(state, client);
        for (Aggregation aggregation : builder.get().getAggregations()) {
            Filters memberMonthFilters = (Filters) aggregation;
            for (Filters.Bucket bucket : memberMonthFilters.getBuckets()) {
                memberMonths = memberMonths + bucket.getDocCount();
            }
        }
        return memberMonths;
    }


    public static double getMemberCount(RequestState state, Client client, double memberCount) {
        SearchRequestBuilder builder = memberMonthsBuilder(state, client);
        for (Aggregation aggregation : builder.get().getAggregations()) {
            Filters memberMonthFilters = (Filters) aggregation;
            for (Filters.Bucket bucket : memberMonthFilters.getBuckets()) {
                if(state.periodTo().contains(bucket.getKey()))memberCount =bucket.getDocCount();
            }
        }
        return memberCount;
    }


    public static Map<String, Double> getMemberMonthMap(RequestState state, Client client, Map<String, Double> memberMonthMap) {
        SearchRequestBuilder builder = memberMonthsBuilder(state, client);
        for (Aggregation aggregation : builder.get().getAggregations()) {
            Filters memberMonthFilters = (Filters) aggregation;
            for (Filters.Bucket bucket : memberMonthFilters.getBuckets()) {
                memberMonthMap.put(bucket.getKey(), new Double(bucket.getDocCount()));
            }
        }
        return memberMonthMap;
    }


    public static AndFilterBuilder implementCaseMembersFilters(List<Integer> caseMembers, AndFilterBuilder andFilterBuilder) {
        andFilterBuilder.add(FilterBuilders.termsFilter(CostWiseMemberFilter.MEMBER_FIELD, caseMembers));
        return andFilterBuilder;
    }


    public static AndFilterBuilder caseMemberFilter(RequestState state, Client client, AndFilterBuilder andFilterBuilder) {
        if (state.request.hasParam("range")) {

            int range=Integer.valueOf(state.request.get("range"));
            String includeExclude=state.request.get("isExclude");
            boolean isExclude=false;
            if(includeExclude.equalsIgnoreCase("true")) isExclude=true;

            if(range>0 &&  isExclude) {
                List<Integer> caseMembers;
                caseMembers = CostWiseMemberFilter.createBuilder(state, client);
                andFilterBuilder = CommonQueries.implementCaseMembersFilters(caseMembers, andFilterBuilder);
            }
        }
        return andFilterBuilder;
    }


    public static class CostWiseMemberFilter {

        public static final String MEMBER_FIELD = "intMemberId";

        public static List<Integer> createBuilder(RequestState state, Client client) {

            SearchRequestBuilder builder = QueryUtils.buildEndSearchWihType(client, state.request, MemberSearch.NAME);
            int range = Integer.parseInt(state.request.get("range"));
            String includeExclude=state.request.get("isExclude");

            boolean isExclude=false;
            if(includeExclude.equalsIgnoreCase("true")) isExclude=true;

            AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
            LevelUtils.setLevelFilter(state, andFilterBuilder, state.periodTo());


            AndFilterBuilder memberAmountFilter = new AndFilterBuilder();
            paidMonth(state, memberAmountFilter, "member_amount");
            serviceMonth(state, memberAmountFilter, "member_amount");

            NestedFilterBuilder memberAmountNestedFilter = FilterBuilders.nestedFilter(MemberSearch.MEMBER_AMOUNT.NAME, memberAmountFilter);
            andFilterBuilder.add(memberAmountNestedFilter);

            builder.setQuery(QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), andFilterBuilder));
            builder.addAggregation(AggregationBuilders.terms("members").field(MemberSearch.FIELD_INT_MEMBER_ID).size(0)
                    .subAggregation(AggregationBuilders.nested("totalPaidAmount").path(MemberSearch.MEMBER_AMOUNT.NAME)
                            .subAggregation(AggregationBuilders.filter("loaFilters").filter(memberAmountFilter)
                                    .subAggregation(AggregationBuilders.sum("paidAmountTotal").field(MemberSearch.MEMBER_AMOUNT.PAID_AMOUNT)))));
            return extractCaseMembers(builder, range,isExclude);
        }


        public static List<Integer> extractCaseMembers(ActionRequestBuilder builder, int range, boolean isExclude) {

            SearchResponse response = (SearchResponse) BuilderAction.getResponse(builder, "caseMembersGenerator");
            List<Integer> memberList = new ArrayList<>();

            memberExtract(range, isExclude, response, memberList);
            return memberList;
        }

        public static void memberExtract(int range, boolean isExclude, SearchResponse response, List<Integer> memberList) {
            for (Aggregation aggregation : response.getAggregations()) {
                Terms intMembers = (Terms) aggregation;

                for (Terms.Bucket bucket : intMembers.getBuckets()) {
                    Nested totalPaidAmountNested = bucket.getAggregations().get("totalPaidAmount");
                    Filter loaFilters = totalPaidAmountNested.getAggregations().get("loaFilters");
                    Sum paidAmountTotal = loaFilters.getAggregations().get("paidAmountTotal");

                    if (paidAmountTotal.getValue() > range && !isExclude) memberList.add(Integer.parseInt(bucket.getKey()));
                    else if(paidAmountTotal.getValue() <= range && isExclude) memberList.add(Integer.parseInt(bucket.getKey()));
                }
            }
        }
    }


    public static void serviceMonth(RequestState state, AndFilterBuilder filterBuilder, String type) {
        String basis = QueryUtils.parseBasis(state.request);

        if (basis.equals("serviceDate")) {
            addRangeFilter(state.periodTo(), state.periodFrom(), type + "." + "serviceMonth", filterBuilder);
        }
    }


    public static void paidMonth(RequestState state, AndFilterBuilder filterBuilder, String type) {
        addRangeFilter(state.periodTo(), state.periodFrom(), type + "." + "paidMonth", filterBuilder);
    }


    public static void addRangeFilter(String periodTo, String periodFrom, String field, AndFilterBuilder filterBuilder) {
        filterBuilder.add(FilterBuilders.rangeFilter(field).to(DateUtils.getTimeFromDateWrtTimeZone(periodTo)).
                from(DateUtils.getTimeFromDateWrtTimeZone(periodFrom)));
    }

    public static NestedBuilder nestedVisitAmountSum(RequestState state) {
        AndFilterBuilder andFilterBuilder = new AndFilterBuilder();
        FilterUtils.nestedPaidDateFilter(state, VisitAdmission.VisitAmounts.NAME, andFilterBuilder);
        return AggregationBuilders.nested("nestedVisitAmounts").path(VisitAdmission.VisitAmounts.NAME)
                .subAggregation(AggregationBuilders.filter("visitAmountFilter").filter(andFilterBuilder)
                        .subAggregation(AggregationBuilders.sum("totalPaid").field(VisitAdmission.VisitAmounts.TOTAL_PAID)));
    }

    public static FilterBuilder newQmFilter(String month, Object... metric) {

        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.termsFilter("qm.measure", metric));
        andFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));
        andFilter.add(FilterUtils.newQmRangeFilter(month));

        return FilterBuilders.nestedFilter("qm", andFilter);
    }

    public static void getMemberMonthsFilter(RequestState state, String paramFrom, String paramTo, FiltersAggregationBuilder filtersAggs, Object[] chronicCode) {
        for (String month : DateUtils.getMonthsBetween(paramFrom, paramTo)) {
            AndFilterBuilder andFilter = new AndFilterBuilder();
            LevelUtils.setLevelFilter(state, andFilter, month);
            andFilter.add(FilterUtils.eligibleDateFilter(month));
            andFilter.add(CommonQueries.newQmFilter(month, chronicCode));
            filtersAggs.filter(month, andFilter);
        }
    }


    public static FilterBuilder newQmFilter(String month, List<Integer> metric) {
        AndFilterBuilder andFilter = new AndFilterBuilder();
        andFilter.add(FilterBuilders.termsFilter("qm.measure", metric));
        andFilter.add(FilterUtils.getTermsFilter("qm.numerator", 0, 1));
        andFilter.add(FilterUtils.newQmRangeFilter(month));
        return FilterBuilders.nestedFilter("qm", andFilter);
    }

    public static boolean containsCohort(Map<String,String> requestMap){
        return requestMap.containsKey("cohortKey");
    }


    public static void cohortFilter(AndFilterBuilder filterBuilder,Map<String,String> requestParameters) {

        if(CommonQueries.containsCohort(requestParameters)){
            List<String> memberList = new ArrayList<>();
            String cohortKey=requestParameters.get("cohortKey");
            MembersByCohort obj=new MembersByCohort();
            List<MembersByCohort> list= (List<MembersByCohort>) obj.getById(cohortKey,"cohort_id");
            for (MembersByCohort membersByCohort : list) {
                memberList.add(membersByCohort.getMbrId());
            }

            String index=requestParameters.get("clientId");
            String field=MemberSearch.FIELD_UNBLIND_MEMBER_ID;
            if(index.contains("30780002")) field=MemberSearch.MEMBER_ID;
            filterBuilder.add(FilterBuilders.termsFilter(field, memberList));
        }

//        if(requestParameters.containsKey("task")) filterBuilder.add(FilterBuilders.termsFilter(MemberSearch.FIELD_UNBLIND_MEMBER_ID,))


    }

    public static void reportFromDomainFilter(AndFilterBuilder filterBuilder,List<String> memberList) {
      filterBuilder.add(FilterBuilders.termsFilter(MemberSearch.FIELD_UNBLIND_MEMBER_ID,memberList.toArray()));
    }

    public static boolean containsRange(Map<String,String> requestMap){
        return requestMap.containsKey("range");
    }

    public static boolean useMemberIdInAggInExpPop(Map<String,String> requestParams){
        return requestParams.containsKey("useMemberId");
    }

    public static String getFieldIntMemberId(Map<String,String> requestMap) {
        if(CommonQueries.useMemberIdInAggInExpPop(requestMap)) return MemberSearch.FIELD_UNBLIND_MEMBER_ID;
        else return MemberSearch.FIELD_INT_MEMBER_ID;
    }

    public static AndFilterBuilder getFilterForReportFromDatabase(Map<String,String> map){
        String report=map.get("domainTask");
        ReportFromDomainBuilders builder=SpringBeanUtils.getBean(report);
        List<String> memberList= (List<String>) builder.getDrillResults(map);
        return  new AndFilterBuilder().add(FilterBuilders.termsFilter(MemberSearch.FIELD_UNBLIND_MEMBER_ID,memberList.toArray()));
    }
}
