package zakipointCore.builder;

import com.configurations.utils.BuilderAction;
import org.codehaus.groovy.grails.web.json.JSONObject;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.ActionResponse;
import org.elasticsearch.action.search.MultiSearchRequestBuilder;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.AndFilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import zakipointCore.commonUtils.QueryUtils;
import zakipointCore.commonUtils.RequestState;
import zakipointCore.commonUtils.SearchRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sktamang on 6/21/16.
 */
public  class ZphSpecificMemberDrillExecutor {

    SearchRequest searchRequest;

    public void setSearchRequest(SearchRequest searchRequest) {
        this.searchRequest = searchRequest;
    }

    public void setMemberList(List<String> memberList) {
        this.memberList = memberList;
    }

    public void setFinalResponse(JSONObject finalResponse) {
        this.finalResponse = finalResponse;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setProcessComplete(AtomicBoolean processComplete) {
        this.processComplete = processComplete;
    }

    public void setDateGroup(Map<String, String> dateGroup) {
        this.dateGroup = dateGroup;
    }

    List<String> memberList =null;
    JSONObject finalResponse=null;
    Client client;
    public AtomicBoolean processComplete;
    Map<String,String> dateGroup;
    protected  JSONObject jsonObject;
    protected String reportName="areasToTarget";
    public boolean isSuccess = false;
    Map<String,RequestState> requestStateMap;

    public ZphSpecificMemberDrillExecutor(SearchRequest searchRequest, List<String> memberList, JSONObject finalResponse,
                                          Client client, AtomicBoolean isComplete,Map<String,String> dateGroup){
        this.searchRequest=searchRequest;
        this.memberList=memberList;
        this.finalResponse=finalResponse;
        this.client=client;
        this.processComplete=isComplete;
        this.dateGroup=dateGroup;
    }

    public ZphSpecificMemberDrillExecutor(){

    }

    public JSONObject execute(){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        FutureTask<JSONObject> futureTask = new FutureTask<JSONObject>(new Executor(memberList,finalResponse));
        futureTask.run();
        executorService.execute(futureTask);
        try {
            finalResponse = futureTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        isSuccess = futureTask.isDone();
        return finalResponse;
    }


    public  class Executor implements Callable<JSONObject> {
        List<String> memberList;
        JSONObject finalResponse;

        public Executor( List<String> memberList, JSONObject finalResponse) {
            this.memberList = memberList;
            this.finalResponse = finalResponse;
        }

        @Override
        public JSONObject call() throws Exception {
            ActionResponse response = null;
            MultiSearchRequestBuilder builder=null;
            try {
                for (Map.Entry<String, String> dates : dateGroup.entrySet()) {

                    String fromDate=dates.getValue().split("__")[0];
                    String toDate=dates.getValue().split("__")[1];
                    MultiSearchRequestBuilder multiSearchRequestBuilder= QueryUtils.buildMultiSearch(client);

                    searchRequest.requestParameters.put("reportingFrom", fromDate);
                    searchRequest.requestParameters.put("reportingTo", toDate);
                    searchRequest.requestParameters.put("reportingPeriod", dates.getKey());

                    RequestState state = new RequestState(reportName, "", searchRequest, null);
                    builder=constructDrillBuilder(multiSearchRequestBuilder,state,memberList,client);
                    response = BuilderAction.executeBuilder(builder,reportName);
                    processResponse(response, finalResponse,state);
                }
                finalResponseGenerator(jsonObject);


            }catch (ElasticsearchException e) {
//                BuilderAction.print(reportName, builder);
            }catch (Exception e){
                System.out.println(e);
            }
            while (!processComplete.get());
            return finalResponse;
        }
    }


    protected void processResponse(ActionResponse response,JSONObject finalResponse,RequestState state) {
        MultiSearchResponse response1 = (MultiSearchResponse) response;
        jsonObject=getJsonObject(reportName);
        for (MultiSearchResponse.Item item : response1.getResponses()) {
            if (item.isFailure()) {
                System.out.println("the error is = " + item.getFailureMessage());
            }
            else {
                constructDrillListener(item.getResponse(), finalResponse,state);
            }
        }
        processComplete.set(true);
    }

    protected void finalResponseGenerator(JSONObject thisJson) {}


    protected JSONObject getJsonObject(String key){
        if(finalResponse.containsKey(key)) return (JSONObject) finalResponse.get(key);
        else {
            finalResponse.put(key,new JSONObject());
            return (JSONObject) finalResponse.get(key);
        }
    }

    protected JSONObject getInnerJsonObject(String key){
        if(jsonObject.containsKey(key)) return (JSONObject) jsonObject.get(key);
        else {
            jsonObject.put(key,new JSONObject());
            return (JSONObject) jsonObject.get(key);
        }
    }

    public static void memberListFilter(AndFilterBuilder andFilterBuilder,List<String> memberList,Map<String,String> requestMap){
        if(CommonQueries.containsCohort(requestMap)) CommonQueries.cohortFilter(andFilterBuilder,requestMap);
        else if(requestMap.containsKey("task") && requestMap.get("task").equalsIgnoreCase("reportFromDomain")) CommonQueries.reportFromDomainFilter(andFilterBuilder,memberList);
        else andFilterBuilder.add(FilterBuilders.termsFilter(CommonQueries.getFieldIntMemberId(requestMap),memberList));
    }

    protected String insertMetaInfo(SearchResponse response, JSONObject jsonObject1,RequestState state) {
        String reportingPeriod=state.request.requestParameters.get("reportingPeriod");
//        String reportingFrom=searchHit.field("reportingFrom").getValue();
//        String reportingTo=searchHit.field("reportingTo").getValue();
//
//        jsonObject1.put("reportingPeriod",reportingPeriod);
//        jsonObject1.put("reportingFrom",reportingFrom);
//        jsonObject1.put("reportingTo",reportingTo);
        return reportingPeriod;
    }

    protected void addScripts(RequestState state, SearchRequestBuilder builder, String reportingPeriod) {
        builder.addScriptField("reportingPeriod", "\"" + reportingPeriod + "\"");
        builder.addScriptField("reportingFrom", "\"" + state.request.requestParameters.get("reportingFrom") + "\"");
        builder.addScriptField("reportingTo", "\"" + state.request.requestParameters.get("reportingTo") + "\"");
        return;
    }


    public  MultiSearchRequestBuilder constructDrillBuilder(MultiSearchRequestBuilder multiSearchRequestBuilder,RequestState state, List<String> memberList,Client client){
        return null;
    }
    protected  JSONObject constructDrillListener(SearchResponse response,JSONObject finalResponse,RequestState state){
        return null;
    }
}
