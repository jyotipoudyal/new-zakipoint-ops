package zakipointCore.builder

import org.codehaus.groovy.grails.web.json.JSONObject


/**
 * Created by sktamang on 3/3/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
public interface ReportFromDomainBuilders {

    public JSONObject getResults(Map<String,String> requestParams);

    public Object getDrillResults(Map<String,String> requestParams);

}
