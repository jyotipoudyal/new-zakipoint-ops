package com.zakipoint.service

import com.configurations.config.AbstractConfigurationManager
import org.codehaus.groovy.grails.web.converters.exceptions.ConverterException
import org.codehaus.groovy.grails.web.json.JSONObject
import org.elasticsearch.action.ActionRequestBuilder
import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.logging.ESLogger
import org.elasticsearch.common.logging.Loggers
import org.elasticsearch.common.settings.ImmutableSettings
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.springframework.beans.factory.annotation.Autowired
import zakipointCore.builder.SummaryQueryBuilder
import zakipointCore.commonUtils.BuilderExecutor
import zakipointCore.commonUtils.ReportGenerator
import zakipointCore.commonUtils.ReportUtils
import zakipointCore.commonUtils.RequestState
import zakipointCore.commonUtils.SearchRequest
import zakipointCore.commonUtils.SpringBeanUtils
import zakipointCore.listener.AbstractSummaryListener

import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by sktamang on 9/24/15.
 */
class ZakiPointService {

    static transactional = true
    def grailsApplication
    SearchRequest searchRequest

    protected Client client
    protected ESLogger logger
    protected Settings settings
    protected String[] DEFAULT_TYPES = ["Medical"]
    @Autowired AbstractConfigurationManager configurationManager

    private final String LISTENER = "listener"
    private final String BUILDER = "builder"


    String getResults(SearchRequest searchRequest) {
//        String esClusterName = !configurationManager.getClusterConfig(ConfigFileName.fileName).getClusterName().isEmpty() ? configurationManager.getClusterConfig(ConfigFileName.fileName).getClusterName() : ClusterConfig.ES_DEFAULT_CLUSTER_NAME ;
        //todo check if this works
        String esClusterName = "zakipoint_es"
        Settings settings    = ImmutableSettings.settingsBuilder().put("cluster.name",esClusterName).build();
        Client client        = new TransportClient(settings).addTransportAddress(new InetSocketTransportAddress("172.19.0.74", 9300));
        try{
            JSONObject finalObj=ReportUtils.generateReport(searchRequest.requestParameters,new ReportGenerator() {
                @Override
                JSONObject generate(Map<String, String> map) throws ConverterException{
                    SearchRequest sr= new SearchRequest(requestParameters: map);
                    String res= handleRequest(sr, client, settings)
                    JSONObject toSend=new JSONObject(res);
                    return toSend;
                }
            });
            return finalObj.toString()
        }
        catch (e){
            throw new RuntimeException(e)
        }
    }

    private Client getClient() {
        return new TransportClient(settings);
    }

    public JSONObject handleRequest(final SearchRequest request, Client client, Settings settings) throws Exception  {
        this.client=client;
        this.settings=settings

        String[] types = getType(request)
        String report  = getReportName(request)
        final String field = getField(request)

        JSONObject content = handleInternal(request, field, report, types);

        return content  ? content : new JSONObject();
    }

    protected JSONObject handleInternal(final SearchRequest request, final String field, String report, String... types) throws Exception {
        AtomicBoolean process = new AtomicBoolean(false);
        ActionRequestBuilder builder;
        AbstractSummaryListener top = newListener(field, request, report, types, process);
        SummaryQueryBuilder defaultBuilder=getBuilder(report)
        builder=getRequestBuilder(defaultBuilder,report,top)
        BuilderExecutor executor=new BuilderExecutor(builder,top);
        return executor.execute().response;
    }
//
    def AbstractSummaryListener newListener(String field, SearchRequest request, String report, String[] types, AtomicBoolean processComplete) throws Exception {
        this.logger = Loggers.getLogger(AbstractSummaryListener.class.getName(), settings);
        AbstractSummaryListener listener = getListener(report);
        println "re = $report"
        RequestState state = new RequestState(report, field, request, types);
        state.noFilter = false;
        initializeListener(listener, state, report, processComplete)
        return listener;
    }

    SummaryQueryBuilder getBuilder(String report) {
        String builderName=getBuilderName(report)
        if (!SpringBeanUtils.doesBeanExist(builderName)){
            throw new RuntimeException(builderName+" builder doesn't exists")
        }
        return (SummaryQueryBuilder)SpringBeanUtils.getBean(builderName)
    }


    protected ActionRequestBuilder getRequestBuilder(SummaryQueryBuilder builder, String report, AbstractSummaryListener top){
            return builder.multiSearchQuery(top.state, client);
    }
//
    protected String getField(SearchRequest request) {
        request.hasParameter("keyField") ? request.get("keyField") : "memberId"
    }

    public String getReportName(SearchRequest request) {
        println request.requestParameters
        request.hasParameter("report") ? request.get("report") : "submittedDiscounts"
    }

    protected String[] getType(SearchRequest request) {
        request.hasParameter("recordType") ? request.get("recordType").split(",") : DEFAULT_TYPES
    }

    protected String getBuilderName(String report) {
        return getBuilderListenerName(report,BUILDER)
    }
    protected String getListenerName(String report) {
        return getBuilderListenerName(report,LISTENER)
    }
    protected String getBuilderListenerName(String report,String builderListener) {
        StringBuilder builder=new StringBuilder();
        builder.append(report.trim())
        if (!getPrefix().equals("")){
            builder.append(".")
            builder.append(getPrefix())
        }
        builder.append(".").append(builderListener)
        return builder.toString()
    }

    AbstractSummaryListener getListener(String report) {
        String listenerName=getListenerName(report)
        if (!SpringBeanUtils.doesBeanExist(listenerName)){
            throw new RuntimeException(listenerName+" listener doesn't exists")
        }
        return (AbstractSummaryListener)SpringBeanUtils.getBean(listenerName)
    }

    private void initializeListener(AbstractSummaryListener listener, RequestState state, String report, AtomicBoolean processComplete) {
        listener.setClient(client);
        listener.setState(state);
        listener.setLogger(logger);
        listener.setReport(report);
        listener.processComplete = processComplete;
    }

    String getPrefix() {
        return ""
    }
}
