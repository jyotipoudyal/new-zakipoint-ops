package com.zakipoint.service

import com.cohort.*
import com.configurations.utils.DomainOperations
import grails.converters.JSON
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import zakipointCore.commonUtils.CalculatorUtils

@Transactional
class CohortService {
    static List<String> LOA_LIST=Arrays.asList("clientId","groupId","showInIdentity","comparatorId");
    static List<String> LOA_LIST_COHORT_COMPARATOR=Arrays.asList("groupId","comparatorId");

    public static Map<String,Object> TABLE_CODE_MAP=new HashMap<String,Object>(){{
        put("cohortComparator", new CohortComparator());
        put("cohortsByComparator", new CohortsByComparator());
    }};

    public static Map<String,String> YEAR_AMP=new HashMap<String,String>(){{
        put("1", "current");
        put("2", "last");
        put("3", "prior");
    }};

    static getCohortList(Map<String,String> params){

        JSONArray jsonObject=new JSONArray();
        Map<String,Object> PARAM_MAP=new HashMap<>();
        for (String s : LOA_LIST) {
           if(params.containsKey(s)) {
               if(s.equalsIgnoreCase("showInIdentity")){
                   PARAM_MAP.put(s,Integer.parseInt(params.get(s)));
               }
               else PARAM_MAP.put(s, params.get(s));
           }
        }

        def cohortList=Cohort.createCriteria().list(){
            PARAM_MAP.each {k,v->
                eq k, v
            }
        }

        for (Cohort cohort : cohortList) {
            Map<String,Object> data= cohort.toMap()
            jsonObject.put(data);
        }

        return jsonObject as JSON
    }


    static getCohortTrack(Map<String,String> params){

        String domainName=params.get("domain")
        DomainOperations domainObject=TABLE_CODE_MAP.get(domainName);

        JSONArray jsonObject=new JSONArray();
        Map<String,Object> PARAM_MAP=new HashMap<>();

        for (String s : LOA_LIST_COHORT_COMPARATOR) {
            if(params.containsKey(s)) {
                PARAM_MAP.put(s, params.get(s));
            }
        }

        def objectList=domainObject.createCriteria().list(){
            PARAM_MAP.each {k,v->
                eq k, v
            }
        }

        for (DomainOperations object : objectList) {
            Map<String, Object> data = object.toMap()
            if (object instanceof CohortsByComparator) {
                def cid = Cohort.findByCohortId(object.cohortId)
            if(cid !=null)data.name = cid.cohortName;
            }
            jsonObject.put(data);
        }

        return jsonObject as JSON
    }



    static getCohortBasedMetrics(Map<String,String> params){


        String metricId=params.get("cohortKey")
        List<CustomMetric> customMetricList=CustomMetric.findAll{metricId==metricId}

        JSONObject jsonObject=new JSONObject();

        for (CustomMetric customMetric : customMetricList) {
            jsonObject.put("formula",customMetric.formula)
            jsonObject.put("metricId",customMetric.metricId)
            jsonObject.put("metric",customMetric.metricName)

            String[] formula=customMetric.formula.split("/")
            String formulaNum= formula[0].toLowerCase()
            String formulaDen=formula[1];

            double memberInCohort=MemberByCustomMetric.executeQuery("select count(*) from MemberByCustomMetric as mcm where mcm.metricId= :e", [e: customMetric.metricId]).get(0)


            for (String year : YEAR_AMP.keySet()) {
                double sum = MemberByCustomMetric.executeQuery("select "+formulaNum +" from MemberByCustomMetric as mcm where mcm.metricId= :e and mcm.period= :p", [e: customMetric.metricId,p:year]).get(0)
                jsonObject.put(YEAR_AMP.get(year)+"YearValue",sum)
                jsonObject.put(YEAR_AMP.get(year)+"metric",CalculatorUtils.divide(sum,memberInCohort))
            }

            jsonObject.put("percentDiffInCurrent",CalculatorUtils.calculatePercentage(jsonObject.get("currentmetric")-jsonObject.get("lastmetric"),jsonObject.get("lastmetric")))
            jsonObject.put("percentDiffInLast",CalculatorUtils.calculatePercentage(jsonObject.get("lastmetric")-jsonObject.get("priormetric"),jsonObject.get("priormetric")))

            jsonObject.put("memberCount",memberInCohort)
        }

        return jsonObject as JSON
    }
}