package com.zakipoint.service

import com.configurations.config.FieldConfig
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.context.request.RequestContextHolder

import javax.servlet.http.HttpSession

class BlindingService {
    public static final String SERVICE_DATE_BETWEEN                = "serviceDate.between"
    public static final String SERVICE_DATE_GREATER_THAN           = "serviceDate.gt"
    public static final String SERVICE_DATE_GREATER_THAN_EQUALS    = "serviceDate.gte"
    public static final String SERVICE_DATE_LESS_THAN              = "serviceDate.lt"
    public static final String SERVICE_DATE_LESS_THAN_EQUALS       = "serviceDate.lte"
    public static final String SERVICE_DATE_EQUALS                 = "serviceDate.eq"
    public static final String GROUP_ID                            = "groupId"
    public static final Long THREE_SIXTY_FOUR_DAYS_IN_MILLISECONDS = 31449600000
    List<String> reportsToBlind = ['memberCost', 'memberCostMonthly','highRiskMembers', 'triggerDiagnosis']

    List <String> phiFieldList = [];
    List <String> fiFieldList = [];

    private static final Map<String, String> PROVIDER_REPLACE_FIELD_MAP = new HashMap<String, String>(){{
        put("prvFullName", "providerId");
    }}

    public Map<String,Map<String,Boolean>> getPhiInfo(Long userId, Long clientId){
        def session = RequestContextHolder.currentRequestAttributes().getSession();
        String phiFieldName = "groupId";
        String fiFieldName = "groupId"
        if(session && session["userAcessLevelsField"]){
            phiFieldName = session["userAcessLevelsField"].get("userAccessLevelsFieldName");
        }

        println "clientId = $clientId"
        println "userID = $userId"
        def dwClient = DwClient.findById(clientId);
        println "dwClient = "+dwClient.id
//        User userObj = User.findById(userId)
//        Service service = Service.findByName('z5dev');
        println "serviceId = "+service.id

        //def userAccessList  = UserAccessLevels.findAllByUserIdAndDwClient(userId,dwClient)
        def userLoaMap = new HashMap<String,Map<String,Boolean>>();
        def exclusionCodes = [];
        def userAccessMap = new HashMap<String,Boolean>()
        def userFiMap = new HashMap<String,Boolean>();
        def userAccessList;
        def exclusionLevels;
        def checkExclusion = UserDwClientGroup.findAll("from UserDwClientGroup as userDwClient where userDwClient.dwClientGroup.client=:client and userDwClient.service=:service and userDwClient.user=:user and userDwClient.exclusion=1",[client:dwClient,service:service,user:userObj]);
        if(checkExclusion){
            exclusionLevels = checkExclusion[0].dwClientGroup.level;
            checkExclusion.each{

                exclusionCodes.add(it.dwClientGroup.code);
            }

            userAccessList  = DwClientGroup.findAll("from DwClientGroup as userDwClient where userDwClient.client=:client and userDwClient.level=:exclusionLevels and userDwClient.code not in :exclusionCodes",[client:dwClient,exclusionLevels:exclusionLevels,exclusionCodes:exclusionCodes])

//            if (!userAccessList.isEmpty() && userAccessList != null){
//                for(DwClientGroup userAccessLevels : userAccessList){
//                    userAccessMap.put(userAccessLevels.code,checkExclusion[0].phi)
//                    userFiMap.put(userAccessLevels.code,checkExclusion[0].fi)
//                }
//            }

        }
        else{
            userAccessList  = UserDwClientGroup.findAll("from UserDwClientGroup as userDwClient where userDwClient.dwClientGroup.client=:client and userDwClient.service=:service and userDwClient.user=:user",[client:dwClient,service:service,user:userObj])
            if (!userAccessList.isEmpty() && userAccessList != null){
//                for(UserDwClientGroup userAccessLevels : userAccessList){
//                    if(phiFieldName.equals(userAccessLevels.dwClientGroup.level)){
//                        userAccessMap.put(userAccessLevels.dwClientGroup.code,userAccessLevels.phi)
//                    }
//                    if(!userAccessLevels.fi){
//                        userFiMap.put(userAccessLevels.dwClientGroup.code,userAccessLevels.fi)
//                    }
//                }
            }
        }
        userLoaMap.put('userAccessPhi',userAccessMap);
        userLoaMap.put('userAccessFi',userFiMap);

        println "userLoaMap = $userLoaMap"
        return userLoaMap;
    }

    public synchronized String getUserAccessLevelFieldNameFromSession(String type)
    {
        String userAccessLevelFieldName;
        def session = RequestContextHolder.currentRequestAttributes().getSession();
        if(session){
            if(type=="phi"){
                userAccessLevelFieldName = session["userAcessLevelsField"]?.get("userAccessLevelsFieldName");
            }else if(type=="fi"){
                userAccessLevelFieldName = session["userAcessLevelsField"]?.get("userFiAccessFieldName");
            }
        }
        userAccessLevelFieldName
    }
    public  def getPhiFieldListFromSession(String type)
    {
        List<String> fieldList = [];
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        if(session && (session["fieldList"] && session["fieldList"].keySet().size()>0))
        {
            if(type=="phi"){
                fieldList = session["fieldList"].get('phiFieldList');
            }else{
                fieldList = session["fieldList"].get('fiFieldList');
            }

        }
        fieldList;
    }

    public def performBlinding(JSONObject inputJson, String tableCode, String type) {

        phiFieldList = getPhiFieldListFromSession(type);
        for (String key : inputJson.keySet()) {

            if (tableCode.equalsIgnoreCase("prs")){
                FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCode).fromDisplayName(key);
                if (phiFieldList.contains(key)) {
                    if(fieldConfig.type.equalsIgnoreCase("Date")){
                        inputJson[key] = inputJson.get(key).toString().split("-")[0]
                    }else{
                        for(Map.Entry entry:PROVIDER_REPLACE_FIELD_MAP){
                            if(entry.key.equals(fieldConfig.field)){
                                inputJson[key] = inputJson[entry.value]
                            }
                        }
                    }
                }
            }
            else if(tableCode!="emp" && tableCode!="mpra"){
                FieldConfig fieldConfig = FieldConfigurations.fromTableCode(tableCode).fromDisplayName(key);
                //println 'key=='+key
                //if the field is a phi field blind it(or don't add it in the final JSON)
                if (phiFieldList.contains(key) && fieldConfig?.displayName?.equals("unblindMemberId")) {
                    inputJson[key] = inputJson.get("memberId")
                } else if (phiFieldList.contains(key)) {
//                println("PHI field is Blinding"+key);


                    if(fieldConfig.type=="Date"){
                        inputJson[key] = inputJson.get(key).toString().split("-")[0]

                    }else
                    {
                        inputJson[key] = ""
                    }


                }
            }else
            {
                if (phiFieldList.contains(key)) {
                    //println"===="+key
                    if(key=="lastClaimDate" || key=="dob" || key=="serviceTo" || key=="serviceFrom" || key=="serviceDate"){
                        def splitby = "-"
                        if(inputJson.get(key).toString().contains("/")){
                            splitby = "/"
                            inputJson[key] =inputJson.get(key).toString().split(splitby)[2]
                        }else{

                            inputJson[key] =inputJson.get(key).toString().split(splitby)[0]
                        }
                    }else{
                        inputJson[key] = ""
                    }
                }
            }

        }
    }

    /**
     * Checks if this user is allowed to view the phi data for the group passed as argument
     * If there is an entry ion PHI FIin phiInfo map return the value (true/false). If there is no entry
     * return true
     */
    public boolean isAllowedToViewPhiData(Map<String, Boolean> phiInfo, String groupCode){
        phiInfo?.containsKey(groupCode) ? phiInfo[groupCode] : true

    }

    /**
     * Checks if this user is allowed to view the FI data for the group passed as argument
     * If there is an entry ion PHI FIin phiInfo map return the value (true/false). If there is no entry
     * return true
     */
    public boolean isAllowedToViewFiData(Map<String, Boolean> phiInfo, String groupCode){
        phiInfo?.containsKey(groupCode) ? phiInfo[groupCode] : true

    }


    public configurePhiInfo(HttpSession session) {
        String user     = session["user"]?.toString()
        Long userId     = User.findByUsername(user)?.id

        String client   = session["clientName"]?.toString()
        Long clientId   = DwClient.findByName(client)?.id

        def phiInfo = getPhiInfo(userId, clientId)
        phiInfo
    }



    public def blindData(JSONObject jsonData, HttpSession session, fieldsToBeBlinded) {
        phiFieldList = getPhiFieldListFromSession('phi');
        fiFieldList = getPhiFieldListFromSession('fi');

        Map<String, Boolean> phiInfo = configurePhiInfo(session)
        //println phiInfo;
        String userAccessLevelField = session["userAcessLevelsField"].get("userAccessLevelsFieldName");
        String userFiAccessLevelField = session["userAcessLevelsField"].get("userFiAccessFieldName");
        jsonData?.each { key,jsonArray ->
            if (jsonArray instanceof JSONArray) {
                jsonArray?.each {jsonInArray->
                    jsonInArray?.each { dataKey, dataValue ->
                        if (dataKey in phiFieldList && jsonInArray[userAccessLevelField] in phiInfo.get('userAccessPhi').keySet() && phiInfo.get('userAccessPhi')[jsonInArray[userAccessLevelField]] == false && dataKey == "unblindMemberId") {
                            jsonInArray[dataKey] = jsonInArray.getAt("memberId")
                        } else {
                            jsonInArray[dataKey] = dataKey in phiFieldList && jsonInArray[userAccessLevelField] in phiInfo.get('userAccessPhi').keySet() && phiInfo.get('userAccessPhi')[jsonInArray[userAccessLevelField]] == false ? "" : dataValue
                        }
                        if (dataKey in fiFieldList && jsonInArray[userFiAccessLevelField] in phiInfo.get('userAccessFi').keySet() && phiInfo.get('userAccessFi')[jsonInArray[userFiAccessLevelField]] == false) {
                            jsonInArray[dataKey] = dataKey in fiFieldList && jsonInArray[userFiAccessLevelField] in phiInfo.get('userAccessFi').keySet() && phiInfo.get('userAccessFi')[jsonInArray[userFiAccessLevelField]] == false ? "" : dataValue
                        }

                    }
                }
            }
        }
    }

    public def blindDrillData(JSONObject jsonData, HttpSession session, fieldsToBeBlinded) {
        phiFieldList = getPhiFieldListFromSession();
        String userAccessLevelField = session.userAcessLevelsField;
        Map<String, Boolean> phiInfo = configurePhiInfo(session)
        jsonData.getAt('result_sets')?.each { key,jsonArray ->
            jsonArray?.each { dataKey, dataValue ->
                if (dataKey in phiFieldList && jsonArray[userAccessLevelField] in phiInfo.keySet() && phiInfo[jsonArray[userAccessLevelField]] == false && dataKey == "unblindMemberId") {
                    jsonArray[dataKey] = jsonArray.getAt("memberId")
                } else {
                    //  println("PHI field is blindDrillData Blinding"+phiFieldList);
                    jsonArray[dataKey] = dataKey in phiFieldList && jsonArray[userAccessLevelField] in phiInfo.keySet() && phiInfo[jsonArray[userAccessLevelField]] == false ? "" : dataValue
                }
            }
        }
        jsonData
    }

    /**
     * A method to blind sum it feature
     * If there are 6 items in the json, blind the phi data and put memberId in term's value
     * If there are 3 items in the json, add a new item called displayTerm and make it empty so that
     * the unBlindMemberId (that the term holds) is not displayed in the front. However, they can still
     * perform drill down using the unBlindedMemberId
     */
    public def blindSumItData(JSONObject jsonData, HttpSession session, fieldsToBeBlinded) {
        String userAccessLevelField = session["userAcessLevelsField"].get("userAccessLevelsFieldName");
        phiFieldList = getPhiFieldListFromSession();
        phiFieldList.add("displayTerm");
        Map<String, Boolean> phiInfo = configurePhiInfo(session).get("userAccessPhi");
        jsonData.getAt('result_sets')?.each { key, jsonObject ->
            if (jsonObject?.size() == 6) {
                jsonObject?.each { dataKey, dataValue ->
                    if (dataKey in phiFieldList && jsonObject[userAccessLevelField] in phiInfo.keySet() && phiInfo[jsonObject[userAccessLevelField]] == false) {
                        jsonObject[dataKey] = ""
                        jsonObject["term"]  = jsonObject.getAt("memberId")
                    }
                    jsonObject["memberId"] = jsonObject.getAt("memberId");
                }
            } else if (jsonObject?.size() == 3){
                jsonObject.put("displayTerm", "")
            }
        }
        jsonData
    }

    public def getBlindingInfo(){
        def session = RequestContextHolder.currentRequestAttributes().getSession();
        session
    }

    /**
     *  If query param from front contains phi fields OR difference in service date is less 12 months OR return true.
     */
    public boolean isInvalidUserForRequestedGroups(def params, HttpSession session) {
        Map<String, Boolean> phiInfo  = configurePhiInfo(session)
        boolean isInvalidRequest      = false

        String query          = params.query          ?: null
        String table          = params.table          ?: ""
        String cycleEndDate   = params.phiCEDate      ?: ""
        String cycleStartDate = params.phiCSDate      ?: ""
        String reportingFrom  = params.reportingFrom  ?: ""
        String reportingTo    = params.reportingTo    ?: ""
        String report         = params.report         ?: ""

        if (query && phiInfo?.containsValue(false)) {
            JSONObject queryJson = (JSONObject) JSON.parse(query)

            queryJson?.each { field, value->
//                println "field = $field value = $value"

                Iterator itr = value?.iterator()
                while (itr.hasNext()) {
                    String tempValue = itr.next()?.toString()
//                    println "tempValue = $tempValue"
                    String fieldName = tempValue?.split("\\.")[0]

                    FieldConfig fieldConfig = FieldConfigurations.fromTableCode(table).fromFieldName(fieldName)
//                    println "FieldName: ${fieldName} Is phi field: " + fieldConfig?.phi

                    if (fieldConfig?.phi || isInvalidServiceDate(tempValue, cycleEndDate, cycleStartDate)) {
                        isInvalidRequest = true
                        break
                    }
                }
            }
        } else if (report in reportsToBlind && phiInfo?.containsValue(false)) {
            isInvalidRequest = isReportValidForUser(reportingTo, reportingFrom)
        }
        println "----------------------------------Is invalid request: ${isInvalidRequest}----------------------------------"
        isInvalidRequest
    }

    public Boolean isReportValidForUser(String reportingTo, String reportingFrom) {
        if ("".equals(reportingTo) || "".equals(reportingFrom)) {
            println "Reporting to and reporting from date cannot be empty"
            return false
        }
        long reportingFromDate = DateUtils.getTimeFromDateWrtTimeZone(reportingFrom?.trim())
        long reportingToDate   = DateUtils.getTimeFromDateWrtTimeZone(reportingTo?.trim())
        long dateDifference    = getDateDifference(reportingToDate, reportingFromDate)
        println "Date difference in isReportValidForUser: ${dateDifference}"
        dateDifference < THREE_SIXTY_FOUR_DAYS_IN_MILLISECONDS
    }

    public boolean isInvalidServiceDate(String value, String cycleEndDate, String cycleStartDate) {
        if ("".equals(cycleEndDate) || "".equals(cycleStartDate)) {
            println "Cycle end date and cycle start date cannot be empty"
            return true
        }
        String fieldName    = cleanField(value?.split(":")[0])
        String fieldValue   = cleanField(value?.split(":")[1])
        long dateDifference = 0
        boolean isServiceDate = true
        println "FieldName = $fieldName   FieldValue = $fieldValue"
        switch (fieldName) {
            case SERVICE_DATE_BETWEEN:
                println SERVICE_DATE_BETWEEN
                dateDifference = getServiceDatesDifference(fieldValue)
                break

            case SERVICE_DATE_GREATER_THAN: case SERVICE_DATE_GREATER_THAN_EQUALS:
                println SERVICE_DATE_GREATER_THAN + " OR " + SERVICE_DATE_GREATER_THAN_EQUALS
                dateDifference = getServiceDateAndCycleDateDifference(fieldValue, cycleEndDate)
                break

            case SERVICE_DATE_LESS_THAN: case SERVICE_DATE_LESS_THAN_EQUALS:
                println SERVICE_DATE_LESS_THAN + " OR " + SERVICE_DATE_LESS_THAN_EQUALS
                dateDifference = getServiceDateAndCycleDateDifference(cycleStartDate, fieldValue)
                break

            case SERVICE_DATE_EQUALS:
                println SERVICE_DATE_EQUALS
                return true
            default:
                isServiceDate = false
                break
        }
        println "Date difference in isInvalidServiceDate: ${dateDifference}"
        dateDifference < THREE_SIXTY_FOUR_DAYS_IN_MILLISECONDS && isServiceDate
    }

    public long getServiceDatesDifference(String fieldValue) {
        long serviceFromDate  = DateUtils.getTimeFromDateWrtTimeZone(getItemFromArray(fieldValue,0))
        long serviceToDate    = DateUtils.getTimeFromDateWrtTimeZone(getItemFromArray(fieldValue,1))
        getDateDifference(serviceToDate, serviceFromDate)
    }

    /**
     * @param toDate       Should always be the bigger of the two dates
     * @param fromDate     Should always be the smaller of the two dates
     * @return             Difference between the two dates
     */
    public long getServiceDateAndCycleDateDifference(String fromDate, String toDate) {
        long serviceFromDate  = DateUtils.getTimeFromDateWrtTimeZone(fromDate?.trim())
        long serviceToDate    = DateUtils.getTimeFromDateWrtTimeZone(toDate?.trim())
        getDateDifference(serviceToDate, serviceFromDate)
    }

    public String getItemFromArray(String field, int itemNumber) {
        field?.substring(1, field?.length() -1)?.split(",")[itemNumber]?.trim()
    }

    public long getDateDifference(long greaterValue, long smallerValue) {
        greaterValue - smallerValue
    }

    public String cleanField(String field) {
        field?.replace("{","")?.replace("}","")?.replace("\"","")?.trim()
    }

    public static List<String> getGroupsAsList(String query) {
        JSONObject queryJson = (JSONObject) JSON.parse(query)
        String groups = ""
        List<String> groupList = null

        queryJson?.each { field, value->
            Iterator itr = value.iterator()
            while (itr.hasNext()) {
                String tempValue = itr.next()?.toString()
                groups = tempValue?.indexOf(GROUP_ID + ".eq") > 0 ? new JSONObject(tempValue).get(GROUP_ID + ".eq") : ""
            }
            groupList = !groups.empty ? SearchUtils.getListFromArray(groups) : null
        }
        groupList
    }

    public  boolean checkValue(value, String comparator, checkValue,FieldConfig config)
    {
        def checkValue1=checkValue;
        def checkValue2=0;
        if(comparator.equalsIgnoreCase("between"))
        {
            checkValue = checkValue.toString().replace("[","").replace("]","").split(",");
            checkValue1 = checkValue[0].replace('"',"");
            checkValue2 = checkValue[1].replace('"',"");
        }
        if(config.type.equalsIgnoreCase("Date"))
        {
            value = DateConversionUtility.getTimeFromDateWrtTimeZone(value);
            checkValue1 = DateConversionUtility.getTimeFromDateWrtTimeZone(checkValue[0]);
            checkValue2 = DateConversionUtility.getTimeFromDateWrtTimeZone(checkValue[1]);

        }

        if(value instanceof ArrayList){

            Set<String> valueSet = new HashSet<String>(value);
            Set<String> checkValueSet = new HashSet<String>();
            checkValueSet.addAll(checkValue.toString().replace("[","").replace("]","").replace('"',"").split(","))
            Set<String> intersectedValues = valueSet.intersect(checkValueSet);


            if(!intersectedValues.isEmpty())
            {
                return true;
            }
        }

        if(checkValue instanceof JSONArray){
            if(checkValue.toString().replace("[","").replace("]","").contains(value))
            {
                return true;
            }
        }

        if (value instanceof String) {
            return check(comparator, value, checkValue1,checkValue2)
        }
        if (value instanceof Integer) {
            return check(comparator, Integer.valueOf(value), Integer.valueOf(checkValue1),Integer.valueOf(checkValue2))
        }
        if (value instanceof Long) {
            return check(comparator, Long.valueOf(value), Long.valueOf(checkValue1),Long.valueOf(checkValue1))
        }
        if (value instanceof Float) {
            return check(comparator, Float.valueOf(value), Float.valueOf(checkValue1), Float.valueOf(checkValue2))
        }
        if (value instanceof Double) {
            return check(comparator, Double.valueOf(value), Double.valueOf(checkValue1),Double.valueOf(checkValue2))
        }

        return false
    }

    private boolean check(String comparator,value, checkValue1,checkValue2) {
        try {
            switch (comparator) {
                case 'eq':
                    return value.toString().equalsIgnoreCase(checkValue1.toString());
                    break;
                case 'neq':
                    return !value.toString().equalsIgnoreCase(checkValue1.toString());
                    break;
                case 'contains':
                    return value.toString().contains(checkValue1.toString());
                    break;
                case 'gte':
                    return value >= checkValue1;
                    break;
                case 'gt':
                    return value > checkValue1;
                    break;
                case 'lt':
                    return value < checkValue1;
                    break;
                case 'lte':
                    return value <= checkValue1;
                    break;
                case 'between':
                    println("Inside Between"+checkValue1+"=="+checkValue2)
                    return value >= checkValue1 && value <= checkValue2;
                    break;
                default:
                    return false;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
