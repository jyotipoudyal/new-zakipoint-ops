package com.zakipoint.service

import grails.converters.JSON
import grails.transaction.Transactional
import grails.util.Environment
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import zakipointCore.commonUtils.RequestUtils

import javax.servlet.http.HttpSession

@Transactional
class ParameterService {
    boolean isFieldexist = false;
    BlindingService blindingService
    Map<String, String> reportWiseQuery = ["expenseDistributionDrillDown":"reportingEligible"];

    def configureCohortParam(params, session) {

        if (params && params.containsKey("query") && params.get("table").equals("cohort") && params.get("clientId").equals("cohorts")) {
            Map<String, Map<String, Boolean>> phiInfo = new BlindingService().configurePhiInfo(session)

            int phiSize = phiInfo.get("userAccessPhi").keySet().size()//phiInfo.keySet().size()

            def userId = session.getAttribute("userId")
            def JSONObject jsonQuery = JSON.parse(params.query)
            if (!Environment.getCurrent().name.equals("development")) {
                JSONArray extraParams = new JSONArray()
                extraParams.add(["userId.eq":userId])
                if(phiSize<=0)extraParams.add(["readAccess.eq":"true"]) // for groupwise user

                params.query =  String.valueOf(jsonQuery.accumulate("and",["or":extraParams]) as JSON)
            }

        }
    }

    def checkPhiFilter(Map<String, String> requestParams, HttpSession session) {

        BlindingService blindingService = new BlindingService();
        String userPhiFieldName = session["userAcessLevelsField"].getAt("userAccessLevelsFieldName");
        String userPhiFieldValue = blindingService.configurePhiInfo(session).get("userAccessPhi").keySet().toString();
        if (!requestParams.containsKey("query")) {
            if (requestParams.table) {
                JSONObject jsonObject = new JSONObject(buildLOAPhiQuery(requestParams, userPhiFieldName, userPhiFieldValue));
                requestParams.query = String.valueOf(jsonObject as JSON)
            } else {
                requestParams[userPhiFieldName] = getCommonLoaValues(requestParams.getAt(userPhiFieldName), session);
            }
        } else {
            if(reportWiseQuery.containsKey(requestParams.get("report"))){
                if (requestParams.containsKey("comparisonEligible") && requestParams.containsKey("comparisonQuery"))
                {
                    JSONObject comparisonEligible = getFilter((JSONObject) JSON.parse(requestParams.comparisonEligible), session)
                    accumulatereportinQuery(comparisonEligible, requestParams, userPhiFieldName, userPhiFieldValue)
                    requestParams.comparisonEligible = (comparisonEligible as JSON).toString();
                    JSONObject comparisonQuery = getFilter((JSONObject) JSON.parse(requestParams.comparisonQuery), session)
                    accumulatereportinQuery(comparisonQuery, requestParams, userPhiFieldName, userPhiFieldValue)
                    requestParams.comparisonQuery = (comparisonQuery as JSON).toString();
                }
                JSONObject reportingEligible = getFilter((JSONObject) JSON.parse(requestParams.reportingEligible), session)
                accumulatereportinQuery(reportingEligible, requestParams, userPhiFieldName, userPhiFieldValue)
                requestParams.reportingEligible = (reportingEligible as JSON).toString();
                JSONObject reportingQuery = getFilter((JSONObject) JSON.parse(requestParams.reportingQuery), session)
                accumulatereportinQuery(reportingQuery, requestParams, userPhiFieldName, userPhiFieldValue)
                requestParams.reportingQuery = (reportingQuery as JSON).toString();

            } else {
                JSONObject query = getFilter((JSONObject) JSON.parse(requestParams.query), session)
                accumulatereportinQuery(query, requestParams, userPhiFieldName, userPhiFieldValue)
                requestParams.query = (query as JSON).toString();
            }
        }
        return requestParams;
    }

    private void accumulatereportinQuery(JSONObject reportingquery, Map<String, String> requestParams, String userPhiFieldName, String userPhiFieldValue) {
        if (!isFieldexist)
            reportingquery.accumulate("and", new JSONObject(buildLOAPhiQuery(requestParams, userPhiFieldName, userPhiFieldValue)))
    }

    private def buildLOAPhiQuery(Map<String, String> requestParams, String userPhiFieldName, String userPhiFieldValue) {
        if (requestParams.containsKey("drillReportingTo") && requestParams.containsKey("drillEligibilityType"))
            return buildphiLOAFilter(userPhiFieldName, userPhiFieldValue, requestParams)
        else
            return buildPhiTermFilter(userPhiFieldName, userPhiFieldValue)
    }

    def oldcheckPhiFilter(Map<String,String> requestParams, HttpSession session) {
        if (!requestParams.containsKey("query")) {
            println "=====================";
            return buildQuery(requestParams,session)
        } else{
            JSONObject query =  getFilter((JSONObject)JSON.parse(requestParams.query),session)
            println 'query====='+query;
            if(ElasticSearchConnectionManager.isForTest){
                if (!isFieldexist) {
                    query.accumulate("and", new JSONObject().put("groupId.eq","01"));
                }
            }else{
                if (!isFieldexist) {
                    BlindingService blindingService = new BlindingService();
                    String userPhiFieldName = session["userAcessLevelsField"].getAt("userAccessLevelsFieldName");
                    String userPhiFieldValue = blindingService.configurePhiInfo(session).get("userAccessPhi").keySet().toString();
                    query.accumulate("and", new JSONObject().put("${userPhiFieldName+".eq"}",userPhiFieldValue));
                }
            }
            requestParams.query = (query as JSON).toString();
            return requestParams;
        }
    }

//    def buildPhiTermFilter(String field, String value ){
//        DasAndQuery dasAndQuery = new DasAndQuery()
//                .add(new DasEqQuery(field, value))
//        DasRequestBuilder dasRequestBuilder = new DasRequestBuilder(dasAndQuery)
//        return dasRequestBuilder.toString()
//    }

//    def buildphiLOAFilter(String userPhiFieldName, String userPhiFieldValue, Map<String, String> requestParams){
//        String fromToDate = requestParams.containsKey("drillReportingFrom")? requestParams.get("drillReportingFrom") : requestParams.get("drillReportingTo");
//        DasAndQuery dasAndQuery = new DasAndQuery()
//                .add(new DasEqQuery(userPhiFieldName, userPhiFieldValue))
//                .add(new DasLteQuery(userPhiFieldName+"FromDate", requestParams.get("drillReportingTo")))
//                .add(new DasGteQuery(userPhiFieldName+"ToDate", fromToDate))
//                .add(new DasEqQuery(userPhiFieldName+"EligibilityType", requestParams.get("drillEligibilityType")))
//        DasRequestBuilder dasRequestBuilder = new DasRequestBuilder(dasAndQuery)
//        return dasRequestBuilder.toString()
//    }

    private String buildQuery(Map<String, String> requestParams,HttpSession session) {

        BlindingService blindingService = new BlindingService();
        String userPhiFieldName
        String userPhiFieldValue
        if(ElasticSearchConnectionManager.isForTest){
            userPhiFieldName = "groupId";
            userPhiFieldValue = '01';
        }else{

            userPhiFieldName = session["userAcessLevelsField"].get("userAccessLevelsFieldName");
            userPhiFieldValue = blindingService.configurePhiInfo(session).get("userAccessPhi").keySet().toString();;
        }

        def newQuery;
        if(requestParams.table){ // used for data search
            def JSONObject jsonQuery = new JSONObject()
            JSONArray extraParams = new JSONArray()
            println 'userPhiFieldValue=='+userPhiFieldValue;
            extraParams.add(["${userPhiFieldName+".eq"}": userPhiFieldValue]) // override here
            newQuery = String.valueOf(jsonQuery.accumulate("and", extraParams) as JSON)
            requestParams.query = newQuery
        }else{ // used for report params
            //println "BuildQuery==="+userPhiFieldValue+"==="+requestParams[userPhiFieldName]+"==="+userPhiFieldValue.contains(requestParams[userPhiFieldName]);

            requestParams[userPhiFieldName] = getCommonLoaValues(requestParams.getAt(userPhiFieldName), session);


        }
        println "requestParams = $requestParams"
        return requestParams;
    }

    public JSONObject getFilter(JSONObject jsonObject,HttpSession session) {
        if (isSimpleExpression(jsonObject)) {
            fieldInfo(jsonObject,session);
        }
        else {
            String filterType = getKey(jsonObject);
            JSONArray innerJson = (JSONArray) jsonObject.get(filterType);

            for (Object object : innerJson) {
                getFilter((JSONObject) object,session);
            }
            return jsonObject
        }

    }

    private static boolean isSimpleExpression(JSONObject jsonObject) {
        return !isBooleanExpression(jsonObject);
    }

    public static boolean isBooleanExpression(JSONObject jsonObject) {

        String key = getKey(jsonObject);
        if (key.equals("or") || key.equals("and") || key.equals("nested") || key.equals("not")) {
            return true;
        }
        return false;
    }
    public static String getKey(JSONObject jsonObject) {
        if (jsonObject.keySet().size() > 1) {
            throw new RuntimeException("Unexpected size of jsonObject in ctor of BooleanFilter . JsonObject" + jsonObject);
        }
        return jsonObject.keys().next().toString();
    }

    public def fieldInfo(JSONObject jsonObject, HttpSession session) {
        blindingService = new BlindingService();

        if (jsonObject.keySet().size() > 1) {
            throw new IllegalArgumentException("Unexpected key size for jsonObject " + jsonObject);
        }
        String jsonKey = (String) jsonObject.keys().next();
        Object value = jsonObject.get(jsonKey);

        int indexOfDot = jsonKey.lastIndexOf(".");
        String operand = jsonKey.substring(0, indexOfDot);
        String operator = jsonKey.substring(indexOfDot + 1);
        String fieldName = operand;
        String userPhiFieldName
        Set<String> userPhiFieldValue = new HashSet<>();

        userPhiFieldName = session["userAcessLevelsField"].get("userAccessLevelsFieldName");
        if(ElasticSearchConnectionManager.isForTest){
            userPhiFieldName = "groupId";
            userPhiFieldValue.add("01");

        }
        else if (fieldName.equals(userPhiFieldName)){

            if (operator.equals("neq")) {
                userPhiFieldValue = blindingService.configurePhiInfo(session).get("userAccessPhi").keySet();
                getMatchedValue(value, userPhiFieldValue);
                jsonObject.remove(jsonKey)
                jsonKey = jsonKey.replace("neq","eq")

            }else{
                userPhiFieldValue.addAll(getCommonLoaValues(value,session));
            }
            isFieldexist=true;
            jsonObject.put(jsonKey,userPhiFieldValue.toString())  // override here

        }



    }

    private getCommonLoaValues(values, HttpSession session) {
        blindingService == null ? blindingService = new BlindingService() : blindingService;
        Set<String> userPhiFieldValue = new HashSet<>();
        def temp = blindingService.configurePhiInfo(session).get("userAccessPhi").keySet();
        Set<String> tempValue = new HashSet<String>()
        if (values) {
            String[] paramValue = RequestUtils.getArrayRequest(values)
            for (String val : paramValue) {
                if (temp.contains(val)) {
                    tempValue.add(val);
                }
            }
        }

        tempValue.size() == 0 ? userPhiFieldValue.addAll(temp) : userPhiFieldValue.addAll(tempValue);
        userPhiFieldValue.toString()
    }

    private void getMatchedValue(value, Set<String> userPhiFieldValue) {
        String[] paramValue = RequestUtils.getArrayRequest(value.toString())
        Set<String> tempValue = new HashSet<String>()
        for (String val : paramValue) {
            if (userPhiFieldValue.contains(val)) {
                tempValue.add(val);
            }
        }
        userPhiFieldValue.removeAll(tempValue)
    }

}
