package com.zakipoint.service

import zakipointCore.builder.ReportFromDomainBuilders
import zakipointCore.commonUtils.RequestUtils
import zakipointCore.commonUtils.SpringBeanUtils

/**
 * Created by sktamang on 3/3/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class ReportFromDomainService {

    static List<String> LOA_LIST_COHORT_COMPARATOR=Arrays.asList("group","division");


    static hanldeRequest(Map<String,String> requestParams){
        loaParamsManagement(requestParams)
        String report=requestParams.get("report")
        ReportFromDomainBuilders builder=SpringBeanUtils.getBean(report);
        builder.getResults(requestParams)
    }

    public static void loaParamsManagement(Map<String, String> requestParams) {
        for (String key : LOA_LIST_COHORT_COMPARATOR) {
            if (requestParams.containsKey(key)) {
                requestParams.put(key + "Id", RequestUtils.getArrayRequestValue(requestParams.get(key)))
                requestParams.remove(key)
            }
        }
    }
}
