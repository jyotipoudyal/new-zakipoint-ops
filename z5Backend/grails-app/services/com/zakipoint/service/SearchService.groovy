package com.zakipoint.service

import com.configurations.config.AbstractConfigurationManager
import com.configurations.search.MemberSearchRequest
import com.configurations.utils.MemberSearchQueryBuilder
import com.configurations.utils.MemberSearchQueryExecutor
import org.codehaus.groovy.grails.web.json.JSONObject
import org.elasticsearch.action.search.SearchRequestBuilder
import org.springframework.beans.factory.annotation.Autowired
import zakipointCore.drill.ZphMemberDrillGenerator

class SearchService {
    public static final List<String> hideFields =  new ArrayList<String>(Arrays.asList("serviceDate","paidDate"));
    public static final String RESULTSET        = "result_sets"
    public static final String SUMMARY          = "summary"
    private final int defaultPageNumber         = 1
    private final int defaultPageSize           = 20

    @Autowired AbstractConfigurationManager configurationManager

    def search(MemberSearchRequest memberSearchRequest) {
        SearchRequestBuilder builder = buildRequest(memberSearchRequest)
        MemberSearchQueryExecutor queryExecutor = new MemberSearchQueryExecutor(builder, memberSearchRequest)

        JSONObject finalJSON = new JSONObject();
        JSONObject resultJson = queryExecutor.executeSearch();
        JSONObject summaryJson = queryExecutor.getSummaryJson();

        finalJSON.put(RESULTSET, resultJson);
        finalJSON.put(SUMMARY, summaryJson);
        ZphMemberDrillGenerator.checkIsZphClient(memberSearchRequest.requestParameters,builder,finalJSON);
        finalJSON
    }

    def buildRequest(MemberSearchRequest memberSearchRequest){
        memberSearchRequest.initialize()
        SearchRequestBuilder builder = new MemberSearchQueryBuilder(memberSearchRequest)
                .setTypes()
                .setPagination()
                .addFields()
                .addFilter()
                .setSortOrder()
                .build();
        builder
    }

    private def configureParams(Map<String,String> params) {
        params.remove("controller")
        params.remove("action")

        if (!params.containsKey("page")) {
            params.put("page", String.valueOf(defaultPageNumber));
        }
        if (!params.containsKey("pageSize")) {
            params.put("pageSize", String.valueOf(defaultPageSize));
        }
        if (!params.containsKey("table")) {
            throw new RuntimeException("Table not specified")
        }

        }
    }