package com.qm

import com.configurations.utils.DomainOperations


/**
 * Created by sktamang on 3/1/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class QualityMetrics implements DomainOperations,Serializable{

    String groupId;
    String divisionId;
    String qmId;
    String qmName;
    String category;
    Integer memberCount;
    Integer criteriaMetCount;
    Integer criteriaNotMetCount;
    Date processingMonth;
    String paQmId;

    static mapping = {
        datasource 'z5cohort'
        id composite:['groupId', 'divisionId','qmId']
    }


    public Map toMap() {
        def result = [:];
        result.groupId = this.groupId
        result.divisionId = this.divisionId;
        result.qmId= this.qmId
        result.qmName = this.qmName
        result.category = this.category
        result.memberCount = this.memberCount
        result.criteriaMetCount = this.criteriaMetCount
        result.criteriaNotMetCount = this.criteriaNotMetCount
        result.processingMonth = this.processingMonth
        result.paQmId = this.paQmId
        return result;
    }

}
