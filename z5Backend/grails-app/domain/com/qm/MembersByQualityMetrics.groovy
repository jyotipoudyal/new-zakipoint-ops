package com.qm

import com.configurations.utils.DomainOperations


/**
 * Created by sktamang on 3/1/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class MembersByQualityMetrics implements DomainOperations,Serializable{

    String groupId;
    String divisionId;
    String qmId;
    String qmName;
    String memberId;
    Integer criteriaMetFlag;

    static mapping = {
        datasource 'z5cohort'
        id composite:['groupId', 'divisionId','qmId','memberId']
    }

    @Override
    Object toMap() {
        def result = [:];
        result.groupId = this.groupId
        result.divisionId = this.divisionId;
        result.qmId= this.qmId
        result.qmName = this.qmName
        result.memberId = this.memberId
        result.criteriaMetFlag = this.criteriaMetFlag
        return result;
    }
}
