package com.cohort

import com.configurations.utils.DomainOperations

import java.sql.Date

class Cohort implements DomainOperations{

    String cohortId;
    String cohortName;
    String cohortDesc;
    String logoId;
    String problemSizeFormula;
    String saving_min;
    String saving_max;
    Date createdDate;

    String groupId;
    String divisionId;
    Date updatedDate;
    Integer numberOfMembers;

    Integer showInIdentity;
    Integer activeFlag;
    String parentCohortId;
    String clientId;
    String createdBy;
    String actions;
    String cohortType;


    String udf1;
    String udf2;
    String udf3;

    Date udf4;
    Date udf5;
    Date udf6;

    Double udf7;
    Double udf8;
    Double udf9;

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        cohortId(blank: true, nullable: true)
        cohortName( blank: true, nullable: true)
        cohortDesc(blank: true, nullable: true)
        logoId(blank: true, nullable: true)
        problemSizeFormula(blank: true, nullable: true)
        saving_min(blank: true, nullable: true)
        saving_max(blank: true, nullable: true)
        createdDate(blank: true, nullable: true)

        groupId(blank: true, nullable: true)
        divisionId(blank: true, nullable: true)
        updatedDate(blank: true, nullable: true)
        numberOfMembers(blank: true, nullable: true)

        showInIdentity(blank: true, nullable: true)
        activeFlag(blank: true, nullable: true)
        parentCohortId(blank: true, nullable: true)
        clientId(blank: true, nullable: true)
        createdBy(blank: true, nullable: true)
        actions(blank: true, nullable: true)
        cohortType(blank: true, nullable: true)
        udf1(blank: true, nullable: true)
        udf2(blank: true, nullable: true)
        udf3(blank: true, nullable: true)
        udf4(blank: true, nullable: true)
        udf5(blank: true, nullable: true)
        udf6(blank: true, nullable: true)
        udf7(blank: true, nullable: true)
        udf8(blank: true, nullable: true)
        udf9(blank: true, nullable: true)

    }

    public Map toMap() {
        def result = [:];
        result.cohortId = this.cohortId
        result.cohortName = this.cohortName;
        result.cohortDesc= this.cohortDesc
        result.logoId = this.logoId
        result.problemSizeFormula = this.problemSizeFormula
        result.saving_min = this.saving_min
        result.saving_max = this.saving_max
        result.createdDate = this.createdDate

        result.groupId = this.groupId
        result.divisionId = this.divisionId
        result.updatedDate = this.updatedDate
        result.numberOfMembers = this.numberOfMembers

        result.showInIdentity = this.showInIdentity
        result.activeFlag = this.activeFlag
        result.parentCohortId = this.parentCohortId
        result.clientId = this.clientId
        result.createdBy = this.createdBy
        result.actions = this.actions
        result.cohortType = this.cohortType
        result.udf1 = this.udf1
        result.udf2 = this.udf2
        result.udf3 = this.udf3
        result.udf4 = this.udf4
        result.udf5 = this.udf5
        result.udf6 = this.udf6
        result.udf7 = this.udf7
        result.udf8 = this.udf8
        result.udf9 = this.udf9

        result.id = this.id;
        return result;
    }
}
