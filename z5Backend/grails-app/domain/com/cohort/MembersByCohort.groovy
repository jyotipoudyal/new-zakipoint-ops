package com.cohort

import com.configurations.utils.DomainOperations


class MembersByCohort implements DomainOperations{

    String cohortId;
    String mbrId;
    Boolean action;
    Date createdDate;
    Date removedDate;

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        cohortId( blank: true, nullable: true)
        mbrId(blank: true, nullable: true)
        action(blank: true, nullable: true)
        createdDate(blank: true, nullable: true)
        removedDate(blank: true, nullable: true)
    }

    public Map toMap() {
        def result = [:];
        result.cohortId = this.cohortId;
        result.mbrId= this.mbrId
        result.action = this.action
        result.createdDate = this.createdDate
        result.removedDate = this.removedDate
        result.id = this.id;
        return result;
    }

    public  Object getById(String value, Object key) {
        return this.findAllWhere(cohortId:value)
    }
}
