package com.cohort

import com.configurations.utils.DomainOperations


/**
 * Created by sktamang on 2/22/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class CohortsByComparator implements DomainOperations{

    String comparatorId;
    String cohortId;
    Integer order;

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        comparatorId( blank: true, nullable: true)
        cohortId(blank: true, nullable: true)
        order(blank: true, nullable: true)
    }

    public Map toMap() {
        def result = [:];
        result.comparatorId = this.comparatorId;
        result.cohortId= this.cohortId
        result.order = this.order
        return result;
    }

}
