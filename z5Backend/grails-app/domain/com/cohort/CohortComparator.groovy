package com.cohort

import com.configurations.utils.DomainOperations


/**
 * Created by sktamang on 2/22/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class   CohortComparator implements DomainOperations {

    String groupId;
    String comparatorId;
    String comparatorName;
    Integer displayOrder;



    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        groupId(blank: true, nullable: true)
        comparatorId( blank: true, nullable: true)
        comparatorName(blank: true, nullable: true)
        displayOrder(blank: true, nullable: true)

    }

    public Map toMap() {
        def result = [:];
        result.groupId = this.groupId
        result.comparatorId = this.comparatorId;
        result.comparatorName= this.comparatorName
        result.displayOrder = this.displayOrder
        return result;
    }

}
