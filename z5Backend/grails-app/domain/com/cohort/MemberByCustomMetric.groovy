package com.cohort

import com.configurations.utils.DomainOperations


class MemberByCustomMetric implements DomainOperations{

    String metricId;
    String mbrId;
    String period;
    Double  A;
    Double  B;
    Double  C;
    Double  D;
    String groupId;

    static constraints = {
    }

    static mapping = {
        version false
        datasource 'z5cohort'
        a column : 'A'
        b column : 'B'
        c column : 'C'
        d column : 'D'
    }

    @Override
    Object toMap() {
        return null
    }
}
