package com.cohort

import com.configurations.utils.DomainOperations


class CustomMetric implements DomainOperations{

    String metricId;
    String metricName;
    String clientId;
    String groupId;
    String metricUnit;
    String formula;
    String expectedDirection;

    static constraints = {
    }

    static mapping = {
        version false
        datasource 'z5cohort'
    }

    @Override
    Object toMap() {
        return null
    }
}
