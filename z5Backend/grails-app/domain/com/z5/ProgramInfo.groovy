package com.z5

import com.configurations.utils.DomainOperations

import java.sql.Date

/**
 * Created by sktamang on 9/5/16.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class ProgramInfo implements DomainOperations{

    String programId
    String programName
    String programType
    Date programBeginDate
    Date programEndDate
    String programManager
    String groupId
    String clientId
    Date updatedDate
    String divisionId

    String udf1_program
    String udf2_program
    String udf3_program
    String udf4_program
    String udf5_program

    Double udf6_program
    Double udf7_program
    Double udf8_program
    Double udf9_program
    Double udf10_program

    Date udf11_program
    Date udf12_program
    Date udf13_program
    Date udf14_program
    Date udf15_program

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        programId( blank: true, nullable: true)
        programName(blank: true, nullable: true)
        programType(blank: true, nullable: true)
        programBeginDate(blank: true, nullable: true)
        programEndDate(blank: true, nullable: true)
        programManager(blank: true, nullable: true)
        groupId(blank: true, nullable: true)
        clientId(blank: true, nullable: true)
        updatedDate(blank: true, nullable: true)
        divisionId(blank: true, nullable: true)

        udf1_program(blank: true, nullable: true)
        udf2_program(blank: true, nullable: true)
        udf3_program(blank: true, nullable: true)
        udf4_program(blank: true, nullable: true)
        udf5_program(blank: true, nullable: true)
        udf6_program(blank: true, nullable: true)
        udf7_program(blank: true, nullable: true)
        udf8_program(blank: true, nullable: true)
        udf9_program(blank: true, nullable: true)
        udf10_program(blank: true, nullable: true)
        udf11_program(blank: true, nullable: true)
        udf12_program(blank: true, nullable: true)
        udf13_program(blank: true, nullable: true)
        udf14_program(blank: true, nullable: true)
        udf15_program(blank: true, nullable: true)

    }

    @Override
    public Map toMap() {
        def result = [:];
        result.programId = this.programId;
        result.programName = this.programName
        result.programType= this.programType
        result.programBeginDate = this.programBeginDate
        result.programEndDate = this.programEndDate
        result.programManager = this.programManager
        result.groupId = this.groupId
        result.clientId = this.clientId
        result.updatedDate = this.updatedDate
        result.divisionId= this.divisionId

        result.udf1_program = this.udf1_program;
        result.udf2_program = this.udf2_program;
        result.udf3_program = this.udf3_program;
        result.udf4_program = this.udf4_program;
        result.udf5_program = this.udf5_program;
        result.udf6_program = this.udf6_program;
        result.udf7_program = this.udf7_program;
        result.udf8_program = this.udf8_program;
        result.udf9_program = this.udf9_program;
        result.udf10_program = this.udf10_program;
        result.udf11_program = this.udf11_program;
        result.udf12_program = this.udf12_program;
        result.udf13_program = this.udf13_program;
        result.udf14_program = this.udf14_program;
        result.udf15_program = this.udf15_program;

        result.id = this.id;
        return result;
    }
}
