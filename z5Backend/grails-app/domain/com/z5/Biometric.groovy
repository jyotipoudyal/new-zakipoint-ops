package com.z5

import com.configurations.utils.DomainOperations

import java.sql.Date

/**
 * Created by sktamang on 9/5/16.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class Biometric implements DomainOperations {

    String unblindMemberId
    String groupId
    String biometricId
    String biometricName
    Date measuredDate
    Date systemUpdateDate
    Double biometricValue
    Date updatedDate
    String period
    String divisionId
    String biometricLevel

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        unblindMemberId( blank: true, nullable: true)
        groupId(blank: true, nullable: true)
        biometricId(blank: true, nullable: true)
        biometricName(blank: true, nullable: true)
        measuredDate(blank: true, nullable: true)
        systemUpdateDate(blank: true, nullable: true)
        biometricValue(blank: true, nullable: true)
        updatedDate(blank: true, nullable: true)
        period(blank: true, nullable: true)
        divisionId(blank: true, nullable: true)
        biometricLevel(blank: true, nullable: true)

    }

    @Override
    public Map toMap() {
        def result = [:];
        result.unblindMemberId = this.unblindMemberId;
        result.groupId= this.groupId
        result.biometricId = this.biometricId
        result.biometricName = this.biometricName
        result.measuredDate = this.measuredDate
        result.systemUpdateDate = this.systemUpdateDate
        result.biometricValue = this.biometricValue
        result.updatedDate = this.updatedDate
        result.period = this.period
        result.divisionId= this.divisionId
        result.biometricLevel=this.biometricLevel

        result.id = this.id;
        return result;
    }

}
