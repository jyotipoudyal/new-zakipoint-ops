package com.z5

import com.configurations.utils.DomainOperations

import java.sql.Date

/**
 * Created by sktamang on 9/5/16.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class Campaign implements DomainOperations{

    String campaignId
    String campaignName
    String campaignSummary
    String programId
    String programName
    Date campaignDate
    String campaignType
    String campaignTarget
    Date updatedDate

    String udf1_campaign
    String udf2_campaign
    String udf3_campaign
    String udf4_campaign
    String udf5_campaign

    Double udf6_campaign
    Double udf7_campaign
    Double udf8_campaign
    Double udf9_campaign
    Double udf10_campaign

    Date udf11_campaign
    Date udf12_campaign
    Date udf13_campaign
    Date udf14_campaign
    Date udf15_campaign

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        campaignId( blank: true, nullable: true)
        campaignName(blank: true, nullable: true)
        campaignSummary(blank: true, nullable: true)
        programId(blank: true, nullable: true)
        programName(blank: true, nullable: true)
        campaignDate(blank: true, nullable: true)
        campaignType(blank: true, nullable: true)
        campaignTarget(blank: true, nullable: true)
        updatedDate(blank: true, nullable: true)

        udf1_campaign(blank: true, nullable: true)
        udf2_campaign(blank: true, nullable: true)
        udf3_campaign(blank: true, nullable: true)
        udf4_campaign(blank: true, nullable: true)
        udf5_campaign(blank: true, nullable: true)
        udf6_campaign(blank: true, nullable: true)
        udf7_campaign(blank: true, nullable: true)
        udf8_campaign(blank: true, nullable: true)
        udf9_campaign(blank: true, nullable: true)
        udf10_campaign(blank: true, nullable: true)
        udf11_campaign(blank: true, nullable: true)
        udf12_campaign(blank: true, nullable: true)
        udf13_campaign(blank: true, nullable: true)
        udf14_campaign(blank: true, nullable: true)
        udf15_campaign(blank: true, nullable: true)
    }

    @Override
    public Map toMap() {
        def result = [:];
        result.id = this.id;
        result.campaignId = this.campaignId;
        result.campaignName = this.campaignName
        result.campaignSummary= this.campaignSummary
        result.programId = this.programId
        result.programName = this.programName
        result.campaignDate = this.campaignDate
        result.campaignType = this.campaignType
        result.campaignTarget = this.campaignTarget
        result.updatedDate = this.updatedDate

        result.udf1_campaign = this.udf1_campaign;
        result.udf2_campaign = this.udf2_campaign;
        result.udf3_campaign = this.udf3_campaign;
        result.udf4_campaign = this.udf4_campaign;
        result.udf5_campaign = this.udf5_campaign;
        result.udf6_campaign = this.udf6_campaign;
        result.udf7_campaign = this.udf7_campaign;
        result.udf8_campaign = this.udf8_campaign;
        result.udf9_campaign = this.udf9_campaign;
        result.udf10_campaign = this.udf10_campaign;
        result.udf11_campaign = this.udf11_campaign;
        result.udf12_campaign = this.udf12_campaign;
        result.udf13_campaign = this.udf13_campaign;
        result.udf14_campaign = this.udf14_campaign;
        result.udf15_campaign = this.udf15_campaign;

        return result;
    }

}
