package com.z5

import com.configurations.utils.DomainOperations


/**
 * Created by sktamang on 3/30/17.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class BiometricLevelInfo implements DomainOperations{

    String biometricId
    String levelRangeName
    Double  rangeBegin
    Double rangeEnd
    Integer sn

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        biometricId(blank: true, nullable: true)
        levelRangeName(blank: true, nullable: true)
        rangeBegin(blank: true, nullable: true)
        rangeEnd(blank: true, nullable: true)
        sn(blank: true, nullable: true)
    }

    @Override
    public Map toMap() {
        def result = [:];
        result.biometricId = this.biometricId;
        result.levelRangeName = this.levelRangeName
        result.rangeBegin = this.rangeBegin
        result.rangeEnd = this.rangeEnd
        result.sn = this.sn
        return result;
    }
}
