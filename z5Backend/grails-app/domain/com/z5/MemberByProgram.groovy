package com.z5

import com.configurations.utils.DomainOperations

import java.sql.Date

/**
 * Created by sktamang on 9/5/16.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
class MemberByProgram implements DomainOperations{

    String memberId
    String unblindMemberId
    String groupId
    String programId
    String mostRecentStatus
    Date statusChangeDate
    Date programIdentificationDate
    Date programEnrollmentDate
    Date updatedDate
    String divisionId

    String udf1_memberByProgram
    String udf2_memberByProgram
    String udf3_memberByProgram
    String udf4_memberByProgram
    String udf5_memberByProgram

    Double udf6_memberByProgram
    Double udf7_memberByProgram
    Double udf8_memberByProgram
    Double udf9_memberByProgram
    Double udf10_memberByProgram

    Date udf11_memberByProgram
    Date udf12_memberByProgram
    Date udf13_memberByProgram
    Date udf14_memberByProgram
    Date udf15_memberByProgram

    static mapping = {
        datasource 'z5cohort'
//        Date =
    }

    static constraints = {
        memberId( blank: true, nullable: true)
        unblindMemberId(blank: true, nullable: true)
        groupId(blank: true, nullable: true)
        programId(blank: true, nullable: true)
        mostRecentStatus(blank: true, nullable: true)
        statusChangeDate(blank: true, nullable: true)
        programIdentificationDate(blank: true, nullable: true)
        programEnrollmentDate(blank: true, nullable: true)
        updatedDate(blank: true, nullable: true)
        divisionId(blank: true, nullable: true)

        udf1_memberByProgram(blank: true, nullable: true)
        udf2_memberByProgram(blank: true, nullable: true)
        udf3_memberByProgram(blank: true, nullable: true)
        udf4_memberByProgram(blank: true, nullable: true)
        udf5_memberByProgram(blank: true, nullable: true)
        udf6_memberByProgram(blank: true, nullable: true)
        udf7_memberByProgram(blank: true, nullable: true)
        udf8_memberByProgram(blank: true, nullable: true)
        udf9_memberByProgram(blank: true, nullable: true)
        udf10_memberByProgram(blank: true, nullable: true)
        udf11_memberByProgram(blank: true, nullable: true)
        udf12_memberByProgram(blank: true, nullable: true)
        udf13_memberByProgram(blank: true, nullable: true)
        udf14_memberByProgram(blank: true, nullable: true)
        udf15_memberByProgram(blank: true, nullable: true)
    }

    @Override
    public Map toMap() {
        def result = [:];
        result.memberId = this.memberId
        result.unblindMemberId = this.unblindMemberId;
        result.groupId= this.groupId
        result.programId = this.programId
        result.mostRecentStatus = this.mostRecentStatus
        result.statusChangeDate = this.statusChangeDate
        result.programIdentificationDate = this.programIdentificationDate
        result.programEnrollmentDate = this.programEnrollmentDate
        result.updatedDate = this.updatedDate
        result.divisionId= this.divisionId

        result.udf1_memberByProgram = this.udf1_memberByProgram;
        result.udf2_memberByProgram = this.udf2_memberByProgram;
        result.udf3_memberByProgram = this.udf3_memberByProgram;
        result.udf4_memberByProgram = this.udf4_memberByProgram;
        result.udf5_memberByProgram = this.udf5_memberByProgram;
        result.udf6_memberByProgram = this.udf6_memberByProgram;
        result.udf7_memberByProgram = this.udf7_memberByProgram;
        result.udf8_memberByProgram = this.udf8_memberByProgram;
        result.udf9_memberByProgram = this.udf9_memberByProgram;
        result.udf10_memberByProgram = this.udf10_memberByProgram;
        result.udf11_memberByProgram = this.udf11_memberByProgram;
        result.udf12_memberByProgram = this.udf12_memberByProgram;
        result.udf13_memberByProgram = this.udf13_memberByProgram;
        result.udf14_memberByProgram = this.udf14_memberByProgram;
        result.udf15_memberByProgram = this.udf15_memberByProgram;
        result.id = this.id;
        return result;
    }

}
