package com

import com.configurations.utils.DomainOperations


class MemberByLevels implements DomainOperations{

    String groupId;
    String category;
    String mbrId;
    String p1;
    String p2;
    String p3;

    static mapping = {
        datasource 'z5cohort'
    }

    static constraints = {
        groupId(blank: true, nullable: true)
        category(blank: true, nullable: true)
        mbrId(blank: true, nullable: true)
        p1(blank: true, nullable: true)
        p2(blank: true, nullable: true)
        p3(blank: true, nullable: true)
    }

    public Map toMap() {
        def result = [:];
        result.groupId = this.groupId
        result.category = this.category
        result.mbrId = this.mbrId
        result.p1 = this.p1
        result.p2 = this.p2
        result.p3 = this.p3
        return result;
    }

}
