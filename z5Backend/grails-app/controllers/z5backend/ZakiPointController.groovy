package z5backend

import com.configurations.config.AbstractConfigurationManager
import com.zakipoint.service.CohortService
import com.zakipoint.service.ReportFromDomainService
import com.zakipoint.service.ZakiPointService
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import zakipointCore.commonUtils.LevelUtils
import zakipointCore.commonUtils.SearchRequest
import zakipointCore.commonUtils.SpringBeanUtils

/**
 * Created by sktamang on 9/24/15.
 */
class ZakiPointController {

    private final int defaultPageNumber = 1;
    private final int defaultPageSize = 5;

    SearchRequest searchRequest
    def loggerService
    @Autowired AbstractConfigurationManager configurationManager

    def index = {
        try {
            render report() as JSON
        }
        catch (Exception e){
            JSONObject jsonObject=new JSONObject()
            jsonObject.put("error",e.getMessage())
            render jsonObject as JSON
            throw e;
        }
    }

    def report = {

        long initial    = new Date().getTime()

        println ("INFO: Loading report :: with request paramenters")
        println ("------------------------------------------------")
        println ("------------------------------------------------")

        String result   = reportData()
        long first      = new Date().getTime()
        JSONObject data = result ? JSON.parse(result) : new JSONObject()
        long last       = new Date().getTime()

        println("INFO: parsed and rendered in " + (last - first)/1000 + " s ")
        println("INFO: whole service " + (last - initial)/1000 + " s ")
        data
    }

    private def reportData() {
        searchRequest = searchRequest ?: new SearchRequest(requestParameters: new HashMap<String, String>())
        configureParams()


        params.each { k , v ->
            println "$k = $v"
        }
        println ("------------------------------------------------")
        println ("------------------------------------------------")

        def serviceClass=getService(controllerName)
        return serviceClass.getResults(searchRequest)
    }


    private void configureParams() {
        configurationManager.setIndex(params)
        if (!params.containsKey("page")) {
            params.put("page", String.valueOf(defaultPageNumber));
        }
        if (!params.containsKey("pageSize")) {
            params.put("pageSize", String.valueOf(defaultPageSize));
        }
        /*For loa fields : for eg. groupId = group */
        for (String level : LevelUtils.getLevels(params.clientId)) {
            if (params.containsKey(level + "Id")) {
                String loa = params.get(level + "Id")
                params.remove(level + "Id")
                params.put(level, loa)
            }
        }
        params.put("clientId", params.get("index_name"))
        searchRequest.requestParameters.putAll(params)
    }

    ZakiPointService getService(String beanName) {

        if (SpringBeanUtils.doesBeanExist(beanName+"."+"service")){
            return getServiceBean(beanName)
        }else{
            return getServiceBean("default")
        }
    }

    private static ZakiPointService getServiceBean(String controller) {
        (ZakiPointService) SpringBeanUtils.getBean(controller + "." + "service")
    }


    def cohort = {
        render CohortService.getCohortList(params)
    }

    def cohortTrack = {
        render CohortService.getCohortTrack(params)
    }

    def cohortBasedMetrics={
        render CohortService.getCohortBasedMetrics(params)
    }

    def reportFromDomain = {
        render ReportFromDomainService.hanldeRequest(params) as JSON
    }
}
