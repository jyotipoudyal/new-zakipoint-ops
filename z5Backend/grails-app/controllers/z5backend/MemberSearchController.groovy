package z5backend

import com.configurations.config.AbstractConfigurationManager
import com.configurations.search.MemberSearchRequest
import com.zakipoint.service.SearchService
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired

class MemberSearchController {

    def SearchService searchService

    @Autowired AbstractConfigurationManager configurationManager
    MemberSearchRequest memberSearchRequest

    def index = {
        render memberSearch() as JSON
    }


     def memberSearch() {
        println "${new Date().format("yyyy-MM-dd hh:mm:ss")} MemberSearchController.memberSearch Called"
        long startTime = System.nanoTime();
        memberSearchRequest = new MemberSearchRequest(requestParameters: new HashMap<String, String>())
        configurationManager.setIndex(params)
        configureParams(memberSearchRequest)
        memberSearchRequest.initialize()
        println "Time taken before service call ${((System.nanoTime() - startTime) / 1000000000)} seconds."
        JSONObject result = searchService.search(memberSearchRequest)
        println "${new Date().format("yyyy-MM-dd hh:mm:ss")} MemberSearchController.memberSearch executed in ${((System.nanoTime() - startTime) / 1000000000)} seconds."
        result
    }


    private void configureParams(MemberSearchRequest memberSearchRequest) {
        long startTime = System.nanoTime();
        params.remove("controller")
        params.remove("action")
        params.put("clientId", params.get("index_name"))
        memberSearchRequest.requestParameters.putAll(params)
        println "Time taken to configure params : ${((System.nanoTime() - startTime) / 1000000000)} seconds."
    }

}
