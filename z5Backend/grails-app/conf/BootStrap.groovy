import com.configurations.config.AbstractConfigurationManager
import com.configurations.config.ConfigurationFactory
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes
import org.springframework.context.ApplicationContext

class BootStrap {
    def grailsApplication

    List<String> tableScriptFiles = new ArrayList<>()

    def init = { servletContext ->
        loadClasses();
//        setBenchmarkValues()
        ApplicationContext context = servletContext.getAttribute(GrailsApplicationAttributes.APPLICATION_CONTEXT)
        AbstractConfigurationManager configurationManager=(AbstractConfigurationManager)context.getBean("configurationManager");
        servletContext.setAttribute('tableScriptFiles', tableScriptFiles)
        ConfigurationFactory.init(configurationManager,tableScriptFiles.toArray() as String[]);
    }
    def destroy = {
    }
    def loadClasses={
        def classes=grailsApplication.allClasses.findAll {
            it.getName().contains("dastables")
        }
        for (Class o : classes) {
            tableScriptFiles.add(o.getName())
        }
    }

//    def setBenchmarkValues = {
//        List<ClientSetting> list = ClientSetting.findAll()
//        for (ClientSetting client : list) {
//            ConfigurationFactory.benchmark.put(client.clientId, client.benchmark)
//            if(client.clusterConfigFile!=null && !client.clusterConfigFile.isEmpty()){
//                ConfigurationFactory.clusterConfigFiles.put(client.clientId, client.clusterConfigFile)
//            }
//        }
//    }

}
