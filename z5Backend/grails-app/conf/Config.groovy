
import grails.util.Environment
grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [html: ['text/html', 'application/xhtml+xml'],
                     xml: ['text/xml', 'application/xml'],
                     text: 'text/plain',
                     js: 'text/javascript',
                     rss: 'application/rss+xml',
                     atom: 'application/atom+xml',
                     css: 'text/css',
                     csv: 'text/csv',
                     all: '*/*',
                     json: ['application/json', 'text/json'],
                     form: 'application/x-www-form-urlencoded',
                     multipartForm: 'multipart/form-data'
]


// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable for AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []

environments {
    devEnv {
        qualifier="dev_"
        copyIndex = ""
        dataIndex = ""
    }
    production {
        grails.serverURL = "http://www.changeme.com"
        qualifier="prod_"
        copyIndex = ""
        dataIndex = ""
        serverConfig = "ServersConfigA"
    }
    development {
        grails.serverURL = "http://localhost:8080/${appName}"
        qualifier="dev_"
        copyIndex = ""
        dataIndex = ""
        serverConfig = "ServersConfigA"
        benchmarkType = "DevBenchmark"
        ServersConfigA {
            clusterName = "zakipoint_es"
            node1 {
                name = "Node1"
                hostname = "172.19.0.74"
                port = 9300
                httpPort = 9200
            }
        }
        disable.auto.recompile=false
        grails.reload.enabled = true
        grails.gsp.enable.reload=true

    }

}

// log4j configuration
log4j = {

}


grails {
    mail {
    }
}