import com.configurations.config.AbstractConfigurationManager
import com.configurations.search.MemberSearchRequest
import com.zakipoint.service.ZakiPointService
import zakipointCore.dashBoard.MedicalClaimsPmPmBuilder
import zakipointCore.dashBoard.MedicalClaimsPmPmListener
import zakipointCore.dashBoard.PharmacyClaimsPmPmBuilder
import zakipointCore.dashBoard.PharmacyClaimsPmPmListener
import zakipointCore.dashBoard.populationRisk.PopulationRiskListener
import zakipointCore.dashBoard.populationRisk.PopulationRiskQueryBuilder
import zakipointCore.dashBoard.populationRisk.PopulationRiskSankeyBuilder
import zakipointCore.dashBoard.populationRisk.PopulationRiskSankeyBuilderListener
import zakipointCore.dashBoard.populationRisk.PopulationRiskSankeyListener
import zakipointCore.dashBoard.populationRiskByTopChronicConditions.PopulationRiskByTopChronicConditionBuilder
import zakipointCore.dashBoard.populationRiskByTopChronicConditions.PopulationRiskByTopChronicConditionListener
import zakipointCore.drill.filters.ChronicComorbiditiesDrillFilter
import zakipointCore.drill.filters.ErDrillFilter
import zakipointCore.drill.filters.zphMemberDrillBuilderListener.PopulationRiskDrillFilter
import zakipointCore.features.biometric.biometricMemberFlow.BiometricSankeyBuilderListener
import zakipointCore.features.biometric.biometricMemberFlow.BiometricTrendingSankeyBuilder
import zakipointCore.features.biometric.biometricMemberFlow.BiometricTrendingSankeyListener
import zakipointCore.features.biometric.dashboard.BiometricSnapshotBuilder
import zakipointCore.features.biometric.dashboard.BiometricSnapshotBuilderListener
import zakipointCore.features.biometric.dashboard.BiometricSnapshotListener
import zakipointCore.features.erUtilization.avoidableErVsUrgentCareVisit.AvoidableErVsUrgentVisitBuilder
import zakipointCore.features.erUtilization.avoidableErVsUrgentCareVisit.AvoidableErVsUrgentVisitListener
import zakipointCore.features.erUtilization.avoidableToTotalErRatio.AvoidableToTotalErRatioBuilder
import zakipointCore.features.erUtilization.avoidableToTotalErRatio.AvoidableToTotalErRatioListener
import zakipointCore.features.erUtilization.avoidableToTotalErRatio.AvoidabletoTotalErVisitRatioBuilder
import zakipointCore.features.erUtilization.avoidableToTotalErRatio.AvoidabletoTotalErVisitRatioListener
import zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions.ErUtilizatinMonthlyTrendListener
import zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions.ErUtilzationMonthlyTrendBuilder
import zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions.ErVisitsLikelyToIncreaseBuilder
import zakipointCore.features.erUtilization.erUtilizationTrendsAndPredictions.ErVisitsLikelyToIncreaseListener
import zakipointCore.features.erUtilization.topChronicConditionErPopulations.TopChronicConditionErBuilder
import zakipointCore.features.erUtilization.topChronicConditionErPopulations.TopChronicConditionErDetailBuilder
import zakipointCore.features.erUtilization.topChronicConditionErPopulations.TopChronicConditionErDetailFormatter
import zakipointCore.features.erUtilization.topChronicConditionErPopulations.TopChronicConditionErDetailListener
import zakipointCore.features.erUtilization.topChronicConditionErPopulations.TopChronicConditionErLIstener
import zakipointCore.features.erUtilization.topConditionByCost.TopConditionByCostAllErBuilder
import zakipointCore.features.erUtilization.topConditionByCost.TopConditionByCostAllErListener
import zakipointCore.features.erUtilization.topConditionByCost.TopConditionByCostAvoidErBuilder
import zakipointCore.features.erUtilization.topConditionByCost.TopConditionByCostAvoidErListener
import zakipointCore.features.erUtilization.useOfErMissedPrvntvOrRoutnCare.CareAlertErVisitBuilder
import zakipointCore.features.erUtilization.useOfErMissedPrvntvOrRoutnCare.CareAlertErVisitListener
import zakipointCore.features.familySpouse.HighFamilySpouseCostBuilder
import zakipointCore.features.familySpouse.HighFamilySpouseCostListener
import zakipointCore.features.featureInfo.FeatureInfoBuilder
import zakipointCore.features.featureInfo.FeatureInfoListener
import zakipointCore.features.gapInCare.GapInCareBuilder
import zakipointCore.features.gapInCare.GapInCareListener
import zakipointCore.features.gapInCare.GapInCareRealtimeBuilder
import zakipointCore.features.groupMemberView.GroupMemberCountViewBuilder
import zakipointCore.features.groupMemberView.GroupMemberCountViewListener
import zakipointCore.features.highPharmacyCost.PharmacyBrandNameListBuilder
import zakipointCore.features.highPharmacyCost.PharmacyBrandNameListListener
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostBuilder
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostDetailBuilder
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostDetailListener
import zakipointCore.features.highPharmacyCost.rxBrandGenericCostDrivers.RxBrandGenericCostListener
import zakipointCore.features.highPharmacyCost.rxHistoricAndPredictedPharmacyCost.RxClaimsLikelyToIncreaseBuilder
import zakipointCore.features.highPharmacyCost.rxHistoricAndPredictedPharmacyCost.RxClaimsLikelyToIncreaseListener
import zakipointCore.features.highPharmacyCost.rxHistoricAndPredictedPharmacyCost.RxHistoricPredictiveBuilder
import zakipointCore.features.highPharmacyCost.rxHistoricAndPredictedPharmacyCost.RxHistoricPredictiveListener
import zakipointCore.features.highPharmacyCost.rxRetailMailCostDrivers.RxMailRetailCostDetailBuilder
import zakipointCore.features.highPharmacyCost.rxRetailMailCostDrivers.RxMailRetailCostDetailListener
import zakipointCore.features.highPharmacyCost.rxRetailMailCostDrivers.RxRetailMailCostBuilder
import zakipointCore.features.highPharmacyCost.rxRetailMailCostDrivers.RxRetailMailCostListener
import zakipointCore.features.highPharmacyCost.rxSpecialityCostDrivers.RxSpecialityCostBuilder
import zakipointCore.features.highPharmacyCost.rxSpecialityCostDrivers.RxSpecialityCostDetailBuilder
import zakipointCore.features.highPharmacyCost.rxSpecialityCostDrivers.RxSpecialityCostDetailListener
import zakipointCore.features.highPharmacyCost.rxSpecialityCostDrivers.RxSpecialityCostListener
import zakipointCore.features.network.highClaimantsOON.HighOutNetworkMemberBuilder
import zakipointCore.features.network.highClaimantsOON.HighOutNetworkMemberListener
import zakipointCore.features.network.networkTrendingSankey.NetworkTrendingSankeyBuilder
import zakipointCore.features.network.networkTrendingSankey.NetworkTrendingSankeyListener
import zakipointCore.features.network.outOfNetworkProvider.TopOutOfNetworkProviderBuilder
import zakipointCore.features.network.outOfNetworkProvider.TopOutOfNetworkProviderListener
import zakipointCore.features.network.outOfNetworkProvider.TopOutOfNetworkProviderLocationBuilder
import zakipointCore.features.network.outOfNetworkProvider.TopOutOfNetworkProviderLocationListener
import zakipointCore.features.network.outOfNetworkProvider.TopOutOfNetworkProviderSpecialityBuilder
import zakipointCore.features.network.outOfNetworkProvider.TopOutOfNetworkProviderSpecialityListener
import zakipointCore.features.network.usage.NetworkUsageBuilder
import zakipointCore.features.network.usage.NetworkUsageListener
import zakipointCore.features.programTracking.ProgramTrackingBuilder
import zakipointCore.features.programTracking.ProgramTrackingListener
import zakipointCore.features.zPopulation.chronicCostTrend.ChronicConditionCostTrendBuilder
import zakipointCore.features.zPopulation.chronicCostTrend.ChronicConditionCostTrendListener
import zakipointCore.features.zPopulation.chronicCostTrend.ChronicCostLikelyToIncreaseBuilder
import zakipointCore.features.zPopulation.chronicCostTrend.ChronicCostLikelyToIncreaseLietener
import zakipointCore.features.zPopulation.recommendedCareForChronicPatients.RecommendedCareForChronicBuilder
import zakipointCore.features.zPopulation.recommendedCareForChronicPatients.RecommendedCareForChronicListener
import zakipointCore.features.zPopulation.rootConditionAlongOtherChronicConditions.RootCondnAlongOtherChronicBuilder
import zakipointCore.features.zPopulation.rootConditionAlongOtherChronicConditions.RootCondnAlongOtherChronicListener
import zakipointCore.features.zPopulation.useOfErByChronicMembers.NewUseOfErByChronicMembersBuilder
import zakipointCore.features.zPopulation.useOfErByChronicMembers.NewUseOfErByChronicMembersListener
import zakipointCore.features.zPopulation.useOfErByChronicMembers.UseOfErByChronicMembersBuilder
import zakipointCore.features.zPopulation.useOfErByChronicMembers.UseOfErByChronicMembersListener
import zakipointCore.formatters.DefaultFormatter
import zakipointCore.formatters.PopulationRiskFormatter
import zakipointCore.renderers.GapInCareCounter
import zakipointCore.renderers.MemberSearchZphRenderer
import zakipointCore.renderers.MemberStatusZphRenderer

// Place your Spring DSL code here
beans = {
    "default.dasformatter"(DefaultFormatter) { bean -> bean.scope = "prototype" }
    "configurationManager"(AbstractConfigurationManager){bean-> bean.scope="prototype"}
    "memberSearchRequest"(MemberSearchRequest){ bean-> bean.scope="prototype"}
    "zakiPoint.service"(ZakiPointService){ bean-> bean.scope="prototype" }

    "msZakipoint.renderer"(MemberSearchZphRenderer){ bean-> bean.scope="prototype" }
    "erZakipoint.reportFilter" (ErDrillFilter){ bean -> bean.scope = "prototype" }
    "popilationRiskZakipoint.reportFilter" (PopulationRiskDrillFilter){ bean -> bean.scope = "prototype" }
    "msZakipoint.reportFilter" (ChronicComorbiditiesDrillFilter){ bean -> bean.scope = "prototype" }
    "gapInCareCounter.nested.renderer"(GapInCareCounter) { bean -> bean.scope = "prototype" }
    "memberStatusZph.nested.renderer"(MemberStatusZphRenderer) { bean -> bean.scope = "prototype" }

    /* These reports are contained in the dashboard
    * */
    "medicalClaimsPmpm.builder"(MedicalClaimsPmPmBuilder){ bean-> bean.scope="prototype" }
    "medicalClaimsPmpm.listener"(MedicalClaimsPmPmListener){ bean-> bean.scope="prototype" }

    "pharmacyClaimsPmpm.builder"(PharmacyClaimsPmPmBuilder){ bean-> bean.scope="prototype" }
    "pharmacyClaimsPmpm.listener"(PharmacyClaimsPmPmListener){ bean-> bean.scope="prototype" }

    "populationRisk.builder"(PopulationRiskQueryBuilder){ bean-> bean.scope="prototype" }
    "populationRisk.listener"(PopulationRiskListener){ bean-> bean.scope="prototype" }
    "populationRisk.formatters"(PopulationRiskFormatter) { bean -> bean.scope = "prototype" }
    "populationRiskSankey.builder"(PopulationRiskSankeyBuilder) { bean -> bean.scope = "prototype" }
    "populationRiskSankey.listener"(PopulationRiskSankeyListener) { bean -> bean.scope = "prototype" }

    "groupMemberCountView.builder"(GroupMemberCountViewBuilder){ bean-> bean.scope="prototype" }
    "groupMemberCountView.listener"(GroupMemberCountViewListener){ bean-> bean.scope="prototype" }

    "featureInfo.builder"(FeatureInfoBuilder){ bean-> bean.scope="prototype" }
    "featureInfo.listener"(FeatureInfoListener){ bean-> bean.scope="prototype" }

    "populationRiskByTopChronicCondition.builder"(PopulationRiskByTopChronicConditionBuilder){ bean-> bean.scope="prototype" }
    "populationRiskByTopChronicCondition.listener"(PopulationRiskByTopChronicConditionListener){ bean-> bean.scope="prototype" }


    //These are for the High PharmacyCost Feature
//        "highPharmacyCost:default.builder"(HighPharmacyCostBuilder){bean-> bean.scope="prototype" }
//        "highPharmacyCost:default.listener"(HighPharmacyCostListener){bean-> bean.scope="prototype" }

//        "highPharmacyCost:speciality.builder"(HighPharmacyCostBuilder){bean-> bean.scope="prototype" }
//        "highPharmacyCost:speciality.listener"(HighPharmacyCostListener){bean-> bean.scope="prototype" }

    "pharmacyBrandNameList.builder"(PharmacyBrandNameListBuilder){ bean-> bean.scope="prototype" }
    "pharmacyBrandNameList.listener"(PharmacyBrandNameListListener){ bean-> bean.scope="prototype" }

    "rxBrandGenericCost.builder"(RxBrandGenericCostBuilder){ bean-> bean.scope="prototype" }
    "rxBrandGenericCost.listener"(RxBrandGenericCostListener){ bean-> bean.scope="prototype" }

    "rxHistoricPredictive.builder"(RxHistoricPredictiveBuilder){ bean-> bean.scope="prototype" }
    "rxHistoricPredictive.listener"(RxHistoricPredictiveListener){ bean-> bean.scope="prototype" }

    "rxMailRetailCost.builder"(RxRetailMailCostBuilder){ bean-> bean.scope="prototype" }
    "rxMailRetailCost.listener"(RxRetailMailCostListener){ bean-> bean.scope="prototype" }

    "rxSpecialityCost.builder"(RxSpecialityCostBuilder){ bean-> bean.scope="prototype" }
    "rxSpecialityCost.listener"(RxSpecialityCostListener){ bean-> bean.scope="prototype" }

    "rxClaimsLikelyToIncrease.builder"(RxClaimsLikelyToIncreaseBuilder){ bean-> bean.scope="prototype" }
    "rxClaimsLikelyToIncrease.listener"(RxClaimsLikelyToIncreaseListener){ bean-> bean.scope="prototype" }

    "rxBrandGenericCostDetail.builder"(RxBrandGenericCostDetailBuilder){ bean-> bean.scope="prototype" }
    "rxBrandGenericCostDetail.listener"(RxBrandGenericCostDetailListener){ bean-> bean.scope="prototype" }

    "rxMailRetailCostDetail.builder"(RxMailRetailCostDetailBuilder){ bean-> bean.scope="prototype" }
    "rxMailRetailCostDetail.listener"(RxMailRetailCostDetailListener){ bean-> bean.scope="prototype" }

    "rxSpecialityCostDetail.builder"(RxSpecialityCostDetailBuilder){ bean-> bean.scope="prototype" }
    "rxSpecialityCostDetail.listener"(RxSpecialityCostDetailListener){ bean-> bean.scope="prototype" }


    //These are for the Er Utilization Feature

    "avoidableToTotalErRatio.builder"(AvoidableToTotalErRatioBuilder){ bean-> bean.scope="prototype" }
    "avoidableToTotalErRatio.listener"(AvoidableToTotalErRatioListener){ bean-> bean.scope="prototype" }

    "avoidabletoTotalErVisitRatio.builder"(AvoidabletoTotalErVisitRatioBuilder){ bean-> bean.scope="prototype" }
    "avoidabletoTotalErVisitRatio.listener"(AvoidabletoTotalErVisitRatioListener){ bean-> bean.scope="prototype" }

//        "overErUtilization.builder"(OverErUtilizationBuilder){bean-> bean.scope="prototype" }
//        "overErUtilization.listener"(OverErUtilizationListener){bean-> bean.scope="prototype" }

//        "savingsByAvoidableEr.builder"(SavingsByAvoidableErBuilder){bean-> bean.scope="prototype" }
//        "savingsByAvoidableEr.listener"(SavingsByAvoidableErListener){bean-> bean.scope="prototype" }

    "topConditionByCostAllEr.builder"(TopConditionByCostAllErBuilder){ bean-> bean.scope="prototype" }
    "topConditionByCostAllEr.listener"(TopConditionByCostAllErListener){ bean-> bean.scope="prototype" }
    "topConditionByCostAvoidEr.builder"(TopConditionByCostAvoidErBuilder){ bean-> bean.scope="prototype" }
    "topConditionByCostAvoidEr.listener"(TopConditionByCostAvoidErListener){ bean-> bean.scope="prototype" }

    "erUtilizationTrend.builder"(ErUtilzationMonthlyTrendBuilder){ bean-> bean.scope="prototype" }
    "erUtilizationTrend.listener"(ErUtilizatinMonthlyTrendListener){ bean-> bean.scope="prototype" }

    "topChronicConditionEr.builder"(TopChronicConditionErBuilder){ bean-> bean.scope="prototype" }
    "topChronicConditionEr.listener"(TopChronicConditionErLIstener){ bean-> bean.scope="prototype" }
    "topChronicConditionErDetail.builder"(TopChronicConditionErDetailBuilder){ bean-> bean.scope="prototype" }
    "topChronicConditionErDetail.listener"(TopChronicConditionErDetailListener){ bean-> bean.scope="prototype" }
    "topChronicConditionErDetail.formatters"(TopChronicConditionErDetailFormatter) { bean -> bean.scope = "prototype" }

    "careAlertErVisit.builder"(CareAlertErVisitBuilder){ bean-> bean.scope="prototype" }
    "careAlertErVisit.listener"(CareAlertErVisitListener){ bean-> bean.scope="prototype" }

    "erVisitsLikelyToIncrease.builder"(ErVisitsLikelyToIncreaseBuilder){ bean-> bean.scope="prototype" }
    "erVisitsLikelyToIncrease.listener"(ErVisitsLikelyToIncreaseListener){ bean-> bean.scope="prototype" }

    "avoidableErVsUrgentVisit.builder"(AvoidableErVsUrgentVisitBuilder){ bean-> bean.scope="prototype" }
    "avoidableErVsUrgentVisit.listener"(AvoidableErVsUrgentVisitListener){ bean-> bean.scope="prototype" }

//        "potentailSavingsForEr.builder"(PotentialSavingsForErBuilder){bean-> bean.scope="prototype" }
//        "potentailSavingsForEr.listener"(PotentitalSavingsForErListener){bean-> bean.scope="prototype" }


    //these are for the zPopulation
    "diabetesAlongOtherChronic.builder"(RootCondnAlongOtherChronicBuilder){ bean-> bean.scope="prototype" }
    "diabetesAlongOtherChronic.listener"(RootCondnAlongOtherChronicListener){ bean-> bean.scope="prototype" }

    "diabetesCost.builder"(ChronicConditionCostTrendBuilder){ bean-> bean.scope="prototype" }
    "diabetesCost.listener"(ChronicConditionCostTrendListener){ bean-> bean.scope="prototype" }

    "recommendedCareForDiabetes.builder"(RecommendedCareForChronicBuilder){ bean-> bean.scope="prototype" }
    "recommendedCareForDiabetes.listener"(RecommendedCareForChronicListener){ bean-> bean.scope="prototype" }

    "useOfErByChronicMembers.builder"(UseOfErByChronicMembersBuilder){ bean-> bean.scope="prototype" }
    "useOfErByChronicMembers.listener"(UseOfErByChronicMembersListener){ bean-> bean.scope="prototype" }

    "newUseOfErByChronicMembers.builder"(NewUseOfErByChronicMembersBuilder){ bean-> bean.scope="prototype" }
    "newUseOfErByChronicMembers.listener"(NewUseOfErByChronicMembersListener){ bean-> bean.scope="prototype" }

    "ChronicCostLikelyToIncrease.builder"(ChronicCostLikelyToIncreaseBuilder){ bean-> bean.scope="prototype" }
    "ChronicCostLikelyToIncrease.listener"(ChronicCostLikelyToIncreaseLietener){ bean-> bean.scope="prototype" }


    //these are for the program tracking
    "programTracking.builder"(ProgramTrackingBuilder){ bean-> bean.scope="prototype" }
    "programTracking.listener"(ProgramTrackingListener){ bean-> bean.scope="prototype" }


    //these are for the gap in care section
    "gapInCare.builder"(GapInCareBuilder){ bean-> bean.scope="prototype" }
    "gapInCare.listener"(GapInCareListener){ bean-> bean.scope="prototype" }


    //these are for the inefficient network usage section
    "networkUsage.builder"(NetworkUsageBuilder){ bean-> bean.scope="prototype" }
    "networkUsage.listener"(NetworkUsageListener){ bean-> bean.scope="prototype" }

    "highOutNetworkMember.builder"(HighOutNetworkMemberBuilder){ bean-> bean.scope="prototype" }
    "highOutNetworkMember.listener"(HighOutNetworkMemberListener){ bean-> bean.scope="prototype" }

    "topOutOfNetworkProvider.builder"(TopOutOfNetworkProviderBuilder){ bean-> bean.scope="prototype" }
    "topOutOfNetworkProvider.listener"(TopOutOfNetworkProviderListener){ bean-> bean.scope="prototype" }

    "topOutOfNetworkProviderLocation.builder"(TopOutOfNetworkProviderLocationBuilder){ bean-> bean.scope="prototype" }
    "topOutOfNetworkProviderLocation.listener"(TopOutOfNetworkProviderLocationListener){ bean-> bean.scope="prototype" }

    "topOutOfNetworkProviderSpeciality.builder"(TopOutOfNetworkProviderSpecialityBuilder){ bean-> bean.scope="prototype" }
    "topOutOfNetworkProviderSpeciality.listener"(TopOutOfNetworkProviderSpecialityListener){ bean-> bean.scope="prototype" }

    "networkTrendingSankey.builder"(NetworkTrendingSankeyBuilder){ bean-> bean.scope="prototype" }
    "networkTrendingSankey.listener"(NetworkTrendingSankeyListener){ bean-> bean.scope="prototype" }

    //these are for the biometric
    "biometricSnapshot.builder"(BiometricSnapshotBuilder){ bean-> bean.scope="prototype" }
    "biometricSnapshot.listener"(BiometricSnapshotListener){ bean-> bean.scope="prototype" }
    "biometricSnapshot"(BiometricSnapshotBuilderListener){ bean-> bean.scope="prototype" }

    "biometricTrendingSankey.builder"(BiometricTrendingSankeyBuilder){ bean-> bean.scope="prototype" }
    "biometricTrendingSankey.listener"(BiometricTrendingSankeyListener){ bean-> bean.scope="prototype" }
    "biometricSankeyBuilderListener"(BiometricSankeyBuilderListener){ bean-> bean.scope="prototype" }

    //these are for the High Family Spouse Cost
    "highFamilySpouseCost.builder"(HighFamilySpouseCostBuilder){ bean-> bean.scope="prototype" }
    "highFamilySpouseCost.listener"(HighFamilySpouseCostListener){ bean-> bean.scope="prototype" }


    //these are for the reports generated from the database
    //gap in care
    "gapInCareRealtime"(GapInCareRealtimeBuilder){ bean-> bean.scope="prototype" }
    "populationRiskSankeyBuilderListener"(PopulationRiskSankeyBuilderListener){ bean-> bean.scope="prototype" }
}
