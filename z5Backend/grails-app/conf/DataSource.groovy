
import org.hibernate.dialect.MySQL5InnoDBDialect
//
//dataSource_usermanagement {
//    pooled = true
//    driverClassName = 'com.mysql.jdbc.Driver'
//    dialect = MySQL5InnoDBDialect
//    pooled = true
//    dbCreate = 'update'
//    properties {
//        maxActive = 50
//        maxIdle = 25
//        minIdle = 5
//        initialSize = 5
//        maxWait = 10000
//        maxAge = 10 * 60000
//        removeAbandoned=true
//        removeAbandonedTimeout=400
//        logAbandoned=true
//        minEvictableIdleTimeMillis=1800000
//        timeBetweenEvictionRunsMillis=1800000
//        numTestsPerEvictionRun=3
//        testOnBorrow=true
//        testWhileIdle=true
//        testOnReturn=true
//        validationQuery="SELECT 1"
//    }
//}
//
//dataSource_ds2 {
//    pooled = true
//    driverClassName = 'com.mysql.jdbc.Driver'
//    dialect = MySQL5InnoDBDialect
//    pooled = true
//    dbCreate = 'update'
//    properties {
//        maxActive = 50
//        maxIdle = 25
//        minIdle = 5
//        initialSize = 5
//        maxWait = 10000
//        maxAge = 10 * 60000
//        removeAbandoned=true
//        removeAbandonedTimeout=400
//        logAbandoned=true
//        minEvictableIdleTimeMillis=1800000
//        timeBetweenEvictionRunsMillis=1800000
//        numTestsPerEvictionRun=3
//        testOnBorrow=true
//        testWhileIdle=true
//        testOnReturn=true
//        validationQuery="SELECT 1"
//    }
//}



hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
//    cache.region.factory_class = 'org.hibernate.cache.SingletonEhCacheRegionFactory' // Hibernate 3
    cache.region.factory_class = 'org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory' // Hibernate 4
    singleSession = true // configure OSIV singleSession mode
    flush.mode = 'manual' // OSIV session flush mode outside of transactional context
}
// environment specific settings
environments {
    development {

//        dataSource_usermanagement {
//            url = "jdbc:mysql://localhost:3306/usermanagement?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
//            driverClassName = 'com.mysql.jdbc.Driver'
//            pooled = true
//            username = 'root'
//            password = 'root'
//            dbCreate ='update';
//            properties {
//                maxActive = 50
//                maxIdle = 25
//                minIdle = 5
//                initialSize = 5
//                maxWait = 10000
//                maxAge = 10 * 60000
//                removeAbandoned=true
//                removeAbandonedTimeout=400
//                logAbandoned=true
//                minEvictableIdleTimeMillis=1800000
//                timeBetweenEvictionRunsMillis=1800000
//                numTestsPerEvictionRun=3
//                testOnBorrow=true
//                testWhileIdle=true
//                testOnReturn=true
//                validationQuery="SELECT 1"
//            }
//        }
//
//        dataSource_ds2 {
//            url = "jdbc:mysql://localhost:3306/dasSetting?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
//            driverClassName = 'com.mysql.jdbc.Driver'
//            pooled = true
//            username = 'root'
//            password = 'root'
//            dbCreate ='update';
//            properties {
//                maxActive = 50
//                maxIdle = 25
//                minIdle = 5
//                initialSize = 5
//                maxWait = 10000
//                maxAge = 10 * 60000
//                removeAbandoned=true
//                removeAbandonedTimeout=400
//                logAbandoned=true
//                minEvictableIdleTimeMillis=1800000
//                timeBetweenEvictionRunsMillis=1800000
//                numTestsPerEvictionRun=3
//                testOnBorrow=true
//                testWhileIdle=true
//                testOnReturn=true
//                validationQuery="SELECT 1"
//            }
//        }

        dataSource_programTracking{
            url="jdbc:mysql://localhost:3306/programTracking?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
            pooled = true
            driverClassName = 'com.mysql.jdbc.Driver'
            dbCreate = 'update'
            username = 'root'
            password = 'root'
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                maxWait = 10000
                maxAge = 10 * 60000
                removeAbandoned=true
                removeAbandonedTimeout=400
                logAbandoned=true
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }

        dataSource_z5cohort{
            url="jdbc:mysql://localhost:3306/z5cohort?autoReconnect=true&zeroDateTimeBehavior=convertToNull"
            pooled = true
            driverClassName = 'com.mysql.jdbc.Driver'
            dbCreate = 'update'
            username = 'root'
            password = 'root'
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                maxWait = 10000
                maxAge = 10 * 60000
                removeAbandoned=true
                removeAbandonedTimeout=400
                logAbandoned=true
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }

//    test {
//        dataSource {
//            driverClassName = 'com.mysql.jdbc.Driver'
//            pooled = true
//            dbCreate = "update"
//            url = "jdbc:mysql://localhost:3306/usermanagement"
//            username = "root"
//            password = ""
//        }
//    }

    production {
//        dataSource_ds2 {
//            driverClassName = 'com.mysql.jdbc.Driver'
//            pooled = true
//            username = ''
//            password = ''
//            url = ""
//            dbCreate = 'update'
//            properties {
//                maxActive = 50
//                maxIdle = 25
//                minIdle = 5
//                initialSize = 5
//                maxWait = 10000
//                maxAge = 10 * 60000
//                removeAbandoned=true
//                removeAbandonedTimeout=400
//                logAbandoned=true
//                minEvictableIdleTimeMillis=1800000
//                timeBetweenEvictionRunsMillis=1800000
//                numTestsPerEvictionRun=3
//                testOnBorrow=true
//                testWhileIdle=true
//                testOnReturn=true
//                validationQuery="SELECT 1"
//            }
//        }
//
//        dataSource_usermanagement {
//            driverClassName = 'com.mysql.jdbc.Driver'
//            pooled = true
//            dbCreate = "update"
//            url = ""
//            username = ""
//            password = ""
//            properties {
//                maxActive = 50
//                maxIdle = 25
//                minIdle = 5
//                initialSize = 5
//                maxWait = 10000
//                maxAge = 10 * 60000
//                removeAbandoned=true
//                removeAbandonedTimeout=400
//                logAbandoned=true
//                minEvictableIdleTimeMillis=1800000
//                timeBetweenEvictionRunsMillis=1800000
//                numTestsPerEvictionRun=3
//                testOnBorrow=true
//                testWhileIdle=true
//                testOnReturn=true
//                validationQuery="SELECT 1"
//            }
//        }

        dataSource_z5cohort{
            url=""
            pooled = true
            driverClassName = 'com.mysql.jdbc.Driver'
            dbCreate = 'update'
            username = ''
            password = ''
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                maxWait = 10000
                maxAge = 10 * 60000
                removeAbandoned=true
                removeAbandonedTimeout=400
                logAbandoned=true
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }


    devEnv {
//        dataSource_ds2 {
//            driverClassName = 'com.mysql.jdbc.Driver'
//            pooled = true
//            username = ''
//            password = ''
//            url = ""
//            dbCreate = 'update'
//            properties {
//                maxActive = 50
//                maxIdle = 25
//                minIdle = 5
//                initialSize = 5
//                maxWait = 10000
//                maxAge = 10 * 60000
//                removeAbandoned=true
//                removeAbandonedTimeout=400
//                logAbandoned=true
//                minEvictableIdleTimeMillis=1800000
//                timeBetweenEvictionRunsMillis=1800000
//                numTestsPerEvictionRun=3
//                testOnBorrow=true
//                testWhileIdle=true
//                testOnReturn=true
//                validationQuery="SELECT 1"
//            }
//        }
//
//        dataSource_usermanagement {
//            driverClassName = 'com.mysql.jdbc.Driver'
//            pooled = true
//            dbCreate = "update"
//            url = ""
//            username = ""
//            password = ""
//            properties {
//                maxActive = 50
//                maxIdle = 25
//                minIdle = 5
//                initialSize = 5
//                maxWait = 10000
//                maxAge = 10 * 60000
//                removeAbandoned=true
//                removeAbandonedTimeout=400
//                logAbandoned=true
//                minEvictableIdleTimeMillis=1800000
//                timeBetweenEvictionRunsMillis=1800000
//                numTestsPerEvictionRun=3
//                testOnBorrow=true
//                testWhileIdle=true
//                testOnReturn=true
//                validationQuery="SELECT 1"
//            }
//        }
        dataSource_programTracking{
            url=""
            pooled = true
            driverClassName = 'com.mysql.jdbc.Driver'
            dbCreate = 'update'
            username = ''
            password = ''
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                maxWait = 10000
                maxAge = 10 * 60000
                removeAbandoned=true
                removeAbandonedTimeout=400
                logAbandoned=true
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }

        dataSource_z5cohort{
            url=""
            pooled = true
            driverClassName = 'com.mysql.jdbc.Driver'
            dbCreate = 'update'
            username = ''
            password = ''
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                maxWait = 10000
                maxAge = 10 * 60000
                removeAbandoned=true
                removeAbandonedTimeout=400
                logAbandoned=true
                minEvictableIdleTimeMillis=1800000
                timeBetweenEvictionRunsMillis=1800000
                numTestsPerEvictionRun=3
                testOnBorrow=true
                testWhileIdle=true
                testOnReturn=true
                validationQuery="SELECT 1"
            }
        }
    }
}
