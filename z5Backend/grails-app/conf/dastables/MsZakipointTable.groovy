
/**
 * Created by sktamang on 5/26/16.
 */
package dastables

tables = {
    table() {
        tableCode('msZakipoint')
        tableName('MemberSearch')
        defaultColumns(['memberSearch'])
        fields(family: 'memberSearch')  {
            field(field: 'intMemberId', displayName: 'intMemberId', type: 'String')
            field(field: 'memberCityId', displayName: 'memberCity', type: 'String', phi:true)
            field(field: 'memberZip', displayName: 'memberZip', type: 'String')
            field(field: 'memberState', displayName: 'memberState', type: 'String')
            field(field: 'memberFullName', displayName: 'memberFullName', type: 'String',phi:true,toLowerCase:true)
            field(field: 'memberAge', displayName: 'memberAge', type: 'int',phi: true)
            field(field: 'rxTotalPaidAmount', displayName: 'rxTotalPaidAmount', type: 'float')
            field(field: 'medTotalPaidAmount', displayName: 'medTotalPaidAmount', type: 'float')
            field(field: 'totalPaidAmount', displayName: 'totalPaidAmount', type: 'float')
            field(field: 'prospectiveTotal', displayName: 'riskScore', type: 'double')
            field(field: 'memberGenderId', displayName: 'memberGenderId', type: 'String', phi: true)
            field(field: 'memberRelationshipClass', displayName: 'relationshipFlag', type: 'String')
            field(field: 'memberId', displayName: 'memberId', type: 'String')
            field(field: 'memberFirstName', displayName: 'memberFirstName', type: 'String',phi:true,toLowerCase:true)
            field(field: 'memberGender', displayName: 'memberGender', type: 'String',phi:true)
            field(field: 'memberLastName', displayName: 'memberLastName', type: 'String',phi:true,toLowerCase:true)
            field(field: 'memberDOB', displayName: 'memberDOB', type: 'Date',phi:true)
            field(field: 'qm', displayName: 'qm', type: 'nested', displayClass: 'gapInCareCounter')
            field(field: 'memberStatus', displayName: 'memberStatus', type: 'nested',displayClass: 'memberStatusZph')
            field(field: 'groupId', displayName: 'groupId', type: 'String',phi:true)
            field(field: 'divisionId', displayName: 'divisionId', type: 'String',phi:true)
            field(field: 'unblindMemberId', displayName: 'unblindMemberId', type: 'String',phi:true)


            //chronic Conditions
            field(field: 'hasAffectivePsychosis', displayName: 'affectivePsychosis', type: 'string')
            field(field: 'hasAsthma', displayName: 'asthma', type: 'string')
            field(field: 'hasAtrialFibrillation', displayName: 'atrialFibrillation', type: 'string')
            field(field: 'hasBloodDisorders', displayName: 'bloodDisorders', type: 'string')
            field(field: 'hasCad', displayName: 'CAD', type: 'string')
            field(field: 'hasCopd', displayName: 'COPD', type: 'string')
            field(field: 'hasCancer', displayName: 'cancer', type: 'string')
            field(field: 'hasChronicPain', displayName: 'chronicPain', type: 'string')
            field(field: 'hasCongestiveHeartFailure', displayName: 'chf', type: 'string')
            field(field: 'hasDemyelinatingDiseases', displayName: 'demyelinatingDiseases', type: 'string')
            field(field: 'hasDepression', displayName: 'depression', type: 'string')
            field(field: 'hasDiabetes', displayName: 'diabetes', type: 'string')
            field(field: 'hasEsrd', displayName: 'CKD', type: 'string')
            field(field: 'hasEatingDisorders', displayName: 'eatingDisorders', type: 'string')
            field(field: 'hasHivAids', displayName: 'HIVAIDS', type: 'string')
            field(field: 'hasHyperlipidemia', displayName: 'hyperlipidemia', type: 'string')
            field(field: 'hasHypertension', displayName: 'hypertension', type: 'string')
            field(field: 'hasImmuneDisorders', displayName: 'immuneDisorders', type: 'string')
            field(field: 'hasInflammatoryBowelDisease', displayName: 'inflammatoryBowelDisease', type: 'string')
            field(field: 'hasLiverDisease', displayName: 'liverDiseases', type: 'string')
            field(field: 'hasMorbidObesity', displayName: 'morbidObesity', type: 'string')
            field(field: 'hasOsteoarthritis', displayName: 'osteoarthritis', type: 'string')
            field(field: 'hasPeripheralVascularDisease', displayName: 'peripheralVascularDisease', type: 'string')
            field(field: 'hasRheumatoidArthritis', displayName: 'rheumatoidArthritis', type: 'string')
            field(field: 'chronicConditionCount', displayName: 'numberOfChronicConditions', type: 'int')

            field(field: 'acuteConcurrentTotalNormalizedToGroup', displayName: 'acuteConcurrentTotalNormalizedToGroup', type: 'int')
            field(field: 'chronicConcurrentTotalNormalizedToGroup', displayName: 'chronicConcurrentTotalNormalizedToGroup', type: 'int')
            field(field: 'concurrentTotalDoubleNormalizedToGroup', displayName: 'concurrentTotalDoubleNormalizedToGroup', type: 'int')
            field(field: 'acuteConcurrentTotalNormalizedToBob', displayName: 'acuteConcurrentTotalNormalizedToBob', type: 'int')
            field(field: 'chronicConcurrentTotalNormalizedToBob', displayName: 'chronicConcurrentTotalNormalizedToBob', type: 'int')
            field(field: 'concurrentTotalDoubleNormalizedToBob', displayName: 'concurrentTotalDoubleNormalizedToBob', type: 'int')


        }

        fields(family: 'nested') {
            field(field: 'qm.numerator', displayName: 'qmNumerator', type: 'int', path: 'qm')
            field(field: 'qm.measure', displayName: 'qmMeasure', type: 'String', path: 'qm')
            field(field: 'qm.fromDate', displayName: 'qmFromDate', type: 'Date', path: 'qm')
            field(field: 'qm.toDate', displayName: 'qmToDate', type: 'Date', path: 'qm')
            field(field: 'memberStatus.eligibilityType', displayName: 'statusEligibilityType', type: 'String', path: 'memberStatus')
            field(field: 'memberStatus.fromDate', displayName: 'statusFromDate', type: 'Date', path: 'memberStatus')
            field(field: 'memberStatus.toDate', displayName: 'statusToDate', type: 'Date', path: 'memberStatus')
            field(field: 'memberStatus.status', displayName: 'currentStatus', type: 'String', path: 'memberStatus')
            field(field: 'eligibleDates.fromDate', displayName: 'eligibleDatesFromDate', type: 'Date', path: 'eligibleDates')
            field(field: 'eligibleDates.toDate', displayName: 'eligibleDatesToDate', type: 'Date', path: 'eligibleDates')
            field(field: 'group.id', displayName: 'groupId', type: 'String', path: 'group')
            field(field: 'group.description', displayName: 'groupName', type: 'String', path: 'group')
            field(field: 'group.eligibilityType', displayName: 'groupIdEligibilityType', type: 'string', path: 'group')
            field(field: 'group.fromDate', displayName: 'groupIdFromDate', type: 'Date', path: 'group')
            field(field: 'group.toDate', displayName: 'groupIdToDate', type: 'Date', path: 'group')
        }
        fields(family:'other'){
            field(field: 'eligibleDates', displayName: 'eligibleMonths', type: 'Date')
            field(field: 'gapInCareCount', displayName: 'gapInCareCount', type: 'String')
            field(field: 'gapInCareList', displayName: 'gapInCareList', type: 'String')
        }
    }
}
