/**
 * Created by sktamang on 9/1/16.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
package dastables

tables = {
    table() {
        tableCode('Z5MemberByProgram')
        tableName('Z5MemberByProgram')
        defaultColumns(['Z5MemberByProgram'])

        fields(family: 'Z5MemberByProgram') {
            field(field: 'unblindMemberId', displayName: 'unblindMemberId', type: 'String')
            field(field: 'memberId', displayName: 'memberId', type: 'String')
            field(field: 'programId', displayName: 'programId', type: 'String')
            field(field: 'porgramIdentificationDate', displayName: 'porgramIdentificationDate', type: 'date')
            field(field: 'programEnrollmentDate', displayName: 'programEnrollmentDate', type: 'date')
            field(field: 'statusChangeDate', displayName: 'statusChangeDate', type: 'date')
            field(field: 'groupId', displayName: 'groupId', type: 'String')
            field(field: 'divisionId', displayName: 'divisionId', type: 'String')
            field(field: 'mostRecentStatus', displayName: 'mostRecentStatus', type: 'String')
            field(field: 'updateDate', displayName: 'updateDate', type: 'date')

            field(field: 'udf1_memberByProgram', displayName: 'udf1_memberByProgram', type: 'String')
            field(field: 'udf2_memberByProgram', displayName: 'udf2_memberByProgram', type: 'String')
            field(field: 'udf3_memberByProgram', displayName: 'udf3_memberByProgram', type: 'String')
            field(field: 'udf4_memberByProgram', displayName: 'udf4_memberByProgram', type: 'String')
            field(field: 'udf5_memberByProgram', displayName: 'udf5_memberByProgram', type: 'String')

            field(field: 'udf6_memberByProgram', displayName: 'udf6_memberByProgram', type: 'double')
            field(field: 'udf7_memberByProgram', displayName: 'udf7_memberByProgram', type: 'double')
            field(field: 'udf8_memberByProgram', displayName: 'udf8_memberByProgram', type: 'double')
            field(field: 'udf9_memberByProgram', displayName: 'udf9_memberByProgram', type: 'double')
            field(field: 'udf10_memberByProgram', displayName: 'udf10_memberByProgram', type: 'double')

            field(field: 'udf11_memberByProgram', displayName: 'udf11_memberByProgram', type: 'date')
            field(field: 'udf12_memberByProgram', displayName: 'udf12_memberByProgram', type: 'date')
            field(field: 'udf13_memberByProgram', displayName: 'udf13_memberByProgram', type: 'date')
            field(field: 'udf14_memberByProgram', displayName: 'udf14_memberByProgram', type: 'date')
            field(field: 'udf15_memberByProgram', displayName: 'udf15_memberByProgram', type: 'date')
        }
    }
}