/**
 * Created by sktamang on 8/31/16.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
package dastables

tables = {
    table() {
        tableCode('Z5Program')
        tableName('Z5Program')
        defaultColumns(['Z5Program'])

        fields(family: 'Z5Program') {
            field(field: 'programId', displayName: 'programId', type: 'String')
            field(field: 'programName', displayName: 'programName', type: 'String',phi: true)
            field(field: 'programType', displayName: 'programType', type: 'String')
            field(field: 'programBeginDate', displayName: 'programBeginDate', type: 'date')
            field(field: 'programEndDate', displayName: 'programEndDate', type: 'date',phi: true)
            field(field: 'programManager', displayName: 'programManager', type: 'String',phi: true)
            field(field: 'groupId', displayName: 'groupId', type: 'String', phi: true)
            field(field: 'divisionId', displayName: 'divisionId', type: 'String', phi: true)
            field(field: 'clientId', displayName: 'clientId', type: 'String')
            field(field: 'updateDate', displayName: 'updateDate', type: 'date')

            field(field: 'udf1_program', displayName: 'udf1_program', type: 'String')
            field(field: 'udf2_program', displayName: 'udf2_program', type: 'String')
            field(field: 'udf3_program', displayName: 'udf3_program', type: 'String')
            field(field: 'udf4_program', displayName: 'udf4_program', type: 'String')
            field(field: 'udf5_program', displayName: 'udf5_program', type: 'String')

            field(field: 'udf6_program', displayName: 'udf6_program', type: 'double')
            field(field: 'udf7_program', displayName: 'udf7_program', type: 'double')
            field(field: 'udf8_program', displayName: 'udf8_program', type: 'double')
            field(field: 'udf9_program', displayName: 'udf9_program', type: 'double')
            field(field: 'udf10_program', displayName: 'udf10_program', type: 'double')

            field(field: 'udf11_program', displayName: 'udf11_program', type: 'date')
            field(field: 'udf12_program', displayName: 'udf12_program', type: 'date')
            field(field: 'udf13_program', displayName: 'udf13_program', type: 'date')
            field(field: 'udf14_program', displayName: 'udf14_program', type: 'date')
            field(field: 'udf15_program', displayName: 'udf15_program', type: 'date')


        }
    }
}
