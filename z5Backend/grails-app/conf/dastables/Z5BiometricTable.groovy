package dastables


tables = {
    table() {
        tableCode('Z5Biometric')
        tableName('Z5Biometric')
        defaultColumns(['Z5Biometric'])

        fields(family: 'Z5Biometric') {
            field(field: 'unblindMemberId', displayName: 'unblindMemberId', type: 'String')
            field(field: 'groupId', displayName: 'groupId', type: 'String')
            field(field: 'divisionId', displayName: 'divisionId', type: 'String')
            field(field: 'biometricId', displayName: 'biometricId', type: 'String')
            field(field: 'biometricName', displayName: 'biometricName', type: 'String')
            field(field: 'measuredDate', displayName: 'measuredDate', type: 'date')
            field(field: 'systemUpdateDate', displayName: 'systemUpdateDate', type: 'date')
            field(field: 'updateDate', displayName: 'updateDate', type: 'date')
            field(field: 'biometricValue', displayName: 'biometricValue', type: 'int')
            field(field: 'period', displayName: 'period', type: 'int')
            field(field: 'biometricLevel', displayName: 'biometricLevel', type: 'int')
        }
    }
}
