/**
 * Created by sktamang on 9/1/16.
 Sanjeev Kumar Tamang
 email : tamang88sanjeev@gmail.com
 */
package dastables

tables = {
    table() {
        tableCode('Z5Campaign')
        tableName('Z5Campaign')
        defaultColumns(['Z5Campaign'])

        fields(family: 'Z5Campaign') {
            field(field: 'programId', displayName: 'programId', type: 'String')
            field(field: 'programName', displayName: 'programName', type: 'String')
            field(field: 'campaignId', displayName: 'campaignId', type: 'String')
            field(field: 'campaignName', displayName: 'campaignName', type: 'String')
            field(field: 'campaignSummary', displayName: 'campaignSummary', type: 'String')
            field(field: 'campaignDate', displayName: 'campaignDate', type: 'date')
            field(field: 'campaignType', displayName: 'campaignType', type: 'String')
            field(field: 'campaignTarget', displayName: 'campaignTarget', type: 'String')
            field(field: 'updateDate', displayName: 'updateDate', type: 'date')

            field(field: 'udf1_campaign', displayName: 'udf1_campaign', type: 'String')
            field(field: 'udf2_campaign', displayName: 'udf2_campaign', type: 'String')
            field(field: 'udf3_campaign', displayName: 'udf3_campaign', type: 'String')
            field(field: 'udf4_campaign', displayName: 'udf4_campaign', type: 'String')
            field(field: 'udf5_campaign', displayName: 'udf5_campaign', type: 'String')

            field(field: 'udf6_campaign', displayName: 'udf6_campaign', type: 'double')
            field(field: 'udf7_campaign', displayName: 'udf7_campaign', type: 'double')
            field(field: 'udf8_campaign', displayName: 'udf8_campaign', type: 'double')
            field(field: 'udf9_campaign', displayName: 'udf9_campaign', type: 'double')
            field(field: 'udf10_campaign', displayName: 'udf10_campaign', type: 'double')

            field(field: 'udf11_campaign', displayName: 'udf11_campaign', type: 'date')
            field(field: 'udf12_campaign', displayName: 'udf12_campaign', type: 'date')
            field(field: 'udf13_campaign', displayName: 'udf13_campaign', type: 'date')
            field(field: 'udf14_campaign', displayName: 'udf14_campaign', type: 'date')
            field(field: 'udf15_campaign', displayName: 'udf15_campaign', type: 'date')
        }
    }
}