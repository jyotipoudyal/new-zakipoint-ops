grails.servlet.version = "2.5"
grails.project.target.level = 1.8
grails.project.source.level = 1.8
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.war.file = "target/ztdas.war"

grails.project.dependency.resolver = "maven"
grails.project.dependency.resolution = {

    inherits("global") {
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        grailsPlugins()
        grailsHome()
        grailsCentral()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        mavenLocal()
        mavenCentral()
        mavenRepo "http://download.java.net/maven/2"
        mavenRepo "http://oss.sonatype.org/content/repositories/releases"
        mavenRepo "http://repository.codehaus.org/"
        dependencies {

            compile 'commons-logging:commons-logging:1.1.1'
            compile 'commons-cli:commons-cli:1.2'


            runtime 'org.elasticsearch:elasticsearch:1.4.0'
            //Optional dependency for elasticseach
            runtime 'com.vividsolutions:jts:1.13'


            runtime 'mysql:mysql-connector-java:5.1.34'
            runtime 'ch.qos.logback:logback-core:0.9.17', 'ch.qos.logback:logback-classic:0.9.17',
                    'org.mockito:mockito-core:1.9.5', 'org.mockito:mockito-all:1.9.5'
            test "org.spockframework:spock-grails-support:0.7-groovy-2.0"
            compile 'org.apache.activemq:activemq-spring:5.10.1'
            compile 'org.apache.activemq:activemq-broker:5.10.1'
            compile 'org.springframework:spring-jms:3.1.4.RELEASE'
            compile group: 'joda-time', name: 'joda-time', version: '2.9.9'

            test "org.grails:grails-datastore-test-support:1.0.2-grails-2.4"

        }

        plugins {
            compile ":asset-pipeline:2.2.3"
            runtime ":hibernate4:4.3.8.1"
            build ":tomcat:7.0.55.3" // or ":tomcat:8.0.22"
            compile ":jdbc-pool:7.0.47"
        }

        grails.war.resources = { stagingDir ->
            delete(file: "${stagingDir}/WEB-INF/classes/logback-test.xml")
        }

    }
}
